
ALTER TABLE contacts ADD departmentId INT NULL DEFAULT NULL;
ALTER TABLE contacts ADD CONSTRAINT fk_contacts_departments FOREIGN KEY (departmentId) REFERENCES departments(id);