﻿using System.Configuration;
using Calani.BusinessObjects.Hangfire;
using Hangfire;
using Hangfire.MySql;
using Owin;

namespace Calani.WebappNew
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_180)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseStorage(new MySqlStorage(ConfigurationManager.ConnectionStrings["HangfireConnection"].ConnectionString,
                    new MySqlStorageOptions(){PrepareSchemaIfNecessary = true}));
            
            app.UseHangfireServer();
            app.UseHangfireDashboard("/e2c9d9d8c3a04e6b8240d53c9f0c58e46cc478e0", new DashboardOptions
            {
                Authorization = new[] { new AuthorizationFilter() }
            });
            
            HangfireJobScheduler.CreateOrUpdateRecurringJob("Recurring invoices execution", 
                () => RecurringJobsManager.ProceedRecurringJobs(), "0 7 * * *");
        }
    }
}