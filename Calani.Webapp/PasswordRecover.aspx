﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PasswordRecover" Codebehind="PasswordRecover.aspx.cs" %>

<%@ Register Src="~/assets/Controls/PasswordAdvisor.ascx" TagPrefix="uc1" TagName="PasswordAdvisor" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>GES</title>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<script type="text/javascript" src="assets/js/pages/login.js"></script>
	<!-- /theme JS files -->

    
    <style type="text/css">
        .bg-gesmobile
        {
            background: #934553;
            color:white;
        }

        .color-gesmobile
        {
            color: #934553;
        }
    </style>

</head>

<body class="login-container">

        <ul class="nav navbar-nav navbar-right">
			<li class="dropdown language-switch">
				<%= LanguageSwitch %>
			</li>
        </ul>

    <form runat="server">
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Password recovery -->
					<asp:Panel runat="server" id="PanelRecover">
						<div class="panel panel-body login-form">


							<div class="text-center">
								<div class="icon-object border-warning text-warning"><i class="icon-spinner11"></i></div>
								<h5 class="content-group"><%= Resources.Resource.PasswordRecovery %> <small class="display-block"><%= Resources.Resource.InstructionSentByEmail %></small></h5>
							</div>

							<div class="form-group has-feedback">
								<input type="email" class="form-control" placeholder='<%$ Resources:Resource, EmailAddress %>' runat="server" id="tbxEmail">
								<div class="form-control-feedback">
									<i class="icon-mail5 text-muted"></i>
								</div>
							</div>

							<asp:Button runat="server" ID="btnReset" type="submit" class="btn bg-gesmobile btn-block" Text='<%$ Resources:Resource, ResetPassword %>'  OnClick="btnReset_Click" />
						</div>
					</asp:Panel>
					<!-- /password recovery -->

                    <asp:Panel runat="server" ID="PanelSuccess" Visible="false">
							<div class="panel panel-body login-form">
								<div class="panel registration-form">
									<div class="panel-body">
										<div class="text-center">
											<div class="icon-object border-success text-success"><i class="icon-envelop2"></i></div>
											<h5 class="content-group-lg text-success"><%= Resources.Resource.RecoverMailSent %> </h5>
										</div>
                                    </div>
                                </div>
                            </div>
                    </asp:Panel>

                    <asp:Panel runat="server" ID="PanelError" >
						<div class="row">
							<div class="col-lg-6 col-lg-offset-3">
								<div class="panel registration-form">
									<div class="panel-body">
										<div class="text-center">
											<div class="icon-object border-danger text-danger"><i class="icon-user-cancel"></i></div>
											<h5 class="content-group-lg text-danger"><%= Resources.Resource.ERR_ActivationLink %> 
										</div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>

                    <!-- Registration form -->
					<asp:Panel runat="server" ID="PanelChangePassword">
						<div class="row">
							<div class="col-lg-6 col-lg-offset-3">
								<div class="panel registration-form">
									<div class="panel-body">
										<div class="text-center">
											<div class="icon-object border-success text-success"><i class="icon-key"></i></div>
											<h5 class="content-group-lg"><%= Resources.Resource.RecoverYourPassword_button %></h5>
										</div>

                                        <div class="alert alert-danger alert-bordered" id="lblError" runat="server" visible="false">
								            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only"><%= Resources.Resource.Close %></span></button>
								            <span class="text-semibold"><asp:Label id="lblErrorText" runat="server" /></span>
							            </div>

                                        <div class="alert alert-success success-bordered" id="lblSuccess" runat="server" visible="false">
								            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only"><%= Resources.Resource.Close %></span></button>
								            <span class="text-semibold"><%= Resources.Resource.NewPasswordSet %></span>
							            </div>
										
                                        <asp:Panel runat="server" ID="PanelForm">
										    <div class="row">
											    <div class="col-md-6">
												    <div class="form-group has-feedback">
													    <input style="padding-right: 48px" type="password" class="form-control" runat="server" id="tbxPassword" placeholder='<%$ Resources:Resource, CreatePassword %>'>
													    <div class="form-control-feedback" style="width: 50px !important; pointer-events:all">
														    <button type="button" style="padding: 0; border:none; background: transparent" onclick="togglePasswordVisibility('tbxPassword')">
															    <i id="tbxPasswordIcon" class="icon-eye-blocked text-muted"></i>
														    </button>
														    <i class="icon-user-lock text-muted"></i>
													    </div>
												    </div>
											    </div>

											    <div class="col-md-6">
												    <div class="form-group has-feedback">
													    <input style="padding-right: 48px" type="password" class="form-control" runat="server" id="tbxPassword2" placeholder='<%$ Resources:Resource, ConfirmPassword %>'>
													    <div class="form-control-feedback" style="width: 50px !important; pointer-events:all">
														    <button type="button" style="padding: 0; border:none; background: transparent" onclick="togglePasswordVisibility('tbxPassword2')">
															    <i id="tbxPassword2Icon" class="icon-eye-blocked text-muted"></i>
														    </button>
														    <i class="icon-user-lock text-muted"></i>
													    </div>
												    </div>
											    </div>
										    </div>
                                            <div class="row">
                                                <div class="col-md-12" style="margin-bottom:20px">
                                                    <uc1:PasswordAdvisor runat="server" ID="PasswordAdvisor" PasswordInput="tbxPassword" />
                                                </div>
                                            </div>


										    <div class="text-right">
											    <asp:Button runat="server" ID="btnChangePassword" type="submit" CssClass="btn bg-teal-400  ml-10"
                                                    Text='<%$ Resources:Resource, Save %>' OnClick="btnChangePassword_Click"/>
										    </div>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="PanelEnd" Visible="false">
                                            <div class="form-group">
                                                <a class="btn bg-blue btn-block"  href="Login.aspx" ><%= Resources.Resource.Login %></a>
                                            </div>
                                        </asp:Panel>
									</div>
								</div>
							</div>
						</div>
					</asp:Panel>
					<!-- /registration form -->
                    
                    <!-- Footer -->
                    <div class="row  text-muted text-center">
                        &copy; <asp:Label runat="server" ID="LabelCopyYear" />. 
                        <asp:HyperLink runat="server" ID="HyperLinkCopy" />
                    </div>
                    <!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->
        

	</div>
	<!-- /page container -->
    </form>
	<script >
		function togglePasswordVisibility(id){
		    let $input = $(`#${id}`);
			if ($input.attr('type') === 'password')$input.attr('type', 'text');
			else $input.attr('type', 'password');
						
			$(`#${id}Icon`).toggleClass('icon-eye icon-eye-blocked');
		}
	</script>
</body>
</html>
