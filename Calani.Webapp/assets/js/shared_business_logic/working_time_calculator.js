﻿
class WorkingTimeCalculator {

    constructor(startWorkingHour, startWorkingMin, endWorkingHour, endWorkingMin, lunchBreakDurationMin, absenceDayDurationMin) {

        if (!startWorkingHour || !endWorkingHour || startWorkingHour >= endWorkingHour) {
            startWorkingHour = 9;
            endWorkingHour = 18;
        }

        this._startWorkingHour = startWorkingHour;
        this._startWorkingMin = startWorkingMin;
        this._endWorkingHour = endWorkingHour;
        this._endWorkingMin = endWorkingMin;
        this._lunchBreakDurationMin = lunchBreakDurationMin ?? 60;
        this._workingDayDurationHours = (this._endWorkingHour * 60 + this._endWorkingMin - this._startWorkingHour * 60 - this._startWorkingMin - this._lunchBreakDurationMin) * 1.0 / 60;
        this._absenceDayDurationMin = absenceDayDurationMin;
    }

    get StartWorkingHour() {
        return this._startWorkingHour;
    }

    get StartWorkingMin() {
        return this._startWorkingMin;
    }

    get EndWorkingHour() {
        return this._endWorkingHour;
    }

    get EndWorkingMin() {
        return this._endWorkingMin;
    }

    get LunchBreakDurationMin() {
        return this._lunchBreakDurationMin;
    }

    get WorkingDayDurationHours() {
        return this._workingDayDurationHours;
    }

    get WorkingDayDurationMinutes() {
        return this._workingDayDurationHours * 60;
    }

    get AbsenceDayDurationMin() {
        return this._absenceDayDurationMin;
    }

    get AbsenceDayDurationHours() {
        return this._absenceDayDurationMin / 60;
    }

    getDateZeroTime(date) {
        if (!date) return null;

        return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }

    getNextDayDate(date) {
        if (!date) return null;

        var nextDay = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 1);

        return nextDay;
    }

    getSameDayDifferentTime(date, hours, mins) {
        if (!date) return null;

        var newDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), hours, mins);

        return newDate;
    }

    getWorkingMinutesSameDay(date1, date2) {
        /*
         * date1 and date2 are expected to be the same day, different time
         */

        if (!date1 || !date2) return 0;

        var dayOfWeek = date1.getDay();
        if (date1 >= date2 ||
            dayOfWeek == 0 || // Sunday
            dayOfWeek == 6) { // Saturday
            return 0;
        }

        var start = date1;
        var end = date2;

        var workingMinutes = (end - start) / (1000 * 60);

        var halfWorkingDayMin = (this._workingDayDurationHours / 2) * 60;
        if (workingMinutes > halfWorkingDayMin) {
            // Subtract lunch break
            workingMinutes -= this._lunchBreakDurationMin;
        }

        // If more hours are selected within a day than working day duration - duration is working / absence day
        if (workingMinutes >= this.WorkingDayDurationMinutes) {
            workingMinutes = this.WorkingDayDurationMinutes;
        }

        return workingMinutes;
    }

    areDatesSameDay(date1, date2) {
        if (!date1 || !date2) return false;

        return date1.getFullYear() == date2.getFullYear()
            && date1.getMonth() == date2.getMonth()
            && date1.getDate() == date2.getDate();
    }

    getWorkingMinutesBetween2Dates(date1, date2) {

        if (!date1 || !date2) return 0;

        if (!date1.getDate && typeof (date1) !== 'string' ||
            !date2.getDate && typeof (date2) !== 'string') {
            console.error("WorkingTimeCalculator.getWorkingMinutesBetween2Dates: invalid dates");
            return 0;
        }

        if (date1 >= date2) return 0;

        if (typeof date1 != "date") date1 = new Date(Date.parse(date1));
        if (typeof date2 != "date") date2 = new Date(Date.parse(date2));

        date1.setSeconds(0);
        date1.setMilliseconds(0);
        date2.setSeconds(0);
        date2.setMilliseconds(0);

        // If dates are the same day
        if (this.areDatesSameDay(date1, date2)) {
            return this.getWorkingMinutesSameDay(date1, date2);
        }

        var workingMinutes = 0;

        var firstDayEnd = this.getSameDayDifferentTime(date1, this._endWorkingHour, this._endWorkingMin);
        workingMinutes += this.getWorkingMinutesSameDay(date1, firstDayEnd);

        var date = this.getNextDayDate(date1);
        while (this.areDatesSameDay(date, date2) == false) {
            var dayOfWeek = date.getDay();
            var isWeekend = dayOfWeek == 0 || dayOfWeek == 6;
            workingMinutes +=  isWeekend ? 0 : this._workingDayDurationHours * 60;
            date = this.getNextDayDate(date);
        }

        var lastDayStart = this.getSameDayDifferentTime(date2, this._startWorkingHour, this._startWorkingMin);
        workingMinutes += this.getWorkingMinutesSameDay(lastDayStart, date2);

        return workingMinutes;
    }

    getCurrentWeekStartAndEndDates() {

        var today = new Date();
        var start = today.getDate() - today.getDay() + 1;
        var end = start + 6;

        return [new Date(today.setDate(start)), new Date(today.setDate(end))];
    }

    getMinutesDifference(time1, time2) {
        let [hours1, minutes1] = time1.split(':').map(Number);
        let [hours2, minutes2] = time2.split(':').map(Number);

        let totalMinutes1 = hours1 * 60 + minutes1;
        let totalMinutes2 = hours2 * 60 + minutes2;

        return Math.abs(totalMinutes2 - totalMinutes1);
    }
}
