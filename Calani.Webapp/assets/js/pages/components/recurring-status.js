﻿function getRecurringStatusHtml(projectStatus) {

    let text = '';
    let cssClass = '';
    let iconHtml = '';

    switch (projectStatus) {
        case true:
            text = "<strong> " + STRING_RESOURCES["Active"] + "</strong>";
            cssClass = "label bg-success";
            iconHtml = "<i class=\"icon-checkmark\"></i>";
            break;

        case false:
            text = "<strong> " + STRING_RESOURCES["Disable"] + "</strong>";
            cssClass = "label bg-danger";
            iconHtml = "<i class=\"icon-trash\"></i>";
            break;
        default:
            break;
    }
    
    return '<span class="' + cssClass + '">' + iconHtml + '</span><span>' + text + '</span>';
}

function getIsActiveNameble(isActive) {
    switch (isActive) {
        case true:
            return ressources["Active"];
        case false:
            return ressources["Disable"];
    }
}

function getFrequencyNameble(frequency) {
    switch (frequency) {
        case 1:
            return ressources["Week"];
        case 2:
            return ressources["Month"];
        case 3:
            return ressources["Year"];
    }
}