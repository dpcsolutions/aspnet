﻿function getProjectStatusHtml(projectStatus) {

    var text = '';
    var cssClass = '';
    var iconHtml = '';

    switch (projectStatus) {

        case "DeliveryNoteDraft":
            text = "<em>" + STRING_RESOURCES["Draft"] + "</em>";
            cssClass = "label bg-grey";
            iconHtml = "<i class=\"icon-floppy-disk\"></i>";
            break;

        case "DeliveryNoteSent":
            text = STRING_RESOURCES["Sent"];
            cssClass = "label bg-primary";
            iconHtml = "<i class=\"icon-envelop\"></i>";
            break;

        case "DeliveryNoteAccepted":
            text = "<strong>" + STRING_RESOURCES["Accepted"] + "</strong>";
            cssClass = "label bg-success animated tada infinite";
            iconHtml = "<i class=\"icon-checkmark\"></i>";
            break;

        case "DeliveryNoteRefused":
            text = "<strong>" + STRING_RESOURCES["Declined"] + "</strong>";
            cssClass = "label bg-danger";
            iconHtml = "<i class=\"icon-trash\"></i>";
            break;

        case "QuoteDraft":
            text = "<em>" + STRING_RESOURCES["Draft"] + "</em>";
            cssClass = "label bg-grey";
            iconHtml = "<i class=\"icon-floppy-disk\"></i>";
            break;

        case "QuoteSent":
            text = STRING_RESOURCES["Sent"];
            cssClass = "label bg-primary";
            iconHtml = "<i class=\"icon-envelop\"></i>";
            break;

        case "QuoteAccepted":
            text = "<strong>" + STRING_RESOURCES["Accepted"] + "</strong>";
            cssClass = "label bg-success animated tada infinite";
            iconHtml = "<i class=\"icon-checkmark\"></i>";
            break;

        case "QuoteRefused":
            text = "<strong>" + STRING_RESOURCES["Declined"] + "</strong>";
            cssClass = "label bg-danger";
            iconHtml = "<i class=\"icon-trash\"></i>";
            break;

        case "QuoteOutDated":
            text = "<strong>" + STRING_RESOURCES["OutdatedQuote"] + "</strong>";
            cssClass = "label bg-grey animated tada infinite";
            iconHtml = "<i class=\"icon-calendar52\"></i>";
            break;

        case "JobOpened":
            text = STRING_RESOURCES["Opened"];
            cssClass = "label bg-primary";
            iconHtml = "<i class=\"icon-play4\"></i>";
            break;

        case "JobFinished":
            text = STRING_RESOURCES["Finished"];
            cssClass = "label bg-success animated tada infinite";
            iconHtml = "<i class=\"icon-finish\"></i>";
            break;

        case "JobOnHold":
            text = "<strong>" + STRING_RESOURCES["On_hold"] + "</strong>";
            cssClass = "label bg-orange";
            iconHtml = "<i class=\"icon-pause2\"></i>";
            break;

        case "JobOutDated":
            text = "<strong>" + STRING_RESOURCES["OutdatedJob"] + "</strong>";
            cssClass = "label bg-grey animated tada infinite";
            iconHtml = "<i class=\"icon-calendar2\"></i>";
            break;


        case "InvoiceDraft":
            text = "<em>" + STRING_RESOURCES["Draft"] + "</em>";
            cssClass = "label bg-grey";
            iconHtml = "<i class=\"icon-floppy-disk\"></i>";
            break;

        case "InvoicePaymentDue":
            text = "<strong>" + STRING_RESOURCES["Pending"] + "</strong>";
            cssClass = "label bg-primary";
            iconHtml = "<i class=\"icon-bell-check\"></i>";
            break;

        case "InvoicePaid":
            text = "<strong>" + STRING_RESOURCES["Paid"] + "</strong>";
            cssClass = "label bg-success";
            iconHtml = "<i class=\"icon-cash2\"></i>";
            break;

        case "InvoicePaymentLate":
            text = "<strong>" + STRING_RESOURCES["Late"] + "</strong>";
            cssClass = "label bg-danger animated tada infinite";
            iconHtml = "<i class=\"icon-bell2\"></i>";
            break;

        default:
            break;
    };

    var html = '<span class="' + cssClass + '">' + iconHtml + '</span><span>' + text + '</span>';

    return html;
}

function getProjectStatusNamable(projectStatus) {
    switch (projectStatus) {
        case "DeliveryNoteDraft":
            return STRING_RESOURCES["Draft"];
        case "DeliveryNoteSent":
            return STRING_RESOURCES["Sent"];
        case "DeliveryNoteAccepted":
            return STRING_RESOURCES["Accepted"];
        case "DeliveryNoteRefused":
            return STRING_RESOURCES["Declined"];
        case "QuoteDraft":
            return STRING_RESOURCES["Draft"];
        case "QuoteSent":
            return STRING_RESOURCES["Sent"];
        case "QuoteAccepted":
            return STRING_RESOURCES["Accepted"];
        case "QuoteRefused":
            return STRING_RESOURCES["Declined"];
        case "QuoteOutDated":
            return STRING_RESOURCES["OutdatedQuote"];
        case "JobOpened":
            return STRING_RESOURCES["Opened"];
        case "JobFinished":
            return STRING_RESOURCES["Finished"];
        case "JobOnHold":
            return STRING_RESOURCES["On_hold"];
        case "JobOutDated":
            return STRING_RESOURCES["OutdatedJob"];
        case "InvoiceDraft":
            return STRING_RESOURCES["Draft"];
        case "InvoicePaymentDue":
            return STRING_RESOURCES["Pending"];
        case "InvoicePaid":
            return STRING_RESOURCES["Paid"];
        case "InvoicePaymentLate":
            return STRING_RESOURCES["Late"];
        default:
            return;
    }
}