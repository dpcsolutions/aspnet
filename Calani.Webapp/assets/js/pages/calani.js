/* ------------------------------------------------------------------------------
*
*  # Buttons extension for Datatables. HTML5 examples
*
*  Specific JS code additions for datatable_extension_buttons_html5.html page
*
*  Version: 1.1
*  Latest update: Mar 6, 2016
*
* ---------------------------------------------------------------------------- */

var CALANI = {
    DEFAULTS: {
        dateFormat: 'DD/MM/YYYY',
        dateTimeFormat: 'DD.MM.YYYY HH:mm:ss',
    }
};

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function initDataTable(dt, firstColDesc, isGrouping, footerTotal, pageLength, aggregatedColumnIdx, noDefaultSort, columns) {

    var initOrder = null;
    if (firstColDesc)
        initOrder = [[0, "desc"]];
    else if (!noDefaultSort)
        initOrder = [[0, "asc"]];
    else
        initOrder = [];

    var defaultsort = null;
    if (dt.length > 0) {
        defaultsort = $(dt[0]).data('defaultsort');
        if (defaultsort != null) {
            defaultsort = defaultsort.split(';');
            if (defaultsort.length == 2) {
                initOrder = [[parseInt(defaultsort[0]), defaultsort[1]]];
                
            }
        }
    }
    function parsePotentiallyGroupedFloat(stringValue) {
        stringValue = stringValue.trim();
        var result = stringValue.replace(/[^0-9]/g, '');
        if (/[,\.]\d{2}$/.test(stringValue)) {
            result = result.replace(/(\d{2})$/, '.$1');
        }
        return parseFloat(result);
    }

    var options = {
        stateSave: true,
        buttons: {
            buttons: [
                {
                    extend: 'copyHtml5',
                    className: 'btn btn-default',
                    exportOptions: {
                        columns: [0, ':visible']
                    }
                },
                {
                    extend: 'excelHtml5',
                    className: 'btn btn-default',
                    exportOptions: {
                        columns: ':visible',
                        format: {
                            body: function (data, row, column, node) {

                                data = data.replace(/<a\b[^>]*>/i, "").replace(/<\/a>/i, "");

                                data = data.replace(/&nbsp;/i, " ");

                                var div = $('<div />');
                                $(div).append(data);
                                var clone = $(div).clone();
                                data = clone.text();


                                var numdata = data.replace(/\s/g, "").replace(/'*/i, "");

                                numdata = numdata.replace(',', '.');



                                if ($.isNumeric(numdata))
                                    return parseFloat(numdata);

                                return data;

                            }
                        }
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                    className: 'btn bg-blue btn-icon'
                }
            ],
        },
        fixedHeader: true,
        order: initOrder,
        pageLength: pageLength || 10,
        "language": {
            "url": assets + "js/plugins/tables/datatables/languages/" + lang + ".js"
        },
        orderFixed: isGrouping ? [1, 'asc'] : undefined,

        rowGroup: isGrouping ? {

            dataSrc: 1,

            startRender: function (rows, group) {

                var r = this.c.dataSrc;
                var content = group;
                if (r == 9) {
                    var str = rows.data()[0][6];
                    content = str.display;
                }

                if (typeof (group) === 'object' && group.display) {
                    content = group.display;
                }

                return content;
            },

            endRender: function (rows, group) {

                var aggrColIdx = typeof (aggregatedColumnIdx) != 'undefined' ? aggregatedColumnIdx : 5;

                var sum = rows
                    .data()
                    .pluck(aggrColIdx)
                    .reduce(function (a, b) {
                        return a + parseInt(b, 10);
                    }, 0);


                var r = this.c.dataSrc;
                var content = group;

                if (r == 9) {
                    var str = rows.data()[0][6];
                    content = str.display;
                }

                if (typeof (group) === 'object' && group.display) {
                    content = group.display;
                }

                return $('<tr/>')
                    .append('<td colspan="7" class="text-right">' + txtTotalFor + ' ' + content + ' :</td>')
                    .append('<td class="text-right">' + sum + '</td>')
                    ;

            }
        } : undefined,

        "footerCallback": footerTotal ? function (tfoot, data, start, end, display) {
            var api = this.api();
            // Total over all pages
            var rows = api.rows({ search: 'applied' });

            var total = rows.data()
                .reduce(function (a, b, ind, d) {
                    var col5 = b[5];
                    return parseInt(a, 10) + parseInt(col5, 10);
                }, 0);

            $(tfoot).html('<td colspan="7" class="text-right text-black">' + txtTotalForPeriod + '</td>' +
                '<td class="text-right "><span class="text-black">' + total + '</span></td>');
        } : undefined

    };

    if (columns) {
        options.columns = columns;
    }

    var table = dt.DataTable(options);
    return table;
}

function getDataTableOptions(dt, firstColDesc, isGrouping, footerTotal, pageLength, aggregatedColumnIdx) {

    var initOrder = [[0, firstColDesc ? "desc" : "asc"]];

    var defaultsort = null;
    if (dt.length > 0) {
        defaultsort = $(dt[0]).data('defaultsort');
        if (defaultsort != null) {
            defaultsort = defaultsort.split(';');
            if (defaultsort.length == 2) {
                initOrder = [[parseInt(defaultsort[0]), defaultsort[1]]];
            }
        }
    }

    var options = {
        stateSave: true,
        buttons: {
            buttons: [
                {
                    extend: 'copyHtml5',
                    className: 'btn btn-default',
                    exportOptions: {
                        columns: [0, ':visible']
                    }
                },
                {
                    extend: 'excelHtml5',
                    className: 'btn btn-default',
                    exportOptions: {
                        columns: ':visible',
                        format: {
                            body: function (data, row, column, node) {

                                data = ("" + data).replace(/<a\b[^>]*>/i, "").replace(/<\/a>/i, "");
                                data = data.replace(/&nbsp;/i, " ");

                                var div = $('<div />');
                                $(div).append(data);
                                var clone = $(div).clone();
                                data = clone.text();

                                var numdata = data.replace(/\s/g, "").replace(/'*/i, "");
                                numdata = numdata.replace(',', '.');

                                if ($.isNumeric(numdata)) {
                                    return parseFloat(numdata);
                                }

                                return data;
                            }
                        }
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                    className: 'btn bg-blue btn-icon'
                }
            ],
        },
        fixedHeader: true,
        order: initOrder,
        pageLength: pageLength || 10,
        "language": {
            "url": assets + "js/plugins/tables/datatables/languages/" + lang + ".js"
        },
        orderFixed: isGrouping ? [1, 'asc'] : undefined,
        rowGroup: isGrouping ? {
            dataSrc: 1,
            startRender: function (rows, group) {
                var r = this.c.dataSrc;
                var content = group;
                if (r == 9) {
                    var str = rows.data()[0][6];
                    content = str.display;
                }

                if (typeof (group) === 'object' && group.display) {
                    content = group.display;
                }

                return content;
            },
            endRender: function (rows, group) {
                var aggrColIdx = typeof (aggregatedColumnIdx) != 'undefined' ? aggregatedColumnIdx : 5;
                var sum = rows
                    .data()
                    .pluck(aggrColIdx)
                    .reduce(function (a, b) {
                        return a + parseInt(b, 10);
                    }, 0);


                var r = this.c.dataSrc;
                var content = group;

                if (r == 9) {
                    var str = rows.data()[0][6];
                    content = str.display;
                }

                if (typeof (group) === 'object' && group.display) {
                    content = group.display;
                }

                return $('<tr/>')
                    .append('<td colspan="7" class="text-right">' + txtTotalFor + ' ' + content + ' :</td>')
                    .append('<td class="text-right">' + sum + '</td>');
            }
        } : undefined,

        "footerCallback": footerTotal ? function (tfoot, data, start, end, display) {
            var api = this.api();
            // Total over all pages
            var rows = api.rows({ search: 'applied' });
            var total = rows.data()
                .reduce(function (a, b, ind, d) {
                    var col5 = b[5];
                    return parseInt(a, 10) + parseInt(col5, 10);
                }, 0);

            $(tfoot).html('<td colspan="7" class="text-right text-black">' + txtTotalForPeriod + '</td>' +
                '<td class="text-right "><span class="text-black">' + total + '</span></td>');
        } : undefined
    };

    $.fn.dataTable.ext.type.order['date-pre'] = function (data) {
        return moment(data, CALANI.DEFAULTS.dateFormat).toDate();
    };

    return options;
}

function getPeriodFilterDates() {
    var selected = $("#ddlPeriod").val();
    var dates = selected.split(";");
    for (var i = 0; i < dates.length; i++) {
        if (isNaN(parseInt(dates[i]))) {
            dates[i] = 0;
        }
    }
    return dates;
}

function parseDate(dateStr) {
    if (dateStr) return new Date(parseInt(dateStr.substr(6)));
    return null;
}

function displayDate(dateStr, format) {
    if (dateStr) return parseDate(dateStr).format(format ? format : 'dd.mm.yyyy');
    return '';
}

function displayMoney(value) {
    return value.toFixed(2);
}

function displayDouble(value) {
    return Math.round(value * 100) / 100;
}

function displayRating(rating) {

    if (rating == null || rating <= 0) {
        return '';
    }

    var ratingNum = 1 * rating;
    var html = '<span style="display:none;">R' + rating + '</span><span class="stars">';
    for (var i = 1; i <= ratingNum; i++) {
        html += '<i class="fa fa-star" runat="server" id="ir' + i + '"></i>'
    }

    html += '</span>';

    return html;
}

function dateTimeToTicks(date) {
    return date && date.getTime ? date.getTime() * 10000 + 621355968000000000 : 0;
}

function datesEqual(date1, date2) {
    if (typeof (date1) !== typeof (date2)) return false;
    if (!date1.getFullYear || !date2.getFullYear) return false;

    return date1.getFullYear() == date2.getFullYear() &&
           date1.getMonth() == date2.getMonth() &&
           date1.getDate() == date2.getDate();
}

var toggleTuto_url = null;
var toggleTuto_varname = null;

function toggleTuto(showmessage) {
    $('#panelTuto').toggle();

    if (toggleTuto_varname == null) return;

    var toggleTuto_value = "0";
    if ($('#panelTuto').is(":visible")) toggleTuto_value = "1";


    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: toggleTuto_url,
        data: JSON.stringify({ name: toggleTuto_varname, value: toggleTuto_value }),
        processData: false,
        dataType: "json",
        success: function (response) {
        

        },
        error: function (a, b, c) {
            
        }
    });

    if (showmessage == true) {
        window.swal({
            html: restoreTutoMsg,
            showConfirmButton: true
        })

    }

}

function checkHHMM(ctrl) {
    var str = ctrl.val();
    var p = str.split(":");
    var force = false;
    if (p.length == 1) {
        str = str + ":00";
        p = str.split(":");
        force = true;
    }

    if (p.length == 2) {

        p[0] = p[0].replace('_', '');
        p[0] = p[0].replace('_', '');
        p[1] = p[1].replace('_', '');
        p[1] = p[1].replace('_', '');

        if (p[0] == '') p[0] = '00';
        if (p[1] == '') p[1] = '00';

        var h = '00';
        var m = '00';

        try {

            h = parseInt(p[0]);
            m = parseInt(p[1]);
            if (isNaN(h)) h = 0;
            if (isNaN(m)) m = 0;
            if (h > 23) h = 23;
            if (m > 59) m = 59;
            h = h + '';
            m = m + '';
            if (h.length < 2) h = '0' + h;
            if (m.length < 2) m = '0' + m;

        } catch (e) {

        }

        var str2 = h + ':' + m;
        if (str2 != str || force == true) {
            ctrl.val(str2);
        }

    }
}

$(function () {


    var modalTemplate = '<div class="modal-dialog modal-lg" role="document">\n' +
        '  <div class="modal-content">\n' +
        '    <div class="modal-header">\n' +
        '      <div class="kv-zoom-actions btn-group">{toggleheader}{fullscreen}{borderless}{close}</div>\n' +
        '      <h6 class="modal-title">{heading} <small><span class="kv-zoom-title"></span></small></h6>\n' +
        '    </div>\n' +
        '    <div class="modal-body">\n' +
        '      <div class="floating-buttons btn-group"></div>\n' +
        '      <div class="kv-zoom-body file-zoom-content"></div>\n' + '{prev} {next}\n' +
        '    </div>\n' +
        '  </div>\n' +
        '</div>\n';

    var previewZoomButtonClasses = {
        toggleheader: 'btn btn-default btn-icon btn-xs btn-header-toggle',
        fullscreen: 'btn btn-default btn-icon btn-xs',
        borderless: 'btn btn-default btn-icon btn-xs',
        close: 'btn btn-default btn-icon btn-xs'
    };

    var previewZoomButtonIcons = {
        prev: '<i class="icon-arrow-left32"></i>',
        next: '<i class="icon-arrow-right32"></i>',
        toggleheader: '<i class="icon-menu-open"></i>',
        fullscreen: '<i class="icon-screen-full"></i>',
        borderless: '<i class="icon-alignment-unalign"></i>',
        close: '<i class="icon-cross3"></i>'
    };

    var fileActionSettings = {
        zoomClass: 'btn btn-link btn-xs btn-icon',
        zoomIcon: '<i class="icon-zoomin3"></i>',
        dragClass: 'btn btn-link btn-xs btn-icon',
        dragIcon: '<i class="icon-three-bars"></i>',
        removeClass: 'btn btn-link btn-icon btn-xs',
        removeIcon: '<i class="icon-trash"></i>',
        indicatorNew: '<i class="icon-file-plus text-slate"></i>',
        indicatorSuccess: '<i class="icon-checkmark3 file-icon-large text-success"></i>',
        indicatorError: '<i class="icon-cross2 text-danger"></i>',
        indicatorLoading: '<i class="icon-spinner2 spinner text-muted"></i>'
    };


    // Steps Wizard
    // ------------------------------
    
    $(".steps-basic").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        enableAllSteps: true, 
        titleTemplate: '<span class="number">#index#</span> #title#',
        labels: {
            next: tutrx[0],
            previous: tutrx[1],
            finish: tutrx[2]
        },
        onFinished: function (event, currentIndex) {
            toggleTuto();
        }
    });

    $('#btnToggleTuto').click(function (e) {
        toggleTuto(false);
    });

    


    // Bar Rating
    // ------------------------------

    $('.barrating').barrating({
        theme: 'fontawesome-stars'
    });


    // File upload
    // ------------------------------
    $('.file-input').fileinput({
        browseLabel: '...',
        removeLabel: '',
        browseIcon: '<i class="icon-file-plus"></i>',
        showUpload: false,
        removeIcon: '<i class="icon-cross3"></i>',
        layoutTemplates: {
            icon: '<i class="icon-file-check"></i>',
            modal: modalTemplate
        },
        initialCaption: "",
        previewZoomButtonClasses: previewZoomButtonClasses,
        previewZoomButtonIcons: previewZoomButtonIcons,
        fileActionSettings: fileActionSettings
    });


    // Select 2 setup
    // ------------------------------
    $('.select').select2({
        minimumResultsForSearch: Infinity
    });
    $('.select-search').select2();

    // Styled checkbox
    // ------------------------------
     $(".styled, .multiselect-container input").uniform({
        radioClass: 'choice'
    });

    // touchspin
    // --------------
    if($(".touchspin-set-value").length != 0)
    {
        $(".touchspin-set-value").TouchSpin({
                initval: 1
            });
    }

    


    // Date Picker
    // -----------

    $('.daterange-basic').daterangepicker({
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default',
        locale: {
            format: CALANI.DEFAULTS.dateFormat
        }
    });

    $('.toggle-date-input-method-alt').click(function()
    {
        $('.date-input-method-alt').toggle();
    });

    $('.daterange-single').pickadate({
        singleDatePicker: true,
        selectYears: true,
        
        selectMonths: true,
        format: CALANI.DEFAULTS.dateFormat
    });




    // Animations
    // ----------

    // datatable highlight
    $('.datatable-demo tbody').on('mouseover', 'td', function() {

        var table = this.closest('table');
        table = $(table).DataTable();

        
        var colIdx = table.cell(this).index().column;

        $(table.cells().nodes()).removeClass('active');
        $(table.column(colIdx).nodes()).addClass('active');

    }).on('mouseleave', function() {
        $(table.cells().nodes()).removeClass('active');
    });

    
    
    

    // Table setup
    // ------------------------------

    // Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '_INPUT_',
            searchPlaceholder: '',
            lengthMenu: '_MENU_',
            paginate: { 'first': '&lArr;', 'last': '&rArr;', 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });


    

    // Column selectors
    initDataTable($('.datatable-dynamic'));


  


    // ColReorder integration
    var table_reorder = $('.datatable-header-reorder').DataTable({
        fixedHeader: true,
        colReorder: true
    });


    // Adjust table header if sidebar toggler is clicked
    $('.sidebar-control').on('click', function() {
        table_basic.fixedHeader.adjust();
        table_footer.fixedHeader.adjust();
        table_offset.fixedHeader.adjust();
        table_reorder.fixedHeader.adjust();
    });






    // External table additions
    // ------------------------------

    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });



    var events = [
            {
                title: 'Ferudus compound - Arrachage arbre (presta suuplémentaire)',
                start: '2018-01-04T10:30:00',
                end: '2018-01-04T12:30:00',
                color:'#388E3C',
                resourceId: 'a'
            },
            {
                title: 'Dumbo Jardinage - Gazon',
                start: '2018-01-04T08:30:00',
                end: '2018-01-04T10:30:00',
                color: '#388E3C',
                resourceId: 'a'
            },
            {
                title: 'Dumbo Jardinage - Gazon',
                start: '2018-01-04T08:30:00',
                end: '2018-01-04T10:30:00',
                color: '#388E3C',
                resourceId: 'd'
            },
            {
                title: 'Ferudus compound - Forfait janvier 1/3',
                start: '2018-01-04T09:00:00',
                end: '2018-01-04T11:00:00',
                color: '#388E3C',
                resourceId: 'c'
            },
            {
                title: 'Mecaprom - Prestation scarification gazon',
                start: '2018-01-04T09:30:00',
                end: '2018-01-04T11:30:00',
                color: '#388E3C',
                resourceId: 'b'
            },
            {
                title: 'Simply Nie - arrosage',
                start: '2018-01-04T12:30:00',
                end: '2018-01-04T14:30:00',
                color: '#1976D2',
                resourceId: 'd'
            },
            {
                title: 'Securitus - palmiers',
                start: '2018-01-04T15:30:00',
                end: '2018-01-04T16:30:00',
                color: '#1976D2',
                resourceId: 'b'
            },
            {
                title: 'Meeting',
                start: '2018-01-04T16:30:00',
                end: '2018-01-04T17:30:00',
                color: '#1976D2',
                resourceId: 'a'
            },
            {
                title: 'Ferudus compound - Forfait janvier 2/3',
                start: '2018-01-05T08:45:00',
                end: '2018-01-05T11:15:00',
                color: '#1976D2',
                resourceId: 'b'
            },
            {
                title: 'Meeting',
                start: '2018-01-05T12:00:00',
                end: '2018-01-05T14:00:00',
                color: '#1976D2',
                resourceId: 'c'
            },
            {
                title: 'Meeting',
                start: '2018-01-05T12:00:00',
                end: '2018-01-05T13:30:00',
                color: '#1976D2',
                resourceId: 'a'
            },
            {
                title: 'Meeting',
                start: '2018-01-05T14:30:00',
                end: '2018-01-045T14:30:00',
                color: '#D32F2F',
                resourceId: 'b'
            },
            {
                title: 'Ferudus compound - Forfait janvier 3/2',
                start: '2018-01-05T16:30:00',
                end: '2018-01-05T18:00:00',
                color: '#1976D2',
                resourceId: 'c'
            }
        ];


    var events4sched = [

            /* the editable event */
            {
                title: 'Ferudus compound - Arrachage arbre (presta suuplémentaire)',
                start: '2018-01-04T11:30:00',
                end: '2018-01-04T12:30:00',
                color:'#388E3C',
                resourceId: 'new',
                editable:true
            },

            /*gray invalidity dates (fusion of all chexcked ressource. to calculate...) */
            {
                title: '',
                start: '2018-01-04T08:30:00',
                end: '2018-01-04T11:30:00',
                color:'#bbb',
                resourceId: 'new',
                editable:false
            },
            {
                title: '',
                start: '2018-01-04T12:30:00',
                end: '2018-01-04T14:30:00',
                color: '#bbb',
                resourceId: 'new',
                editable:false
            },
            {
                title: '',
                start: '2018-01-04T15:30:00',
                end: '2018-01-04T17:30:00',
                color: '#bbb',
                resourceId: 'new',
                editable:false
            },
            {
                title: '',
                start: '2018-01-05T08:45:00',
                end: '2018-01-05T11:15:00',
                color: '#bbb',
                resourceId: 'new',
                editable:false
            },
            {
                title: '',
                start: '2018-01-05T12:00:00',
                end: '2018-01-05T14:00:00',
                color: '#bbb',
                resourceId: 'new',
                editable:false
            },
            {
                title: '',
                start: '2018-01-05T14:30:00',
                end: '2018-01-05T18:00:00',
                color: '#bbb',
                resourceId: 'new',
                editable:false
            },


            /* real events of ressources */
            {
                title: 'Dumbo Jardinage - Gazon',
                start: '2018-01-04T08:30:00',
                end: '2018-01-04T10:30:00',
                color: '#0277bd',
                resourceId: 'a',
                editable:false
            },
            {
                title: 'Dumbo Jardinage - Gazon',
                start: '2018-01-04T08:30:00',
                end: '2018-01-04T10:30:00',
                color: '#0277bd',
                resourceId: 'd',
                editable:false
            },
            {
                title: 'Ferudus compound - Forfait janvier 1/3',
                start: '2018-01-04T09:00:00',
                end: '2018-01-04T11:00:00',
                color: '#0277bd',
                resourceId: 'c',
                editable:false
            },
            {
                title: 'Mecaprom - Prestation scarification gazon',
                start: '2018-01-04T09:30:00',
                end: '2018-01-04T11:30:00',
                color: '#0277bd',
                resourceId: 'b',
                editable:false
            },
            {
                title: 'Simply Nie - arrosage',
                start: '2018-01-04T12:30:00',
                end: '2018-01-04T14:30:00',
                color: '#0277bd',
                resourceId: 'd',
                editable:false
            },
            {
                title: 'Securitus - palmiers',
                start: '2018-01-04T15:30:00',
                end: '2018-01-04T16:30:00',
                color: '#0277bd',
                resourceId: 'b',
                editable:false
            },
            {
                title: 'Meeting',
                start: '2018-01-04T16:30:00',
                end: '2018-01-04T17:30:00',
                color: '#0277bd',
                resourceId: 'a',
                editable:false
            },
            {
                title: 'Ferudus compound - Forfait janvier 2/3',
                start: '2018-01-05T08:45:00',
                end: '2018-01-05T11:15:00',
                color: '#0277bd',
                resourceId: 'b',
                editable:false
            },
            {
                title: 'Meeting',
                start: '2018-01-05T12:00:00',
                end: '2018-01-05T14:00:00',
                color: '#0277bd',
                resourceId: 'c',
                editable:false
            },
            {
                title: 'Meeting',
                start: '2018-01-05T12:00:00',
                end: '2018-01-05T13:30:00',
                color: '#0277bd',
                resourceId: 'a',
                editable:false
            },
            {
                title: 'Meeting',
                start: '2018-01-05T14:30:00',
                end: '2018-01-045T14:30:00',
                color: '#0277bd',
                resourceId: 'b',
                editable:false
            },
            {
                title: 'Ferudus compound - Forfait janvier 3/2',
                start: '2018-01-05T16:30:00',
                end: '2018-01-05T18:00:00',
                color: '#0277bd',
                resourceId: 'c',
                editable:false
            }
        ];

    //
    //  Calendar
    //
    if ($(".fullcalendar-basic").length != 0 || $(".fullcalendar-scheduler").length != 0 || $('.fullcalendar-timeline').length !=0)
    {


        $('.fullcalendar-scheduler').fullCalendar({
                    height: 650,
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'timelineThreeDays'
                    },

                    views: {
                        timelineThreeDays: { type: 'timeline', duration: { days: 3 } }
                        
                    },


                    defaultDate: '2018-01-04',
                    defaultView: 'timelineThreeDays',
                    
                    editable: true,

                    resourceLabelText: 'Rooms',
                    resources: [ { id: 'new', title: 'NEW JOB' }, { id: 'a', title: 'Pierre Mesclun' }, { id: 'c', title: 'Simon Malcom' }, { id: 'b', title: 'Victor Rentom' }, { id: 'd', title: 'Lorie Baumbgartz' } ],

                    events: events4sched,
                    slotEventOverlap: false,
                    eventOverlap:false,




                    eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
                        
                        if (event.resourceId != "new") {

                            alert('You cannot move event on another line than "NEW JOB".');
                            revertFunc();
                        }
                    },

                    resourceRender: function(resourceObj, labelTds, bodyTds) {


                        if(resourceObj.id == 'new')
                        {
                            labelTds.css('font-weight', 'strong');
                        }
                        else
                        {


                            if(resourceObj.title == 'Pierre Mesclun' || resourceObj.title == 'Simon Malcom')
                            {
                                //labelTds.css('background', 'blue');
                                labelTds.addClass('bg-green-300');
                                labelTds.prepend($('<input type="checkbox" style="float:left; margin-top:8px; margin-left:8px" checked=checked />'));
                            }
                            else
                            {
                                //labelTds.css('background', 'blue');
                                labelTds.addClass('bg-danger-300');
                                labelTds.prepend($('<input type="checkbox" style="float:left; margin-top:8px; margin-left:8px"  />'));
                            }
                        }
                    }

                    
                });
        
        $('.fullcalendar-basic').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,agendaWeek,listWeek,agendaDay,list,timelineDay,timelineThreeDays'
                            },

                            views: {
                                month: { buttonText: 'Month' },
                                agendaWeek: { buttonText: 'Week' },
                                agendaDay: { buttonText: 'Day' },
                                list: { buttonText: 'Day list' },
                                listWeek: { buttonText: 'Week list' },
                                timelineDay: { buttonText: 'Planning' },
                                timelineThreeDays: { type: 'timeline', duration: { days: 3 } }
                                
                            },


                            defaultDate: '2018-01-04',
                            defaultView: 'agendaWeek',
                            
                            editable: true,

                            resourceLabelText: 'Rooms',
                            resources: [ { id: 'a', title: 'Pierre Mesclun' }, { id: 'b', title: 'Victor Rentom' }, { id: 'c', title: 'Simon Malcom' }, { id: 'd', title: 'Lorie Baumbgartz' } ],

                            events: events,
                            slotEventOverlap: false,
        });

    }

    // Date time pickers
    try
    {
        var $moment = moment();

        $(".datetimepicker").AnyTime_picker({
            format: "%W %d-%m-%y %H:%i",
            dayAbbreviations: $moment.lang()._weekdaysMin,
            monthAbbreviations: $moment.lang()._monthsShort,
            labelTitle: labelTitle,
            labelYear: labelYear,
            labelMonth: labelMonth,
            labelDayOfMonth: labelDayOfMonth,
            labelHour: labelHour,
            labelMinute: labelMinute
        });
        $(".datetimepicker-h").AnyTime_picker({
            format: "%Y-%m-%d %H:%i",
            dayAbbreviations: $moment.lang()._weekdaysMin,
            monthAbbreviations: $moment.lang()._monthsShort,
            labelTitle: labelTitle,
            labelYear: labelYear,
            labelMonth: labelMonth,
            labelDayOfMonth: labelDayOfMonth,
            labelHour: labelHour,
            labelMinute: labelMinute
        });
    }
    catch(e) { }




    // scheduler modal issue, trigger resize event to correct calendar size
    $('#modal_scheduler').on('show.bs.modal', function (e) {
        $(window).trigger('resize');
    })



    
});
