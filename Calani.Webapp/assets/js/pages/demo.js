/* ------------------------------------------------------------------------------
*
*  # Buttons extension for Datatables. HTML5 examples
*
*  Specific JS code additions for datatable_extension_buttons_html5.html page
*
*  Version: 1.1
*  Latest update: Mar 6, 2016
*
* ---------------------------------------------------------------------------- */

$(function() {


    // Select 2 setup
    // ------------------------------
    $('.select').select2({
        minimumResultsForSearch: Infinity
    });

    // Styled checkbox
    // ------------------------------
     $(".styled, .multiselect-container input").uniform({
        radioClass: 'choice'
    });

    // touchspin
    // --------------
    if($(".touchspin-set-value").length != 0)
    {
        $(".touchspin-set-value").TouchSpin({
                initval: 1
            });
    }

    // Handsome table
    // --------------

    // Add sample data

    var hot_date_data = [
            {item: "032 - Heure de jardinage", qty: 5, price: 100},
            {item: "066 - Scarification", qty: 1, price: 240},
            {item: "050 - Arrachage arbre", qty: 2, price: 490},
            {},
            {},
            {},
            {},
            {}
        ];

    var hot_date_data_dates = [
            {ressources:"VRE - Victor RENTOM", date: "11/01/2015", time: '11:00', duration: 1, timesheet: "Done: 0.8 - PRE", status:"Done"},
            {date: "15/01/2015", time: '15:30', duration: 2, timesheet: "Not yet", status:"Scheduled"},
            {},
            {},
            {},
            {},
            {},
            {}
        ];

    var hot_date_data_payments = [
            {date: "20/01/2015", paid:1000, comment:"cash on visit", remains: 720},
            {},
            {},
            {}
        ];

    if(typeof Handsontable !== 'undefined')
    {


        

        // Define element
        var hot_ac_lazy = document.getElementById('hot_ac_lazy');

        // Initialize with options
        var hot_ac_lazy_init = new Handsontable(hot_ac_lazy, {
            data: hot_date_data,
            stretchH: 'all',
            colHeaders: ['Product', 'Unit Price', 'Qty', 'Total', 'Taxes', ''],
            colWidths: [150, 100, 100, 100, 100, 30],
            columns: [
                {
                    data: 'item',
                    type: 'autocomplete',
                    source: ['032 - Heure de jardinage', '050 - Arrachage arbre', '022 - Tonte gazson', '066 - Scarification', '099 - Apéro'],
                    strict: false
                },
                {
                    data: 'price',
                    type: 'numeric',
                    className: 'htLeft',
                    format: '0,0.00 $'
                },
                {
                    data: 'qty',
                    type: 'numeric',
                    className: 'htLeft'
                },               
                {
                    data: 'totalprice',
                    type: 'numeric',
                    className: 'htLeft',
                    format: '0,0.00 $'
                },
                {
                    data: 'tax',
                    type: 'autocomplete',
                    source: ['VAT 8%', 'VAT 3.8%', 'VAT 2.5%', 'Alcohol', 'Out of country'],
                    strict: false
                },
                {
                    renderer:
                        function (instance, td, row, col, prop, value, cellProperties) {
                            var div;
                            // Remove existing buttons to avoid duplicating them.
                            $(td).children('.btn').remove();

                            div = document.createElement('div');
                            div.className = 'btn';
                            //div.appendChild(document.createTextNode('x'));
                            delico = $('<i class="fa fa-trash" aria-hidden="true"></i>');
                            $(div).append(delico);
                            td.appendChild(div);

                            $(div).on('mouseup', function() {
                              return instance.alter("remove_row", row);
                            });
                        return td;
                    },
                    readOnly:true
                }           
                
            ]
        });


        if($('#hot_ac_lazy_dates').length > 0)
        {


            // Define element
            var hot_ac_lazy_dates = document.getElementById('hot_ac_lazy_dates');

            // Initialize with options
            var hot_ac_lazy_init = new Handsontable(hot_ac_lazy_dates, {
                data: hot_date_data_dates,
                stretchH: 'all',
                colHeaders: ['Resource', 'Date', 'Time', 'Scheduled Duration', 'Timesheet'],
                columns: [
                    {
                        data: 'ressources',
                        type: 'autocomplete',
                        source: ['PME - Pierre MESCLUN', 'VRE - Victor RENTOM', 'SMA - Sam Matador'],
                        strict: true
                    },
                    {
                        data: 'date',
                        type: 'date',
                        dateFormat: 'MM/DD/YYYY',
                        correctFormat: true,
                        datePickerConfig: {
                          // First day of the week (0: Sunday, 1: Monday, etc)
                          firstDay: 0,
                          showWeekNumber: true,
                          numberOfMonths: 3,
                          disableDayFn: function(date) {
                            

                            if(date.getDate() === 15 || date.getDate() === 23) return true; 


                            // Disable Sunday and Saturday
                            return date.getDay() === 0 || date.getDay() === 6;
                          }
                        }
                    },
                    {
                        data: 'time',
                        type: 'time',
                        dateFormat: 'HH:mm',
                        correctFormat: true
                    },
                    {
                        data: 'duration',
                        type: 'numeric',
                        className: 'htLeft',
                        format: '0,0.00'
                    },
                    {
                        data: 'timesheet',
                        renderer:
                            function (instance, td, row, col, prop, value, cellProperties) {
                                var div;
                                // Remove existing buttons to avoid duplicating them.
                                $(td).children('.btn').remove();

                                div = document.createElement('div');
                                div.className = 'btn';
                                //div.appendChild(document.createTextNode('x'));
                                if(value != null)
                                {
                                    delico = $('<div><i class="fa fa-clock-o" aria-hidden="true"></i> </div>');
                                    $(delico).append(document.createTextNode(value));
                                    $(div).append(delico);
                                    td.appendChild(div);
                                }

                                
                            return td;
                        }
                    },
                    {
                        data: 'status',
                        type: 'autocomplete',
                        source: ['Done', 'Scheduled', 'Canceled (chargeable)', 'Canceled (not chargeable)'],
                        strict: true
                    }      
                    
                ]
            });
        }


        if($('#hot_ac_lazy_payments').length > 0)
        {


            // Define element
            var hot_ac_lazy_payments = document.getElementById('hot_ac_lazy_payments');

            // Initialize with options
            var hot_ac_lazy_init = new Handsontable(hot_ac_lazy_payments, {
                data: hot_date_data_payments,
                stretchH: 'all',
                colHeaders: ['Date', 'Paid', 'Comment', 'Remains'],
                columns: [
                    {
                        data: 'date',
                        type: 'date',
                        dateFormat: 'MM/DD/YYYY',
                        correctFormat: true
                    },
                    {
                        data: 'paid',
                        type: 'numeric',
                        className: 'htLeft',
                        format: '0,0.00'
                    },
                    {
                        data: 'comment'
                    },
                    {data: "remains", readOnly:true}       
                    
                ]
            });
        }
    }


    // Date Picker
    // -----------

    $('.daterange-basic').daterangepicker({
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default'
    });

    $('.toggle-date-input-method-alt').click(function()
    {
        $('.date-input-method-alt').toggle();
    });

    $('.daterange-single').daterangepicker({ 
        singleDatePicker: true
    });




    // Animations
    // ----------

    // datatable highlight
    $('.datatable-demo tbody').on('mouseover', 'td', function() {

        var table = this.closest('table');
        table = $(table).DataTable();

        
        var colIdx = table.cell(this).index().column;

        $(table.cells().nodes()).removeClass('active');
        $(table.column(colIdx).nodes()).addClass('active');

    }).on('mouseleave', function() {
        $(table.cells().nodes()).removeClass('active');
    });

    
    // Pie arc with legend
    // ------------------------------
    if(typeof d3 !== 'undefined')
    {
        // Initialize chart
        pieArcWithLegend("#pie_arc_legend", 170);

        // Chart setup
        function pieArcWithLegend(element, size) {


            // Basic setup
            // ------------------------------

            // Add data set
            var data = [
                {
                    "status": "Late",
                    "icon": "<i class='status-mark border-danger-300 position-left'></i>",
                    "value": 7720,
                    "color": "#EF5350"
                },
                {
                    "status": "Open",
                    "icon": "<i class='status-mark border-danger-300 position-left'></i>",
                    "value": 14921,
                    "color": "#2196F3"
                }
                ,
                {
                    "status": "Paid",
                    "icon": "<i class='status-mark border-success-300 position-left'></i>",
                    "value": 32533,
                    "color": "#66BB6A"
                }
            ];

            // Main variables
            var d3Container = d3.select(element),
                distance = 2, // reserve 2px space for mouseover arc moving
                radius = (size/2) - distance,
                sum = d3.sum(data, function(d) { return d.value; });







            // Create chart
            // ------------------------------

            // Add svg element
            var container = d3Container.append("svg");
            
            // Add SVG group
            var svg = container
                .attr("width", size)
                .attr("height", size / 2)
                .append("g")
                    .attr("transform", "translate(" + (size / 2) + "," + (size / 2) + ")");  



            // Construct chart layout
            // ------------------------------

            // Pie
            var pie = d3.layout.pie()
                .sort(null)
                .startAngle(-Math.PI / 2)
                .endAngle(Math.PI / 2)
                .value(function (d) { 
                    return d.value;
                }); 

            // Arc
            var arc = d3.svg.arc()
                .outerRadius(radius)
                .innerRadius(radius / 1.3);



            //
            // Append chart elements
            //

            // Group chart elements
            var arcGroup = svg.selectAll(".d3-arc")
                .data(pie(data))
                .enter()
                .append("g") 
                    .attr("class", "d3-arc")
                    .style({
                        'stroke': '#fff',
                        'stroke-width': 2,
                        'cursor': 'pointer'
                    });
            
            // Append path
            var arcPath = arcGroup
                .append("path")
                .style("fill", function (d) {
                    return d.data.color;
                });


            //
            // Interactions
            //

            // Mouse
            arcPath
                .on('mouseover', function(d, i) {

                    // Transition on mouseover
                    d3.select(this)
                    .transition()
                        .duration(500)
                        .ease('elastic')
                        .attr('transform', function (d) {
                            d.midAngle = ((d.endAngle - d.startAngle) / 2) + d.startAngle;
                            var x = Math.sin(d.midAngle) * distance;
                            var y = -Math.cos(d.midAngle) * distance;
                            return 'translate(' + x + ',' + y + ')';
                        });

                    $(element + ' [data-slice]').css({
                        'opacity': 0.3,
                        'transition': 'all ease-in-out 0.15s'
                    });
                    $(element + ' [data-slice=' + i + ']').css({'opacity': 1});
                })
                .on('mouseout', function(d, i) {

                    // Mouseout transition
                    d3.select(this)
                    .transition()
                        .duration(500)
                        .ease('bounce')
                        .attr('transform', 'translate(0,0)');

                    $(element + ' [data-slice]').css('opacity', 1);
                });

            // Animate chart on load
            arcPath
                .transition()
                    .delay(function(d, i) {
                        return i * 500;
                    })
                    .duration(500)
                    .attrTween("d", function(d) {
                        var interpolate = d3.interpolate(d.startAngle,d.endAngle);
                        return function(t) {
                            d.endAngle = interpolate(t);
                            return arc(d);  
                        }; 
                    });


            //
            // Append total text
            //

            svg.append('text')
                .attr('class', 'text-muted')
                .attr({
                    'class': 'half-donut-total',
                    'text-anchor': 'middle',
                    'dy': -25
                })
                .style({
                    'font-size': '12px',
                    'fill': '#999'
                })
                .text('Invoiced');


            //
            // Append count
            //

            // Text
            svg
                .append('text')
                .attr('class', 'half-conut-count')
                .attr('text-anchor', 'middle')
                .attr('dy', -8)
                .style({
                    'font-size': '15px',
                    'font-weight': 500
                });

            // Animation
            svg.select('.half-conut-count')
                .transition()
                .duration(1500)
                .ease('linear')
                .tween("text", function(d) {
                    var i = d3.interpolate(this.textContent, sum);

                    return function(t) {
                        this.textContent = d3.format(",d")(Math.round(i(t)));
                    };
                });


            //
            // Legend
            //

            // Add legend list
            var legend = d3.select(element)
                .append('ul')
                .attr('class', 'chart-widget-legend')
                .selectAll('li')
                .data(pie(data))
                .enter()
                .append('li')
                .attr('data-slice', function(d, i) {
                    return i;
                })
                .attr('style', function(d, i) {
                    return 'font-size: 15px; border-bottom: solid 2px ' + d.data.color;
                })
                .text(function(d, i) {
                    return d.data.status + ': ';
                });

            // Legend text
            legend.append('span')
                .text(function(d, i) {
                    return d.data.value;
                });
        }
    }
    

    

    // Table setup
    // ------------------------------

    // Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });




    // Column selectors
    $('.datatable-demo').DataTable({
        buttons: {            
            buttons: [
                {
                    extend: 'copyHtml5',
                    className: 'btn btn-default',
                    exportOptions: {
                        columns: [ 0, ':visible' ]
                    }
                },
                {
                    extend: 'excelHtml5',
                    className: 'btn btn-default',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    className: 'btn btn-default',
                    exportOptions: {
                        columns: [0, 1, 2, 5]
                    }
                },
                {
                    extend: 'colvis',
                    text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                    className: 'btn bg-blue btn-icon'
                }
            ]
        },
        fixedHeader: true
    });


  


    // ColReorder integration
    var table_reorder = $('.datatable-header-reorder').DataTable({
        fixedHeader: true,
        colReorder: true
    });


    // Adjust table header if sidebar toggler is clicked
    $('.sidebar-control').on('click', function() {
        table_basic.fixedHeader.adjust();
        table_footer.fixedHeader.adjust();
        table_offset.fixedHeader.adjust();
        table_reorder.fixedHeader.adjust();
    });






    // External table additions
    // ------------------------------

    // Enable Select2 select for the length option
    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });



    var events = [
            {
                title: 'Ferudus compound - Arrachage arbre (presta suuplémentaire)',
                start: '2018-01-04T10:30:00',
                end: '2018-01-04T12:30:00',
                color:'#388E3C',
                resourceId: 'a'
            },
            {
                title: 'Dumbo Jardinage - Gazon',
                start: '2018-01-04T08:30:00',
                end: '2018-01-04T10:30:00',
                color: '#388E3C',
                resourceId: 'a'
            },
            {
                title: 'Dumbo Jardinage - Gazon',
                start: '2018-01-04T08:30:00',
                end: '2018-01-04T10:30:00',
                color: '#388E3C',
                resourceId: 'd'
            },
            {
                title: 'Ferudus compound - Forfait janvier 1/3',
                start: '2018-01-04T09:00:00',
                end: '2018-01-04T11:00:00',
                color: '#388E3C',
                resourceId: 'c'
            },
            {
                title: 'Mecaprom - Prestation scarification gazon',
                start: '2018-01-04T09:30:00',
                end: '2018-01-04T11:30:00',
                color: '#388E3C',
                resourceId: 'b'
            },
            {
                title: 'Simply Nie - arrosage',
                start: '2018-01-04T12:30:00',
                end: '2018-01-04T14:30:00',
                color: '#1976D2',
                resourceId: 'd'
            },
            {
                title: 'Securitus - palmiers',
                start: '2018-01-04T15:30:00',
                end: '2018-01-04T16:30:00',
                color: '#1976D2',
                resourceId: 'b'
            },
            {
                title: 'Meeting',
                start: '2018-01-04T16:30:00',
                end: '2018-01-04T17:30:00',
                color: '#1976D2',
                resourceId: 'a'
            },
            {
                title: 'Ferudus compound - Forfait janvier 2/3',
                start: '2018-01-05T08:45:00',
                end: '2018-01-05T11:15:00',
                color: '#1976D2',
                resourceId: 'b'
            },
            {
                title: 'Meeting',
                start: '2018-01-05T12:00:00',
                end: '2018-01-05T14:00:00',
                color: '#1976D2',
                resourceId: 'c'
            },
            {
                title: 'Meeting',
                start: '2018-01-05T12:00:00',
                end: '2018-01-05T13:30:00',
                color: '#1976D2',
                resourceId: 'a'
            },
            {
                title: 'Meeting',
                start: '2018-01-05T14:30:00',
                end: '2018-01-045T14:30:00',
                color: '#D32F2F',
                resourceId: 'b'
            },
            {
                title: 'Ferudus compound - Forfait janvier 3/2',
                start: '2018-01-05T16:30:00',
                end: '2018-01-05T18:00:00',
                color: '#1976D2',
                resourceId: 'c'
            }
        ];


    var events4sched = [

            /* the editable event */
            {
                title: 'Ferudus compound - Arrachage arbre (presta suuplémentaire)',
                start: '2018-01-04T11:30:00',
                end: '2018-01-04T12:30:00',
                color:'#388E3C',
                resourceId: 'new',
                editable:true
            },

            /*gray invalidity dates (fusion of all chexcked ressource. to calculate...) */
            {
                title: '',
                start: '2018-01-04T08:30:00',
                end: '2018-01-04T11:30:00',
                color:'#bbb',
                resourceId: 'new',
                editable:false
            },
            {
                title: '',
                start: '2018-01-04T12:30:00',
                end: '2018-01-04T14:30:00',
                color: '#bbb',
                resourceId: 'new',
                editable:false
            },
            {
                title: '',
                start: '2018-01-04T15:30:00',
                end: '2018-01-04T17:30:00',
                color: '#bbb',
                resourceId: 'new',
                editable:false
            },
            {
                title: '',
                start: '2018-01-05T08:45:00',
                end: '2018-01-05T11:15:00',
                color: '#bbb',
                resourceId: 'new',
                editable:false
            },
            {
                title: '',
                start: '2018-01-05T12:00:00',
                end: '2018-01-05T14:00:00',
                color: '#bbb',
                resourceId: 'new',
                editable:false
            },
            {
                title: '',
                start: '2018-01-05T14:30:00',
                end: '2018-01-05T18:00:00',
                color: '#bbb',
                resourceId: 'new',
                editable:false
            },


            /* real events of ressources */
            {
                title: 'Dumbo Jardinage - Gazon',
                start: '2018-01-04T08:30:00',
                end: '2018-01-04T10:30:00',
                color: '#0277bd',
                resourceId: 'a',
                editable:false
            },
            {
                title: 'Dumbo Jardinage - Gazon',
                start: '2018-01-04T08:30:00',
                end: '2018-01-04T10:30:00',
                color: '#0277bd',
                resourceId: 'd',
                editable:false
            },
            {
                title: 'Ferudus compound - Forfait janvier 1/3',
                start: '2018-01-04T09:00:00',
                end: '2018-01-04T11:00:00',
                color: '#0277bd',
                resourceId: 'c',
                editable:false
            },
            {
                title: 'Mecaprom - Prestation scarification gazon',
                start: '2018-01-04T09:30:00',
                end: '2018-01-04T11:30:00',
                color: '#0277bd',
                resourceId: 'b',
                editable:false
            },
            {
                title: 'Simply Nie - arrosage',
                start: '2018-01-04T12:30:00',
                end: '2018-01-04T14:30:00',
                color: '#0277bd',
                resourceId: 'd',
                editable:false
            },
            {
                title: 'Securitus - palmiers',
                start: '2018-01-04T15:30:00',
                end: '2018-01-04T16:30:00',
                color: '#0277bd',
                resourceId: 'b',
                editable:false
            },
            {
                title: 'Meeting',
                start: '2018-01-04T16:30:00',
                end: '2018-01-04T17:30:00',
                color: '#0277bd',
                resourceId: 'a',
                editable:false
            },
            {
                title: 'Ferudus compound - Forfait janvier 2/3',
                start: '2018-01-05T08:45:00',
                end: '2018-01-05T11:15:00',
                color: '#0277bd',
                resourceId: 'b',
                editable:false
            },
            {
                title: 'Meeting',
                start: '2018-01-05T12:00:00',
                end: '2018-01-05T14:00:00',
                color: '#0277bd',
                resourceId: 'c',
                editable:false
            },
            {
                title: 'Meeting',
                start: '2018-01-05T12:00:00',
                end: '2018-01-05T13:30:00',
                color: '#0277bd',
                resourceId: 'a',
                editable:false
            },
            {
                title: 'Meeting',
                start: '2018-01-05T14:30:00',
                end: '2018-01-045T14:30:00',
                color: '#0277bd',
                resourceId: 'b',
                editable:false
            },
            {
                title: 'Ferudus compound - Forfait janvier 3/2',
                start: '2018-01-05T16:30:00',
                end: '2018-01-05T18:00:00',
                color: '#0277bd',
                resourceId: 'c',
                editable:false
            }
        ];

    //
    //  Calendar
    //
    if($(".fullcalendar-basic").length != 0 || $(".fullcalendar-scheduler").length != 0)
    {


        $('.fullcalendar-scheduler').fullCalendar({
                    height: 650,
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'timelineThreeDays'
                    },

                    views: {
                        timelineThreeDays: { type: 'timeline', duration: { days: 3 } }
                        
                    },


                    defaultDate: '2018-01-04',
                    defaultView: 'timelineThreeDays',
                    
                    editable: true,

                    resourceLabelText: 'Rooms',
                    resources: [ { id: 'new', title: 'NEW JOB' }, { id: 'a', title: 'Pierre Mesclun' }, { id: 'c', title: 'Simon Malcom' }, { id: 'b', title: 'Victor Rentom' }, { id: 'd', title: 'Lorie Baumbgartz' } ],

                    events: events4sched,
                    slotEventOverlap: false,
                    eventOverlap:false,




                    eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
                        
                        if (event.resourceId != "new") {

                            alert('You cannot move event on another line than "NEW JOB".');
                            revertFunc();
                        }
                    },

                    resourceRender: function(resourceObj, labelTds, bodyTds) {


                        if(resourceObj.id == 'new')
                        {
                            labelTds.css('font-weight', 'strong');
                        }
                        else
                        {


                            if(resourceObj.title == 'Pierre Mesclun' || resourceObj.title == 'Simon Malcom')
                            {
                                //labelTds.css('background', 'blue');
                                labelTds.addClass('bg-green-300');
                                labelTds.prepend($('<input type="checkbox" style="float:left; margin-top:8px; margin-left:8px" checked=checked />'));
                            }
                            else
                            {
                                //labelTds.css('background', 'blue');
                                labelTds.addClass('bg-danger-300');
                                labelTds.prepend($('<input type="checkbox" style="float:left; margin-top:8px; margin-left:8px"  />'));
                            }
                        }
                    }

                    
                });

            


        
        $('.fullcalendar-basic').fullCalendar({
                            header: {
                                left: 'prev,next today',
                                center: 'title',
                                right: 'month,agendaWeek,listWeek,agendaDay,list,timelineDay,timelineThreeDays'
                            },

                            views: {
                                month: { buttonText: 'Month' },
                                agendaWeek: { buttonText: 'Week' },
                                agendaDay: { buttonText: 'Day' },
                                list: { buttonText: 'Day list' },
                                listWeek: { buttonText: 'Week list' },
                                timelineDay: { buttonText: 'Planning' },
                                timelineThreeDays: { type: 'timeline', duration: { days: 3 } }
                                
                            },


                            defaultDate: '2018-01-04',
                            defaultView: 'agendaWeek',
                            
                            editable: true,

                            resourceLabelText: 'Rooms',
                            resources: [ { id: 'a', title: 'Pierre Mesclun' }, { id: 'b', title: 'Victor Rentom' }, { id: 'c', title: 'Simon Malcom' }, { id: 'd', title: 'Lorie Baumbgartz' } ],

                            events: events,
                            slotEventOverlap: false,
                });


    }


    // Date time pickers
    try
    {
        $(".datetimepicker").AnyTime_picker({
                format: "%W %d-%m-%y %H:%i"
            });
    }
    catch(e) { }




    // scheduler modal issue, trigger resize event to correct calendar size
    $('#modal_scheduler').on('show.bs.modal', function (e) {
        $(window).trigger('resize');
    })



    
});
