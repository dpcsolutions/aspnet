﻿<%@ WebHandler Language="C#" Class="OrgLogo" %>

using System;
using System.Web;

public class OrgLogo : IHttpHandler, System.Web.SessionState.IRequiresSessionState {

    public void ProcessRequest (HttpContext context)
    {
        context.Response.Clear();
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.Cache.SetExpires(DateTime.MinValue);
        context.Response.ContentType = "image/png";
        long orgId = PageHelper.GetCurrentOrganizationId(context.Session);
        Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(orgId);
        mgr.Load();
        byte[] data = mgr.Logo;
        if (data == null) data = System.IO.File.ReadAllBytes(context.Server.MapPath("~/assets/images/db/default_logo.png"));
        context.Response.OutputStream.Write(data, 0, data.Length);
    }

    public bool IsReusable {
        get
        {
            return false;
        }
    }

}