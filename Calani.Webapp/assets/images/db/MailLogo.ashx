﻿<%@ WebHandler Language="C#" Class="MailLogo" %>

using System;
using System.Web;
using System.Linq;

public class MailLogo : IHttpHandler {

    public void ProcessRequest (HttpContext context) {

        string org = context.Request.Params["o"];
        string mail = context.Request.Params["m"];

        Calani.BusinessObjects.Model.CalaniEntities db = new Calani.BusinessObjects.Model.CalaniEntities();
        long orgId = (from r in db.organizations where r.id.ToString() == org && r.email == mail select r.id).FirstOrDefault();

        context.Response.Clear();
        context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        context.Response.Cache.SetExpires(DateTime.MinValue);
        context.Response.ContentType = "image/png";

        Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(orgId);
        mgr.Load();
        byte[] data = mgr.Logo;
        if (data == null) data = System.IO.File.ReadAllBytes(context.Server.MapPath("~/assets/images/db/default_logo.png"));
        context.Response.OutputStream.Write(data, 0, data.Length);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}