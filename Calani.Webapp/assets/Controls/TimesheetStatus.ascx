﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="assets_Controls_TimesheetStatus" Codebehind="TimesheetStatus.ascx.cs" %>

<span runat="server" id="panelValidated" visible="false" class="label border-left-success label-striped text-success">
    <i class="icon-checkmark"></i> <%= Resources.Resource.Validated %> <asp:Label runat="server" ID="lblValidatedDate" />
</span>
<span runat="server" id="panelRejected" visible="false" class="label border-left-danger label-striped text-danger">
    <i class="icon-cross2"></i> <%= Resources.Resource.Rejected %> <asp:Label runat="server" ID="lblRejectedDate" />
</span>
<span runat="server" id="panelToValidate" visible="false" class="label border-left-primary label-striped text-primary">
    <i class="icon-question6"></i> <%= Resources.Resource.ToValidate %>
</span>
<span runat="server" id="panelCurrentWeek" visible="false" class="label border-left-grey label-striped text-grey">
    <i class="icon-pencil3"></i> <%= Resources.Resource.CurrentWeek %>
</span>