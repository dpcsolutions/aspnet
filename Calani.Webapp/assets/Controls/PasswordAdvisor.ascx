﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="assets_Controls_PasswordAdvisor" Codebehind="PasswordAdvisor.ascx.cs" %>
    <div class="progress">
        <div id="divpassadvisor" runat="server" class="progress-bar bg-success-400" 
            data-popup="tooltip" data-container="body" data-original-title="<%$ Resources:Resource, Password_Advice %>" style="width:0%;">
    </div>
</div>
<script type="text/javascript">
    $(function () {


        function onPassChange() {

            var password = $(this).val();
           


            var score = 1;

            if (password.length < 1) {
                score = 0;
            }

            else if (password.length < 4) {
                score = 1;
            }

            else {

                if (password.length >= 8)
                    score++;
                if (password.length >= 12)
                    score++;

                if (new RegExp('[0-9]+(\.[0-9][0-9]?)?').test(password))
                    score++;

                if (new RegExp('^(?=.*[a-z])(?=.*[A-Z]).+$').test(password))
                    score++;

                if (new RegExp('[\.,!,@,#,$,%,^,&,*,?,_,~,-,£,(,)]').test(password))
                    score++;

                if (score > 5) score = 5;
            }

            var width = 20 * score;

            if (score >= 4) {
                $('#<%= divpassadvisor.ClientID %>').addClass("bg-success-400");
                $('#<%= divpassadvisor.ClientID %>').removeClass("bg-danger-400");
            }
            else {
                $('#<%= divpassadvisor.ClientID %>').addClass("bg-danger-400");
                $('#<%= divpassadvisor.ClientID %>').removeClass("bg-success-400");
            }

            $('#<%= divpassadvisor.ClientID %>').width(width + '%');


        }

        $('#<%= PasswordInput %>').keyup(onPassChange);
        $('#<%= PasswordInput %>').change(onPassChange);


    });

</script>