﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="assets_Controls_ProjectStatusLabel" Codebehind="ProjectStatusLabel.ascx.cs" %>

<span class="label bg-success animated tada infinite" runat="server" id="lblIconCtn"></span>
<asp:Label runat="server" ID="lblStatus" />
<asp:Label runat="server" ID="lblMissingChild" CssClass="label label-default" data-popup="tooltip"  data-container="body"><i class="icon-notification2"></i></asp:Label>