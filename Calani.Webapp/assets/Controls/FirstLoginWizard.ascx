﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="assets_Controls_FirstLoginWizard" Codebehind="FirstLoginWizard.ascx.cs" %>
 <!-- Modal with h3 -->
<div id="modal_firstloginwizard" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title"><%= Resources.Resource.GoodStartTitle %></h3>
			</div>

           
               <div class="modal-body">

				<div class="steps-basic-welcome" action="#">
							<h6><%= Resources.Resource.YourCompany %></h6>
						<fieldset style="margin-top:20px; margin-bottom:40px;">
						
								<p style="margin-bottom:25px;">
                                    <%= Resources.Resource.YourCompany_Wizard %>
                                </p>
									

								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<div class="form-group">
											<label><%= Resources.Resource.YourCompanyName %> *:</label>
											<input type="text" id="tbxCompanyName" value="<%= CompanyName %>" class="required form-control" placeholder="<%= Resources.Resource.YourCompany %>">
										</div>
									</div>
                                </div>


								<div class="row">
                                    <div class="col-md-2"></div>
									<div class="col-md-5">
										<div class="form-group">
											<label><%= Resources.Resource.Street %>:</label>
											<input type="text" id="tbxCompanyStreet" value="<%= CompanyStreet %>" class="form-control" placeholder="<%= Resources.Resource.WizardSample_Street %>">
										</div>
									</div>
                                    <div class="col-md-3">
										<div class="form-group">
											<label><%= Resources.Resource.Street_nr %>:</label>
											<input type="text" id="tbxCompanyStreetNb" value="<%= CompanyStreetNb %>"  class="form-control" placeholder="1">
										</div>
									</div>
								</div>


                                <div class="row">
                                    <div class="col-md-2"></div>
									<div class="col-md-4">
										<div class="form-group">
											<label><%= Resources.Resource.ZipCode %>:</label>
											<input type="text" id="tbxCompanyZipCode" value="<%= CompanyZipCode %>" class="form-control" placeholder="<%= Resources.Resource.WizardSample_ZipCode %>">
										</div>
									</div>
                                    <div class="col-md-4">
										<div class="form-group">
											<label><%= Resources.Resource.Cityname %>:</label>
											<input type="text" id="tbxCompanyCity" value="<%= CompanyCity %>" class="form-control" placeholder="<%= Resources.Resource.WizardSample_City %>">
										</div>
									</div>
								</div>


                                <div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<div class="form-group">
											<label><%= Resources.Resource.Phone %>:</label>
											<input type="text" id="tbxCompanyPhone" value="<%= CompanyPhone %>" class="form-control" placeholder="<%= Resources.Resource.WizardSample_Phone %>">
										</div>
									</div>
                                </div>

                                <div class="row" style="text-align:center">
                                    <%= Resources.Resource.Optional %>:
                                    <asp:HyperLink runat="server" ID="lnkContinueCompanyIdentity" NavigateUrl="~/Settings/Identity/"><%= Resources.Resource.WizardSample_ContinueCompanyIdentity %> </asp:HyperLink>
                                </div>
								
							</fieldset>					

							<h6><%= Resources.Resource.Employees %></h6>
							<fieldset style="margin-top:20px; margin-bottom:40px;">
								
                                <p style="margin-bottom:25px;">
                                    <%= Resources.Resource.WizardSample_Employees %>
                                </p>

									

									

								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<div class="form-group">
											<label><%= Resources.Resource.Lastname %> *:</label>
											<input type="text" id="tbxEmployeeLastName" value="<%= EmployeeLastName %>" class="required form-control" placeholder="<%= Resources.Resource.WizardSample_LastName %>">
										</div>
									</div>
                                </div>


								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<div class="form-group">
											<label><%= Resources.Resource.Firstname %> *:</label>
											<input type="text" id="tbxEmployeeFirstName" value="<%= EmployeeFirstName %>"  class="required form-control" placeholder="<%= Resources.Resource.WizardSample_FirstName %>">
										</div>
									</div>
                                </div>


                                <div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<div class="form-group">
											<label><%= Resources.Resource.Phone %>:</label>
											<input type="text" id="tbxEmployeePhone" value="<%= EmployeePhone %>"  class="form-control" placeholder="<%= Resources.Resource.Phone %>">
										</div>
									</div>
                                </div>

                                <div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<div class="form-group">
											<label><%= Resources.Resource.Email %> *:</label>
											<input type="email" id="tbxEmployeeEmail" value="<%= EmployeeEmail %>"  class="required email form-control" placeholder="<%= Resources.Resource.WizardSample_Email %>">
										</div>
									</div>
                                </div>

                                <div class="row" style="text-align:center">
                                    <%= Resources.Resource.Optional %>:
                                    <asp:HyperLink runat="server" ID="lnkContinueEmployees" NavigateUrl="~/Ressources/Employees/"><%= Resources.Resource.WizardSample_ContinueEmployees %> </asp:HyperLink>
                                </div>
								
							</fieldset>

							<h6><%= Resources.Resource.Items %></h6>
							<fieldset style="margin-top:20px; margin-bottom:40px;">
								

                                 <p style="margin-bottom:25px;">
                                     <%= Resources.Resource.WizardSample_Items %>
                                    
                                </p>
									

									

								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<div class="form-group">
											<label><%= Resources.Resource.Items_Services_Products %> *:</label>
											<input type="text" id="tbxServiceName" value="<%= ServiceName %>" class=" required form-control" placeholder="<%= Resources.Resource.WizardSample_ItemName %>">
										</div>
									</div>
                                </div>


								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<div class="form-group">
											<label><%= Resources.Resource.RefNr %> *:</label>
											<input type="text" id="tbxServiceCode" value="<%= ServiceCode %>" class="required form-control" placeholder="<%= Resources.Resource.WizardSample_RefNr %>">
										</div>
									</div>
                                </div>


                                <div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<div class="form-group">
											<label><%= Resources.Resource.Unit_Price %> *:</label>
											<input type="number" id="tbxServicePrice" value="<%= ServicePrice %>"  class="required form-control" step="0.01" min="-999999" max="999999" placeholder="50.00">
										</div>
									</div>
                                </div>

                                <div class="row" style="text-align:center">
                                    <%= Resources.Resource.Optional %>:
                                    <asp:HyperLink runat="server" ID="lnkContinueItems" NavigateUrl="~/Ressources/Services/"><%= Resources.Resource.WizardSample_ContinueItems %> </asp:HyperLink>
                                </div>

    

								
							</fieldset>


                    <h6><%= Resources.Resource.Customers %></h6>
							<fieldset style="margin-top:20px; margin-bottom:40px;">
								

                                 <p style="margin-bottom:25px;">
                                     <%= Resources.Resource.WizardSample_Customer %>
                                    
                                </p>
									

									
								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<div class="form-group">
											<label><%= Resources.Resource.Company%> * :</label>
											<input type="text" id="tbxCustomerCompany" value="<%= CustomerCompany %>" class="required form-control" placeholder="<%= Resources.Resource.WizardSample_CustomerCompany %>">
										</div>
									</div>
                                </div>

                                
								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<div class="form-group">
											<label><%= Resources.Resource.Lastname %> * :</label>
											<input type="text" id="tbxCustomerLastName" value="<%= CustomerLastName %>" class="required form-control" placeholder="<%= Resources.Resource.WizardSample_CustomerLastName %>">
										</div>
									</div>
                                </div>


								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<div class="form-group">
											<label><%= Resources.Resource.Firstname %> * :</label>
											<input type="text" id="tbxCustomerFirstName" value="<%= CustomerFirstName %>"  class="required form-control" placeholder="<%= Resources.Resource.WizardSample_CustomerFirstName %>">
										</div>
									</div>
                                </div>




                                <div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<div class="form-group">
											<label><%= Resources.Resource.Email %> * :</label>
											<input type="email" id="tbxCustomerEmail" value="<%= CustomerEmail %>"  class="required email form-control" placeholder="<%= Resources.Resource.WizardSample_CustomerEmail %>">
										</div>
									</div>
                                </div>

                                <div class="row" style="text-align:center">
                                    <%= Resources.Resource.Optional %>:
                                    <asp:HyperLink runat="server" ID="lnkContinueCustomers" NavigateUrl="~/Ressources/Employees/"><%= Resources.Resource.WizardSample_ContinueCustomer %> </asp:HyperLink>
                                </div>

    

								
							</fieldset>


                            <h6><%= Resources.Resource.ReadyQ %></h6>
							<fieldset style="margin-top:20px; margin-bottom:40px;">
								

                                <h2 style="text-align:center"><strong><%= Resources.Resource.Great %> !</strong></h2>
                                 <p style="text-align:center; margin-bottom:25px;">
                                    <%= Resources.Resource.AllYouNeedForUsingGesMobile  %>
                                </p>
									

                                <div style="width:100%;height:0;padding-bottom:56%;position:relative;"><iframe src="https://giphy.com/embed/3o7TKF5DnsSLv4zVBu" width="100%" height="100%" style="position:absolute" frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div><p><a href="https://giphy.com/gifs/tipsyelves-yes-yeah-3o7TKF5DnsSLv4zVBu">via GIPHY</a></p>

								<p style="text-align:center">
                                    <img srg="https://giphy.com/gifs/3o7TKF5DnsSLv4zVBu/html5" />
								</p>

                                <p>
                                    <asp:LinkButton runat="server" ID="btnNewQuote" CssClass="btn btn-default" OnClick="btnNewQuote_Click"><i class="icon-upload position-left"></i> <%= Resources.Resource.Create + " " + Resources.Resource.Quote %> </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnNewInvoice" CssClass="btn btn-default" OnClick="btnNewInvoice_Click"><i class="icon-file-check2 position-left"></i><%= Resources.Resource.Create  + " " + Resources.Resource.Invoice %> </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnNewTimesheet" CssClass="btn btn-default" OnClick="btnNewTimesheet_Click"><i class="icon-file-spreadsheet position-left"></i> <%= Resources.Resource.Create  + " " + Resources.Resource.Time_Sheet %> </asp:LinkButton>
                                </p>
    

								
							</fieldset>


				</div>


			</div>

			

			
		</div>
	</div>
</div>
<script type="text/javascript">


    function saveWizard(step) {

		var data = { step: step };

		if (step == 0) {
            data.company = {};
            data.company.name = $('#tbxCompanyName').val();
            data.company.street = $('#tbxCompanyStreet').val();
            data.company.streetnb = $('#tbxCompanyStreetNb').val();
            data.company.zipcode = $('#tbxCompanyZipCode').val();
            data.company.city = $('#tbxCompanyCity').val();
            data.company.phone = $('#tbxCompanyPhone').val();
        }
		else if (step == 1) {
            data.employee = {};
            data.employee.lastname = $('#tbxEmployeeLastName').val();
            data.employee.firstname = $('#tbxEmployeeFirstName').val();
            data.employee.phone = $('#tbxEmployeePhone').val();
            data.employee.email = $('#tbxEmployeeEmail').val();
        }
        else if (step == 2) {
            data.service = {};
            data.service.name = $('#tbxServiceName').val();
            data.service.code = $('#tbxServiceCode').val();
            try { data.service.price = $('#tbxServicePrice').val() * 1; } catch (e) { }
            if (isNaN(data.service.price)) data.service.price = 0;
        }
        else if (step == 3) {
            data.customer = {};
            data.customer.lastname = $('#tbxCustomerLastName').val();
            data.customer.firstname = $('#tbxCustomerFirstName').val();
            data.customer.company = $('#tbxCustomerCompany').val();
            data.customer.email = $('#tbxCustomerEmail').val();
        }

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '../API/Service.svc/WelcomeWizardChange',
            data: JSON.stringify(data),
            processData: false,
            dataType: "json",
            success: function (response) {

                var x = response;
                

            }
        });
    }
    
    $(function () {

        $(".steps-basic-welcome").steps({
            headerTag: "h6",
            bodyTag: "fieldset",
            transitionEffect: "fade",
            enableAllSteps: false, 
            titleTemplate: '<span class="number">#index#</span> #title#',
            labels: {
                next: tutrx[0],
                previous: tutrx[1],
                finish: tutrx[2]
            },
			startIndex: <%= StepIndex %>,
			onStepChanging: function (event, currentIndex, newIndex) {

                var fields = $('#steps-uid-0-p-' + currentIndex+' input');

				fields.validate({
					errorClass: "danger",
					rules: {
                        email: {
                            required: true,
                            email: true
                        }
					},
					messages: {
                        required: "<%= Resources.Resource.FieldIsRequired %>",
                        email: {
                            required: "<%= Resources.Resource.FieldIsRequired %>",
                            email: "<%= Resources.Resource.EmailMustBeValid %>"
                        }
                    }
                });
                return fields.valid();
			},
			onStepChanged: function (event, currentIndex, priorIndex) {
                if (priorIndex < 4) {
                    saveWizard(priorIndex);
                }
            },
            onFinished: function (event, currentIndex) {
                $("#modal_firstloginwizard").modal('toggle');
            }
        });

        $("#modal_firstloginwizard").modal({ backdrop: 'static' });
    });
</script>
<style type="text/css">
	.error {
		color: red;
	}
</style>
<!-- /modal with h3 -->
