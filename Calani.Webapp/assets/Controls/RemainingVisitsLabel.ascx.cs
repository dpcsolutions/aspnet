﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_Controls_RemainingVisitsLabel : System.Web.UI.UserControl
{
    public int? RemainingVisits
    {
        get
        {
            if (String.IsNullOrWhiteSpace(lbl.Text)) return null;
            return Convert.ToInt32(lbl.Text);
        }
        set
        {
            if (value != null)
            {
                if (value.Value <= 0) lbl.Text = "";
                else
                {
                    lbl.Text = value.Value.ToString();
                    if(value.Value < 3)
                    {
                        lbl.CssClass = "text-danger text-bold";
                    }
                    else
                    {
                        lbl.CssClass = "";
                    }
                }
                
            }
            else
            {
                lbl.Text = "";
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}