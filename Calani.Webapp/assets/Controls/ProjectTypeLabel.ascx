﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="assets_Controls_ProjectTypeLabel" Codebehind="ProjectTypeLabel.ascx.cs" %>

<span class="label bg-success animated tada infinite" runat="server" id="lblIconCtn"></span>
<asp:Label runat="server" ID="lblStatus" />
<asp:Label runat="server" ID="lblMissingChild" CssClass="label label-default"><%=Resources.Resource.ToPlan %></asp:Label>