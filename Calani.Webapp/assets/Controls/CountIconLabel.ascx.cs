﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_Controls_CountIconLabel : System.Web.UI.UserControl
{
    public string Icon { get; set; }
    public long? Count { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Count != null)
        {
            lbl.Text = Count.Value.ToString();
            ico.Attributes["class"] = Icon;
        }
        else
        {
            lbl.Visible = false;
            ico.Visible = false;
        }
    }
}