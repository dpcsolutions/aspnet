﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_Controls_ProjectNextVisitLabel : System.Web.UI.UserControl
{
    DateTime? nextVisitDate;

    public DateTime? NextVisitDate
    {
        get
        {
            return nextVisitDate;
        }

        set
        {
            nextVisitDate = value;
        }
    }

    public Dictionary<string, string> NextVisitContacts
    {
        get
        {
            return nextVisitContacts;
        }

        set
        {
            nextVisitContacts = value;
        }
    }

    public bool NextVisitMissingRessource
    {
        get
        {
            return nextVisitMissingRessource;
        }

        set
        {
            nextVisitMissingRessource = value;
        }
    }

    Dictionary<string, string> nextVisitContacts;



    bool nextVisitMissingRessource;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (nextVisitDate != null)
        {
            lblDateSystem.Text = nextVisitDate.Value.ToString("yyyy-MM-dd HH:mm");
            lblDate.Text = nextVisitDate.Value.ToString("ddd dd.MM.yy HH:mm");
        }
        else
        {
            lblDate.Text = "";
        }

        if(nextVisitContacts != null)
        {
            contacts.InnerHtml = "";
            foreach (var c in nextVisitContacts)
            {
                contacts.InnerHtml += "<span class=\"label label-flat border-grey text-grey-600\" data-popup=\"tooltip\"  data-placement=\"left\" title=\""+c.Value+ "\">" + c.Key + "</span>";
            }
        }

        if (nextVisitMissingRessource)
        {
            contacts.InnerHtml += "<span class=\"label bg-danger animated tada infinite\" data-popup=\"tooltip\" title=\"" + Resources.Resource.Resource_to_schedule + "\" data-placement=\"left\"><i class=\"icon-user-cancel small\"></i></span> ";
        }
    }

    /*
    <span class="label label-flat border-grey text-grey-600" data-popup="tooltip"  data-placement="left" title="Citrrew SDFSD">VRE</span>
    */
}
 