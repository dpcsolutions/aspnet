﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="assets_Controls_ImportedInLink" Codebehind="ImportedInLink.ascx.cs" %>
<asp:Label runat="server" CssClass="label label-primary">
    <asp:HyperLink runat="server" CssClass="text-white" target="_blank" ID="link">
        <i class="icon-chevron-right"></i> 
        <asp:Label runat="server" ID="labelName"></asp:Label>
    </asp:HyperLink>
</asp:Label>