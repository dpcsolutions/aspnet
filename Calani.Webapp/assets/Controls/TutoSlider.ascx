﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="assets_Controls_TutoSlider" Codebehind="TutoSlider.ascx.cs" %>
<div class="row" id="panelTuto" style="<%= DaStyle %>">
    <div class="col-sm-12 col-md-12">

            <!-- TutoFile: <%= TutoFile %> -->
            <div class="panel bg-slate bg-slate-800">
						<div class="panel-heading">
							<h6 class="panel-title"><%= Resources.Resource.Tutorial %></h6>
							<div class="heading-elements">
								<ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
					                <li><a data-action="hide" href="javascript:toggleTuto(true)"></a></li>
					            </ul>
				            </div>
						</div>

						<div class="panel-body">
							<div class="steps-basic" action="#" id="tutoContent" runat="server">
							    

						    </div>
						</div>
					</div>
                    
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#btnToggleTuto').show();
    });
    toggleTuto_varname = "<%= VarName %>";
    toggleTuto_url = "<%= ToggleTutoUrl %>";
</script>