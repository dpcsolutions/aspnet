﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_Controls_ContactTypeLabel : System.Web.UI.UserControl
{

    public int? Type { get; set; } 
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Type != null && Type == Convert.ToInt32(Calani.BusinessObjects.Contacts.ContactTypeEnum.Contact_CustomerMain))
        {
            s.InnerHtml = Resources.Resource.Main;
        }
        else
        {
            s.InnerHtml = "";
        }
    }
}