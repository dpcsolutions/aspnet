﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_Controls_FirstLoginWizard : System.Web.UI.UserControl
{
    public int StepIndex { get; set; }
    public string CompanyName { get; set; }
    public string CompanyStreet{ get; set; }
    public string CompanyStreetNb { get; set; }
    public string CompanyCity { get; set; }
    public string CompanyZipCode { get; set; }
    public string CompanyPhone { get; set; }

    public string EmployeeLastName { get; set; }
    public string EmployeeFirstName { get; set; }
    public string EmployeePhone { get; set; }
    public string EmployeeEmail { get; set; }

    public string ServiceName { get; set; }
    public string ServiceCode { get; set; }
    public double ServicePrice { get; set; }

    public string CustomerCompany { get; set; }
    public string CustomerLastName { get; set; }
    public string CustomerFirstName { get; set; }
    public string CustomerEmail { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        long orgid = PageHelper.GetCurrentOrganizationId(Session);


        StepIndex = 3;


        Calani.BusinessObjects.CustomerAdmin.OrganizationManager orgmgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(orgid);
        orgmgr.Load();


        CompanyName = ProtectValue(orgmgr.CompanyName);
        CompanyStreet = ProtectValue(orgmgr.AdrStreet);
        CompanyStreetNb = ProtectValue(orgmgr.AdrStreetNb);
        CompanyCity = ProtectValue(orgmgr.AdrCity);
        CompanyZipCode = ProtectValue(orgmgr.AdrZipCode);
        CompanyPhone = ProtectValue(orgmgr.Phone);

        Calani.BusinessObjects.Contacts.EmployeesManager empmgr = new Calani.BusinessObjects.Contacts.EmployeesManager(orgid);
        var emps = empmgr.List();
        Calani.BusinessObjects.Model.contacts firstemp = (from r in emps where r.type == 2 orderby r.id select r).FirstOrDefault();
        if(firstemp != null)
        {
            EmployeeLastName = ProtectValue(firstemp.lastName);
            EmployeeFirstName = ProtectValue(firstemp.firstName);
            EmployeePhone = ProtectValue(firstemp.primaryPhone);
            EmployeeEmail = ProtectValue(firstemp.primaryEmail);
        }

        Calani.BusinessObjects.CustomerAdmin.ServicesManager srvmgr = new Calani.BusinessObjects.CustomerAdmin.ServicesManager(orgid);
        var services = srvmgr.List();
        var firstservice = (from r in services orderby r.id select r).FirstOrDefault();
        if (firstservice != null)
        {
            ServiceName = ProtectValue(firstservice.name);
            ServiceCode = ProtectValue(firstservice.internalId);
            ServicePrice = firstservice.unitPrice;
        }


        Calani.BusinessObjects.CustomerAdmin.ClientsManager custmgr = new Calani.BusinessObjects.CustomerAdmin.ClientsManager(orgid);
        var customers = custmgr.List();
        var firstcust = (from r in customers orderby r.id select r).FirstOrDefault();
        if(firstcust != null)
        {
            CustomerCompany = firstcust.companyName;
            Calani.BusinessObjects.Contacts.ClientContactsManager contmgr = new Calani.BusinessObjects.Contacts.ClientContactsManager(orgid, firstcust.id);
            var contact = contmgr.List().FirstOrDefault();
            if (contact != null)
            {
                CustomerFirstName = contact.firstName;
                CustomerLastName = contact.lastName;
                CustomerEmail = contact.primaryEmail;
            }
        }

        Visible = false;

      
       

      

        // missing company info
        if (String.IsNullOrWhiteSpace(CompanyCity))
        {
            StepIndex = 0;
            Visible = true;
            return;
        }

        // missing employee
        if (String.IsNullOrWhiteSpace(EmployeeLastName))
        {
            StepIndex = 1;
            if (String.IsNullOrWhiteSpace(CompanyCity))
            {
                Visible = true;
                return;
            }
        }

        // missing item
        if (String.IsNullOrWhiteSpace(ServiceName))
        {
            StepIndex = 2;
            Visible = true;
            return;
        }

        // missing customer
        if (String.IsNullOrWhiteSpace(CustomerCompany))
        {
            StepIndex = 3;
            Visible = true;
            return;
        }



    }

    private string ProtectValue(string x)
    {
        if (x == null) x = "";
        x = x.Replace("\"", "&quot;");
        return x;
    }

    protected void btnNewQuote_Click(object sender, EventArgs e)
    {
        long orgid = PageHelper.GetCurrentOrganizationId(Session);

        Calani.BusinessObjects.Projects.ProjectsManager mgr = new Calani.BusinessObjects.Projects.ProjectsManager(orgid);
        var quotes = mgr.ListQuotes();

        var quote = quotes.FirstOrDefault();
        if(quote == null)
        {
            Response.Redirect("~/Projects/Quotes/Job.aspx");
        }
        else
        {
            Response.Redirect("~/Projects/Quotes/Job.aspx?id=" + quote.id);
        }
    }

    protected void btnNewInvoice_Click(object sender, EventArgs e)
    {
        long orgid = PageHelper.GetCurrentOrganizationId(Session);

        Calani.BusinessObjects.Projects.ProjectsManager mgr = new Calani.BusinessObjects.Projects.ProjectsManager(orgid);
        var invoices = mgr.ListInvoices(orgid);

        var invoice = invoices.FirstOrDefault();
        if (invoice == null)
        {
            Response.Redirect("~/Projects/Invoices/Job.aspx");
        }
        else
        {
            Response.Redirect("~/Projects/Invoices/Job.aspx?id=" + invoice.id);
        }
    }

    protected void btnNewTimesheet_Click(object sender, EventArgs e)
    {
        long orgid = PageHelper.GetCurrentOrganizationId(Session);

        int week = Calani.BusinessObjects.Generic.CalendarTools.GetNonIsoWeekOfYear(DateTime.Now);

        Calani.BusinessObjects.Contacts.EmployeesManager empmgr = new Calani.BusinessObjects.Contacts.EmployeesManager(orgid);
        var emps = empmgr.List();
        Calani.BusinessObjects.Model.contacts firstemp = (from r in emps where r.type == 2 orderby r.id select r).FirstOrDefault();

        Calani.BusinessObjects.TimeManagement.TimeSheetsManager mgr = new Calani.BusinessObjects.TimeManagement.TimeSheetsManager(PageHelper.GetCurrentOrganizationId(Session));

        var sheet = mgr.GetTimeSheet(firstemp.id, DateTime.Now.Year, week);

        if(sheet == null)
        {
            Response.Redirect("~/Time/Sheets/Sheet.aspx");
        }
        else
        {
            Response.Redirect( "~/Time/Sheets/Sheet.aspx?id=" + sheet.id);
        }

    }
}