﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="assets_Controls_ExpenseApprovedAmountLabel" Codebehind="ExpenseApprovedAmountLabel.ascx.cs" %>
<asp:Panel runat="server" ID="iapppart" style="display:inline">
    <span class="label label-primary">
        <i class="icon-check display-inline-block text-white"></i>
        <%= Resources.Resource.Partial %>
    </span>
</asp:Panel>
<asp:Panel runat="server" ID="iappfull" style="display:inline">
    <span class="label label-success">
        <i class="icon-check display-inline-block text-white"></i>
        <i class="icon-check display-inline-block text-white"></i>
        <%= Resources.Resource.Total %>
        
    </span>
</asp:Panel>
<asp:Label runat="server" ID="l" />
<asp:Panel runat="server" ID="w">
    <i class="icon-question6 display-inline-block text-danger"></i>
</asp:Panel>
