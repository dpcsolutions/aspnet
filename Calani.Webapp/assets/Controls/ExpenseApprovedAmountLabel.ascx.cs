﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_Controls_ExpenseApprovedAmountLabel : System.Web.UI.UserControl
{
    public double? Amount { get; set; }
    public double Price { get; set; }
    public bool Approved { get; set; }
    public bool Rejected { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Approved && Amount > 0)
        {
            l.Text = String.Format("{0:F2}", Math.Round(Amount.Value, 2, MidpointRounding.AwayFromZero));
            l.Visible = true;
            w.Visible = false;
            iappfull.Visible = false;
            iapppart.Visible = false;
            if (Price != 0)
            {
                if (Price == Amount.Value) iappfull.Visible = true;
                else iapppart.Visible = true;
            }
        }
        else if(Rejected)
        {
            l.Text = String.Format("{0:F2}", Math.Round(Amount.Value, 2, MidpointRounding.AwayFromZero));
            l.Visible = true;
            w.Visible = false;
            iappfull.Visible = false;
            iapppart.Visible = false;
        }
        else
        {
            l.Visible = false;
            w.Visible = true;
            iappfull.Visible = false;
            iapppart.Visible = false;
        }
    }
}