﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="assets_Controls_ProjectVisitsLabel" Codebehind="ProjectVisitsLabel.ascx.cs" %>
<div class="small"><asp:Label runat="server" ID="lblStatus" /></div>
<div class="progress" runat="server" id="pgbar" Visible="false">
	<div class="progress-bar bg-success-400" runat="server" id="pgDone" data-popup="tooltip"  data-container="body">
	</div>

    <div class="progress-bar progress-bar-info progress-bar-striped active" runat="server" id="pgPlanned" data-popup="tooltip"  data-container="body">
	</div>

	<div class="progress-bar progress-bar-danger" runat="server" id="pgPMissing" data-popup="tooltip"  data-container="body">
	</div>
</div>