﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_Controls_RatingViewer : System.Web.UI.UserControl
{
    int value = -1;

    public string Value
    {
        get
        {
            return value.ToString();
        }

        set
        {
            if (!String.IsNullOrWhiteSpace(value))
            {
                this.value = Convert.ToInt32(value);

                ir1.Visible = ir2.Visible = ir3.Visible = ir4.Visible = ir5.Visible = true;
                ir1.Attributes["class"] = ir2.Attributes["class"] = ir3.Attributes["class"] = ir4.Attributes["class"] = ir5.Attributes["class"] = "fa fa-star-o";
                for (int i = 1; i <= this.value; i++)
                {
                    (FindControl("ir"+i) as System.Web.UI.HtmlControls.HtmlGenericControl).Attributes["class"] = "fa fa-star";
                }
            }
            else
            {
                this.value = -1;
                ir1.Visible = ir2.Visible = ir3.Visible = ir4.Visible = ir5.Visible = false;
            }
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {

    }
}