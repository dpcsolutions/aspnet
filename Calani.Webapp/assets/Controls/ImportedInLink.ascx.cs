﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_Controls_ImportedInLink : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    long? _importedInId;
    public long? ImportedInId
    {
        get
        {
            return _importedInId;
        }
        set
        {
            _importedInId = value;
            this.Visible = (_importedInId != null);
            if(_importedInId != null)
            {
                link.NavigateUrl = "~/Projects/Invoices/Job.aspx?id=" + _importedInId;
            }
        }
    }

    string _importedInName;
    public string ImportedInName
    {
        get
        {
            return _importedInName;
        }
        set
        {
            _importedInName = value;
            labelName.Text = value;
        }
    }
}