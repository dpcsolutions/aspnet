﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_Controls_RatingStars : System.Web.UI.UserControl
{
    public int? Value
    {
        get
        {
            int? ret = null;
            if (o5.Selected) ret = 5;
            if (o4.Selected) ret = 4;
            if (o3.Selected) ret = 3;
            if (o2.Selected) ret = 2;
            if (o1.Selected) ret = 1;
            return ret;
        }
        set
        {
            bool notSet = false;
            if (value == null)
            {
                notSet = true;
            }
            else
            {
                if (value >= 5)
                {
                    SetSelected(o1, false);
                    SetSelected(o2, false);
                    SetSelected(o3, false);
                    SetSelected(o4, false);
                    SetSelected(o5, true);
                }
                else if (value == 4)
                {
                    SetSelected(o1, false);
                    SetSelected(o2, false);
                    SetSelected(o3, false);
                    SetSelected(o4, true);
                    SetSelected(o5, false);
                }
                else if (value == 3)
                {
                    SetSelected(o1, false);
                    SetSelected(o2, false);
                    SetSelected(o3, true);
                    SetSelected(o4, false);
                    SetSelected(o5, false);
                }
                else if (value == 2)
                {
                    SetSelected(o1, false);
                    SetSelected(o2, true);
                    SetSelected(o3, false);
                    SetSelected(o4, false);
                    SetSelected(o5, false);
                }
                else if (value == 1)
                {
                    SetSelected(o1, true);
                    SetSelected(o2, false);
                    SetSelected(o3, false);
                    SetSelected(o4, false);
                    SetSelected(o5, false);
                }
                else
                {
                    notSet = true;
                }
            }

            if (notSet)
            {
                SetSelected(o1, false);
                SetSelected(o2, false);
                SetSelected(o3, false);
                SetSelected(o4, false);
                SetSelected(o5, false);
            }
        }
    }

    public bool Disabled
    {
        set
        {
            s.Enabled = !value;
            
        }
    }

    private void SetSelected(ListItem item, bool s)
    {
        item.Selected = s;
    }


    protected void Page_Load(object sender, EventArgs e)
    {

    }
}