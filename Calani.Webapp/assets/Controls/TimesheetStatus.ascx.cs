﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_Controls_TimesheetStatus : System.Web.UI.UserControl
{

    public DateTime? AcceptedDate { get; set; }
    public DateTime? RejectDate { get; set; }
    public string Status { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if(AcceptedDate == null && RejectDate == null)
        {
            panelToValidate.Visible = true;
            panelRejected.Visible = false;
            panelValidated.Visible = false;
            panelCurrentWeek.Visible = false;

            if (Status == "currentweek")
            {
                panelToValidate.Visible = false;
                panelCurrentWeek.Visible = true;
            }
        }
        else
        {
            if(RejectDate != null)
            {
                panelToValidate.Visible = false;
                panelRejected.Visible = true;
                panelValidated.Visible = false;
                panelCurrentWeek.Visible = false;
                lblRejectedDate.Text = RejectDate.Value.ToString("dd/MM/yyyy HH:mm");
            }
            else if(AcceptedDate != null)
            {
                panelToValidate.Visible = false;
                panelRejected.Visible = false;
                panelValidated.Visible = true;
                panelCurrentWeek.Visible = false;
                lblValidatedDate.Text = AcceptedDate.Value.ToString("dd/MM/yyyy HH:mm");
            }
        }
    }
}