﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="assets_Controls_RatingViewer" Codebehind="RatingViewer.ascx.cs" %>
<span style="display:none;">R<%= Value %></span>
<span class="stars">
    <i class="fa" runat="server" id="ir1"></i>
    <i class="fa" runat="server" id="ir2"></i>
    <i class="fa" runat="server" id="ir3"></i>
    <i class="fa" runat="server" id="ir4"></i>
    <i class="fa" runat="server" id="ir5"></i>
</span>