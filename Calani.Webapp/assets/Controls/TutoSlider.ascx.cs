﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_Controls_TutoSlider : System.Web.UI.UserControl
{

    public string DaStyle { get; set; }
    public string VarName { get; set; }
    public string ToggleTutoUrl { get; set; }
    public string TutoFile { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        InitCoreSlider();
        LoadSliderData();
    }


    private void InitCoreSlider()
    {
        string existingUrl = this.Page.Request.Url.AbsolutePath;
        int existingUrlStart = existingUrl.IndexOf("/", 2);
        if (existingUrlStart > 0) existingUrl = existingUrl.Substring(existingUrlStart);
        if (existingUrl.ToLower().EndsWith("default.aspx")) existingUrl = existingUrl.Substring(0, existingUrl.Length - 12);

        long orgid = PageHelper.GetCurrentOrganizationId(this.Session);

        Calani.BusinessObjects.Membership.UserVariableManager vars = new Calani.BusinessObjects.Membership.UserVariableManager(HttpContext.Current.User.Identity.Name, orgid);

        TutoFile = existingUrl.Replace(".aspx", "").Replace("/", "_").Trim('_');
        VarName = "tuto:" + existingUrl;

        int levels = existingUrl.Split('/').Length;
        ToggleTutoUrl = "";
        for (int i = 0; i < levels - 2; i++)
        {
            ToggleTutoUrl += "../";
        }
        ToggleTutoUrl += "API/Service.svc/SetUserVal";

        string show = vars.Load(VarName, "1");
        if (PageHelper.GetUserStaff(Session)) show = "0";
        if (show == "1") DaStyle = "display:block;";
        else DaStyle = "display:none;";
    }


    private void LoadSliderData()
    {
        string file = Server.MapPath("~/assets/tuto");
        file = System.IO.Path.Combine(file, TutoFile);
        file += ".csv";

        string currentLang = "en";
        try
        {
            currentLang = System.Threading.Thread.CurrentThread.CurrentCulture.ToString().ToLower().Substring(0, 2);
        }
        catch { }
        

        if (System.IO.File.Exists(file))
        {
            Calani.BusinessObjects.Generic.CSVReader csv = new Calani.BusinessObjects.Generic.CSVReader();
            csv.FileEncoding = System.Text.Encoding.GetEncoding(1252);
            csv.LoadFile(file);

            string sqlWhere = "culture = '" + currentLang+"'";
            string sqlOrder = "slide ASC";

            System.Data.DataRow[] dt = csv.DataSet.Tables[0].Select(sqlWhere, sqlOrder);

            if(dt.Length == 0) dt = csv.DataSet.Tables[0].Select("culture = 'fr'", sqlOrder);

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            foreach (System.Data.DataRow r in dt)
            {
                string slide = r["slide"].ToString();
                string title = r["title"].ToString();
                string image = r["image"].ToString();
                string text = r["text"].ToString();


               
                sb.AppendLine("<h6>" + title + "</h6>");
                sb.AppendLine("<fieldset>");
                sb.AppendLine("<div class=\"row\">");

                sb.AppendLine("<div class=\"col-sm-12 col-md-6 col-description\">");
                sb.AppendLine("<div class=\"tuto-content tuto-content-text\">");
                sb.AppendLine("<p>");
                sb.AppendLine(text);
                sb.AppendLine("</p>");
                sb.AppendLine("</div>");
                sb.AppendLine("</div>");

                sb.AppendLine("<div class=\"col-sm-12 col-md-6\">");
                sb.AppendLine("<div class=\"tuto-content tuto-content-image\">");
                sb.AppendLine("<img src=\""+ Page.ResolveClientUrl("~/assets/tuto/" + image)+ "\"/>");
                sb.AppendLine("</div>");
                sb.AppendLine("</div>");

                
                sb.AppendLine("</div>");
                sb.AppendLine("</fieldset>");


            }


            tutoContent.InnerHtml = sb.ToString();

        }
        else
        {
            this.Visible = false;
        }
    }
}
