﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_Controls_ProjectVisitsLabel : System.Web.UI.UserControl
{
    int doneVisits;
    public int DoneVisits
    {
        get
        {
            return doneVisits;
        }

        set
        {
            doneVisits = value;
        }
    }

    public int? ContractualUnitsCount
    {
        get
        {
            return contractualUnitsCount;
        }

        set
        {
            contractualUnitsCount = value;
        }
    }

    int? contractualUnitsCount;


    public int? PlannedVisits
    {
        get
        {
            return plannedVisits;
        }

        set
        {
            plannedVisits = value;
        }
    }

    int? plannedVisits;

    protected void Page_Load(object sender, EventArgs e)
    {
        if(contractualUnitsCount != null)
        {
            lblStatus.Text = doneVisits + " of " + contractualUnitsCount;
            pgbar.Visible = true;

            int max = Math.Max(DoneVisits, contractualUnitsCount.Value);
            int x = 100 / max;

            int rest = 100;

            pgDone.Style.Add("width", ToWidth(x, doneVisits));
            pgDone.Attributes["title"] = doneVisits + " " + Resources.Resource.Done_Visits;

            rest -= x * doneVisits;
            if (plannedVisits != null && plannedVisits.Value > 0)
            {
                pgPlanned.Style.Add("width", ToWidth(x, plannedVisits.Value));
                pgPlanned.Attributes["title"] = plannedVisits.Value + " " + Resources.Resource.Planned_Visits;
                rest -= x * plannedVisits.Value;
            }
            else
            {
                pgPlanned.Visible = false;
            }

            if(rest > 0)
            {
                int missing = contractualUnitsCount.Value;
                if (doneVisits > 0) missing -= doneVisits;
                if (plannedVisits != null && plannedVisits.Value > 0) missing -= plannedVisits.Value;
                if (missing > 0)
                {
                    pgPMissing.Style.Add("width", ToWidth(1, rest));
                    pgPMissing.Attributes["title"] = missing + " " + Resources.Resource.Missing_Visits;
                }
            }
            else
            {
                pgPMissing.Visible = false;
            }

        }
        else
        {
            if (doneVisits > 0)
                lblStatus.Text = doneVisits.ToString();
            
        }
    }

    private string ToWidth(int x, int val)
    {
        int w = x * val;
        return w + "%";
    }
}