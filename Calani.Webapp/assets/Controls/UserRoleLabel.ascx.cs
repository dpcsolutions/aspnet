﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_Controls_UserRoleLabel : System.Web.UI.UserControl
{
    public int? UserRole { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if(UserRole != null)
        {
            if(UserRole.Value == Convert.ToInt32(Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin))
            {
                l.Text = Resources.Resource.CompanyAdministrator;
                l.Font.Bold = true;
            }
            if (UserRole.Value == Convert.ToInt32(Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee))
            {
                l.Text = Resources.Resource.Employee;
                l.Font.Bold = false;
            }
            if (UserRole.Value == Convert.ToInt32(Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Disabled))
            {
                l.Text = Resources.Resource.DisabledAccount;
                l.Font.Strikeout = true;
                l.ForeColor = System.Drawing.Color.Red;
            }
        }
    }
}