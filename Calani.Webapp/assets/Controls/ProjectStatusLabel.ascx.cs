﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.Projects;

public partial class assets_Controls_ProjectStatusLabel : System.Web.UI.UserControl
{

    Calani.BusinessObjects.Projects.ProjectStatus _status;


    public string ViewMode { get; set; }

    public ProjectStatus ProjectStatus
    {
        get
        {
            return _status;
        }

        set
        {
            _status = value;
            Page_Load(this, new EventArgs());
        }
    }

    public bool MissingChild { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {

        lblMissingChild.Visible = MissingChild;
        lblMissingChild.ToolTip = Resources.Resource.AvailableForImportOfInvoice;

        switch (ProjectStatus)
        {

            case ProjectStatus.DeliveryNoteDraft:
                lblStatus.Text = "<em>" + Resources.Resource.Draft + "</em>";
                lblIconCtn.Attributes["class"] = "label bg-grey";
                lblIconCtn.InnerHtml = "<i class=\"icon-floppy-disk\"></i>";
                break;
            case ProjectStatus.DeliveryNoteSent:
                lblStatus.Text = Resources.Resource.Sent;
                lblIconCtn.Attributes["class"] = "label bg-primary";
                lblIconCtn.InnerHtml = "<i class=\"icon-envelop\"></i>";
                break;
            case ProjectStatus.DeliveryNoteAccepted:
                lblStatus.Text = "<strong>" + Resources.Resource.Accepted + "</strong>";
                lblIconCtn.Attributes["class"] = "label bg-success animated tada infinite";
                lblIconCtn.InnerHtml = "<i class=\"icon-checkmark\"></i>";
                break;
            case ProjectStatus.DeliveryNoteRefused:
                lblStatus.Text = "<strong>" + Resources.Resource.Declined + "</strong>";
                lblIconCtn.Attributes["class"] = "label bg-danger";
                lblIconCtn.InnerHtml = "<i class=\"icon-trash\"></i>";
                break;

            case ProjectStatus.QuoteDraft:
                lblStatus.Text = "<em>" + Resources.Resource.Draft + "</em>";
                lblIconCtn.Attributes["class"] = "label bg-grey";
                lblIconCtn.InnerHtml = "<i class=\"icon-floppy-disk\"></i>";
                break;
            case ProjectStatus.QuoteSent:
                lblStatus.Text = Resources.Resource.Sent;
                lblIconCtn.Attributes["class"] = "label bg-primary";
                lblIconCtn.InnerHtml = "<i class=\"icon-envelop\"></i>";
                break;
            case ProjectStatus.QuoteAccepted:
                lblStatus.Text = "<strong>" + Resources.Resource.Accepted + "</strong>";
                lblIconCtn.Attributes["class"] = "label bg-success animated tada infinite";
                lblIconCtn.InnerHtml = "<i class=\"icon-checkmark\"></i>";
                break;
            case ProjectStatus.QuoteRefused:
                lblStatus.Text = "<strong>"+ Resources.Resource.Declined + "</strong>";
                lblIconCtn.Attributes["class"] = "label bg-danger";
                lblIconCtn.InnerHtml = "<i class=\"icon-trash\"></i>";
                break;
            case ProjectStatus.QuoteOutDated:
                lblStatus.Text = "<strong>" + Resources.Resource.OutdatedQuote + "</strong>";
                lblIconCtn.Attributes["class"] = "label bg-grey animated tada infinite";
                lblIconCtn.InnerHtml = "<i class=\"icon-calendar52\"></i>";
                break;

            case ProjectStatus.JobOpened:
                lblStatus.Text = Resources.Resource.Opened;
                lblIconCtn.Attributes["class"] = "label bg-primary";
                lblIconCtn.InnerHtml = "<i class=\"icon-play4\"></i>";
                break;
            case ProjectStatus.JobFinished:
                lblStatus.Text = Resources.Resource.Finished;
                lblIconCtn.Attributes["class"] = "label bg-success animated tada infinite";
                lblIconCtn.InnerHtml = "<i class=\"icon-finish\"></i>";
                break;
            case ProjectStatus.JobOnHold:
                lblStatus.Text = "<strong>" + Resources.Resource.On_hold + "</strong>";
                lblIconCtn.Attributes["class"] = "label bg-orange";
                lblIconCtn.InnerHtml = "<i class=\"icon-pause2\"></i>";
                break;
            case ProjectStatus.JobOutDated:
                lblStatus.Text = "<strong>" + Resources.Resource.OutdatedJob + "</strong>";
                lblIconCtn.Attributes["class"] = "label bg-grey animated tada infinite";
                lblIconCtn.InnerHtml = "<i class=\"icon-calendar2\"></i>";
                break;


            case ProjectStatus.InvoiceDraft:
                lblStatus.Text = "<em>" + Resources.Resource.Draft + "</em>";
                lblIconCtn.Attributes["class"] = "label bg-grey";
                lblIconCtn.InnerHtml = "<i class=\"icon-floppy-disk\"></i>";
                break;
            case ProjectStatus.InvoicePaymentDue:
                lblStatus.Text = "<strong>" + Resources.Resource.Pending + "</strong>";
                lblIconCtn.Attributes["class"] = "label bg-primary";
                lblIconCtn.InnerHtml = "<i class=\"icon-bell-check\"></i>";
                break;
            case ProjectStatus.InvoicePaid:
                lblStatus.Text = "<strong>" + Resources.Resource.Paid + "</strong>";
                lblIconCtn.Attributes["class"] = "label bg-success";
                lblIconCtn.InnerHtml = "<i class=\"icon-cash2\"></i>";
                break;
            case ProjectStatus.InvoicePaymentLate:
                lblStatus.Text = "<strong>" + Resources.Resource.Late + "</strong>";
                lblIconCtn.Attributes["class"] = "label bg-danger animated tada infinite";
                lblIconCtn.InnerHtml = "<i class=\"icon-bell2\"></i>";
                break;

            default:
                break;
        }


    }


}