﻿using Calani.BusinessObjects.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_Controls_ProjectTypeLabel : System.Web.UI.UserControl
{
    Calani.BusinessObjects.Projects.ProjectStatus _status;


    public string ViewMode { get; set; }

    public ProjectStatus ProjectStatus
    {
        get
        {
            return _status;
        }

        set
        {
            _status = value;
            Page_Load(this, new EventArgs());
        }
    }

    public bool MissingChild { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {

        lblMissingChild.Visible = MissingChild;

        switch (ProjectStatus)
        {
            case ProjectStatus.QuoteDraft:
                lblStatus.Text = "<em>" + Resources.Resource.Quote + "</em>";
                lblIconCtn.Attributes["class"] = "label bg-green";
                lblIconCtn.InnerHtml = "<i class=\"icon-upload\"></i>";
                break;
            case ProjectStatus.QuoteSent:
                lblStatus.Text = Resources.Resource.Quote;
                lblIconCtn.Attributes["class"] = "label bg-green";
                lblIconCtn.InnerHtml = "<i class=\"icon-upload\"></i>";
                break;
            case ProjectStatus.QuoteAccepted:
                lblStatus.Text = "<strong>" + Resources.Resource.Quote + "</strong>";
                lblIconCtn.Attributes["class"] = "label bg-green";
                lblIconCtn.InnerHtml = "<i class=\"icon-upload\"></i>";
                break;
            case ProjectStatus.QuoteRefused:
                lblStatus.Text = "<strong>" + Resources.Resource.Quote + "</strong>";
                lblIconCtn.Attributes["class"] = "label bg-green";
                lblIconCtn.InnerHtml = "<i class=\"icon-upload\"></i>";
                break;
            case ProjectStatus.QuoteOutDated:
                lblStatus.Text = "<strong>" + Resources.Resource.Quote + "</strong>";
                lblIconCtn.Attributes["class"] = "label bg-green";
                lblIconCtn.InnerHtml = "<i class=\"icon-upload\"></i>";
                break;

            case ProjectStatus.JobOpened:
                lblStatus.Text = Resources.Resource.Opened_Job;
                lblIconCtn.Attributes["class"] = "label bg-blue";
                lblIconCtn.InnerHtml = "<i class=\"icon-cog5\"></i>";
                break;
            case ProjectStatus.JobFinished:
                lblStatus.Text = Resources.Resource.Opened_Job;
                lblIconCtn.Attributes["class"] = "label bg-blue";
                lblIconCtn.InnerHtml = "<i class=\"icon-cog5\"></i>";
                break;
            case ProjectStatus.JobOnHold:
                lblStatus.Text = "<strong>" + Resources.Resource.Opened_Job + "</strong>";
                lblIconCtn.Attributes["class"] = "label bg-blue";
                lblIconCtn.InnerHtml = "<i class=\"icon-cog5\"></i>";
                break;
            case ProjectStatus.JobOutDated:
                lblStatus.Text = "<strong>" + Resources.Resource.Opened_Job + "</strong>";
                lblIconCtn.Attributes["class"] = "label bg-blue";
                lblIconCtn.InnerHtml = "<i class=\"icon-cog5\"></i>";
                break;


            case ProjectStatus.InvoiceDraft:
                lblStatus.Text = "<em>" + Resources.Resource.Invoice + "</em>";
                lblIconCtn.Attributes["class"] = "label bg-danger";
                lblIconCtn.InnerHtml = "<i class=\"icon-file-check2\"></i>";
                break;
            case ProjectStatus.InvoicePaymentDue:
                lblStatus.Text = "<strong>" + Resources.Resource.Invoice + "</strong>";
                lblIconCtn.Attributes["class"] = "label bg-danger";
                lblIconCtn.InnerHtml = "<i class=\"icon-file-check2\"></i>";
                break;
            case ProjectStatus.InvoicePaid:
                lblStatus.Text = "<strong>" + Resources.Resource.Invoice + "</strong>";
                lblIconCtn.Attributes["class"] = "label bg-danger";
                lblIconCtn.InnerHtml = "<i class=\"icon-file-check2\"></i>";
                break;
            case ProjectStatus.InvoicePaymentLate:
                lblStatus.Text = "<strong>" + Resources.Resource.Invoice + "</strong>";
                lblIconCtn.Attributes["class"] = "label bg-danger";
                lblIconCtn.InnerHtml = "<i class=\"icon-file-check2\"></i>";
                break;

            default:
                break;
        }


    }


}