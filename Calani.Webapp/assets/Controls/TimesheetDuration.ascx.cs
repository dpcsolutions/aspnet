﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class assets_Controls_TimesheetDuration : System.Web.UI.UserControl
{
    public double TotalDuration { get; set; }
    public double? AttendedDuration { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        l.Text = String.Format("{0:0,0.00}", TotalDuration);
        if(AttendedDuration != null)
        {
            l.Text += " / " + String.Format("{0:0,0.00}", AttendedDuration.Value);

            if(Math.Abs(AttendedDuration.Value - TotalDuration) > 5)
            {
                l.CssClass += " text-danger";
            }
        }
    }
}