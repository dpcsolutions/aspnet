﻿<%@ Application Language="C#" %>
<%@ Import Namespace = "log4net" %>
<%@ Import Namespace = "System.Reflection" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="Hangfire" %>
<%@ Import Namespace="Hangfire.SqlServer" %>

<script runat="server">

	void Application_BeginRequest(Object Sender, EventArgs e)
    {
    	HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");
		HttpContext.Current.Response.AddHeader("Access-Control-Allow-Method", "POST, GET, OPTIONS");
		HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");

       if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
		{
			HttpContext.Current.Response.End();
		}
    }
    
    void Application_Start(object sender, EventArgs e)
    {

        log4net.Config.XmlConfigurator.Configure();

        // Code qui s’exécute au démarrage de l’application
        //Calani.BusinessObjects.Generic.MailSender.SmtpHost = ConfigurationManager.AppSettings["SmtpHost"];
        //Calani.BusinessObjects.Generic.MailSender.SmtpPort = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);
        //Calani.BusinessObjects.Generic.MailSender.SmtpUser = ConfigurationManager.AppSettings["SmtpUser"];
        //Calani.BusinessObjects.Generic.MailSender.SmtpPassword = ConfigurationManager.AppSettings["SmtpPassword"];
        //Calani.BusinessObjects.Generic.MailSender.SmtpSsl = ConfigurationManager.AppSettings["SmtpSsl"].ToLower() == "true";
        Calani.BusinessObjects.Generic.MailSender.MailTemplates = Server.MapPath("~/App_Data/mailtemplates");
        Calani.BusinessObjects.Generic.MailSender.PublicUrl = ConfigurationManager.AppSettings["PublicUrl"];
        //Calani.BusinessObjects.Generic.MailSender.MailFrom = ConfigurationManager.AppSettings["MailFrom"];
        //Calani.BusinessObjects.Generic.MailSender.CopyYear = ConfigurationManager.AppSettings["copyYear"];
        //Calani.BusinessObjects.Generic.MailSender.CopyrightText = ConfigurationManager.AppSettings["copyrightText"];
        //Calani.BusinessObjects.Generic.MailSender.CopyrightUrl = ConfigurationManager.AppSettings["copyrightUrl"];
        //Calani.BusinessObjects.Generic.MailSender.ApplicationTitle = ConfigurationManager.AppSettings["title"];

        Calani.BusinessObjects.Attachments.ProfilePictureManager.DIR = Server.MapPath("~/App_Data/storage/profilepict");
        Calani.BusinessObjects.Attachments.AttachmentsManager.DIR = Server.MapPath("~/App_Data/storage/attachments");
        Calani.BusinessObjects.Projects.WorkReportsManager.DIR = Server.MapPath("~/App_Data/storage/reports");
        Calani.BusinessObjects.Projects.WorkReportsManager.PublicUrl = ConfigurationManager.AppSettings["PublicUrl"];
        Calani.BusinessObjects.DocGenerator.CostsReportGenerator.ExpensesDir = Server.MapPath("~/App_Data/storage/expenses");
        Calani.BusinessObjects.Attachments.AttachmentsManager.MaxUploadedImagesWidth = Convert.ToInt32(ConfigurationManager.AppSettings["maxUploadedImagesWidth"]);

        Calani.BusinessObjects.Imports.FileImportManager.DIR = Server.MapPath("~/App_Data/storage/imports");

        Calani.BusinessObjects.DocGenerator.InvoiceGenerator.ExcelTemplates = Server.MapPath("~/App_Data/exceltemplates");
        Calani.BusinessObjects.DocGenerator.QuoteGenerator.ExcelTemplates = Server.MapPath("~/App_Data/exceltemplates");
        Calani.BusinessObjects.DocGenerator.TimeSheetGenerator.ExcelTemplates = Server.MapPath("~/App_Data/exceltemplates");

        Calani.BusinessObjects.DocGenerator.InvoiceGenerator.BvrTemplates = Server.MapPath("~/App_Data/bvr");

        Calani.BusinessObjects.SharedResource.Resource = Resources.Resource.ResourceManager;

    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code qui s’exécute à l’arrêt de l’application

    }

    void Application_Error(object sender, EventArgs e)
    {
        ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        Exception ex = null;

        // Code qui s'exécute lorsqu'une erreur non gérée se produit

        string username = "unknown";
        string userid = "unknown";
        string error = "unkown";
        string path = "";

        try
        {
            var currentContext = HttpContext.Current;
            username = PageHelper.GetUserName(currentContext.Session);
            userid = PageHelper.GetUserId(currentContext.Session).ToString();
        }
        catch { }

        try
        {
            ex = Server.GetLastError();
            if (sender is HttpApplication)
                path = ((HttpApplication)sender).Request.Url.PathAndQuery;

            if (ex.InnerException != null) ex = ex.InnerException;

            error = ex.Message;
            error += "\r\n";
            error += "\r\n";
            error += ex.StackTrace;
            error += "\r\n";
            error += "\r\n";
            error += path;
        }
        catch { }


        string reqParams = "Params\r\n";
        try
        {

            var currentContext = HttpContext.Current;

            foreach (string k in currentContext.Request.Params.Keys)
            {
                string val = currentContext.Request.Params[k];
                reqParams += k + " = " + val;
                reqParams += "\r\n";
            }



        }
        catch { }

        string body = "User Name: " + username;
        body += "\r\n";
        body += "User Id: " + userid;
        body += "\r\n";
        body += error;
        body += "\r\n";
        body += reqParams;

        log.Error(body, ex);

        Calani.BusinessObjects.Generic.MailSender email = new Calani.BusinessObjects.Generic.MailSender();
        string to = System.Configuration.ConfigurationManager.AppSettings["Bcc"];
        //email.SendEmail("error@gesmobile.ch", "error@gesmobile.ch", to.Split(';'), "GesMobile server error", body, false, null);
        email.SendErrorNotification(to.Split(';'), body);
    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code qui s’exécute lorsqu’une nouvelle session démarre

    }

    void Session_End(object sender, EventArgs e)
    {
        // Code qui s’exécute lorsqu’une session se termine. 
        // Remarque : l'événement Session_End est déclenché uniquement lorsque le mode sessionstate
        // a la valeur InProc dans le fichier Web.config. Si le mode de session a la valeur StateServer 
        // ou SQLServer, l’événement n’est pas déclenché.

    }

</script>
