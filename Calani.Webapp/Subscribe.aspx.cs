﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class Subscribe : System.Web.UI.Page
{
    public string LanguageSwitch { get; set; }

    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {


        LanguageSwitch = HtmlLanguageSwitch.Render(this.Page.Request.Url.AbsoluteUri, System.Threading.Thread.CurrentThread.CurrentCulture.ToString(), Page.ResolveClientUrl("~/assets/"));


        LabelCopyYear.Text = ConfigurationSettings.AppSettings["copyYear"];
        HyperLinkCopy.Text = ConfigurationSettings.AppSettings["copyrightText"];
        HyperLinkCopy.NavigateUrl = ConfigurationSettings.AppSettings["copyrightUrl"];

        this.Title = ConfigurationSettings.AppSettings["title"];


        if (!IsPostBack)
        {
            if (Request.Params["integration"] != null && Request.Params["integration"] == "extweb")
            {
                tbxEmail.Value = Request.Params["email"];
                tbxCompanyName.Value = Request.Params["company"];
                tbxPassword.Value = Request.Params["pwd"];
                tbxFirstname.Value = Request.Params["firstname"];
                tbxLastname.Value = Request.Params["lastname"];
                tbxPhone.Value = Request.Params["phone"];

                if (!String.IsNullOrWhiteSpace(tbxEmail.Value)
                    && !String.IsNullOrWhiteSpace(tbxCompanyName.Value)
                    && !String.IsNullOrWhiteSpace(tbxPassword.Value))
                {
                    cbxAcceptTerms.Checked = true;
                    btnCreateAccount_Click(btnCreateAccount, new EventArgs());
                }
            }
        }

    }

    protected void btnCreateAccount_Click(object sender, EventArgs e)
    {
        var mgr = new Calani.BusinessObjects.Membership.NewAccountManager();
        mgr.CompanyName = tbxCompanyName.Value;
        mgr.Email = tbxEmail.Value;
        //mgr.EmailConfirm = tbxEmail2.Value;

        string firstname = Resources.Resource.Firstname;
        string lastname = Resources.Resource.Lastname;

        

        if (!String.IsNullOrWhiteSpace(tbxFirstname.Value))
            firstname = tbxFirstname.Value;

        if (!String.IsNullOrWhiteSpace(tbxLastname.Value))
            lastname = tbxLastname.Value;

        if (!String.IsNullOrWhiteSpace(tbxPhone.Value))
            mgr.Phone = tbxPhone.Value;


        mgr.FirstName = firstname;
        mgr.LastName = lastname;


        mgr.Password = tbxPassword.Value;
        mgr.AcceptTerms = cbxAcceptTerms.Checked;
        mgr.KnowUs = ddlKnowUs.SelectedValue;

        if (!String.IsNullOrWhiteSpace(tbxBithdate.Value))
        {
            mgr.DateOfBirth = Convert2.ToDate(tbxBithdate.Value, "picker");
        }



        var r = mgr.Create(Server.MapPath("~/App_Data/dbschema/common/common.sql"));
        if (r.Success)
        {
            lblError.Visible = false;
            lblErrorText.Text = "";
            //lblSuccess.Text = Resources.Resource.NeedToValidateAccountByEmail.Replace("$email", tbxEmail.Value.ToLower());
            PanelRegistration.Visible = false;

            // instant login + redirect
            Session.Clear();
            PageHelper.InitUserSessions(mgr.Email, Session);
            //FormsAuthentication.RedirectFromLoginPage(mgr.Email, false);

            FormsAuthentication.SetAuthCookie(mgr.Email, true);
            Response.Redirect("FirstLogin.aspx");
            
        }
        else
        {
            lblError.Visible = true;
            lblErrorText.Text = ResourceHelper.Get(r.ErrorMessage);
        }
    }

    
}