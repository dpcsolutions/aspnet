﻿<%@ Page Title="Expenses Report" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Costs_Report_Details" Codebehind="Details.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
    <div class="btn-group" runat="server" id="quickmenu">
		<button type="button" class="btn bg-gesmobile btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
		</button>

		<ul class="dropdown-menu dropdown-menu-right">
			<li><a href="../Expenses/Edit.aspx"><i class="icon-plus2"></i> <%= Resources.Resource.Add_new %> <%= Resources.Resource.Expenses %></a></li>
		</ul>
	</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">


        <div class="content">

            <div>
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
										<strong id="lblLitle" runat="server"></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

							<div class="panel-body">
                                
                                <asp:Label runat="server" ID="lblNoData" Visible="false">
                                    <%= Resources.Resource.NoExpensesYet %>
                                </asp:Label>

                                <div>
                                    <a href="." class="btn btn-primary btn-labeled"> 
                                        <b><i class="icon-square-left"></i></b>
                                        <span id="ContentPlaceHolder1_SendScreen1_lblBack"><%= Resources.Resource.BackToList %></span>
                                    </a>
                                    <br />&nbsp;
                                </div>
                                
                                <asp:GridView runat="server" ID="grid" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:BoundField DataField="Year" HeaderText="Year" HtmlEncode="false"  />
                                        <asp:BoundField DataField="Month" HeaderText="Month" HtmlEncode="false"  />
                                        <asp:BoundField DataField="Lastname" HeaderText="employeeLastName" HtmlEncode="false"  />
                                        <asp:BoundField DataField="Firstname" HeaderText="employeeFirstName" HtmlEncode="false"  />
                                        <asp:BoundField DataField="Count" HeaderText="Expenses" HtmlEncode="false"  ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right" />
                                        <asp:BoundField DataField="Sum" HeaderText="Sum" HtmlEncode="false"  ItemStyle-CssClass="text-right" DataFormatString="{0:F2}" HeaderStyle-CssClass="text-right" />
   
                                        <asp:TemplateField ItemStyle-CssClass="text-right">
                                            <ItemTemplate>

                                                <asp:HyperLink runat="server"  ID="lnkPdf" NavigateUrl='<%# "Pdf.ashx?year=" + Eval("Year") + "&month=" + Eval("Month") + "&employee=" + Eval("Employee") + "&c=" + Culture %>' CssClass="btn btn-danger btn-xs" Target="_blank" style="margin-right:15px">
                                                    <i class="icon-file-pdf position-left"></i> PDF
                                                </asp:HyperLink>

                                                <asp:HyperLink runat="server"  ID="HyperLink1" NavigateUrl='<%# "Pdf.ashx?year=" + Eval("Year") + "&month=" + Eval("Month") + "&employee=" + Eval("Employee") + "&c=" + Culture + "&d=1" %>' CssClass="btn btn-danger btn-xs" style="margin-right:15px">
                                                    <i class="icon-download position-left"></i>
                                                </asp:HyperLink>

                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                            </div><!-- panel-body -->

                            

                        </div>
                    </div>

        </div>
    </form>
</asp:Content>
