﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.Costs;

public partial class Costs_Report_Default : System.Web.UI.Page
{
    public PageHelper ph;
    
    public string Culture
    {
        get
        {
            string ret = "en";
            try
            {
                ret = System.Threading.Thread.CurrentThread.CurrentCulture.ToString().ToLower().Substring(0, 2);
            }
            catch { }
            return ret;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ph = new PageHelper(this);
        
        ExpenseManager mgr = new ExpenseManager(ph.CurrentOrganizationId);
        mgr.UserId = ph.CurrentUserId;
        grid.DataSource = mgr.ReportMonths();
        grid.DataBind();

        new Html5GridView(grid, false);
    }


}