﻿<%@ WebHandler Language="C#" Class="Pdf" %>

using System;
using System.Web;

public class Pdf : IHttpHandler, System.Web.SessionState.IRequiresSessionState {

    public void ProcessRequest(HttpContext context)
    {

        int year = 0;
        int month = 0;
        long employee = 0;
        bool useCurrentUser;
        string culture = "";

        int.TryParse(context.Request.Params["year"], out year);
        int.TryParse(context.Request.Params["month"], out month);
        long.TryParse(context.Request.Params["employee"], out employee);
        bool.TryParse(context.Request.Params["useCurrentUser"], out useCurrentUser);
        culture = context.Request.Params["c"];

        if (useCurrentUser)
        {
            var employeeType = PageHelper.GetUserType(context.Session);
            if (employeeType != Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
                employee = PageHelper.GetUserId(context.Session);
        }

        bool dl = false;
        if (context.Request.Params["d"] != null && context.Request.Params["d"] == "1") dl = true;

        long userId = PageHelper.GetUserId(context.Session);
        long orgId = PageHelper.GetCurrentOrganizationId(context.Session);
        System.Globalization.CultureInfo ci = new System.Globalization.CultureInfo(culture);

        Calani.BusinessObjects.DocGenerator.CostsReportGenerator costs = new Calani.BusinessObjects.DocGenerator.CostsReportGenerator(ci, userId, orgId);
        costs.Year = year;
        costs.Month = month;
        if (employee > 0)
            costs.Employee = employee;

        //if (dl) // DEBUG
        //    costs.ExcelMode = true;

        byte[] data = costs.GenerateDocument();

        context.Response.ContentType = "application/pdf";

        string filename = year + "-" + month + "-" + employee + (costs.ExcelMode ? ".xls" : ".pdf");

        if (dl)
        {
            context.Response.AddHeader("content-disposition", "Attachment; filename=" + filename);
        }
        context.Response.OutputStream.Write(data, 0, data.Length);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}