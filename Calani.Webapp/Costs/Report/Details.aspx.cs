﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.Costs;

public partial class Costs_Report_Details : System.Web.UI.Page
{
    public PageHelper ph;

    public string Culture
    {
        get
        {
            string ret = "en";
            try
            {
                ret = System.Threading.Thread.CurrentThread.CurrentCulture.ToString().ToLower().Substring(0, 2);
            }
            catch { }
            return ret;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ph = new PageHelper(this);


        if (Request.QueryString["year"] != null && Request.QueryString["month"] != null)
        {
            string year = Request.QueryString["year"].ToString();
            string month = Request.QueryString["month"].ToString();

            DateTime dt = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), 1);

            if (!IsPostBack)
            {
                ExpenseManager mgr = new ExpenseManager(ph.CurrentOrganizationId);
                mgr.UserId = ph.CurrentUserId;
                grid.DataSource = mgr.ReportMonthsDetails(Convert.ToInt32(year), Convert.ToInt32(month));
                grid.DataBind();

                lblLitle.InnerText = Resources.Resource.Report + " - " + dt.ToString("MMMM") + " " + year;

            }

            new Html5GridView(grid, false);
        }
        else
        {
            Response.Redirect(".");
        }
    }
}