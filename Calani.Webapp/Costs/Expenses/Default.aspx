﻿<%@ Page Title="Expenses" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Costs_Expences_Default" Codebehind="Default.aspx.cs" %>

<%@ Register Src="~/assets/Controls/ExpenseApprovedAmountLabel.ascx" TagPrefix="uc1" TagName="ExpenseApprovedAmountLabel" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
    <div class="btn-group" runat="server" id="quickmenu">
		<button type="button" class="btn bg-gesmobile btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
		</button>

		<ul class="dropdown-menu dropdown-menu-right">
			<li><a href="Edit.aspx"><i class="icon-plus2"></i> <%= Resources.Resource.Add_new %> <%= Resources.Resource.Expenses %></a></li>
		</ul>
	</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">


        <div class="content" id="app">


                    <div class="row">
                <div class="col-sm-12 col-md-4">

                    <!-- User card with thumb and social icons at the bottom -->
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row text-center">

                                <div class="col-xs-4"  v-bind:class="{ 'bordered alpha-primary': statusFilter=='0' }">
                                    <a href="" class="text-muted" v-on:click="setStatusFilter($event, '0')" >
                                    <p><i class="icon-question6 icon-2x display-inline-block text-primary tada infinite" id="icoCountDraft" runat="server"></i></p>
                                    <h5 class="text-semibold no-margin" id="lblCountToApproved" runat="server">.</h5>
                                    <strong class="text-muted text-size-small"><%= Resources.Resource.ToApproved %></strong>
                                    </a>
                                </div>


                                <div class="col-xs-4" v-bind:class="{ 'bordered alpha-primary': statusFilter=='1' }">
                                    <a href="" class="text-success" v-on:click="setStatusFilter($event, '1')" >
                                    <p><i class="icon-history icon-2x display-inline-block text-success tada infinite" id="icoCountOutdated" runat="server"></i></p>
                                    <h5 class="text-semibold no-margin text-muted" id="lblCountArchives" runat="server">...</h5>
                                    <strong class="text-muted text-size-small"><%= Resources.Resource.Archives %></strong>
                                    </a>
                                </div>

                                <div class="col-xs-4" v-bind:class="{ 'bordered alpha-primary': statusFilter=='2' }">
                                    <a href="" class="text-success" v-on:click="setStatusFilter($event, '2')" >
                                    <p><i class="icon-cross2 icon-2x display-inline-block text-danger tada infinite" id="icoCountRejected" runat="server"></i></p>
                                    <h5 class="text-semibold no-margin text-muted" id="lblCountRejected" runat="server">...</h5>
                                    <strong class="text-muted text-size-small"><%= Resources.Resource.AbsenceRequest_Rejected %></strong>
                                    </a>
                                </div>

                            </div>
                        </div>

                        <div class="panel-footer text-center no-padding">
                            <div class="row">
                            </div>
                        </div>
                    </div>
                    <!-- /user card with thumb and social icons at the bottom -->

                </div>


                <div class="col-sm-6 col-md-4">
                </div>


                <div class="col-sm-7 col-md-4">
                    <div class="panel panel-body panel-body-accent">
                        <div class="media no-margin">
                            <div>
								<i class="fa fa-calendar-check-o" aria-hidden="true"></i> <%= Resources.Resource.Select_Period %>
							</div>
							<div>
								<select id="ddlPeriod" class="select" onchange="setPeriodFilter()">
									<%= OptionsPeriod %>
								</select>
							</div>
                        </div>
                    </div>
                </div>
            </div><!-- /row -->

					<div>
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
										<strong><%= ph.CMP.Name %></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

							<div class="panel-body">
                                <asp:Label runat="server" ID="lblNoData" Visible="false">
                                    <%= Resources.Resource.NoExpensesYet %>
                                </asp:Label>
                                <asp:GridView runat="server" ID="grid" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:HyperLinkField DataTextField="id" HeaderText="#" DataNavigateUrlFields="id"  ItemStyle-CssClass="text-nowrap p-5" DataNavigateUrlFormatString="Edit.aspx?id={0}" />
                                        <asp:ButtonField/>
                                        <asp:BoundField DataField="employeeName" HeaderText="Employee"  ItemStyle-CssClass="p-5" HtmlEncode="false"  />
                                        <asp:BoundField DataField="date" HeaderText="Date" HtmlEncode="false"  ItemStyle-CssClass="p-5"/>
                                        <asp:BoundField DataField="description" HeaderText="Description"   ItemStyle-CssClass="p-5"/>
                                        <asp:BoundField DataField="price" HeaderText="Amount" ItemStyle-CssClass="text-right text-nowrap p-5" DataFormatString="{0:F2}" HtmlEncode="false" HeaderStyle-CssClass="text-right" />
                                        <asp:BoundField DataField="currency" HeaderText="Currency"  ItemStyle-CssClass="text-nowrap p-5" />
                                        <asp:TemplateField HeaderText="ApprovedAmount" ItemStyle-CssClass="text-right text-nowrap p-5" HeaderStyle-CssClass="text-right" >
                                            <ItemTemplate>
                                                <uc1:ExpenseApprovedAmountLabel runat="server" ItemStyle-CssClass="text-nowrap p-5" ID="ExpenseApprovedAmountLabel" Amount='<%# Bind("approvedAmount") %>' Approved='<%# Bind("approved") %>' Rejected='<%# Bind("rejected") %>'/>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div><!-- panel-body -->

                            <div class="panel-footer text-right no-padding">
						    	<div class="row" style="margin:10px">
						    		<a href="Edit.aspx" class="btn btn-default"><i class="icon-plus2 position-left"></i> 
                                        <%=Resources.Resource.Add_new_Expense %>
      
						    		</a>
						    	</div>
							</div><!-- panel-footer -->

                        </div>
                    </div>
        </div><!-- /content -->

    </form>
    <script type="text/javascript" src="Default.js?<%= Guid.NewGuid().ToString("n") %>"></script>
    <%--<script type="text/javascript" src="Default.js"></script>--%>
</asp:Content>

