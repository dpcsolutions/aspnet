﻿using System;
using System.Collections.Generic;
using Calani.BusinessObjects.Costs;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Generic;

public partial class Costs_Expences_Default : System.Web.UI.Page
{
    public PageHelper ph;
    public string OptionsPeriod { get; set; }

    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        var orgMgr = new OrganizationManager(ph.CurrentOrganizationId);
        orgMgr.Load();

        long? filterEmployee = null;
        // remove records for employee
        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            filterEmployee = ph.CurrentUserId;
        }
        // -->

        OptionsPeriod = ph.RenderPeriodsListOption(preselect: orgMgr.DefaultPeriodFilter);

        EnableViewState = false;
        if (!IsPostBack)
        {
            ExpenseManager mgr = new ExpenseManager(ph.CurrentOrganizationId);
            var data = mgr.ListExpenses2(null, filterEmployee, null, null, Tools.DefaultDateFormat);
            grid.DataSource = data;
           // lblCountToApproved.InnerText = mgr.CountExpenses(false).ToString();
           // lblCountArchives.InnerText = mgr.CountExpenses(true).ToString();
            grid.DataBind();
            lblNoData.Visible = data.Count == 0;

            List<string> rowData = new List<string>() { "date", "status" };
            new Html5GridView(grid, false, rowData);
        }
    }
}