﻿<%@ WebHandler Language="C#" Class="ViewPhoto" %>

using System;
using System.Web;
using Calani.BusinessObjects.Costs;

public class ViewPhoto : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest (HttpContext context) {

        long orgId = PageHelper.GetCurrentOrganizationId(context.Session);
        string id = context.Request.Params["id"];
        string page = context.Request.Params["page"];
        long lid = 0;
        int ipage = 0;
        
        long.TryParse(id, out lid);
        int.TryParse(page, out ipage);

        ExpenseManager mgr = new ExpenseManager(orgId);
        var exp = mgr.Get(lid);

        if (ipage > 0) {
            if (exp != null && mgr.HasPhotos(lid))
            {
                byte[] bin = mgr.GetPhotoPage(lid, ipage);
                context.Response.ContentType = "image/jpeg";
                context.Response.BinaryWrite(bin);

                return;
            }
        }
        else {
            if (exp != null && mgr.HasPhoto(lid))
            {
                byte[] bin = mgr.GetPhoto(lid);
                context.Response.ContentType = "image/jpeg";
                context.Response.BinaryWrite(bin);

                return;
            }
        }

        context.Response.ContentType = "text/plain";
        context.Response.Write("Not found");
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}