﻿var _periodFilterFrom = null;
var _periodFilterTo = null;
var _statusFilter = '0';

var _dataTable = null;

function checkField(val, str){
    return val?.toString().toLowerCase().includes(str.toLowerCase());
}

function containsString (obj, str) {
    if (checkField(obj[0], str)) return true;
    if (checkField(obj[1], str)) return true;
    if (checkField(obj[2], str)) return true;
    if (checkField(obj[3].display, str)) return true;
    if (checkField(obj[4], str)) return true;
    if (checkField(obj[5], str)) return true;
    if (checkField(obj[6], str)) return true;
    if (checkField(obj[7], str)) return true;

    return false;
}

function setPeriodFilter() {
    _periodFilterFrom = null;
    _periodFilterTo = null;

    var pval = $('#ddlPeriod').val();
    if (pval != null && pval.indexOf(';') > 0) {
        pval = pval.split(';');
        if (pval.length == 2) {
            _periodFilterFrom = pval[0];
            _periodFilterTo = pval[1];

            if (_periodFilterFrom != "*")
                _periodFilterFrom = parseInt(_periodFilterFrom);
            else
                _periodFilterFrom = null;

            if (_periodFilterTo != "*")
                _periodFilterTo = parseInt(_periodFilterTo);
            else
                _periodFilterTo = null;
        }
    }

    refreshLines();
}

function refreshLines() {
    _dataTable.draw();
}




function refreshLiveStats() {

    let countToApproved = 0;
    let countArchives = 0;
    let countRejected = 0;

    let rowsId = _dataTable.rows().eq(0);

    for (let i = 0; i < rowsId.length; i++) {

        let row = _dataTable.row(rowsId[i]);
        let lastUpdate = parseInt(row.node().attributes['data-date'].value);
        
        let data = row.data();
        let searchText = $('.dataTables_filter input').val();

        if (_periodFilterFrom != null) {
            if (lastUpdate < _periodFilterFrom)
                continue;
        }
        if (_periodFilterTo != null) {
            if (lastUpdate >= _periodFilterTo)
                continue;
        }
        let rowStatus = row.node().attributes["data-status"].value;

        if (rowStatus != null) {
            let matchesSearch = true;
            if (searchText) matchesSearch = containsString(data, searchText);

            switch (rowStatus) {
                case "0":
                    if (matchesSearch) countToApproved++;
                    break;
                case "1":
                    if (matchesSearch) countArchives++;
                    break;
                case "2":
                    if (matchesSearch) countRejected++;
                    break;
            }
        }
    }


    $('#ContentPlaceHolder1_lblCountToApproved').html(countToApproved);
    $('#ContentPlaceHolder1_lblCountArchives').html(countArchives);
    $('#ContentPlaceHolder1_lblCountRejected').html(countRejected);
}

function filterItem(settings, data, dataIndex) {

    var visible = true;


    var lastUpdate = parseInt(settings.aoData[dataIndex].nTr.getAttribute('data-date'));
    if (_periodFilterFrom != null) {
        if (lastUpdate < _periodFilterFrom)
            visible = false;
    }
    if (_periodFilterTo != null) {
        if (lastUpdate >= _periodFilterTo)
            visible = false;
    }

    settings.aoData[dataIndex].nTr.setAttribute('data-anystatusFilter', visible);

    var status = settings.aoData[dataIndex].nTr.getAttribute('data-status');

    if (_statusFilter != null) {
        visible &= _statusFilter == status;
    }

    return visible;
}

$(function () {

    
    // client side filters
    $.fn.dataTable.ext.search.push(filterItem);
    // -->

    _dataTable = initDataTable($('#ContentPlaceHolder1_grid'), true);
    _dataTable.search('');

    _dataTable.on('draw', function () {
        refreshLiveStats();
    });


   

    setPeriodFilter();
});

new Vue({

    el: '#app',

    data: {
        statusFilter: '0'
    },
    methods: {
        setStatusFilter: function (e, f) {
            this.statusFilter = f;
            _statusFilter = f;
            _dataTable.draw();
            e && e.preventDefault();
        }
    }
});