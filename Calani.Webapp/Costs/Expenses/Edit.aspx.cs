﻿using Calani.BusinessObjects.Costs;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.DocGenerator;

public partial class Costs_Expences_Edit : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    public string CurrentId { get; set; }
    
    public string CurrenciesForJs { get; set; }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        ph.ItemLabel = Resources.Resource.Expenses;
        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
            btnDelete, btnMore, btnMoreDdl, lblTitle, linkBackList, linkNew);

        CurrentId = "-1";
        if (ph.CurrentId != null)
        {
            CurrentId = ph.CurrentId.ToString();
        }

      
        CurrenciesForJs = "null";

        var currencies = PageHelper.GetOrgCurrencies(ph.CurrentOrganizationId);
        if (currencies != null && currencies.Any())
        {
            CurrenciesForJs = new JavaScriptSerializer().Serialize(currencies);
            if (String.IsNullOrWhiteSpace(CurrenciesForJs))
            {
                CurrenciesForJs = "null";
            }
        }

        if (!IsPostBack)
        {
            Calani.BusinessObjects.Contacts.EmployeesManager taxesMgr = new Calani.BusinessObjects.Contacts.EmployeesManager(ph.CurrentOrganizationId);
            var employees = (from r in taxesMgr.List() select new { id = r.id, name = r.firstName + " " + r.lastName }).ToList();
            ddlEmployee.DataSource = employees;
            ddlEmployee.DataBind();
            
            #region Currency

            if (currencies != null && currencies.Any())
            {
                divProject_Currencies.Visible = true;
                ddlCurrencies.DataSource = currencies;
                ddlCurrencies.DataBind();
            }
            else
            {
                CurrenciesForJs = "null";
                divProject_Currencies.Visible = false;
            }

            #endregion
            
           
            Calani.BusinessObjects.Projects.ProjectsManager projMgr = new Calani.BusinessObjects.Projects.ProjectsManager(ph.CurrentOrganizationId);
            List<JsVariableForTemplate> projects = (from r in projMgr.ListOpens() where r.ProjectStatus == Calani.BusinessObjects.Projects.ProjectStatus.JobOpened select new JsVariableForTemplate {  value = r.id.ToString(), name = r.clientName + " - " + r.internalId }).ToList();
            projects.Insert(0, new JsVariableForTemplate { value = "", name = "(" + Resources.Resource.None + ")" });
            ddlProjects.DataSource = projects;
            ddlProjects.DataBind();

            ddlEmployee.SelectedValue = ph.CurrentUserId.ToString();

            long pid;

            long.TryParse(Request.Params["pid"], out pid);

            if (pid > 0)
            {
                ddlProjects.SelectedValue = pid.ToString();
            }
        }

        if (!IsPostBack && ph.CurrentId != null)
        {
            Load();
            ph.SetStateEdit(Resources.Resource.Edit_Expense);
        }

        if (ph.CurrentId == null)
        {
            ph.SetStateNew(Resources.Resource.Add_new_Expense);

            liPhotoAdd.Attributes["class"] = "active";
            tabphotoupload.Attributes["class"] = "tab-pane active";
            liPhotoView.Visible = false;
        }
        

        // field disabled for for employee
        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            ddlEmployee.Enabled = false;
            tbxApprovedAmount.Disabled = true;
            btnAction.Visible = false;
            btnActionDisabled.Visible = true;
            scriptsValidation.Visible = false;
        }
        // -->
    }
    
    #region CRUD operations
    // Load from db and bind controls
    private void Load()
    {
        ExpenseManager mgr = new ExpenseManager(ph.CurrentOrganizationId);
        

        var item = mgr.GetExpense(Convert.ToInt64(CurrentId));
        if (item != null)
        {
            if (!canEdit(item))
            {
                btnSave.CssClass = btnSave.Attributes["class"]  + " disabled";
                btnMore.Attributes["class"] += " disabled";
            }

            ddlCurrencies.Enabled = false;

            if (item.date != null)
                tbxDate.Value = item.date.Value.ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture);

            if (item.date != null)
                tbxDate.Value = item.date.Value.ToString("dd/MM/yyyy");

            if (item.employeeId != null)
                ddlEmployee.SelectedValue = item.employeeId.Value.ToString();
            else
                ddlEmployee.SelectedValue = ph.CurrentUserId.ToString();


            if (item.linkedjobid != null)
                ddlProjects.SelectedValue = item.linkedjobid.ToString();

            tbxDescription.Value = item.description;

            var currencies = PageHelper.GetOrgCurrencies(ph.CurrentOrganizationId);

            if (!String.IsNullOrWhiteSpace(item.currency))
            {
                ddlCurrencies.ClearSelection();
                var ddlItem = ddlCurrencies.Items.FindByValue(item.currency);

                if (null != ddlItem)
                {
                    ddlItem.Selected = true;
                }
                else
                {
                    ddlCurrencies.Items.Insert(0, new ListItem(item.currency, "0"));
                    ddlCurrencies.Items.FindByText(item.currency).Selected = true;

                    divProject_Currencies.Visible = true;
                }

            }

            if (item.amount != null)
            {

                var amount = Convert2.ToString(item.amount);
                tbxAmount.Value = amount;

            }

            if (item.id > 0 && item.contacts.id != ph.CurrentUserId)
            {
                tbxAmount.Disabled = true;
            }

            if (item.approvedmount != null)
                tbxApprovedAmount.Value = Convert2.ToString(item.approvedmount);

            ddlPayType.SelectedValue = item.paytypeId.ToString();
            inVAT1.Value = Convert2.ToString(item.vat1);
            inVAT2.Value = Convert2.ToString(item.vat2);
            inVAT3.Value = Convert2.ToString(item.vat3);

            if (mgr.HasPhoto(Convert.ToInt64(CurrentId)))
            {
                liPhotoView.Visible = true;
                tabphotoview.Visible = true;
                
                liPhotoView.Attributes["class"] = "active";
                tabphotoview.Attributes["class"] = "tab-pane active";

                liPhotoAdd.Attributes["class"] = "";
                tabphotoupload.Attributes["class"] = "tab-pane";

                AlertNoPicture.Visible = false;

                img.Visible = true;
                img.ImageUrl = "ViewPhoto.ashx?id=" + CurrentId;
                imgzoom.NavigateUrl = img.ImageUrl;
            }
            else if (mgr.HasPhotos(Convert.ToInt64(CurrentId)))
            {
                liPhotoView.Visible = false;
                tabphotoview.Visible = false;
                
                liPhotoAdd.Attributes["class"] = "";
                tabphotoupload.Attributes["class"] = "tab-pane";
                
                AlertNoPicture.Visible = false;
                
                var di = new DirectoryInfo(Path.Combine(Calani.BusinessObjects.DocGenerator.CostsReportGenerator.ExpensesDir, ph.CurrentOrganizationId.ToString()));
                var i = 1;

                foreach (var fileInfo in di.GetFiles($"{CurrentId}page*"))
                {
                    var tabId = $"page{i}";

                    HtmlGenericControl tabListItem = new HtmlGenericControl("li");
                    tabListItem.Attributes["class"] = i == 1 ? "active" : "";

                    HtmlGenericControl link = new HtmlGenericControl("a");
                    link.Attributes["href"] = $"#{tabId}";
                    link.Attributes["data-toggle"] = "tab";
                    link.InnerText = Resources.Resource.View + " " + i;

                    tabListItem.Controls.Add(link);
                    tabHeaderUl.Controls.Add(tabListItem);

                    HtmlGenericControl tabContentChild = new HtmlGenericControl("div");
                    tabContentChild.Attributes["class"] = $"tab-pane{(i == 1 ? " active" : "")}";
                    tabContentChild.Attributes["id"] = $"page{i}";
                    
                    HtmlGenericControl imgElement = new HtmlGenericControl("img");
                    imgElement.Attributes["src"] = $"ViewPhoto.ashx?id={CurrentId}&page={i}";
                    imgElement.Attributes["style"] = "width:100%";

                    tabContentChild.Controls.Add(imgElement);
                    tabContentDiv.Controls.Add(tabContentChild);
                    
                    i++;
                }
            }
            else
            {
                liPhotoView.Attributes["class"] = "";
                tabphotoview.Attributes["class"] = "tab-pane";

                liPhotoAdd.Attributes["class"] = "active";
                tabphotoupload.Attributes["class"] = "tab-pane active";

                AlertNoPicture.Visible = true;
            }

        }
        else
        {
            throw new Exception("Not allowed to access this record");
        }

    }

    
    // Create a new db record from controls values
    private void Update()
    {
        var mgr = new ExpenseManager(ph.CurrentOrganizationId);
        var ex = mgr.GetExpense(Convert.ToInt64(CurrentId));

        if (!canEdit(ex))
        {
            return;
        }

        var isAmountSet = !string.IsNullOrWhiteSpace(tbxApprovedAmount.Value);

        var expense = new ExpenseExtended()
        {
            id = ex.id.ToString(),
            date = Convert2.ToDate(tbxDate.Value, Tools.DefaultDateFormat, DateTime.UtcNow).Value.ToString(Tools.IsoLongDateFormat, CultureInfo.InvariantCulture),
            lastModificationDate = DateTime.Now.ToString(Tools.IsoLongDateFormat, CultureInfo.InvariantCulture),
            lastModifiedBy = ph.CurrentUserId,
            employeeId = Convert2.ToNullableLong(ddlEmployee.SelectedValue),
            description = tbxDescription.Value,
            price = Convert2.ToNullableDouble(tbxAmount.Value, 0, 2, 0) ?? 0,
            approvedAmount = Convert2.ToNullableDouble(tbxApprovedAmount.Value, null, 2, 0) ?? 0,
            linkedjobid = GetJob(),
            photo = GetPictures(),
            jobid = Convert2.ToNullableLong(ddlProjects.SelectedValue),
            PayType = Convert.ToInt32(ddlPayType.SelectedValue),
            VAT1 = Convert2.ToNullableDouble(inVAT1.Value, maxDecimals: 2) ?? 0,
            VAT2 = Convert2.ToNullableDouble(inVAT2.Value, maxDecimals: 2) ?? 0,
            VAT3 = Convert2.ToNullableDouble(inVAT3.Value, maxDecimals: 2) ?? 0,
        };


        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            expense.approvedAmount = ex.approvedmount ?? 0;
        }
        else if(ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
        {
            expense.status = !isAmountSet ? (int)ValidationEnum.Pending : expense.approvedAmount == 0 ? (int)ValidationEnum.Rejected : (int)ValidationEnum.Approved;
        }

        if (ex.currency != null)
        {
            expense.currencyRate = ex.currencyRate??1;
            expense.currency = ex.currency;
            expense.price = expense.price;
        }


        expense = mgr.SaveOrUpdate(expense, ph.CurrentUserId);

       

        var id = long.Parse(expense.id);
        if (id > 0)
        {
            ph.DisplaySuccess(Resources.Resource.Updated_var);
            Load();
        }
        else
            ph.DisplayError(Resources.Resource.ProcessingError);
    }

    private long? GetJob()
    {
        long r = -1;
        if(!String.IsNullOrWhiteSpace(ddlProjects.SelectedValue))
        {
            long.TryParse(ddlProjects.SelectedValue, out r);
            if(r > -1)
            {
                Calani.BusinessObjects.Projects.ProjectsManager mgr = new Calani.BusinessObjects.Projects.ProjectsManager(ph.CurrentOrganizationId);
                var p = mgr.Get(r);
                if (p != null)
                {
                    r = p.id;
                }
            }
        }
        if (r <= -1) return null;
        return r;
    }

    private string GetPictures()
    {
        if (string.IsNullOrWhiteSpace(hfFiles.Value)) return null;
        
        var fileList = hfFiles.Value.Split(',').ToList();
        if (!fileList.Any()) return null;
        
        string base64String = null;
        
        foreach (var file in fileList)
        {
            byte[] ret = null;
            string path = Path.Combine(Calani.BusinessObjects.Attachments.AttachmentsManager.DIR, ph.CurrentOrganizationId.ToString(), file);
            if (File.Exists(path))
            {
                byte[] bin = File.ReadAllBytes(path);
                File.Delete(path);
                ret = bin;
            }

            if (string.IsNullOrEmpty(base64String)) base64String =  Convert.ToBase64String(ret ?? Array.Empty<byte>()); 
            else base64String += ';' + Convert.ToBase64String(ret ?? Array.Empty<byte>());
        }
        
        return base64String;
       
    }

    // Update existing db record from new controls values
    private void Create()
    {
        var mgr = new ExpenseManager(ph.CurrentOrganizationId);

        var expense = new ExpenseExtended
        {
            date = Convert2.ToDate(tbxDate.Value, Tools.DefaultDateFormat, DateTime.Now).Value.ToString(Tools.IsoLongDateFormat, CultureInfo.InvariantCulture),
            employeeId = Convert2.ToNullableLong(ddlEmployee.SelectedValue),
            description = tbxDescription.Value,
            price = Convert2.ToNullableDouble(tbxAmount.Value, 0, 2, 0)??0,
            approvedAmount = Convert2.ToNullableDouble(tbxApprovedAmount.Value, null, 2, 0)??0,
            linkedjobid = GetJob(),
            photo = GetPictures(),
            PayType = Convert.ToInt32(ddlPayType.SelectedValue),
            VAT1 = Convert2.ToNullableDouble(inVAT1.Value, maxDecimals: 2) ?? 0,
            VAT2 = Convert2.ToNullableDouble(inVAT2.Value, maxDecimals: 2) ?? 0,
            VAT3 = Convert2.ToNullableDouble(inVAT3.Value, maxDecimals: 2) ?? 0
        };
        
        var currencyId = ddlCurrencies.SelectedValue;
        
        var currencies = PageHelper.GetOrgCurrencies(ph.CurrentOrganizationId);

        if (!String.IsNullOrWhiteSpace(currencyId))
        {
            if (currencies != null && currencies.Any())
            {
                var curr = currencies.FirstOrDefault(c => c.currency1 == currencyId);

                if (curr != null)
                {
                    expense.currencyRate = curr.rate;
                    expense.currency = curr.currency1;
                }
                else
                {
                    curr = currencies.FirstOrDefault();
                    if (curr != null)
                    {
                        expense.currencyRate = 1;
                        expense.currency = curr.currency1;
                    }
                }
            }
        }
        else
        {
            var curr = currencies.FirstOrDefault();
            if (curr != null)
            {
                expense.currencyRate = 1;
                expense.currency = curr.description;
            }
        }


        expense = mgr.SaveOrUpdate(expense, ph.CurrentUserId);
        //var ret = mgr.Add(expense);
        var id = long.Parse(expense.id);
        if (id>0)
        {
            ph.DisplaySuccess(Resources.Resource.Created_var);
            CurrentId = expense.id;
           
            Load();
            btnSave.Enabled = false;
        }
        else 
            ph.DisplayError(Resources.Resource.ProcessingError);
    }
    

    protected void Delete()
    {
        var mgr = new ExpenseManager(ph.CurrentOrganizationId);
        var ex = mgr.GetExpense(Convert.ToInt64(CurrentId));

        if (!canEdit(ex))
        {
            return;
        }

        var ret = mgr.Delete(ph.CurrentId.Value, ph.CurrentUserId);
        if (ret.Success)
        {
            panelEdit.Visible = false;
            ph.DisplaySuccess(Resources.Resource.Deleted_var);
        }
    }
    #endregion
    
    #region Buttons (triggers)
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) 
            Update();
        else 
            Create();
    }


    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null)
            Delete();
    }
    #endregion
    
    private bool isAdmin()
    {
        return ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin;
    }

    private bool canEdit(expenses expense)
    {
        var isValidated = (ValidationEnum)expense.validationStatus != ValidationEnum.Pending;

        return !isValidated || isAdmin();
    }
}