﻿<%@ WebHandler Language="C#" Class="UploadPhoto" %>

using System;
using System.Reflection;
using System.Web;
using log4net;

public class UploadPhoto : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
    public void ProcessRequest(HttpContext context)
    {
        try
        {
                context.Response.ContentType = "text/plain";


        long orgId = PageHelper.GetCurrentOrganizationId(context.Session);

        string dir = System.IO.Path.Combine(Calani.BusinessObjects.Attachments.AttachmentsManager.DIR, orgId.ToString());
        if (!System.IO.Directory.Exists(dir)) System.IO.Directory.CreateDirectory(dir);

        if (context.Request.Files.Count == 1)
        {

            HttpPostedFile file = context.Request.Files[0];
            string fileName = file.FileName;
            string fileType = file.ContentType;


            byte[] fileData = null;

            if(fileType.StartsWith("application/pdf"))
            {
                using (var binaryReader = new System.IO.BinaryReader(file.InputStream))
                {
                    fileData = binaryReader.ReadBytes(file.ContentLength);
                }

                System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(dir);
                if (!di.Exists) di.Create();

                string guid = Guid.NewGuid().ToString("n");

                string filepath = System.IO.Path.Combine(di.FullName, guid + ".pdf");
                System.IO.File.WriteAllBytes(filepath, fileData);
                fileData = null;

                // ne fonctionne pas
                Calani.BusinessObjects.Generic.PdfToImage converter = new Calani.BusinessObjects.Generic.PdfToImage();
                converter.Convert(filepath, 1);

                System.IO.File.Delete(filepath);

                fileType = "image/jpg";
                fileName = fileName + ".jpg";
                filepath = filepath +".jpg";
                fileData = System.IO.File.ReadAllBytes(filepath);
            }

            if (fileType.StartsWith("image/"))
            {
                if (fileData == null)
                {
                    using (var binaryReader = new System.IO.BinaryReader(file.InputStream))
                    {
                        fileData = binaryReader.ReadBytes(file.ContentLength);
                    }
                }

                Calani.BusinessObjects.Generic.ImageManager imgMgr = new Calani.BusinessObjects.Generic.ImageManager();
                imgMgr.MaxSide = 2048;
                /*imgMgr.TargetWidth = 2048;
                imgMgr.TargetHeight = 2048;*/
                imgMgr.Init(fileData);

                byte[] newData = imgMgr.Save(System.Drawing.Imaging.ImageFormat.Jpeg);


                string name = "exp_" + DateTime.Now.ToString("yyyyMMddHHmmss") + "_" + Guid.NewGuid().ToString("n") + ".jpg";



                System.IO.File.WriteAllBytes(System.IO.Path.Combine(dir, name), newData);
                context.Response.Write(name);


            }

            else

            {


                throw new Exception("File is not an image");
            }
        }
        else
        {
            throw new Exception("Can not upload multiple photos");
        }
        }
        catch (Exception e)
        {
            _log.Error("UploadPhoto. error. ", e);
            throw;
        }
        



    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}