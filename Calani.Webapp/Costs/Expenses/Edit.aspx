﻿<%@ Page Title="Edit Service" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Costs_Expences_Edit" Codebehind="Edit.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    var currencies = <%= CurrenciesForJs %>;
</script>

    <form runat="server"  >

        <div class="content">

		    <div class="row">

                <!-- left column -->
			    <div class="col-sm-12 col-md-9">


                        <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Error %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblErrorMessage" />
						    </div>
					    </asp:Panel><!-- panelError -->

                        <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Done %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblSuccess" />
                                <br />
                                <strong><a href="." id="linkBackList" runat="server"></a></strong>
                                    <%= Resources.Resource.or %>
                                <a href="Edit.aspx" id="linkNew" runat="server"></a>.
						    </div>
					    </asp:Panel><!-- panelSuccess -->
					    
					    <asp:Panel runat="server" ID="panelEdit" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><asp:Label runat="server" ID="lblTitle" /></h6>
						    </div>

						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">

                                        <!-- date -->
									    <div class="form-group">
										    <label class="control-label col-lg-3"><%= Resources.Resource.Date %></label>
										    <div class="col-lg-9">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-calendar"></i></span>
												    <input type="text" class="form-control daterange-single" placeholder='<%$ Resources:Resource, Description %>' runat="server" id="tbxDate" />
											    </div>
										    </div>
									    </div>
                                        <!-- /date -->

                                        <!-- employee -->
									    <div class="form-group">
										    <label class="control-label col-lg-3"><%= Resources.Resource.Employee %></label>
										    <div class="col-lg-9">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-user"></i></span>
                                                    <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlEmployee"
                                                        DataValueField="id" DataTextField="name" />
											    </div>
										    </div>
									    </div>
                                        <!-- /employee -->

                                        <!-- description -->
									    <div class="form-group">
										    <label class="control-label col-lg-3"><%= Resources.Resource.Description %></label>
										    <div class="col-lg-9">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-font"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Description %>' runat="server" id="tbxDescription" />
											    </div>
										    </div>
									    </div>
                                        <!-- /description -->


                                        <!-- picture -->
									    <div class="form-group">
										    <label class="control-label col-lg-3"><%= Resources.Resource.Photo %></label>
										    <div class="col-lg-9">


                                                <div class="tabbable">
					                                <ul class="nav nav-tabs nav-tabs-highlight" id = "tabHeaderUl" runat="server">
						                                <li class="" runat="server" id="liPhotoView"><a href="#ContentPlaceHolder1_tabphotoview" data-toggle="tab"><%= Resources.Resource.View %></a></li>
						                                <li class="" runat="server" id="liPhotoAdd"><a href="#ContentPlaceHolder1_tabphotoupload" data-toggle="tab"><%= Resources.Resource.Upload %></a></li>
					                                </ul>

                                                    <div class="tab-content" id = "tabContentDiv" runat="server">
	                                                    <div class="tab-pane" id="tabphotoview" runat="server">
                                                            <div class="alert alert-danger no-border" runat="server" id="AlertNoPicture">
										                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only"><%= Resources.Resource.Close %></span></button>
										                        <span class="text-semibold"><%= Resources.Resource.NoPicture %>!</span> <%= Resources.Resource.NoPicture_More %>.
								                            </div>

                                                            <asp:HyperLink runat="server" Target="_blank" ID="imgzoom">
                                                                <asp:Image runat="server" ID="img" style="width:100%" />
                                                            </asp:HyperLink>
                                                        </div>
						                                
                                                        <div class="tab-pane" id="tabphotoupload" runat="server">
                                                            

                                                            <asp:HiddenField runat="server" ID="hfFiles" />

											                <div id="dropzone_photo" class="fallback dropzone dz-clickable">
                              
                                                            </div>


                                                        </div>

                                                    </div>
                                                </div>


                                                
										    </div>
									    </div>
                                        <!-- /picture -->

                                        

                                        <!-- amount -->
									    <div class="form-group">
										    <label class="control-label col-lg-3"><%= Resources.Resource.Amount %></label>
										    <div class="col-lg-9">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-credit-card"></i></span>
												    <input type="number" class="form-control" min="1" max="1000" placeholder='<%$ Resources:Resource, Amount %>' runat="server" id="tbxAmount" />
											    </div>
										    </div>
									    </div>
                                        <!-- /amount -->
										
                                        <!-- currencies -->
                                        <div class="form-group" runat="server" id="divProject_Currencies">
                                            <label class="control-label col-lg-3"><%= Resources.Resource.Currencies %></label>
                                            <div class="col-lg-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-coin-dollar"></i></span>
                                                    <asp:DropDownList runat="server" ID="ddlCurrencies" CssClass="form-control select" DataTextField="description" DataValueField="currency1" >
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
											
                                        </div>
                                        <!-- /currencies -->

                                         <!-- approvedamount -->
									    <div class="form-group">
										    <label class="control-label col-lg-3"><%= Resources.Resource.ApprovedAmount %></label>
										    <div class="col-lg-9">
											    <div class="input-group">
                                                    <span class="input-group-addon" runat="server" id="btnActionDisabled" visible="false"><i class="icon-checkmark"></i></span>
                                                    <div class="input-group-btn" runat="server" id="btnAction">
													    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="icon-checkmark"></i>
                                                            <%= Resources.Resource.Action %> <span class="caret"></span>
													    </button>
					                                    <ul class="dropdown-menu" style="min-width:300px">
														    <li><a href="javascript:accept()"><i class="icon-checkmark"></i> <%= Resources.Resource.ApproveAll %></a></li>
														    <li><a href="javascript:anyamount()"><i class="icon-calculator"></i> <%= Resources.Resource.ApproveAnyAmount %></a></li>
                                                            <li><a href="javascript:reject()"><i class="icon-blocked"></i> <%= Resources.Resource.ApproveNothing %></a></li>
													    </ul>
												    </div>
												    <input type="number" class="form-control" placeholder='<%$ Resources:Resource, ApprovedAmount_Tooltip %>' 
                                                           runat="server" id="tbxApprovedAmount" onblur="validateApprovedAmount()"/>
                                                   
											    </div>
                                                <div runat="server" id="scriptsValidation">
                                                    <script type="text/javascript">
                                                        function accept() {
                                                            
                                                            $('#<%= tbxApprovedAmount.ClientID %>').val($('#<%=tbxAmount.ClientID %>').val());

                                                            }

                                                            function anyamount() {
                                                                swal({
                                                                    text: "<%= Resources.Resource.ApproveAnyAmount %>",
                                                                    input: 'number',
                                                                    inputAttributes: {
                                                                        min: 0,
                                                                        max: $('#<%=tbxAmount.ClientID %>').val(),
                                                                        step: 1
                                                                    },
                                                                }).then(function(result) {
                                                                    if (result.value) {
                                                                        $('#<%= tbxApprovedAmount.ClientID %>')
                                                                            .val(result.value);
                                                                        $('#<%= tbxApprovedAmount.ClientID %>').focus();
                                                                    }
                                                                });

                                                            }

                                                            function reject() {
                                                                $('#ContentPlaceHolder1_tbxApprovedAmount').val('0');
                                                            }
                                                    </script>
                                                </div>
										    </div>
									    </div>
                                        <!-- /approvedamount -->


                                        <!-- amount -->
									    <div class="form-group">
										    <label class="control-label col-lg-3"><%= Resources.Resource.AttributeProject %></label>
										    <div class="col-lg-9">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-cog5"></i></span>
												    <asp:DropDownList runat="server" ID="ddlProjects" CssClass="select-search"
                                                        DataTextField="name" DataValueField="value">
												    </asp:DropDownList>
											    </div>
										    </div>
									    </div>
                                        <!-- /amount -->
										

                                         <!-- paytype -->
									    <div class="form-group">
										    <label class="control-label col-lg-3"><%= Resources.Resource.PayType%></label>
										    <div class="col-lg-9">
<%--												<select id="selPayType" runat="server" class="select">
													<option value="1"><%= Resources.Resource.PayTypeCash%></option>
													<option value="2"><%= Resources.Resource.PayTypeCard%></option>
													<option value="1" title='<%$ Resources:Resource, PayTypeCash %>'></option>
													<option value="2" title='<%$ Resources:Resource, PayTypeCard %>'></option>
												</select>--%>

												<asp:DropDownList runat="server" ID="ddlPayType" CssClass="form-control select">
													<asp:ListItem Value="1" Text='<%$ Resources:Resource, PayTypeCash %>'></asp:ListItem>
													<asp:ListItem Value="2" Text='<%$ Resources:Resource, PayTypeCard %>'></asp:ListItem>
												</asp:DropDownList>

<%--											    <div class="input-group">
                                                    <span class="input-group-addon" runat="server" id="Span1" visible="false"><i class="icon-checkmark"></i></span>
                                                    <div class="input-group-btn" runat="server" id="Div1">
													    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="icon-checkmark"></i>
                                                            <%= Resources.Resource.Action %> <span class="caret"></span>
													    </button>
					                                    <ul class="dropdown-menu" style="min-width:300px">
														    <li><a ><i class="icon-checkmark"></i> <%= Resources.Resource.PayTypeCash%></a></li>
														    <li><a ><i class="icon-calculator"></i> <%= Resources.Resource.PayTypeCard%></a></li>
													    </ul>
												    </div>
											    </div>--%>
										    </div>
									    </div>
                                        <!-- /paytype -->

                                        <!-- VAT -->
									    <div class="form-group">
										    <label class="control-label col-lg-3"><%= Resources.Resource.VAT81%></label>
										    <div class="col-lg-9">
											    <div class="input-group">
													<input type="number" runat="server" id="inVAT1" class="form-control" />
												</div>
											</div>
										</div>

									    <div class="form-group">
										    <label class="control-label col-lg-3"><%= Resources.Resource.VAT38%></label>
										    <div class="col-lg-9">
											    <div class="input-group">
													<input type="number" runat="server" id="inVAT2" class="form-control" />
												</div>
											</div>
										</div>

									    <div class="form-group">
										    <label class="control-label col-lg-3"><%= Resources.Resource.VAT26%></label>
										    <div class="col-lg-9">
											    <div class="input-group">
													<input type="number" runat="server" id="inVAT3" class="form-control" />
												</div>
											</div>
										</div>
                                        <!-- VAT -->

								    </fieldset>



                                    <!-- buttonsaction -->
								    <div class="buttonsaction text-right">
										<label class="control-label col-lg-2"></label>
										<div class="col-lg-10">
											<div class="btn-group">
                                                <asp:LinkButton runat="server" ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click">
                                                    <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                                </asp:LinkButton>
                                                
									            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore"><span class="caret"></span></button>
									            <ul class="dropdown-menu dropdown-menu-right" runat="server" id="btnMoreDdl">
                                                    <li><a href="javascript:void(0)" data-toggle="modal" data-target="#modal_default"><i class="fa fa-trash"></i> <%= Resources.Resource.Delete %></a></li>
												</ul>
											</div>
										</div>
									</div>
                                    <!-- /buttonsaction -->


							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->
					    
           


			    </div>
			    <!-- /left column -->
                

                <!-- right column -->
                <div class="col-sm-12 col-md-6">

			    </div>
                <!-- /right column -->
						

		    </div> <!-- /row -->


					
            <!-- basic modal: confirm delete -->
		    <div id="modal_default" class="modal fade">
			    <div class="modal-dialog">
				    <div class="modal-content">
					    <div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal">&times;</button>
						    <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
					    </div>

					    <div class="modal-body">
						    <h6 class="text-semibold"><%= Resources.Resource.Are_You_Sure_To_Delete %></h6>
						    <p><%= Resources.Resource.Delete_Is_Irreversible %></p>

					    </div>

					    <div class="modal-footer">
						    <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
                            <asp:Button runat="server" ID="btnDelete" OnClick="btnDelete_Click" CssClass="btn btn-primary" Text="<%$ Resources:Resource, Yes_Delete %>" />

					    </div>
				    </div>
			    </div>
		    </div>
		    <!-- /basic modal -->



        </div><!-- /content -->

    </form>
    <script type="text/javascript">

        var ressources = {};
        ressources['DropFilesToUpload'] = "<%= Resources.Resource.DropFilesToUpload %>";
        ressources['OrClick'] = "<%= Resources.Resource.OrClick %>";

        var id = <%= CurrentId %>;

        $(function () {
            Dropzone.autoDiscover = false;
            $("#dropzone_photo").dropzone({
                uploadMultiple: false,
                url: "UploadPhoto.ashx?id=" + id,
                dictDefaultMessage: ressources['DropFilesToUpload'] + ' <span>' + ressources['OrClick'] + '</span>',
                maxFilesize: 30,
                acceptedFiles: "image/*",
                init: function () {
                    this.on("complete", function (file) {
						let $files = $('#ContentPlaceHolder1_hfFiles');
						let currentVal = $files.val();
                        if (currentVal === ''){
                        	$files.val(file.xhr.responseText);
						} else {
							$files.val(currentVal + ',' + file.xhr.responseText);
						}
                    });
                }
            });
        });


        function validateApprovedAmount() {
            
            var tbxApprovedAmount = $('#<%=tbxApprovedAmount.ClientID %>').val();
            var tbxAmount = $('#<%=tbxAmount.ClientID %>').val();
            
            if (Number(tbxApprovedAmount) > Number(tbxAmount)) {
                $('#<%=tbxApprovedAmount.ClientID %>').val(tbxAmount);
            }
            return true;
        }
        
      

    </script>
</asp:Content>


