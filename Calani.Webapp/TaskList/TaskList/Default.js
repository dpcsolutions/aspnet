﻿let _periodFilterFrom = null;
let _periodFilterTo = null;
let _clientFilter = null;
let _statusFilter = 'InProgress';

let _dataTable = null;

function setPeriodFilter() {
    _periodFilterFrom = null;
    _periodFilterTo = null;

    let pval = $('#ddlPeriod').val();
    if (pval != null && pval.indexOf(';') > 0) {
        pval = pval.split(';');
        if (pval.length == 2) {
            _periodFilterFrom = pval[0];
            _periodFilterTo = pval[1];

            if (_periodFilterFrom != "*") _periodFilterFrom = parseInt(_periodFilterFrom);
            else _periodFilterFrom = null;

            if (_periodFilterTo != "*") _periodFilterTo = parseInt(_periodFilterTo);
            else _periodFilterTo = null;
        }
    }

    refreshLines();
}
function setClientFilter() {
    _clientFilter = $('#ddlClient').val();
    refreshLines();
}
function setStatusFilter(newStatus) {
    _statusFilter = newStatus;
    refreshLines();
}
function refreshLines() {
    _dataTable.draw();
}

function checkField(val, str){
    return val?.toString().toLowerCase().includes(str.toLowerCase());
}
function containsString (obj, str) {
    if (checkField(obj[0], str)) return true;
    if (checkField(obj[1], str)) return true;
    if (checkField(obj[2], str)) return true;
    if (checkField(obj[3], str)) return true;
    if (checkField(obj[4], str)) return true;
    if (checkField(obj[5], str)) return true;

    return false;
}

function refreshLiveStats() {
    let countInProgress = 0;
    let countImpossible = 0;
    let countCompleted = 0;
    let countArchived = 0;

    let rowsId = _dataTable.rows().eq(0);
    for (let i = 0; i < rowsId.length; i++) {
        let row = _dataTable.row(rowsId[i]);
        let data = row.data();
        
        if (row.node().attributes["data-anystatusfilter"] != null && row.node().attributes["data-anystatusfilter"].value == "true") {
            if (row.node().attributes["data-statusname"] != null) {
                let rowStatus = row.node().attributes["data-statusname"].value;
                if (rowStatus != null) {
                    let matchesSearch = true;
                    let searchText = $('.dataTables_filter input').val();
                    if (searchText) matchesSearch = containsString(data, searchText); 
                    
                    switch (rowStatus) {
                        case "InProgress":
                            if (matchesSearch) countInProgress++;
                            break;
                        case "Impossible":
                            if (matchesSearch) countImpossible++;
                            break;
                        case "Completed":
                            if (matchesSearch) countCompleted++;
                            break;
                        case "Archived":
                            if (matchesSearch) countArchived++;
                            break;
                    }
                }
            }
        }
    }

    $('#ContentPlaceHolder1_lblCountInProgress').html(countInProgress);
    $('#ContentPlaceHolder1_lblCountImpossible').html(countImpossible);
    $('#ContentPlaceHolder1_lblCountCompleted').html(countCompleted);
    $('#ContentPlaceHolder1_lblCountArchived').html(countArchived);

}

function filterItem(settings, data, dataIndex) {
    let visible = true; 
    let searchText = $('.dataTables_filter input').val();
    if (searchText) {
        visible = containsString(data, searchText);
    }
    
    let ntr = settings.aoData[dataIndex].nTr; gg = ntr;
    if (_clientFilter != null) {
        let clientMatch = false;
        for (let i = 0; i < _clientFilter.length; i++) {
            if (_clientFilter[i] === ntr.getAttribute('data-clientId')) clientMatch = true;
        }
        if (clientMatch === false)
            visible = false;
    } 
    
    let attr = ntr.getAttribute('data-date');
    let date = parseInt(attr);

    if (_periodFilterFrom != null && _periodFilterTo != null) visible = date <= _periodFilterTo && date >= _periodFilterFrom;
    else if (_periodFilterFrom != null) visible = date >= _periodFilterFrom;
    else if (_periodFilterTo != null) visible = date <= _periodFilterTo;

    ntr.setAttribute('data-anystatusFilter', visible);
    if (_statusFilter != null) {
        if (ntr.getAttribute('data-statusName') !== _statusFilter)
            visible = false;
    }

    return visible;
}

$(function () {
    let f = getUrlParameter('f');
    if (f != null) _statusFilter = f;
    $.fn.dataTable.ext.search.push(filterItem);
    _dataTable = initDataTable($('#ContentPlaceHolder1_taskListGrid'), true);
    _dataTable.search('');
    _dataTable.on('draw', function () {
        refreshLiveStats();
    });
    setPeriodFilter();
});

new Vue({
    el: '#app',
    data: {
        statusFilter: 'InProgress'
    },
    methods: {
        setStatusFilter: function (e, f) {
            this.statusFilter = f;
            _statusFilter = f;
            _dataTable.draw();
            e && e.preventDefault();
        }
    }
});