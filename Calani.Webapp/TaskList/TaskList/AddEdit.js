Dropzone.autoDiscover = false
var canFinish = true;

function processTasklistAnswers(validateOnly = false){
    let answers = [];
    let validationError = false;
    
    $('.question-answer').each(function () {
        let id = $(this).data('id');
        let type = $(this).data('type');
        let isRequired = $(this)[0].outerHTML.includes('required="required"');
        let value;

        switch (type) {
            case 'Checkbox':
                value = $(this).find('input[type="checkbox"]').prop('checked') ? "1" : "0";
                break;
            case 'Yes/No':
                value = $(this).find('input[type="radio"]:checked').val() || null;
                break;
            case 'Date':
            case 'Text':
                value = $(this).val().trim() === "" ? null : $(this).val();
                break;
            case 'Option':
                let val = $(this).val();
                value = val 
                    ?  Array.isArray(val) 
                        ? val.join(", ") 
                        : val
                    : null;
                break;
            case 'AddPhoto':
                value = $(`#hiddenAddPhoto${id}`).val();
                break;
        }

        if (isRequired && value === null) {
            validationError = true;
        }
        answers.push({ Id: id, Value: value });
    });

    canFinish = !validationError;
    $(`#ContentPlaceHolder1_canBeFinished`).val(canFinish);
    $('#ContentPlaceHolder1_taskListHiddenAnswerData').val(JSON.stringify(answers)); 
    if (!validateOnly) eval($('#ContentPlaceHolder1_btnSaveQuestionsData').attr('href'));
}

function finishTasklistAnswers(){
    processTasklistAnswers(true);
    if (canFinish){
        eval($('#ContentPlaceHolder1_btnFinish').attr('href'));
    } else {
        showFinishSwal();
    }
}

function showFinishSwal() {
    swal({
        type: "warning",
        title: `${resources['YouHaveNotFilledInAllTheRequiredFields']}.`,
        html: `
            <span class='sweetalerttext'>${resources['YouСannotСompleteThisToDoList']}.</span>
            <br>
            <textarea id="swal-textarea-reason" class="form-control" style="display: none; width: 100%; padding: 5px; resize: none; margin-top: 20px;" rows="3" placeholder="${resources['ReasonThatPreventsEntry']}"></textarea>
            <div class="swal-custom-buttons">
                <button type="button" role="button" tabindex="0" class="SwalBtnComplete sweetalertbtn btn">${resources['CompleteTheForm']}</button>
                <button type="button" role="button" tabindex="0" class="SwalBtnReason sweetalertbtn btn">${resources['IndicateAReasonThatPreventsEntry']}</button>
            </div>
        `,
        showCancelButton: false,
        showConfirmButton: false,
        customClass: "wide-swal",
        onOpen: function () {

            $(".SwalBtnComplete").off().on("click", function () {
                swal.close();
            });

            $(".SwalBtnReason").off().on("click", function () {
                let textarea = $("#swal-textarea-reason");
                if (textarea.css('display') === 'none') {
                    textarea.show();
                    $(".SwalBtnReason").text(`${resources['SubmitReasonAndCloseTheForm']}`);
                } else {
                    let reason = textarea.val().trim();
                    if (reason) {
                        $('#ContentPlaceHolder1_cancelReason').val(reason);
                        eval($('#ContentPlaceHolder1_btnFinish').attr('href'));
                        swal.close();
                    }
                }
            });
        }
    });
}