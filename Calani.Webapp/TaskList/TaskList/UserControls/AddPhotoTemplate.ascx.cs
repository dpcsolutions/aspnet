﻿using System;
using System.Linq;
using System.Web.UI;
using Calani.BusinessObjects.Model;

namespace Calani.WebappNew.TaskList.TaskList.UserControls
{
    public partial class AddPhotoTemplate : UserControl
    {
        protected string Required { get; set; }
        protected long Id { get; set; }
        protected string FileIds { get; set; }
        
        public void SetData(questions data, long orgId, int number)
        {
            lblCaption.Attributes["data-id"] = $"lblCaption-{data.questionID}";
            lblCaption.Text = $"#{number}. {data.question}";
            lblDescription.Text = data.descriptions;
            lblDescription.Visible = !string.IsNullOrEmpty(data.descriptions);
            if (data.isMandatory ?? false) {
                lblCaption.Text += $" <span style='color: red;' title='{Resources.Resource.Required}'>*</span>";
            }

            Id = data.questionID;
            Required = data.isMandatory ?? false ? "required" : "";

            var answer = data.tasklistanswers.FirstOrDefault();
            if (answer != null && !string.IsNullOrEmpty(answer.answer))
            {
                var attmgr = new BusinessObjects.Attachments.AttachmentsManager(orgId);
                var ids = answer.answer.Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries).Select(long.Parse).ToList();
                var dsAttachments = attmgr.ListAttachmentsById(ids);
                
                FileIds = string.Join(",", dsAttachments.Select(x => x.id.ToString()).ToList());
                gridFilesAttachments.DataSource = dsAttachments;
                gridFilesAttachments.DataBind();
                new Html5GridView(gridFilesAttachments, false);
            }
        }
    }
}