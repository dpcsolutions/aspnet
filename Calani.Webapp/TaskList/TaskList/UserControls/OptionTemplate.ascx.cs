﻿using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Model;

namespace Calani.WebappNew.TaskList.TaskList.UserControls
{
    public partial class OptionTemplate : UserControl
    {
        public void SetData(questions data, int number)
        {
            lblCaption.Attributes["data-id"] = $"lblCaption-{data.questionID}";
            lblCaption.Text = $"#{number}. {data.question}";
            lblDescription.Text = data.descriptions;
            lblDescription.Visible = !string.IsNullOrEmpty(data.descriptions);
            if (data.isMandatory ?? false) {
                lblCaption.Text += $" <span style='color: red;' title='{Resources.Resource.Required}'>*</span>";
            }
            
            lbList.Attributes["data-id"] = data.questionID.ToString();
            lbList.Attributes["required"] = data.isMandatory ?? false ? "required" : "";
            lbList.Attributes["data-type"] = ResponseType.Option;

            var items = data.optionForPickList.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => new { Value = x.Trim(), Text = x.Trim() })
                .ToList();

            lbList.SelectionMode = data.possibleAnswer == "several" ? ListSelectionMode.Multiple : ListSelectionMode.Single;
            lbList.DataSource = items;
            lbList.DataBind();

            var answer = data.tasklistanswers.FirstOrDefault();
            if (answer != null && !string.IsNullOrEmpty(answer.answer))
            {
                if (data.possibleAnswer == "several")
                {
                    foreach (var value in answer.answer.Split(new[] { ", " }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        var item = lbList.Items.FindByValue(value);
                        if (item != null) {
                            item.Selected = true;
                        }
                    }
                }
                else
                {
                    lbList.SelectedValue = answer.answer;
                }
            }
        }
    }
}