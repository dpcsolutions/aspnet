﻿<%@ Control Language="C#" CodeBehind="~/TaskList/TaskList/UserControls/TextTemplate.ascx.cs" Inherits="Calani.WebappNew.TaskList.TaskList.UserControls.TextTemplate" %>

<div class="form-group">
    <div class="row col-lg-2" style="margin: 0 !important">
        <asp:Label runat="server" ID="lblCaption" CssClass="control-label"/>
        <asp:Label runat="server" ID="lblDescription" CssClass="control-label description col-sm-12"/>
    </div>
    <div class="col-lg-10">
    	<div class="input-group">
            <span class="input-group-addon"><i class="fa fa-font"></i></span>
            <asp:TextBox ID="tbxTextOption" runat="server" CssClass="form-control question-answer"/>
		</div>
    </div>
</div>