﻿<%@ Control Language="C#" CodeBehind="~/TaskList/TaskList/UserControls/AddPhotoTemplate.ascx.cs" Inherits="Calani.WebappNew.TaskList.TaskList.UserControls.AddPhotoTemplate" %>
<%@ Import Namespace="Calani.BusinessObjects.Enums" %>

<div class="form-group">
    <div class="row col-lg-2" style="margin: 0 !important">
        <asp:Label runat="server" ID="lblCaption" CssClass="control-label"/>
        <asp:Label runat="server" ID="lblDescription" CssClass="control-label description col-sm-12"/>
    </div>

    <div class="col-lg-10">

        <div class="input-group">
            <span class="input-group-addon">
                <i class="fa fa-image"></i>
            </span>
            <div class="tabbable">
                <ul class="nav nav-tabs nav-tabs-highlight" style="margin: 0 !important;">
                    <li class="active" style="border-top: 1px solid #ddd;">
                        <a href="#docstabAttachments<%= Id %>" data-toggle="tab"><%= Resources.Resource.Attachments %></a>
                    </li>
                    <li style="border-top: 1px solid #ddd; border-right: 1px solid #ddd;">
                        <a href="#docstabNew<%= Id %>" data-toggle="tab"><%= Resources.Resource.Add_new %></a>
                    </li>
                </ul>
                <div class="tab-content form-control" style="padding: 0 !important; height: unset !important">
                    <div class="tab-pane active" id="docstabAttachments<%= Id %>">
                        <asp:GridView runat="server" ID="gridFilesAttachments" AutoGenerateColumns="False" ShowHeader="False">
                            <Columns>
                                <asp:TemplateField HeaderText="Name">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hpl" runat="server" NavigateUrl='<%# "../../../DownloadDocument.ashx?id=" + Eval("id") + "&orgId=" + Eval("organizationID") + "&t=" + Eval("FileNameNoExtension") %> '>
                                            <asp:Label ID="ico" runat="server" CssClass='<%# Bind("Icon") %>'></asp:Label>
                                            &nbsp;
                                            <asp:Label ID="lbl" runat="server" Text='<%# Bind("NameWithExtension") %>'></asp:Label>
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="date" HeaderText="Date"/>
                                <asp:TemplateField ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:LinkButton id="lnk" runat="server" href="javascript:void(0)" class="btnDeleteAttachment" data-attachment='<%#  Eval("id") %>' data-questionid='<%# Id %>' data-popup="tooltip" title='<%$ Resources:Resource, Delete %>'>
                                            <asp:Label id="ico" runat="server" CssClass='icon-trash text-danger'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="tab-pane" id="docstabNew<%= Id %>">
                        <input type="hidden" id="hiddenAddPhoto<%= Id %>" value="<%= FileIds %>">
                        <div id="drpAddPhoto<%= Id %>" data-id="<%= Id %>" data-type="<%= ResponseType.AddPhoto %>" class="fallback dropzone dz-clickable question-answer" <%= Required %>></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
	$(function () {
        $(`#drpAddPhoto<%= Id %>`).dropzone({
            uploadMultiple: true,
            url: "UserControls/Upload.ashx",
            dictDefaultMessage: '<%= Resources.Resource.DropFilesToUpload %> <span><%= Resources.Resource.OrClick %></span>',
            maxFilesize: 30,
            init: function () {
                this.on("complete", function (file) {
                    if (file.status === "success") {
                        let $selector = $('#hiddenAddPhoto<%= Id %>');
                        let val = $selector.val();
                        if (val !== ''){
                            val = val + ',' + file.xhr.responseText;
                            $selector.val([...new Set(val.split(","))].join(","));
                        } else {
                            $selector.val(file.xhr.responseText)
                        }
                    }
                });
            }
        });
    });
    
    $('.btnDeleteAttachment').click(function () {
        let btn = $(this);
        let tr = btn.closest('tr');
        let id = +btn.data('attachment');
        let questionid = +btn.data('questionid');
        
        let $input = $(`#hiddenAddPhoto${questionid}`);
        let values = $input.val().split(',').map(v => v.trim());
        values = values.filter(v => v !== id.toString());
        $input.val(values.join(','));
        
        tr.hide();
    });

</script>