﻿<%@ Control Language="C#" CodeBehind="~/TaskList/TaskList/UserControls/DateTemplate.ascx.cs" Inherits="Calani.WebappNew.TaskList.TaskList.UserControls.DateTemplate" %>
<%@ Import Namespace="Calani.BusinessObjects.Enums" %>

<div class="form-group">
    <div class="row col-lg-2" style="margin: 0 !important">
        <asp:Label runat="server" ID="lblCaption" CssClass="control-label"/>
        <asp:Label runat="server" ID="lblDescription" CssClass="control-label description col-sm-12"/>
    </div>
    <div class="col-lg-10">
        <div class="input-group">
            <span class="input-group-addon">
                <i class="fa fa-calendar-alt"></i>
            </span>
            <input type="text" id="dpcDateOption<%= Id %>" data-id="<%= Id %>" data-type="<%= ResponseType.Date %>" <%= Required %>
                   <% if (!string.IsNullOrEmpty(DateVal)) { %> value="<%= DateVal %>" <% } %>
                   class="form-control daterange-single question-answer"/>
            <span onclick="$('#dpcDateOption<%= Id %>').val(null);" class="input-group-addon">
                <i class="fa fa-eraser"></i>
            </span>
        </div>
    </div>
</div>