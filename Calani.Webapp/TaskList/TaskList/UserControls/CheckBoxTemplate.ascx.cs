﻿
using System.Web.UI;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Model;

namespace Calani.WebappNew.TaskList.TaskList.UserControls
{
    public partial class CheckBoxTemplate : UserControl
    {
        public void SetData(questions data, int number)
        {
            lblCaption.Attributes["data-id"] = $"lblCaption-{data.questionID}";
            lblCaption.Text = $"#{number}. {data.question}";
            lblDescription.Text = data.descriptions;
            lblDescription.Visible = !string.IsNullOrEmpty(data.descriptions);
            if (data.isMandatory ?? false) {
                lblCaption.Text += $" <span style='color: red;' title='{Resources.Resource.Required}'>*</span>";
            }
        
            chkYesNo.Attributes["data-id"] = data.questionID.ToString();
            chkYesNo.Attributes["required"] = data.isMandatory ?? false ? "required" : "";
            chkYesNo.Attributes["data-type"] = ResponseType.Checkbox;
        }
    }
}