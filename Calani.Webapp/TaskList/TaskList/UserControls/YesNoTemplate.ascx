﻿<%@ Control Language="C#" CodeBehind="~/TaskList/TaskList/UserControls/YesNoTemplate.ascx.cs" Inherits="Calani.WebappNew.TaskList.TaskList.UserControls.YesNoTemplate" %>

<div class="form-group">
    <div class="row col-lg-2" style="margin: 0 !important">
        <asp:Label runat="server" ID="lblCaption" CssClass="control-label"/>
        <asp:Label runat="server" ID="lblDescription" CssClass="control-label description col-sm-12"/>
    </div>
    <div class="col-lg-10">
    	<div class="input-group">
            <span class="input-group-addon"><i class="fa fa-question-circle"></i></span>
		    <asp:RadioButtonList runat="server" ID="rblYesNo" CssClass="form-control question-answer" RepeatDirection="Horizontal">
                <asp:ListItem Value="1"> Yes </asp:ListItem>
                <asp:ListItem Value="0"> No </asp:ListItem>
            </asp:RadioButtonList>
		</div>
    </div>
</div>

