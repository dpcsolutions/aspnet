﻿<%@ WebHandler Language="C#" Class="Upload" %>

using System.Web;
using Calani.BusinessObjects.Attachments;

public class Upload : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        var mgr = new AttachmentsManager(PageHelper.GetCurrentOrganizationId(context.Session));
        string result = "";
        foreach (string s in context.Request.Files)
        {
            var file = context.Request.Files[s];
            if (!string.IsNullOrEmpty(file.FileName))
            {
                FileTypeDefinition ftype = null;
                var name = mgr.UploadAttachmentTemp(null, null, file, null, ref ftype);
                var id = mgr.UploadFileAttachmentToDb(name, jobId: null, userId: PageHelper.GetUserId(context.Session), ftype: null);
                if (result == "") result = id.ToString();
                else result = result + "," + id;
            }
        }
        context.Response.ContentType = "text/plain";
        context.Response.Write(result);
    }

    public bool IsReusable
    {
        get { return false; }
    }
}
