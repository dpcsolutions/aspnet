﻿<%@ Control Language="C#" CodeBehind="~/TaskList/TaskList/UserControls/OptionTemplate.ascx.cs" Inherits="Calani.WebappNew.TaskList.TaskList.UserControls.OptionTemplate" %>

<div class="form-group">
    <div class="row col-lg-2" style="margin: 0 !important">
        <asp:Label runat="server" ID="lblCaption" CssClass="control-label"/>
        <asp:Label runat="server" ID="lblDescription" CssClass="control-label description col-sm-12"/>
    </div>
    <div class="col-lg-10">
        <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-list"></i></span>
            <asp:ListBox runat="server" CssClass="form-control select question-answer" ID="lbList"
                DataValueField="Value" DataTextField="Text"/>
        </div>
    </div>
</div>