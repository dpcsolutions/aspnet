﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.TaskList;               
using System.Collections.Generic;
using System.Linq;

namespace Calani.WebappNew.TaskList.TaskList
{
    public partial class Default : Page
    {
        public PageHelper ph;

        public string OptionsClients { get; set; }
        public string OptionsPeriod { get; set; }
        
        protected override void InitializeCulture()
        {
            ph = new PageHelper(this);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            EnableViewState = false;
            if (!IsPostBack)
            {
                var orgMgr = new OrganizationManager(ph.CurrentOrganizationId);
                orgMgr.Load();
                
                OptionsClients = ph.RenderCustomerListOption();
                OptionsPeriod = ph.RenderPeriodsListOption(preselect: orgMgr.DefaultPeriodFilter);

                taskListGrid.Columns[0].HeaderText = Resources.Resource.Listname;
                taskListGrid.Columns[1].HeaderText = Resources.Resource.Project;
                taskListGrid.Columns[2].HeaderText = Resources.Resource.EnteredBy;
                taskListGrid.Columns[3].HeaderText = Resources.Resource.Date;
                taskListGrid.Columns[4].HeaderText = Resources.Resource.Status; 
                taskListGrid.Columns[5].HeaderText = Resources.Resource.Comment; 
                
                var taskListManager = new TaskListManager(ph.CurrentOrganizationId);
                var list = taskListManager.ListTaskLists();
                if (ph.CurrentUserType != Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
                {
                    list = list.Where(x => x.contactId == ph.CurrentUserId).ToList();
                }
                taskListGrid.DataSource = list;
                var rowData = new List<string>{ "internalId", "project", "employee", "date", "statusName", "clientid", "cancelReason" };
                new Html5GridView(taskListGrid, false, rowDataTags: rowData, defaultSort: "3;desc");
            }
        }
        
        protected void taskListGrid_DataBound(object sender, EventArgs e)
        {
            foreach (GridViewRow row in taskListGrid.Rows)
            {
                row.Cells[4].Text = SharedResource.Resource.GetString($"{row.Cells[4].Text}");
            }
        }
    }
}