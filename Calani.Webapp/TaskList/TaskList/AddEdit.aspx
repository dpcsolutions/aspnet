﻿<%@ Page Title="Add a new task list" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Calani.WebappNew.TaskList.TaskList.AddEdit" Codebehind="~/TaskList/TaskList/AddEdit.aspx.cs" %>
<%@ Import Namespace="Calani.BusinessObjects.TaskList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<form runat="server">
<div class="content" style="padding: 0 20px 10px 20px">
    <div class="row" style="padding: 0 10px 0 10px">
        <div class="col-12">

            <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
                <div class="panel-heading">
                    <h6 class="panel-title"><%= Resources.Resource.Validation_Error %></h6>
                </div>
                <div class="panel-body">
                    <asp:Label runat="server" ID="lblErrorMessage"/>
                </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="panelAlert" Visible="false" CssClass="panel panel-bordered ">
                <div class="panel-heading">
                    <asp:Label runat="server" ID="panelAlertCaption"/>
                </div>

                <div class="panel-body">
                    <asp:Label runat="server" ID="panelAlertLbl"/>
                    <br/>
                    <strong>
                        <a href="." id="panelAlertLblLinkBack" runat="server"></a>
                    </strong>
                    <%= Resources.Resource.or %>
                    <a href="AddEdit.aspx" id="panelAlertLblLinkNew" runat="server"></a>.
                </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
                <div class="panel-heading">
                    <h6 class="panel-title"><%= Resources.Resource.Validation_Done %></h6>
                </div>

                <div class="panel-body">
                    <asp:Label runat="server" ID="lblSuccess"/>
                    <br/>
                    <strong>
                        <a href="." id="linkBackList" runat="server"></a>
                    </strong>
                    <%= Resources.Resource.or %>
                    <a href="AddEdit.aspx" id="linkNew" runat="server"></a>.
                </div>
            </asp:Panel>

            <asp:Panel runat="server" ID="mainProperties" class="panel panel-white">
                <div class="panel-heading">
                    <h6 class="panel-title">
                        <asp:Label runat="server" ID="lblTitle"/>
                    </h6>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal">

                        <fieldset>
                            <div class="form-group">
                                <label class="control-label col-lg-2"><%= Resources.Resource.Name %></label>
                                <div class="col-lg-10">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-font"></i>
                                        </span>
                                        <asp:TextBox ID="tbxListName" runat="server" CssClass="form-control"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2"><%= Resources.Resource.Customer %></label>
                                <div class="col-lg-10">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa-solid fa-user-tag"></i>
                                        </span>
                                        <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlCustomers" AutoPostBack="true"
                                                          OnSelectedIndexChanged="ddlCustomer_SelectedIndexChanged"
                                                          DataValueField="id" DataTextField="companyName"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-lg-2"><%= Resources.Resource.Project %></label>
                                <div class="col-lg-10">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="icon-cog5"></i>
                                        </span>
                                        <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlProject" AutoPostBack="true"
                                                          OnSelectedIndexChanged="ddlProject_SelectedIndexChanged" DataValueField="id" DataTextField="internalId"/>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" id="panel_val">
                                <label class="control-label col-lg-2"><%= Resources.Resource.TaskList %></label>
                                <div class="col-lg-10">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="icon-list3"></i>
                                        </span>
                                        <asp:ListBox runat="server" CssClass="form-control select" ID="ddlListTemplates"
                                                     DataValueField="templateId" DataTextField="listName" SelectionMode="Multiple"/>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <% if (CurrentStatus == (int) TaskListStatus.inProgress)
                           { %>

                            <div class="buttonsaction text-right">
                                <label class="control-label col-lg-2"></label>
                                <div class="col-lg-10">
                                    <div class="btn-group">
                                        <asp:LinkButton runat="server" ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click">
                                            <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                        </asp:LinkButton>

                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right" runat="server" id="btnMoreDdl">
                                            <li>
                                                <a href="javascript:void(0)" data-toggle="modal" data-target="#modal_default">
                                                    <i class="fa fa-trash-o"></i> <%= Resources.Resource.Delete %>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div id="modal_default" class="modal fade">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
                                        </div>

                                        <div class="modal-body">
                                            <h6 class="text-semibold"><%= Resources.Resource.Are_You_Sure_To_Delete %></h6>
                                            <p><%= Resources.Resource.Delete_Is_Irreversible %></p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
                                            <asp:Button runat="server" ID="btnDelete" OnClick="btnDelete_Click" CssClass="btn btn-primary" Text="<%$ Resources:Resource, Yes_Delete %>"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <% } %>
                    </div>
                </div>
            </asp:Panel>
        </div>
    </div>
</div>

<% if (ph.CurrentId > 0)
   { %>
    <div class="content">
        <div class="row" style="padding: 0 10px 0 10px">
            <div class="col-12">
                <asp:Panel runat="server" ID="formDataContainer"></asp:Panel>

                <% if (CurrentStatus == (int) TaskListStatus.completed || CurrentStatus == (int) TaskListStatus.impossible)
                   { %>
                    <asp:Panel runat="server" ID="archivatePanel" class="panel panel-white">
                        <div class="panel-body">
                            <div class="form-horizontal">
                                <div class="row" style="text-align: right; margin-right: 0;">
                                    <div class="btn-group">
                                        <asp:LinkButton runat="server" ID="btnArchivate" class="btn btn-primary" OnClick="btnArchivate_Click">
                                            <i class="icon-inbox position-left"></i> <%= Resources.Resource.Archive %>
                                        </asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                <% } %>


                <% if (CurrentStatus == (int) TaskListStatus.inProgress)
                   { %>
                    <asp:Panel runat="server" ID="questionButtons" class="panel panel-white">
                        <div class="panel-body">
                            <div class="form-horizontal">

                                <div class="row">
                                    <div class="col-lg-10" style="text-align: center; display: flex; justify-content: center;">
                                        <div class="row" style="margin: 0 !important; padding-right: 20px"><%= Resources.Resource.IDidEverythingIWasAskedToDo %></div>
                                        <div>
                                            <asp:CheckBox runat="server" ID="iDidEverythingCbx"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-2" style="text-align: right; margin-right: 0;">
                                        <div class="btn-group">
                                            <a id="btnProcessTasklistAnswers" class="btn btn-primary" onclick="processTasklistAnswers()">
                                                <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                            </a>

                                            <asp:LinkButton runat="server" Style="display: none;" ID="btnSaveQuestionsData" OnClick="btnSaveQuestionsData_Click">
                                                <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                            </asp:LinkButton>
                                        </div>

                                        <div class="btn-group">
                                            <a id="btnValidateTasklistAnswers" class="btn btn-success" onclick="finishTasklistAnswers()">
                                                <i class="fa fa-flag-checkered"></i> <%= Resources.Resource.Finish %>
                                            </a>

                                            <asp:LinkButton runat="server" Style="display: none;" ID="btnFinish" OnClick="btnFinish_Click">
                                                <i class="fa fa-flag-checkered"></i> <%= Resources.Resource.Finish %>
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <asp:HiddenField runat="server" ID="taskListHiddenAnswerData"/>
                                <asp:HiddenField runat="server" ID="canBeFinished"/>
                                <asp:HiddenField runat="server" ID="cancelReason"/>
                            </div>
                        </div>
                    </asp:Panel>
                <% } %>

            </div>
        </div>
    </div>
<% } %>
</form>

<% if (CurrentStatus != (int) TaskListStatus.inProgress)
   { %>
    <style>
        input, textarea, select, button, [contenteditable], .select2-selection, label, .input-group-addon, .btnDeleteAttachment {
            pointer-events: none;
            user-select: none;
        }
        .select2-selection__choice__remove{
            display: none;
        }
    </style>
<% } %>


<script src="../TaskList/addEdit.js"></script>
<link href="../TaskList/addEdit.css" rel="stylesheet">
<script >
        var resources = {};
        resources['Validation_Error'] = "<%= Resources.Resource.Validation_Error %>";
        resources['Close'] = "<%= Resources.Resource.Close %>";
        resources['FillAllRequiredFields'] = "<%= Resources.Resource.FillAllRequiredFields %>";
        
        resources['CompleteTheForm'] = "<%= Resources.Resource.CompleteTheForm %>";
        resources['IndicateAReasonThatPreventsEntry'] = "<%= Resources.Resource.IndicateAReasonThatPreventsEntry %>";
        resources['YouСannotСompleteThisToDoList'] = "<%= Resources.Resource.YouСannotСompleteThisToDoList %>";
        resources['SubmitReasonAndCloseTheForm'] = "<%= Resources.Resource.SubmitReasonAndCloseTheForm %>";
        resources['ReasonThatPreventsEntry'] = "<%= Resources.Resource.ReasonThatPreventsEntry %>";
        resources['YouHaveNotFilledInAllTheRequiredFields'] = "<%= Resources.Resource.YouHaveNotFilledInAllTheRequiredFields %>";
    </script>
</asp:Content>