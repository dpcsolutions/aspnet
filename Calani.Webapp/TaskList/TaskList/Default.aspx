﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Calani.WebappNew.TaskList.TaskList.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form runat="server">
        <div class="content" id="app">
            <div class="row">
                <div class="col-sm-4 col-md-4">
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row text-center">
                                <div class="col-xs-3" v-bind:class="{ 'bordered alpha-primary': statusFilter=='InProgress' }" data-popup="tooltip" data-container="body" title="<%= Resources.Resource.InProgress %>">
                                    <a href="" v-on:click="setStatusFilter($event, 'InProgress')">
                                        <p>
                                            <i class="icon-play4 icon-2x display-inline-block tada infinite text-primary" id="icoCountInProgress" runat="server"></i>
                                        </p>
                                        <h5 class="text-semibold no-margin text-primary" id="lblCountInProgress" runat="server">.</h5>
                                        <p>
                                            <strong class="text-muted text-size-small"><%= Resources.Resource.InProgress %></strong>
                                        </p>
                                    </a>

                                </div>
                                <div class="col-xs-3" v-bind:class="{ 'bordered alpha-primary': statusFilter=='Impossible' }" data-popup="tooltip" data-container="body" title="<%= Resources.Resource.Impossible %>">
                                    <a href="" v-on:click="setStatusFilter($event, 'Impossible')" class="text-orange">
                                        <p> 
                                            <i class="icon-notification2 icon-2x display-inline-block tada infinite text-danger " id="icoCountImpossible" runat="server"></i>
                                        </p>
                                        <h5 class="text-semibold no-margin text-danger" id="lblCountImpossible" runat="server">.</h5>
                                        <p>
                                            <strong class="text-muted text-size-small"><%= Resources.Resource.Impossible %></strong>
                                        </p>
                                    </a>
                                </div>
                                <div class="col-xs-3" v-bind:class="{ 'bordered alpha-primary': statusFilter=='Completed' }" data-popup="tooltip" data-container="body" title="<%= Resources.Resource.Completed %>">
                                    <a href="" v-on:click="setStatusFilter($event, 'Completed')" class="text-muted">
                                        <p>
                                            <i class="icon-finish icon-2x display-inline-block tada infinite text-success" id="icoCountCompleted" runat="server"></i>
                                        </p>
                                        <h5 class="text-semibold no-margin text-success" id="lblCountCompleted" runat="server">.</h5>
                                        <p>
                                            <strong class="text-muted text-size-small"><%= Resources.Resource.Completed %></strong>
                                        </p>
                                    </a>
                                </div>
                                <div class="col-xs-3" v-bind:class="{ 'bordered alpha-primary': statusFilter=='Archived' }" data-popup="tooltip" data-container="body" title="<%= Resources.Resource.Archived %>">
                                    <a href="" v-on:click="setStatusFilter($event, 'Archived')" class="text-muted" id="text-secondary">
                                        <p>
                                            <i class="icon-inbox icon-2x display-inline-block tada infinite text-secondary" id="icoCountArchived" runat="server"></i>
                                        </p>
                                        <h5 class="text-semibold no-margin text-secondary" id="lblCountArchived" runat="server">.</h5>
                                        <p>
                                            <strong class="text-muted text-size-small"><%= Resources.Resource.Archived %></strong>
                                        </p>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer text-center no-padding">
                            <div class="row">
                                <a href="" v-on:click="setStatusFilter($event, null)" class="display-block p-10 text-default"
                                   data-popup="tooltip" data-container="body" title="<%= Resources.Resource.Show_all %>">
                                    <i class="fa fa-ellipsis-h"></i> <%= Resources.Resource.Show_all %>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-5 col-md-5"></div>

                <div class="col-sm-3 col-md-3">
                    <div class="panel panel-body panel-body-accent">
                        <div class="media no-margin">
                            <div>
                                <i class="fa fa-calendar-check-o" aria-hidden="true"></i> <%= Resources.Resource.Select_Period %>
                            </div>
                            <div>
                                <select id="ddlPeriod" class="select" onchange="setPeriodFilter()">
                                    <%= OptionsPeriod %>
                                </select>
                            </div>

                            <div style="margin-top:20px">
                                <i class="fa fa-user-circle-o" aria-hidden="true"></i> <%= Resources.Resource.Customers %>
                            </div>
                            <div>
                                <select id="ddlClient" data-placeholder="Select..." multiple="multiple" class="select" onchange="setClientFilter()">
                                    <%= OptionsClients %>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-white">
                <div class="panel-body">
                    <asp:GridView runat="server" ID="taskListGrid" AutoGenerateColumns="false" OnDataBound="taskListGrid_DataBound">
                        <Columns>
                            <asp:HyperLinkField DataTextField="internalId" DataNavigateUrlFields="id" DataNavigateUrlFormatString="AddEdit.aspx?id={0}"/>
                            <asp:TemplateField>
                                <ItemTemplate><%# Eval("jobs.internalId") %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField><ItemTemplate><%# Eval("contacts.firstName") + " " + Eval("contacts.lastName") %></ItemTemplate></asp:TemplateField>
                            <asp:BoundField DataField="date"/>
                            <asp:BoundField DataField="statusName"/>
                            <asp:BoundField DataField="cancelReason"/>
                        </Columns>
                    </asp:GridView>
                </div><!-- panel-body -->
                <div class="panel-footer text-right no-padding">
                    <div class="row" style="margin:10px">
                        <a href="~/TaskList/TaskList/AddEdit.aspx" class="btn btn-default" runat="server" ID="addNewListTemplate">
                            <i class="icon-plus2 position-left"></i>
                            <%= Resources.Resource.Add_new %>
                        </a>
                    </div>
                </div><!-- panel-footer -->
            </div>
        </div><!-- /content -->
    </form>
    <script type="text/javascript" src="Default.js?<%= Guid.NewGuid().ToString("n") %>"></script>
    <style>
        #text-secondary,
        .text-secondary,
        .text-secondary:hover,
        .text-secondary:focus {
          color: #999999 !important;
        }
    </style>
</asp:Content>