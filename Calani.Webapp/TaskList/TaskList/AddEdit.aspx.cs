﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Web.UI;
using System.Linq;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Projects;
using Calani.BusinessObjects.TaskList;
using Calani.BusinessObjects.Template;
using Calani.WebappNew.TaskList.TaskList.UserControls;
using Newtonsoft.Json;

namespace Calani.WebappNew.TaskList.TaskList
{
    public partial class AddEdit : Page
    {
        public PageHelper ph;
        private TaskListManager _manager;
        private TemplateManager _templateManager;
        private QuestionManager _questionManager;
        public int CurrentStatus { get; set; }

        protected override void InitializeCulture()
        {
            ph = new PageHelper(this);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            CurrentStatus = (int)TaskListStatus.inProgress;
            ph.ItemLabel = Resources.Resource.TaskList;
            ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
                btnDelete, btnMore, btnMoreDdl, lblTitle, linkBackList, linkNew);
            ph.ItemLabel = ph.CurrentId > 0 ? Resources.Resource.EditTaskList : Resources.Resource.CreateTaskList;

            _templateManager = new TemplateManager(ph.CurrentOrganizationId);
            _questionManager = new QuestionManager(ph.CurrentOrganizationId);
            _manager = new TaskListManager(ph.CurrentOrganizationId);
            btnSaveQuestionsData.Attributes["data-uniqueid"] = btnSaveQuestionsData.UniqueID;
            panelSuccess.Visible = false;

            tasklists model = null;
            if (ph.CurrentId > 0)
            {
                model = _manager.GetWithTemplates(ph.CurrentId ?? 0);
                CurrentStatus = model.listStatus;
                foreach (var taskliststemplate in model.taskliststemplates)
                {
                    var questions = _questionManager
                        .GetByTemplateId(taskliststemplate.templateId, model.id);
                    CreateQuestions(questions);
                }
            }

            if (!IsPostBack)
            {
                if (Request.QueryString["taskAlert"] == "true")
                    ph.DisplaySuccess(Resources.Resource.TaskListInfoSavedSuccessfully);
                if (Request.QueryString["questionAlert"] == "true")
                    ph.DisplaySuccess(Resources.Resource.QuestionResultsSavedSuccessfully);
                if (Request.QueryString["imppossibleAlert"] == "true")
                    DisplayAlert(Resources.Resource.ChangesSaved, Resources.Resource.TaskListImpossible, "panel-warning");
                if (Request.QueryString["finishAlert"] == "true")
                    DisplayAlert(Resources.Resource.ChangesSaved, Resources.Resource.TaskListFinished, "panel-success");
                if (Request.QueryString["archiveAlert"] == "true")
                    DisplayAlert(Resources.Resource.Archived, Resources.Resource.Archived, "panel-success");
                
                var customerMgr = new ClientsManager(ph.CurrentOrganizationId);
                var projectMgr = new ProjectsManager(ph.CurrentOrganizationId);
                var org = new OrganizationManager(ph.CurrentOrganizationId);
                org.Load();

                List<clients> customers;
                if (org.EmployeeCanViewAllOpenProjects || ph.CurrentUserType == BusinessObjects.Contacts.ContactTypeEnum.User_Admin){
                    customers = customerMgr.GetPublicQuery().Query.ToList();    
                }
                else {
                    customers = projectMgr.GetPublicQuery().Query
                        .Where(x => 
                            x.projectemployees.Any(z => z.employeeId == ph.CurrentUserId)
                            || x.visits.Any(z => z.contacts.Any(y => y.id == ph.CurrentUserId)))
                        .Select(x => x.clients).Distinct()
                        .ToList();
                }
                
                long projectId = 0;
                if (!string.IsNullOrEmpty(Request.QueryString["projectId"]))
                {
                    long.TryParse(Request.QueryString["projectId"], out projectId);
                }

                ddlCustomers.DataSource = customers;
                ddlCustomers.DataBind();

                if (ph.CurrentId is null && model is null)
                {
                    tbxListName.Text = $"{Resources.Resource.TaskList} #{_manager.GetLastIndex() + 1}";
                    lblTitle.Text = Resources.Resource.CreateTaskList;
                    if (projectId > 0)
                    {
                        var project = projectMgr.Get(projectId);
                        ddlCustomers.SelectedValue = project.clientId.ToString();
                        SetDropDownsFromCustomer(false);
                        ddlProject.SelectedValue = projectId.ToString();
                        SetDropDownsFromProject();
                    }
                }
                else
                {
                    lblTitle.Text = Resources.Resource.EditTaskList;
                    iDidEverythingCbx.Checked = model.isCompleted;
                    tbxListName.Text = model.internalId;
                    ddlCustomers.SelectedValue = model.clientid.ToString();
                    SetDropDownsFromCustomer(false);
                    ddlProject.SelectedValue = model.jobId.ToString();
                    SetDropDownsFromProject();
                    foreach (ListItem item in ddlListTemplates.Items)
                    {
                        item.Selected = model.taskliststemplates
                            .Select(x => x.templateId).Contains(long.Parse(item.Value));
                    }
                }
            }
        }
        
        public void DisplayAlert(string caption, string lLbl, string type)
        {
            panelAlert.Visible = true;
            panelAlert.CssClass += type;
            lLbl = ResourceHelper.Get(lLbl);
            caption = ResourceHelper.Get(caption);
            panelAlertLbl.Text = lLbl;
            panelAlertCaption.Text = caption;
            panelAlertLblLinkBack.InnerText = ph.ParseLabelPlural(Resources.Resource.Back_to_var_list);
            panelAlertLblLinkNew.InnerText = ph.ParseLabelPlural(Resources.Resource.Create_another_var);
        }
        
        private void SetDropDownsFromCustomer(bool setProject = true)
        {
            if (int.TryParse(ddlCustomers.SelectedValue, out var customerId))
            {
                var projectMgr = new ProjectsManager(ph.CurrentOrganizationId);
                var projects = projectMgr.GetPublicQuery().Query
                    .Where(x => x.clients.id == customerId && x.jobs_templates.Any()).ToList();

                ddlProject.DataSource = projects;
                ddlProject.DataBind();
                ddlListTemplates.DataSource = new List<templates>();
                ddlListTemplates.DataBind();
                if (setProject) SetDropDownsFromProject();
            }
        }
        
        private void SetDropDownsFromProject()
        {
            if (int.TryParse(ddlProject.SelectedValue, out var projectId))
            {
                var listTemplates = _templateManager.ListTemplates()
                    .Where(x => x.jobs_templates.Any(y => y.jobId == projectId)).ToList();
                ddlListTemplates.DataSource = listTemplates;
                ddlListTemplates.DataBind();
            }
        }

        protected void ddlProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetDropDownsFromProject();
        }

        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetDropDownsFromCustomer();
        }

        #region Buttons

        protected void btnSave_Click(object sender, EventArgs e)
        {
            CreateOrUpdate();
        }
        
        protected void btnArchivate_Click(object sender, EventArgs e)
        {
            var model = _manager.Get(ph.CurrentId ?? 0);
            model.listStatus = (int) TaskListStatus.archived;
            _manager.SaveOrUpdate(model.id, model);
            Response.Redirect($"{Request.Url.AbsolutePath}?id={ph.CurrentId}&archiveAlert=true");
        }

        private void saveQuestionsDate()
        {
            var data = JsonConvert.DeserializeObject<List<TaskListAnswerModel>>(taskListHiddenAnswerData.Value);
            _manager.SaveOrUpdateTasklistanswers(ph.CurrentId ?? 0, data);

            var model = _manager.Get(ph.CurrentId ?? 0);
            model.isCompleted = iDidEverythingCbx.Checked;
            _manager.SaveOrUpdate(model.id, model);
        }

        protected void btnSaveQuestionsData_Click(object sender, EventArgs e)
        {
            saveQuestionsDate();
            Response.Redirect($"{Request.Url.AbsolutePath}?id={ph.CurrentId}&questionAlert=true");
        }

        protected void btnFinish_Click(object sender, EventArgs e)
        {
            saveQuestionsDate();
            var finished = canBeFinished.Value == "true";
            var model = _manager.Get(ph.CurrentId ?? 0);
            model.listStatus = finished ? (int) TaskListStatus.completed : (int) TaskListStatus.impossible;
            model.cancelReason = cancelReason.Value;

            _manager.SaveOrUpdate(model.id, model);
            Response.Redirect(
                $"{Request.Url.AbsolutePath}?id={ph.CurrentId}&{(finished ? "finishAlert" : "imppossibleAlert")}=true");
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (ph.CurrentId != null) Delete();
        }

        #endregion

        #region CRUD
        
        private void CreateOrUpdate()
        {
            var selectedTemplateIds = ddlListTemplates.Items.Cast<ListItem>().Where(i => i.Selected)
                .Select(i => long.Parse(i.Value)).ToList();

            if (string.IsNullOrWhiteSpace(tbxListName.Text) || !selectedTemplateIds.Any())
            {
                ph.DisplayError(Resources.Resource.Validation_Error);
                return;
            }

            var model = new tasklists();
            var clientId = long.Parse(ddlCustomers.SelectedValue);
            var jobId = long.Parse(ddlProject.SelectedValue);

            model.internalId = tbxListName.Text;
            model.jobId = jobId;
            model.isCompleted = iDidEverythingCbx.Checked;
            model.clientid = clientId;
            model.contactId = ph.CurrentUserId;
            model.date = DateTime.Now;
            model.id = ph.CurrentId ?? 0;

            var ret = _manager.SaveOrUpdate(model.id, model);
            if (ret.Success)
            {
                _manager.SaveOrUpdateTemplates(ret.Record.id, selectedTemplateIds);

                btnSave.Enabled = false;
                btnMore.Visible = false;

                Response.Redirect($"{Request.Url.AbsolutePath}?id={ret.Record.id}&taskAlert=true");
            }
            else
            {
                ph.DisplayError(ret.ErrorMessage);
            }
        }

        private void Delete()
        {
            var ret = _manager.Delete(ph.CurrentId ?? 0);
            if (ret.Success)
            {
                ph.DisplaySuccess(Resources.Resource.Deleted_var);
            }
            else ph.DisplayError(ret.ErrorMessage);
        }

        #endregion

        #region Questions

        private void CreateQuestions(List<questions> questions)
        {
            var template = questions.First().templates;
            var panel = new Panel{CssClass = "panel panel-white"};
            var heading = new Literal {
                Text = $"<div class='panel-heading'><h6 class='panel-title'><span>{Resources.Resource.Forms} - {template.listName}</span></h6></div>"
            };
            panel.Controls.Add(heading);

            var panelBody = new Panel { CssClass = "panel-body" };
            var formHorizontal = new Panel { CssClass = "form-horizontal" };
            var formGroup = new Panel { CssClass = "form-group", ID = $"taskListBlockDiv{template.templateId}" };
            var taskListBlock = new PlaceHolder { ID = $"taskListBlock{template.templateId}", EnableViewState = true };
            
            UserControl control = null;
            var i = 1;
            foreach (var question in questions)
            {
                switch (question.responseType)
                {
                    case ResponseType.Option:
                        control = (UserControl) LoadControl("~/TaskList/TaskList/UserControls/OptionTemplate.ascx");
                        ((OptionTemplate) control).SetData(question, i++);
                        break;
                    case ResponseType.YesNo:
                        control = (UserControl) LoadControl("~/TaskList/TaskList/UserControls/YesNoTemplate.ascx");
                        ((YesNoTemplate) control).SetData(question, i++);
                        break;
                    case ResponseType.Date:
                        control = (UserControl) LoadControl("~/TaskList/TaskList/UserControls/DateTemplate.ascx");
                        ((DateTemplate) control).SetData(question, i++);
                        break;
                    case ResponseType.Text:
                        control = (UserControl) LoadControl("~/TaskList/TaskList/UserControls/TextTemplate.ascx");
                        ((TextTemplate) control).SetData(question, i++);
                        break;
                    case ResponseType.AddPhoto:
                        control = (UserControl) LoadControl("~/TaskList/TaskList/UserControls/AddPhotoTemplate.ascx");
                        ((AddPhotoTemplate) control).SetData(question, ph.CurrentOrganizationId, i++);
                        break;
                    case ResponseType.Checkbox:
                        control = (UserControl) LoadControl("~/TaskList/TaskList/UserControls/CheckBoxTemplate.ascx");
                        ((CheckBoxTemplate) control).SetData(question, i++);
                        break;
                }

                if (control != null)
                {
                    control.ID = $"Control_{question.questionID}";
                    taskListBlock.Controls.Add(control);
                }
            }
            
            formGroup.Controls.Add(taskListBlock);

            formHorizontal.Controls.Add(formGroup);
            panelBody.Controls.Add(formHorizontal);
            panel.Controls.Add(panelBody);
            
            formDataContainer.Controls.Add(panel);
        }

        #endregion
    }
}