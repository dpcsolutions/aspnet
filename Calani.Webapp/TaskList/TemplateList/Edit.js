﻿var submit = 0;
var submitTemplate = 0;
var lastOptionForPickList = '';
var lastPossibleAnswer = '';
var originalQuestion = '';
var originalDescription = '';
var originalResponseType = '';
var originalOptionForPickList = '';
var originalPossibleAnswer = '';
var originalMandatory = false;

$(document).ready(function () {
    $('.OptionsForYourPickList').hide();

    $('#ContentPlaceHolder1_ddlResponseType').on('change', function () {
        var selectedValue = $(this).val();
        if (selectedValue === "Option") {
            $('.OptionsForYourPickList').show();
            $('.Possiblechoices').show();
            // Restore last values if they exist
            $('#ContentPlaceHolder1_optionsForYourPickList').val(lastOptionForPickList);
            $('#ContentPlaceHolder1_ddlListPossiblechoices').val(lastPossibleAnswer).change();
        } else {
            // Store current values before hiding
            lastOptionForPickList = $('#ContentPlaceHolder1_optionsForYourPickList').val();
            lastPossibleAnswer = $('#ContentPlaceHolder1_ddlListPossiblechoices').val();

            $('.OptionsForYourPickList').hide();
            $('.Possiblechoices').hide();
            $('#ContentPlaceHolder1_optionsForYourPickList').val("");
            $('#ContentPlaceHolder1_ddlListPossiblechoices').val('').change();
        }
    });

    // Event handler for the close button
    $('#closeModal').on('click', function () {
        $('#Question-error').hide();
        $('#Description-error').hide();
        $('#ResponseType-error').hide();
        $('#OptionsForYourPickList-error').hide();
        $('#Possiblechoices-error').hide();
        submit = 0;
    });


});
function deleteTemplate(id) {
    $('#<%= templateQuestionId.ClientID %>').val(id);
    $('#modal_delete_prof').modal('show');
}

function validateTemplateSave() {
    if (++submitTemplate > 1) {
        return false;
    }
}

function validateQuestionSave() {
    if (++submit > 1) {
        return false;
    }
    var res = true;
    var question = $('#ContentPlaceHolder1_tbQuestion').val();
    var description = $('#ContentPlaceHolder1_tbDescription').val();
    var responseType = $('#ContentPlaceHolder1_ddlResponseType').val();
    var optionForPickList = $('#ContentPlaceHolder1_optionsForYourPickList').val();
    var possibleAnswer = $('#ContentPlaceHolder1_ddlListPossiblechoices').val();
    var mandatory = $('#ContentPlaceHolder1_cbMandatoryAnswer').is(':checked');

    if (!question || !description || !responseType) {
        res = false;
        if (!question)
            $('#Question-error').show();
        else
            $('#Question-error').hide();

        if (!description)
            $('#Description-error').show();
        else
            $('#Description-error').hide();

        if (!responseType)
            $('#ResponseType-error').show();
        else
            $('#ResponseType-error').hide();
        submit = 0;
    }
    else {
        $('#Question-error').hide();
        $('#Description-error').hide();
        $('#ResponseType-error').hide();
    }
    if (responseType == "Option") {
        if (!optionForPickList || !possibleAnswer) {
            res = false;
            if (optionForPickList == "") {
                $('#OptionsForYourPickList-error').show();
            } else {
                $('#OptionsForYourPickList-error').hide();
            }
            if (possibleAnswer == "") {
                $('#Possiblechoices-error').show();
            } else {
                $('#Possiblechoices-error').hide();
            }
            submit = 0;
        }
        else {
            $('#OptionsForYourPickList-error').hide();
            $('#Possiblechoices-error').hide();
        }
        
        
    } else {
        $('#OptionsForYourPickList-error').hide();
        $('#Possiblechoices-error').hide();
    }

    // Check if question, description, responseType, optionForPickList, and possibleAnswer have been modified
    if (question === originalQuestion && description === originalDescription && responseType === originalResponseType &&
        optionForPickList === originalOptionForPickList && possibleAnswer === originalPossibleAnswer && mandatory === originalMandatory)
    {
        alert(allowQuestionSaveInfoAlert);
        submit = 0;
        return false;
    }
    return res;
}

function editQuestions(questionId, question, description, responseType, optionForPickList, possibleAnswer, mandatory ) {
    $('#ContentPlaceHolder1_templateQuestionId').val(questionId);
    $('#ContentPlaceHolder1_tbQuestion').val(question);
    $('#ContentPlaceHolder1_tbDescription').val(description);
    $('#ContentPlaceHolder1_ddlResponseType').val(responseType).trigger('change');
    $('#ContentPlaceHolder1_optionsForYourPickList').val(optionForPickList);
    $('#ContentPlaceHolder1_ddlListPossiblechoices').val(possibleAnswer).trigger('change');

    // Set the original values after populating the fields
    originalQuestion = question; 
    originalDescription = description; 
    originalResponseType = responseType; 
    originalOptionForPickList = optionForPickList;
    originalPossibleAnswer = possibleAnswer; 
    originalMandatory = (mandatory === "True");

    if (mandatory === "True") {
        $('#ContentPlaceHolder1_cbMandatoryAnswer').prop('checked', true);
    } else {
        $('#ContentPlaceHolder1_cbMandatoryAnswer').prop('checked', false);
    }
    if (questionId > 0) {
        $('#createHeader').hide();
        $('#editHeader').show();
    } else {
        $('#createHeader').show();
        $('#editHeader').hide();
    }
    $('#modal_questioncreate').modal('show');
}

function setQuestionID(questionID) {
    $('#ContentPlaceHolder1_HiddenFieldQuestionID').val(questionID);
}

