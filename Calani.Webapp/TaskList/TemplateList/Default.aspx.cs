﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.Template;

namespace Calani.WebappNew.TaskList.List
{
    public partial class Default : System.Web.UI.Page
    {
        public PageHelper ph;
        protected override void InitializeCulture()
        {
            ph = new PageHelper(this);
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            EnableViewState = false;
            if (!IsPostBack)
            {
                gridListTemplate.Columns[0].HeaderText = Resources.Resource.List_Templates;
                var templateManager = new TemplateManager(ph.CurrentOrganizationId);
                var list = templateManager.ListTemplates();
                gridListTemplate.DataSource = list;
                new Html5GridView(gridListTemplate);
            }
        }
    }
}