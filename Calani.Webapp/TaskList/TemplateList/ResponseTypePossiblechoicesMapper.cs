﻿using Calani.BusinessObjects.Enums;

namespace Calani.WebappNew.TaskList.TemplateList
{
    public static class ResponseTypePossiblechoicesMapper
    {
        public static string GetResponseTypeText(object responseType)
        {
            switch (responseType.ToString())
            {
                case ResponseType.Option:
                    return Resources.Resource.ResponseType_Option;
                case ResponseType.YesNo:
                    return Resources.Resource.ResponseType_Yes_No;
                case ResponseType.Date:
                    return Resources.Resource.ResponseType_Date;
                case ResponseType.Text:
                    return Resources.Resource.ResponseType_Text;
                case ResponseType.AddPhoto:
                    return Resources.Resource.ResponseType_AddPhoto;
                case ResponseType.Checkbox:
                    return Resources.Resource.ResponseType_Checkbox;
                default:
                    return "Unknown";
            }
        }

        public static string GetPossiblechoicesText(object possibleAnswer)
        {
            if (possibleAnswer == null)
            {
                return "";
            }
            switch (possibleAnswer.ToString())
            {
                case "":
                    return "";
                case "one":
                    return Resources.Resource.Possiblechoices_one;
                case "several":
                    return Resources.Resource.Possiblechoices_several;
                default:
                    return "Unknown";
            }
        }

    }
}