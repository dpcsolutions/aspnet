﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Calani.WebappNew.TaskList.List.Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form runat="server">
        <div class="content">
					<div>
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="display-inline-block text"></i>
										<strong><%= Resources.Resource.List_Templates %> </strong>
									</a>
								</h6>
							</div><!-- panel-heading -->
							<div class="panel-body">
                                <asp:GridView runat="server" ID="gridListTemplate" AutoGenerateColumns="false">
                                    <Columns>
                                         <asp:HyperLinkField DataTextField="listName" HeaderText="List Template" DataNavigateUrlFields="templateId" DataNavigateUrlFormatString="Edit.aspx?id={0}" />
                                    </Columns>
                                </asp:GridView>
                            </div><!-- panel-body -->
                            <div class="panel-footer text-right no-padding">
						    	<div class="row" style="margin:10px">
						    		<a href="Edit.aspx" class="btn btn-default" runat="server" ID="addNewListTemplate"><i class="icon-plus2 position-left"></i> 
                                        <%= Resources.Resource.Add_new_ListTemplate %>
						    		</a>
						    	</div>
							</div><!-- panel-footer -->
                        </div>
                    </div>
        </div><!-- /content -->
    </form>
</asp:Content>
