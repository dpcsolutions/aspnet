﻿using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.TaskList;
using Calani.BusinessObjects.Template;
using Calani.BusinessObjects.Template.Models;
using Calani.WebappNew.TaskList.TemplateList;

namespace Calani.WebappNew.TaskList.List
{
    public partial class Edit : System.Web.UI.Page
    {
        public PageHelper ph;
        private TemplateManager _templateManager;
        private QuestionManager questionManager;
        protected override void InitializeCulture()
        {
            ph = new PageHelper(this);
        }
        public long ProjectId = 0;
        public string IsReadOnly = "false";

        protected void Page_Load(object sender, EventArgs e)
        {
            ph.ItemLabel = Resources.Resource.MenuList_Templates;
            _templateManager = new TemplateManager(ph.CurrentOrganizationId);
            questionManager = new QuestionManager(ph.CurrentOrganizationId);

            ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
            btnDelete, btnMore, btnMoreDdl, lblTitle, linkBackList, linkNew);

            if (ph.CurrentId == null)
            {
                divQuestion.Visible = false;
                ph.SetStateNew(Resources.Resource.Create_new_ListTemplate);
                
            }
            if (ph.CurrentId != null)
            {
                divQuestion.Visible = true;

                if (!IsPostBack)
                {
                    var templateId = _templateManager.Get(ph.CurrentId.Value);
                    Load(templateId);
                    ph.SetStateEdit(Resources.Resource.Edit_list_Template);
                }

            }

        }
        #region CRUD
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (ph.CurrentId != null)
                Update();
            else Create();
        }
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (ph.CurrentId != null) Delete();
        }
        protected void btSaveDetails_Click(object sender, EventArgs e)
        {
            long templateQuestionIdValue = long.Parse(templateQuestionId.Value);
            var templateID = ph.CurrentId;

            if (!ph.CurrentId.HasValue)
            {
                var newTemplateId = Create();
                if (newTemplateId > 0)
                    templateID = newTemplateId;
                else
                    return;
            }
            else
            {
                var templates = _templateManager.Get(ph.CurrentId.Value);
            }

            DateTime? dateCreated = null;
            var questions = tbQuestion.Value;
            var description = tbDescription.Value;
            var responseType = ddlResponseType.SelectedValue;
            bool isMandatory = cbMandatoryAnswer.Checked;
            dateCreated  = DateTime.Now.Date;
            var optionForPickList = optionsForYourPickList.Value;
            var possibleAnswer = ddlListPossiblechoices.SelectedValue;
            if (templateQuestionIdValue <= 0 && String.IsNullOrWhiteSpace(questions) || String.IsNullOrWhiteSpace(description) || String.IsNullOrWhiteSpace(responseType))
                return;

            var questionsList = new QuestionDto()
            {
                questionID = templateQuestionIdValue,
                TemplateId = templateID ?? 0,
                Question = questions,
                Descriptions = description,
                ResponseType = responseType,
                IsMandatory = isMandatory,
                LastUpdate = dateCreated,
                CreatedBy = ph.CurrentUserId,
                OptionForPickList = optionForPickList,
                PossibleAnswer = possibleAnswer,
            };
            questionManager.CreateQuestions(questionsList);

            if (ph.CurrentId.HasValue)
                Response.Redirect(Request.Url.PathAndQuery);
        }
        private void Load(templates templates = null)
        {
            if (templates == null)
            {
                templates = _templateManager.Get(ph.CurrentId.Value);
            }
            tbxListName.Value = templates.listName;

            //question section bind
            var questionsList = questionManager.GetQuestionsByTemplateId(ph.CurrentId);
            listQuestion.DataSource = questionsList;
            listQuestion.DataBind();
        }
        protected string GetResponseTypeText(object responseType)
        {
            return ResponseTypePossiblechoicesMapper.GetResponseTypeText(responseType);
        }

        protected string GetPossiblechoicesText(object possibleAnswer)
        {
            return ResponseTypePossiblechoicesMapper.GetPossiblechoicesText(possibleAnswer);
        }
        private long Create()
        {
            try
            {
                if (String.IsNullOrWhiteSpace(tbxListName.Value))
                {
                    ph.DisplayError(Resources.Resource.Error_ListTemplate_Mandatory);
                    return (long)0;
                }

                var mgr2 = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(ph.CurrentOrganizationId);
                mgr2.Load();

                var model = new templates();

                model.listName = tbxListName.Value;
                model.organizationId = ph.CurrentOrganizationId;
                model.dateCreated = DateTime.UtcNow;
                model.createdBy = ph.CurrentUserId;
             
                var ret = _templateManager.SaveOrUpdate(0, model);
                long id = 0;
                if (ret.Success)
                {
                    ph.DisplaySuccess(Resources.Resource.Created_var);
                    btnSave.Enabled = false;
                    id = ret.Record.templateId;
                }
                else
                    ph.DisplayError(ret.ErrorMessage);

                return id;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
        private void Update()
        {
            if (String.IsNullOrWhiteSpace(tbxListName.Value))
            {
                ph.DisplayError(Resources.Resource.Error_Initials_Mandatory);
                return;
            }

            var templatesEntity = new Calani.BusinessObjects.Model.templates
            {
                listName = tbxListName.Value,
                lastUpdate = DateTime.UtcNow,
                updatedBy = ph.CurrentUserId,

            };

            var ret = _templateManager.SaveOrUpdate(ph.CurrentId.Value, templatesEntity);

            if (ret.Success)
            {
                ph.DisplaySuccess(Resources.Resource.Updated_var);
                Load();
            }
            else ph.DisplayError(ret.ErrorMessage);
        }
        protected void Delete()
        {

            var ret = _templateManager.Delete(ph.CurrentId.Value);
            if (ret.Success)
            {
                panelEdit.Visible = false;
                ph.DisplaySuccess(Resources.Resource.Deleted_var);
            }
            else ph.DisplayError(ret.ErrorMessage);
        }
        protected void ButtonEditDelete_Click(object sender, EventArgs e)
        {
            string questionIDString = HiddenFieldQuestionID.Value;
            long questionID;
            questionID = long.Parse(questionIDString);
            var ret = questionManager.DeleteQuestionByQuestionId(questionID,ph.CurrentUserId, ph.CurrentId.Value);
            if (ret)
            {
                ph.DisplaySuccess(Resources.Resource.Deleted_var);
                Load();
            }
            else
            {
                ph.DisplayError("Question not found or could not be deleted.");
            }
        }

        #endregion CRUD
    }
}