<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="Calani.WebappNew.TaskList.List.Edit" %>
<%@ Import Namespace="Calani.BusinessObjects.Enums" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <%--Canceled Seubi css for page to apply theme style--%>
    <style type="text/css">
        @media screen and (max-width: 768px) {
            .table-responsive > .table {
                margin-bottom: 0;
                table-layout: auto !important;
            }
        }
        .table > thead > tr > th {
            border-bottom: none;  
            background: none; 
            color: black;
        }
        .input-group-responseType {
            position: relative;
            display: table;
            border-collapse: separate;
            width: 100%;
        }
        .input-group-possiblechoices {
            position: relative;
            display: table;
            border-collapse: separate;
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form runat="server">
        <div class="content">
		    <div class="row">
                <!-- table column -->
			    <div class="col-sm-12 col-md-12">
                        <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Error %></h6>
						    </div>
						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblErrorMessage" />
						    </div>
					    </asp:Panel><!-- panelError -->
                        <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Done %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblSuccess" />
                                <br />
                                <strong><a href="." id="linkBackList" runat="server"></a></strong>
                                    <%= Resources.Resource.or %>
                                <a href="Edit.aspx" id="linkNew" runat="server"></a>.
						    </div>
					    </asp:Panel><!-- panelSuccess -->				    
					    <asp:Panel runat="server" ID="panelEdit" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><asp:Label runat="server" ID="lblTitle" /></h6>
						    </div>
						    <div class="panel-body">
							    <div class="form-horizontal">
								    <fieldset class="content-group">
                                        <!-- Listname -->
									    <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-12 col-sm-3" style="margin-left: 10px;"><%= Resources.Resource.Listname %></label>
                                                <div class="col-10 col-sm-8">
                                                    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-font"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Listname %>' runat="server" id="tbxListName" />
											    </div>
                                                </div>
                                            </div>
									    </div>
                                        <!-- Listname -->
								    </fieldset>
                                    <%--showing the question addition grid--%>
                                    <div class="content-group">
                                        <legend class="text-bold"><%= Resources.Resource.Question %></legend>
                                            <div class="table-responsive panel-body" runat="server" ID="divQuestion">
                                                <asp:ListView runat="server" ID="listQuestion" >
                                                    <LayoutTemplate>
                                                        <table class="table table-hover" style="table-layout:fixed">
                                                            <thead>
                                                            <tr>
                                                                <th runat="server" class="col-lg-4"><%= Resources.Resource.QuestionView %></th>
                                                                <th runat="server" class="col-lg-4"><%= Resources.Resource.DescriptionView %></th>
                                                                <th runat="server" class="col-lg-2"><%= Resources.Resource.ResponseTypeView %></th>
                                                                <th runat="server" class="col-lg-2"><%= Resources.Resource.MandatoryAnswerView %></th>
                                                                <th runat="server" class="col-lg-2"><%= Resources.Resource.OptionsForYourPickListView %></th>
                                                                <th runat="server" class="col-lg-2"><%= Resources.Resource.PossiblechoicesView %></th>
                                                                <th runat="server" style="width:130px!important"><%= Resources.Resource.Action %></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr runat="server" id="itemPlaceholder" ></tr>
                                                            </tbody>
                                                        </table>
                                                    </LayoutTemplate>
                                                    <EmptyDataTemplate>
                                                        <table class="table table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th runat="server" class="col-lg-4"><%= Resources.Resource.QuestionView %></th>
                                                                <th runat="server" class="col-lg-4"><%= Resources.Resource.DescriptionView %></th>
                                                                <th runat="server" class="col-lg-2"><%= Resources.Resource.ResponseTypeView %></th>
                                                                <th runat="server" class="col-lg-2"><%= Resources.Resource.MandatoryAnswerView %></th>
                                                                <th runat="server" class="col-lg-2"><%= Resources.Resource.OptionsForYourPickListView %></th>
                                                                <th runat="server" class="col-lg-2"><%= Resources.Resource.PossiblechoicesView %></th>
                                                                <th runat="server" style="width:130px!important"><%= Resources.Resource.Action %></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr runat="server" id="Tr1" ></tr>
                                                            </tbody>
                                                        </table>
                                                    </EmptyDataTemplate>                                                
                                                    <ItemTemplate>
                                                        <%--Binding--%>
                                                        <tr id='<%# Eval("questionID") %>' class='<%#  (RecordStatus)Enum.Parse(typeof(RecordStatus),Eval("RecordStatus").ToString()) == RecordStatus.Active ?  "" : "bg-light-grey" %>'>
                                                            <td>
                                                                <input type="text" id="lstQuestionID" runat="server" hidden="" value='<%# Bind("questionID") %>'/>
                                                                <asp:Label ID="StartLabel" runat="server" Text='<%#Eval("Question") %>' CssClass="text-left text-default"/>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="DescriptionsLabel" runat="server" Text='<%#Eval("Descriptions") %>' CssClass="text-left text-default"/>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="ResponseTypeLabel" runat="server" Text='<%#GetResponseTypeText(Eval("ResponseType"))%>' CssClass="text-left text-default"/>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="IsMandatoryLabel" runat="server" Text='<%#Eval("IsMandatory").ToString() == "True" ? Resources.Resource.Yes : Resources.Resource.No %>' CssClass="text-left text-default"/>
                                                            </td>
                                                            <td style="width: 200px; word-wrap: break-word; overflow-wrap: break-word;">
                                                                <asp:Label ID="OptionForPickListLabel" runat="server" Text='<%#Eval("OptionForPickList") %>' CssClass="text-left text-default"/>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="PossibleAnswerLabel" runat="server" Text='<%#GetPossiblechoicesText(Eval("PossibleAnswer"))%>' CssClass="text-left text-default"/>
                                                            </td>
                                                            <td runat="server" Visible='<%# (RecordStatus)Enum.Parse(typeof(RecordStatus),Eval("RecordStatus").ToString()) == RecordStatus.Suspended%>'>
                                                                <span class="label label-danger" style="display: inline-block;"><%= Resources.Resource.Canceled %></span>
                                                                <a runat="server" href="#" class="text-danger" data-toggle="modal" data-target="#modal_defaultEditDelete" 
                                                                    onclick='<%#"setQuestionID(" + Eval("questionID") + ");" %>' style="display: inline-block; margin-left: 5px; padding: 4px;">
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                            </td>
                                                            <td runat="server" Visible='<%# (RecordStatus)Enum.Parse(typeof(RecordStatus),Eval("RecordStatus").ToString()) == RecordStatus.Active%>'>
                                                                <ul class="icons-list">
                                                                    <li class="text-primary-600">
                                                                        <a runat="server" href="#" data-target="#modal_questioncreate" 
                                                                            onclick='<%# "editQuestions(" + Eval("questionID") + ", \"" + 
                                                                            Eval("Question").ToString() + "\", \"" + 
                                                                            Eval("Descriptions").ToString() + "\", \"" + 
                                                                            Eval("ResponseType").ToString() + "\", \"" +
                                                                            (Eval("OptionForPickList") != null ? Eval("OptionForPickList").ToString() : "") + "\", \"" + 
                                                                            (Eval("PossibleAnswer") != null ? Eval("PossibleAnswer").ToString() : "") + "\", \"" + 
                                                                            Eval("IsMandatory").ToString() + "\");" %>'>
                                                                            <i class="icon-pencil7"></i>
                                                                        </a>
                                                                    </li>
                                                                    <li class="text-danger">
                                                                        <a runat="server" href="#" 
                                                                           data-toggle="modal" 
                                                                           data-target="#modal_defaultEditDelete" 
                                                                           onclick='<%#"setQuestionID(" + Eval("questionID") + ");" %>'>
                                                                           <i class="fa fa-trash"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:ListView>
                                                <br/>
                                                <div class="col-lg-2 pull-left">
                                                    <a onclick="editQuestions(0, '', '', '','','','')"  class="btn btn-default pull-left"><i class="icon-plus2 position-left"></i><%= Resources.Resource.Add_another_question %></a>
                                                </div>
                                            </div>
                                        </div>
                                    <%--end showing the question addition grid--%>
                                    <!-- buttonsection -->
								    <div class="buttonsaction text-right">
										<label class="control-label col-lg-2"></label>
										<div class="col-lg-10">
											<div class="btn-group">
                                                <asp:LinkButton runat="server" ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click" OnClientClick="return  validateTemplateSave()">
                                                    <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                                </asp:LinkButton>
                                                
									            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore"><span class="caret"></span></button>
									            <ul class="dropdown-menu dropdown-menu-right" runat="server" id="btnMoreDdl">
                                                    <li><a href="javascript:void(0)" data-toggle="modal" data-target="#modal_default"><i class="fa fa-trash-o"></i> <%= Resources.Resource.Delete %></a></li>
												</ul>
											</div>
										</div>
									</div>
                                    <!-- /buttonsection -->
							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->
			    </div>
			    <!-- /table column -->
		    </div> <!-- /row -->		
            <!-- basic modal: confirm delete -->
		    <div id="modal_default" class="modal fade">
			    <div class="modal-dialog">
				    <div class="modal-content">
					    <div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal">&times;</button>
						    <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
					    </div>

					    <div class="modal-body">
						    <h6 class="text-semibold"><%= Resources.Resource.Are_You_Sure_To_Delete %></h6>
						    <p><%= Resources.Resource.Delete_Is_Irreversible %></p>

					    </div>

					    <div class="modal-footer">
						    <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
                            <asp:Button runat="server" ID="btnDelete" OnClick="btnDelete_Click" CssClass="btn btn-primary" Text="<%$ Resources:Resource, Yes_Delete %>" />

					    </div>
				    </div>
			    </div>
		    </div>
		    <!-- /basic modal -->
            <!-- basic modal: confirm delete for edit portion -->
		    <div id="modal_defaultEditDelete" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
                        </div>

                        <div class="modal-body">
                            <h6 class="text-semibold"><%= Resources.Resource.Are_You_Sure_To_Delete %></h6>
                            <p><%= Resources.Resource.Delete_Is_Irreversible %></p>
                            <asp:HiddenField ID="HiddenFieldQuestionID" runat="server" />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
                            <asp:Button runat="server" ID="ButtonEditDelete" OnClick="ButtonEditDelete_Click" CssClass="btn btn-primary" Text="<%$ Resources:Resource, Yes_Delete %>" />
                        </div>
                    </div>
                </div>
            </div>
		    <!-- /basic modal: confirm delete for edit portion -->
            <div id="modal_questioncreate" class="modal fade">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title text-center" id="createHeader"><%= Resources.Resource.Create %></h5>
                        <h5 class="modal-title text-center" id="editHeader"><%= Resources.Resource.Edit_Line %></h5>
                    </div>
                    <div class="modal-body">
                        <input type="text" id="templateQuestionId" runat="server" hidden="" />
                        <div class="row">
                             <h6 class="col-12 col-sm-3"><%= Resources.Resource.Question %></h6>
                            <div class="col-12 col-sm-9">
                                <div class="input-group form-group">
                                    <span class="input-group-addon"><i class="fa fa-question-circle"></i></span>
                                    <input type="text" id="tbQuestion" name="Question" runat="server" class="form-control" />
                                </div>
                                <label id="Question-error" class="validation-error-label" for="Question" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                            </div>
                        </div>
                        <div class="row">
                            <h6 class="col-12 col-sm-3"><%= Resources.Resource.Description %></h6>
                            <div class="col-12 col-sm-9">
                                <div class="input-group form-group">
                                    <span class="input-group-addon"><i class="fas fa-audio-description"></i></span>
                                    <input type="text" id="tbDescription" name="Description"  runat="server" class="form-control" value="" placeholder="" />
                                </div>
                                <label id="Description-error" class="validation-error-label" for="Description" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                            </div>
                        </div>
                        <div class="row">
                            <h6 class="col-12 col-sm-3"><%= Resources.Resource.ResponseType %></h6>
                            <div class="col-12 col-sm-9">
                                <div class="form-group">
                                    <span class=""></span>
                                    <div class="input-group-responseType">
										<span class="input-group-addon"><i class="fa fa-font"></i></span>
                                        <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlResponseType">
                                            <asp:ListItem Value="Option" Text='<%$ Resources:Resource, ResponseType_Option %>' />
                                            <asp:ListItem Value="Yes/No" Text='<%$ Resources:Resource, ResponseType_Yes_No %>' />
                                            <asp:ListItem Value="Date" Text='<%$ Resources:Resource, ResponseType_Date %>' />
                                            <asp:ListItem Value="Text" Text='<%$ Resources:Resource, ResponseType_Text %>' />
                                            <asp:ListItem Value="AddPhoto" Text='<%$ Resources:Resource, ResponseType_AddPhoto %>' />
                                            <asp:ListItem Value="Checkbox" Text='<%$ Resources:Resource, ResponseType_Checkbox %>' />
                                        </asp:DropDownList>
									</div>
                                </div>
                                <label id="ResponseType-error" class="validation-error-label" for="ResponseType" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                            </div>
                        </div>
                        <div class="row OptionsForYourPickList">
                            <h6 class="col-12 col-sm-3"><%= Resources.Resource.OptionsForYourPickList %></h6>
                            <div class="col-12 col-sm-9">
                                <div class="input-group form-group">
                                    <span class="input-group-addon"><i class="fas fa-audio-description"></i></span>
                                    <input type="text" id="optionsForYourPickList" name="OptionsForYourPickList"  runat="server" class="form-control" value="" placeholder="" />
                                </div>
                                <label id="OptionsForYourPickList-error" class="validation-error-label" for="OptionsForYourPickList" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                            </div>
                        </div>
                        <div class="row Possiblechoices">
                            <h6 class="col-12 col-sm-3"><%= Resources.Resource.Possiblechoices %></h6>
                            <div class="col-12 col-sm-9">
                                <div class="form-group">
                                    <span class=""></span>
                                    <div class="input-group-possiblechoices">
										<span class="input-group-addon"><i class="fa fa-font"></i></span>
                                        <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlListPossiblechoices">
                                            <asp:ListItem Value="" Text='<%$ Resources:Resource, Select___ %>' />
                                            <asp:ListItem Value="one" Text='<%$ Resources:Resource, Possiblechoices_one %>' />
                                            <asp:ListItem Value="several" Text='<%$ Resources:Resource, Possiblechoices_several %>' />
                                        </asp:DropDownList>
									</div>
                                    <label id="Possiblechoices-error" class="validation-error-label" for="Possiblechoices" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <h6 class="col-12 col-sm-3"><%= Resources.Resource.MandatoryAnswer %></h6>
                            <div class="col-12 col-sm-9">
                                <div class="form-inline" style="margin-top:10px">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" runat="server" id="cbMandatoryAnswer" class="checkbox-inline" />
                                    </label>
						        </div> 
                                <div style="clear:both"></div>
                            </div>
                        </div>                    
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="btSaveDetails"  runat="server"  OnClick="btSaveDetails_Click" OnClientClick="return  validateQuestionSave()"
                                    CssClass="btn btn-primary" Text="<%$ Resources:Resource, Save %>" />  
                        <button type="button" id="closeModal" name="closeModal" class="btn btn-default" data-dismiss="modal"><%= Resources.Resource.Close %></button>
                    </div>
                </div>
            </div>
        </div>
        </div><!-- /content -->
    </form>
    <script type="text/javascript">
        const allowQuestionSaveInfoAlert = "<%= Resources.Resource.AllowQuestionInfoSaveAlert %>";
    </script>
    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/TaskList/TemplateList/Edit.js") %>"></script>
</asp:Content>
