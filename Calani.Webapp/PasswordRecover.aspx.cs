﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PasswordRecover : System.Web.UI.Page
{
    public string LanguageSwitch { get; set; }

    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {


        LanguageSwitch = HtmlLanguageSwitch.Render(this.Page.Request.Url.AbsoluteUri, System.Threading.Thread.CurrentThread.CurrentCulture.ToString(), Page.ResolveClientUrl("~/assets/"));


        LabelCopyYear.Text = ConfigurationSettings.AppSettings["copyYear"];
        HyperLinkCopy.Text = ConfigurationSettings.AppSettings["copyrightText"];
        HyperLinkCopy.NavigateUrl = ConfigurationSettings.AppSettings["copyrightUrl"];

        this.Title = ConfigurationSettings.AppSettings["title"];

        if(Request.Params["r"] != null)
        {
            PanelRecover.Visible = false;
            PanelSuccess.Visible = false;

            Calani.BusinessObjects.Membership.LoginManager mgr = new Calani.BusinessObjects.Membership.LoginManager();
            if(mgr.CanRecoverPassword(Request.Params["m"], Request.Params["r"]))
            {
                PanelError.Visible = false;
                PanelChangePassword.Visible = true;
            }
            else
            {
                PanelError.Visible = true;
                PanelChangePassword.Visible = false;
            }
        }
        else
        {
            PanelRecover.Visible = true;
            PanelSuccess.Visible = false;
            PanelError.Visible = false;
            PanelChangePassword.Visible = false;
        }

    }


    protected void btnChangePassword_Click(object sender, EventArgs e)
    {
        Calani.BusinessObjects.Membership.LoginManager mgr = new Calani.BusinessObjects.Membership.LoginManager();
        var action = mgr.SetNewPasswordRecover(Request.Params["m"], Request.Params["r"], tbxPassword.Value, tbxPassword2.Value);
        if (action.Success)
        {
            lblError.Visible = false;
            lblSuccess.Visible = true;
            PanelForm.Visible = false;
            PanelEnd.Visible = true;
        }
        else
        {
            lblErrorText.Text = ResourceHelper.Get(action.ErrorMessage);
            lblError.Visible = true;
        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        PanelRecover.Visible = false;
        PanelSuccess.Visible = true;

        Calani.BusinessObjects.Membership.LoginManager mgr = new Calani.BusinessObjects.Membership.LoginManager();
        mgr.RecoverPassword(tbxEmail.Value);


    }
}