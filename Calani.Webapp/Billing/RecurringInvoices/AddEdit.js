﻿$(document).ready(function(){    
    $('#ContentPlaceHolder1_ddlFrequencyType').change(function() {updateNextExecutionDate()});
    $('#ContentPlaceHolder1_ddlFrequencyCount').change(function() {updateNextExecutionDate()});
    $('#ContentPlaceHolder1_ddlSpecificDayCount').change(function() {updateNextExecutionDate()});
    $('#ContentPlaceHolder1_ddlSpecificMonthCount').change(function() {updateNextExecutionDate()});
    
    $('#ContentPlaceHolder1_cbxUntilNoticeDeadLine').change(function() {
        updateNextExecutionDate()
        if (this.checked) $('#ContentPlaceHolder1_cbxUntilSpecificMonthDeadLine').prop('checked', false);
    });
    
    $('#ContentPlaceHolder1_cbxUntilSpecificMonthDeadLine').change(function() {
        updateNextExecutionDate()
        if (this.checked) $('#ContentPlaceHolder1_cbxUntilNoticeDeadLine').prop('checked', false);
    });
    
    $('#ContentPlaceHolder1_cbxSpecificDayExecution').change(function() {
        updateNextExecutionDate()
        if (this.checked) $('#ContentPlaceHolder1_cbxLastDayExecution').prop('checked', false);
    });
    
    $('#ContentPlaceHolder1_cbxLastDayExecution').change(function() {
        updateNextExecutionDate()
        if (this.checked) $('#ContentPlaceHolder1_cbxSpecificDayExecution').prop('checked', false);
    });
    
    $('#ContentPlaceHolder1_ddlSendingType').change(function() {
        console.log("ContentPlaceHolder1_ddlSendingType");
        console.log($(this).val());
        if (+$(this).val() === 1) $('#create-draft').css('display', 'none');
        else $('#create-draft').css('display', 'flex');
    });
});
    
function updateNextExecutionDate(){
    $.ajax({
        url: '../../API/Service.svc/GetNextExecutionDate',
        type: 'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(getRecurringInfo()),
        success: function(response) {
            $('#ContentPlaceHolder1_dtNextExecutionDate').val(response.data);
        },
        error: function(xhr, status, error) {
            $('#ContentPlaceHolder1_dtNextExecutionDate').val('');
        }
    });
}

function afterJobSave(query, response)
{
    if (query.action.indexOf("save") == 0)
    {
        swal({
            title: ressources2['CreateAnOtherInvoice'],
            text: '...',
            type: "success",
            showCancelButton: true,
            showCloseButton: true,
            closeButtonHtml: 'lapin',
            cancelButtonText: ressources2['GoToList'],
            confirmButtonColor: "#4CAF50",
            confirmButtonText: ressources2['YesCreateNew']
        })
            .then((value) => {



                if (value.value != null && value.value == true)
                {
                    /* create another */
                    window.location = "Job.aspx";
                }
                else
                {
                    if(value.dismiss == "cancel")
                    {
                        /* go to list */
                        window.location = "../RecurringInvoices/";
                    }
                    else if(value.dismiss=="close")
                    {
                        /* stay here */
                        if(query.id == -1)
                        {
                            window.location = "Job.aspx?id=" + response.id;
                        }

                    }
                }

            });


    }

    if (query.action == "sendinvoice" && query.id > 0)
    {
        window.location = "Send.aspx?id=" + query.id + "&sel=" + response.attachment;
    }
}

function onFrequencyTypeChange(){
    let ddl = document.getElementById('ContentPlaceHolder1_ddlSpecificDayCount');
    let $lbl = $("#last-day-label");
    let $monthRow = $("#specific-month-row");
    
    switch (+$('#ContentPlaceHolder1_ddlFrequencyType').val()) {
        case 1:
            $lbl.text(ressources2['LastDayOfTheWeek'].toLowerCase());
            $monthRow.css("display", "none");
            ddl.options.length = 0;
            for (let i = 1; i <= 7; i++) {
                let option = document.createElement("option");
                option.text = i.toString();
                option.value = i.toString();
                ddl.add(option);
            }
            break;
        case 2:
            $lbl.text(ressources2['LastDayOfTheMonth'].toLowerCase());
            $monthRow.css("display", "none");
            
            ddl.options.length = 0;
            for (let i = 1; i <= 31; i++) {
                let option = document.createElement("option");
                option.text = i.toString();
                option.value = i.toString();
                ddl.add(option);
            }
            break;
        case 3:
            $lbl.text(ressources2['LastDayOfTheYear'].toLowerCase());
            $monthRow.css("display", "block");

            ddl.options.length = 0;
            for (let i = 1; i <= 31; i++) {
                let option = document.createElement("option");
                option.text = i.toString();
                option.value = i.toString();
                ddl.add(option);
            }
            break;
    }
}