﻿using System;
using Calani.BusinessObjects.CustomerAdmin;

public partial class Billing_RecurringInvoices_Default : System.Web.UI.Page
{
    public PageHelper PageHelper;
    public string OptionsClients { get; set; }
    
    public bool UsePriceIncludingVat { get; set; }
    protected override void InitializeCulture()
    {
        PageHelper = new PageHelper(this);
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        var orgMgr = new OrganizationManager(PageHelper.CurrentOrganizationId);
        orgMgr.Load();
        UsePriceIncludingVat = orgMgr.UsePriceIncludingVat;

        OptionsClients = PageHelper.RenderCustomerListOption();
    }
}
