﻿<%@ Page Title="RecurringInvoices" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Billing_RecurringInvoices_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
    <div class="btn-group" runat="server" id="quickmenu">
        <button type="button" class="btn bg-gesmobile btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
        </button>

        <ul class="dropdown-menu dropdown-menu-right">
            <li>
                <a href="AddEdit.aspx">
                    <i class="icon-plus2"></i> <%= Resources.Resource.Add_new %> <%= Resources.Resource.RecurringInvoices %>
                </a>
            </li>
        </ul>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
        <div class="content" id="app">
            <div class="row">
                <div class="col-sm-12 col-md-3">

                    <!-- User card with thumb and social icons at the bottom -->
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row text-center" style="justify-content: space-around; display: flex;">

                                <div class="col-xs-6" v-bind:class="{ 'bordered alpha-primary': statusFilter==true }">
                                    <a href="" class="text-green" v-on:click="setStatusFilter($event, true)">
                                        <p><i class="icon-checkmark icon-2x display-inline-block text-green" id="icoCountActive" runat="server"></i></p>
                                        <h5 class="text-semibold no-margin" id="lblCountActive" runat="server">-</h5>
                                        <strong class="text-muted text-size-small"><%= Resources.Resource.Active %></strong>
                                    </a>
                                </div>

                                <div class="col-xs-6" v-bind:class="{ 'bordered alpha-primary': statusFilter==false }">
                                    <a href="" class="text-danger" v-on:click="setStatusFilter($event, false)">
                                        <p><i class="icon-trash icon-2x display-inline-block text-danger" id="icoCountDisable" runat="server"></i></p>
                                        <h5 class="text-semibold no-margin" id="lblCountDisable" runat="server">-</h5>
                                        <strong class="text-muted text-size-small"><%= Resources.Resource.Disable %></strong>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer text-center no-padding">
                            <div class="row">
                                <a href=""  v-on:click="setStatusFilter($event, null)"  class="display-block p-10 text-default" 
                                	data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.ListAllRecords %>">
                                		<i class="fa fa-ellipsis-h"></i> <%= Resources.Resource.Show_all %>
                                	</a>
                            </div>
                        </div>
                    </div>
                    <!-- /user card with thumb and social icons at the bottom -->

                </div>


                <div class="col-sm-6 col-md-4">
                </div>


                <div class="col-sm-7 col-md-4">
                    <div class="panel panel-body panel-body-accent">
                        <div class="media no-margin">
                            <div>
                                <i class="fa fa-user-circle-o" aria-hidden="true"></i> <%= Resources.Resource.Customers %>
                            </div>
                            <div>
                                <select id="ddlClient" data-placeholder="<%= Resources.Resource.Select___ %>" multiple="multiple" class="select" onchange="setClientFilter()">
                                    <%= OptionsClients %>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /row -->

            <div>
                <div class="panel panel-white">

                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <a name="recurringInvoices">
                                <i class="icon-windows2 display-inline-block text-black"></i>
                                <strong><%= Resources.Resource.RecurringInvoices %></strong>
                            </a>
                        </h6>
                    </div><!-- panel-heading -->

                    <div class="panel-body">
                        <table id="recurring-invoices-datatable" class="table table-hover dataTable no-footer">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th><%= Resources.Resource.Customer %></th>
                                <th><%= Resources.Resource.Description %></th>
								<%= UsePriceIncludingVat ? "" : "<th>" + Resources.Resource.TotalTTC + "</th>" %>
								<th><%= UsePriceIncludingVat ? Resources.Resource.TotalTTC : Resources.Resource.TotalExcTax %></th>
                                <th><%= Resources.Resource.Frequency %></th>
                                <th><%= Resources.Resource.NextExecutionDate %></th>
                                <th><%= Resources.Resource.GeneratedCount %></th>
                                <th><%= Resources.Resource.Status %></th>
                            </tr>
                            </thead>
                        </table>
                    </div><!-- panel-body -->

                    <div class="panel-footer text-right no-padding">
                        <div class="row" style="margin:10px">
                            <a href="AddEdit.aspx" class="btn btn-default">
                                <i class="icon-plus2 position-left"></i>
                                <%= Resources.Resource.Add_new_ReccInvoice %>
                            </a>
                        </div>
                    </div><!-- panel-footer -->

                </div>
            </div>
        </div><!-- /content -->
    </form>


    <script type="text/javascript">
        var ressources = [];                
        ressources['Week'] = "<%= Resources.Resource.Week %>";
        ressources['Month'] = "<%= Resources.Resource.Month %>";
        ressources['Year'] = "<%= Resources.Resource.Year %>";
        ressources['Active'] = "<%= Resources.Resource.Active %>";
        ressources['Disable'] = "<%= Resources.Resource.Disable %>";
		var usePriceIncludingVat = "<%=UsePriceIncludingVat%>";
    </script>
    <script type="text/javascript" src="Default.js?<%= Guid.NewGuid().ToString("n") %>"></script>
</asp:Content>