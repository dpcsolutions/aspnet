﻿using System;
using System.Text;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Projects;
using Calani.BusinessObjects.Projects.Enums;
using Newtonsoft.Json;

public partial class Billing_RecurringInvoices_AddEdit : System.Web.UI.Page
    {
        public PageHelper PageHelper;
        
        protected override void InitializeCulture()
        {
            PageHelper = new PageHelper(this);
        }

        public bool ShowDraftDays { get; set; }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            JobEditor1.IsAdmin = PageHelper.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin;
            
            if (!IsPostBack)
            {
                long invoiceId = -1;
                long organizationId = -1;

                if (Request.Params["id"] != null)
                {
                    long.TryParse(Request.Params["id"], out var id);
                    invoiceId = id;
                }
                
                if (Request.Params["data"] != null)
                {
                    var str = Encoding.UTF8.GetString(Convert.FromBase64String(Request.Params["data"]));

                    var obj = JsonConvert.DeserializeObject<dynamic>(str);
                    invoiceId = obj.id;
                    organizationId = obj.org;
                }
                
                if (organizationId < 1)
                    organizationId = PageHelper.GetCurrentOrganizationId(Session);
                
                for (var i = 1; i <= 12; i++) ddlFrequencyCount.Items.Add(new ListItem(i.ToString(), i.ToString()));
                for (var i = 1; i <= 7; i++) ddlSpecificDayCount.Items.Add(new ListItem(i.ToString(), i.ToString()));
                for (var i = 1; i <= 12; i++) ddlSpecificMonthCount.Items.Add(new ListItem(i.ToString(), i.ToString()));
                for (var i = 1; i <= 21; i++) ddlDaysInAdvance.Items.Add(new ListItem(i.ToString(), i.ToString()));
                
                ddlFrequencyType.Items.Add(new ListItem(Resources.Resource.Week, ((int)RecurringFrequencyType.Week).ToString()));
                ddlFrequencyType.Items.Add(new ListItem(Resources.Resource.Month, ((int)RecurringFrequencyType.Month).ToString()));
                ddlFrequencyType.Items.Add(new ListItem(Resources.Resource.Year, ((int)RecurringFrequencyType.Year).ToString()));
                
                ddlSendingType.Items.Add(new ListItem(Resources.Resource.Automatic, ((int)RecurringSendingType.Automatic).ToString()));
                ddlSendingType.Items.Add(new ListItem(Resources.Resource.DraftInvoiceWithNotificationMail, ((int)RecurringSendingType.DraftInvoiceWithNotificationMail).ToString()));
                ddlSendingType.Items.Add(new ListItem(Resources.Resource.DraftQuoteWithNotificationMail, ((int)RecurringSendingType.DraftQuoteWithNotificationMail).ToString()));
                
                ShowDraftDays = false;
                if (invoiceId > 0)
                {
                    var recMgr = new RecurringManager(organizationId);
                    var recurringInfo = recMgr.GetActualForInvoice(invoiceId);
                    LoadPageData(recurringInfo);
                }
            }
        }

        private void LoadPageData(recurringinfos recurringInfo)
        {
            if (recurringInfo != null)
            {
                ddlFrequencyCount.SelectedValue = recurringInfo.frequencyCount.ToString();
                ddlSpecificDayCount.SelectedValue = recurringInfo.specificDayCount.ToString();
                ddlFrequencyType.SelectedValue = recurringInfo.frequencyType.ToString();
                
                dtSpecificMonth.Value = recurringInfo.lastDueDate.ToString("dd/MM/yyyy");
                dtNextDueDate.Value = recurringInfo.nextDueDate.ToString("dd/MM/yyyy");
                dtNextExecutionDate.Value = recurringInfo.nextExecutionDate.ToString("dd/MM/yyyy");
                
                ddlSendingType.SelectedValue = recurringInfo.invoiceSendingType.ToString();
                ddlDaysInAdvance.SelectedValue = recurringInfo.daysInAdvance.ToString();
                
                cbxIsActiveCheckBox.Checked = recurringInfo.isActive;
                cbxLastDayExecution.Checked = recurringInfo.isLastDay;
                cbxSpecificDayExecution.Checked = recurringInfo.isSpecificDay;
                cbxUntilNoticeDeadLine.Checked = recurringInfo.isUntilNotice;
                cbxUntilSpecificMonthDeadLine.Checked = recurringInfo.isSpecificMonth;
                
                ShowDraftDays = recurringInfo.invoiceSendingType > 1;
            }
        }

    }
