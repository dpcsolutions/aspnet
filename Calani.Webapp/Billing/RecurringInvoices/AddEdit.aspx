﻿<%@ Page Title="Invoince" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Billing_RecurringInvoices_AddEdit" Codebehind="AddEdit.aspx.cs" %>
<%@ Register src="../../Projects/JobEditor.ascx" tagname="JobEditor" tagprefix="uc1" %>
<%@ Import Namespace="Calani.BusinessObjects.Generic" %>
<%@ Import Namespace="Calani.BusinessObjects.Model" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="../../assets/js/shared_business_logic/working_time_calculator.js"></script>
    <script src="../../assets/js/plugins/ui/mobiscroll.javascript.min.js"></script>
    <script src="../RecurringInvoices/AddEdit.js"></script>
    <link href="../../assets/css/mobiscroll.javascript.min.css" rel="stylesheet" type="text/css">

    <style type="text/css">
		.invoice-payment-datepicker {
			border: 1px solid #ddd;
			border-left: none;
			font-family: "Roboto", Helvetica Neue, Helvetica, Arial, sans-serif;
			font-size: 13px;
		}
        .popover {
            width: 300px;
        }
	</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<form runat="server">
<div class="content">

<uc1:JobEditor ID="JobEditor1" runat="server" EnableInvoiceFields="true"/>


<div class="row">
    <div class="col-sm-12 col-md-12" id="DateAndSendingSettingsContainer" data-recurring-id="<%= JobEditor1.Recurringinfo.id %>">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title"><%= Resources.Resource.DateAndSendingSettings %></h5>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4 col-xs-6">

                        <div class="row">
                            <div class="input-group mr-2">
                                <label class="checkbox-inline">
                                    <asp:CheckBox ID="cbxIsActiveCheckBox" runat="server" CssClass="control-primary"/>
                                </label>
                            </div>
                            <label class="control-label ml-15 col-lg-5"><%= Resources.Resource.Active %></label>
                        </div>


                        <div class="panel-heading" style="justify-content: left; display: flex;">
                            <h5 class="panel-title"><%= Resources.Resource.PaymentFrequency %></h5>
                        </div>

                        <div class="row" style="display: flex;">
                            <div class="input-group" style="margin-right: 15px;">
                                <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlFrequencyCount"
                                                  DataValueField="id" DataTextField="name"/>
                            </div>
                            <div class="input-group">
                                <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlFrequencyType" onchange="onFrequencyTypeChange(this)"
                                                  DataValueField="id" DataTextField="name"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-6">
                       <div class="panel-heading" style="justify-content: left; display: flex;">
                           <h5 class="panel-title"><%= Resources.Resource.NextDueDate %></h5>
                       </div>

                       <div class="row" style="display: flex;">
                           <div class="input-group">
                               <span class="input-group-addon"><i class="icon-calendar"></i></span>
                               <input type="text" id="dtNextDueDate" runat="server" 
                                      class="form-control daterange-single" />
                           </div>
                       </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-xs-6">

                        <div class="panel-heading" style="justify-content: left; display: flex;">
                            <h5 class="panel-title"><%= Resources.Resource.ExecutionDay %></h5>
                        </div>

                        <div class="row">
                            <div class="input-group">
                                <label class="checkbox-inline">
                                    <asp:CheckBox ID="cbxLastDayExecution" runat="server" CssClass="control-primary"/>
                                </label>
                            </div>
                            <label class="control-label  ml-15  col-lg-5">
                                <span id="last-day-label"><%= Resources.Resource.LastDayOfTheWeek %></span>
                            </label>
                        </div>

                        <div class="row">
                            <div class="input-group">
                                <label class="checkbox-inline">
                                    <asp:CheckBox ID="cbxSpecificDayExecution" runat="server" CssClass="control-primary"/>
                                </label>
                            </div>

                            <div id="specific-month-row" style="display: none;">
                                <label class="control-label ml-15 col-lg-3"><%= Resources.Resource.SpecificMonth %></label>
                                <div class="input-group">
                                    <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlSpecificMonthCount"
                                                      DataValueField="id" DataTextField="name"/>
                                </div>
                            </div>
                            <label class="control-label ml-15 col-lg-3"><%= Resources.Resource.SpecificDay %></label>
                            <div class="input-group">
                                <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlSpecificDayCount"
                                                  DataValueField="id" DataTextField="name"/>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-4 col-xs-6">

                        <div class="panel-heading" style="justify-content: left; display: flex;">
                            <h5 class="panel-title"><%= Resources.Resource.LastDeadline %></h5>
                        </div>

                        <div class="row">
                            <div class="input-group">
                                <label class="checkbox-inline">
                                    <asp:CheckBox ID="cbxUntilNoticeDeadLine" runat="server" CssClass="control-primary"/>
                                </label>
                            </div>
                            <label class="control-label  ml-15 col-lg-5"><%= Resources.Resource.UntilFurtherNotice %></label>
                        </div>

                        <div class="row">
                            <div class="row">
                                <div class="input-group">
                                    <label class="checkbox-inline">
                                        <asp:CheckBox ID="cbxUntilSpecificMonthDeadLine" runat="server" CssClass="control-primary"/>
                                    </label>
                                </div>
                                <label class="control-label  ml-15  col-lg-5"><%= Resources.Resource.EndDate %></label>
                            </div>

                            <div class="row">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="icon-calendar"></i>
                                    </span>
                                    <input type="text" id="dtSpecificMonth" runat="server"
                                           class="form-control daterange-single"/>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-xs-6">
                        <div class="panel-heading" style="justify-content: left; display: flex;">
                            <h5 class="panel-title"><%= Resources.Resource.NextExecutionDate %></h5>
                        </div>
                        <div class="row" style="display: flex;">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                <input type="text" id="dtNextExecutionDate" runat="server" 
                                       class="form-control daterange-single" /> <!--*disabled='disabled'*@ -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="panel-heading" style="justify-content: left; display: flex;">
                            <h5 class="panel-title"><%= Resources.Resource.HowTheInvoiceIsSent %></h5>
                        </div>

                        <div class="row" style="display: flex;">
                            <div class="input-group">
                                <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlSendingType"
                                                  DataValueField="id" DataTextField="name"/>
                            </div>

                            <div id="create-draft" class="input-group ml-15" style="align-items: center; display: <%= ShowDraftDays ? "flex" : "none" %>;">
                                <label class="control-label"><%= Resources.Resource.CreateDraftInvoice %></label>
                                <div class="input-group ml-15">
                                    <asp:DropDownList runat="server" CssClass="form-control select wp-80" ID="ddlDaysInAdvance"
                                                      DataValueField="id" DataTextField="name"/>
                                </div>
                                <label class="control-label ml-15"><%= Resources.Resource.DaysInAdvance %></label>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title"><%= Resources.Resource.InvoiceManagement %></h5>
            </div>
            <div class="panel-body">
                <div class="row text-center">
                    <div class="col-md-4 col-xs-6">
                        <div class="panel panel-body border-top-primary text-center">
                            <p class="text-muted content-group-sm text-size-small two-lines-small-text" id="lblLastChange" runat="server">

                            </p>

                            <div class="btn-group">
                                <a href="javascript:void(0)" runat="server" class="btn btn-primary btn-save" data-action="save" data-lbl-date="ContentPlaceHolder1_lblLastChange">
                                    <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                </a>
                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" style="min-width:240px">
                                    <li>
                                        <a href="javascript:void(0)" class="btn-save" data-action="save" data-lbl-date="ContentPlaceHolder1_lblLastChange">
                                            <i class="icon-floppy-disk"></i> <%= Resources.Resource.SaveChanges %>
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a href="javascript:void(0)" class="btn-save" data-action="previewpdf:recurringinvoice">
                                            <i class="icon-file-pdf"></i> <%= Resources.Resource.PreviewPDF %>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="btn-clone" data-type="*" data-toggle="modal" data-target="#modal_form_cloneproject">
                                            <i class="fa fa-copy"></i> <%= Resources.Resource.Clone %>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="btn-delete" data-type="*" data-toggle="modal" data-target="#modal_form_deleteproject">
                                            <i class="fa fa-trash"></i> <%= Resources.Resource.Delete %>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                            <input type="hidden" id="invoice-payment-date-hdn"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    #DateAndSendingSettingsContainer .row {
        margin-left: 0;
    } 
    .picker{
        display: none;
    }
</style>
<script type="text/javascript">
    var ressources2 = [];
    ressources2['Date'] = "<%= Resources.Resource.Date %>";
    ressources2['Description'] = "<%= Resources.Resource.Description %>";
    ressources2['Amount'] = "<%= Resources.Resource.Amount %>";
    ressources2['Comment'] = "<%= Resources.Resource.Comment %>";
    ressources2['Error_PaymentSameDateTwice'] = "<%= Resources.Resource.Error_PaymentSameDateTwice %>";
    ressources2['CreateAnOtherInvoice'] = "<%= Resources.Resource.CreateAnOtherInvoice %>";
    ressources2['YesCreateNew'] = "<%= Resources.Resource.YesCreateNew %>";
    ressources2['GoToList'] = "<%= Resources.Resource.GoToList %>";
    ressources2['Import'] = "<%= Resources.Resource.Import %>";
    ressources2['OpenInNewWindow'] = "<%= Resources.Resource.OpenInNewWindow %>";
    ressources2['AlreadyImportedOnAnInvoice'] = "<%= Resources.Resource.AlreadyImportedOnAnInvoice %>";
    ressources2['NeverImportedYet'] = "<%= Resources.Resource.NeverImportedYet %>";
    ressources2['SorryNothingToImport'] = "<%= Resources.Resource.SorryNothingToImport %>";
    ressources2['FeatureDisabledIfMultipleVAT'] = "<%= Resources.Resource.FeatureDisabledIfMultipleVAT %>";
    ressources2['Month'] = "<%= Resources.Resource.Month %>";
    ressources2['Year'] = "<%= Resources.Resource.Year %>";
    ressources2['Unit'] = "<%= Resources.Resource.Unit %>";
    ressources2['LastDayOfTheWeek'] = "<%= Resources.Resource.LastDayOfTheWeek %>";
    ressources2['LastDayOfTheMonth'] = "<%= Resources.Resource.LastDayOfTheMonth %>";
    ressources2['LastDayOfTheYear'] = "<%= Resources.Resource.LastDayOfTheYear %>";
</script>
</div>
</form>
</asp:Content>