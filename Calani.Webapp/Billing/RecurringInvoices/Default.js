﻿var _clientFilter = null;
var _statusFilter = true;
var _dataTable = null;

function checkField(val, str) {
    return val?.toString().toLowerCase().includes(str.toLowerCase());
}

function containsString(obj, str) {

    if (obj.Ids === undefined) {
        if (checkField(obj[0], str)) return true;
        if (checkField(obj[1], str)) return true;
        if (checkField(obj[2], str)) return true;
        if (checkField(obj[3], str)) return true;
        if (checkField(obj[4], str)) return true;
        if (checkField(obj[5], str)) return true;
        if (checkField(obj[6], str)) return true;
        if (checkField(obj[7], str)) return true;
        if (checkField(obj[8], str)) return true;
        if (checkField(obj[9], str)) return true;
    } else {
        if (checkField(obj.Ids?.InternalId, str)) return true;
        if (checkField(obj.ClientName, str)) return true;
        if (checkField(obj.Description, str)) return true;
        if (checkField(obj.Total, str)) return true;
        if (checkField(obj.TotalExTax, str)) return true;
        if (checkField(getFrequencyNameble(obj.RecurringInfo?.frequencyType), str)) return true;
        if (checkField(obj.RecurringInfo?.nextDueDate, str)) return true;
        if (checkField(obj.generatedNumber, str)) return true;
        if (checkField(getIsActiveNameble(obj.RecurringInfo?.isActive), str)) return true;
        
    }

    return false;
}

function initInvoicesDataTable(dt, firstColDesc, isGrouping, footerTotal, pageLength, aggregatedColumnIdx, dataUrl) {
    let options = getDataTableOptions(dt, firstColDesc, isGrouping, footerTotal, pageLength, aggregatedColumnIdx);
    options.ajax = dataUrl;
    options.columns = [
        {
            "data": "Ids",
            "render": function (Ids) {
                return "<a href=\"../../Billing/RecurringInvoices/AddEdit.aspx?id=" + Ids.Id + "\">" + Ids.InternalId + "</a>";
            }
        },
        {"data": "ClientName"},
        {"data": "Description"}];

        if (usePriceIncludingVat === "False"){
            options.columns.push({ "data": "Total", "render": displayMoney });
        }

        options.columns.push(
        { "data": "TotalExTax", "render": displayMoney },
        {"data": "RecurringInfo.frequencyType", "render": getFrequencyNameble},
        {
            "data": "RecurringInfo.nextExecutionDate",
            "type": "date",
            "render": function (date) {
                const match = date.match(/\/Date\((\d+)([+-]\d{4})?\)\//);
                if (!match) return '';
                const timestamp = parseInt(match[1], 10);
                return moment(new Date(timestamp)).format('DD.MM.YYYY HH:mm:ss');
            }
        },
        {"data": "RecurringInfo.generatedNumber"},
        {"data": "RecurringInfo.isActive", "render": getRecurringStatusHtml}
    );

    return dt.DataTable(options);
}

function getDataUrl() {
    return "../../API/Service.svc/ListRecurringInvoices";
}

function setClientFilter() {
    _clientFilter = $('#ddlClient').val();
    refreshLines();
}

function setStatusFilter(newStatus) {
    _statusFilter = newStatus;
    refreshLines();
}

function refreshLines() {
    _dataTable.draw();
}

function refreshLiveStats() {
    let countActive = 0;
    let countDisabled = 0;

    let rowsId = _dataTable.rows().eq(0);
    let searchText = $('.dataTables_filter input').val();

    for (let i = 0; i < rowsId.length; i++) {

        let row = _dataTable.row(rowsId[i]);
        let data = row.data();
        let matchesSearch = true;
        if (searchText) matchesSearch = containsString(data, searchText);

        if (row.node().attributes["data-anystatusfilter"] != null && row.node().attributes["data-anystatusfilter"].value === "true") {
            if (data["Status"] != null && data["Total"] != null) {
                let status = data["RecurringInfo"]["isActive"];

                switch (status) {
                    case true:
                        if (matchesSearch) countActive++;
                        break;
                    case false:
                        if (matchesSearch) countDisabled++;
                        break;
                }
            }
        }
    }

    $('#ContentPlaceHolder1_lblCountActive').html(countActive);
    $('#ContentPlaceHolder1_lblCountDisable').html(countDisabled);
}

function filterItem(settings, data, dataIndex) {
    let visible = true;
    let status = settings.aoData[dataIndex]._aData["RecurringInfo"]["isActive"];
    let searchText = $('.dataTables_filter input').val();

    if (searchText) {
        visible = containsString(data, searchText);
    }

    if (_clientFilter != null) {
        let clientMatch = false;
        for (let i = 0; i < _clientFilter.length; i++) {
            if (_clientFilter[i] == settings.aoData[dataIndex]._aData["ClientId"])
                clientMatch = true;
        }
        if (clientMatch == false)
            visible = false;
    }

    settings.aoData[dataIndex].nTr.setAttribute('data-anystatusFilter', visible);

    if (_statusFilter != null) {
        if (status != _statusFilter)
            visible = false;
    }

    return visible;
}

$(function () {
    // client side filters
    $.fn.dataTable.ext.search.push(filterItem);

    _dataTable = initInvoicesDataTable($('#recurring-invoices-datatable'), true, false, false, undefined, 0, getDataUrl());
    _dataTable.search('');
    _dataTable.on('draw', refreshLiveStats);

});

new Vue({
    el: '#app',
    data: {
        statusFilter: true
    },
    methods: {
        setStatusFilter: function (e, f) {
            this.statusFilter = f;
            _statusFilter = f;
            _dataTable.draw();
            e && e.preventDefault();
        }
    }
});