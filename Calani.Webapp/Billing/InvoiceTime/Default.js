﻿var _periodFilterFrom = null;
var _periodFilterTo = null;
var _clientFilter = null;
var _statusFilter = 'RecordNotInvoiced';

var _dataTable = null;

function setClientFilter() {
    _clientFilter = $('#ddlClient').val();
    checkAllRows({ checked: false });
    refreshLines();
}

function checkAllRows(obj) {

    for (var i = 0; i < _dataTable.rows().nodes().length; i++) {
        var checkbox = $(_dataTable.rows().nodes()[i]).find("input.invoicetime-checkbox");
        if (checkbox.length > 0 && $(checkbox[0]).is(":visible")) {
            checkbox[0].checked = obj.checked;
            checkUncheckCheckbox(checkbox[0], true);
        }
    }

    enableDisableCreateInvoiceButton()
}

function countCheckedRows() {
    var checkedCount = _dataTable.rows('.checked').data().length;
    return checkedCount;
}

function enableDisableCreateInvoiceButton() {
    var checkedCount = countCheckedRows();
    $("#btn-create-invoice").prop("disabled", checkedCount == 0);
}

function checkUncheckHeaderCheckbox() {

    var checkedCount = countCheckedRows();
    var total = $('input.invoicetime-checkbox').length;

    if (checkedCount == 0 || checkedCount != total) {
        $("input#header-checkbox").prop("checked", false);
        
    }
    else if (checkedCount == total) {
        $("input#header-checkbox").prop("checked", true);
    }
}

function checkUncheckCheckbox(obj, skipHeaderCheck) {
    
    var objRow = $(obj).closest("tr");

    obj.checked ? $(objRow).addClass("checked") : $(objRow).removeClass("checked");

    if (!skipHeaderCheck) {
        checkUncheckHeaderCheckbox();
        enableDisableCreateInvoiceButton();
    }
}

function refreshLines() {
    _dataTable.draw();
}

function refreshLiveStats() {
    var countWarning = 0;
    var countInvoiced = 0;
    var countNotInvoiced = 0;

    var rowsId = _dataTable.rows().eq(0);

    for (var i = 0; i < rowsId.length; i++) {
        var row = _dataTable.row(rowsId[i]);
        var data = row.data();

        var clientId = data["Imported"]["ClientId"];
        var imported = data["Imported"]["Imported"];

        if (imported) {
            countInvoiced++;
        }

        else if (!imported && clientId != null && clientId > 0) {
            countNotInvoiced++;
        } else {
            countWarning++;
        }
    }

    var searchText = $('.dataTables_filter input').val();
    if (searchText) {
        countWarning = countItems('RecordWarning');
        countInvoiced = countItems('RecordInvoiced');
        countNotInvoiced = countItems('RecordNotInvoiced');
    }

    $('#ContentPlaceHolder1_lblCountRecordWarning').html(countWarning);
    $('#ContentPlaceHolder1_lblCountRecordInvoiced').html(countInvoiced);
    $('#ContentPlaceHolder1_lblCountRecordNotInvoiced').html(countNotInvoiced);

}

function countItems(status) {
    var prevStatusFilter = _statusFilter;
    _statusFilter = status;
    var count = _dataTable.page.info().recordsDisplay;
    _statusFilter = prevStatusFilter;
    return count;
}

function filterItem(settings, data, dataIndex) {

    var visible = true;

    if (_clientFilter != null && _clientFilter != 0) {

        visible = (_clientFilter == -1 && settings.aoData[dataIndex]._aData["Imported"]["ClientId"] == null) ||
            (_clientFilter == settings.aoData[dataIndex]._aData["Imported"]["ClientId"]);
    }

    var lastUpdate = parseInt(settings.aoData[dataIndex]._aData["StartDate"]);

    if (_periodFilterFrom != null) {
        if (lastUpdate < _periodFilterFrom) visible = false;
    }

    if (_periodFilterTo != null) {
        if (lastUpdate >= _periodFilterTo) visible = false;
    }


    if (visible && _statusFilter != null) {

        var cientId = settings.aoData[dataIndex]._aData["Imported"]["ClientId"];
        var imported = settings.aoData[dataIndex]._aData["Imported"]["Imported"];

        visible = false;

        switch (_statusFilter) {
            case "RecordNotInvoiced":

                if (!imported && cientId != null && cientId > 0) {
                    visible = true;
                }

                break;
            case "RecordInvoiced":

                if (imported) {
                    visible = true;
                }

                break;

            case "RecordWarning":
                if (!cientId && !imported) {
                    visible = true;
                }
                break;

            default:
                visible = true;
                break;
        }

    }


    return visible;
}

$(function () {

    // client side filters
    $.fn.dataTable.ext.search.push(filterItem);
    // -->

    _dataTable = initTimeSheetRecordsDataTable($('#timesheet-records-datatable'), getDataUrl());
    _dataTable.search('');

    _dataTable.on('draw', function () {
        refreshLiveStats();
        if (_clientFilter && _clientFilter != 0) {
            $(":checkbox").prop("disabled", false);
        }
        else {
            $(":checkbox").prop("disabled", true);
        }

        $('[data-toggle="popover"]').popover();

    });

    _dataTable.on('xhr', function (e, settings, json, xhr) {
        var clients = extractClients(json);
        updateClientsOptions(clients);
    });
});

function periodChanged() {
    _dataTable.destroy();
    _dataTable = initTimeSheetRecordsDataTable($('#timesheet-records-datatable'), getDataUrl());
}

function initTimeSheetRecordsDataTable(dt, dataUrl) {

    var options = getDataTableOptions(dt, false, false, false, 100, null);

    options.ajax = dataUrl;
    options.columns = [
        {
            "data": "Imported",
            "render": function (imported) {
                var html = '';

                if (imported["ClientId"] != null && imported["ClientId"] > 0 && imported["Imported"] == false) {
                    html += "<input class=\"invoicetime-checkbox\" type=\"checkbox\" value=\"" + imported["ClientId"] + "\" onclick=\"checkUncheckCheckbox(this);\">";
                }

                if (imported["ClientId"] == null || imported["ClientId"] == 0) {
                    html += "<span><i class=\"icon-warning icon text-danger\" data-container=\"body\" data-placement=\"top\" data-toggle=\"popover\" data-title=\"" + STRING_RESOURCES["Warning"] + "\" data-trigger=\"hover\" data-content=\"" + STRING_RESOURCES["RecordsWarn_ToolTip"] + "\" data-original-title=\"" + STRING_RESOURCES["Warning"] + "\"></i></span>";
                }

                if (imported["Imported"] == true) {
                    html += "<span><i class=\"icon-file-check icon text-green\" data-container=\"body\" data-placement=\"top\" data-toggle=\"popover\"  data-title=\"" + STRING_RESOURCES["ImportDone"] + "\" data-trigger=\"hover\" data-content=\"" + STRING_RESOURCES["ImportDone"] + "\" data-original-title=\"" + STRING_RESOURCES["Success"] + "\"></i></span>";
                }

                return html;
            }
        },
        {
            "data": "Invoice",
            "render": function (invoice) {
                return invoice && invoice["Id"] ? '<a href="../../Projects/Invoices/Job.aspx?id=' + invoice["Id"] + '" class="text-nowrap">' + invoice["InternalId"] + '</a>' : '';
            }
        },
        {
            "data": "TimeSheetId",
            "render": function (id) {
                return id > 0 ? '<a href="../../Time/Sheets/Sheet.aspx?id=' + id + '" class="text-nowrap">' + id + '</a>' : '';
            }
        },
        {
            "data": "StartDateStr",
            type: 'date'
        },
        { "data": "Duration" },
        { "data": "Client.Name" },
        { "data": "EmployeeName" },
        {
            "data": "Project",
            "render": function (project) {
                return project && project["ProjectId"] ? '<a href="../../Projects/Opens/Job.aspx?id=' + project["ProjectId"] + '" class="text-nowrap">' + project["InternalId"] + '</a>' : '';
            }
        },
        { "data": "ServiceName" },
        { "data": "Comment" },
        {
            "data": "GroupedIds",
            "render": function (ids) {
                return "<a href=\"\" onclick=\"deleteRec(" + JSON.stringify(ids) + "); return false;\"><i class=\"icon-trash\"></i></a>";
            }
        }
    ];

    return dt.DataTable(options);
}

function getDataUrl() {
    var dates = getPeriodFilterDates();
    var url = "../../API/Service.svc/ListTimeSheetsRecordsForInvoiceTime?startTicks=" + dates[0] + "&endTicks=" + dates[1];
    return url;
}

function deleteRec(/* array */ ids) {
    $('#RecId').val(ids.join());
    $('#modal_delete').modal('show');
}

function cancelImport() {

    var recIds = $('#RecId').val().split(',');

    if (!recIds || recIds.length == 0) {
        console.error("Trying to cancel import for records with ids = " + recIds);
        return;
    }
    
    $.ajax({
        url: "../../API/Service.svc/CancelTimeSheetRecordImport?stringDelimetedIds=" + recIds.join(),
        dataType: 'json',
        success: function (response) {
            console.log("response", response);
            periodChanged();
        },
        error: function (response) {
            console.log("response", response);
        }
    });

    $('#modal_delete').modal('hide');
}

function createInvoice() {

    var checked = _dataTable.rows('.checked').data();

    if (checked.length < 1) return;

    var selectedClientId = $("#ddlClient").select2('data')[0]["id"];

    var data = [];

    for (var i = 0; i < checked.length; i++) {
        if (checked[i]["Client"]["Id"] != selectedClientId) {
            console.error("Client id error");
            return;
        }

        var p = {
            "ClientId": checked[i]["Client"]["Id"],
            "ProjectId": checked[i]["Project"]["ProjectId"],
            "TimeSheetRecordsIds": checked[i]["GroupedIds"]
        };

        data.push(p);
    }

    $.ajax({
        url: "../../API/Service.svc/ImportTimeSheetRecords",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        data: JSON.stringify(data),
        success: function (response) {
            window.location.href = "../../Projects/Invoices/Job.aspx?cust=" + selectedClientId + "&serviceJobsIds=" + response.join();
        },
        error: function (response) {
            console.log("response", response);
        }
    });
}

function extractClients(timesheetRecords) {
    var clients = [];

    if (timesheetRecords && timesheetRecords.data && timesheetRecords.data.length) {
        var set = [];
        for (var i = 0; i < timesheetRecords.data.length; i++) {
            var item = timesheetRecords.data[i];
            if (item["Client"] && item["Client"]["Id"] && set.indexOf(item["Client"]["Id"]) == -1) {
                set.push(item["Client"]["Id"]);
                clients.push({ "id": item["Client"]["Id"], "text": item["Client"]["Name"] });
            }
        }
    }
    
    clients.sort(function (a, b) { return a["text"] == b["text"] ? 0 : a["text"] < b["text"] ? -1 : 1; });

    clients.unshift(
        { "id": 0, "text": STRING_RESOURCES["Select___"] },
        { "id": -1, "text": STRING_RESOURCES["None"] });

    return clients;
}

function updateClientsOptions(clients) {

    if ($('#ddlClient').hasClass("select2-hidden-accessible")) {
        $('#ddlClient').select2('destroy');
    }
    
    $("#ddlClient").select2({ data: clients });
}

new Vue({

    el: '#app',

    data: {
        statusFilter: 'RecordNotInvoiced'
    },
    methods: {
        setStatusFilter: function (e, f) {
            this.statusFilter = f;
            _statusFilter = f;
            _dataTable.draw();

           
            e && e.preventDefault();
        }
    }
});