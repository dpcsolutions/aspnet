﻿<%@ Page Title="Invoices" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Billing_InvoiceTime_Default" Codebehind="Default.aspx.cs" %>
<%@ Register src="../../assets/Controls/ProjectStatusLabel.ascx" tagname="ProjectStatusLabel" tagprefix="psl" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">


        <div class="content" id="app">

                <div class="row">

					<div class="col-sm-6 col-md-6">

							<!-- User card with thumb and social icons at the bottom -->
							<div class="panel">
									<div class="panel-body">
										<div class="row text-center" >
											
											<div class="col-lg-4"  id="divRecordWarning" v-bind:class="{ 'bordered alpha-primary': statusFilter=='RecordWarning' }">
												<a href="" v-on:click="setStatusFilter($event, 'RecordWarning')" class="text-grey">
												<p><i class="icon-warning icon-2x display-inline-block text-danger tada infinite" id="icoRecordWarning" runat="server"></i></p>
												<h5 class="text-semibold no-margin" id="lblCountRecordWarning" runat="server">.</h5>
												<strong class="text-muted text-size-small" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.Warning %>">
													<%= Resources.Resource.Warning %></strong>
												</a>
											</div>

											<div class="col-lg-4"  id="divRecordInvoiced" v-bind:class="{ 'bordered alpha-primary': statusFilter=='RecordInvoiced' }">
												<a href="" v-on:click="setStatusFilter($event, 'RecordInvoiced')" class="text-green">
												<p><i class="icon-file-check icon-2x display-inline-block text-green tada infinite" id="icoRecordInvoiced" runat="server"></i></p>
												<h5 class="text-semibold no-margin" id="lblCountRecordInvoiced" runat="server">.</h5>
												<strong class="text-muted text-size-small" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.RecordsInvoiced_Tooltip %>">
                                                    <%= Resources.Resource.RecordsInvoiced %>
                                                </strong>
												</a>
											</div>
											
											
                                            <div class="col-lg-4"  id="divRecordNotInvoiced" v-bind:class="{ 'bordered alpha-primary': statusFilter=='RecordNotInvoiced' }">
												<a href="" v-on:click="setStatusFilter($event, 'RecordNotInvoiced')" class="text-primary">
                                                <p><i class="icon-stack icon-2x display-inline-block text-primary tada infinite" id="i1" runat="server"></i></p>
                                                <h5 class="text-semibold no-margin" id="lblCountRecordNotInvoiced" runat="server">.</h5>
                                                <strong class="text-muted text-size-small" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.RecordsNotInvoiced_Tooltip %>">
                                                    <%= Resources.Resource.RecordsNotInvoiced %></strong>
												</a>
                                            </div>
										</div>									
									</div>

							    	<div class="panel-footer text-center no-padding">
								    	<div class="row" >
								    		<a href=""  v-on:click="setStatusFilter($event, null)"  class="display-block p-10 text-default" 
								    			data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.ListAllRecords %>">
								    				<i class="fa fa-ellipsis-h"></i> <%= Resources.Resource.Show_all %>
								    				
								    			</a>
								    	</div>
									</div>
								</div>
								<!-- /user card with thumb and social icons at the bottom -->

					</div>

                    <div class="col-sm-6 col-md-6">
								<div class="panel panel-body panel-body-accent">
									<div class="media no-margin">
										
										<div>
											<i class="fa fa-calendar-check-o" aria-hidden="true"></i> <%= Resources.Resource.Select_Period %>
										</div>
										<div>
											<select id="ddlPeriod" class="select" onchange="periodChanged()">
												<%= OptionsPeriod %>
											</select>
										</div>

										<div style="margin-top:20px">
											<i class="fa fa-user-circle-o" aria-hidden="true"></i> <%= Resources.Resource.Customers %>
										</div>
										<div>
											<select id="ddlClient" data-placeholder="<%= Resources.Resource.Select___ %>" onchange="setClientFilter()">
                                                
											</select>
										</div>
										
									</div>
								</div>
							</div>

                </div>

					<div class="row">
                        <div class="col-sm-12 col-md-12">
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="icon-upload display-inline-block text-green"></i>
										<strong><%= ph.CMP.Name %></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

							<div class="panel-body">

								<table id="timesheet-records-datatable" class="table table-hover dataTable">
									<thead>
										<tr>
											<th><input id="header-checkbox" type="checkbox" onclick="checkAllRows(this);" /></th>
											<th><%= Resources.Resource.Invoice %></th>
											<th><%= Resources.Resource.TimeSheet %></th>
											<th><%= Resources.Resource.Date %></th>
											<th><%= Resources.Resource.Duration %></th>
											<th><%= Resources.Resource.Client %></th>
											<th><%= Resources.Resource.Employee %></th>
											<th><%= Resources.Resource.Project %></th>
											<th><%= Resources.Resource.Service %></th>
											<th><%= Resources.Resource.Description %></th>
											<th>&nbsp;</th>
										</tr>
									</thead>
								</table>

                                
                            </div><!-- panel-body -->

                            <div class="panel-footer text-right no-padding">
						    	<div class="row" style="margin:10px">
						    		<button id="btn-create-invoice" type="button" class="btn btn-default createInvoice" onclick="createInvoice()"><%= Resources.Resource.Create_Invoice %></button>
						    	</div>
							</div><!-- panel-footer -->

                        </div>


                    </div>
               </div>
               
            <div id="modal_delete" class="modal fade">
                <div class="modal-dialog  modal-sm">
                    <div class="modal-content">
                        <div class="modal-header bg-danger">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
                        </div>
                        <input type="text" id="RecId" hidden="" />
                        <div class="modal-body" runat="server" ID="warnMessage">
                            <h6 class="text-semibold"><%= Resources.Resource.Are_You_Sure_To_Delete %></h6>
                            <p><%= Resources.Resource.Delete_Is_Irreversible %></p>
                        </div>
                       

                        <div class="modal-footer">
                            <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
                            <button type="button" class="btn btn-primary" onclick="cancelImport()"><%= Resources.Resource.Yes_Delete %></button>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /content -->

    </form>

    <script type="text/javascript">
        var currency = '<%= ph.Currency %>';
    </script>

    <script type="text/javascript" src="Default.js?<%= Guid.NewGuid().ToString("n") %>"></script>
	<%--<script type="text/javascript" src="Default.js"></script>--%>
</asp:Content>

