﻿using Calani.BusinessObjects.CustomerAdmin;
using System;

public partial class Billing_InvoiceTime_Default : System.Web.UI.Page
{
    public PageHelper ph;

    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    public string OptionsPeriod { get; set; }


    protected void Page_Load(object sender, EventArgs e)
    {
        var orgMgr = new OrganizationManager(ph.CurrentOrganizationId);
        orgMgr.Load();

        DateTime yearBegin = new DateTime(DateTime.Now.Year, 1, 1);        
        OptionsPeriod = ph.RenderPeriodsListOption(yearBegin.Ticks + ";*", preselect: orgMgr.DefaultPeriodFilter);

        // page not available for employee
        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            Response.Redirect("~/");
        }
    }

}
