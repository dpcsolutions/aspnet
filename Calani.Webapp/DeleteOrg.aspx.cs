﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DeleteOrg : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblID.Text = PageHelper.GetCurrentOrganizationId(Session).ToString();
        PageHelper ph = new PageHelper(this);
        if (ph.CurrentUserStaff)
        {
            // ok
        }
        else
        {
            Response.Redirect("~/");
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        long org = PageHelper.GetCurrentOrganizationId(Session);
        Calani.BusinessObjects.Membership.DeletionManager mgr = new Calani.BusinessObjects.Membership.DeletionManager(org);
        mgr.Delete();

        Session.Clear();
        Response.Redirect("Login.aspx");
    }
}