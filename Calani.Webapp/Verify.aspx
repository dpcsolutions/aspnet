﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Verify" Codebehind="Verify.aspx.cs" %>
<!DOCTYPE html>
<html lang="en">
<head runat="server">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>GES</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<script type="text/javascript" src="assets/js/pages/login.js"></script>
	<!-- /theme JS files -->

</head>

<body class="login-container">

        <ul class="nav navbar-nav navbar-right">
			<li class="dropdown language-switch">
				<%= LanguageSwitch %>
			</li>
        </ul>

    <form runat="server">
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					
                    <asp:Panel runat="server" ID="PanelSuccess" >
						<div class="row">
							<div class="col-lg-6 col-lg-offset-3">
								<div class="panel registration-form">
									<div class="panel-body">
										<div class="text-center">
											<div class="icon-object border-success text-success"><i class="icon-user-check"></i></div>
											<h5 class="content-group-lg text-success"><%= Resources.Resource.EmailVerified %> 
                                                <small class="display-block"><%= Resources.Resource.EmailVerified_more %></small></h5>
										</div>
                                        <div class="form-group">
                                        <a class="btn bg-blue btn-block"  href="Login.aspx" ><%= Resources.Resource.Login %></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                     <asp:Panel runat="server" ID="PanelError" >
						<div class="row">
							<div class="col-lg-6 col-lg-offset-3">
								<div class="panel registration-form">
									<div class="panel-body">
										<div class="text-center">
											<div class="icon-object border-danger text-danger"><i class="icon-user-cancel"></i></div>
											<h5 class="content-group-lg text-danger"><%= Resources.Resource.ERR_ActivationLink %> 
										</div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>

                    <!-- Footer -->
                    <div class="row  text-muted text-center">
                        &copy; <asp:Label runat="server" ID="LabelCopyYear" />. 
                        <asp:HyperLink runat="server" ID="HyperLinkCopy" />
                    </div>
                    <!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->
        

	</div>
	<!-- /page container -->
    </form>

</body>
</html>
