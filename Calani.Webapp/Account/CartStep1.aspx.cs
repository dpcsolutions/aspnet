﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_Cart_PricingTable : System.Web.UI.Page
{
    public string basePricePlanA = "";
    public string basePricePlanA_12 = "";
    public string optionUserPricePlanA = "";
    public string optionUserPricePlanA_12 = "";

    public string basePricePlanM = "";
    public string optionUserPricePlanM = "";


    int yearlyFreeMonths = 0;

    public string baseCcy = "";
    public string monthlyPlanId = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorPlanId_Monthly"];
    public string yearlyPlanId = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorPlanId_Yearly"];

    public string All_GesMobile_features { get; set; }

    public PageHelper ph = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        

        All_GesMobile_features = Resources.Resource.All_GesMobile_features.Replace("$users", 
        System.Configuration.ConfigurationManager.AppSettings["NewAccountUsersLimit"]);

        panelContent.Visible = true;
        panelPartner.Visible = false;

        ph = new PageHelper(this);

        if (!IsPostBack)
        {
            RefreshCoupon();
        }

        Bind();
    }

    private void Bind()
    {
        monthlyPlanId = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorPlanId_Monthly"];
        yearlyPlanId = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorPlanId_Yearly"];

    Calani.BusinessObjects.Partnership.PartnershipManager pmgr = new Calani.BusinessObjects.Partnership.PartnershipManager(ph.CurrentOrganizationId);
        if (pmgr.AlreadyJoined == true && pmgr.PartnerInvoicing.Enabled == false)
        {
            monthlyPlanId = pmgr.CustomerDiscountPlan.MonthlyPlanId;
            yearlyPlanId = pmgr.CustomerDiscountPlan.YearlyPlanId;






        }

        if (pmgr.AlreadyJoined == true && pmgr.PartnerInvoicing.Enabled == true)
        {
            panelContent.Visible = false;
            panelPartner.Visible = true;
            return;
        }


        if (ph.CurrentUserType != Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
        {
            Response.Redirect("~/");
            return;
        }

        if (String.IsNullOrWhiteSpace(System.Configuration.ConfigurationManager.AppSettings["ShopOnline"])
            || System.Configuration.ConfigurationManager.AppSettings["ShopOnline"].ToLower() != "true")
        {
            Response.Redirect("~/Account/Offline.aspx");
            return;
        }

        if (ph.HasValidPaymentMethod())
        {
            stepPayContent.Visible = false;
            stepPayTitle.Visible = false;
        }



        this.baseCcy = "CHF";




        lnkYearly.InnerText = Resources.Resource.Buy;
        lnkMonthly.InnerText = Resources.Resource.Buy;

        PaymentProcessor pp = new PaymentProcessor();
        var plans = pp.ListPlans();

        var planMonthly = (from r in plans where r.Name == monthlyPlanId select r).FirstOrDefault();
        var planYearly = (from r in plans where r.Name == yearlyPlanId select r).FirstOrDefault();


        if (planMonthly != null)
        {
            this.basePricePlanM = FormatPrice(planMonthly.BasePrice, 1);
            this.optionUserPricePlanM = FormatPrice(planMonthly.UserLicensePrice, 1);
            planMonthlyOption.Visible = true;
        }
        else
        {
            planMonthlyOption.Visible = false;
        }

        if (planYearly != null)
        {
            yearlyFreeMonths = planYearly.Frequency - 12;
            if (yearlyFreeMonths > 0)
            {
                lblFreeMonth.Text = Resources.Resource.FreeMonthsIncluded.Replace("$count", yearlyFreeMonths.ToString());
                lblFreeMonth.Visible = true;
            }
            else
            {
                lblFreeMonth.Visible = false;
            }

            this.basePricePlanA = FormatPrice(planYearly.BasePrice, 12 + yearlyFreeMonths);
            this.basePricePlanA_12 = FormatPrice(planYearly.BasePrice, 1);
            this.optionUserPricePlanA = FormatPrice(planYearly.UserLicensePrice, 12 + yearlyFreeMonths);
            this.optionUserPricePlanA_12 = FormatPrice(planYearly.UserLicensePrice, 1);
            planYearlyOption.Visible = true;
        }
        else
        {
            planYearlyOption.Visible = false;
        }


        var db = new Calani.BusinessObjects.Model.CalaniEntities();
        var subid = (from r in db.organizations where r.id == ph.CurrentOrganizationId select r.subscriptionId).FirstOrDefault();
        if (!String.IsNullOrWhiteSpace(subid))
        {
            var sub = pp.getSubscription(subid);
            if (sub != null && sub.PlanId != null)
            {

                if (sub.PlanId == monthlyPlanId)
                {
                    lnkYearly.Visible = false;
                    lnkYearlyHelp.Visible = true;
                    lnkMonthly.InnerText = Resources.Resource.Update;
                }
                else if (sub.PlanId == yearlyPlanId)
                {
                    lnkMonthly.Visible = false;
                    lnkMonthlyHelp.Visible = true;
                    lnkYearly.InnerText = Resources.Resource.Update;
                }

            }
        }
    }

    private string FormatPrice(double d, double multiplier)
    {
        d /= multiplier;
        d = Math.Round(d, 2, MidpointRounding.AwayFromZero);
        string s = string.Format(System.Globalization.CultureInfo.InvariantCulture , "{0:0,0.00}", d);

        return s;
    }

    // coupon


    
    private void RefreshCoupon()
    {
        Calani.BusinessObjects.Partnership.PartnershipManager mgr = new Calani.BusinessObjects.Partnership.PartnershipManager(ph.CurrentOrganizationId);


        SetStatus(mgr.CanJoin, mgr.Code);
    }
    
    private void SetStatus(bool canregister, string code)
    {
        //btnActivate.Enabled = canregister;
        tbxCode.Enabled = canregister;
        //panelDescriptionNotYet.Visible = canregister;
        //panelDescriptionAlready.Visible = !canregister;

        if (canregister)
        {
            btnActivate.CssClass = "btn btn-success";
            btnActivate.Text = Resources.Resource.Enable;
            tbxCode.Text = "";

        }
        else
        {
            btnActivate.CssClass = "btn btn-danger";
            btnActivate.Text = Resources.Resource.Leave;
            tbxCode.Text = code;

        }
    }
    
    protected void btnActivate_Click(object sender, EventArgs e)
    {
        Calani.BusinessObjects.Partnership.PartnershipManager mgr = new Calani.BusinessObjects.Partnership.PartnershipManager(ph.CurrentOrganizationId);

        if (String.IsNullOrEmpty(mgr.Code))
        {

            if (mgr.Join(tbxCode.Text.Trim()))
            {
                SetStatus(false, tbxCode.Text.Trim());
            }
            RefreshCoupon();
            Bind();
        }
        else
        {
            if (mgr.CanLeave)
            {
                mgr.Leave();
                SetStatus(true, "");
                Bind();
            }
        }
    }
}