﻿<%@ Page Title="Commande" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Account_Cart_PricingTable2" Codebehind="CartStep2.aspx.cs" %>

<%@ Reference VirtualPath="~/Account/UserControl/UCEditSubscription.ascx"%>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"><div class="panel" style="margin-bottom: 40px;">
	<form runat="server" method="post">
     <div class="content">
		<div class="panel panel-white">
        	<div class="panel-heading">
				<h6 class="panel-title">
					<a name="quotes">
						<i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
						<strong><%= Resources.Resource.ShoppingCart %></strong>
					</a>
				</h6>
			</div><!-- panel-heading -->

			<div class="panel-body">
                <div id="wizard">
                    <h1><%= Resources.Resource.ChoosePlan %></h1>
                    <div class="table-responsive"></div>
                    <h1><%= Resources.Resource.Options %></h1>
                    <div class="">
                        <asp:PlaceHolder ID="placeHolderId" runat="server"></asp:PlaceHolder>
                    </div>
                    <h1 id="stepPayTitle" runat="server"><%= Resources.Resource.Pay %></h1>
                    <div class="panel panel-white" id="stepPayContent" runat="server">
			        </div>

                </div>
            </div>
        </div>
    </div>
<script type="text/javascript" src="../assets/js/plugins/forms/wizards/steps.min.js"></script>
<script type="text/javascript">
    $(function () {

        $("#wizard").steps({
            enablePagination:false,
            startIndex: 1,
            onStepChanging: function (e, idNew, idPrev) {
                return false;
            }
           });
    })
</script>
        </form>
</asp:Content>
