﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_Cart_PricingTable3 : System.Web.UI.Page
{
    public PageHelper ph = null;
    protected Account_EditCC pt;
    protected void Page_Load(object sender, EventArgs e)
    {
        ph = new PageHelper(this);
        if (ph.CurrentUserType != Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
        {
            Response.Redirect("~/");
            return; 
        }

        if (String.IsNullOrWhiteSpace(System.Configuration.ConfigurationManager.AppSettings["ShopOnline"])
            || System.Configuration.ConfigurationManager.AppSettings["ShopOnline"].ToLower() != "true")
        {
            Response.Redirect("~/Account/Offline.aspx");
            return;
        }

        pt = new Account_EditCC();
        pt.ph = ph;
        placeHolderId.Controls.Add(pt);
    }
}