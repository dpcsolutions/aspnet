﻿<%@ Control  Language="C#" AutoEventWireup="true" className="Account_EditCC" Inherits="Account_EditCC" Codebehind="UCEditCC.ascx.cs" %>
        <div class="content">
             <div class="row">
                 <div class="col-sm-12 col-md-12">
                    <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
						<div class="panel-heading">
							<h6 class="panel-title"><%= Resources.Resource.ProcessingError %></h6>
						</div>

						<div class="panel-body">
							<asp:Label runat="server" ID="lblErrorMessage" />
						</div>
					</asp:Panel><!-- panelError -->

                    <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
						<div class="panel-heading">
							<h6 class="panel-title"><%= Resources.Resource.UpdateDone %></h6>
						</div>

						<div class="panel-body">
							<asp:Label runat="server" ID="lblSuccess" /> &nbsp;<asp:LinkButton href="Default.aspx"  id="btnBackToSummaryId" runat="server" Text="Retour à l'abonnement" ></asp:LinkButton>
                    	</div>
					</asp:Panel><!-- panelSuccess -->
					
	                <asp:Panel runat="server" ID="panelEdit" class="panel panel-white">
						<div class="panel-heading">
                             <h6 class="panel-title">
					            <a name="quotes">
						            <i class="icon-credit-card"></i>
						            <strong><%= Resources.Resource.PaymentMethod %></strong>
					            </a>
				            </h6>
                        </div>

						<div class="panel-body">
                            <table class="table ">
	                            <tbody>
		                            <tr><td style="border-top-style:none">
                                        <div id="dropin-container"></div>
                                        </td></tr>
                                        <tr><td align="right">
                                        <a id="BtnFinaliseId" class="btn disabled bg-green legitRipple">
                                            <i class="icon-cart position-left"></i><%= Resources.Resource.SetPaymentMethod %> </a>
		                                </td></tr>
                                    </tbody>
                            </table>
                            <div style="display:none"><asp:LinkButton runat="server" id="BtnHandlNonceId" OnClick="BtnHandlNonceId_Click"></asp:LinkButton></div>
		                    <input runat="server" type="hidden" id="nonceId" />
                        </div>
                    </asp:Panel>
		         </div>
            </div>
        </div>
    <script type="text/javascript">

        var createErrX = null;
        var dropinInstanceX = null;


        $(function () {

            var data = '<%=token%>';
            var displayCCards = '<%=displayCCards%>' == 'True';

            var lang = lang4.replace('-', '_');

            var req = {
                authorization: data,
                container: '#dropin-container',
                locale: lang
            };

            $("#BtnFinaliseId").removeAttr('disabled');
            $("#BtnFinaliseId").removeClass('disabled');

            if (displayCCards)
            {
               // $("#BtnFinaliseId").attr('disabled', true);

                braintree.dropin.create(req, function (createErr, dropinInstance) {
                    createErrX = createErr;
                    dropinInstanceX = dropinInstance;
                    
                    if (!createErrX)
                    {
                        dropinInstanceX.on('paymentMethodRequestable', function (event) {
                            $("#BtnFinaliseId").removeAttr('disabled');
                            $("#BtnFinaliseId").removeClass('disabled');
                        });

                        dropinInstance.on('noPaymentMethodRequestable', function () {
                            $("#BtnFinaliseId").attr('disabled', true);
                            $("#BtnFinaliseId").addClass('disabled');
                        });

                        dropinInstance.on("paymentOptionSelected", function () {
                            $("#BtnFinaliseId").removeAttr('disabled');
                            $("#BtnFinaliseId").removeClass('disabled');
                        });

                        $("#BtnFinaliseId").click(function (e) {
                            e.preventDefault(); 
                                window.swal({
                                    title: "<%= Resources.Resource.Validation %>...",
                                    text: "<%= Resources.Resource.PleaseWait %>",
                                    imageUrl: "Ajaxloader.gif",
                                    showConfirmButton: false,
                                    allowOutsideClick: false
                                });

                            if (dropinInstanceX.isPaymentMethodRequestable) {
                                dropinInstanceX.requestPaymentMethod(function (err, payload) {

                                    setTimeout(function () {
                                        var nonce = (err == null && payload != null ? payload.nonce : undefined);
                                        $("#ContentPlaceHolder1_ctl01_nonceId").val(nonce);
                                        __doPostBack('ctl00$ContentPlaceHolder1$ctl01$BtnHandlNonceId', '');
                                    }, 1000);
                                });
                            }
                         });
                    }
            });
        }
    });
</script>
