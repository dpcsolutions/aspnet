﻿using System;
using System.Linq;

public partial class Account_UCEditSubscription : System.Web.UI.UserControl
{
    public PageHelper ph = null;
    string subsId = "";
    string isoCCy = "CHF";
    decimal addinPrice = 0;
    decimal subsPrice = 0;
    decimal addinQuantity = 0;
    string planId = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorPlanId_Monthly"];

    string monthlyPlanId = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorPlanId_Monthly"];
    string yearlyPlanId = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorPlanId_Yearly"];
    public string monthlyExtraUserId = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorAddinId_MonthlyExtraUser"];
    public string yearlyExtraUserId = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorAddinId_YearlyExtraUser"];


    public bool DoNotNeedToChangePayment { get; set; }

    public string IncUsers { get; set; }



    private bool hasRightToManageSubscription(string subsId)
    {
        IncUsers = Resources.Resource.Inc2Users.Replace("$users",
        System.Configuration.ConfigurationManager.AppSettings["NewAccountUsersLimit"]);

        PaymentProcessor pp = new PaymentProcessor();
        
        var s = pp.getSubscription(subsId);
        var customer = pp.getCustomer(PaymentProcessor.getClientId(ph.CurrentOrganizationId));

        bool foundSubscription = false;

        //make sure the user has the right to change this subscription
        foreach (var c in customer.CreditCards)
        {
            foreach (var ss in c.Subscriptions)
                if (ss.Id == subsId)
                {
                    foundSubscription = true;
                    break;
                }

            if (foundSubscription)
                break;
        }

        if (!foundSubscription)
            ph.DisplayError(Resources.Resource.InternalErrorOccurs_PleaseCall);

        return foundSubscription;
    }

    private void GetPlanAndPrices(PaymentProcessor pp)
    {
        if (Request.Params["plan"] != null)
        {
            if (Request.Params["plan"] == "2")
            {
                planId = monthlyPlanId;
            }
            else if (Request.Params["plan"] == "1")
            {
                planId = yearlyPlanId;

            }
        }


        var plans = pp.ListPlans();

        var btPlan = (from r in plans where r.Name == planId select r).FirstOrDefault();


        subsPrice = Convert.ToDecimal(btPlan.BasePrice);
        addinPrice = Convert.ToDecimal(btPlan.UserLicensePrice);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (ph.CurrentUserType != Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
        {
            Response.Redirect("~/");
            return;
        }

        Calani.BusinessObjects.Partnership.PartnershipManager pmgr = new Calani.BusinessObjects.Partnership.PartnershipManager(ph.CurrentOrganizationId);
        if (pmgr.AlreadyJoined == true && pmgr.PartnerInvoicing.Enabled == false)
        {
            monthlyPlanId = pmgr.CustomerDiscountPlan.MonthlyPlanId;
            yearlyPlanId = pmgr.CustomerDiscountPlan.YearlyPlanId;

            PaymentProcessor pp2 = new PaymentProcessor();
            var plans = pp2.ListPlans();
            foreach (var item in plans)
            {
                if (item.Name == monthlyPlanId) monthlyExtraUserId = item.UserLicenseId;
                if (item.Name == yearlyPlanId) yearlyExtraUserId = item.UserLicenseId;
            }
        }


        ph = new PageHelper(this.Page);
        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
                null, null, null, null, null, null);

        PaymentProcessor pp = new PaymentProcessor();
        GetPlanAndPrices(pp);
        ToggleUIMode();








        if (!IsPostBack)
        {
            Recalculate2();
        }

    }


    private void Recalculate2()
    {
        Recalculate();

        decimal minimum = (decimal.Parse(txtQuantityAddinId.Text));
        int current = GetCurrentLicensesCount();
        if (current > minimum)
        {
            txtQuantityAddinId.Text = current.ToString();
            Recalculate();
        }
        minimum = Convert.ToDecimal(System.Configuration.ConfigurationManager.AppSettings["NewAccountUsersLimit"]);
        if (current < minimum)
        {
            txtQuantityAddinId.Text = minimum.ToString();
            Recalculate();
        }
    }


    private int GetCurrentLicensesCount()
    {
        var db = new Calani.BusinessObjects.Model.CalaniEntities();
        var x = (from r in db.organizations where r.id == ph.CurrentOrganizationId select r.userLimit).FirstOrDefault();
        int ret = 0;
        if (x != null) ret = x.Value;
        return ret;
    }

    private int GetCurrentActiveUsersCount()
    {
        Calani.BusinessObjects.Contacts.EmployeesManager mgr = new Calani.BusinessObjects.Contacts.EmployeesManager(ph.CurrentOrganizationId);
        var users = mgr.List();
        int currentUsersCount = (from r in users where r.type != 0 select r).Count() - 1;
        return currentUsersCount;
    }

    private void Recalculate()
    {
        int currentUsersCount = GetCurrentActiveUsersCount();

        decimal quantity = (decimal.Parse(txtQuantityAddinId.Text));


        decimal payedQuantity = quantity;
        payedQuantity -= Convert.ToDecimal(System.Configuration.ConfigurationManager.AppSettings["NewAccountUsersLimit"]);
        payedQuantity = Math.Max(0, payedQuantity);


        if (quantity < currentUsersCount)
        {
            quantity = currentUsersCount;
            txtQuantityAddinId.Text = quantity.ToString();
        }
        lblAddinTotalId.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0,0.00}", addinPrice * payedQuantity) + " " + isoCCy;
        lblGrandTotalId.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0,0.00}", (addinPrice * payedQuantity + subsPrice)) + " " + isoCCy;

        lblAddinPriceId.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0,0.00}", addinPrice) + " " + isoCCy;
        lblBasePlanPriceId1.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0,0.00}", subsPrice) + " " + isoCCy;
        lblBasePlanPriceId2.Text = lblBasePlanPriceId1.Text;


        lblPlanId.Text = Request.Params["plan"].ToLower().Contains("1") ? Resources.Resource.Yearly : Resources.Resource.Monthly;
        lblPlanId2.Text = lblPlanId.Text;

        


        PaymentProcessor pp = new PaymentProcessor();
        var db = new Calani.BusinessObjects.Model.CalaniEntities();
        var subid = (from r in db.organizations where r.id == ph.CurrentOrganizationId select r.subscriptionId).FirstOrDefault();
        if (!String.IsNullOrWhiteSpace(subid))
        {
            var sub = pp.getSubscription(subid);
            if (sub != null && sub.PlanId != null)
            {
                lblExistingBasePlanPriceId1.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0,0.00}", sub.Price) + " " + isoCCy;
                lblExistingBasePlanPriceId2.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0,0.00}", sub.Price) + " " + isoCCy;
                lblExistingGrandTotalId.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0,0.00}", sub.Price) + " " + isoCCy;

                BindDifferences(lblDiffrencePlanPrice, sub.Price, subsPrice);
                BindDifferences(lblDiffrenceGrandTotal, sub.Price, subsPrice);


                var option = sub.AddOns.FirstOrDefault();
                if (option != null)
                {
                    lblExistingAddinPriceId.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0,0.00}", option.Amount) + " " + isoCCy;
                    if (option.Quantity != null)
                    {
                        lblExistingAddinQuantity.Text = option.Quantity.Value.ToString();
                        lblExistingAddinTotalId.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0,0.00}", option.Amount * option.Quantity) + " " + isoCCy;
                        lblExistingGrandTotalId.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0,0.00}", sub.Price + (option.Amount * option.Quantity)) + " " + isoCCy;

                        BindDifferences(lblDiffrenceAddins, option.Amount * option.Quantity, addinPrice * quantity);
                        BindDifferences(lblDiffrenceGrandTotal, sub.Price + (option.Amount * option.Quantity), subsPrice + addinPrice * quantity);
                    }
                }



                
                

            }
        }


    }


    private void BindDifferences(System.Web.UI.WebControls.Label lbl, decimal? currentVal, decimal? newVal)
    {
        double n = 0;
        double c = 0;
        if (newVal != null) n = Convert.ToDouble(newVal.Value);
        if (c != null) c = Convert.ToDouble(currentVal.Value);

        double d = n - c;

        lbl.Text = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0,0.00}", d) + " " + isoCCy;
        if (!lbl.Text.StartsWith("-")) lbl.Text = "+" + lbl.Text;
        if (d > 0)
        {
            lbl.ForeColor = System.Drawing.Color.Red;
        }
        else
        {
            lbl.ForeColor = System.Drawing.Color.Green;
        }
    }



    protected void BtnPlanConfirmId_Click(object sender, EventArgs e)
    {
        Recalculate();


        PaymentProcessor pp = new PaymentProcessor();
        Braintree.Subscription s = null;
        Braintree.Customer customer = pp.getCustomer(ph.CurrentOrganizationId.ToString());

        Calani.BusinessObjects.Model.CalaniEntities db = new Calani.BusinessObjects.Model.CalaniEntities();
        subsId = (from r in db.organizations where r.id == ph.CurrentOrganizationId select r.subscriptionId).FirstOrDefault();

        if (!String.IsNullOrEmpty(subsId))
        {
            s = pp.getSubscription(subsId);
        }

        if (s != null)
        {
            if (s.Status != Braintree.SubscriptionStatus.CANCELED)
            {
                subsId = s.Id;
                if (!hasRightToManageSubscription(s.Id))
                {
                    ph.DisplayError(Resources.Resource.InternalErrorOccurs_PleaseCall);
                    return;
                }
            }
        }

        string ret = null;

        if(String.IsNullOrWhiteSpace(subsId))
        {
            // new plan
            //Session.Add("CartSubscription", subsId);
            Session.Add("CartPlan", planId);
            Session.Add("CartLicenses", txtQuantityAddinId.Text);
            Response.Redirect("CartStep3.aspx?"+ Guid.NewGuid().ToString("n")); // just for fun
        }
        else
        {
            // updatecurrent plan

            // NE FAIT RIEN DU TOUT

            var subs = pp.getSubscription(subsId);

            planId = subs.PlanId; // can not change plan type


            int newQtyYearly = 0;
            int newQtyMonthly = 0;
            if (subs.PlanId.ToLower().Contains("yearly"))
            {
                newQtyYearly = int.Parse(txtQuantityAddinId.Text);
                newQtyYearly -= Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NewAccountUsersLimit"]);
                newQtyYearly = Math.Max(0, newQtyYearly);
            }
            if (subs.PlanId.ToLower().Contains("monthly"))
            {
                newQtyMonthly = int.Parse(txtQuantityAddinId.Text);
                newQtyMonthly -= Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NewAccountUsersLimit"]);
                newQtyMonthly = Math.Max(0, newQtyMonthly);
            }
           

            ret = pp.updateSubscriptionForClient(ph.CurrentOrganizationId, subsId, null, planId,
                   newQtyYearly,
                   newQtyMonthly,
                   yearlyExtraUserId, monthlyExtraUserId);

            if (ret != null)
            {
                //store changes in DB
                ph.DisplaySuccess(Resources.Resource.SubscriptionUpdated);

                BtnPlanConfirmId.Visible = false;
                txtQuantityAddinId.Enabled = false;

                db = new Calani.BusinessObjects.Model.CalaniEntities();
                Calani.BusinessObjects.Model.organizations theorg = (from r in db.organizations where r.id == ph.CurrentOrganizationId select r).FirstOrDefault();
                theorg.subscriptionId = ret;
                theorg.userLimit = int.Parse(txtQuantityAddinId.Text);
                db.SaveChanges();

                ph.UpdateSubscription();

            }
            else
            {
                ph.DisplayError(Resources.Resource.SubscriptionCanNotBeenUpdated);
            }

        }


       
        
    }


    private void UpdateUIMode(bool upgrade)
    {
        lbl_action_update.Visible = upgrade;
        lbl_action_buy.Visible = !upgrade;
        div_update_terms.Visible = upgrade;
        cell_upgrade_title.Visible = upgrade;
        cell_upgrade_title_r.Visible = upgrade;
        cell_upgrade_title2a.Visible = upgrade;
        cell_upgrade_title2b.Visible = upgrade;
        cell_upgrade_title2c.Visible = upgrade;
        cell_upgrade_title2_r.Visible = upgrade;
        cell_upgrade_base1.Visible = upgrade;
        cell_upgrade_base2.Visible = upgrade;
        cell_upgrade_base3.Visible = upgrade;
        cell_upgrade_base_r.Visible = upgrade;
        cell_upgrade_addin1.Visible = upgrade;
        cell_upgrade_addin2.Visible = upgrade;
        cell_upgrade_addin3.Visible = upgrade;
        cell_upgrade_addin_r.Visible = upgrade;
        cell_upgrade_separator.Visible = upgrade;
        cell_upgrade_separator_r.Visible = upgrade;
        cell_upgrade_total1.Visible = upgrade;
        cell_upgrade_total2.Visible = upgrade;
        cell_upgrade_total_r.Visible = upgrade;
        cell_upgrade_freq1.Visible = upgrade;
        cell_upgrade_freq2.Visible = upgrade;
        cell_upgrade_freq_r.Visible = upgrade;
        cell_upgrade_footer.Visible = upgrade;
    }

    private void ToggleUIMode()
    {
        bool is_upgrade = false;

        PaymentProcessor pp = new PaymentProcessor();
        Braintree.Subscription s = null;
        Braintree.Customer customer = pp.getCustomer(ph.CurrentOrganizationId.ToString());

        Calani.BusinessObjects.Model.CalaniEntities db = new Calani.BusinessObjects.Model.CalaniEntities();
        subsId = (from r in db.organizations where r.id == ph.CurrentOrganizationId select r.subscriptionId).FirstOrDefault();

        if (!String.IsNullOrEmpty(subsId))
        {
            s = pp.getSubscription(subsId);
        }
        

        if (s != null)
        {
            if (s.Status != Braintree.SubscriptionStatus.CANCELED)
            {
                subsId = s.Id;
                is_upgrade = true;
                if (!hasRightToManageSubscription(s.Id))
                {
                    ph.DisplayError(Resources.Resource.InternalErrorOccurs_PleaseCall);
                    return;
                }
            }
        }


        UpdateUIMode(is_upgrade);

    }

    protected void btnRecalculate_Click(object sender, EventArgs e)
    {
        Recalculate2();
    }
}