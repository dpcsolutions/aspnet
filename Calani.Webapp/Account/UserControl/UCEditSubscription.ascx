﻿<%@ Control Language="C#" AutoEventWireup="true" className="Account_UCEditSubscription" Inherits="Account_UCEditSubscription" Codebehind="UCEditSubscription.ascx.cs" %>
    <div class="content">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
					<div class="panel-heading">
						<h6 class="panel-title"><%= Resources.Resource.ProcessingError %></h6>
					</div>

					<div class="panel-body">
						<asp:Label runat="server" ID="lblErrorMessage" />
					</div>
				</asp:Panel><!-- panelError -->

                <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
					<div class="panel-heading">
						<h6 class="panel-title"><%= Resources.Resource.UpdateDone %></h6>
					</div>

					<div class="panel-body">
						<asp:Label runat="server" ID="lblSuccess" /> &nbsp;<asp:LinkButton href="Default.aspx"  id="btnBackToSummaryId" runat="server" Text="Retour à l'abonnement" ></asp:LinkButton>
                    </div>
				</asp:Panel><!-- panelSuccess -->
					
	            <asp:Panel runat="server" ID="panelEdit" class="panel panel-white">
					<div class="panel-heading">
                        <h6 class="panel-title">
                            <%= Resources.Resource.Subscription %>
                        </h6>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
                            <style type="text/css">
                                .activeCell { background: #f7fbf1; }
                                .activeCellAlt { background: #f2f7ec; }
                                .oldCell { background: #f5f5f5; }
                                .oldCellAlt { background: #f2f2f2; }
                            </style>
                            <table class="table table-hover table-striped table-bordered text-center">
	                            <thead>
                                    <tr>

                                        <th></th>

                                        <th colspan="3" class="oldCellAlt" runat="server" id="cell_upgrade_title"><%= Resources.Resource.CurrentPackage %></th>

                                        <th colspan="3" class="activeCellAlt"><%= Resources.Resource.NewPackage %></th>

                                        <th runat="server" id="cell_upgrade_title_r"></th>

                                    </tr>
		                            <tr>
			                            <th>Description</th>
                                        <th class="text-center oldCell" runat="server" id="cell_upgrade_title2a">
				                            <h6 class="mt-5 mb-5"><%= Resources.Resource.Unit_Price %></h6>
			                            </th>
			                            <th class="text-center oldCell"  runat="server" id="cell_upgrade_title2b">
				                            <h6 class="mt-5 mb-5"><%= Resources.Resource.Qty %></h6>
			                            </th>
			                            <th class="text-center oldCell"  runat="server" id="cell_upgrade_title2c">
				                            <h6 class="mt-5 mb-5"><%= Resources.Resource.Total %></h6>
			                            </th>

                                        <th class="text-center activeCell">
				                            <h6 class="text-semibold mt-5 mb-5"><%= Resources.Resource.Unit_Price %></h6>
			                            </th>
			                            <th class="text-center activeCell">
				                            <h6 class="text-semibold mt-5 mb-5"><%= Resources.Resource.Qty %></h6>
			                            </th>
			                            <th class="text-center activeCell">
				                            <h6 class="text-semibold mt-5 mb-5"><%= Resources.Resource.Total %></h6>
			                            </th>
                                        <th class="text-center"  runat="server" id="cell_upgrade_title2_r">
				                            <h6 class="text-semibold mt-5 mb-5">Différence</h6>
			                            </th>

		                            </tr>
	                            </thead>
	                            <tbody>
		                            <tr>
			                            <td class="text-left">
                                            <%= Resources.Resource.Subscription %> 
                                            (<%= IncUsers %>)
			                            </td>
                                        <td class="oldCellAlt" runat="server" id="cell_upgrade_base1"><asp:Label runat="server" ID="lblExistingBasePlanPriceId1"></asp:Label></td>
                                        <td class="oldCellAlt"  runat="server" id="cell_upgrade_base2">1</td>
                                        <td class="oldCellAlt"  runat="server" id="cell_upgrade_base3"><asp:Label runat="server" ID="lblExistingBasePlanPriceId2"></asp:Label></td>
                                        <td class="activeCellAlt"><asp:Label runat="server" ID="lblBasePlanPriceId1"></asp:Label></td>
                                        <td class="activeCellAlt">1</td>
			                            <td class="basePlanPrice activeCellAlt"><asp:Label runat="server" ID="lblBasePlanPriceId2"></asp:Label></td>
                                        <td  runat="server" id="cell_upgrade_base_r"><asp:Label runat="server" ID="lblDiffrencePlanPrice"></asp:Label></td>
                                    </tr>
		                            <tr>
			                            <td class="text-left">
                                            <%= Resources.Resource.UserLicenses %>
                                            <br />
                                            <span class="text-green"><strong>Les 5 premiers utilisateurs sont gratuits</strong></span>
			                            </td>
                                        <td class="oldCell" runat="server" id="cell_upgrade_addin1"><asp:Label runat="server" ID="lblExistingAddinPriceId"></asp:Label></td>
                                        <td class="oldCell" runat="server" id="cell_upgrade_addin2"><asp:Label runat="server" ID="lblExistingAddinQuantity"></asp:Label></td>
                                        <td class="oldCell" runat="server" id="cell_upgrade_addin3"><asp:Label runat="server" ID="lblExistingAddinTotalId"></asp:Label></td>
			                            <td class="activeCell"><asp:Label runat="server" ID="lblAddinPriceId"></asp:Label></td>
                                        <td class="activeCell">

                                            <asp:Label runat="server" ID="lblCurrentQuantity" />
                                            <asp:TextBox runat="server" onkeypress="return functionx(event)"  TextMode="Number" Text="0" id="txtQuantityAddinId" style="text-align:center;width:50px;"/>
                                            <asp:LinkButton runat="server" ID="btnRecalculate" OnClick="btnRecalculate_Click" CssClass="btn text-primary btn-flat btn-icon " ToolTip="Recalculate">
                                                <i class="fa fa-refresh"></i>
                                            </asp:LinkButton>
                                        </td>
			                            <td class="activeCell"><asp:Label runat="server" ID="lblAddinTotalId"></asp:Label></td>
                                        <td  runat="server" id="cell_upgrade_addin_r"><asp:Label runat="server" ID="lblDiffrenceAddins"></asp:Label></td>
		                            </tr>
		                            <tr>
                                        <td></td>			                            
                                        <td colspan="3" class="text-left oldCellAlt" runat="server" id="cell_upgrade_separator"></td>
                                        <td colspan="3" class="text-left activeCellAlt"></td>
                                        <td  runat="server" id="cell_upgrade_separator_r"></td>
		                            </tr>
		                            <tr>
			                            <td class="text-left"><strong><%= Resources.Resource.Total_with_VAT %></strong></td>
			                            <td class="oldCell" colspan="2" runat="server" id="cell_upgrade_total1"></td>
                                        <td class="oldCell" runat="server" id="cell_upgrade_total2"><asp:Label runat="server" ID="lblExistingGrandTotalId"></asp:Label></td>
                                        <td class="activeCell" colspan="2"><strong>Prix du nouveau forfait</strong></td>
			                            <td class="activeCell"><strong><asp:Label runat="server" ID="lblGrandTotalId"></asp:Label></strong></td>
                                        <td runat="server" id="cell_upgrade_total_r"><asp:Label runat="server" ID="lblDiffrenceGrandTotal"></asp:Label></td>
		                            </tr>
                                    <tr>
			                            <td class="text-left"><strong>Fréquence</strong></td>
			                            <td class="oldCell" colspan="2" runat="server" id="cell_upgrade_freq1"></td>
                                        <td class="oldCell" runat="server" id="cell_upgrade_freq2"><asp:Label runat="server" ID="lblPlanId"></asp:Label></td>
                                        <td class="activeCell" colspan="2"></td>
			                            <td class="activeCell"><strong><asp:Label runat="server" ID="lblPlanId2"></asp:Label></strong></td>
                                        <td runat="server" id="cell_upgrade_freq_r"><asp:Label runat="server" ID="Label3"></asp:Label></td>
		                            </tr>
                                    <tr>
			                            <td class="text-left"></td>
			                            <td class="text-left" colspan="3" runat="server" id="cell_upgrade_footer"></td>
			                            <td class="text-left" colspan="3"></td>
			                            <td>
                                            <asp:LinkButton runat="server" ID="BtnPlanConfirmId" CssClass="btn bg-green legitRipple" OnClick="BtnPlanConfirmId_Click">
                                                <i class="icon-cart position-left"></i> 

                                                <span runat="server" id="lbl_action_update"><%= Resources.Resource.UpdatePackage %></span>
                                                <span runat="server" id="lbl_action_buy"><%= Resources.Resource.Buy %></span>

                                            </asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="BtnRecalculate2" CssClass="btn bg-primary legitRipple" OnClick="btnRecalculate_Click" style="display:none"><i class="fa fa-refresh position-left"></i> <%= Resources.Resource.Recalculate %></asp:LinkButton>


			                            </td>
                                
		                            </tr>
	                            </tbody>
                            </table>
                            <div id="div_update_terms" runat="server">
                                <h5><%= Resources.Resource.ProrataPaymentModality %></h5>
                                <ul>
                                    <li>
                                        <%= Resources.Resource.ProrataPaymentModality_Inc %>
                                    </li>
                                    <li>
                                        <%= Resources.Resource.ProrataPaymentModality_Dec %>
                                    </li>
                                </ul>
                            </div>
                        </div>
					</div>
                </asp:Panel>
			    </div>
        </div>
    </div>
    <script type = "text/javascript">
        function functionx(evt) 
        {
            if (evt.charCode > 31 && (evt.charCode < 48 || evt.charCode > 57))
                return false;
        }

        $(function () {

            function needRecalculation() {
                $('#<%= BtnPlanConfirmId.ClientID %>').hide();
                $('#<%= BtnRecalculate2.ClientID %>').show();
                $('#<%= lblAddinTotalId.ClientID %>').css("text-decoration", "line-through");
                $('#<%= lblGrandTotalId.ClientID %>').css("text-decoration", "line-through");
                $('#<%= lblAddinTotalId.ClientID %>').fadeOut("slow");
                $('#<%= lblGrandTotalId.ClientID %>').fadeOut("slow");
            }

            $("#<%= txtQuantityAddinId.ClientID %>").change(function (e) {

                needRecalculation();

            });

            $("#<%= txtQuantityAddinId.ClientID %>").keyup(function (e) {
                
                needRecalculation();

            });
        })
    </script>