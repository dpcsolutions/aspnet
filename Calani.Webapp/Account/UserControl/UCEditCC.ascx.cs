﻿using System;
using System.Linq;
public partial class Account_EditCC : System.Web.UI.UserControl
{
    public string token = "";
    public bool displayCCards = true;
    string subsId = null;
    string licence = null;
    string plan = null;
    public bool hideCC = false;

    public string monthlyPlanId = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorPlanId_Monthly"];
    public string yearlyPlanId = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorPlanId_Yearly"];
    public string monthlyExtraUserId = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorAddinId_MonthlyExtraUser"];
    public string yearlyExtraUserId = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorAddinId_YearlyExtraUser"];


    public PageHelper ph = null;

    public bool NoSubscriptionMode { get; set; }

    protected void handleError(string msg, bool enableFinaliseBtn = false, bool displayCC = false)
    {
        ph.DisplayError(msg);
        displayCCards = displayCC;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (ph.CurrentUserType != Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
        {
            Response.Redirect("~/");
            return;
        }

        Calani.BusinessObjects.Partnership.PartnershipManager pmgr = new Calani.BusinessObjects.Partnership.PartnershipManager(ph.CurrentOrganizationId);
        if (pmgr.AlreadyJoined == true && pmgr.PartnerInvoicing.Enabled == false)
        {
            monthlyPlanId = pmgr.CustomerDiscountPlan.MonthlyPlanId;
            yearlyPlanId = pmgr.CustomerDiscountPlan.YearlyPlanId;

            PaymentProcessor pp2 = new PaymentProcessor();
            var plans = pp2.ListPlans();
            foreach (var item in plans)
            {
                if (item.Name == monthlyPlanId) monthlyExtraUserId = item.UserLicenseId;
                if (item.Name == yearlyPlanId) yearlyExtraUserId = item.UserLicenseId;


            }



            //monthlyExtraUserId
        }


        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
                null, null, null, null, null, null);

        PaymentProcessor pp = new PaymentProcessor();

        Calani.BusinessObjects.Model.CalaniEntities db = new Calani.BusinessObjects.Model.CalaniEntities();
        subsId = (from r in db.organizations where r.id == ph.CurrentOrganizationId select r.subscriptionId).FirstOrDefault();

        if (!String.IsNullOrWhiteSpace(subsId))
        {

            var sub = pp.getSubscription(subsId);
            if(sub != null && sub.Status != Braintree.SubscriptionStatus.CANCELED)
            {
                subsId = sub.Id;
            }
        }

        if (Session["CartPlan"] != null) plan = Session["CartPlan"].ToString();
        if (Session["CartLicenses"] != null) licence = Session["CartLicenses"].ToString();

        //check organisation and user type

        if(subsId != null)
            token = pp.getClientToken(ph.CurrentOrganizationId.ToString());
        else
        {
            token = pp.getClientToken(ph.CurrentOrganizationId.ToString());

            if(String.IsNullOrEmpty(token))
                token = pp.getClientToken();
        }

        if(String.IsNullOrEmpty(token))
            handleError(Resources.Resource.UnknownCustAccount_PleaseContact);
    }
    
    protected void BtnHandlNonceId_Click(object sender, EventArgs e)
    {
        if (nonceId.Value != "")
        {
           if(subsId == null && NoSubscriptionMode == false)
           {
                //get user
                Calani.BusinessObjects.Contacts.EmployeesManager mgr = new Calani.BusinessObjects.Contacts.EmployeesManager(ph.CurrentOrganizationId);
                var contact = mgr.Get(ph.CurrentUserId);

                //create new subscription
                PaymentProcessor pp = new PaymentProcessor();


                string planId = monthlyPlanId;
                int? monthlyLicenses = int.Parse(licence);
                int? yearlyLicenses = null;
                if (!String.IsNullOrEmpty(plan) && plan == yearlyPlanId)
                {
                    planId = yearlyPlanId;
                    monthlyLicenses = null;
                    yearlyLicenses = int.Parse(licence);
                }


                try
                {
                    subsId = pp.createSubscriptionForClient(ph.CurrentOrganizationId,
                        contact.firstName,
                        contact.lastName,
                        contact.primaryEmail,
                        contact.organizations.name,
                        contact.primaryPhone,
                        nonceId.Value,
                        planId,
                        yearlyLicenses,
                        monthlyLicenses,
                        yearlyExtraUserId,
                        monthlyExtraUserId
                        );
                }
                catch(Exception ex)
                {
                    handleError(Resources.Resource.YouSubscriptionCanNotBeenCreated_PleaseContact, false, true);
                    subsId = null;
                }

                if (subsId != null)
                {
                    Calani.BusinessObjects.Model.CalaniEntities db = new Calani.BusinessObjects.Model.CalaniEntities();
                    Calani.BusinessObjects.Model.organizations theorg = (from r in db.organizations where r.id == ph.CurrentOrganizationId select r).FirstOrDefault();
                    theorg.subscriptionId = subsId;
                    theorg.userLimit = int.Parse(licence);
                    db.SaveChanges();
                    ph.UpdateSubscription();


                    string email = (from r in db.contacts where r.id == ph.CurrentUserId select r.primaryEmail).FirstOrDefault();

                    if (!String.IsNullOrWhiteSpace(email))
                    {
                        Calani.BusinessObjects.Generic.MailSender mailSender = new Calani.BusinessObjects.Generic.MailSender();
                        mailSender.MailTo.Email = email;
                        mailSender.MailTo.UserId = ph.CurrentUserId;
                        mailSender.SendSmartHtmlEmail("ges_05_remerciement_abonnement.html", "thanksforpurchase");
                    }



                    ph.DisplaySuccess(Resources.Resource.YourSubsriptionIsNowActive);
                    panelEdit.Visible = false;

                }
                else
                {
                    handleError(Resources.Resource.YouSubscriptionCanNotBeenCreated_PleaseContact, false, true);
                }
            }
            else
            {
                //try to change payment method for subscription
                PaymentProcessor pp = new PaymentProcessor();
                var paymentMethodToken = pp.getPaymentMethodToken(PaymentProcessor.getClientId(ph.CurrentOrganizationId), nonceId.Value);

                try
                {
                    if (pp.updatePaymentMethod(subsId, paymentMethodToken))
                    {
                        ph.DisplaySuccess(Resources.Resource.PaymentMethod_Updated);
                    }
                    else
                    {
                        handleError(Resources.Resource.YouSubscriptionCanNotBeenUpdated_PleaseContact, false, true);
                    }
                }
                catch
                {
                    handleError(Resources.Resource.YouSubscriptionCanNotBeenUpdated_PleaseContact, false, true);
                }
            }
           
        }
        else
            handleError(Resources.Resource.PaymentMethod_Rejected, false, true);
    }

    protected void BtnFinaliseId_Click(object sender, EventArgs e)
    {

    }

}