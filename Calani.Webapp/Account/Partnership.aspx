﻿<%@ Page Title="Partnership" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Account_Partnership" Codebehind="Partnership.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server" method="post">
        <div class="content">
		    <div class="panel panel-white">
        	    <div class="panel-heading">
				    <h6 class="panel-title">
					    <a name="quotes">
						    <i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
						    <strong><%= Resources.Resource.Partnership %></strong>
					    </a>
				    </h6>
			    </div><!-- panel-heading -->

			    <div class="panel-body">
                    <asp:Panel runat="server" ID="panelDescriptionNotYet">
                        <p>
                            <%= Resources.Resource.Partnership_description %>
                        </p>
                        <p>
                            <%= Resources.Resource.Partenrship_fillcode %>
                        </p>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="panelDescriptionAlready">
                        <p><%= Resources.Resource.Partnership_already_description %></p>
                    </asp:Panel>
                    
					<div>
						<div class="input-group">
							<span class="input-group-addon"><i class="icon-ticket"></i></span>
                            <asp:TextBox runat="server" ID="tbxCode" CssClass="form-control"></asp:TextBox>
							
						</div>
					</div>
                    <div style="margin-top:15px">

                        <asp:LinkButton runat="server" CssClass="btn btn-success" ID="btnActivate" OnClick="btnActivate_Click" Text="Click here" />
                    </div>
                </div>
        </div>
    </form>
</asp:Content>

