﻿<%@ Page Title="Commande" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Account_Cart_PricingTable" Codebehind="CartStep1.aspx.cs" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <form runat="server">
	
   <div class="content" runat="server" id="panelPartner" visible="False">
       <div class="panel panel-white">
        	<div class="panel-heading">
				<h6 class="panel-title">
					<a name="quotes">
						<i class="icon-ticket display-inline-block text-<%= ph.CMP.Color %>"></i>
						<strong><%= Resources.Resource.Partnership %></strong>
					</a>
				</h6>
			</div><!-- panel-heading -->
           <div class="panel-body">
               
               <p>Your GesMobile account is subsidise by your partnership plan, so you can enjoy all features without paying it yourself.</p>
               <p style="text-align:center;"><i class="fa fa-smile-o icon-3x display-inline-block tada infinite text-green animated"></i></p>
           </div>
        </div>
       
    </div>
    
    <div class="content" runat="server" id="panelContent" visible="True">
		<div class="panel panel-white">
        	<div class="panel-heading">
				<h6 class="panel-title">
					<a name="quotes">
						<i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
						<strong><%= Resources.Resource.ShoppingCart %></strong>
					</a>
				</h6>
			</div><!-- panel-heading -->

			<div class="panel-body" >
                <div id="wizard">
                    <h1><%= Resources.Resource.ChoosePlan %></h1>
                    
                    <div class="row pricing-table">
	                    <div class="col-sm-6 col-lg-4" id="planMonthlyOption" runat="server">
	                        <div class="panel text-center bordered">
	                            <div class="panel-body">
	                                <h4>Mensuel</h4>
                                    <h1 class="pricing-table-price">
                                        <span><%=baseCcy %></span><%=basePricePlanM %> <span style="font-size:40%"> / <%= Resources.Resource.Month %></span>
                                        <br /><span style="font-size:11px">&nbsp;</span>
                                    </h1>
                    
	                                <ul class="list-unstyled content-group">
                                        <li><strong><%= All_GesMobile_features %></strong></li>
                                        <li>
                                            <strong><%= Resources.Resource.Extra_user_license %>:<br /> <%=baseCcy %> <%=optionUserPricePlanM %> / <%= Resources.Resource.Month %></strong>
                                            <br /><span>&nbsp;</span>
                                        </li>
	                                </ul>
	                                <a href="#" runat="server" id="lnkMonthlyHelp" class="btn btn-lg btn-info text-uppercase text-size-small text-semibold legitRipple btn-change-period" visible="false"> <i class="icon-warning2"></i> </a>
	                                <a href="CartStep2.aspx?plan=2" runat="server" id="lnkMonthly" class="btn bg-success-400 btn-lg text-uppercase text-size-small text-semibold legitRipple">Buy</a>
	                            </div>
	                        </div>
	                    </div>



	                    <div class="col-sm-6 col-lg-4" id="planYearlyOption" runat="server">
                            
	                        <div class="panel text-center bordered<">
	                            <div class="panel-body">
	                                <h4>Annuel</h4>
	                                <h1 class="pricing-table-price">
                                        <span><%=baseCcy %></span><%=basePricePlanA %><span style="font-size:40%"> / <%= Resources.Resource.Month %></span>
                                        <br /><span style="font-size:11px"><%= Resources.Resource.In_single_payment_of %> <%=baseCcy %> <%= basePricePlanA_12 %></span>
	                                </h1>
	                                <ul class="list-unstyled content-group">
                                        <li><strong><%= All_GesMobile_features %></strong></li>
                                        <li>
                                            <strong><%= Resources.Resource.Extra_user_license %>:<br /> <%=baseCcy %> <%=optionUserPricePlanA %> / <%= Resources.Resource.Month %></strong>
                                            <br /><span style="font-size:11px"><%= Resources.Resource.In_single_payment_of %> <%=baseCcy %> <%= optionUserPricePlanA_12 %></span>
                                        </li>

	                                </ul>
                                    <div class="ribbon-container">
					                    <div class="ribbon bg-danger-400"><%=Resources.Resource.BestDeal %></div>
				                    </div>
	                                <a href="#" runat="server" id="lnkYearlyHelp" class="btn btn-lg btn-info text-uppercase text-size-small text-semibold legitRipple btn-change-period" visible="false"> <i class="icon-warning2"></i> </a>
	                                <a href="CartStep2.aspx?plan=1" runat="server" id="lnkYearly" class="btn bg-danger-400 btn-lg text-uppercase text-size-small text-semibold legitRipple">Buy</a>
                                    <asp:Label runat="server" ID="lblFreeMonth" style="padding-top:15px; display:block; font-weight:bold; text-transform: uppercase;" Visible="false" />
	                            </div>
	                        </div>
	                    </div>
                    </div>


                    <h1><%= Resources.Resource.Options %></h1>
                    <div class="table-responsive">
                    </div>
                    
                    <h1 id="stepPayTitle" runat="server"><%= Resources.Resource.Pay %></h1>
                    <div class="panel panel-white" id="stepPayContent" runat="server">
			        </div>

                </div>
            </div>


            
        </div>


        <div runat="server" id="panelCoupon">
		<div class="panel panel-white">
        	    <div class="panel-heading">
				    <h6 class="panel-title">
					    <a name="quotes">
						    <i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
						    <strong>Coupon de réduction</strong>
					    </a>
				    </h6>
			    </div><!-- panel-heading -->

			    <div class="panel-body">
                    <asp:Panel runat="server" ID="panelDescriptionNotYet">
                        <p>
                            Profitez de tarifs avantageux avec un coupon de réduction ?
                        </p>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="panelDescriptionAlready" Visible="false">
                        <p>Les tarifs de cette page sont actuellements affichés en tenant compte du coupon de réduction suivant :</p>
                    </asp:Panel>
                    
					<div>
						<div class="input-group">
							<span class="input-group-addon"><i class="icon-ticket"></i></span>
                            <asp:TextBox runat="server" ID="tbxCode" CssClass="form-control"></asp:TextBox>
							
						</div>
					</div>
                    <div style="margin-top:15px">

                        <asp:LinkButton runat="server" CssClass="btn btn-success" ID="btnActivate" OnClick="btnActivate_Click" Text="Click here" />
                    </div>
                </div>
        </div>
        </div>
        


    </div>
<script type="text/javascript" src="../assets/js/plugins/forms/wizards/steps.min.js"></script>
<script type="text/javascript">
    $(function () {

        $("#wizard").steps({
            enablePagination:false,
            startIndex: 0,
        });


        $('.btn-change-period').click(function () {
            window.swal({
                title:"<%= Resources.Resource.ChangeBillingPeriod %>",
                text: "<%= Resources.Resource.ChangeBillingPeriod_HowTo %>",
                showConfirmButton: true
            });
        });
    })
</script>
         </form>
</asp:Content>
