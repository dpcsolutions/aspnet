﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_Partnership : System.Web.UI.Page
{
    public PageHelper ph = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        ph = new PageHelper(this);
        if (ph.CurrentUserType != Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
        {
            Response.Redirect("~/");
            return;
        }

        if (String.IsNullOrWhiteSpace(System.Configuration.ConfigurationManager.AppSettings["ShopOnline"])
            || System.Configuration.ConfigurationManager.AppSettings["ShopOnline"].ToLower() != "true")
        {
            Response.Redirect("~/Account/Offline.aspx");
            return;
        }

        if (!IsPostBack)
        {
            Refresh();
        }



        
    }

    private void Refresh()
    {
        Calani.BusinessObjects.Partnership.PartnershipManager mgr = new Calani.BusinessObjects.Partnership.PartnershipManager(ph.CurrentOrganizationId);

        
        SetStatus(mgr.CanJoin, mgr.Code);
    }

    private void SetStatus(bool canregister, string code)
    {
        btnActivate.Enabled = canregister;
        tbxCode.Enabled = canregister;
        panelDescriptionNotYet.Visible = canregister;
        panelDescriptionAlready.Visible = !canregister;

        if (canregister)
        {
            btnActivate.CssClass = "btn btn-success";
            btnActivate.Text = Resources.Resource.Enable;
            tbxCode.Text = "";

        }
        else
        {
            btnActivate.CssClass = "btn btn-default disabled";
            btnActivate.Text = Resources.Resource.AlreadyEnabled;
            tbxCode.Text = code;

        }
    }

    protected void btnActivate_Click(object sender, EventArgs e)
    {
        Calani.BusinessObjects.Partnership.PartnershipManager mgr = new Calani.BusinessObjects.Partnership.PartnershipManager(ph.CurrentOrganizationId);
        if (mgr.Join(tbxCode.Text.Trim()))
        {
            SetStatus(false, tbxCode.Text.Trim());
        }
        Refresh();
    }
}