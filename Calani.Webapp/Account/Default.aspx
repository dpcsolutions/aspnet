﻿<%@ Page Title="Compte" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Account_Overview" Codebehind="Default.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <form runat="server">
        <div class="content">
             <div class="row">


                 <div class="col-sm-12 col-md-12" runat="server" id="alert" visible="false">
                     <div class="col-sm-12 col-md-12">
                         <div class="alert alert-danger alert-styled-right alert-bordered">
							<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only"><%= Resources.Resource.Close %></span></button>
                                <asp:Label runat="server" ID="lblAlertMessage"  />
                                <asp:LinkButton href="CartStep1.aspx"  runat="server" Visible="False"><%=Resources.Resource.Buy_or_renew %></asp:LinkButton>
					        </div>
                        </div>
                 </div>

                 <div class="col-sm-12 col-md-12">
	                 <div class="col-sm-6 col-md-4">
                         <div class="content-group">
                             <div class="panel panel-white">
							<div class="panel-heading">
                                
                                <h6 class="panel-title">
					                <a name="quotes">
						                <i class="icon-profile"></i>
						                <strong><%= Resources.Resource.Subscription %></strong>
					                </a>
				                </h6>
                                <div class="heading-elements">
							        <asp:LinkButton runat="server" CssClass="label bg-green-400 heading-text" id="accederAuDetailId" href="AccountDetail.aspx">
                                        <%=Resources.Resource.GotoDetails %>
							        </asp:LinkButton> 
                        			<asp:LinkButton runat="server" CssClass="label bg-indigo-400 heading-text" ID="editSubscriptionId">
                                        <%=Resources.Resource.Update %>
                        			</asp:LinkButton>
                                    <asp:HyperLink runat="server" CssClass="label bg-indigo-400 heading-text" ID="btnBuy" Visible="false" NavigateUrl="~/Account/CartStep1.aspx">
                                        <%=Resources.Resource.Buy_or_renew %>
                        			</asp:HyperLink>
				                </div>
							</div>
							<div class="panel-body">
								<div class="form-group mt-5">
									<label class="text-semibold"><%=Resources.Resource.SubscriptionId %></label>
									<span class="pull-right-sm"><asp:Label runat="server" id="lblSubscriptionId"/></span>
								</div>
                                <div class="form-group mt-5">
									<label class="text-semibold"><%=Resources.Resource.SubscriptionType %></label>
									<span class="pull-right-sm"><asp:Label runat="server" id="lblSubscriptionType"/></span>
								</div>
                                <div class="form-group mt-5">
									<label class="text-semibold"><%=Resources.Resource.FirstInvoice %></label>
									<span class="pull-right-sm"><asp:Label runat="server" id="lblFirstBilledDate"/></span>
								</div>
                                <div class="form-group mt-5">
									<label class="text-semibold"><%=Resources.Resource.NextRenewal %></label>
									<span class="pull-right-sm"><asp:Label runat="server" id="lblSubscriptionNextBillingDate"/></span>
								</div>
                                <div class="form-group mt-5">
									<label class="text-semibold"><%=Resources.Resource.Amount %></label>
									<span class="pull-right-sm"><asp:Label runat="server" id="lblSubscriptionAmount"/></span>
								</div>
                                <div class="form-group mt-5">
								    <label class="text-semibold"><%=Resources.Resource.UsedUserLicenses %></label>
                                    <asp:Label CssClass="pull-right-sm" runat="server" id="lblLicencesUsed"/>
                                    <div class="progress progress-xxs">
                                        <asp:Label CssClass="progress-bar progress-bar-info" runat="server" id="lblLicencesUsedProgress"/>
									</div>
								</div>
                                <div class="form-group no-margin-bottom">
									<label class="text-semibold"><%=Resources.Resource.Status %></label>
									<span class="pull-right-sm"><asp:Label runat="server" id="lblSubStatus"/></span>
								</div>
                    		</div>
						
                             </div>
                        </div>
		             </div>
                     <div class="col-sm-6 col-md-4">
                         <div class="content-group">
                             <div class="panel panel-white">
							    <div class="panel-heading">
                                    <h6 class="panel-title">
					                    <a name="quotes">
						                    <i class="icon-credit-card"></i>
						                    <strong><%= Resources.Resource.PaymentMethod %></strong>
					                    </a>
                                    </h6>
                                    <div class="heading-elements">
									    <asp:LinkButton runat="server" ID="editCardId" CssClass="label bg-indigo-400 heading-text"><%=Resources.Resource.Update %></asp:LinkButton>
				                    </div>
							    </div>
							    <div class="panel-body">
								    <div class="form-group mt-5">
									    <label class="text-semibold"><%=Resources.Resource.Type %></label>
									    <span class="pull-right-sm"><asp:Label runat="server" id="lblCCType"/></span>
								    </div>
                                    <div class="form-group mt-5">
									    <label class="text-semibold"><%=Resources.Resource.CardNumber %></label>
									    <span class="pull-right-sm"><asp:Label runat="server" id="lblCCNum"/></span>
								    </div>
                                    <div class="form-group mt-5">
									    <label class="text-semibold"><%=Resources.Resource.ExpirationDate %></label>
									    <span class="pull-right-sm"><asp:Label runat="server" id="lblCCDateExp"/></span>
								    </div>
                                    <div class="form-group no-margin-bottom">
									    <label class="text-semibold"><%=Resources.Resource.Status %></label>
									    <span class="pull-right-sm"><asp:Label runat="server" id="lblCCStatus"/></span>
								    </div>
							    </div>
                            </div>
						</div>
		             </div>
                 </div>
            </div>
        </div>
     </form>
</asp:Content>
