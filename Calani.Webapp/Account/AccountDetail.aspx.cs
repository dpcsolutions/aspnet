﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.PaymentService;

public partial class AccountDetail : System.Web.UI.Page
{

    public PageHelper ph;


    List<subscription> subs = new List<subscription>();

    protected void Page_Load(object sender, EventArgs e)
    {
        ph = new PageHelper(this);
        if (ph.CurrentUserType != Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
        {
            Response.Redirect("~/");
            return;
        }

        

        if (true)
        {

            LoadData();
        }

    }

    private void LoadData()
    {
        PaymentProcessor pp = new PaymentProcessor();

        var client = pp.getCustomer(PaymentProcessor.getClientId(ph.CurrentOrganizationId));
        if (client != null)
        {
            panelBraintreeDetails.Visible = true;
            panelBraintreeNoData.Visible = false;
            panelBraintreeNoDataCancel.Visible = false;
            foreach (var cc in client.CreditCards)
            {
                var isoCode = "";

                foreach (var s in cc.Subscriptions)
                {
                    decimal addinsTotal = 0;

                    List<transaction> trans = new List<transaction>();

                    foreach (var t in s.Transactions)
                    {
                        trans.Add(new transaction((t.CreditCard != null ? t.CreditCard.LastFour.ToString() : ""), (t.Amount == null ? 0 : (decimal)t.Amount), (DateTime)t.CreatedAt, t.CurrencyIsoCode, t.PlanId, t.Status.ToString()));
                        if (String.IsNullOrEmpty(isoCode))
                            isoCode = t.CurrencyIsoCode;
                        else if (t.CurrencyIsoCode != isoCode)
                            isoCode = "?";
                    }

                    List<addin> addins = new List<addin>();
                    foreach (var a in s.AddOns)
                    {
                        addinsTotal += (a.Amount != null ? (decimal)a.Amount : 0) * (a.Quantity != null ? (decimal)a.Quantity : 0);

                        addins.Add(new addin()
                        {
                            CurrencyIsoCode = isoCode,
                            Amount = (a.Amount != null ? (decimal)a.Amount : 0),
                            Name = a.Name,
                            Quantity = (a.Quantity != null ? (int)a.Quantity : 0),
                            Total = (a.Quantity != null ? (int)a.Quantity : 0) * (a.Amount != null ? (decimal)a.Amount : 0),
                        });
                    }

                    
                    
                   

                    subs.Add(new subscription(isoCode, s.Id, cc.LastFour, cc.CardType.ToString(), cc.IsExpired != null ? (bool)cc.IsExpired : false, s.PlanId,
                        s.Price != null ? (decimal)s.Price : 0, s.CreatedAt != null ? (DateTime)s.CreatedAt : new DateTime(1900, 1, 1),
                        s.UpdatedAt != null ? (DateTime)s.UpdatedAt : new DateTime(1900, 1, 1), s.Status != null ? s.Status.ToString() : "?",
                        s.DaysPastDue != null ? (int)s.DaysPastDue : 0,
                        s.FailureCount != null ? (int)s.FailureCount : 0,
                        addins, trans, (DateTime)s.NextBillingDate, s.NextBillingPeriodAmount, s.Balance));
                }
            }
        }

        else
        {
            panelBraintreeDetails.Visible = false;
            panelBraintreeNoData.Visible = true;

            Calani.BusinessObjects.Model.CalaniEntities db = new Calani.BusinessObjects.Model.CalaniEntities();
            string subid = (from r in db.organizations where r.id == ph.CurrentOrganizationId select r.subscriptionId).FirstOrDefault();
            if (String.IsNullOrWhiteSpace(subid)) subid = null;
            panelBraintreeNoDataCancel.Visible = subid != null;

            return;
        }
        //grid:
        gridsubs.DataSource = subs;
        foreach (var s in subs)
        {
            if (s.PlanId == null) s.PlanId = "";
        }
        gridsubs.DataBind();
        gridsubs.RowCreated += Grid_RowCreated;
        gridsubs.SelectedIndex = 0;

        if (subs.Count > 0)
        {
            if (gridsubs.SelectedIndex < subs.Count)
            {
                gridAddin.DataSource = subs[gridsubs.SelectedIndex].Addins;
                gridAddin.DataBind();

                List<transaction> transactions = subs[gridsubs.SelectedIndex].Transactions;
                if (transactions != null)
                {
                    foreach (var t in transactions)
                    {
                        if (t.PlanId == null) t.PlanId = "";
                    }
                    GridTransactions.DataSource = transactions;
                }
                else
                {
                    GridTransactions.DataSource = null;
                }
                
                GridTransactions.DataBind();
            }
            else
            {
                gridAddin.DataSource = null;
                GridTransactions.DataSource = null;
            }

        }

        new Html5GridView(gridsubs, false);
        new Html5GridView(gridAddin, false, null);
        new Html5GridView(GridTransactions, false, null);
    }

    private void Grid_RowCreated(object sender, GridViewRowEventArgs e)
    {
        //if (e.Row.RowType == DataControlRowType.DataRow)
            //e.Row.Attributes["onclick"] = this.Page.ClientScript.GetPostBackEventReference(this.gridsubs, "Select$" + e.Row.RowIndex);
    }
    
    public void Grid_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (gridsubs.SelectedIndex < subs.Count)
        {
            gridAddin.DataSource = subs[gridsubs.SelectedIndex].Addins;
            gridAddin.DataBind();

            GridTransactions.DataSource = subs[gridsubs.SelectedIndex].Transactions;
            GridTransactions.DataBind();
        }
        else
        {
            gridAddin.DataSource = null;
            GridTransactions.DataSource = null;
        }

    }

    protected void CancelButton_Click(object sender, EventArgs e)
    {
        Button btn = (Button)sender;
        //Get the row that contains this button
        GridViewRow gvr = (GridViewRow)btn.NamingContainer;
        var id = (gvr.FindControl("lblSubscriptionId") as Label).Text;
        PaymentProcessor pp = new PaymentProcessor();
        if(pp.cancelSubscription(id))
        {
            var db = new Calani.BusinessObjects.Model.CalaniEntities();
            var org = (from r in db.organizations where r.id == ph.CurrentOrganizationId select r).FirstOrDefault();
            if(org != null)
            {
                org.trialEnd = DateTime.Now.AddSeconds(-1);
                org.subscriptionEnd = null;
                org.subscriptionId = null;
                db.SaveChanges();
            }
        }
        gridsubs.DataSource = null;
        gridAddin.DataSource = null;
        GridTransactions.DataSource = null;
        Response.Redirect("AccountDetail.aspx");
    }

    public string Translate(string n)
    {
        n = n.ToUpper();
        if (n == "AUTHORIZED") return Resources.Resource.Authorized;
        if (n == "SETTLED") return Resources.Resource.Settled;
        if (n == "SUBMITTED_FOR_SETTLEMENT") return Resources.Resource.SubmittedForSettlement;
        if (n == "SETTLING") return Resources.Resource.Settling;
        return n;
    }


    public string FormatMoney(object o)
    {
        double d = Convert.ToDouble(o);
        return String.Format("{0:0,0.00}", d);
    }

    protected void btnCancelBrainTree_Click(object sender, EventArgs e)
    {
        Calani.BusinessObjects.Model.CalaniEntities db = new Calani.BusinessObjects.Model.CalaniEntities();
        var org = (from r in db.organizations where r.id == ph.CurrentOrganizationId select r).FirstOrDefault();
        string prevsubid = org.subscriptionId;
        

        Calani.BusinessObjects.PaymentService.PaymentService mgr = new PaymentService();
        mgr.cancelSubscription(null);


        var mailSender = new Calani.BusinessObjects.Generic.MailSender();
        if (!String.IsNullOrWhiteSpace(System.Configuration.ConfigurationManager.AppSettings["Bcc"]))
        {


            mailSender.MailTo.Email = System.Configuration.ConfigurationManager.AppSettings["Bcc"];
            mailSender.MailTo.UserId = ph.CurrentUserId;


            mailSender.Variables.Add("orgid", org.id.ToString());
            mailSender.Variables.Add("orgname", org.name);
            mailSender.Variables.Add("userid", ph.CurrentUserId.ToString());
            mailSender.Variables.Add("username", ph.CurrentUserName);
            mailSender.Variables.Add("subid", prevsubid);
            

            mailSender.SendSmartHtmlEmail("dissociatebraintree.html", "activation");

        }
        LoadData();
    }
}