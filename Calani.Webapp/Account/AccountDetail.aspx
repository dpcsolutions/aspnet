﻿<%@ Page Title="Taxes" Language="C#" EnableEventValidation="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Codebehind="AccountDetail.aspx.cs" Inherits="AccountDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
        <div class="content">
        <asp:Panel runat="server" ID="panelBraintreeNoData" Visible="false">
            <div class="panel panel-white">
                    <p>Aucune donnée de facturation disponible.</p>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="panelBraintreeNoDataCancel" Visible="false">
            <div class="panel panel-white">
                    <p>
                        <asp:LinkButton ID="btnCancelBrainTree" runat="server" OnClick="btnCancelBrainTree_Click" OnClientClick="confirm ('Are you sure to lost your subscription?');">
                            <%=Resources.Resource.DissociateBrainTree %>
                        </asp:LinkButton>
                    </p>
                <p>
                    <%=Resources.Resource.ContactSupport %> <a href="mailto:info@gesmobile.ch">info@gesmobile.ch</a>
                </p>
            </div>
        </asp:Panel>

        <asp:Panel runat="server" ID="panelBraintreeDetails">
        <script type="text/javascript">
            function areyousure()
            {
                return confirm("<%= Resources.Resource.Are_You_Sure_To_CancelSubscription %>");
            }
        </script>
        
            <div class="panel panel-white">
                            
			    <div class="panel-heading">
				    <h6 class="panel-title">
					    <a name="quotes">
						    <i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
						    <strong><%= Resources.Resource.SubscriptionAccountDetail %></strong>
					    </a>
				    </h6>
			    </div><!-- panel-heading -->

			    <div class="panel-body">
                    <asp:GridView runat="server" ID="gridsubs" AutoGenerateColumns="False" OnSelectedIndexChanged="Grid_SelectedIndexChanged">
                        <Columns>
                            <asp:TemplateField HeaderText="Subscription" ItemStyle-CssClass="nowrap">
                                <ItemTemplate>


                                        <div>
                                            <asp:LinkButton ID="lblSelect" runat="server" CausesValidation="False" CommandName="Select">
                                            <asp:Label ID="lblPlan" runat="server" Enabled="false" Font-Bold="true" Text='<%#Eval("PlanId").ToString().ToLower().Contains("year") ? Resources.Resource.Yearly : Resources.Resource.Monthly  %>'/>
                                            - <asp:Label ID="lblSubscriptionId" runat="server" Enabled="false" Text='<%#Eval("Id")  %>'/>
                                        </asp:LinkButton>
                                        </div>
                                        <div>
                                            <asp:Label ID="lblUpdate"  CssClass="small" runat="server" Enabled="false" Text='<%#Eval("UpdatedAt")  %>'/>
                                        </div>
                                        <div>
                                            <span class='<%#Eval("Status") != null && Eval("Status").ToString() == "Canceled" ? "label-danger" : "label-success" %> label'>
                                                <asp:Label ID="lblStatus" runat="server" Enabled="false" Text='<%#Eval("Status").ToString()%>'/>
                                            </span>    
                                        </div>

                                   
                                    
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CreditCard" ItemStyle-CssClass="nowrap">
                                <ItemTemplate>
                                        <div>
                                            <i class="fa fa-credit-card"></i>
                                            <asp:Label runat="server" ID="lblCardType" Text='<%# Eval("CardType") %>' />
                                            °°° °°° °°° <asp:Label runat="server" ID="lblCardLastFour" Text='<%# Eval("LastFour") %>' />
                                        </div>

                                        <div>
                                            <asp:Label ID="cbxCcValid" runat="server" Enabled="false" Visible='<%#(Eval("IsExpired").ToString() == "true") ? false : true  %>' Text="Valid" CssClass="label label-success" />
                                            <asp:Label ID="cbxCcExpired" runat="server" Enabled="false" Visible='<%#(Eval("IsExpired").ToString() == "true") ? true : false  %>' Text="Expired" CssClass="label label-danger" />
                                        </div>
                                </ItemTemplate>
                            </asp:TemplateField>


                            
                            <asp:TemplateField HeaderText="Subscription" ItemStyle-CssClass="nowrap text-right" HeaderStyle-CssClass="text-right">
                                <ItemTemplate>
                                    
                                    <div class="small">
                                        <em>
                                            <%= Resources.Resource.Subscription %>:
                                            <asp:Label ID="Label1" runat="server" Enabled="false" Text='<%# FormatMoney(Eval("Price")) + " " + Eval("CurrencyIsoCode").ToString() %>'/>
                                        </em>
                                    </div>
                                    <div class="small">
                                        <em>
                                            <%= Resources.Resource.UserLicenses %>:
                                            <asp:Label ID="Label2" runat="server" Enabled="false" Text='<%# FormatMoney(Eval("AddinsTotal")) + " " + Eval("CurrencyIsoCode").ToString() %>'/>
                                        </em>
                                    </div>
                                    <div>
                                        <strong>
                                            <%= Resources.Resource.Amount %>:
                                            <asp:Label ID="lblAmount" runat="server" Enabled="false" Text='<%# FormatMoney(decimal.Parse(Eval("Price").ToString()) + decimal.Parse(Eval("AddinsTotal").ToString())) + " " + Eval("CurrencyIsoCode").ToString() %>'/>
                                        </strong>
                                    </div>

                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:Button ID="RetryButton" Visible='<%#(Eval("FailureCount") != null && Eval("FailureCount").ToString() != "0") %>' CssClass="btn btn-primary" Text="Retry" runat="server" />
                                    <asp:Button ID="CancelButton" OnClientClick="return areyousure();" OnClick="CancelButton_Click" Visible='<%# Eval("Status") != null && Eval("Status").ToString() == "Active" %>' CssClass="btn btn-danger" Text='<%$ Resources:Resource,Cancel %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>  


                            <asp:TemplateField HeaderText="Subscription" ItemStyle-CssClass="nowrap" HeaderStyle-CssClass="">
                                <ItemTemplate>
                                    
                                    <div class="small">
                                        <em>
                                            <%= Resources.Resource.Balance %>:
                                            <asp:Label ID="lblBalance" runat="server" Enabled="false" Text='<%# FormatMoney(Eval("Balance")) + " " + Eval("CurrencyIsoCode").ToString() %>'/>
                                        </em>
                                    </div>
                                    <div class="small">
                                        <em>
                                            <%= Resources.Resource.NextBilling %>:
                                            <asp:Label ID="Label3" runat="server" Enabled="false" Text='<%# FormatMoney(Eval("NextBillingAmount")) + " " + Eval("CurrencyIsoCode").ToString() %>'/>
                                        </em>
                                    </div>
                                    <div class="small">
                                        <em>
                                            <%= Resources.Resource.Date %>:
                                            <asp:Label ID="Label4" runat="server" Enabled="false" Text='<%# Eval("NextBilled").ToString()  %>'/>
                                        </em>
                                    </div>
                                    <div class="small">
                                        <em>
                                            <%= Resources.Resource.DaysPastDue %>:
                                            <asp:Label ID="Label5" runat="server" Enabled="false" Text='<%# Eval("DaysPastDue").ToString()  %>'/>
                                        </em>
                                    </div>
                                    <div class="small">
                                        <em>
                                            <%= Resources.Resource.FailureCount %>:
                                            <asp:Label ID="Label6" runat="server" Enabled="false" Text='<%# Eval("FailureCount").ToString()  %>'/>
                                        </em>
                                    </div>
                                    

                                </ItemTemplate>
                            </asp:TemplateField>
                            
                        </Columns>
                        
                    </asp:GridView>
                </div>
            </div>
            <div class="panel panel-white">
                            
			<div class="panel-heading">
				<h6 class="panel-title">
					<a name="quotes">
						<i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
						<strong><%= "Add-ins" %></strong>
					</a>
				</h6>
			</div><!-- panel-heading -->
            <div class="panel-body">
                    <asp:GridView runat="server" ID="gridAddin" AutoGenerateColumns="false">
                    <Columns>
                    <asp:BoundField DataField="Name" HeaderText="Name" ItemStyle-CssClass="nowrap"   />
                    <asp:TemplateField HeaderText="Amount" ItemStyle-CssClass="nowrap text-right" HeaderStyle-CssClass="text-right">
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" runat="server" Enabled="false" Text='<%#Eval("Amount").ToString() + " " + Eval("CurrencyIsoCode").ToString() %>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Quantity" HeaderText="Qty" ItemStyle-CssClass="nowrap text-right" HeaderStyle-CssClass="text-right" />
                    <asp:TemplateField HeaderText="Total" ItemStyle-CssClass="nowrap text-right" HeaderStyle-CssClass="text-right">
                        <ItemTemplate>
                            <asp:Label ID="lblAmount" runat="server" Enabled="false" Text='<%#Eval("Total").ToString() + " " + Eval("CurrencyIsoCode").ToString() %>'/>
                        </ItemTemplate>
                    </asp:TemplateField></Columns>
                </asp:GridView>
            </div>
            </div>
            <div class="panel panel-white">
                            
			<div class="panel-heading">
				<h6 class="panel-title">
					<a name="quotes">
						<i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
						<strong><%= "Transactions" %></strong>
					</a>
				</h6>
			</div>
            <div class="panel-body">
                    <asp:GridView runat="server" ID="GridTransactions" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField HeaderText="CreditCard" ItemStyle-CssClass="nowrap" HeaderStyle-CssClass="nowrap" >
                            <ItemTemplate>
                                °°° °°° °°° <asp:Label runat="server" ID="lblCardLastFour" Text='<%# Eval("CreditCard") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount" ItemStyle-CssClass="nowrap text-right" HeaderStyle-CssClass="text-right" >
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" runat="server" Enabled="false" Text='<%#Eval("Amount").ToString() + " " + Eval("CurrencyIsoCode").ToString() %>'/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="CreatedAt" HeaderText="Date" ItemStyle-CssClass="nowrap"  HeaderStyle-CssClass="nowrap" />
                        <asp:TemplateField HeaderText="Subscription" ItemStyle-CssClass="nowrap"  HeaderStyle-CssClass="nowrap">
                            <ItemTemplate>
                                <asp:Label ID="lblPlan" runat="server" Enabled="false" Text='<%#Eval("PlanId").ToString().Contains("Year") ? "Yearly" : "Monthly"  %>'/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" ItemStyle-CssClass="nowrap"  HeaderStyle-CssClass="nowrap">
                            <ItemTemplate>
                                    <span class='<%#Eval("Status") != null && (Eval("Status").ToString() == "Authorized" || Eval("Status").ToString() == "Authorized" || Eval("Status").ToString() == "Settled" ) ? "label-success" : (Eval("Status") != null && (Eval("Status").ToString() == "Submitted for settlement" || Eval("Status").ToString() == "Settling") ? "label-warning" : "label-success") %> label'>
                                    <asp:Label ID="lblStatus" runat="server" Enabled="false" Text='<%# Translate(Eval("Status").ToString()) %>'/>
                                    </span>    
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            
    </div>
        </div><!-- /content -->
            </asp:Panel>
        </div>
    </form>
    
  
</asp:Content>

