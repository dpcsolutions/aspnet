﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.PaymentService;

public partial class Account_Overview : System.Web.UI.Page
{
    public PageHelper ph = null;
    public string subsId = "";

    subscription sub = null;

    protected override void InitializeCulture()
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ph = new PageHelper(this);

        if (ph.CurrentUserType != Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
        {
            Response.Redirect("~/");
            return;
        }

        
        Calani.BusinessObjects.Contacts.EmployeesManager mgr = new Calani.BusinessObjects.Contacts.EmployeesManager(ph.CurrentOrganizationId);
        Calani.BusinessObjects.Model.contacts currentUser = mgr.Get((long)ph.CurrentUserId);

        var org = currentUser.organizations;

        if (org != null && org.subscriptionId != null)
        {
            PaymentProcessor pp = new PaymentProcessor();
            subsId = org.subscriptionId; //from db'b97np2

            var s = pp.getSubscription(subsId);
            string planid = "2";
            if(s != null && s.PlanId !=null && s.PlanId.ToLower().Contains("yearly")) planid = "1";

            editSubscriptionId.Attributes.Add("href", "CartStep2.aspx?plan=1");
            editCardId.Attributes.Add("href", "EditCC.aspx?subsid=" + subsId);

            

            int licencesAvailable = s != null && s.AddOns != null ? (int)s.AddOns[0].Quantity : 0;



            // from db
            int freeIncludedLicenses = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NewAccountUsersLimit"]);
            int licencesUsed = mgr.CountEmployees(true) - freeIncludedLicenses;
            if (licencesUsed < 0) licencesUsed = 0;
            // -->


            if (s != null)
            {
                switch (s.Status.ToString())
                {
                    case "Pending":
                        lblSubStatus.Text = Resources.Resource.Pending;
                        break;
                    case "Active":
                        lblSubStatus.Text = Resources.Resource.Active;
                        break;
                    case "Past Due":
                        lblSubStatus.Text = Resources.Resource.Failure;
                        break;
                    case "Expired":
                        lblSubStatus.Text = Resources.Resource.Expired;
                        break;
                    case "Canceled":
                        lblSubStatus.Text = Resources.Resource.Canceled;
                        break;
                }

                var isoCode = (s.Transactions != null && s.Transactions.Count > 0 ? s.Transactions[0].CurrencyIsoCode : "?");



                lblSubscriptionId.Text = s.Id;
                lblSubscriptionType.Text = (s.PlanId.ToLower().Contains("monthly") ? Resources.Resource.Monthly : Resources.Resource.Yearly);
                lblSubscriptionNextBillingDate.Text = s.NextBillingDate != null ? ((DateTime)s.NextBillingDate).ToShortDateString() : "?";
                lblSubscriptionAmount.Text = s.NextBillAmount != null ? s.NextBillAmount.ToString() + " " + isoCode : "?";
                lblFirstBilledDate.Text = s.BillingPeriodStartDate != null ? ((DateTime)s.BillingPeriodStartDate).ToShortDateString() : "?";
                lblLicencesUsed.Text = licencesUsed.ToString() + "/" + licencesAvailable.ToString();
                lblLicencesUsedProgress.ApplyStyle(new Style() { Width = new Unit((licencesUsed * 100 / licencesAvailable), UnitType.Percentage) });

                //get payment method token
                var paymentMethod = pp.getPaymentMethodForSubscription(s.Id);
                lblCCType.Text = paymentMethod is Braintree.CreditCard ? ((Braintree.CreditCard)paymentMethod).CardType.ToString() : "?";
                lblCCNum.Text = paymentMethod is Braintree.CreditCard ? ((Braintree.CreditCard)paymentMethod).MaskedNumber.ToString() : "?";
                lblCCDateExp.Text = paymentMethod is Braintree.CreditCard ? ((Braintree.CreditCard)paymentMethod).ExpirationDate.ToString() : "?";
                lblCCStatus.Text = paymentMethod is Braintree.CreditCard ? ((bool)((Braintree.CreditCard)paymentMethod).IsExpired ? Resources.Resource.Expired : Resources.Resource.Active) : "?";
            }


        }
        else if(org != null && org.partnership != null && org.subscriptionStatus == "Partnership_Active")
        {
            editCardId.Visible = false;
            accederAuDetailId.Visible = false;
            editSubscriptionId.Visible = false;
            lblSubscriptionType.Text = Resources.Resource.Partnership;
            lblSubscriptionId.Text = "Partn, " + org.partnership;


            btnBuy.Visible = false;
            alert.Visible = false;
            
        }
        else
        {
            editCardId.Visible = false;
            accederAuDetailId.Visible = false;
            editSubscriptionId.Visible = false;
            lblSubscriptionType.Text = Resources.Resource.FreeTrial;
            lblSubscriptionId.Text = Resources.Resource.NoSubscriptionYet;

            if (org != null && org.trialEnd != null)
            {
                lblSubscriptionNextBillingDate.Text = org.trialEnd.Value.ToString("dd.MM.yyyy");
                double daysLeft = Math.Floor(org.trialEnd.Value.Subtract(DateTime.Now).TotalDays);
                lblSubStatus.Text = daysLeft + " " + Resources.Resource.DaysLeft;
                if(daysLeft < 0)
                {

                }
            }

            if (ph.IsTrialEnded())
            {
                btnBuy.Visible = true;
                alert.Visible = true;
                lblAlertMessage.Text = Resources.Resource.TrialEnded_Error;
            }
            if (ph.IsTrialEnded())
            {
                btnBuy.Visible = true;
                alert.Visible = true;
                lblAlertMessage.Text = Resources.Resource.SubscriptionEnded_Error;
            }
        }
    }
}