﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Account_Offline : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!String.IsNullOrWhiteSpace(System.Configuration.ConfigurationManager.AppSettings["ShopOnline"])
            && System.Configuration.ConfigurationManager.AppSettings["ShopOnline"].ToLower() == "true")
        {
            Response.Redirect("~/Account/CartStep1.aspx");
            return;
        }
    }
}