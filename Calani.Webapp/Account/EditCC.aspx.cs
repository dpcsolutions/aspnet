﻿using System;
using System.Linq;
public partial class PayPal_EditCC : System.Web.UI.Page
{
    public PageHelper ph = null;
    protected Account_EditCC cc = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        ph = new PageHelper(this);

        if (ph.CurrentUserType != Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
        {
            Response.Redirect("~/");
            return;
        }

        if (String.IsNullOrWhiteSpace(System.Configuration.ConfigurationManager.AppSettings["ShopOnline"])
            || System.Configuration.ConfigurationManager.AppSettings["ShopOnline"].ToLower() != "true")
        {
            Response.Redirect("~/Account/Offline.aspx");
            return;
        }

        var db = new Calani.BusinessObjects.Model.CalaniEntities();
        string sub = (from r in db.organizations where r.id == ph.CurrentOrganizationId select r.subscriptionId).FirstOrDefault();

        if (!String.IsNullOrWhiteSpace(sub))
        {
            cc = new Account_EditCC();
            cc.ph = ph;
            cc.NoSubscriptionMode = true;
            editCCId.Controls.Add(cc);
        }
    }
}