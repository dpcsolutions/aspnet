﻿<%@ Page Title="TimeSheet" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Time_Reports_List" Codebehind="List.aspx.cs" %>

<%@ Register Src="~/assets/Controls/TimesheetStatus.ascx" TagPrefix="uc1" TagName="TimesheetStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
    <div class="btn-group" runat="server" id="quickmenu">
		<button type="button" class="btn bg-gesmobile btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
		</button>

		<ul class="dropdown-menu dropdown-menu-right">
            <li><a type="button" onclick="generateReportClick('Employee')" class="btn" runat="server"> <i class="icon-plus2"></i> <%=Resources.Resource.Employee + " " + Resources.Resource.Report.ToLower()%> </a></li>
            <li><a type="button" onclick="generateReportClick('Reports')" class="btn" runat="server"> <i class="icon-plus2"></i> <%=Resources.Resource.Project + " " + Resources.Resource.Report.ToLower()%> </a></li>
            <li><a type="button" onclick="generateReportClick('ServicesReport')" class="btn" runat="server"> <i class="icon-plus2"></i> <%=Resources.Resource.Services + " " + Resources.Resource.Report.ToLower()%> </a></li>
		</ul>
	</div>
    
    <script>
        function generateReportClick(name){
            $(`#ContentPlaceHolder1_btn${name}`).click()
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        var txtDate = "<%= Resources.Resource.Date %>";
        
    </script>
    <form runat="server">

        <div class="content">


            <div class="row">
                
                <div class="col-sm-12 col-md-6">
                    <div class="panel panel-white" runat="server" id="EmployeesReport">
                        <div class="panel-heading text-center">
                            <p><i class="icon-users4 icon-2x display-inline-block tada infinite"></i></p>
                            <strong><%= Resources.Resource.Employees %></strong>
                        </div>
                                      
                        <div class="panel-body">
                                <div>
                                    <i class="fa fa-calendar-check-o" aria-hidden="true"></i> <%= Resources.Resource.Select_Period %>
                                </div>
                                              
                                <div>
                                    <asp:HiddenField ID="hEmployeesFrom" runat="server" />
                                    <asp:HiddenField ID="hEmployeesTo" runat="server" />
                                    <select id="ddlEmployeesPeriod" class="select" onchange="setEmployeesPeriodFilter()">
                                        <%= OptionsPeriod %>
                                    </select>
                                </div>
                                              
                                <div style="margin-top:20px">
                                    <i class="icon-users4" aria-hidden="true"></i> <%= Resources.Resource.Employees %>
                                </div>
                                <div>
                                    <asp:DropDownList ID="ddlEmployees" DataValueField="id" DataTextField="name" class="select" runat="server" AppendDataBoundItems="true">
                                          <asp:ListItem Text= '<%$ Resources:Resource, Select___ %>'></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            <br />
                            <strong class="text-size-small pull-right">
                                <asp:Button runat="server" ID="btnEmployee" Text= '<%$ Resources:Resource, Report %>' CommandArgument="1" OnClick="generateReport_Click"/>
                            </strong>
                        </div>
                    </div>
                </div>

                
                <div class="col-sm-12 col-md-6">
                    <div class="panel panel-white" runat="server" id="Div1">
                        <div class="panel-heading text-center">
                            <p><i class="icon-folder icon-2x display-inline-block tada infinite"></i></p>
                            <strong><%= Resources.Resource.Projects %></strong>
                        </div>
                                      
                        <div class="panel-body">
                                <div>
                                    <i class="fa fa-calendar-check-o" aria-hidden="true"></i> <%= Resources.Resource.Select_Period %>
                                </div>
                                              
                                <div>
                                    <asp:HiddenField ID="hProjectsFrom" runat="server" />
                                    <asp:HiddenField ID="hProjectsTo" runat="server" />
                                    <select id="ddlProjectsPeriodFilter" class="select" onchange="setProjectsPeriodFilter()">
                                        <%= OptionsPeriod %>
                                    </select>
                                </div>
                                              
                                <div style="margin-top:20px">
                                    <i class="icon-folder" aria-hidden="true"></i> <%= Resources.Resource.Project %>
                                </div>
                                <div>
                                    <asp:DropDownList ID="ddlProjects" DataValueField="id" DataTextField="name" class="select" runat="server" AppendDataBoundItems="true">
                                          <asp:ListItem Text= '<%$ Resources:Resource, Select___ %>'></asp:ListItem>
                                    </asp:DropDownList>

                                    
                                </div>
                            <br />
                            <strong class="text-size-small pull-right">
                                <asp:Button runat="server" ID="btnReports" Text= '<%$ Resources:Resource, Report %>' CommandArgument="2" OnClick="generateReport_Click"/>
                            </strong>
                        </div>
                    </div>
                </div>

                <%--Services Report--%>
                <div class="col-sm-12 col-md-6">
                    <div class="panel panel-white" runat="server" id="Div2">
                        <div class="panel-heading text-center">
                            <p><i class="icon-folder icon-2x display-inline-block tada infinite"></i></p>
                            <strong><%= Resources.Resource.Services %></strong>
                        </div>
                                      
                        <div class="panel-body">
                                <div>
                                    <i class="fa fa-calendar-check-o" aria-hidden="true"></i> <%= Resources.Resource.Select_Period %>
                                </div>
                                              
                                <div>
                                    <asp:HiddenField ID="hServicesFrom" runat="server" />
                                    <asp:HiddenField ID="hServicesTo" runat="server" />
                                    <select id="ddlServicesPeriodFilter" class="select" onchange="setServicesPeriodFilter()">
                                        <%= OptionsPeriod %>
                                    </select>
                                </div>
                                              
                                <div style="margin-top:20px">
                                    <i class="icon-folder" aria-hidden="true"></i> <%= Resources.Resource.Service %>
                                </div>
                                <div>
                                    <asp:DropDownList ID="ddlServices" DataValueField="id" DataTextField="name" class="select" runat="server" AppendDataBoundItems="true">
                                          <asp:ListItem Text= '<%$ Resources:Resource, Select___ %>'></asp:ListItem>
                                    </asp:DropDownList>

                                    
                                </div>
                            <br />
                            <strong class="text-size-small pull-right">
                                <asp:Button runat="server" ID="btnServicesReport" Text= '<%$ Resources:Resource, Report %>' CommandArgument="3" OnClick="generateReport_Click"/>
                            </strong>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- /content -->
        
    </form>

    <script type="text/javascript" src="Default.js?<%= Guid.NewGuid().ToString("n") %>"></script>
    <%--<script type="text/javascript" src="Default.js"></script>--%>
</asp:Content>
