﻿using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.DocGenerator;
using Calani.BusinessObjects.Enums;
using System;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

public partial class Time_Reports_List : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    public string OptionsPeriod { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        long? filterEmployee = null;
        DateTime? filterDateFrom = null;
        DateTime? filterDateTo = null;

        if (!IsPostBack)
        {
            var currYearStart = new DateTime(DateTime.Now.Year, 1, 1).Ticks;
            var currYearEnd = new DateTime(DateTime.Now.Year, 12, 31).Ticks;

            hEmployeesFrom.Value = currYearStart.ToString();
            hEmployeesTo.Value = currYearEnd.ToString();
            hProjectsFrom.Value = currYearStart.ToString();
            hProjectsTo.Value = currYearEnd.ToString();
            hServicesFrom.Value = currYearStart.ToString();
            hServicesTo.Value = currYearEnd.ToString();
        }

        var orgMgr = new OrganizationManager(ph.CurrentOrganizationId);
        orgMgr.Load();

        string currentPeriod = Request.Params["p"];
        OptionsPeriod = ph.RenderPeriodsListOption(null, false, preselect: orgMgr.DefaultPeriodFilter);

        if (!String.IsNullOrWhiteSpace(currentPeriod) && currentPeriod.Contains(";"))
        {
            filterDateFrom = new DateTime(Convert.ToInt64(currentPeriod.Split(';')[0]));

            if (currentPeriod.Split(';')[1] == "*") filterDateTo = DateTime.Now;
            else filterDateTo = new DateTime(Convert.ToInt64(currentPeriod.Split(';')[1]));
        }
        else
        {
            // mois en cours pa défaut
            filterDateTo = DateTime.Now;
            filterDateFrom = new DateTime(filterDateTo.Value.Year, filterDateTo.Value.Month, 1);
        }
        
        
        var emplMgr = new Calani.BusinessObjects.Contacts.EmployeesManager(ph.CurrentOrganizationId);
        var employees = (from r in emplMgr.List() select new { id = r.id, name = r.firstName + " " + r.lastName }).ToList();
        ddlEmployees.DataSource = employees;
        ddlEmployees.DataBind();


        var prjMgr = new Calani.BusinessObjects.Projects.ProjectsManager(ph.CurrentOrganizationId);

        var list = prjMgr.ListOpens(null);
        if (list != null)
            list = (from r in list orderby r.id descending select r).ToList();

        

        var projects = (from r in list select new { id = r.id, name = r.clientName + " - " + r.internalId}).ToList();
        ddlProjects.DataSource = projects;
        ddlProjects.DataBind();

        var servicesManager = new Calani.BusinessObjects.CustomerAdmin.ServicesManager(ph.CurrentOrganizationId);
        var services = servicesManager.List();
        ddlServices.DataSource = (from s in services select new { id = s.id, name = s.name }).ToList();
        ddlServices.DataBind();
    }
        
    protected void generateReport_Click(object sender, EventArgs e)
    {
        var btn = (Button)sender;       
        ReportKind kind;
        if (!Enum.TryParse(btn.CommandArgument, true, out kind))
            kind = ReportKind.Employees;

        var employeeMgr = new EmployeesManager(ph.CurrentOrganizationId);

        var user = employeeMgr.Get(ph.CurrentUserId);
        var c = user.culture??"fr-FR";
        
        var mgr = new TimeSheetGenerator(new System.Globalization.CultureInfo(c), ph.CurrentOrganizationId);

        DateTime now = DateTime.Now;
        DateTime startDate = DateTime.Now; 
        DateTime endDate = DateTime.Now;

        long ticksFrom;
        long ticksTo;

        string selectedItem = null;

        var name = string.Empty;
        var localizer = Calani.BusinessObjects.SharedResource.Resource;
        var culture = CultureInfo.CurrentCulture;
        
        switch (kind)
        {
            case ReportKind.Employees:
                var efrom = hEmployeesFrom.Value;
                var eto = hEmployeesTo.Value;

                if(Int64.TryParse(efrom, out ticksFrom))
                {
                    startDate = new DateTime(ticksFrom);
                }
                else
                    startDate = new DateTime(now.Year, now.Month, 1);

                if (Int64.TryParse(eto, out ticksTo))
                {
                    endDate = new DateTime(ticksTo);
                }
                else
                {
                    var year = DateTime.Now.Year;
                    var month = DateTime.Now.Month;
                    endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));
                }

                selectedItem = ddlEmployees.SelectedValue;
                name = localizer.GetString("Employee", culture);
                break;

            case ReportKind.Projects:
                var pfrom = hProjectsFrom.Value;
                var pto = hProjectsTo.Value;

                if (Int64.TryParse(pfrom, out ticksFrom))
                {
                    startDate = new DateTime(ticksFrom);
                }
                else
                    startDate = new DateTime(now.Year, now.Month, 1);

                if (Int64.TryParse(pto, out ticksTo))
                {
                    endDate = new DateTime(ticksTo);
                }
                else
                {
                    var year = DateTime.Now.Year;
                    var month = DateTime.Now.Month;
                    endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));
                }

                selectedItem = ddlProjects.SelectedValue;
                name = localizer.GetString("Project", culture);
                break;

            case ReportKind.Services:

                var servicesFrom = hServicesFrom.Value;
                var servicesTo = hServicesTo.Value;

                startDate = Int64.TryParse(servicesFrom, out ticksFrom) ? new DateTime(ticksFrom) : new DateTime(now.Year, now.Month, 1);

                if (Int64.TryParse(servicesTo, out ticksTo))
                {
                    endDate = new DateTime(ticksTo);
                }
                else
                {
                    var year = DateTime.Now.Year;
                    var month = DateTime.Now.Month;
                    endDate = new DateTime(year, month, DateTime.DaysInMonth(year, month));
                }

                selectedItem = ddlServices.SelectedValue;
                name = localizer.GetString("Services", culture);
                break;
        }

        long selectedId = 0;

        Int64.TryParse(selectedItem, out selectedId);

        var file = mgr.GenerateReport(kind, startDate, endDate, selectedId);
        
        name += $" {localizer.GetString("Report", culture)?.ToLower()} {DateTime.Now.ToString("dd-MM-yyyy_HH-mm", CultureInfo.InvariantCulture)}.xls";
        Context.Response.ContentType = "application/xls";
        Context.Response.AddHeader("content-disposition", "Attachment; filename=" + name);

        Context.Response.BinaryWrite(file);
    } 
}