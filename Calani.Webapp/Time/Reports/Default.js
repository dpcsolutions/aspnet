﻿function setPeriodFilter(controlId, name) {
    let from = null;
    let to = null;

    let pval = $('#' + controlId).val(); 
    if (pval != null && pval.indexOf(';') > 0) {
        pval = pval.split(';');

        if (pval.length == 2) {
            from = pval[0];
            to = pval[1]; 

            if (from != "*")
                from = parseInt(from);
            else
                from = null;

            if (to != "*")
                to = parseInt(to);
            else
                to = null;
        }
    }

    $('#ContentPlaceHolder1_h' + name +'From').val(from);
    $('#ContentPlaceHolder1_h' + name +'To').val(to);
}

function setProjectsPeriodFilter() {
    setPeriodFilter('ddlProjectsPeriodFilter', 'Projects');
};

function setEmployeesPeriodFilter() {
    setPeriodFilter('ddlEmployeesPeriod', 'Employees');
};

function setServicesPeriodFilter() {
    setPeriodFilter('ddlServicesPeriodFilter', 'Services');
};

$(document).ready(function(){
    setProjectsPeriodFilter()
    setEmployeesPeriodFilter()
    setServicesPeriodFilter()
});

    
