﻿<%@ Page Title="TimeSheet" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Time_Sheets_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
    <div class="btn-group" runat="server" id="quickmenu">
		<button type="button" class="btn bg-gesmobile btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
		</button>

		<ul class="dropdown-menu dropdown-menu-right">
			<li><a href="Sheet.aspx"><i class="icon-plus2"></i>  <%= Resources.Resource.Time_Sheet %></a></li>
		</ul>
	</div>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        var txtTotalForPeriod = "<%= Resources.Resource.TotalForPeriod %>";
        var txtTotalHours = "<%= Resources.Resource.TotalHours %>";
        var txtTotalFor = "<%= Resources.Resource.TotalFor %>";
        var txtGrouping = "<%= Resources.Resource.Grouping %>";
        var txtEmploee = "<%= Resources.Resource.Employee %>";
        var txtDate = "<%= Resources.Resource.Date %>";
        var totalHours = "<%= TotalHours %>";
        var useWorkSchedule = <%= UseWorkSchedule.ToString().ToLower() %>;
        var isAdmin = <%= IsAdmin.ToString().ToLower() %>;
        var timesheetTemplateType = <%= TimesheetTemplateType %>;
    </script>
    <form runat="server">

        <div class="content" id="app">

            <div class="row">
                <div class="col-sm-12 col-md-4">

                    <!-- User card with thumb and social icons at the bottom -->
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row text-center">


                                <div class="col-xs-4"  v-bind:class="{ 'bordered alpha-primary': statusFilter=='currentweek' }">
                                    <p><i class="icon-pencil3 icon-2x display-inline-block text-grey tada infinite" id="i1" runat="server"></i></p>
                                    <h5 class="text-semibold no-margin" id="lblCountCurrentWeek" runat="server">.</h5>
                                    <strong class="text-muted text-size-small">
                                        <a href="" v-on:click="setStatusFilter($event, 'currentweek')" class="text-grey"><%= Resources.Resource.CurrentWeek %></a>
                                    </strong>
                                </div>


                                <div class="col-xs-4" v-bind:class="{ 'bordered alpha-primary': statusFilter=='tovalidate' }">
                                    <p><i class="icon-question6 icon-2x display-inline-block text-primary tada infinite" id="i2" runat="server"></i></p>
                                    <h5 class="text-semibold no-margin" id="lblCountToValidate" runat="server">.</h5>
                                    <strong class="text-muted text-size-small">
                                        <a href="" v-on:click="setStatusFilter($event, 'tovalidate')"><%= Resources.Resource.ToValidate %></a>
                                    </strong>
                                </div>


                                <div class="col-xs-4" v-bind:class="{ 'bordered alpha-primary': statusFilter=='validated' }">
                                    <p><i class="icon-history icon-2x display-inline-block text-success tada infinite" id="i3" runat="server"></i></p>
                                    <h5 class="text-semibold no-margin text-muted" id="lblCountValidated" runat="server">...</h5>
                                    <strong class="text-muted text-size-small">
                                        <a href="" v-on:click="setStatusFilter($event, 'validated')"><%= Resources.Resource.Archives %></a>
                                    </strong>
                                </div>

                            </div>
                        </div>

                        <div class="panel-footer text-center no-padding">
                            <div class="row">
                            </div>
                        </div>
                    </div>
                    <!-- /user card with thumb and social icons at the bottom -->

                </div>


                <div class="col-sm-6 col-md-4">
                </div>


                <div class="col-sm-7 col-md-4">
                    <div class="panel panel-body panel-body-accent">
                        <div class="media no-margin">
                            <div>
								<i class="fa fa-calendar-check-o" aria-hidden="true"></i> <%= Resources.Resource.Select_Period %>
							</div>
							<div>
								<select id="ddlPeriod" class="select" onchange="periodChanged()">
									<%= OptionsPeriod %>
								</select>
							</div>
                        </div>
                    </div>
                </div>
            </div><!-- /row -->


            <div class="row">
                <div class="col-sm-12 col-md-12">
                <div class="panel panel-white">

                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <a name="quotes">
                                <i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
                                <strong><%= ph.CMP.Name %></strong>
                            </a>
                        </h6>
                    </div>
                    <!-- panel-heading -->

                    <div class="panel-body">
                        <asp:Label runat="server" ID="lblNoData" Visible="false">
                            <%= Resources.Resource.NoData %>
                        </asp:Label>

                        <table id="timesheets-datatable" class="table table-hover dataTable">
							<thead>
								<tr>
									<th>&nbsp;</th>
									<th>&nbsp;</th>
									<th><%= Resources.Resource.Employee %></th>
									<th>&nbsp;</th>
									<th><%= Resources.Resource.Year %></th>
									<th><%= Resources.Resource.Week %></th>
									<th><%= Resources.Resource.Date %></th>
									<th><%= Resources.Resource.Comment %></th>
									<th><%= Resources.Resource.Status %></th>
									<th><%= Resources.Resource.TotalHours %></th>
								</tr>
							</thead>
                            <tfoot>
								<tr>
									<td colspan="9"></td>
								</tr>
							</tfoot>
						</table>

                    </div>
                    <!-- panel-body -->

                    <div class="panel-footer no-padding">
                        <div class="row" style="margin: 10px">
                            <a id="validate-selected-timesheets-button" onclick="validateSelectedTimesheets()" class="btn btn-default text-left" style="float: left;">
                                <%= Resources.Resource.ValidateSelectedTimesheets %>
                            </a>
                            
                            <a href="Sheet.aspx" class="btn btn-default" style="float: right;"><i class="icon-plus2 position-left"></i>
                                <%= Resources.Resource.Add_new_Time_Sheet %>
                            </a>
                        </div>
                    </div>
                    <!-- panel-footer -->

                </div>
            </div>
            </div>
        </div>
        <!-- /content -->
        
    </form>

    <script type="text/javascript">

        var _employeeId = <%= FilterEmployee ?? 0 %>;

    </script>
    <style>
        .swal2-container .swal2-popup{
            max-height: 80%;
        }
        .swal2-container .swal2-popup .swal2-content{
            overflow-y: auto;
        }
    </style>
    <script type="text/javascript" src="Default.js?<%= Guid.NewGuid().ToString("n") %>"></script>
    <%--<script type="text/javascript" src="Default.js"></script>--%>
</asp:Content>
