﻿var missingItems = [];
var mainVueComponentReference = null;

var blanklines = linesPerDay;

var minDateJs = new Date(2000, 1, 1, 0, 0, 0);
var maxDateJs = new Date(2050, 1, 1, 0, 0, 0);

var defaultDate = new Date();

if (minDate != null) {
    minDateJs = new Date(minDate);
    defaultDate = minDate;
}
if (maxDate != null) {
    maxDateJs = new Date(maxDate);
}
var DurationVal = {
    Seconds: 'seconds',
    Minutes: 'minutes',
    Hours: 'hours'
}
var TimeCategoryType = {
    ScheduleRule: 0,
    ScheduleHoliday:1,
    Custom: 2,
    Auto: 3,
    Predefined: 4,
    TimeOfDay: 11,
    Holiday: 14,
};
function getGuid(length) {
    var min = Math.ceil(10000);
    var max = Math.floor(20000);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getCategoriesByType(type) {

    return $.grep(scheduleRecList, function (v, i) {
        return v.timeType == type;
    });
}
function sysTimeCat(type) {
    var cat = getCategoriesByType(type);
    if (cat && cat.length == 1)
        return cat[0];
    return {};
}

function requestActiveContract(contactId) {
    $.ajax({
        url: "../../API/Service.svc/GetActiveContract?contactId=" + contactId,
        dataType: 'json',
        success: function (response) {
            console.log("Active contract changed:", response);
            activeContract = response;

            if (mainVueComponentReference) {
                mainVueComponentReference.onActiveContractChanged();
            }
        },
        error: function (response) {
            console.log("response", response);
        }
    });
}

$(document).ready(function () {
});

$(function () {

    $("div").on("mousedown", ".v-select", function () {
        $(".item-deleted").parent().hide();
        var cw = $(".vs__dropdown-menu").width();
        if (cw < 350)
            $(".vs__dropdown-menu").width('350px');
    });

    function rangeInCategories(start, end, holiday, dayId) {
        var records = $.grep(scheduleRecList,
            function (val, ind) {
                var m = false;
                if (val.timeType < TimeCategoryType.Custom) {
                    var vst = moment(val.timeStart, ['h:m a', 'H:m']);
                    var vet = moment(val.timeEnd, ['h:m a', 'H:m']);

                    var stSame = vst.isSameOrBefore(start, 'minute');
                    var etSame = vet.isSameOrAfter(end, 'minute');
                    var dSame = jQuery.inArray(dayId, val.timeDays) != -1;

                    if (holiday) {

                        m = stSame && etSame && val.timeType == TimeCategoryType.ScheduleHoliday;
                    }
                    else
                        m = stSame && etSame && dSame;
                }
                return m;
            });

        return records;
    }

    function categoriesInRange(start, end, holiday, dayId) {
        var records = $.grep(scheduleRecList,
            function (val, ind) {
                var m = false;
                if (val.timeType < TimeCategoryType.Custom) {
                    val.mTimeStart = moment(val.timeStart, ['h:m a', 'H:m']);
                    val.mTimeEnd = moment(val.timeEnd, ['h:m a', 'H:m']);

                    var isStart = val.mTimeStart.isBetween(start, end, 'minute', '[)');
                    var isEnd = val.mTimeEnd.isBetween(start, end, 'minute', '(]');

                    var isDay = jQuery.inArray(dayId, val.timeDays) != -1;

                    if (holiday) {
                        m = (isStart || isEnd) && val.timeType == TimeCategoryType.ScheduleHoliday;
                    }
                    else
                        m = (isStart || isEnd) && isDay;
                }
                return m;
            });

        return records;
    }

    function getMatchedTimeRecords2(datarow, st, et) {

        var dayStart = moment(datarow.date + " - " + datarow.startTime + ":00", momentDateShortTimeFormat);
        var dayEnd = moment(datarow.date + " - " + datarow.endTime + ":00", momentDateShortTimeFormat);

        var dayId = dayStart.isoWeekday();/*1-monday*/
        var holiday = false;

        $.each(calendarDays, function (key, arr) {
            $.each(arr, function (k, value) {
                var strArr = value.split(" - ");
                var mSd = moment(strArr[0], momentDateShortTimeFormat);
                var mEd = moment(strArr[1], momentDateShortTimeFormat);

                var ss = mSd.isSameOrBefore(dayStart, 'minute');
                var es = mEd.isSameOrAfter(dayEnd, 'minute');

                if (ss && es)
                    holiday = true;
            });

        });


        var rangeInCats = rangeInCategories(st, et, holiday, dayId);
        if (rangeInCats && rangeInCats.length == 1)
            return rangeInCats;
        else {
            var catsInRange = categoriesInRange(st, et, holiday, dayId);
            if (catsInRange && catsInRange.length == 1)
                return catsInRange;
            return new Array(scheduleRecList[0]);
        }
        
    }

    function timeToMoment(time) {
        return   moment(time, ['h:m a', 'H:m']);
    }

    function timeToDuration(time, durationVal) {
        if (!time)
            return 0;

        var sec = timeToMoment(time).diff(moment().startOf('day'), 'seconds');
        var res = sec;
        switch (durationVal) {
            case 'seconds':
                break;
            case 'minutes':
                res = precisionRound((sec / 60), 2);
                break;
            case 'hours':
                res = precisionRound((sec / (60*60)), 2);
                break;
            default:
                break;

        }
        return res;
    }

    function formatTime(seconds) {

        if (!seconds || seconds < 60)
            return '00:00';

        var h = Math.floor(seconds / 3600);
        var m = Math.floor((seconds % 3600) / 60);

        if (h < 24) {
            var ms = seconds * 1000;
            return moment.utc(ms).format(moment.HTML5_FMT.TIME);
        }
    
        var res= [h ,m > 9 ? m : (h ? '0' + m : m || '0')].filter(Boolean).join(':');

        return res;
    }

    function getDefaultTimeCategory() {
        // For timesheet simplifiedtemplate with type=3 select Time-of-day as default, otherwise - the 1st one
        var defaultTimeCategory = $.grep(scheduleRecList, function (timeCategory) {
            return timesheetType != 3 || timeCategory.timeType == TimeCategoryType.TimeOfDay;
        })[0];
        return defaultTimeCategory;
    }

    function getRecord(date, parentId, start, end, rowType, lunch, enteredDuration, dayOfWeek, visit, projectId, serviceId, comment, isDateReadonly, pause, rate, time) {
        var customCat = sysTimeCat(TimeCategoryType.Custom);

        var item = {
            id: 0,
            position: getGuid(4),
            isParent: false,
            parentId: parentId,
            warn: false,
            date: date,
            dayOfWeek: dayOfWeek,
            startTime: start||'',
            endTime: end||'',
            rate: rate || customCat.timeRate,
            duration: 0,
            time: time || customCat.timeName,
            serviceId: serviceId||null,
            jobId: projectId || null,
            visit: visit || null,
            recordType: 0,
            rowType: rowType,
            lunchDuration: lunch,
            pauseDuration: pause,
            enteredDuration: enteredDuration,
            comment: comment,
            isDateReadonly: isDateReadonly === true,

        };
        return item;
    }

    Vue.component('v-select', VueSelect.VueSelect);

    Vue.component('day-item',
        {
            template: '#day-item',
            mixins: [Vue2Filters.mixin],
            props: {
                ind: Number,
                selected: Boolean,
                lunchChecked: Boolean,
                details: Object,
                previousLunch: Number,
                total:Number
            },

            computed: {
                hours: function() {
                    var d = this.duration;

                    var res = formatTime(d);

                    return res;
                },
                duration: function () {
                    var duration = 0;
                    
                    if (this.rowsDuration > 0) {
                        duration = this.rowsDuration;

                    } else {
                        var sec = timeToDuration(this.details.enteredDuration, DurationVal.Seconds);
                        if (sec > 60)
                            duration = sec;
                    }

                    var dur_lunch = duration - (this.details.lunch || 0) * 60;

                    dur_lunch = dur_lunch > 0 ? dur_lunch : 0;

                    Vue.set(this.details, 'total', dur_lunch);

                    return dur_lunch;
                },

                rowsHours: function () {
                    var d = this.rowsDuration;

                    var res = formatTime(d);

                    return res;
                },
                rowsDuration: function () {
                    var duration = 0;

                    var rec = this.details.records;

                    $.each(rec,
                        function (i, val) {
                            if (val.startTime && val.endTime && !val.isParent) {
                                var start = timeToMoment(val.startTime);
                                var end = timeToMoment(val.endTime);

                                var d = moment.duration(end.diff(start));

                                var h = d.asSeconds();

                                h *= (val.rate || 100) / 100;

                                duration += (h || 0);
                            }

                        });

                    this.details.rowsChecked = duration && duration > 0;
                    return duration;
                },
                overtime: function() {
                    return false;
                }
            },
            methods: {
                checkLunch: function (e) {

                    this.lunchChecked = e.target.checked;
                    
                    if (!this.lunchChecked) {
                        this.details.previousLunch = this.details.lunch;
                        this.details.lunch = 0;

                    } else if (this.details.previousLunch>0)
                        this.details.lunch = this.details.previousLunch;

                },
                onDayDuration: function (e) {
                    
                    if (this.details.rowsChecked && this.rowsDuration>0) {
                        var self = this;
                        

                        swal({
                                title: resources['Confirmation'],
                                text: resources['EnteredRecordsWillBeDeleted'],
                                type: "warning",
                                showCancelButton: true,
                                cancelButtonText: resources['No_Cancel'],
                                confirmButtonColor: "#F44336",
                                confirmButtonText: resources['Yes_Delete']
                            })

                            .then((value) => {
                                if (value.value != null && value.value == true) {
                                    self.details.rowsChecked = false;
                                    var item = getRecord(self.details.date, null, '', '', 1, self.details.lunch, self.details.enteredDuration);
                                    self.details.records = [item];

                                    self.details.servicesRec = [
                                        {
                                            id: 0,
                                            date: self.details.date,
                                            duration: '',
                                            serviceId: null,
                                            jobId: null,
                                            recordType: 1
                                    }];


                                    vts.$refs.timesheettable.setRecords(self.details.records, self.details.date);

                                    vts.$refs.timesheetservicestable.setRecords(self.details.servicesRec, self.details.date);
                                }
                            });

                       
                    }
                    else
                        this.details.rowsChecked = !this.details.rowsChecked;
                },
                onTimeChanged(e) {
                    if (e && e.target) {
                        var newVal = '';

                        var value = $(e.target).val();

                        var m = moment(value, moment.HTML5_FMT.TIME);
                        var res = m.isValid();

                        if (res)
                            newVal = m.format(moment.HTML5_FMT.TIME);

                        Vue.set(this.details, 'enteredDuration', newVal);
                    }
                }
            }
        });

    Vue.component('timesheet-table-row',
        {
            template: '#timesheet-table-row',
            props: {
                index: Number,
                position:Number,
                item: Object,
                week: Array,
                options: Array,
                isdatereadonly: Boolean,
                hiderate: Boolean,
            },
            computed: {
                warn: function() {
                    return true;
                }
            },
            methods: {
               
                getDuration() {
                    var st = timeToMoment(this.item.startTime);
                    var et = timeToMoment(this.item.endTime);

                    var duration = this.item.duration;
                    var rate = this.item.rate;

                    if (st.isValid() && et.isValid()) {

                        var d = moment.duration(et.diff(st));
                        duration = d.asHours();
                    }

                    if (this.item.isParent){
                        var childs = this.$parent.getChilds(this.item);

                        duration = 0;
                        rate = 0;
                        var timeDuration = 0;
                        $.each(childs, function (i, v) {
                            duration += v.duration;
                            timeDuration += v.duration * 100 / v.rate;
                        });
                        rate = duration / (timeDuration||1)*100;
                        Vue.set(this.item, 'rate', precisionRound(rate, 2));
                    }
                    else
                        duration = duration * ((rate || 0) / 100);

                    Vue.set(this.item, 'duration', precisionRound(duration, 2));
                },
                onTimeChanged( prop, e) {
                    if (e && e.target) {
                        var newVal =  '00:00';

                        var value = $(e.target).val();

                        var m = moment(value, moment.HTML5_FMT.TIME);
                        var res = m.isValid();

                        if (res) {

                            var st = timeToMoment(this.item.startTime);
                            var et = timeToMoment(this.item.endTime);

                            newVal = m.format(moment.HTML5_FMT.TIME);

                            if (st.isValid() && et.isValid()) {
                                if (st.isSameOrAfter(et)) {
                                    newVal = '';
                                    Vue.set(this.item, 'duration', 0);
                                    vts.$refs.timesheettable.showTimeWarning(true);
                                }
                                else {
                                    Vue.set(this.item, 'rowType', 0);

                                    this.$parent.regenerateItemRecords(this.item, this.index);
                                }
                            }
                        }

                        Vue.set(this.item, prop, newVal);
                    }
                },
                onDayChanged(e) {

                    var dayOfWeek = e.target.value;
                    this.item.date = $.grep(this.week, function (d, i) {
                        return d.dayOfWeek == dayOfWeek;
                    })[0].date;

                },
                deleteRow () {
                    this.$parent.deleteRow(this.index);
                },
                onScheduleRecChanged(e) {
                    this.$parent.onScheduleRecChanged(this.item, e);
                },
                click() {
                   
                }
            },
            created: function () {
                let defaultTimeCategory = getDefaultTimeCategory();
                if (!this.item.time || this.item.time === defaultTimeCategory.timeName || resources[this.item.time] === defaultTimeCategory.timeName) {
                    this.item.time = defaultTimeCategory.timeName;
                    this.item.timeId = defaultTimeCategory.timeId;
                    this.item.timeName = defaultTimeCategory.timeName;
                }
            }
        });

/*
 * Simplified template components 
 */

    Vue.component('timesheet-table-simplified-day',
        {
            template: '#timesheet-table-simplified-day',
            mixins: [Vue2Filters.mixin],

            data: function () {
                return {
                    date: '',
                    records: {},
                    week: [],
                    dayIndex: -1,
                    pauseNotTaken: false,
                };
            },

            props: {
                records: Array,
                dayindex: String,
                week: Array,
                isTimeWarning: Boolean,
            },

            computed: {
                totalDay: function () {

                    var duration = 0;
                    if (this.records && this.records.length > 0) {
                        var rec = this.records;

                        $.each(rec, function (i, val) {
                            var start, end;

                            if (val.startTime && val.endTime) {
                                if (!val.isParent) {
                                    start = timeToMoment(val.startTime);
                                    end = timeToMoment(val.endTime);
                                }
                            }
                            else if (val.enteredDuration) {

                                start = timeToMoment('00:00');
                                end = timeToMoment(val.enteredDuration);
                                end.add(val.lunchDuration * (-1), 'm');
                            }

                            if (start && end) {
                                var d = moment.duration(end.diff(start));
                                var h = d.asSeconds();

                                h *= (val.rate || 100) / 100;

                                duration += (h || 0);
                            }
                        });
                    }

                    return duration;
                },
                totalDayFormatted: function () {
                    var duration = this.totalDay;
                    var res = formatTime(duration);
                    return res;
                },
                breakDurationSeconds: function () {
                    // Saturdays have pause = 0
                    if (this.dayIndex == 5) {
                        return 0;
                    }

                    return this.$data.pauseNotTaken ? 0 : 60 * orgDailyBreakMin;
                },
                breakDurationFormatted: function () {
                    return formatTime(this.breakDurationSeconds);
                }
            },

            methods: {

                addLine(ind, parentIndex, date, start, end, dayOfWeek, visit, projectId, serviceId, comment, rate, timeCategory) {
                    var index = (ind == 0 || ind) ? ind + 1 : this.records.length;

                    date = date || this.$data.date;
                    dayOfWeek = dayOfWeek || this.$data.dayIndex + 1;

                    var rowType = timesheetType ? 0 : 1;
                    var item = getRecord(date, parentIndex, start, end, rowType, this.getLunchDuration(), '', dayOfWeek, visit, projectId, serviceId, comment, true, this.getPauseDuration(), rate);
                    var duration = this.calculateRecordDurationHours(item);
                    Vue.set(item, 'duration', duration);

                    var defaultTimeCategory = getDefaultTimeCategory();
                    Vue.set(item, 'time', timeCategory ? timeCategory : defaultTimeCategory.timeName);
                    Vue.set(item, 'rate', rate ? rate : defaultTimeCategory.timeRate);

                    this.records.splice(index, 0, item);
                    this.$data.records = this.records;

                    this.$root.$emit('dayDurationChanged');

                    return item;
                },

                onScheduleRecChanged(item, event, rec) {
                    if (!item) {
                        this.$root.$emit('dayDurationChanged');
                        return;
                    }

                    if (!event && !rec) {/*nothing found*/
                        var cat = getDefaultTimeCategory();
                        Vue.set(item, 'time', cat.timeName);
                        Vue.set(item, 'rate', cat.timeRate);

                    } else {
                        //Vue.set(item, 'time', event && event.target.value || rec && rec.timeName);

                        var selected = rec || $.grep(scheduleRecList,
                            function (val, ind) {
                                return val.timeName == item.time;
                            })[0];

                        if (selected)
                            Vue.set(item, 'rate', selected.timeRate);
                    }
                    
                    var duration = this.calculateRecordDurationHours(item);
                    Vue.set(item, 'duration', duration);

                    this.$root.$emit('dayDurationChanged');
                },

                deleteEmptyRows() {

                    for (i = this.records.length - 1; i >= 0; --i) {
                        var v = this.records[i];

                        if (this.calculateRecordDurationHours(v) == 0) {
                            Vue.delete(this.records, i);
                        }
                    }
                },

                deleteRow(index, childOnly) {

                    var rec = this.records[index];
                    recId = rec.id > 0 ? rec.id : rec.position;

                    if (!childOnly) {
                        Vue.delete(this.records, index);
                    }

                    for (i = this.records.length - 1; i >= 0; --i) {
                        var v = this.records[i];

                        if (v.parentId != null && v.parentId == recId) {
                            Vue.delete(this.records, i);
                        }
                    }

                    this.$root.$emit('dayDurationChanged');
                },

                regenerateItemRecords(item, index) {

                    var rec = null;
                    index = index || item.index;
                    var st = timeToMoment(item.startTime);
                    var et = timeToMoment(item.endTime);
                    var tr = getMatchedTimeRecords2(item, st, et);
                    rec = tr[0];

                    this.onScheduleRecChanged(item, null, rec);

                    this.$root.$emit('dayDurationChanged');
                },

                getLunchDuration() {
                    return timesheetType == 3 ? 0 : orgLunch;
                },

                getPauseDuration() {
                    return timesheetType == 3 ? orgDailyBreakMin : 0;
                },

                initPauseNotTakenValue() {
                    // Check records if any not empty record has pause > 0

                    if (this.records.length == 0) {
                        // Saturdays have pause = 0
                        this.$data.pauseNotTaken = this.dayIndex == 5;
                        return;
                    }

                    var anyRecordWithPause = $.grep(this.records, function (record) {
                        return !record.startTime || record.pauseDuration > 0;
                    });

                    this.$data.pauseNotTaken = anyRecordWithPause.length == 0;
                },

                onPauseNotTakenChanged() {

                    var pauseDurationMin = this.getPauseDuration();

                    for (var i in this.records) {
                        var record = this.records[i];
                        record.pauseDuration = 0;
                    }

                    if (!this.$data.pauseNotTaken && this.records.length > 0) {
                        // Set pause to the first timesheet record
                        this.records[0].pauseDuration = pauseDurationMin;
                    }

                    this.$data.records = this.records;

                    this.$root.$emit('dayDurationChanged');
                },

                initPopup: function () {
                    var buttonNode = $('#btnDuplicateLastLine-' + this.dayindex, this.$el)[0];
                    var dayIndex = this.dayindex;
                    $(buttonNode).popover({
                        container: 'body',
                        placement: 'top',
                        html: true,
                        trigger: 'manual',
                        content: function () {
                            var content = $('#duplicate-form-' + dayIndex).html();
                            return content;
                        }
                    })
                    .click(function (e) {
                        $(this).popover('show');
                        e.preventDefault();
                    });

                    $(buttonNode).on('shown.bs.popover', function () {
                        $('#duplicate-btn-' + dayIndex).click(function () { console.log('click ' + dayIndex); return false; });
                    });
                },

                calculateRecordDurationHours: function(item) {
                    var duration = 0;

                    var st = timeToMoment(item.startTime);
                    var et = timeToMoment(item.endTime);

                    var duration = item.duration;
                    var rate = item.rate;

                    if (st.isValid() && et.isValid()) {
                        var d = moment.duration(et.diff(st));
                        duration = d.asHours();
                    }

                    duration = duration * ((rate || 0) / 100);

                    return duration;
                },
            },

            created() {
                this.$data.records = this.records;
                this.$data.week = this.week;
                this.$data.dayIndex = 1 * this.dayindex;
                this.$data.date = this.week[this.dayindex].date;

                this.initPauseNotTakenValue();
                this.onPauseNotTakenChanged();
            },

            mounted() {
                var _this = this;
                setTimeout(function () {
                    _this.initPopup();
                }, 10);
            },
        });

    Vue.component('timesheet-table-simplified',
        {
            template: '#timesheet-table-simplified',
            mixins: [Vue2Filters.mixin],

            data: function () {
                return {
                    date: '',
                    records: {},
                    week: [],
                    
                    summary: {
                        totalWeekDurationSeconds: 0,
                        totalWeekDurationFormatted: '',
                        totalOvertimeSeconds: 0,
                        totalOvertimeFormatted: '',
                        overtimeRate1Seconds: 0,
                        overtimeRate1Formatted: '',
                        overtimeRate2Seconds: 0,
                        overtimeRate2Formatted: '',
                        timeToBeCompensatedSeconds: 0,
                        timeToBeCompensatedFormatted: '',
                    },
                };
            },

            props: {
                isTimeWarning: Boolean,
                daysComponents: Array,
            },

            computed: {
                totalWeek: function () {
                    var duration = 0;

                    if (typeof this.daysComponents === "undefined") {
                        return duration;
                    }

                    var days = this.daysComponents;
                    for (var dayIdx = 0; dayIdx < days.length; dayIdx++) {
                        duration += days[dayIdx].totalDay;
                    }

                    this.$data.totalWeekDurationSec = duration;

                    return duration;
                },
                totalWeekFormatted: function () {
                    var duration = this.totalWeek;
                    var res = formatTime(duration);
                    return res;
                },
            },

            methods: {

                setRecords(week) {

                    var records = {};
                    for (var i in week.days) {
                        records[i] = $.merge([], week.days[i].records);

                        if (this.$refs.days && this.$refs.days.length > i) {
                            this.$refs.days[i].$data.date = week.days[i].date;
                            this.$refs.days[i].$data.records = week.days[i].records;

                            for (var r in this.$refs.days[i].$data.records) {
                                if (!this.$refs.days[i].$data.records[r].time) {
                                    this.$refs.days[i].$data.records[r].time = getDefaultTimeCategory().timeName;
                                }
                            }
                        }
                    }
                    
                    this.$data.records = records;

                    Vue.set(this.$data, 'records', records);
                },

                getAllRecords() {
                    return this.flattenRecords();
                },

                addLine(ind, parentIndex, date, start, end, dayOfWeek, visit, projectId, serviceId, desc, timeCategory) {
                    if (typeof this.$data.records[dayOfWeek - 1] === 'undefined') {
                        this.$data.records[dayOfWeek - 1] = [];
                    }

                    timeCategory = timeCategory === undefined ?  getDefaultTimeCategory() : timeCategory;
                    var dayRecords = this.$data.records[dayOfWeek - 1];

                    var index = (ind == 0 || ind) ? ind + 1 : dayRecords.length;
                    date = date || this.date;

                    var rowType = timesheetType ? 0 : 1;
                                        
                    var item = getRecord(date, parentIndex, start, end, rowType, this.getLunchDuration(), '', 
                        dayOfWeek, visit, projectId, serviceId, desc, true, this.getPauseDuration(), timeCategory.timeRate, timeCategory.timeName);
                    
                    dayRecords.splice(index, 0, item);

                    return item;
                },

                duplicateRecords(fromDayIndex, duplicateOption) {
                    if (this.$refs.days.length <= fromDayIndex) {
                        console.error('simplified timesheet table duplicateRecords(): day with index ' + fromDayIndex + ' is not available.');
                        return;
                    }

                    if (duplicateOption !== "week" && duplicateOption != "nextday") {
                        console.error('simplified timesheet table duplicateRecords(): unknown option = ' + duplicateOption + '. Expected values = [week, nextday].');
                        return;
                    }

                    var recordsToDuplicate = this.$data.records[fromDayIndex];
                    var daysToAddRecordsTo = [];

                    switch (duplicateOption) {
                        case "nextday":
                            if (this.$refs.days.length > (fromDayIndex + 1)) {
                                daysToAddRecordsTo.push(this.$refs.days[fromDayIndex + 1]);
                            }
                            break;
                        case "week":
                            for (var i in this.$refs.days) {
                                var day = this.$refs.days[i];
                                if (day.dayIndex > fromDayIndex && day.dayIndex < 5 && day.totalDay == 0) {
                                    daysToAddRecordsTo.push(day);
                                }
                            }
                            break;
                    }
                    
                    for (var i = 0; i < daysToAddRecordsTo.length; i++) {
                        var dayToAddTo = daysToAddRecordsTo[i];
                        for (var r = 0; r < recordsToDuplicate.length; r++) {
                            var record = recordsToDuplicate[r];
                            dayToAddTo.deleteEmptyRows();
                            dayToAddTo.addLine(null, null, null, record.startTime, record.endTime, dayToAddTo.dayOfWeek,
                                record.visit, record.jobId, record.serviceId, record.comment, record.rate, record.time);
                        }
                    }

                    this.recalculateWeeklySummary();
                },

                generateDays: function () {

                    var d = getDateOfISOWeek($('#ContentPlaceHolder1_tbxWeek').val(), $('#ContentPlaceHolder1_tbxYear').val());
                    var week = [];

                    for (var j = 0; j < getWeekDaysCount(); j++) {
                        var m = moment(d).add(j, 'd');

                        var date = m.format(momentDateFormat);
                        
                        var day = {
                            ind: j,
                            name: m.format('ddd, DD'),
                            date: date,
                            num: m.format('DD'),
                            dayOfWeek: m.day()

                        };

                        week.push(day);
                    }

                    this.week = week;

                    Vue.set(this.week, 'week', true);
                },

                countRecords: function () {
                    if (!this.records) return 0;

                    var count = this.week
                        .map((day, dayIdx) => typeof this.records[dayIdx] !== 'undefined' ? this.records[dayIdx].length : 0)
                        .reduce((a, b) => a + b, 0);

                    return count;
                },

                flattenRecords: function () {
                    var flattened = [];

                    this.updatePauseDuration();
                    for (var dayIdx in this.week) {
                        flattened = $.merge(flattened, typeof this.records[dayIdx] !== 'undefined' ? this.records[dayIdx] : []);
                    }

                    return flattened;
                },

                updatePauseDuration: function () {
                    // Make sure pause duration is attahced to the 1st record with non 0 duration

                    var days = this.$refs.days;
                    for (var dayIdx = 0; dayIdx < days.length; dayIdx++) {

                        var pauseDurationMin = days[dayIdx].getPauseDuration();
                        var pauseSet = false;
                        for (var i in this.records[dayIdx]) {
                            var record = this.records[dayIdx][i];

                            // Saturdays have pause = 0
                            if (record.dayOfWeek != 6 && !pauseSet && !days[dayIdx].$data.pauseNotTaken && record.duration > 0) {
                                record.pauseDuration = pauseDurationMin;
                                pauseSet = true;
                            }
                            else {
                                record.pauseDuration = 0;
                            }
                        }
                    }
                },

                getLunchDuration() {
                    return timesheetType == 3 ? 0 : orgLunch;
                },

                getPauseDuration() {
                    return timesheetType == 3 ? orgDailyBreakMin : 0;
                },

                calculateTotalWeekSeconds: function (days) {
                    var duration = 0;

                    if (typeof days === "undefined" || !days.length) {
                        //console.log('calculateTotalWeekSeconds: days not set yet');

                        this.$data.summary.totalWeekDurationSeconds = 0;
                        this.$data.summary.totalWeekDurationFormatted = formatTime(0);
                        return duration;
                    }

                    var dayDuration = 0;
                    for (var dayIdx = 0; dayIdx < days.length; dayIdx++) {                       
                        
                        dayDuration = days[dayIdx].totalDay;
                        if (dayDuration >= days[dayIdx].breakDurationSeconds) {
                            dayDuration -= days[dayIdx].breakDurationSeconds;
                        }

                        duration += dayDuration;
                    }

                    this.$data.summary.totalWeekDurationSeconds = duration;
                    this.$data.summary.totalWeekDurationFormatted = formatTime(duration);

                    return duration;
                },

                calculateTotalOvertimeSeconds: function () {
                    if (!activeContract) {
                        console.warn('Active contract is not available.');

                        this.$data.summary.totalOvertimeSeconds = 0;
                        this.$data.summary.totalOvertimeFormatted = formatTime(0);
                        return 0;
                    }

                    if (activeContract.OvertimeEnabled === false) {
                        this.$data.summary.totalOvertimeSeconds = 0;
                        this.$data.summary.totalOvertimeFormatted = formatTime(0);
                        return 0;
                    }

                    var weekNormalHours = activeContract.Hours;
                    var totalOvertimeSeconds = Math.max(this.$data.summary.totalWeekDurationSeconds - 60 * 60 * weekNormalHours, 0);

                    this.$data.summary.totalOvertimeSeconds = totalOvertimeSeconds;
                    this.$data.summary.totalOvertimeFormatted = formatTime(totalOvertimeSeconds);

                    return totalOvertimeSeconds;
                },

                // Calculate time not applying rate
                calculateOvertimeRate2Seconds: function () {
                    if (!activeContract) {
                        console.warn('Active contract is not available.');

                        this.$data.summary.overtimeRate2Seconds = 0;
                        this.$data.summary.overtimeRate2Formatted = formatTime(0);
                        return 0;
                    }

                    if (activeContract.OvertimeEnabled === false) {
                        this.$data.summary.overtimeRate2Seconds = 0;
                        this.$data.summary.overtimeRate2Formatted = formatTime(0);
                        return 0;
                    }

                    if (activeContract.Overtime2Enabled === false) {
                        this.$data.summary.overtimeRate2Seconds = 0;
                        this.$data.summary.overtimeRate2Formatted = formatTime(0);
                        return 0;
                    }

                    var totalOvertimeSeconds = this.calculateTotalOvertimeSeconds();
                    var thresholdSeconds = 60 * 60 * activeContract.Overtime2ThresholdHours;
                    var overtimeRate2Seconds = Math.max(totalOvertimeSeconds - thresholdSeconds, 0);
                    var rate = 0.01 * activeContract.OvertimeRate2;

                    this.$data.summary.overtimeRate2Seconds = overtimeRate2Seconds;
                    this.$data.summary.overtimeRate2Formatted = formatTime(rate * overtimeRate2Seconds);

                    return overtimeRate2Seconds;
                },

                // Calculate time not applying rate
                calculateOvertimeRate1Seconds: function () {
                    if (!activeContract) {
                        console.warn('Active contract is not available.');

                        this.$data.summary.overtimeRate1Seconds = 0;
                        this.$data.summary.overtimeRate1Formatted = formatTime(0);
                        return 0;
                    }

                    if (activeContract.OvertimeEnabled === false) {
                        this.$data.summary.overtimeRate1Seconds = 0;
                        this.$data.summary.overtimeRate1Formatted = formatTime(0);
                        return 0;
                    }

                    var totalOvertimeSeconds = this.calculateTotalOvertimeSeconds();
                    var overtimeRate2Seconds = this.calculateOvertimeRate2Seconds();
                    var overtimeRate1Seconds = totalOvertimeSeconds - overtimeRate2Seconds;
                    var rate = 0.01 * activeContract.OvertimeRate;

                    this.$data.summary.overtimeRate1Seconds = overtimeRate1Seconds;
                    this.$data.summary.overtimeRate1Formatted = formatTime(rate * overtimeRate1Seconds);

                    return overtimeRate1Seconds;
                },

                calculateHoursToBeCompensated: function () {
                    if (!activeContract) {
                        console.warn('Active contract is not available.');

                        this.$data.summary.timeToBeCompensatedSeconds = 0;
                        this.$data.summary.timeToBeCompensatedFormatted = formatTime(0);
                        return 0;
                    }

                    if (activeContract.OvertimeEnabled === false) {
                        this.$data.summary.timeToBeCompensatedSeconds = 0;
                        this.$data.summary.timeToBeCompensatedFormatted = formatTime(0);
                        return 0;
                    }

                    var rate1 = 0.01 * activeContract.OvertimeRate;
                    var rate2 = 0.01 * activeContract.OvertimeRate2;

                    var overtimeRate1Seconds = this.calculateOvertimeRate1Seconds();
                    var overtimeRate2Seconds = this.calculateOvertimeRate2Seconds();

                    var toBeCompensatedSeconds = rate1 * overtimeRate1Seconds + rate2 * overtimeRate2Seconds;

                    this.$data.summary.timeToBeCompensatedSeconds = toBeCompensatedSeconds;
                    this.$data.summary.timeToBeCompensatedFormatted = formatTime(toBeCompensatedSeconds);

                    return toBeCompensatedSeconds;
                },

                recalculateWeeklySummary: function () {
                    this.calculateTotalWeekSeconds(this.$refs.days);
                    this.calculateTotalOvertimeSeconds();
                    this.calculateOvertimeRate1Seconds();
                    this.calculateOvertimeRate2Seconds();
                    this.calculateHoursToBeCompensated();

                    $('#total-week-hours').val(this.$data.summary.totalWeekDurationFormatted);

                    return this.$data.summary.totalWeekDurationFormatted;
                }

            },

            mounted() {
                var _this = this;
                setTimeout(function () {
                    _this.daysComponents = _this.$refs.days;
                    _this.recalculateWeeklySummary();
                }, 10);
            },

            created() {

                this.$root.$on('projectsTotalChanged', function (pt) {
                    var projectDuration = parseFloat(pt);

                    var h = (vts.$refs.timesheettable.totalDay || 0) / 3600;

                    var dayDuration = parseFloat(precisionRound(h, 2));

                    if (projectDuration > dayDuration)
                        this.$root.$emit('exceedMaximumValue', vts.$refs.timesheettable.totalDay);
                    else
                        this.$root.$emit('exceedMaximumValue', 0);
                });

                this.$root.$on('dayDurationChanged', this.recalculateWeeklySummary);

            }
        });

/*
* End of Simplified template components
*/

    Vue.component('timesheet-table',
        {
            template: '#timesheet-table',
            mixins: [Vue2Filters.mixin],

            data: function () {
                return {
                    date: '',
                    records: [],
                    week: []
                };
            },
            props: {
                isTimeWarning: Boolean,
            },
            computed: {
                totalDay: function () {
                    var duration = 0;
                    if (this.records && this.records.length > 0) {
                        var rec = this.records;

                        $.each(rec, function (i, val) {
                            var start, end;
                            if (val.startTime && val.endTime) {
                                if (!val.isParent) {
                                    start = timeToMoment(val.startTime);
                                    end = timeToMoment(val.endTime);
                                }
                            }
                            else if (val.enteredDuration) {

                                start = timeToMoment('00:00');
                                end = timeToMoment(val.enteredDuration);
                                end.add(val.lunchDuration * (-1), 'm');
                            }

                            if (start && end) {
                                var d = moment.duration(end.diff(start));
                                var h = d.asSeconds();

                                h *= (val.rate || 100) / 100;

                                duration += (h || 0);
                            }



                        });
                    }
                    return duration;
                },
                totalDayFormatted: function () {
                    var duration = this.totalDay;
                    var res = formatTime(duration);
                    return res;
                }
            },
            methods: {

                setRecords(week) {

                    var records = [];
                    $.grep(week.days, function (d, i) {
                        $.merge(records, d.records);
                    });

                    this.$data.records = records;
                },

                getAllRecords() {
                    return this.$data.records;
                },

                addLine(ind, parentIndex, date, start, end, dayOfWeek, visit, projectId, serviceId, desc, timeCategory) {
                    var index = (ind == 0 || ind) ? ind + 1 : this.records.length;
                    date = date || this.date;

                    var rowType = timesheetType ? 0 : 1;
                    var lunch = timesheetType ? 0 : orgLunch;

                    timeCategory = timeCategory === undefined ?  getDefaultTimeCategory() : timeCategory;
                    
                    var item = getRecord(date, parentIndex, start, end, rowType, lunch, '',
                        dayOfWeek, visit, projectId, serviceId, desc, null, null, timeCategory.timeRate, timeCategory.timeName);
                    this.$data.records.splice(index, 0, item);

                    return item;
                }, 

                onScheduleRecChanged(item, event, rec) {
                    if (!event && !rec) {/*nothing found*/
                        var cat = sysTimeCat(TimeCategoryType.Custom);
                        Vue.set(item, 'time', cat.timeName);
                        Vue.set(item, 'rate', cat.timeRate);

                    } else {
                        //Vue.set(item, 'time', event && event.target.value || rec && rec.timeName);


                        var selected = rec || $.grep(scheduleRecList,
                            function (val, ind) {
                                return val.timeName == item.time;
                            })[0];

                        if (selected)
                            Vue.set(item, 'rate', selected.timeRate);
                    }


                },

                deleteRow(index, childOnly) {
                    var rec = this.$data.records[index];
                    recId = rec.id > 0 ? rec.id : rec.position;

                    if (!childOnly) {
                        Vue.delete(this.$data.records, index);
                        /* var self = this;
                         $.each(this.$data.records, function (i, v) {
                             if (v.isParent)
                                 self.regenerateItemRecords(v);
                         });*/
                    }


                    for (i = this.$data.records.length - 1; i >= 0; --i) {
                        var v = this.$data.records[i];

                        if (v.parentId != null && v.parentId == recId) {
                            Vue.delete(this.$data.records, i);
                        }
                    }



                },

                getChilds(item) {

                    return $.grep(this.$data.records, function (v, i) {
                        return (v.parentId == item.id && item.id > 0) || v.parentId == item.position;
                    });



                },

                regenerateItemRecords(item, index) {

                    index = index || item.index;
                    var st = timeToMoment(item.startTime);

                    var et = timeToMoment(item.endTime);

                    var tr = getMatchedTimeRecords2(item, st, et);

                    if (tr && tr.length == 1) {
                        Vue.set(item, 'warn', false);
                        this.onScheduleRecChanged(item, null, tr[0]);

                        if (item.isParent) {
                            item.isParent = false;
                            this.deleteRow(index, true);
                        }

                    } else {
                        if (tr && tr.length > 0) {
                            item.isParent = true;
                            this.deleteRow(index, true);
                            for (var i = 0; i < tr.length; i++) {
                                var v = tr[i];
                                var start = moment.max(st, v.mTimeStart);
                                var end = moment.min(et, v.mTimeEnd);
                                var parentId = item.id || item.position;
                                var addedItem = this.addLine(index + i, parentId, item.date, start.format(moment.HTML5_FMT.TIME), end.format(moment.HTML5_FMT.TIME));

                                this.onScheduleRecChanged(addedItem, null, v);
                            }
                            var autoCat = sysTimeCat(TimeCategoryType.Auto);
                            this.onScheduleRecChanged(item, null, autoCat);
                        }
                        else {
                            Vue.set(item, 'warn', true);

                            this.onScheduleRecChanged(item, null, null);
                        }
                    }
                },

                showTimeWarning(value) {

                    this.isTimeWarning = value;
                },

                generateDays: function () {
                    var d = getDateOfISOWeek($('#ContentPlaceHolder1_tbxWeek').val(), $('#ContentPlaceHolder1_tbxYear').val());
                    var week = [];
                    for (var j = 0; j < 7; j++) {
                        var m = moment(d).add(j, 'd');

                        var date = m.format(momentDateFormat);

                        var day = {
                            ind: j,
                            name: m.format('ddd, DD'),
                            date: date,
                            num: m.format('DD'),
                            dayOfWeek: m.day()

                        };

                        week.push(day);
                    }
                    this.week = week;
                    Vue.set(this.week, 'week', true);

                }
            },

            created() {
                this.$root.$on('projectsTotalChanged', function (pt) {
                    var projectDuration = parseFloat(pt);

                    var h = (vts.$refs.timesheettable.totalDay || 0) / 3600;

                    var dayDuration = parseFloat(precisionRound(h, 2));

                    if (projectDuration > dayDuration)
                        this.$root.$emit('exceedMaximumValue', vts.$refs.timesheettable.totalDay);
                    else
                        this.$root.$emit('exceedMaximumValue', 0);
                });
            }
        });

    var vts = new Vue({
        el: '#vueTimeSheet',
        mixins: [Vue2Filters.mixin],
        data: {
            tableDataSource: tableDataSource,
            timesheetType: timesheetType,
            week: {
                selectedInd: 0,
                days: []
            },
            selectedDay: function () {
                return this.week.days[this.week.selectedInd];
            }
        },
        computed: {           
            totalWeekHours: function () {
                if (vts) {
                    var duration = vts.$refs.timesheettable.totalDay;
                    var res = formatTime(duration);
                    return res;
                }
                return '';

              
            },
            selectedDayStr: function () {
                var day = this.selectedDay();
                if (day)
                    return moment(day.date, momentDateFormat).format("dddd, MMMM Do");
                return "";

            }
        },
        methods:
        {
            duplicateRecords(dayIndex, optionSelected) {

                if (!vts.$refs.timesheettable.duplicateRecords) {
                    console.error('vts.duplicateRecords: duplicateRecords() method is not available in timesheet table.');
                    return;
                }

                vts.$refs.timesheettable.duplicateRecords(dayIndex, optionSelected);
            },

            onAddLineClick() {
                
                vts.$refs.timesheettable.addLine(null, null, this.selectedDay().date);
            },

            getTotalWeekHours: function () {
                if (vts) {

                    // Simplified timesheet #3
                    if (vts.$refs.timesheettable.recalculateWeeklySummary) {
                        return vts.$refs.timesheettable.recalculateWeeklySummary();
                    }

                    var duration = vts.$refs.timesheettable.totalDay;
                    var res = formatTime(duration);
                    return res;
                }
                return '';

              

            },
                       
            generateDays: async function (d, records) {
                var week = [];

                var items = records || vts.tableDataSource;

                $('.dataTables_wrapper').hide();
                $('.row.seven-cols').hide();
                for (var j = 0; j < getWeekDaysCount(); j++) {

                    var m = moment(d).add(j, 'd');

                    var date = m.format(momentDateFormat);

                    /*group by day */
                    var dayRecords = $.grep(items, function (v, i) {
                        v.position = getGuid(4);

                        return v.date == date && v.recordType == 0;
                    });

                    /* Keep original order if there are nested records */
                    var hasParentRecords = dayRecords.some(function (r) { return r.isParent; });
                    dayRecords = this.orderBy(dayRecords, hasParentRecords === true ? 'index' : 'startTime');

                    var lunch = '';
                    var pause = '';
                    var enteredDuration = '';
                    var isRows = false;

                    if (sheetId > 0) {
                        var isMaster = $.grep(dayRecords,
                            function (v, i) {
                                return v.rowType == 1;
                            });

                        if (isMaster && isMaster.length > 0) {
                            $.each(isMaster,
                                function (i, v) {
                                    if (i === 0) {
                                        if (v.lunchDuration > 0) {
                                            lunch = v.lunchDuration;
                                        }

                                        if (v.pauseDuration > 0) {
                                            pause = v.pauseDuration;
                                        }

                                        enteredDuration = v.enteredDuration;
                                    }
                                });
                        } else
                            isRows = dayRecords && dayRecords.length > 0;
                    } else {
                        lunch = orgLunch;
                        pause = orgDailyBreakMin;
                    }

                    var dayProjRecords = $.grep(items, function (v, i) {
                        return v.date == date && v.recordType == 1;
                    });

                    dayProjRecords = this.orderBy(dayProjRecords, 'index');

                    var day = {
                        ind: j,
                        name: m.format('dddd'),
                        date: date,
                        num: m.format('DD'),
                        records: dayRecords,
                        lunch: lunch,
                        pause: pause,
                        rowsChecked: isRows,
                        enteredDuration: enteredDuration,
                        total: 0

                    };

                    if ((!timesheetType || sheetId == 0) && dayRecords.length < blanklines) {

                        var holiday = null;
                        await $.ajax({
                            type: "GET",
                            contentType: "application/json; charset=utf-8",
                            url: '../../API/Service.svc/GetHolidayIfExists?date=' + m.format('YYYY-MM-DDTHH:mm:ss'),
                            dataType: "json",
                            success: function (res) {
                                if (res.success) {
                                    holiday = $.parseJSON(res.data);
                                }
                            },
                            error: function (a, b, c) {
                                console.error(a.responseText);
                            }
                        });

                        if (holiday == null) {
                            for (var i = dayRecords.length; i < blanklines; i++) {
                                var rAdded = vts.$refs.timesheettable.addLine(i, null, date, null, null, m.day());
                                day.records.splice(i, 0, rAdded);
                            }
                        } else {
                            
                            let start = holiday.startDate.slice(11, 16);
                            let end = holiday.endDate.slice(11, 16);
                            
                            var rAdded = vts.$refs.timesheettable
                                .addLine(i, null, date, start, end, m.day(), null, null, null, null,
                                    $.grep(scheduleRecList, function (timeCategory) {
                                        return timeCategory.timeType == TimeCategoryType.Holiday 
                                    })[0]);
                            day.records.splice(i, 0, rAdded);
                            
                        }
                    }
                    
                    week.push(day);
                }

                this.$set(this.week, 'days', week);
                var ind = 0;
                if (records) {
                    ind = this.week.selectedInd;

                    let dtc = getDefaultTimeCategory();
                    records.forEach(record => {
                        if (!record.time || record.time === dtc.timeName || resources[record.time] === dtc.timeName) {
                            record.time = dtc.timeName;
                            record.timeId = dtc.timeId;
                            record.timeName = dtc.timeName;
                        }
                    });
                }
                this.onDayChanged(ind);
                $('.dataTables_wrapper').show();
                $('.row.seven-cols').show();

            },

            onDayChanged(ind) {
                
                var day = this.week.days[ind];

                Vue.set(day, 'selected', true);

                this.week.selectedInd = ind;

                vts.$refs.timesheettable.setRecords(this.week);
            },

            getAllRecords() {
                var records = [];

                if (timesheetType) {

                    records = $.merge(records, vts.$refs.timesheettable.getAllRecords());


                }
                else {
                    $.each(vts.$data.week.days, function (i, v) {

                        var isMaster = false;
                        var enteredDuration = timeToDuration((v.enteredDuration || 0), DurationVal.Hours);

                        if (!v.records || v.records.length == 0) {
                            var item = getRecord(v.date, null, '', '', 1, v.lunch, v.enteredDuration);
                            v.records = [item];
                        }

                        $.each(v.records, function (j, o) {

                            if (o.rowType == 1) {
                                o.lunchDuration = v.lunch;
                                isMaster = true;
                                if (enteredDuration > 0)
                                    o.duration = enteredDuration;

                            } else
                                o.rowType = 0;

                        });
                        if (!isMaster && v.records && v.records.length > 0) {
                            v.records[0].lunchDuration = v.lunch;
                            v.records[0].rowType = 1;

                            if (enteredDuration > 0) {
                                v.records[0].duration = enteredDuration;
                            }
                        }
                        var rec = $.grep(v.records, function (r, j) {
                            return r.duration != 0 || r.rowType == 1;
                        });

                       
                        $.merge(records, rec);
                    });

                }
                
                return records;
            },

            onActiveContractChanged() {
                if (vts.$refs.timesheettable.recalculateWeeklySummary) {
                    vts.$refs.timesheettable.recalculateWeeklySummary();
                }
            }
        }
    });

    mainVueComponentReference = vts;
    
    function getDateOfISOWeek(w, y) {
        var simple = new Date(y, 0, 1 + (w - 1) * 7);
        var dow = simple.getDay();
        var ISOweekStart = simple;
        if (dow <= 4)
            ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
        else
            ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
        return ISOweekStart;
    }
///refreshWeekPreview
    function refreshWeekPreview()
    {
        var wd = getDateOfISOWeek($('#ContentPlaceHolder1_tbxWeek').val(), $('#ContentPlaceHolder1_tbxYear').val());

        var wdtext = wd.toLocaleDateString();

        wdtext = wd.toLocaleString(lang4, { weekday: 'long' }) + " " + wdtext;

        $('#lblWeekPreview').text(wdtext);
            vts.$refs.timesheettable.generateDays(wd);

        
            vts.generateDays(wd);
    }

    function getWeekDaysCount() {
        var daysCount = 7;

        switch (timesheetType) {
            // DetailedFullWeek template - full week
            case 1:
                daysCount = 7;
                break;
            // DetailedWorkDays template - working days only
            case 2:
                daysCount = 5;
                break;
            // SimplifiedEntry template - Monday - Saturday
            case 3:
                daysCount = 6;
                break;
            default: break;
        }

        return daysCount;
    }

    $('#ContentPlaceHolder1_tbxYear').change(refreshWeekPreview);
    $('#ContentPlaceHolder1_tbxWeek').change(refreshWeekPreview);
    refreshWeekPreview();
    
    // save button
    // ------------------------------------------------------------------------------------------------------------------------
    $('.btnSave').click(function () {

        var buttonCall = $(this);
        var action = buttonCall.data("action");
        
        if (buttonCall.hasClass('disabled')){
            buttonCall.prop('disabled', true); 
            return;
        }

        flagChanges(false);
        
        $(buttonCall).attr("disabled", true);

        var records = vts.getAllRecords();
        var validation = true;

        for (var i = 0; i < records.length; i++) {
            if (records[i].id == null)
                records[i].id = 0;

            if (records[i].index == null)
                records[i].index = 0;

            if (records[i].duration == '' || records[i].duration == '00:00')
                records[i].duration = 0;

            if (!records[i].lunchDuration || records[i].lunchDuration == '')
                records[i].lunchDuration = 0;

            if (!records[i].pauseDuration || records[i].pauseDuration == '')
                records[i].pauseDuration = 0;

            if (records[i].rate == '')
                records[i].rate = 0;


            if (records[i].recordType == 0 && records[i].startTime != records[i].endTime) {

                if (!records[i].startTime || !records[i].endTime) {
                    validation = false;
                    $(buttonCall).attr("disabled", false);
                    break;
                }

                var st = timeToMoment(records[i].startTime);
                var et = timeToMoment(records[i].endTime);

                if (st.isValid() && et.isValid()) {
                    if (st.isSameOrAfter(et)) {
                        validation = false;
                        $(buttonCall).attr("disabled", false);
                        break;
                    }
                }
                else {
                    validation = false;
                    $(buttonCall).attr("disabled", false);
                    break;
                }
            }
            
            records[i].visit = convertToItemId(records[i].visit, 'visit');
            records[i].time = convertToItemId(records[i].time, 'time');

        }

        if (!validation) {
            swal({
                title: resources['Validation_Error'],
                text: resources['ERR_StartDate_EndDate'],
                type: "error",
                showCancelButton: false,
                confirmButtonText: resources['Close']
            });
            return;
        }

        if (sheetId == null)
            sheetId = -1;

        var projectUpdate = {
            culture: lang,
            sheetId: sheetId,
            action: action,
            items: records,
            employee: parseInt($('#ContentPlaceHolder1_ddlEmployee').val()),
            year: parseInt($('#ContentPlaceHolder1_tbxYear').val()),
            week: parseInt($('#ContentPlaceHolder1_tbxWeek').val()),
            comment: $('#ContentPlaceHolder1_tbxComment').val()
        };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '../../API/Service.svc/UpdateTimeSheet',
            data: JSON.stringify(projectUpdate),
            processData: false,
            dataType: "json",
            success: function (response) {
                
                if (action === "save" && response.success === false) { 
                    swal({
                        title: resources['Validation_Error'],
                        text: response.message,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonText: resources['Close']
                    });
                    
                    return;
                }
                                
                sheetId = response.id;
                var firstDay = getDateOfISOWeek($('#ContentPlaceHolder1_tbxWeek').val(), $('#ContentPlaceHolder1_tbxYear').val());
                vts.generateDays(firstDay, response.items);
                
                $(buttonCall).attr("disabled", false);

                $('#ContentPlaceHolder1_lblLastChange').html(response.lastUpdateStatus);
                $('#ContentPlaceHolder1_lblValidationStatus').html(response.validationStatus);

                if (action == "save") {
                    $('#ContentPlaceHolder1_lblLastChange').addClass("shake");
                    $('#ContentPlaceHolder1_lblLastChange').addClass("animated");
                }

                else if (action == "validate" || action == "unvalidate") {
                    $('#ContentPlaceHolder1_lblValidationStatus').addClass("shake");
                    $('#ContentPlaceHolder1_lblValidationStatus').addClass("animated");
                }

                sheetId = response.id;
                
                if (action == "validate")
                {
                    swal({
                        title: resources['TimeSheetValidated'],
                        text: resources['DoYouWantToGoBackList'],
                        type: "success",
                        showCancelButton: true,
                        cancelButtonText: resources['NoStayHere'],
                        confirmButtonColor: "#4CAF50",
                        confirmButtonText: resources['YesBackToList']
                    })
                    
                    .then((value) => {
                        
                        
                        if (value.value != null && value.value == true) {
                            window.location = '.';
                        }
                            
                    });
                }
                else if (action === "unvalidate"){
                    let $saveChangesBtn = $(".btnSave.btnSaveChanges");
                    $saveChangesBtn.attr("disabled", false);
                    $saveChangesBtn.removeClass("disabled");
                }

                if (response.items && response.items.length > 0) {
                    $("#ContentPlaceHolder1_tbxYear").attr("disabled", true);
                    $("#ContentPlaceHolder1_tbxWeek").attr("disabled", true);
                }

            },
            error: function (a, b, c) {
                $(buttonCall).attr("disabled", false);

                if (a && a.status == 401) {
                    window.location.href = '~/Login.aspx';
                }
            }
        });

    });


    function convertToItemId(name, type) {
        var ret = null;
        for (var i = 0; i < itemsList.length; i++) {

            if (type == 'visit') {
                if (itemsList[i].visitName == name) {
                    return itemsList[i].visitId;
                }
            }
            else if (type == 'job') {
                if (itemsList[i].jobName == name) {
                    return itemsList[i].jobId;
                }
            }
            else if (type == 'service') {
                if (itemsList[i].serviceName == name) {
                    return itemsList[i].serviceId;
                }
            }
            else if (type == 'time') {
                if (itemsList[i].timeName == name) {
                    return itemsList[i].timeId;
                }
            }

            
        }
        return ret;
    }

    function convertItemsToName(dataset) {

        for (var r = 0; r < dataset.length; r++) {
            var type = dataset[r].type;
            var id = dataset[r].item;

            for (var i = 0; i < itemsList.length; i++) {
                if (itemsList[i].type == type && itemsList[i].id == id) {
                    dataset[r].item = itemsList[i].name;
                    break;
                }
            }

            dataset[r].totalDuration = precisionRound(dataset[r].duration * dataset[r].rate / 100, 2);
        }
        for (var i = 0; i < blanklines; i++) {
            dataset.push({});
        }
    }

    // ------------------------------------------------------------------------------------------------------------------------>
    

    // btn delete
    // ------------------------------------------------------------------------------------------------------------------------

    function deleteSheet()
    {
        flagChanges(false);

        swal({
            title: resources['Confirmation'],
            text: resources['SureToDeleteSheet'],
            type: "warning",
            showCancelButton: true,
            cancelButtonText: resources['No_Cancel'],
            confirmButtonColor: "#F44336",
            confirmButtonText: resources['Yes_Delete']
        })

            .then((value) => {


      
                if (value.value != null && value.value == true)
                {

                    var projectUpdate = {
                        culture: lang,
                        sheetId: sheetId,
                        action: "delete",
                        items: [],
                        employee: -1,
                        year: -1,
                        week: -1,
                        comment: ''
                    };


                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: '../../API/Service.svc/UpdateTimeSheet',
                        data: JSON.stringify(projectUpdate),
                        processData: false,
                        dataType: "json",
                        success: function (response) {
                            window.location = '.';
                        }
                    });
                }
            });
    }


    $('.btnDelete').click(function (e) {
        deleteSheet();
    });

    // ------------------------------------------------------------------------------------------------------------------------>

    function onHeaderChange(e) {
        var year = parseInt($('#ContentPlaceHolder1_tbxYear').val());
        var week = parseInt($('#ContentPlaceHolder1_tbxWeek').val())
        var employee = parseInt($('#ContentPlaceHolder1_ddlEmployee').val());

        var res = $.grep(existingTimesheets, function(v, i) {
            return v.Year == year && v.Week == week && v.Employee == employee && (v.TimeSheetId < 1 || v.TimeSheetId != sheetId);
        });

        if (res && res.length > 0) {

            $("#alertErrorTimesheetExists").show();
            $("#alertErrorTimesheetExistsLink").attr("href", "./Sheet.aspx?id=" + res[0].TimeSheetId);

            $('.btnSave').attr("disabled", true);
            
        } else {

            $("#alertErrorTimesheetExists").hide();
            $('.btnSave').removeAttr("disabled");

            flagChanges(true);

            var query = {
                year: year,
                week: week,
                employee: employee,
                culture: lang
            }

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '../../API/Service.svc/GetTimeSheetSelectableTimeItems',
                data: JSON.stringify(query),
                processData: false,
                dataType: "json",
                success: function (response) {

                    itemsList = response;

                    var calrec = $.grep(itemsList,
                        function (v, i) {
                            return v.calendarDays && v.calendarDays.length > 0;
                        })

                    calendarDays = calrec && calrec.length > 0 ? calrec[0].calendarDays : [];


                },
                error: function (a, b, c) {
                    //alert(a.responseText);
                }
            });
        }
      

    }


    $('#ContentPlaceHolder1_ddlEmployee').change(onHeaderChange);
    $('#ContentPlaceHolder1_tbxYear').change(onHeaderChange);
    $('#ContentPlaceHolder1_tbxWeek').change(onHeaderChange);

    // add line button
    // ------------------------------------------------------------------------------------------------------------------------
    $('#btnAddLine').click(function () {
        flagChanges(true);
        tableDataSource.push({});
    });
    // ------------------------------------------------------------------------------------------------------------------------>

    convertItemsToName(tableDataSource);
    
  
    $('#ContentPlaceHolder1_tbxComment').keyup(function (e) {
        $('.btnSaveChanges').prop('disabled', false);
    });

    function precisionRound(number, precision) {
        var factor = Math.pow(10, precision);
        var ret = Math.round(number * factor) / factor;
        var res = ret.toFixed(precision);
        res = parseFloat(res);
        return res;
    }

    var firstCalc = true;

    function init() {

        missingItems = [];

        for (var i = 0; i < itemsList.length; i++) {
            if (itemsList[i].date != null && itemsList[i].date != "*") {

                var isFilled = false;
                for (var j = 0; j < tableDataSource.length; j++) {

                    if (tableDataSource[j].item == itemsList[i].name && tableDataSource[j].date == itemsList[i].date) {
                        isFilled = true;
                        break;
                    }

                }

                if (isFilled == false) {

                    
                    var datarow = {
                        date: itemsList[i].date,                    
                        startTime: itemsList[i].scheduledStart,
                        endTime: itemsList[i].scheduledEnd,
                        visit: itemsList[i].visitName,
                        job: itemsList[i].jobId,
                        serviceId: itemsList[i].serviceName,
                        description: itemsList[i].description
                    };
                    
                    missingItems.push(datarow);
                }

               

            }
        }

        $('#lblMissingCount').html(missingItems.length + "");

        if (missingItems.length > 0){
            

            if (firstCalc) {
                firstCalc = false;

                swal({
                    title: missingItems.length + " " + resources['MissingVisits'],
                    text: resources['AddRelatedRows'],
                    type: "info",
                    showCancelButton: true,
                    cancelButtonText: resources['NoLater'],
                    confirmButtonColor: "#2196F3",
                    confirmButtonText: resources['YesAdd']
                })
                .then((value) => {
                    
                    if (value.value != null && value.value == true) {
                        addMissingVisits();
                    }
                    else {
                        $('#alertMissing').slideDown();
                    }
                });

                    
            }
            else {
                $('#alertMissing').slideDown();
            }
        }
        else {
            $('#alertMissing').slideUp();
        }

        onHeaderChange();

    }

    function addMissingVisits() {

        for (var i = 0; i < missingItems.length; i++) {
            var item = missingItems[i];

            var projectId = item.job;
            var serviceId = convertToItemId(item.service, 'service');
            var desc = item.description;

            var m = moment(item.date, momentDateFormat);
            var day = m.day();
            
            vts.$refs.timesheettable.addLine(null, null, item.date, item.startTime, item.endTime, day, item.visit, projectId, serviceId, desc);

        }
      
        $('.btnSaveChanges').prop('disabled', false);
        $('#alertMissing').slideUp();

    }

    $('#btnAddMissing').click(function(e) {
        addMissingVisits();
    });
    
    init();
});



