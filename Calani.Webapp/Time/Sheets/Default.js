﻿var _periodFilterFrom = null;
var _periodFilterTo = null;
var _statusFilter = "tovalidate";

var _dataTable = null;

function checkField(val, str){
    return val?.toString().toLowerCase().includes(str.toLowerCase());
}

function containsString (obj, str) {
    if (checkField(obj.EmployeeName, str)) return true;
    if (checkField(obj.EmployeeInitials, str)) return true;
    if (checkField(obj.Year, str)) return true;
    if (checkField(obj.Week, str)) return true;
    if (checkField(obj.Range?.EndDateStr?.split(' ')[0], str)) return true;
    if (checkField(obj.Range?.StartDateStr?.split(' ')[0], str)) return true;
    if (checkField(obj.Status.Status, str)) return true;
    if (checkField(obj.Duration?.TotalDurationStr, str)) return true;
    if (checkField(obj.Duration?.AttendedDurationStr, str)) return true;

    return false;
}

function setPeriodFilter() {

    _periodFilterFrom = null;
    _periodFilterTo = null;

    var pval = $('#ddlPeriod').val();
    if (pval != null && pval.indexOf(';') > 0) {
        pval = pval.split(';');
        if (pval.length == 2) {
            _periodFilterFrom = pval[0];
            _periodFilterTo = pval[1];

            if (_periodFilterFrom != "*") _periodFilterFrom = parseInt(_periodFilterFrom);
            else _periodFilterFrom = null;

            if (_periodFilterTo != "*") _periodFilterTo = parseInt(_periodFilterTo);
            else _periodFilterTo = null;
        }
    }

    _dataTable.draw();
}

var groupElement = $(
    '<div class="dataTables_length pull-right  ml-20 pl-20">' +
    '<label class="col-form-label">' + txtGrouping +
    '<select id="ddlGroupingGrid" class="ml-10 width-200">' +
    '<option value="0">--</option>' +
    '<option value="EmployeeName">' + txtEmploee+'</option>' +
    '<option value="RangeStr">' + txtDate+'</option>' +
    '</label ></select></div>');

groupElement.on('change', function (e) {
    var group = e.target.value;
    setGrouping(group);    
});

function setGrouping(group) {

    var map = {
        "EmployeeName": 1,
        "RangeStr": 5,
    };

    if (group != "0") {
        _dataTable.order.fixed({
            pre: [map[group], 'desc']
        });

        _dataTable.rowGroup().dataSrc(group);
        _dataTable.rowGroup().enable().draw();
    }
    else {
        _dataTable.order.fixed({});
        _dataTable.rowGroup().disable().draw();
        _dataTable.order([5, 'desc']).draw();
    }

    _dataTable.draw();
}

function appendGroupingList() {
    $(groupElement).appendTo("#timesheets-datatable_filter");    
}

function refreshLiveStats() {

    let countCurrentWeek = 0;
    let countToValidate = 0;
    let countValidated = 0;

    let rowsId = _dataTable.rows().eq(0);

    for (let i = 0; i < rowsId.length; i++) {

        let row = _dataTable.row(rowsId[i]);
        let data = row.data();

        let lastUpdate = parseInt(data["SheetDate"]);

        if (_periodFilterFrom != null) {
            if (lastUpdate < _periodFilterFrom)
                continue;
        }
        if (_periodFilterTo != null) {
            if (lastUpdate >= _periodFilterTo) 
                continue;
        }
        let rowStatus = data["Status"]["Status"];

        if (rowStatus != null) {
            let matchesSearch = true;
            let searchText = $('.dataTables_filter input').val();
            if (searchText) matchesSearch = containsString(data, searchText);

            switch (rowStatus) {
                case "tovalidate":
                    if (matchesSearch) {
                        countToValidate++;
                    }
                    break;
                case "validated":
                    if (matchesSearch) {
                        countValidated++;
                    }
                    break;
                case "currentweek":
                    if (matchesSearch) {
                        countCurrentWeek++;
                    }
                    break;
            }
        }
    }
    
    $('#ContentPlaceHolder1_lblCountCurrentWeek').html(countCurrentWeek);
    $('#ContentPlaceHolder1_lblCountToValidate').html(countToValidate);
    $('#ContentPlaceHolder1_lblCountValidated').html(countValidated);
}
function filterItem(settings, data, dataIndex) {

    var visible = true;

    var lastUpdate = parseInt(settings.aoData[dataIndex]._aData["SheetDate"]);
    if (_periodFilterFrom != null) {
        if (lastUpdate < _periodFilterFrom) visible = false;
    }
    if (_periodFilterTo != null) {
        if (lastUpdate >= _periodFilterTo) visible = false;
    }

    settings.aoData[dataIndex].nTr.setAttribute('data-anystatusFilter', visible);

    if (_statusFilter != null) {
        if (settings.aoData[dataIndex]._aData["Status"]["Status"] != _statusFilter)
            visible = false;
    }

    return visible;
}

$(function () {

    // client side filters
    $.fn.dataTable.ext.search.push(filterItem);

    _dataTable = initTimeSheetsDataTable($('#timesheets-datatable'), getDataUrl());
    _dataTable.search('');
    _dataTable.on('draw', function () {
        appendGroupingList();
        refreshLiveStats();

        $('[data-toggle="popover"]').popover();
    });

    setPeriodFilter();

    setTimeout(function() {
        $(groupElement).val(0).change();
        toggleValidateMultipleVisibility(isAdmin && (_statusFilter == 'tovalidate' || _statusFilter == 'currentweek'));
    }, 200);

});


function GetTimeSheetCommentsWidget(timeSheetId){
    $.ajax({
        type: "GET",
        url: '../../API/Service.svc/GetTimeSheetComments?timeSheetId=' + timeSheetId + '&comment=' + STRING_RESOURCES['Comment'],
        dataType: 'json',
        success: function (response) {
            console.log(response);
            let html = '<div style="padding: 10px 0px 0 10px; font-size: 18px;color: #333333;text-align: left;">';
            response.forEach((x) => html +=`
                <div class="comemnt-widget row mb-10 mr-5 well" style="padding: 20px 15px 20px 20px;"> 
                    <span style="font-weight: bold;">${x.m_Item1}:</span> 
                    <span>${x.m_Item2}</span> 
                </div>`);
            html += "</div>";
            swal({
                html: html,
                type: "info"
            })
        },
        error: function () {
            alert(STRING_RESOURCES['Error']);
        }
    });
    
}
function initTimeSheetsDataTable(dt, dataUrl) {

    let options = getDataTableOptions(dt, false, true, true, null, "TotalDuration");
    options.ajax = dataUrl;
    options.columns = [
        {
            "orderable": false,
            "className": 'select-checkbox',
            "targets": 0,
            "defaultContent": '',
        },
        {
            "data": "Id",
            "render": function (id) {
                return "<label class=\"col-md-5 pull-left\"><i class=\"icon-warning icon\" data-container=\"body\" data-placement=\"top\" data-toggle=\"popover\" data-title=\"" + STRING_RESOURCES["Warning"] + "\" data-trigger=\"hover\" data-content=\"" + STRING_RESOURCES["TimeSheetWarn_ToolTip"] + "\"></i></label>" +
                    "<a href=\"Sheet.aspx?id=" + id + "\">" + STRING_RESOURCES["Open"] + "</a>";
            }
        },
        { "data": "EmployeeName" },
        {
            "data": "EmployeeInitials",
            "render": function (employeeInitials) {
                return "<span class=\"label label-flat border-grey text-grey-600\">" + employeeInitials + "</span>";
            }
        },
        { "data": "Year" },
        { "data": "Week" },
        {
            "data": "Range",
            "type": "dateRange",
            "render": function (range) {
                return range ? (range.StartDateStr + ' - ' + range.EndDateStr) : "";
            }
        },
        { 
            "data": "CommentNumber" ,
            "render": function (data, type, row) {
                let html = '';

                if (data === 1) {
                    html = `<button type="button" style="padding: 0; border:none" onclick="GetTimeSheetCommentsWidget(${row['Id']})"> <span class="label border-left-info label-striped text-info">` +
                        '<i class="icon-comment"></i>&nbsp;' + data + '</span></button>';
                }
                
                if (data > 1) {
                    html = `<button type="button" style="padding: 0; border:none" onclick="GetTimeSheetCommentsWidget(${row['Id']})"> <span class="label border-left-info label-striped text-info">` +
                        '<i class="icon-comment-discussion"></i>&nbsp;' + data + '</span></button>';
                }

                return html;
            }
        },
        {
            "data": "Status",
            "render": function (status) {
                var html = '';

                if (status.AcceptedDate != null) {
                    html = '<span class="label border-left-success label-striped text-success">' +
                        '<i class="icon-checkmark"></i>&nbsp;' + STRING_RESOURCES["Validated"] + ' ' + displayDate(status.AcceptedDate, 'dd/mm/yyyy');
                }
                else if (status.RejectedDate != null) {
                    html = '<span class="label border-left-danger label-striped text-danger">' +
                        '<i class="icon-cross2"></i>&nbsp;' + STRING_RESOURCES["Rejected"] + ' ' + displayDate(status.RejectedDate, 'dd/mm/yyyy');
                }
                else if (status.Status == 'currentweek') {
                    html = '<span class="label border-left-grey label-striped text-grey">' +
                        '<i class="icon-pencil3"></i>&nbsp;' + STRING_RESOURCES["CurrentWeek"];
                }
                else {
                    html = '<span class="label border-left-primary label-striped text-primary">' +
                        '<i class="icon-question6"></i>' + STRING_RESOURCES["ToValidate"];
                }

                return html;
            }
        },
        {
            "data": "Duration",
            "render": function (duration) {
                var cssClass = duration.DisplayWarning ? "text-danger" : "";
                var text = duration.TotalDurationStr;
                if (duration.AttendedDurationStr != '') {
                    text += " / " + duration.AttendedDurationStr;
                }

                return '<span class="' + cssClass + '">' + text + '</span>';
            }
        },
    ];

    if (timesheetTemplateType == 3) {
        // Do not display warning if simplified timesheet template configured
        var idCol = $.grep(options.columns, function (col) { return col.data == "Id" })[0];
        idCol.render = function (id) {
            return "<a href=\"Sheet.aspx?id=" + id + "\">" + STRING_RESOURCES["Open"] + "</a>";
        };
    }

    options.select = {
        "style": 'multi',
        selector: 'td:first-child'
    };

    $.fn.dataTable.ext.type.order['dateRange-pre'] = function (range) {
        // Extract and parse start date from string representation of range
        var startDate = range.split(' - ')[0];
        return moment(startDate, CALANI.DEFAULTS.dateFormat).toDate();
    };

    options.orderFixed = [1, 'asc'];

    options.rowGroup = {
        dataSrc: 0,

        startRender: function (rows, group) {
            return group;
        },

        endRender: function (rows, group) {

            var sum = rows
                .data()
                .pluck("Duration")
                .reduce(function (a, b) {
                    return a + b['TotalDuration'];
                }, 0);

            sum = Math.round(sum * 100) / 100;

            return $('<tr/>')
                .append('<td colspan="7" class="text-right">' + txtTotalFor + ' ' + group + ' :</td>')
                .append('<td class="text-right">' + sum + '</td>');
        }
    };

    options.footerCallback = function (tfoot, data, start, end, display) {
        
        var api = this.api();
        var rows = api.rows({ search: 'applied' });
        var total = rows.data()
            .reduce(function (a, b, ind, d) {
                var result = Math.round((a + b["Duration"]["TotalDuration"]) * 100) / 100;
                return result;
            }, 0);

        $(tfoot).html('<td colspan="7" class="text-right text-black">' + txtTotalForPeriod + '</td>' +
            '<td class="text-right "><span class="text-black">' + total + '</span></td>');
    };

    return dt.DataTable(options);
}

function periodChanged() {
    _dataTable.destroy();
    _dataTable = initTimeSheetsDataTable($('#timesheets-datatable'), getDataUrl());

    setTimeout(function () {
        $("#ddlGroupingGrid").val("0").change();
    }, 100);
}

function getDataUrl() {
    let dates = getPeriodFilterDates();
    let employeeId = _employeeId != undefined && _employeeId > 0 ? _employeeId : 0;
    let url = "../../API/Service.svc/ListTimeSheets?startTicks=" + dates[0] + "&endTicks=" + dates[1] + "&employeeId=" + employeeId;
    return url;
}

function postValidateMultipleTimesheets(ids) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../API/Service.svc/ValidateMultipleTimesheets',
        data: JSON.stringify(ids),
        processData: false,
        dataType: "json",
        success: function (response) {
            
            var selectedRows = _dataTable.rows({ selected: true });

            for (var i in response) {
                if (response[i].success) {
                    var timesheetId = response[i].id;
                    var rowIdx = -1;
                    selectedRows.rows(function (idx, data, node) {
                        if (data.Id === timesheetId) {
                            rowIdx = idx;
                        }
                        return false;
                    });

                    if (rowIdx > -1) {
                        var newData = _dataTable.row(rowIdx).data();
                        _dataTable.row(rowIdx).deselect();
                        newData.Status.Status = "validated";
                        _dataTable.row(rowIdx).data(newData).draw();
                        console.log("Timesheet #" + timesheetId + " was moved to validated.");
                    }
                }
            }
        },
        error: function (a, b, c) {
            console.error(a);
        }
    });
}

function validateSelectedTimesheets() {
    var selectedTimesheets = _dataTable.rows({ selected: true }).data();
    console.log(selectedTimesheets);
    var selectedTimesheetsIds = $.makeArray($.map(selectedTimesheets, function (timesheet) { return timesheet.Id; }));
    postValidateMultipleTimesheets(selectedTimesheetsIds);
    return false;
}

function toggleValidateMultipleVisibility(show) {
    var selectRowColumn = _dataTable.column(0);
    selectRowColumn.visible(show);

    var button = $('#validate-selected-timesheets-button');
    show ? button.show() : button.hide();
}

new Vue({

    el: '#app',

    data: {
        statusFilter: 'tovalidate'
    },
    methods: {
        setStatusFilter: function (e, f) {
            this.statusFilter = f;
            _statusFilter = f;
            _dataTable.draw();
            toggleValidateMultipleVisibility(isAdmin && (_statusFilter == 'tovalidate' || _statusFilter == 'currentweek'));
            e && e.preventDefault();
        }
    }
});