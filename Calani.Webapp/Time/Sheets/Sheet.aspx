﻿<%@ Page Title="TimeSheet" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Time_Sheets_Sheet" Codebehind="Sheet.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/assets/js/plugins/forms/selects/chosen.jquery.min.js") %>"></script>
   <link href="<%= Page.ResolveClientUrl("~/assets/css/chosen.min.css") %>" rel="stylesheet" type="text/css">
   <style type="text/css">
       .vs-style .vs__search::placeholder,
       .vs-style .vs__dropdown-toggle,
       .vs-style .vs__dropdown-menu {
           background: #ffffff;
       }
       
       .vs__open-indicator{
           display: none;
       }

  
</style>
</asp:Content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
    <div class="btn-group" runat="server" id="quickmenu">
		<button type="button" class="btn bg-gesmobile btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
		</button>

		<ul class="dropdown-menu dropdown-menu-right">
            <li runat="server" id="liQmPdf"><a id="liQmPdfLink" runat="server" target="_blank"><i class="icon-file-pdf"></i> <%= Resources.Resource.Download %> (PDF)</a></li>
		</ul>
	</div>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
    <div id="vueTimeSheet" class="content">
        <script type="text/javascript">
            function flagChanges(changes) {
                if (changes) {
                    window.onbeforeunload = function () {
                        return "<%= Resources.Resource.LeaveWithoutSaveChanges %>";
                    };
                }
                else {
                    window.onbeforeunload = null;
                }
            }
        </script>

        <div class="row">
            <div class="panel panel-white">
                <div class="panel-body pb-5">
                    <div class="form-group no-padding no-margin text-center">
                        <label class="control-label "><%= Resources.Resource.CurrentWeek %>&nbsp;:&nbsp;<span class="text-bold" id="lblWeekPreview" /></label>
                    </div>
                     <div class="form-horizontal pr-15">
							<fieldset class="content-group  pt-15">
                                <div class="form-group ">
									<label class="control-label col-lg-1 text-right"><strong><%= Resources.Resource.Employee %></strong></label>													
									<div class="col-lg-2">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-user"></i></span>
											<asp:DropDownList ID="ddlEmployee" runat="server" CssClass="form-control" 
                                            DataTextField="name" DataValueField="id" onchange="requestActiveContract(event.target.value)" />
                                            <%--onchange="requestActiveContract(event.target.value)"--%>
										</div>
									</div>

									<label class="control-label col-lg-1 text-right"><strong><%= Resources.Resource.Year %></strong></label>													
									<div class="col-lg-2">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-calendar2"></i></span>
                                            <input type="number" step="1" runat="server" value="" class="form-control"  id="tbxYear"/>
										</div>
									</div>

									<label class="control-label col-lg-1 text-right "><strong><%= Resources.Resource.Week %></strong></label>													
									<div class="col-lg-2">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-calendar2"></i></span>
                                            <input type="number" step="1" min = "1" max="53" runat="server" value="" class="form-control"   id="tbxWeek"/>
										</div>
									</div>
                                    
                                    <label class="control-label col-lg-1 text-right"><strong><%= Resources.Resource.Total %></strong></label>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-hour-glass2"></i></span>
                                            <input type="text" id="total-week-hours" :value="getTotalWeekHours()" class="form-control " readonly="readonly"/>
                                        </div>
                                    </div>

                                </div>

                            </fieldset>
                         <fieldset>
                             <div class="col-lg-12" id="alertErrorTimesheetExists" style="display: none">
                                 <div class="alert alert-danger alert-styled-left">
                                     <span class="font-weight-semibold"><%= Resources.Resource.TimeSheetAlreadyExists %>!</span> <%= Resources.Resource.TimeSeetExistsAlert1 %> <a href="#" class="alert-link" id="alertErrorTimesheetExistsLink"><%= Resources.Resource.TimeSeetExistsAlert2 %></a>.
                                 </div>
                             </div>
                         </fieldset>
                    </div>
                </div>
               
            </div>
        </div>
		
        <div class="row" v-show="timesheetType == 0">
			<div class="panel panel-white">

                <div class="panel-body">
                    <div class="row seven-cols">
                        <div class="col-sm-1 p-5" v-for="(d, index) in week.days" v-on:click="onDayChanged(index)" >
                            <day-item v-bind:index="index" v-bind:details="d"  v-bind:selected="week.selectedInd == index"  />
                        </div>
                    </div>
                </div>

			</div>
        </div>
      
        <%--Timesheet 1, 2, 3--%>
        <div class="row" v-show="timesheetType == 1 || timesheetType == 2  || timesheetType == 3">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h5 class="panel-title"><%= Resources.Resource.DetailedTimetables %></h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li>
                                <a data-action="collapse" data-toggle="collapse" data-target="#panel_details" @click="function(e){e.target.classList.toggle('rotate-180')}"></a>
                            </li>
                        </ul>
                    </div>
                </div>
                
                <div  id="panel_details" class="panel-body collapse in" >
                    <div class="dataTables_wrapper">                      
                        <timesheet-table v-if="timesheetType == 0 || timesheetType == 1 || timesheetType == 2" ref="timesheettable" ></timesheet-table>
                        <timesheet-table-simplified v-if="timesheetType == 3" ref="timesheettable"></timesheet-table-simplified>
                    </div>
                    
                    <div class="alert alert-primary no-border" style="line-height:34px; display:none;" id="alertMissing">
                        <button type="button" id="btnAddMissing" style="float:right;" class="btn btn-default btn-xs"><i class="icon-arrow-down16 position-left"></i> 
                            <%= Resources.Resource.Add %>
                        </button>
						<span class="text-semibold"><span id="lblMissingCount">...</span> <%= Resources.Resource.MissingVisits %></span> 
                        <%= Resources.Resource.AddRelatedRows %>
					</div>

                    <div id="hot_ac_lazy" class="hidden"></div>

                </div>
             
               
            </div>
        </div>

        <div class="row hidden">
            <div class="panel panel-white">
                 <div class="panel-heading">
                     <h5 class="panel-title"><%= Resources.Resource.HoursActivity %>: <span>{{selectedDayStr}}</span></h5>
                     <div class="heading-elements">
								<ul class="icons-list">
			                		<li>
                                        <a data-action="collapse" data-toggle="collapse" data-target="#panel_projects" @click="function(e){e.target.classList.toggle('rotate-180')}"></a>
                                    </li>
			                		
			                	</ul>
		                	</div>
			        </div>
                <div id="panel_projects" class="panel-body collapse in">
                    <div class="dataTables_wrapper">
                      
                        <timesheet-services-table ref="timesheetservicestable"></timesheet-services-table>

                    </div>
                   
                </div><!-- panel-body -->
            </div>
        </div>
 
        <div class="row">
            <div class="panel panel-white">
			        <div class="panel-heading">
				        <h5 class="panel-title"><%= Resources.Resource.Comment %></h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li>
                                    <a data-action="collapse" data-toggle="collapse" data-target="#panel_coment" @click="function(e){e.target.classList.toggle('rotate-180')}"></a>
                                </li>
			                		
                            </ul>
                        </div>
			        </div>
			        <div id="panel_coment" class="panel-body collapse in">
				        <div class="row text-center">
											
					       
                            <textarea class="form-control" id="tbxComment" rows="6" runat="server"></textarea>

                         </div>
                     </div>
                </div>
        </div>
        <div class="col-sm-12 col-md-12">
	        <div class="panel panel-flat">
			        <div class="panel-heading">
				        <h5 class="panel-title"></h5>
			        </div>
			        <div class="panel-body">
				        <div class="row text-center">
											
					        <div class="col-xs-6">
							        <div class="panel panel-body border-top-primary text-center">

                                        <p class="content-group-sm text-size-small" id="lblLastChange" runat="server">... </p>
                                        <p class="content-group-sm text-size-small" id="lblValidationStatusEmp" runat="server">...</p>
                                        
                                        <button type="button" class="btn btn-primary btnSave btnSaveChanges <%= CanSaveValidated ? "" : "disabled" %>" data-action="save">
                                            <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.SaveChanges %>
								        </button>
							        </div>	
					        </div>
                            <div class="col-xs-6" runat="server" id="panelValidation">
                                
                                <div class="panel panel-body border-top-success text-center">
                                    
                                    <p class="content-group-sm text-size-small" id="lblValidationStatusAdm" runat="server">...</p>
                                    
                                    <div class="btn-group">
                                        <a href runat="server" class="btn btn-success btnSave" data-action="validate" >
                                            <i class="icon-user-check position-left"></i> <%= Resources.Resource.Validation %>
                                        </a>
                                                
									    <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore"><span class="caret"></span></button>
									    <ul class="dropdown-menu dropdown-menu-right" style="min-width:240px">
                                            <li><a href="javascript:void(0)" class="btnSave" data-action="validate"><i class="fa fa-check-circle-o"></i> <%= Resources.Resource.MarkSheetAsValidated %></a></li>
                                            <li><a href="javascript:void(0)" class="btnSave" data-action="unvalidate"><i class="fa fa-circle-o"></i> <%= Resources.Resource.MarkSheetAsNotValidated %></a></li>
                                            <li runat="server" id="liPdfSep" class="divider"></li>
                                            <li runat="server" id="liPdf"><a id="liPdfLink" runat="server" target="_blank"><i class="icon-file-pdf"></i> <%= Resources.Resource.Download %> (PDF)</a></li>
                                            <li class="divider"></li>
                                            <li><a href="javascript:void(0)" class="btnDelete"><i class="fa fa-trash"></i> <%= Resources.Resource.DeleteSheet %></a></li>
									    </ul>
								    </div>
                               </div>

                            </div>
                         </div>
                     </div>
                </div>
            </div>
        </div><!-- /content -->
    </form>
    <script type="text/javascript">

        var sheetId = <%= SheetId %>;
        var orgLunch = <%= OrgLunchMin %>;
        var orgDailyBreakMin = <%= OrgDailyBreakMin %>;
        var tableDataSource = <%= JsonRecords %>;
        var itemsList = <%= JsonList %>;
        var timesheetType = <%= TimeSheetType %>;
        var linesPerDay = <%= LinesPerDay %>;
        var existingTimesheets = <%= JsonOrgTimesheets %>;
        var activeContract = <%= ActiveContractJson %>;
        
        var useWorkSchedule = <%= UseWorkSchedule.ToString().ToLower() %>;

        var calrec = $.grep(itemsList,
            function (v, i) {
                return v.calendarDays && v.calendarDays.length > 0;
            })

        var calendarDays = calrec && calrec.length > 0 ? calrec[0].calendarDays : [];
        
        var projectsListTmp = $.grep(itemsList,
            function (v, i) {
                return v.jobId > 0 && !v.visitId;
            });

        var clients = {};
        for (var i = 0; i < projectsListTmp.length; i++) {

            var clientName = projectsListTmp[i].clientName;

            if (!clients[clientName]) {
                clients[clientName] = [];
            }
            clients[clientName].push(projectsListTmp[i]);
        }

        var projectsList = []
        for (var clientName in clients) {

            var name = clientName == null || clientName == 'null' ? "<%= Resources.Resource.None %>" : clientName;

            projectsList.push({ group: name, jobName: null, recordStatus: 0 })

            var projects = clients[clientName];

            for (var j = 0; j < projects.length; j++) {
                var proj = projects[j];
                proj.clientName = name;
                projectsList.push(proj);
            }
        }
        


        var internalList = $.grep(itemsList,
            function (v, i) {
                return v.serviceId > 0 && v.serviceInternal;
            });

        var billableList = $.grep(itemsList,
            function (v, i) {
                return v.serviceId > 0 && !v.serviceInternal;
            });  

        var servicesList = [];

        servicesList.push({ group: "<%= Resources.Resource.Services %>", serviceName: null, recordStatus:0 });

        for (var j = 0; j < billableList.length; j++) {
            var item = billableList[j];
            servicesList.push(item);
        }
        

        servicesList.push({ group: "<%= Resources.Resource.InternalJobs %>", serviceName: null, recordStatus: 0 });
                
        for (var j = 0; j < internalList.length; j++) {
            var item = internalList[j];
            servicesList.push(item);
        }

     
        var resources = {};
        resources['SureToDeleteLine'] = "<%= Resources.Resource.SureToDeleteLine %>";
        resources['Date'] = "<%= Resources.Resource.Date %>";
        resources['StartTime'] = "<%= Resources.Resource.StartTime %>";
        resources['EndTime'] = "<%= Resources.Resource.EndTime %>";
        resources['Rate'] = "<%= Resources.Resource.Rate %>";
        resources['Type'] = "<%= Resources.Resource.Type %>";
        resources['Description'] = "<%= Resources.Resource.Description %>";
        resources['Activity'] = "<%= Resources.Resource.HoursActivity %>";
        resources['Duration'] = "<%= Resources.Resource.Duration %>";
        resources['MissingVisits'] = "<%= Resources.Resource.MissingVisits %>";
        resources['AddRelatedRows'] = "<%= Resources.Resource.AddRelatedRows %>";
        resources['YesAdd'] = "<%= Resources.Resource.YesAdd %>";
        resources['NoLater'] = "<%= Resources.Resource.NoLater %>";
        resources['TimeSheetValidated'] = "<%= Resources.Resource.TimeSheetValidated %>";
        resources['DoYouWantToGoBackList'] = "<%= Resources.Resource.DoYouWantToGoBackList %>";
        resources['NoStayHere'] = "<%= Resources.Resource.NoStayHere %>";
        resources['YesBackToList'] = "<%= Resources.Resource.YesBackToList %>";
        resources['Confirmation'] = "<%= Resources.Resource.Confirmation %>";
        resources['SureToDeleteSheet'] = "<%= Resources.Resource.SureToDeleteSheet %>";
        resources['EnteredRecordsWillBeDeleted'] = "<%= Resources.Resource.EnteredRecordsWillBeDeleted %>";
        resources['Yes_Delete'] = "<%= Resources.Resource.Yes_Delete %>";
        resources['No_Cancel'] = "<%= Resources.Resource.No_Cancel %>";
        resources['Visit'] = "<%= Resources.Resource.Visit %>";
        resources['Project'] = "<%= Resources.Resource.Project %>";
        resources['Service'] = "<%= Resources.Resource.Service %>";
        resources['Services'] = "<%= Resources.Resource.Services %>";
        resources['InternalJobs'] = "<%= Resources.Resource.InternalJobs %>";
        resources['CategoryOfTime'] = "<%= Resources.Resource.CategoryOfTime %>";
        resources['Custom'] = "<%= Resources.Resource.Custom %>";
        resources['Auto'] = "<%= Resources.Resource.Auto %>";
        resources['Validation_Error'] = "<%= Resources.Resource.Validation_Error %>";
        resources['Close'] = "<%= Resources.Resource.Close %>";
        resources['ERR_StartDate_EndDate'] = "<%= Resources.Resource.ERR_StartDate_EndDate %>";

        var scheduleRecList = $.grep(itemsList,
            function (v, i) {
                if (v.timeType == 2)
                    v.timeName = resources['Custom'];

                if (v.timeType == 3)
                    v.timeName = resources['Auto'];
                // We don't want Custom (Manual) time category for simplified template (type=3)
                return timesheetType != 3 ? v.timeId > 0 : v.timeId > 0 && v.timeType != 2;
            });


        
        
        var minDate = <%= MinDate %>;
        var maxDate = <%= MaxDate %>;

        function duplicateRecords(dayIndex) {
            var optionSelected = $('input[name=radio-duplicate-'+ dayIndex +']:checked').val();
            mainVueComponentReference.duplicateRecords(dayIndex, optionSelected);
            $('#btnDuplicateLastLine-' + dayIndex).popover('hide');
        }
                   
    </script>

    <template id="day-item"> 
        <div class="panel cursor-pointer text-center bordered " v-bind:class="{ 'text-blue alpha-primary': selected == true }"> 
            <div class="panel-heading no-padding" >
                <h6 class="">{{details.name}}</h6>
            </div>
            <div class="panel-body no-padding " >
                <h1 class="">{{details.num}}</h1>
                
                <div class="input-group">
                    <span class="input-group-btn" >
                        <button data-popup="tooltip" title="<%= Resources.Resource.InputTime_ToolTip %>" class="btn btn-default p-2 border-0" :class="details.rowsChecked ? 'text-default' : 'text-primary'" @click="onDayDuration" type="button">
                            <span class="p-2">
                                <i class="icon-watch2"></i>
                            </span>
                            
                        </button>
                    </span>
                    <input v-if="details.rowsChecked" data-popup="tooltip" title="<%= Resources.Resource.InputTime_ToolTip %>" type="text" class="form-control text-center no-padding border-0 "  v-model="rowsHours"  @click="onDayDuration" readonly="readonly"/>

                    <input v-show="!details.rowsChecked" type="text" class="form-control text-center no-padding" v-bind:class="{ 'border-0': selected == false }"
                           placeholder="hh:mm" v-model="details.enteredDuration" v-on:change="onTimeChanged" />

                    <span v-if="details.rowsChecked" data-popup="tooltip" title="<%= Resources.Resource.InputTime_ToolTip %>" class="input-group-addon p-2 border-0"><i class="icon-lock5"  @click="onDayDuration"></i></span>
                       
                </div>
               
           
                <div class="input-group">
                    
                    <span class="input-group-btn" >
                        <button class="btn btn-default p-2 border-0"  type="button">
                            <span class="p-2">
                                <i class="icon-minus2"></i>
                            </span>
                            
                        </button>
                    </span>
                    <input type="text" class="form-control text-center no-padding "  v-bind:class="{ 'border-0': selected == false }"
                           placeholder="<%= Resources.Resource.Deduction %>" min="0" max="180" v-model="details.lunch" />
                    
                </div>
                
            </div>
            <div class="panel-footer "  v-bind:class="{ 'border-0': selected == false }">
                <span class="full-width"> <%= Resources.Resource.Total %></span> : <span class="">{{hours}}</span>
            </div>  
        </div>
       
    </template>

    <template id="timesheet-table">
        <table class="table">
        	<thead>
				<tr class="row">
                    <th class="col-lg-pull-0" name="warn" ></th>
                    <th class="col-lg-1" style="min-width: 105px"><%= Resources.Resource.Date %></th>
                    <th class="col-lg-1"><%= Resources.Resource.StartTime %></th>
                    <th class="col-lg-1"><%= Resources.Resource.EndTime %></th>                     
                    <th class="col-lg-1"><%= Resources.Resource.CategoryOfTime %></th>
                    <th class="col-lg-2 "><%= Resources.Resource.Project %></th>
                    <th class="col-lg-2 "><%= Resources.Resource.Service %></th>
                    <th class="col-lg-1"><%= Resources.Resource.Rate %></th>
                    <th class="col-lg-1"><%= Resources.Resource.Duration %></th>
                    <th class="col-lg-2" ><%= Resources.Resource.Comment %></th>
				</tr>
			</thead>
			<tbody>
                <tr class="form-group row " is="timesheet-table-row" v-for="(rec, index) in   records" v-bind:index="index" v-bind:item="rec" v-bind:week="week"/>
			</tbody>
            <tfoot class=" ">
                   <tr class="row">
                       <td colspan="2" class="form-group ">
                           <label class="control-label" ><strong><%= Resources.Resource.Total %>:</strong> </label>

                           <label class="control-label " readonly="readonly"><strong>{{totalDayFormatted}}</strong></label>
                       </td>

                        <td colspan="7">
                           <label class="alert alert-danger alert-styled-left alert-dismissible " v-show="isTimeWarning">
									<span class="font-weight-semibold"><%= Resources.Resource.ERR_StartDate_EndDate %></span>
							    </label>
                       </td>
                       <td >
                             <button id="btnAddLine" @click="addLine(null,null,null)" type="button" class="btn btn-default pull-right" style="margin-right:20px">
                                 <i class="icon-plus2 position-left"></i> <%= Resources.Resource.Add_line %>
                             </button>
                       </td>

                   </tr>
               </tfoot>
		</table>
    </template>
   
    <template id="timesheet-table-simplified" v-on:day-duration-changed="recalculateWeeklySummary()">
        <div>
            <table class="table">
                <thead visible="false">
				    <tr class="row">
                        <th class="hidden"></th>
                        <th class="col-lg-pull-0" name="warn" ></th>
                        <th class="col-lg-1 date-min-width-timesheet"><%= Resources.Resource.Date %></th>
                        <th class="col-lg-1"><%= Resources.Resource.StartTime %></th>
                        <th class="col-lg-1"><%= Resources.Resource.EndTime %></th>                     
                        <th class="col-lg-1"><%= Resources.Resource.CategoryOfTime %></th>
                        <th class="col-lg-2 "><%= Resources.Resource.Project %></th>
                        <th class="col-lg-2 "><%= Resources.Resource.Service %></th>
                        <th class="col-lg-1"><%= Resources.Resource.Duration %></th>
                        <th class="col-lg-2" ><%= Resources.Resource.Comment %></th>
				    </tr>
			    </thead>
            </table>
            <table is="timesheet-table-simplified-day" ref="days" v-for="(dayRecords, dayIndex) in records" v-bind:records="dayRecords" v-bind:dayindex="dayIndex" v-bind:week="week"/>
            <table class="table">
                <tr class="form-group row">
                    <td style="padding-left: 45px">
                        <p>
                            <strong><%= Resources.Resource.WeeklySummary %></strong>
                        </p>
                        <div>
                            <%= Resources.Resource.TotalWeeklyHours %>: <span id="summary-total-weekly-hours">{{summary.totalWeekDurationFormatted}}</span>
                        </div>
                        <div>
                            <%= Resources.Resource.TotalOvertime %>: <span id="summary-total-overtime-hours">{{summary.totalOvertimeFormatted}}</span>, <%= Resources.Resource.OfWhich %>:
                        </div>
                        <div>
                            <span id="summary-hors-percent-rate1"><%= string.Format(Resources.Resource.HoursPercent, ActiveContract != null ? ActiveContract.OvertimeRate.ToString("G29") : "0") %></span>:
                            <span id="summary-overtime-hours-rate1">{{summary.overtimeRate1Formatted}}</span>
                        </div>
                        <div>
                            <span id="summary-hors-percent-rate2"><%= string.Format(Resources.Resource.HoursPercent, ActiveContract != null ? ActiveContract.OvertimeRate2.ToString("G29") : "0") %></span>: 
                            <span id="summary-overtime-hours-rate2">{{summary.overtimeRate2Formatted}}</span>
                        </div>
                        <div>
                            <%= Resources.Resource.TotalHoursToBeCompensated %>: <span id="summary-total-hours-to-be-compensated">{{summary.timeToBeCompensatedFormatted}}</span>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </template>

    <template id="timesheet-table-simplified-day">
        <table class="table">
            <tbody>
                <tr v-bind:class="{'form-group': true, 'row': true, 'background-light-gray-timesheet': dayIndex % 2 == 1}" is="timesheet-table-row" v-for="(rec, index) in records" v-bind:index="index" v-bind:item="rec" v-bind:week="week" v-bind:isdatereadonly="true" v-bind:hiderate="true"/>
                <tr v-bind:class="{'form-group': true, 'row': true, 'background-light-gray-timesheet': dayIndex % 2 == 1}" class="form-group row ">
                    <td colspan="10" style="padding:15px 5px 5px 0;">
                        <div style="float: left; margin-left: 60px;"><strong>{{date}}</strong></div>
                        <div style="float: left; margin-left: 20px;">
                            <span style="margin-right: 20px;" class="input-group-inline" v-show="dayIndex < 5">
                                <label class="checkbox-inline checkbox-right">
                                    <%= Resources.Resource.PauseNotTaken %>
                                    <input type="checkbox" class="control-primary" v-model="pauseNotTaken" v-on:change="onPauseNotTakenChanged()" />
                                </label> 
                            </span>
                            <span class="input-group-inline"><%= Resources.Resource.TotalDailyHours %>: {{totalDayFormatted}} - <%= Resources.Resource.Break %>: {{breakDurationFormatted}}</span>
                        </div>
                        <div style="float: right;">
                             <button id="btnAddLine" @click="addLine(null,null,null)" type="button" class="btn btn-default pull-right" style="align-self: flex-end;">
                                <i class="icon-plus2 position-left"></i> <%= Resources.Resource.Add_line %>
                            </button>
                            <%--Duplicate button should not be visible for Saturdays--%>
                            <a href="javascript:void(0)" :id="'btnDuplicateLastLine-' + dayIndex"
                                v-show="dayIndex >= 0 && dayIndex < 5"
                                :disabled="!records || !records.length"
                                class="btn btn-default pull-right" style="margin-right:20px; align-self: flex-end;"
                                data-toggle="popover">
                                <i class="icon-plus2 position-left"></i> <%= Resources.Resource.Duplicate %>
                            </a>

                            <div :id="'duplicate-form-' + dayIndex" class="hide">
                                <div class="form-group">
			                        <div :id="'duplicate-options-' + dayIndex" class="input-group">
                                        <div>
                                            <input type="radio" :name="'radio-duplicate-' + dayIndex" value="week" />
                                            &nbsp;&nbsp;<%= Resources.Resource.DuplicateTimesheetOptions_Week %>
                                        </div>
                                        <div>
                                             <input type="radio"  :name="'radio-duplicate-' + dayIndex" value="nextday" />
                                             &nbsp;&nbsp;<%= Resources.Resource.DuplicateTimesheetOptions_Day %>
                                        </div>
			                        </div>
                                    <br />
                                    <div class="input-group" style="width: 100%">
                                        <a href="javascript:void(0)" class="btn btn-primary btn-save pull-right-xs" 
                                            :onclick="'duplicateRecords(' + dayIndex + ')'"
                                            :id="'duplicate-btn-' + dayIndex"><%= Resources.Resource.Duplicate %></a>
                                        <button type="button" class="btn btn-link pull-right-xs" :onclick="'$(&quot;#btnDuplicateLastLine-' + dayIndex + '&quot;).popover(&quot;hide&quot;)'"><%= Resources.Resource.Cancel %></button>
                                    </div>
		                        </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </template>

    <template id="timesheet-table-row">
        <tr class="collapse in" v-bind:class="[{'bg-light-grey'  : item.parentId != null}, 'parent-id-'+item.parentId]">
            <td class="hidden">{{item.index = index}}</td>
            <td class="col-lg-pull-0" style="padding: 0 20px;">
                <i v-show="useWorkSchedule==true && item.warn == true" class=" icon icon-warning pull-right"></i>
                
                <a  v-show="item.isParent == true " data-action="collapse" data-toggle="collapse" @click="function(e){e.target.classList.toggle('rotate-180')}"
                    :data-target="'.parent-id-' + (item.id<1 ? item.position : item.id)" >
                    <i class=" icon-arrow-down12 pull-right"></i>
                </a>
                
                <i v-show="item.parentId != null" class=" icon-minus2 pull-right"></i>

            </td>

            <%--Date--%>
            <td v-bind:class="{'p-5': true, 'col-lg-1': true, 'date-min-width-timesheet': hiderate}">
                <select class="form-control select" :disabled="isdatereadonly" v-model="item.dayOfWeek"  v-show="item.parentId == null"  v-on:change="onDayChanged( $event)">
                    <option v-for="option in week" :value="option.dayOfWeek">{{ option.name }}</option>
                </select>
            </td>

            <%--Start time--%>
            <td class="p-5 col-lg-1">
                <input type="text" class="form-control" placeholder="hh:mm" v-model="item.startTime" v-on:change="onTimeChanged('startTime', $event)" :disabled="item.parentId !=null"/>

            </td>
            
            <%--End time--%>
            <td class="p-5 col-lg-1">
                <input type="text" class="form-control" placeholder="hh:mm" v-model="item.endTime" v-on:change="onTimeChanged('endTime', $event)" :disabled="item.parentId !=null"/>
            </td>

            <%--Category of time--%>
            <td class="p-5 col-lg-1">
                <select class="form-control select" v-on:change="onScheduleRecChanged($event)" :disabled="item.isParent" v-model="item.time">
                    <option v-for="option in scheduleRecList" v-show="option.timeType!=3 || option.timeType==3 && item.isParent" :value="option.timeName">{{ option.timeName }}</option>
                </select>
            </td>
                
            <%--Project--%>
            <td class="p-5 col-lg-2">
                <div class="v-select-control">
                    <v-select :options="projectsList" label="jobName" v-model="item.jobId" :reduce="job => job.jobId"  :disabled="item.isParent" :selectable="option => (option && !option.hasOwnProperty('group'))" append-icon="false"  class="vs-style">
                        <template #option="{ group, jobName, recordStatus }" v-show="recordStatus == 0" >
                            <v-list-item v-bind:class="{ 'item-deleted': recordStatus != 0 }">
                                <v-list-item-content>
                                    <v-list-item-title>
                                        <v-row no-gutters align="center">
                                            <div v-if="group" class="group" >
                                                {{ group }}

                                            </div>
                                            <span>{{ jobName }}</span>
                                        </v-row>
                                    </v-list-item-title>
                                </v-list-item-content>
                            </v-list-item>

                        </template>
                        <div slot="no-options"><%= Resources.Resource.NoData %></div>
                    </v-select>
                </div>
            </td>

            <%--Service--%>
            <td class="p-5 col-lg-2">
                <div class="v-select-control">
                <v-select :options="servicesList" label="serviceName" v-model="item.serviceId" :reduce="service => service.serviceId" :disabled="item.isParent" :selectable="option => !option.hasOwnProperty('group')"  class="vs-style" >
                     <template #option="{ group, serviceName, recordStatus }">
                         <v-list-item v-bind:class="{ 'item-deleted': recordStatus != 0 }" >
                             <v-list-item-content>
                                 <v-list-item-title>
                                     <v-row no-gutters align="center">
                                         <div v-if="group" class="group" >
                                             {{ group }}

                                         </div>
                                         <span>{{ serviceName }}</span>
                                     </v-row>
                                 </v-list-item-title>
                             </v-list-item-content>
                         </v-list-item>
                     </template>
                     
                    <template v-slot:no-options>
                        <span><%= Resources.Resource.NoData %></span>
                    </template>
                     

                 </v-select>
                </div>
            </td>

            <%--Rate--%>
            <td class="p-5 col-lg-1" v-if="!hiderate">
                    <input type="number" class="form-control" v-model="item.rate"   :disabled="item.isParent ||  item.time != '<%= Resources.Resource.Custom %>'"/>
            </td>

            <%--Duration--%>
            <td class="p-5 col-lg-1">
                <input type="text" class="form-control" placeholder="" v-model="item.duration" :value="getDuration(item)"   readonly="readonly" :disabled="item.parentId !=null"/>
            </td>

            <%--Comment--%>
            <td class="p-5 col-lg-2">
                <div class="input-group">
                    <input type="text" class="form-control pull-left " placeholder="<%= Resources.Resource.Comment %>" v-model="item.comment" v-show="item.parentId ==null"/>
                    <div class="input-group-btn p-5" >
                        <a v-show="item.parentId == null" class="btn-link  pull-right" @click="deleteRow()"><i class="icon-trash"></i></a> 
                    </div>
                </div>
            </td>
        </tr>
    </template>

    
   

<script type="text/javascript" src="Sheet.js?<%= Guid.NewGuid().ToString("n") %>"></script>
<%--<script type="text/javascript" src="Sheet.js"></script>--%>
<script type="text/javascript">
    $(function () {
        $("#<%= ddlEmployee.ClientID %>").chosen();
    });
</script>

</asp:Content>

