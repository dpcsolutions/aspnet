﻿<%@ WebHandler Language="C#" Class="Pdf" %>

using System;
using System.Web;

public class Pdf : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {

        long id = -1;
        long.TryParse(context.Request.Params["id"], out id);


        bool dl = true;
        if (context.Request.Params["a"] != null && context.Request.Params["a"] == "0") dl = false;

        if (id > -1)
        {
            System.Globalization.CultureInfo c = new System.Globalization.CultureInfo("en-US");
            try
            {
                c = new System.Globalization.CultureInfo(context.Request.Params["c"]);
            }
            catch { }


            Calani.BusinessObjects.DocGenerator.TimeSheetGenerator gen = new Calani.BusinessObjects.DocGenerator.TimeSheetGenerator(c, PageHelper.GetCurrentOrganizationId(context.Session));
            gen.TimeSheetId = id;
            var bin = gen.GenerateDocument();

            context.Response.ContentType = "application/pdf";
            if (dl)
            {
                context.Response.AddHeader("content-disposition", "Attachment; filename=TimeSheet-" + id + ".pdf");
            }
            context.Response.BinaryWrite(bin);
        }
        else
        {

            context.Response.ContentType = "text/plain";
            context.Response.Write("Hello World");
        }
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}