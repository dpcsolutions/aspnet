﻿using Calani.BusinessObjects.Contacts;
using System;
using System.Linq;
using System.Web.Script.Serialization;

public partial class Time_Sheets_Sheet : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    public string JsonRecords { get; set; }
    public string JsonOrgTimesheets { get; set; }
    public string ActiveContractJson { get; set; }
    public ContractDto ActiveContract { get; set; }
    
    public string JsonList { get; set; }


    public long SheetId { get; set; }
    public int OrgLunchMin { get; set; }
    public int OrgDailyBreakMin { get; set; }
    public int TimeSheetType { get; private set; }
    public int LinesPerDay { get; private set; }
    
    public bool UseWorkSchedule { get; private set; }
    
    public string MinDate { get; set; }
    public string MaxDate { get; set; }
    
    public bool CanSaveValidated { get; set; }
    public bool IsUserEmployee { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        long id = -1;
        long.TryParse(Request.Params["id"], out id);
        if (id == -1)
        {
            Response.Redirect(".");
            return;
        }

        SheetId = id;

        Calani.BusinessObjects.TimeManagement.TimeSheetsManager tmgr = new Calani.BusinessObjects.TimeManagement.TimeSheetsManager(ph.CurrentOrganizationId);
        var timesheet = tmgr.Get(id);

        CanSaveValidated = false;
        IsUserEmployee = false;
        // only own page available for employee
        if (ph.CurrentUserType == ContactTypeEnum.User_Employee)
        {
            IsUserEmployee = true;
            panelValidation.Visible = false;

            if (timesheet != null)
            {
                if (timesheet.employeeId == null) Response.Redirect(".");
                else if (timesheet.employeeId.Value != ph.CurrentUserId) Response.Redirect(".");                
            }
        }
        // -->
        
        if (ph.CurrentUserType == ContactTypeEnum.User_Admin 
            || ph.CurrentUserType == ContactTypeEnum.PlatformAdmin_Support 
            || timesheet?.acceptedDate == null) {
            CanSaveValidated = true;
        }

        if (timesheet != null && timesheet.year != null && timesheet.week != null)
        {
            Calani.BusinessObjects.Generic.CalendarTools cal = new Calani.BusinessObjects.Generic.CalendarTools(timesheet.year.Value, timesheet.week.Value);
            MinDate = ph.ToUnixMilliseconds(cal.Monday).ToString();
            MaxDate = ph.ToUnixMilliseconds(cal.Monday.AddDays(7)).ToString();

            liPdf.Visible = true;
            liQmPdf.Visible = true;
            liPdfLink.Visible = true;
            liPdfSep.Visible = true;
            liPdfLink.HRef = "Pdf.ashx?id=" + timesheet.id + "&c=" + System.Threading.Thread.CurrentThread.CurrentUICulture.Name + "&a=0";
            liQmPdfLink.HRef = "Pdf.ashx?id=" + timesheet.id + "&c=" + System.Threading.Thread.CurrentThread.CurrentUICulture.Name + "&a=0";


            quickmenu.Visible = true;
        }
        else
        {
            MinDate = "null";
            MaxDate = "null";

            liPdf.Visible = false;
            liPdfLink.Visible = false;
            liPdfSep.Visible = false;

            quickmenu.Visible = false;
        }


        Calani.BusinessObjects.TimeManagement.TimeSheetItemsList selectableItems = null;
        if(timesheet != null)
        {
            selectableItems = new Calani.BusinessObjects.TimeManagement.TimeSheetItemsList(ph.CurrentOrganizationId, timesheet.id);

        }
        else
        {
            selectableItems = new Calani.BusinessObjects.TimeManagement.TimeSheetItemsList(ph.CurrentOrganizationId,
                DateTime.Now.Year, Calani.BusinessObjects.Generic.CalendarTools.GetNonIsoWeekOfYear(DateTime.Now),
                ph.CurrentUserId);
        }

        
        var list = selectableItems.List(System.Threading.Thread.CurrentThread.CurrentUICulture, true);
        JsonList = new JavaScriptSerializer().Serialize(list);

        //TimeSheetTitle = tm.year + " W" + tm.week + ": " + tm.contacts.lastName + " " + tm.contacts.firstName;

        if(timesheet != null && timesheet.year != null) 
            tbxYear.Value = timesheet.year.Value.ToString();

        if(timesheet != null && timesheet.week != null) 
            tbxWeek.Value = timesheet.week.Value.ToString();

        if (timesheet == null) 
            tbxYear.Value = DateTime.Now.Year.ToString();

        if (timesheet == null) 
            tbxWeek.Value = Calani.BusinessObjects.Generic.CalendarTools.GetNonIsoWeekOfYear(DateTime.Now).ToString();

        Calani.BusinessObjects.Contacts.EmployeesManager empMgr = new Calani.BusinessObjects.Contacts.EmployeesManager(ph.CurrentOrganizationId);
        var empList = empMgr.List();

        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            if (timesheet == null)
            {
                empList = (from r in empList where r.id == ph.CurrentUserId select r).ToList();
            }
        }

        ddlEmployee.DataSource = (from r in empList select 
                                  new { id = r.id, name = "["+r.initials+"] " + r.firstName + " " + r.lastName }).ToList();

        if (empList.Any())
        {
            if (timesheet != null && timesheet.employeeId != null)
            {
                if ((from r in empList where r.id == timesheet.employeeId.Value select r).Count() > 0)
                    ddlEmployee.SelectedValue = timesheet.employeeId.Value.ToString();
            }

            if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
            {
                if (timesheet == null)
                {
                    ddlEmployee.SelectedValue = ph.CurrentUserId.ToString();
                }
            }
            else
            {
                if (timesheet == null)
                {
                    ddlEmployee.SelectedValue = ph.CurrentUserId.ToString();
                }
            }
        }

        ddlEmployee.DataBind();


        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee || id > 0)
        {
            ddlEmployee.Enabled = false;
        }


        lblLastChange.InnerText = Calani.BusinessObjects.TimeManagement.TimeSheetsManager.GetLastChangeStatus(timesheet, System.Threading.Thread.CurrentThread.CurrentUICulture);
        
        lblValidationStatusEmp.InnerText = Calani.BusinessObjects.TimeManagement.TimeSheetsManager.GetValidationStatus(timesheet, System.Threading.Thread.CurrentThread.CurrentUICulture);
        lblValidationStatusAdm.InnerText = Calani.BusinessObjects.TimeManagement.TimeSheetsManager.GetValidationStatus(timesheet, System.Threading.Thread.CurrentThread.CurrentUICulture);
        
        lblValidationStatusEmp.Visible = IsUserEmployee;
        lblValidationStatusAdm.Visible = !IsUserEmployee;
        
        if(timesheet != null)
            tbxComment.InnerText = timesheet.comment;


        var orgMgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(ph.CurrentOrganizationId);
        orgMgr.Load();
        OrgLunchMin = orgMgr.LunchDuration;
        OrgDailyBreakMin = orgMgr.DailyBreakDuration;
        TimeSheetType = orgMgr.TimeSheetType;
        LinesPerDay = orgMgr.TimeSheetLinesPerDay;
        UseWorkSchedule = orgMgr.UseWorkSchedule;

        var mgr = new Calani.BusinessObjects.TimeManagement.TimeSheetRecordsManager(ph.CurrentOrganizationId);

        var records = mgr.ListSheetRecords(id);

        var jsRecords = (from r in records select new Calani.BusinessObjects.AjaxService.TimeUpdate(r)).ToList();
        
        if (records.Any())
        {
            tbxWeek.Disabled = true;
            tbxYear.Disabled = true;
        }

        var jdata = new JavaScriptSerializer().Serialize(jsRecords);
        JsonRecords = jdata;

        var sheetsList = tmgr.GetOrgTimeSheets();

        JsonOrgTimesheets = new JavaScriptSerializer().Serialize(sheetsList);

        var contractsManager = new EmployeesManager.EmployeesContractsManager(ph.CurrentOrganizationId);
        long employeeId = 0;
        ActiveContract = long.TryParse(ddlEmployee.SelectedValue, out employeeId) ? contractsManager.GetActiveContractDto(employeeId) : null;
        ActiveContractJson = ActiveContract != null ? new JavaScriptSerializer().Serialize(ActiveContract) : "null";
    }

    
}