﻿using Calani.BusinessObjects.CustomerAdmin;
using System;

public partial class Time_Sheets_Default : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    public string OptionsPeriod { get; set; }
    public double TotalHours { get; set; }
    public long? FilterEmployee { get; set; }
    public bool UseWorkSchedule { get; private set; }
    public string TimesheetTemplateType { get; private set; }
    public bool IsAdmin { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        // remove records for employee
        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            FilterEmployee = ph.CurrentUserId;
        }
        // -->

        IsAdmin = ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin;

        var orgMgr = new OrganizationManager(ph.CurrentOrganizationId);
        orgMgr.Load();

        OptionsPeriod = ph.RenderPeriodsListOption(preselect: orgMgr.DefaultPeriodFilter);

        UseWorkSchedule = orgMgr.UseWorkSchedule;
        TimesheetTemplateType = orgMgr.TimeSheetType.ToString();
    }
}