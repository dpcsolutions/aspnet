﻿var datePick = function (id, localisedDateFormat, options) {
    var _id = ".bootbox-body ." + id;

    var initOptions = {
        timePicker: false,
        singleDatePicker: true,
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default',
        language: lang ?? (options && options.locale ? options.locale : ""),
        locale: {
            format: localisedDateFormat,
            daysOfWeek: jQuery.fn.pickadate.defaults.weekdays,
            monthNames: jQuery.fn.pickadate.defaults.months,
            firstDay: 1
        },
    };

    if (options && options.startDate) {
        initOptions.startDate = options.startDate;
    }

    var init = function (data) {
        $(_id).daterangepicker(initOptions);

        if (options && options.onApplyCallback) {
            $(_id).on('apply.daterangepicker', options.onApplyCallback);
        }
        
        return this;
    };

    init()

    return {
        init: init,
        id: _id,
    }
}