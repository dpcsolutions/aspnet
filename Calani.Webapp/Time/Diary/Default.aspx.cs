﻿using Calani.BusinessObjects;
using Calani.BusinessObjects.Calendars;
using Calani.BusinessObjects.Projects;
using System;
using System.Globalization;
using System.Linq;
using System.Web.Script.Serialization;
using Calani.BusinessObjects.Model;

public partial class Time_Diary_Default : System.Web.UI.Page
{
    public PageHelper ph;   
   
    public string locale { get; set; }
    public Array standardBusinessHours { get; set; }
    public String defaultDate { get; set; }
    public string resources { get; set; }
    public string resourcesForJob { get; set; }
    public string clients { get; set; }
    public string localisedDateFormat { get; set; }
    public string internalServices { get; set; }
    public string currentLanguage { get; set;  }
    public string preselectedJobId { get; set; }
    public string preselectedClientId { get; set; }
    public string officialHolidays { get; set; }
    public string calendarEventsJson { get; set; }
    public string StringResourcesJson { get; private set; }
    public string preSelectedResources { get; set; }
    public string canChangeResourceSelection { get; set; }
    public string EmployeeCanViewAllEvents { get; set; }
    public long currentUserId { get; set; }
    public long preselectedVisitId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        ph = new PageHelper(this);

        preSelectedResources = "null";
        canChangeResourceSelection = "true";


        currentUserId = ph.CurrentUserId;

        var orgMgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(ph.CurrentOrganizationId);
        orgMgr.Load();

        EmployeeCanViewAllEvents = orgMgr.EmployeeCanViewAllEvents.ToString().ToLowerInvariant();
        // employee view : resource filtered
        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            if (!orgMgr.EmployeeCanViewAllEvents)
            {
                preSelectedResources = "[" + ph.CurrentUserId + "]";
                canChangeResourceSelection = "false";
            }
        }
        // -->
        
        long.TryParse(Request.Params["visitId"], out var visitId);
        if (visitId > 0)
        {
            var visit = new CalaniEntities().visits.First(x => x.id == visitId);
            preSelectedResources = "[" + visit.lastModifiedById + "]";
            preselectedVisitId = visitId;
        }
        
        var colorFromTheme = new String[] { "#AD1457", "#6A1B9A", "#4527A0", "#283593", "#0277BD", "#00695C", "#558B2F", "#EF6C00", "#4E342E", "#444444", "#37474F" };


        var jobMgr = new JobsManager(ph.CurrentOrganizationId);
        var employeeMgr = new Calani.BusinessObjects.Contacts.EmployeesManager(ph.CurrentOrganizationId);

        defaultDate = string.Concat(DateTime.UtcNow.ToString("s"), "Z");
        currentLanguage = "fr";
        localisedDateFormat = ph.localisedDateFormat;


        /*  (NOT REQUIRED FOR JOB PAGE) */

        //get list of all clients 
        var mgr1 = new Calani.BusinessObjects.CustomerAdmin.ClientsManager(ph.CurrentOrganizationId);

        var clientsExt = (from c in mgr1.List() select new clientExtended(c));
        clients = new JavaScriptSerializer().Serialize(clientsExt);

        //list all internal jobs
        Calani.BusinessObjects.CustomerAdmin.InternalJobsManager internalJobsMgr = new Calani.BusinessObjects.CustomerAdmin.InternalJobsManager(ph.CurrentOrganizationId);
        internalServices = new JavaScriptSerializer().Serialize(internalJobsMgr.List().Select(r => new Calani.BusinessObjects.AjaxService.SimpleItem() { id = r.id.ToString(), text = r.name }).ToList<Calani.BusinessObjects.AjaxService.SimpleItem>());

        /*  end of (NOT REQUIRED FOR JOB PAGE) */

        //retrieve all resources for organisation
        var tmpList = employeeMgr.ListActiveUsers().Select(r => new resourcesExtended(r)).ToList<resourcesExtended>();
        for (var i = 0; i < tmpList.Count; i++)
            tmpList[i].color = colorFromTheme[i % 10];

        resources = new JavaScriptSerializer().Serialize(tmpList);
        
        //handling public holidays
        var publicHolidays = jobMgr.ListVisits(null, null, null, null, (long?)VisitTypeEnum.PublicHolidays);
        foreach(var p in publicHolidays)
        {
            p.isHolidays = false;
            p.isOfficialHolidays = true;
        }

        officialHolidays = new JavaScriptSerializer().Serialize(publicHolidays);
        StringResourcesJson = SharedResource.ReadStringResources(CultureInfo.CurrentCulture);

        var calendarsRecManager = new CalendarsRecManager(ph.CurrentOrganizationId);
        var calendarEvents = calendarsRecManager.GetCalendarRecordsDto().Select(r => new visitsExtended(r));
        calendarEventsJson = new JavaScriptSerializer().Serialize(calendarEvents);
    }
}
 