﻿$(function () {

    var sp1 = new schedulerPage();

    options = {
        screen: 0, //0:planning, 1:job, 2:holidays
        resources: resources,
        calendarId: 'fullcalendar1',
        canChangeResourceSelection: canChangeResourceSelection,
        renderer: sp1.eventRenderer,
        resourcesListId: '.select-resources2', //todo seubi a undefined//
        defaultView: 'month',
        preSelectedResources: preSelectedResources,
        internalServices: internalServices,
        clients: aclients
    };

    if (canChangeResourceSelection == false) {
        $('.select-resources2').attr('disabled', true);
        $('.select-resources-section .input-group-btn').hide();
        $('.select-resources-section .info').hide();
        $('.input-group-scheduer-ressource-filter').hide();
    }

    sp1.init(options);

    $("#btnAddMeeting").click(
          function () {
              sp1.addOrEditEvent(new moment());
          });

});
