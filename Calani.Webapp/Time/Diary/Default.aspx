﻿<%@ Page Title="Diary" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Time_Diary_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<form runat="server">
    <div class="content">
       <div class="panel panel-white">
			<div class="panel-heading">
				<h6 class="panel-title">
					<a name="quotes">
						<i class="icon-upload display-inline-block text-orange"></i>
						<%= Resources.Resource.Schedulingof %> <strong><%= Resources.Resource.Visits %> </strong>
					</a>
				</h6>
			</div>

			<div class="panel-body">
               	<div class="content-group-lg select-resources-section">
				    <div class="info">
                        <h6 class="text-semibold"><%= Resources.Resource.EmployeesSelection %> </h6>
			            <p class="content-group-sm"><%= Resources.Resource.EmployessVisitsAreVisibleOnTheCalendar %></p>
                    </div>
                    <div class="input-group input-group-scheduer-ressource-filter">
                        <select class="select select-resources2" multiple="multiple">
					    </select>

                        <span class="input-group-btn"><button class="btn select-all-resources"><%= Resources.Resource.All %></button></span>
                        <span class="input-group-btn"><button class="btn clear-all-resources"><%= Resources.Resource.Clear %></button></span>
		            </div>
        		<div class="fullcalendar1"></div>		
	        </div>
			<div class="panel-footer text-right no-padding">
				<div class="row" style="margin:10px">
					<a class="btn btn-default" id="btnAddMeeting"><i class="icon-plus2 position-left"></i> <%= Resources.Resource.AddVisit %></a>
				</div>
			</div>
		</div>
    </div>
    </div>
    
    <script type="text/javascript">
        var publicHolidayBackgroundColor = "#fdb5b5";

        var locale = "<%= locale %>";
        var defaultDate = "<%= defaultDate%>";
        var resources = <%= resources %>;
        var aclients = <%= clients %>;
        var currentLanguage = '<%= currentLanguage%>';
        var localisedDateFormat = '<%= localisedDateFormat%>';
        var internalServices = <%= internalServices%>;
        var officialHolidays = <%= officialHolidays %>;
        var calendarEvents = <%= calendarEventsJson %>;
        var employeeCanViewAllEvents = <%= EmployeeCanViewAllEvents %>;
        var currentUserId =  <%= currentUserId %>;
        officialHolidays.forEach(h => { h.rendering = "background"; h.backgroundColor = publicHolidayBackgroundColor; h.allDay = true });

        var preSelectedResources = <%= preSelectedResources %>;
		var preselectedVisitId = <%= preselectedVisitId %>;	
        var canChangeResourceSelection = <%= canChangeResourceSelection %>;

        var standardBusinessHours = [ // specify an array instead
            {
                dow: [ 1, 2, 3, 4, 5 ], // Monday, Tuesday, Wednesday
                start: '08:00', // 8am
                end: '18:00' // 6pm
            },
        ];
        var ressources2 = { };
        ressources2['Save'] = "<%= Resources.Resource.Save %>";
        ressources2['Delete'] = "<%= Resources.Resource.Delete %>";
        ressources2['Cancel'] = "<%= Resources.Resource.Cancel %>";
        ressources2['DeleteOnlyThisOrAll'] = "<%= Resources.Resource.DeleteOnlyThisOrAll %>";
        ressources2['DeleteAppointment'] = "<%= Resources.Resource.DeleteAppointment %>";
        ressources2['DeleteAllAppointments'] = "<%= Resources.Resource.DeleteAllAppointments %>";
        ressources2['DeleteOnlyThisAppointment'] = "<%= Resources.Resource.DeleteOnlyThisAppointment %>";
        ressources2['DeleteOnlyThisAppointment'] = "<%= Resources.Resource.DeleteOnlyThisAppointment %>";
       
        ressources2['InProgressOnlyThisAppointment'] = "<%= Resources.Resource.InProgressOnlyThisAppointment %>";
        ressources2['InProgressAllAppointments'] = "<%= Resources.Resource.InProgressAllAppointments %>";

        ressources2['TerminateOnlyThisAppointment'] = "<%= Resources.Resource.TerminateOnlyThisAppointment %>";
        ressources2['TerminateAllAppointments'] = "<%= Resources.Resource.TerminateAllAppointments %>";

        ressources2['PostponeOnlyThisAppointment'] = "<%= Resources.Resource.PostponeOnlyThisAppointment %>";
        ressources2['PostponeAllAppointments'] = "<%= Resources.Resource.PostponeAllAppointments %>";

        ressources2['CancelOnlyThisAppointment'] = "<%= Resources.Resource.CancelOnlyThisAppointment %>";
        ressources2['CancelAllAppointments'] = "<%= Resources.Resource.CancelAllAppointments %>";
      

        ressources2['NotStartedOnlyThisAppointment'] = "<%= Resources.Resource.NotStartedOnlyThisAppointment %>";
        ressources2['NotStartedAllAppointments'] = "<%= Resources.Resource.NotStartedAllAppointments %>";

    </script>
   
    <script type="text/javascript" src="Default.js?<%= Guid.NewGuid().ToString("n") %>"></script>
    <%--<script type="text/javascript" src="Default.js"></script>--%>


</form>
</asp:Content>