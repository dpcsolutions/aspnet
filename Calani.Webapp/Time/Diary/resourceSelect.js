﻿var resourceSelect = function (locale, disable, callback) {
    var _id = ".bootbox-body .resourceSelect";

    var select2IsInitialised = function () {
        return ($(_id)[0].className.indexOf("select2") > -1)
    };

    var init = function () {
        if (!select2IsInitialised()) {
            $(_id).select2({
                closeOnSelect: true,
                data: resources,
                language: 'fr',
                multiple: true,
                tags: false,
                minimumResultsForSearch: Infinity,
            });

            $(_id).on("change", function (e) {
                var selected = $(".bootbox-body .resourceSelect").select2('data');

                selected.forEach(function (r) {
                    var query = $(".bootbox-body .select2-selection__choice[title='" + r.title + "']");

                    if (query.length > 0)
                        query.css({ "background-color": r.color });
                });

                if(callback)
                {
                    callback(e, selected);
                }
            });

            if(disable == true)
                $(_id).prop("disabled", true);
        }
        return this;
    };

    init();

    return {
        init: init,
        getValue: function () {
            return $(_id).select2('data');
        },
        setValue: function (visitType) {
            $(_id).val(visitType);
            $(_id).trigger('change');

            return this;
        },
        distroy: function () {
        },
        setColors: function (selection) {
            selection.forEach(function (r) {
                var query = $(".bootbox-body .select2-selection__choice[title='" + r.title + "']");

                if (query.length > 0)
                    query.css({ "background-color": r.color });
            });
        }
    }
}