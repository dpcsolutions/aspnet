﻿var jobSelect = function () {
    var _id = ".bootbox-body .jobSelect";

    var select2IsInitialised = function () {
        return ($(_id).length == 1 ?
            $(_id)[0].className.indexOf("select2") > -1 : false)
    };

    var init = function (data) {
        if (!select2IsInitialised()) {
            $(_id).select2({
                closeOnSelect: true,
                language: 'fr',
                multiple: false,
                placeholder: 'Mandat',
                tags: false,
                minimumResultsForSearch: Infinity,
                data: data
            });
        }

        return this;
    };

    var initAjax = function (clientId) {
        var d = $.Deferred();
        var that = this;

        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            url: '../../API/Service.svc/GetJobsForClient',
            dataType: 'json',
            data: JSON.stringify(clientId),
            success: function (data) {
                that.init(data);
                d.resolve();
            },
            error: function () {
                d.reject();
            }
        });

        return d.promise();
    }

    return {
        selector: _id,
        init: init,
        initAjax:initAjax,
        getValue: function () {
            return $(_id).select2('data');
        },
        setValue: function (jobId) {
            $(_id).val(jobId);
            $(_id).trigger('change');

            return this;
        },
        destroy: function () {
        },
        disable: function () {
            $(_id).prop("disabled", true);
        },
    }
}