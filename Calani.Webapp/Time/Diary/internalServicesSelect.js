﻿var internalServicesSelect = function (id, locale, services) {
    var _id = ".bootbox-body ." + id;

    var select2IsInitialised = function () {
        return ($(_id)[0].className.indexOf("select2") > -1)
    };

    var init = function () {
        if (!select2IsInitialised()) {
            $(_id).select2({
                closeOnSelect: true,
                data: services,
                language: locale,
                multiple: false,
                tags: false,
                minimumResultsForSearch: Infinity,
            });

            $(_id).on('change', function (e) {
            });
        };
        if (services && services.length > 0) {
            $(_id).val(services[0].id);
            $(_id).trigger('change');

        }

        return this;
    };

    init();

    return {
        init: init,
        getValue: function () {
            return $(_id).select2('data')[0].id;
        },
        setValue: function (visitType) {
            $(_id).val(visitType);
            $(_id).trigger('change');
            $(_id).prop("disabled", visitType == 1);

            return this;
        },
        hide: function () {
            $(_id).hide();
        },
        show: function () {
            $(_id).show();
        }
    }
}