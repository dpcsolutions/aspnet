﻿var typeOfMeetingSelect = function(locale){
    var _id = ".bootbox-body .typeOfMeetingSelect";
    var _dialogClientSelection = ".bootbox-body .dialogClientSelection";

    var select2IsInitialised = function () {
        return ($(_id)[0].className.indexOf("select2") > -1)
    };

    var init = function () {
        if (!select2IsInitialised()) {
            $(_id).select2({
                closeOnSelect: true,
                data: [{ id: 0, text: 'Interne' }, { id: 1, text: 'Client' }], //, { id: 2, text: 'Privé' }],
                language: locale,
                multiple: false,
                tags: false,
                minimumResultsForSearch: Infinity,
            });

            $(_id).on('change', function (e) {
                var data =  $(_id).select2('data').length > 0 ? $(_id).select2('data')[0].id : undefined;

                if (data != undefined)
                {
                    if (data == 1) //client
                    {
                        $('.bootbox-body .internalServicesSelect').parent().hide();
                        $(_dialogClientSelection).show();
                    }
                    else
                    {
                        $('.bootbox-body .internalServicesSelect').parent().show();
                        $(_dialogClientSelection).hide();
                    }
                }  
            });
        };

        $(_id).val(0);
        $(_id).trigger('change');

        return this;
    };

    init();

    return {
        init: init,
        getValue: function(){
            return $(_id).select2('data')[0].id;
        },
        setValue: function (visitType) {
            $(_id).val(visitType);
            $(_id).trigger('change');
            $(_id).prop("disabled", visitType == 1);

            return this;
        },
        distroy: function () {
        }
    }
}