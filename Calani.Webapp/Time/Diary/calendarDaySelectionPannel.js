﻿var calendarDaySelectionPannel = function () {

    var days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];

    var buildSelectedDaysString = function (event) {
        let str = "";
        let daysLocal = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche'];

        if (event !== undefined && event.repeatdays !== undefined)
        {
            let strTmp = event.repeatdays.split(',');
            for (let i = 0; i < strTmp.length; i++)
                str += daysLocal[parseInt(strTmp[i]) - 1] + ", ";
            $('.bootbox-body .repeat-visit-num-label').text(event.numrepeat);
        } else {
            for (let i = 0; i < days.length; i++)
                if ($(".bootbox-body .calendar-" + days[i]).hasClass('checked'))
                    str += daysLocal[i] + ", ";
        }

        if (str.endsWith(", ")) {
            str = str.substring(0, str.length - 2);
        }

        if (str === "") {
            $('.bootbox-body .days-label-no-day').show();
            $('.bootbox-body .days-label').text("");
        }
        else {
            $('.bootbox-body .days-label-no-day').hide();
            $('.bootbox-body .days-label').text(str);
        }
    }

    var init = function (event, start) {
        if (event && event.numrepeat && event.numrepeat > 0)
        {
            $(".bootbox-body .calendar-repeat-edit").hide();
            $(".bootbox-body .calendar-repeat-noedit").show();

            buildSelectedDaysString(event);
        } else {
            $(".bootbox-body .calendar-repeat-edit").hide();
            $(".bootbox-body .calendar-repeat-noedit").hide();

            for (var i = 0; i < days.length; i++)
                if (start.day() == (i + 1))
                    $(".bootbox-body .calendar-" + days[i]).addClass('checked');
                else
                    $(".bootbox-body .calendar-" + days[i]).removeClass('checked');

            for (var i = 0; i < days.length; i++) {
                $(".bootbox-body .calendar-" + days[i]).off();
                $(".bootbox-body .calendar-" + days[i]).on('click',
                    function (e) {
                        var item = $(e.target).parent();
                        item.toggleClass('checked');

                        buildSelectedDaysString();
                    })
            }
        }
        return this;
    }
    return {
        init: init
    }
}