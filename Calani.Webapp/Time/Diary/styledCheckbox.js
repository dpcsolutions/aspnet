﻿var styledCheckbox = function (id, callback) {

    var _id = ".bootbox-body " + id;

    var init = function () {
        $(id + " > input").off();
        $(id + " > input").on('click', function (e) {
            if ($(_id).parent().hasClass('disabled')) return false;
            $(_id).addClass('checked');

            if(callback) callback(e);
        });
    }

    init();

    return {
        check: function() {
            $(_id).addClass('checked');
        },
        uncheck: function() {
            $(_id).removeClass('checked');
        },
        triggerClick: function () {
            $(_id + " > input").trigger('click');
        },
        disable: function () {
            $(_id).parent().parent().addClass('disabled');
            $(_id).parent().addClass('disabled');
        },
        enable: function () {
            $(_id).parent().parent().removeClass('disabled');
            $(_id).parent().removeClass('disabled');
        },
        isChecked: function () {
            return $(_id).hasClass('checked')
        },
        isDisabled: function () {
            return $(_id).parent().hasClass('disabled');
        }
    }
}