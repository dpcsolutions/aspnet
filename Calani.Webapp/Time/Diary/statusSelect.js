﻿var statusSelect = function (status) {
    var _id = ".bootbox-body .statusSelect";

    var select2IsInitialised = function (id) {
        return ($(".bootbox-body ." + id).length == 1 ? 
            $(".bootbox-body ." + id)[0].className.indexOf("select2") > -1 : false)
    };

    var init = function () {
        if (!select2IsInitialised()) {
            $(_id).select2({
                closeOnSelect: true,
                language: 'fr',
                multiple: false,
                placeholder: 'statut',
                tags: false,
                minimumResultsForSearch: Infinity,
                data: status
            });
        }

        $(_id).val(4);
        $(_id).trigger('change');

        return this;
    };

    init();

    return {
        init: init,
        getValue: function () {
            return $(_id).select2('data');
        },
        setValue: function (visitType) {
            $(_id).val(visitType);
            $(_id).trigger('change');

            return this;
        },
        distroy: function () {
        },
        disable: function () {
            $(_id).prop("disabled", true);
        }
    }
}