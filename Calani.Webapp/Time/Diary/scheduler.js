﻿var stopPropagation = function (e) { e.stopPropagation() };

var schedulerPage = function () {
    var isInitialised = false;
    var calendarId = '';
    var preSelectedResource;
    var canChangeResourceSelection = true;
    var resourcesTable = [];
    var applyChangesToAllEvents = false;
    var screen = 0;
    var preselectedJobId = undefined;
    var preselectedClientId = undefined;
    var opacifyAllEventsButForJobId = undefined;
    var opacityLevel = "0.35";
    var selectResourcesId;
    var officialHollidays;
    var aspectRatio = 1.35;
    var viewEventsForAllJobs = true;
    var aInternalServices = [];
    var clients = [];
    var allowEmployeeViewAllResources = employeeCanViewAllEvents || false;
    var excludeAbsenceRequests = false;
    var fullCalendarLicenseKey = { schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source' };

    var eventRenderer = function(eventObj, $el)
    {
        //if (eventObj && eventObj.rendering == "background")
        //    return true;

        if (opacifyAllEventsButForJobId && eventObj.jobId !== opacifyAllEventsButForJobId)
            $el.css("opacity", opacityLevel);

        var view = $('.'+calendarId).fullCalendar('getView');

        if (eventObj.end == null)
            eventObj.end = eventObj.start;

        var typeicon = "";
        var label = "";
        var color = (eventObj.resources !== null && eventObj.resources.length > 1 ? color = "#000000" : eventObj.backgroundColor);
        var resources = (eventObj.resources !== null && eventObj.resources.length > 1 ? '<i class="fa fa-users position-left"></i> ' : '');
        var companyName = (eventObj.companyName ? eventObj.companyName : '');
        var startEnd = (eventObj.start.format("HH:mm") + ' - ' + eventObj.end.format("HH:mm"));
        var plannedDuration = "";
        var plannedDurationForAllResources = "";
        var popupTitle = "";
        var resourcesInitials = "";
        var resourcesStr = "";
        var totalTimeForAllResources = 0;
        var timeDifferenceDisplay = "";

        plannedDuration = numeral((eventObj.end - eventObj.start) / 1000).format("00:00");
        plannedDurationForAllResources = numeral((eventObj.end - eventObj.start) / 1000 * (eventObj.resources != null ? eventObj.resources.length : 1)).format("00:00");;

        if (plannedDuration.length > 7)
            plannedDuration = plannedDuration.substring(0, 5);
        else plannedDuration = plannedDuration.substring(0, 4);

        if (plannedDurationForAllResources.length > 7)
            plannedDurationForAllResources = plannedDurationForAllResources.substring(0, 5);
        else plannedDurationForAllResources = plannedDurationForAllResources.substring(0, 4);

        eventObj.resourcesObj.forEach(r=> {
            totalTimeForAllResources += r.effectiveTimeInMinutes;

            var time = numeral(r.effectiveTimeInMinutes * 60).format('00:00');

            if (r.effectiveTimeInMinutes > 0)
            {
                if (time.length > 7)
                    time = time.substring(0, 5);
                else time = time.substring(0, 4);

                resourcesInitials += r.initials + ' (<i class="fa fa-clock-o position-left"></i><a style="text-decoration: underline;color: blue;" onclick="stopPropagation(event)"href="/calani/Time/Sheets/Sheet.aspx?id=' + r.timesheetid + '">' + time + '</a>), ';
                resourcesStr += r.name + ' (<i class="fa fa-clock-o position-left"></i><a style="text-decoration: underline;color: blue;" onclick="stopPropagation(event)" href="/calani/Time/Sheets/Sheet.aspx?id=' + r.timesheetid + '">' + time + '</a>), ';
            }
            else if (eventObj.visitType == 5) { /* absence request */
                resourcesInitials += r.name + ', ';
                resourcesStr += r.name + '<br>';
            }
            else 
            {
                resourcesInitials += r.initials + ' (<i class="fa fa-clock-o position-left"></i>-), ';
                resourcesStr += r.name + ' (<i class="fa fa-clock-o position-left"></i>-)<br>  ';
            }
        });
           
        var visitTimeDifference = (eventObj.end - eventObj.start) / 1000 * (eventObj.resources != null ? eventObj.resources.length : 1) - totalTimeForAllResources * 60;
        var visitTimeDifferenceIcon = "";

        if (visitTimeDifference > 0)
        {
            timeDifferenceDisplay = numeral(visitTimeDifference).format("00:00");
            visitTimeDifferenceIcon = "text-success fa-check";
            visitTimeDifferenceTextStyle = "";
        }
        else
        {
            timeDifferenceDisplay = numeral(visitTimeDifference).format("00:00");
            visitTimeDifferenceIcon = "text-warning fa-exclamation-triangle";
            visitTimeDifferenceTextStyle = "text-warning";
        }


        if (timeDifferenceDisplay.length > 7)
            timeDifferenceDisplay = timeDifferenceDisplay.substring(0, 5);
        else timeDifferenceDisplay = timeDifferenceDisplay.substring(0, 4);

        resourcesInitials = resourcesInitials.length > 0 ? " " + resourcesInitials.substring(0, resourcesInitials.length - 2) : "";
        resourcesStr = resourcesStr.length > 0 ? " " + resourcesStr.substring(0, resourcesStr.length - 2) : "";

        switch (eventObj.visitType) {
            case 3:
                typeicon = '<i class="fa fa-plane position-left"></i>';
                label = "Vacances";
                popupTitle = "Vacances";
                break;
            case 2:
                typeicon = '<i class="fa fa-eye-slash position-left"></i>';
                label = "Privé";
                popupTitle = "Privé";
                break;
            case 1:
                typeicon = '<i class="fa fa-calendar position-left"></i>';
                popupTitle = "Visite client";
                break;
            case 0:
                typeicon = '<i class="fa fa-briefcase position-left"></i>';
                label = "Interne";
                popupTitle = "Interne";
                break;
            case 5:
                var localizedDescription = eventObj.description;
                typeicon = '<i class="fa fa-'+ eventObj.icon +' position-left"></i>';
                label = localizedDescription;
                popupTitle = localizedDescription;
                eventObj.description = localizedDescription;
                break;
            case 6:
                typeicon = '<i class="fa fa-calendar-alt position-left"></i>';
                label = eventObj.description;
                popupTitle = eventObj.description;
                break;
        }

        // For all-day events time is set to 0, so do not display it
        startEnd = (!eventObj.allDay ? startEnd : '');

        switch (view.name) {
            case 'month':
                $el.html('<div class="fc-content"><span class="fc-title">' + resources + typeicon + '<b>' + label + ' ' + companyName + resourcesInitials + '</b></span><br/><span class="fc-time">' + startEnd + '</span></div>');
                break;
            case 'agendaWeek':
                $el.html('<div class="fc-content"><span class="fc-title">' + resources + typeicon + '<b>' + label + ' ' + companyName + resourcesInitials + '</b></span><br/><span class="fc-time">' + startEnd + '</span></div>');
                break;
            case 'agendaDay':
                $el.html('<div class="fc-content"><span class="fc-time">' + typeicon + startEnd + '</span> <span class="fc-title">' + '<b>' + label + ' ' + companyName + '</b></span> <span class="cal-contact-name">(' + (eventObj.contactName ?? '') + ')</span><br/>' + (resources ?? '') + '<span class="cal-resource"><b>' + (eventObj.resourcesStr ?? '') + '</b></span></div>');
                break;
            case 'listYear':
            case 'listWeek':
            case 'listMonth':
            case 'listaDay':
                
                var iconVisitStatus = "icon-play4";
                var visitStatusText ="En cours";

                switch (eventObj.visitStatus) {
                    case 4:
                        iconVisitStatus = "fa-hourglass-start";
                        visitStatusText = "Pas commencé";
                        visitStatusLabelColor = "bg-slate";
                        break;
                    case 0:
                        iconVisitStatus = "icon-play4";
                        visitStatusText = "En cours";
                        visitStatusLabelColor = "bg-primary";
                        break;
                    case 1:
                        iconVisitStatus = "icon-finish";
                        visitStatusText = "Terminé";
                        visitStatusLabelColor = "bg-success";
                        break;
                    case 2:
                        iconVisitStatus = "icon-pause2";
                        visitStatusText = "Reporté";
                        visitStatusLabelColor = "bg-info";
                        break;
                    case 3:
                        iconVisitStatus = "icon-calendar2";
                        visitStatusText = "Annulé";
                        visitStatusLabelColor = "bg-warning";
                        break;
                    case 5:
                        iconVisitStatus = "fa-" + eventObj.icon;
                        var localizedDescription = STRING_RESOURCES ? STRING_RESOURCES[eventObj.description] ?? eventObj.description : eventObj.description;
                        visitStatusText = visitStatusText;
                        visitStatusLabelColor = eventObj.backgroundColor;
                        eventObj.description = localizedDescription;
                        break;
                    case 6:
                        iconVisitStatus = "fa-calendar-alt";
                        visitStatusText = visitStatusText;
                        visitStatusLabelColor = eventObj.backgroundColor;
                        eventObj.description = eventObj.description;
                        break;
                }

                var html = '<td class="fc-list-item-time fc-widget-content">' + startEnd + '<BR><span style="align:center"></span></td>';
                html += '<td class="fc-list-item-marker fc-widget-content"><span style="color:' + color + '">' + typeicon + resources + '</span></td>';
                html += '<td class="fc-list-item-title fc-widget-content"><table style="width:100%;"><tr>';
                html += '<td style="padding:0"><b>' + label + ' ' + companyName + '</b>' + (eventObj.contactName ? ' (' + eventObj.contactName + ')' : '') + '</i></td>';
                html += '<td style="padding:0">' + resourcesStr + '</i></td>';
                html += '<td style="padding:0"><span class=""><i class="fa ' + visitTimeDifferenceIcon + '"></i> <span class="' + visitTimeDifferenceTextStyle + '">' + timeDifferenceDisplay + ' effectuées sur ' + plannedDurationForAllResources + ' prévues </span></span></td>';
                html += '<td style="padding:0;text-align:right"> <span class="label ' + visitStatusLabelColor + '"><i class="' + iconVisitStatus + '"></i> ' + visitStatusText + '</span></td>';
                html += '</tr></table></td>';

                $el.html(html);
                break;
        }

        var color = 'gray';

        $el.popover({
            html: true,
            title: popupTitle,
            content: buildPopupContent(eventObj, false, $el),
            trigger: 'hover',
            placement: 'top',
            container: 'body',
            template: '<div class="popover border-teal-400"><div class="arrow"></div><h3 class="popover-title" style="color:white;background-color:' + color + '"></h3><div class="popover-content"></div></div>'
        });

    }

    var createCalendarButtons = function (buttons) {

        var addOption = function (dest, id, text) {
            $('<li></li>')
               .html("<a href='javascript:void(0)' data-selection='" + id + "'>" + text + "</a>")
               .appendTo(dest); 
        }

        var buildMainButton = function (id, title, icon) {
            $('<div class="btn-group agenda-right-button" id="' + id + '" style="display:none"><button type="button" class="fc-corner-left fc-corner-right btn btn-primary dropdown-toggle legitRipple" data-toggle="dropdown" aria-expanded="false"><i class="' + icon + ' position-left"></i><span>' + title + '</span><span class="caret"></span></button><ul class="dropdown-menu dropdown-menu-right"></ul></div>')
                .appendTo('body');
        }

        

        if (buttons)
        {
            if (buttons.agenda && buttons.agenda.length) {
                buildMainButton(calendarId + 'customAgendaId', 'Agenda', 'icon-calendar');
                buttons.agenda.forEach(b=>addOption($('#' + calendarId + 'customAgendaId > ul'), b.id, b.name));
            }
        }
        else
        {
            buildMainButton(calendarId + 'customAgendaId', 'Agenda', 'icon-calendar');

            buttons = { agenda: [{ id: 'month', name: 'Mois' }, { id: 'agendaWeek', name: 'Semaine' }, { id: 'agendaDay', name: 'Jour' }, ] };
            buttons.agenda.forEach(b=>addOption($('#' + calendarId + 'customAgendaId > ul'), b.id, b.name));

            buttons = undefined;
        }

        if (buttons) {
            if (buttons.list && buttons.list.length) {
                buildMainButton(calendarId + 'customListId', 'Liste', 'icon-list2');
                buttons.list.forEach(b => addOption($('#' + calendarId + 'customListId > ul'), b.id, b.name));
            }
        }
        else {
            buildMainButton(calendarId + 'customListId', 'Liste', 'icon-list2');

            buttons = { list: [{ id: 'listYear', name: 'Année' }, { id: 'listMonth', name: 'Mois' }, { id: 'listWeek', name: 'Semaine' }, { id: 'listDay', name: 'Jour' },] };
            buttons.list.forEach(b => addOption($('#' + calendarId + 'customListId > ul'), b.id, b.name))
            buttons = undefined;
        };
    };

    var templatingEngine = function (template, object) {
        var temp = template;
        var left = "";

        while (temp.indexOf('{{') >= 0) {
            var i = temp.indexOf('{{');
            var j = temp.indexOf('}}');

            left += temp.substring(0, i);

            if (j >= 0) {
                var variable = temp.substring(i + 2, j).trim();
                try {
                    var value = object[variable];
                    if (value)
                        left += value;
                }
                catch (e) { }
            }

            temp = temp.substring(j + 2);
        }

        left += temp;

        return left;
    }

    var select2IsInitialised = function (id) {
        return ($(".bootbox-body ." + id)[0].className.indexOf("select2") > -1)
    }

    var buildSelectedDaysString = function () {
        let str = "";
        let days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
        let daysLocal = ['lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche'];

        for (let i = 0; i < days.length; i++)
            if ($(".bootbox-body .calendar-" + days[i]).hasClass('checked'))
                str += daysLocal[i] + ", ";

        if (str.endsWith(", ")) {
            str = str.substring(0, str.length - 2);
        }

        if (str === "") {
            $('.bootbox-body .days-label-no-day').show();
            $('.bootbox-body .days-label').text("");
        }
        else {
            $('.bootbox-body .days-label-no-day').hide();
            $('.bootbox-body .days-label').text(str);
        }
    }

    var handleCalendarClick = function (e) {
        var item = $(e.target).parent();
        item.toggleClass('checked');

        buildSelectedDaysString();
    }

    var isRecurring = function(event){
        return event && event.numrepeat !== undefined && event.numrepeat > 0
    }

    var initDialog = function (originalEvent, start, date) {

        var cannotChangeModificationApplication = false;

        applyChangesToAllEvents = false;

        var calendarDaySelectionPannel1 = (new calendarDaySelectionPannel()).init(originalEvent, start);

        //init type of meeting section
        var typeOfMeeting = new typeOfMeetingSelect('typeOfMeetingSelect', 'fr');
        var internalServiceSelect = new internalServicesSelect('internalServicesSelect', 'fr', aInternalServices);

        //init recurring checkbox
        var recurringCheckBox = new styledCheckbox('.recurring', function () {
            if (isRecurring(originalEvent))
                $('.bootbox-body .calendar-repeat-edit').hide();
            else
                $('.bootbox-body .calendar-repeat-edit').show();
         
            nonRecurringCheckBox.uncheck();
        });

        var clientsSelect = new clientSelect(clients);
        var statuses = [{ text: 'Pas commencé', id: 4 },
                        { text: 'En cours', id: 0 },
                        { text: 'Terminé', id: 1 },
                        { text: 'Annulé', id: 3 },
                        { text: 'Reporté', id: 2 }];

        var statutSelect = new statusSelect(statuses);
        statutSelect.setValue(originalEvent ? originalEvent.visitStatus : 4);

        // $.ajax({
        //     type: "GET",
        //     contentType: "application/json; charset=utf-8",
        //     url: '../../API/Service.svc/GetVisitDescription', 
        //     data: jobId,
        //     success: function (response) {
        //         $('.meetingDescription').text(response);
        //     }
        // });
        
        //init non recurring checkbox
        var nonRecurringCheckBox = new styledCheckbox('.non-recurring', function () {

            if (isRecurring(originalEvent)) return false;

            $('.bootbox-body .calendar-repeat-edit').hide();;

            recurringCheckBox.uncheck();
        });

        
        var resourcesSelect = new resourceSelect('.resourceSelect', false, function (e, selection) {
            
            if ($(selectResourcesId).length > 0)
            {
                var needRefresh = false;
                var currentSelection = $(selectResourcesId).select2('data');
                for(var i = 0; i<selection.length; i++)
                {
                    var found = false;
                    for(var j = 0; j<currentSelection.length; j++)
                        if(selection[i].id == currentSelection[j].id)
                        {
                            found = true;
                            needRefresh = true;
                            break;
                        }

                    if (!found)
                        currentSelection.push(selection[i]);
                }
                
                if (needRefresh)
                {
                    var newSelection = [];
                    currentSelection.forEach(s => newSelection.push(s.id));

                    $(selectResourcesId).val(newSelection);
                    $(selectResourcesId).trigger('change');
                }
            }
        });
      
        //initialises datepicker for start date
        new datePick('start-date', localisedDateFormat);

        //initialises datepicker for end date
        new datePick('end-date', localisedDateFormat);

        // time pickers
        // editable true désactive complètement le controle, c'est couillon
        //$('.start-time').pickatime({ format: 'H:i', interval: 15, editable: true });
        //$('.end-time').pickatime({ format: 'H:i', interval: 15, editable: true });


        $('.start-time').blur(function () {
            checkHHMM($(this));
        });
        $('.end-time').blur(function () {
            checkHHMM($(this));
        });
        $('.start-time').mask('00:00');
        $('.end-time').mask('00:00');

        //initialises day checkboxes
        var days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
      

        if (originalEvent !== undefined) {
            //executed if we are not creating a new visit
          
            //preselects the resources from the visit
            if (originalEvent.resources) {
                resourcesSelect.setValue(originalEvent.resources);
                resourcesSelect.setColors(resourcesSelect.getValue());
            }
            else {
                resourcesSelect.setColors($(selectResourcesId).select2('data'));
            }

            //existing event
            if (originalEvent.visitType != null) {
                //sets the visittype from the visit
                typeOfMeeting.setValue(originalEvent.visitType);

                if (originalEvent.visitType == 0)
                    internalServiceSelect.setValue(originalEvent.internalServiceId);
            }

            if (isRecurring(originalEvent)) {
                //initialises dialog when called from a repeating event that gets edited
                recurringCheckBox.triggerClick();
                nonRecurringCheckBox.disable();
            }
            else
            {
                $(".bootbox-body .radio-type-of-recurring").show();
                //set the 'non-recurring' checkbox to selected
                nonRecurringCheckBox.triggerClick();
               
                //enable 'recurring' and 'non-recurring' checkboxes
                recurringCheckBox.enable();
                nonRecurringCheckBox.enable();

                //hides read only calendar-repeat section and shows edit calendar-repeat sections

                buildSelectedDaysString();
            }
       
            if (clients.length > 0) {
                //initialises the client list with the first from the list
                var clientId = originalEvent.clientId != 0 ? originalEvent.clientId : clients[0].id;

                clientsSelect.setValue(clientId);

                //creates a dummy option in the job list to hold the client's job from the visit
                if (originalEvent.jobId != 0) {
                    var optionDescription = originalEvent.jobDescription.length == 0 ? originalEvent.jobInternalNumber : originalEvent.jobInternalNumber + " > " + originalEvent.jobDescription;
                    var newOption = new Option(optionDescription, originalEvent.jobId, true, true);
                    $(".bootbox-body .jobSelect").append(newOption);
                }

                //initialises the job select for the client using AJAX
                var jobsSelect = new jobSelect();
                jobsSelect.initAjax(clientId)
            }
            else {
                //the list of client's is empty, disable the list
                clientsSelect.disable();
               
                //same for job list, create and disable
                (new jobSelect([])).disable();
            }
        }
        else {

            //this is a new event not an edition 
            //set the 'non-recurring' checkbox to selected
            nonRecurringCheckBox.triggerClick();
               
            //hides read only calendar-repeat section and shows edit calendar-repeat sections

            buildSelectedDaysString();

            //select the first client from the list
            if (clients.length > 0) {
                clientsSelect.setValue(clients[0].id);
            } else {
                if (select2IsInitialised('jobSelect')) {
                    $(".bootbox-body .jobSelect").select2('destroy');
                    $(".bootbox-body .jobSelect").html('');
                }

                (new jobSelect()).init([]).disable();
            }

            //preselect resources or set the list to no selection
           /* var selection = preSelectedResource ? preSelectedResource : $(selectResourcesId).select2('data');

            var res = [];

            if (selection)
                selection.forEach(r=>res.push(r.id));

            resourcesSelect.setValue(res);
            resourcesSelect.setColors(selection);*/
        }
        //hide client section
      
        switch (screen) {
            case 0: //planning
                $('.bootbox-body .type-of-meeting-and-internal-select-panel').show();
                if (originalEvent !== undefined && originalEvent.visitType != 0) $('.bootbox-body .dialogClientSelection').show();
                break;
            case 1: //job edit
                $('.bootbox-body .type-of-meeting-and-internal-select-panel').hide();
                $('.bootbox-body .dialogClientSelection').hide();
                break;
            case 2: //holidays edit
                $('.bootbox-body .type-of-meeting-and-internal-select-panel').hide();
                $('.bootbox-body .dialogClientSelection').hide();
                $('.bootbox-body .calendar-repeat-edit').hide();
                $('.bootbox-body .radio-type-of-recurring').hide();
                break;
        }
    }

    var buildPopupContent = function (eventObj, showButtons, $el) {
        var aHours = [];
        var hours = "";

        if (eventObj.start !== null)
            hours += eventObj.start.format("HH:mm");

        if (eventObj.end !== null)
            hours += ' - ' + eventObj.end.format("HH:mm");

        if (hours == "00:00 - 00:00")
            hours = "all day";

        $el.attr("id", "ev_" + eventObj.id);

        var popoverDescription = '<div class="popover-content">';
        var title = eventObj.title;

        switch (eventObj.visitType) {
            case 2:
                typeicon = '<i class="fa fa-eye-slash position-left small-icon-calendar-popup"></i>';
                title = "Privé";
                break;
            case 1:
                typeicon = '<i class="fa fa-calendar position-left small-icon-calendar-popup"></i>';
                break;
            case 0:
                typeicon = '<i class="fa fa-briefcase position-left small-icon-calendar-popup"></i>';
                title = "Interne";
                break;
            case 5:
                var localizedDescription = STRING_RESOURCES ? STRING_RESOURCES[eventObj.description] ?? eventObj.description : eventObj.description;
                typeicon = '<i class="fa fa-' + eventObj.icon + ' position-left  small-icon-calendar-popup"></i>';
                title = localizedDescription;
                eventObj.description = localizedDescription;
                break;
            case 6:
                typeicon = '<i class="fa fa-calendar-alt position-left  small-icon-calendar-popup"></i>';
                title = eventObj.description;
                break;
        }

        popoverDescription += '<div class="eventPopupHeader"><h5> ' + typeicon + title + '</h5></div>';
        popoverDescription += '<div class="eventPopupContainer eventPopupPaddingBottom">' +
                                            '<i class="fa fa-clock-o"></i>' +
                                            '<div><i>' + hours + '</i></div>' +
                                        '</div>';
        var adrs = "";
        adrs += (eventObj.addressLine1 !== null ? eventObj.addressLine1 : "");
        adrs += (eventObj.addressLine2 !== null ? (adrs !== "" ? "<BR>" + eventObj.addressLine2 : eventObj.addressLine2) : "");
        adrs += (eventObj.addressLine3 !== null ? (adrs !== "" ? "<BR>" + eventObj.addressLine3 : eventObj.addressLine3) : "");
        adrs += (eventObj.addressLine4 !== null ? (adrs !== "" ? "<BR>" + eventObj.addressLine4 : eventObj.addressLine4) : "");

        if (adrs !== "") popoverDescription += '<div class="eventPopupContainer eventPopupPaddingBottom">' +
                                        '<i class="fa fa-map-marker"></i>' +
                                        '<div class="">' + adrs + '</div>' +
                                        '</div>';
        if (eventObj.phone !== null) popoverDescription +=
                                    '<div class="eventPopupContainer eventPopupPaddingBottom">' +
                                        '<i class="fa fa-phone"></i>' +
                                        '<div>' + eventObj.phone + '</div>' +
                                    '</div>';
        if (eventObj.resourcesStr !== null) {
            var tmp = eventObj.resourcesStr.split(',');
            var icon = (tmp.length > 1 ? '<i class="fa fa-users"></i>' : '<i class="fa fa-user"></i>');

            popoverDescription +=
                                   '<div class="eventPopupContainer eventPopupPaddingBottom">' +
                                        icon + '<div>' + eventObj.resourcesStr + '</div>' +
                                   '</div>';
        }

        if (eventObj.description !== null) popoverDescription +=
                                    '<div class="noLeftPadding">' +
                                        '<div>' + eventObj.description + '</div>' +
                                    '</div>';
        popoverDescription += '</div>';

        if (showButtons)
            popoverDescription += '<div>' +
                            '<button onclick="eventClosePopupClickBtnHandler(event)" data-event="ev_' + eventObj.id + '" type="button" class="btn btn-default legitRipple">' +
                                '<i class="fa fa-close position-left"></i> Fermer</button>&nbsp;' +
                            '<button onclick="eventEditClickBtnHandler(event)" data-event="ev_' + eventObj.id + '" type="button" class="btn btn-default legitRipple">' +
                                '<i class="fa fa-edit position-left"></i> Modifier</button>' +
                        '</div>';

        return popoverDescription;
    }

    var syncResourcesSelects = function (resources) {

        if (resources == undefined || !selectResourcesId)
            return;

        var res = $(selectResourcesId).select2('data');
        var resToSelect = $(selectResourcesId).val();

        if (resToSelect == null || resToSelect == undefined)
            resToSelect = [];

        resources.split(',').forEach(r1 => {
            var contains = false;

            if (r1 != "")
            {
                res.forEach(r2 => {
                    if (r1 == r2.id)
                        contains = true;
                });

                if (!contains)
                    resToSelect.push(r1);
            }
        });

        $(selectResourcesId).val(resToSelect);
        $(selectResourcesId).trigger('change');
    }

    var saveChanges = function (data)
    {
        $.ajax({
            url: '../../API/Service.svc/UpdateVisit',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify(data),
            success: function (data) {
                $("."+calendarId).fullCalendar('refetchEvents');
                return true;
            },
            error: function (data) {
                swal({
                    confirmButtonText: 'ok',
                    type: 'error',
                    title: "Attention",
                    text: "Le système n'a pas été en mesure d'effectuer cette modification. Veuillez recommencer plus tard."
                });

                return false;
            },
        });
    };

    var deleteVisit = function (originalEvent, applyChangesToAllEvents) {
        $.ajax({
            url: '../../API/Service.svc/DeleteVisit',
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({
                id: originalEvent.id,
                applyToAllRepeat: applyChangesToAllEvents,
                repeatGroupId: originalEvent.repeatGroupId,
                numrepeat: originalEvent.numrepeat
            }),
            success: function (data) {
                $("."+calendarId).fullCalendar('refetchEvents');
            },
            error: function (data) {
                swal({
                    confirmButtonText: 'ok',
                    type: 'error',
                    title: "Attention",
                    text: "Le rendez-vous n'a pas pu être effacé. Veuillez recommencer plus tard."
                })
            }
        });
    }

    var eventEditClickBtnHandler = function (originalEvent, start, end, startTime, endTime) {
console.log("eventEditClickBtnHandler");
        var template = $("#modal_form_vertical2").html();

        var description = originalEvent ? originalEvent.description : "";
        if (originalEvent && originalEvent.visitType == 5) {
            // Localize description for absence
            description = STRING_RESOURCES ? STRING_RESOURCES[originalEvent.description] ?? originalEvent.description : originalEvent.description;
        }
      
        template = templatingEngine(template, {
            startDate: start, endDate: end, description: description,
            startTime: startTime, endTime: endTime, id: (originalEvent ? originalEvent.id : '0'),
            jobId: (originalEvent ? originalEvent.jobId : '0'), clientId: (originalEvent ? originalEvent.clientId : '0'),
            meetingDescription: originalEvent ? originalEvent.meetingDescription : ''
        });

        var buttons = (canChangeResourceSelection || !originalEvent || originalEvent.lastModifiedById == currentUserId) ? {
            close: {
                label: ressources2['Cancel'],
                className: "btn-info",
                callback: function (e) {

                }
            },
            delete: {
                label: ressources2['Delete']+"!",
                className: "bg-danger",
                callback: function (e) {
                    var strMsg = ressources2['DeleteAppointment'];

                    if (isRecurring(originalEvent)) {
                        swal({
                            type: 'warning',
                            title: "Attention",
                            html: "<span class='sweetalerttext'>" + ressources2['DeleteOnlyThisOrAll']+"</span>" +
                                "<br><br><div>" +
                                '<button type="button" role="button" tabindex="0" class="SwalBtn2 sweetalertbtn btn btn-primary legitRipple">' + ressources2['DeleteOnlyThisAppointment'] + '</button>' +
                                '<button type="button" role="button" tabindex="0" class="SwalBtn3 sweetalertbtn btn btn-primary legitRipple">' + ressources2['DeleteAllAppointments'] + '</button>' +
                                '<button type="button" role="button" tabindex="0" class="SwalBtn1 sweetalertbtn btn btn-default legitRipple">' + ressources2['Cancel']  + '</button>' +
                                '</div>',
                            showCancelButton: false,
                            showConfirmButton: false,
                            onOpen: function () {
                                $('.SwalBtn1').off();
                                $('.SwalBtn1').on('click', function () {
                                    swal.clickConfirm();
                                });
                                $('.SwalBtn2').off();
                                $('.SwalBtn2').on('click', function () {
                                    deleteVisit(originalEvent, false);
                                    swal.clickConfirm();
                                });
                                $('.SwalBtn3').off();
                                $('.SwalBtn3').on('click', function () {
                                    deleteVisit(originalEvent, true);
                                    swal.clickConfirm();
                                });
                            }
                        });
                    } else
                        swal({
                            type: 'warning',
                            title: "Attention",
                            text: strMsg,
                            textCancelButton: ressources2['Cancel'] ,
                            textConfirmButton: ressources2['Delete'],
                            showCancelButton: true,
                            showConfirmButton: true
                        }).then((response) => {
                            if (response.value == true)
                                deleteVisit(originalEvent, false);
                        });
                 }
            },
            main: {
                label: ressources2['Save'],
                className: "bg-success",
                callback: function (e) {

                    var errors = false;

                    $(".bootbox-body .error_missing_resources").addClass("hidden");
                    $(".bootbox-body .error_enddate_before_startdate").addClass("hidden");

                    var res = $(".bootbox-body .resourceSelect").select2('data');
                    var aRes = "";

                    res.forEach(r => aRes += r.id + ",");

                    if (aRes == "") {
                        $(".bootbox-body .error_missing_resources").removeClass("hidden");
                        errors = true;
                    }

                    var strDays = "";
                    var numRepeats = 0;

                    if (originalEvent && originalEvent.numrepeat > 0)
                        numRepeats = originalEvent.numrepeat;
                    else
                        numRepeats = $(".bootbox-body .repeat-visit-num").val() != "" ? $(".bootbox-body .repeat-visit-num").val() : 0;

                    if ($('.bootbox-body .recurring').hasClass('checked')) {
                        if (originalEvent && originalEvent.numrepeat > 0) {
                            strDays = originalEvent.repeatdays;
                    
                        } else if (numRepeats > 0) {
                            var days = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];

                            for (var i = 0; i < days.length; i++)
                                if ($(".bootbox-body .calendar-" + days[i]).hasClass('checked'))
                                    strDays += (i + 1) + ",";

                            if (strDays != "")
                                strDays = strDays.substring(0, strDays.length - 1);

                            if (strDays == "") {
                                $(".bootbox-body .error_no_repeating_day_selected").removeClass("hidden");
                                errors = true;
                            };
                        }
                        else {
                            $(".bootbox-body .error_no_duration_selected").removeClass("hidden");
                            errors = true;
                        }
                    }

                    var changes = {
                        id: (originalEvent ? originalEvent.id : "-1"),
                        type: (preselectedJobId ? 1 : $('.bootbox-body .typeOfMeetingSelect').select2('data')[0].id),
                        clientId: (preselectedClientId ? preselectedClientId : ($('.bootbox-body .typeOfMeetingSelect').select2('data')[0].id == 1 ? $('.bootbox-body .clientSelect').select2('data')[0].id : "-1")),
                        jobId: (preselectedJobId ? preselectedJobId : ($(".bootbox-body .jobSelect").select2('data').length > 0 ? $(".bootbox-body .jobSelect").select2('data')[0].id : "-1")),
                        startDate: moment($(".bootbox-body .start-date").val(), localisedDateFormat).format(),
                        endDate: moment($(".bootbox-body .end-date").val(), localisedDateFormat).format(),
                        startTime: $(".bootbox-body .start-time").val(),
                        endTime: $(".bootbox-body .end-time").val(),
                        resources: aRes,
                        description: $(".bootbox-body .description").val(),
                        culture: currentLanguage,
                        status: $('.bootbox-body .statusSelect').select2('data')[0].id,
                        numrepeat: $('.bootbox-body .recurring > input').hasClass('checked') ? 0 : numRepeats,
                        repeatGroupId: $('.bootbox-body .recurring > input').hasClass('checked') ? "" : (originalEvent ? originalEvent.repeatGroupId : ""),
                        repeatdays: $('.bootbox-body .recurring > input').hasClass('checked') ? '' : strDays,
                        applyToAllRepeat: applyChangesToAllEvents,
                        internalServiceId: (preselectedJobId ? "-1" : ($('.bootbox-body .typeOfMeetingSelect').select2('data')[0].id == 0 ? $('.bootbox-body .internalServicesSelect').select2('data')[0].id : "-1"))
                    };
           
                    var startDateTime = moment($(".bootbox-body .start-date").val() + ' ' + changes.startTime, localisedDateFormat + " HH:mm");
                    var endDateTime = moment($(".bootbox-body .end-date").val() + ' ' + changes.startTime, localisedDateFormat + " HH:mm");

                    if (endDateTime.isBefore(startDateTime)) {
                        $(".bootbox-body .error_enddate_before_startdate").removeClass("hidden");
                        errors = true;
                    }

                    if (!preselectedJobId && $(".bootbox-body .jobSelect").select2('data').length == 0 && $(".bootbox-body .typeOfMeetingSelect").select2('data')[0].id == 1) {
                        $(".bootbox-body .error_no_job_selected").removeClass("hidden");
                        errors = true;
                    }

                    if (!errors) {
                        if (isRecurring(originalEvent)) {
                            console.log(changes)
                            swal({
                                type: 'warning',
                                title: "Attention",
                                html: "<span class='sweetalerttext'>Voulez-vous appliquer ces changements uniquement à ce rendez-vous ou à tous les rendez-vous récurrents prévus après aujourd'hui ?</span>" +
                                    "<br><br><div>" +
                                    '<button type="button" role="button" tabindex="0" class="SwalBtn2 sweetalertbtn btn btn-primary legitRipple">' + getMessageTextByAction(changes.status, false) + '</button>' +
                                    '<button type="button" role="button" tabindex="0" class="SwalBtn3 sweetalertbtn btn btn-primary legitRipple">' + getMessageTextByAction(changes.status, true) + '</button>' +
                                    '<button type="button" role="button" tabindex="0" class="SwalBtn1 sweetalertbtn btn btn-default legitRipple">' + ressources2['Cancel'] + '</button>' +
                                    '</div>',
                                showCancelButton: false,
                                showConfirmButton: false,
                                onOpen: function () {
                                    $('.SwalBtn1').off();
                                    $('.SwalBtn1').on('click', function () {
                                        swal.clickConfirm();
                                    });
                                    $('.SwalBtn2').off();
                                    $('.SwalBtn2').on('click', function () {
                                        changes.applyToAllRepeat = false;
                                        saveChanges(changes);
                                        syncResourcesSelects(changes.resources);
                                        swal.clickConfirm();
                                    });
                                    $('.SwalBtn3').off();
                                    $('.SwalBtn3').on('click', function () {
                                        changes.applyToAllRepeat = true;
                                        saveChanges(changes);
                                        syncResourcesSelects(changes.resources);
                                        swal.clickConfirm();
                                    });
                                }
                            });
                        } else
                        {
                            saveChanges(changes);
                            syncResourcesSelects(changes.resources);
                        }
                    }
                    else
                        return false;
                }
            }
        } : {
            close: {
                label: ressources2['Cancel'] ,
                className: "btn-info",
                callback: function (e) {

                }
            }

        };

        if (originalEvent != undefined && originalEvent.id != '-1')
            bootbox.dialog({
                message: template,
                backdrop: false,
                closeButton: false,
                buttons: buttons
            });
        else
            bootbox.dialog({
                message: template,
                backdrop: false,
                closeButton: false,
                buttons: { close:buttons.close, main: buttons.main }
            });

        initDialog(originalEvent, moment(start, localisedDateFormat));

        $('.bootbox-body .daterangepicker').css("z-index", "10000");

    };
    function getMessageTextByAction(action, all) {
        var res='';
        switch (action) {
        case '0': //En cours
                res = all ? ressources2['InProgressAllAppointments'] : ressources2['InProgressOnlyThisAppointment'];
            break;
        case '1': //Terminé
                res = all ? ressources2['TerminateAllAppointments'] : ressources2['TerminateOnlyThisAppointment'];
            break;
        case '2': //Reporté
                res = all ? ressources2['PostponeAllAppointments'] : ressources2['PostponeOnlyThisAppointment'];
            break;
        case '3': //Annulé
            res = all ? ressources2['CancelAllAppointments'] : ressources2['CancelOnlyThisAppointment'];
                break;
            case '4': //Pas commencé
                res = all ? ressources2['NotStartedAllAppointments'] : ressources2['NotStartedOnlyThisAppointment'];
            break;
            
        }

        return res;
    }
    function selectAllResources(e) {
       e &&  e.preventDefault();
        
       
            var res = [];
            resources.forEach(r => res.push(parseInt(r.id)));
            $(selectResourcesId).val(res);
            $(selectResourcesId).trigger('change');
        
        
    }

    var init = function init(options) {

        var that = this;

        aInternalServices = options.internalServices != undefined ? options.internalServices : [];
        viewEventsForAllJobs = options.viewEventsForAllJobs;
        officialHollidays = options.officialHollidays;
        opacifyAllEventsButForJobId = options.opacifyAllEventsButForJobId;
        screen = options.screen;
        resourcesTable = options.resources;
        calendarId = options.calendarId;
        canChangeResourceSelection = options.canChangeResourceSelection;
        preselectedJobId = options.preselectedJobId;
        preselectedClientId = options.preselectedClientId;
        aspectRatio = options.aspectRatio ? options.aspectRatio : aspectRatio;
        clients = options.clients != undefined ? options.clients : [];
        excludeAbsenceRequests = options.excludeAbsenceRequests != undefined ? options.excludeAbsenceRequests : false;

        var renderer = options.renderer;
        var buttons = options.buttons;
        selectResourcesId = options.resourcesListId;
        var ppreselectedResources = options.preSelectedResources;
        var defaultView = options.defaultView;

        if (!renderer)
            eventRenderer = renderer;

        createCalendarButtons(buttons);

        window.eventClosePopupClickBtnHandler = function (event) {
            $("#" + event.target.dataset.event).popover("hide");
        };

        if (selectResourcesId)
        {
            $(selectResourcesId).select2({
                minimumResultsForSearch: Infinity,
                data: resourcesTable,
            });
            
            $(selectResourcesId).on('change', function (e) {
                $(selectResourcesId).select2('close');
                $(selectResourcesId).select2('data').forEach(element => {
                    var query = $(".select2-selection__choice[title='" + element.title + "']");

                    if (query.length > 0)
                        query.css({ "background-color": element.color });
                });

                if (isInitialised)
                    $('.'+calendarId).fullCalendar('refetchEvents');
            });

            $('.select-all-resources').on("click", function (e) {
                

                selectAllResources(e);
                
            });

            $('.clear-all-resources').on("click", function (e) {
                e.preventDefault();
                $(selectResourcesId).val([]);
                $(selectResourcesId).trigger('change');
            }); 
        }

        if (ppreselectedResources)
        {
            if(selectResourcesId)
            {
                $(selectResourcesId).val(ppreselectedResources);
                $(selectResourcesId).trigger('change');
            }
            else {

                if ($('.select-resources-section').length > 0)
                    $('.select-resources-section').hide();

                preSelectedResource = [];
                ppreselectedResources.forEach(function (resId) {
                    for(var i = 0; i<resourcesTable.length; i++)
                    {
                        if (resourcesTable[i].id == resId) {
                            preSelectedResource.push(resourcesTable[i]);
                            break;
                        }
                    }
                });

                if (isInitialised)
                    $('.'+calendarId).fullCalendar('refetchEvents');
            }
        }

        $.get('CalendarDialog.html?v=2', function (data) {
            $(data).appendTo('body');
            if (allowEmployeeViewAllResources) {
                selectAllResources();
            }
            $('.' + calendarId).fullCalendar(jQuery.extend(
                {
                customButtons: {
                    listCustomButton: {
                        click: function (e) {
                            var sel = $(e.target).data('selection');
                            if (sel) {
                                $('.'+calendarId).fullCalendar('changeView', sel);
                            }
                        }
                    },
                    agendaCustomButton: {
                        click: function (e) {
                            var sel = $(e.target).data('selection');
                            if (sel) {
                                $('.'+calendarId).fullCalendar('changeView', sel);
                            }
                        }
                    },
                },
                header: {
                    left: 'prev,next today ',
                    center: 'title',
                    right: 'agendaCustomButton, listCustomButton'
                },
                views: {
                    month: { buttonText: 'Mois', timeFormat: 'H(:mm)t' },
                    agendaWeek: { buttonText: 'Semaine', timeFormat: 'HH(:mm)', },
                    agendaDay: { buttonText: 'Jour', timeFormat: 'HH(:mm)' },
                    listYear: { buttonText: 'Liste année', timeFormat: 'HH:mm' },
                    listMonth: { buttonText: 'Liste mois', timeFormat: 'HH:mm' },
                    listWeek: { buttonText: 'Liste semaine', timeFormat: 'HH:mm' },
                    listDay: { buttonText: 'Liste jour', timeFormat: 'HH:mm' },
                },
                defaultDate: defaultDate,
                eventStartEditable: false,
                eventDurationEditable: false,
                eventResourceEditable : false,
                displayEventTime: true,
                displayEventEnd: true,
                eventClick: function (calEvent, jsEvent, view) {
                    if (calEvent.visitType == 5) {
                        return;
                    }
                    if (opacifyAllEventsButForJobId && opacifyAllEventsButForJobId !== calEvent.jobId)
                        return false;
                    var start = (calEvent.start != null ? calEvent.start.format('L') : moment().format('L'));
                    var end = (calEvent.end != null ? calEvent.end.format('L') : start);
                    var startTime = (calEvent.start != null ? calEvent.start.format('HH:mm') : moment().format('HH:mm'));
                    var endTime = (calEvent.end != null ? calEvent.end.format('HH:mm') : startTime);
                    $(jsEvent.currentTarget).popover('hide');
                    eventEditClickBtnHandler(calEvent, start, end, startTime, endTime);
                    jsEvent.preventDefault();
                },
                businessHours: standardBusinessHours,
                defaultView: defaultView ? defaultView : 'month',
                weekNumbersWithinDays: true,
                weekNumbers: true,
                lazyFetching: true,
                eventLimit: 10,
                allDaySlot: true,
                allDayText:"Journée", 
                dayClick: addOrEditEvent,
                aspectRatio: aspectRatio,
                eventRender: renderer,
                loading: function (load) {
                    if (load)
                        $('.'+calendarId).block({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'none'
                            }
                        });
                    else
                        $('.'+calendarId).unblock();
                },
                editable: true,
                resourceLabelText: 'Collaborateurs',
                slotEventOverlap: false,
                events: function (start, end, timezone, callback) {
                    $('.'+calendarId).block({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'none'
                        }
                    });

                    var data = {
                        start: start.format('YYYY/MM/DD HH:mm'),
                        end: end.format('YYYY/MM/DD HH:mm'),
                        resources: [],
                        jobId: !viewEventsForAllJobs && preselectedJobId ? preselectedJobId : null,
                        cultureCode: lang4 ? lang4 : "",
                        excludeAbsenceRequests: excludeAbsenceRequests
                    };
                    if ($(selectResourcesId).length > 0) {
                        var selectedResourcesRaw = $(selectResourcesId).select2('data');
                        var selectedResources = [];
                        if (selectedResourcesRaw)
                            selectedResourcesRaw.forEach(r=>selectedResources.push({ id: r.id, color: r.color }));
                        data.resources = selectedResources;
                    } else if (preSelectedResource)
                        data.resources = preSelectedResource;
                    if (typeof preselectedVisitId === 'undefined'){
                        preselectedVisitId = 0;
                    }
                    
                    $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: '../../API/Service.svc/Getvisits',
                        dataType: "json",
                        data: JSON.stringify(data),
                        success: function (data) {
                            $('.'+calendarId).unblock();

                            if (data) {
                                callback(data);
                                
                                let $visit = $(`#ev_${preselectedVisitId}`);
                                if ($visit.length > 0){
                                    $visit.click();
                                    preselectedVisitId = 0;
                                }
                            }
                            else
                                callback([]);
                        },
                        error: function () {
                            $('.'+calendarId).unblock();
                            alert('there was an error while fetching events!');
                        },
                    });
                }

                },
                fullCalendarLicenseKey));

            isInitialised = true;

            if (officialHolidays)
                $("." + calendarId).fullCalendar('addEventSource', officialHolidays);

            if (calendarEvents) {
                $("." + calendarId).fullCalendar('addEventSource', calendarEvents);
            }

            if ($("#" + calendarId + "customListId").length > 0)
                $("." + calendarId + " .fc-listCustomButton-button").html($("#" + calendarId + "customListId").html());
            else
                $("." + calendarId + " .fc-listCustomButton-button").hide();

            $("." + calendarId + " .fc-listCustomButton-button").css("padding", "0px");

            if ($("#" + calendarId + "customAgendaId").length > 0)
                $("." + calendarId + " .fc-agendaCustomButton-button").html($("#" + calendarId + "customAgendaId").html());
            else
                $("." + calendarId + " .fc-agendaCustomButton-button").hide();

            $("." + calendarId + " .fc-agendaCustomButton-button").css("padding", "0px");
        })
       
        return this;
    }

    var addOrEditEvent = function (date) {
        eventEditClickBtnHandler(undefined, date.format('L'), date.format('L'),
                                        moment().format('HH:mm'), moment().add(1, 'h').format('HH:mm'));
    }

    return {
        addOrEditEvent:addOrEditEvent,
        init: init,
        eventRenderer: eventRenderer,
        refresh: function () { if (isInitialised) $('.' + calendarId).fullCalendar('refetchEvents'); }
    }
};