﻿var clientSelect = function (clients, options) {
    var _id = ".bootbox-body .clientSelect";

    var select2IsInitialised = function (id) {
        return ($(".bootbox-body ." + id).length == 1 ? 
            $(".bootbox-body ." + id)[0].className.indexOf("select2") > -1 : false)
    };

    var init = function () {
        if (!select2IsInitialised()) {
            $(_id).select2({
                closeOnSelect: true,
                language: 'fr',
                multiple: false,
                placeholder: 'client',
                tags: false,
                minimumResultsForSearch: Infinity,
                data: clients
            });

            //client selection handler, allows to link client and jobs together using Ajax
            var onChangeHandler = options && options.onChangeHandler && typeof (options.onChangeHandler == 'function') ? options.onChangeHandler : null;
            $(_id).on("change", onChangeHandler ? onChangeHandler : function (e) {
                var selectedClient = $(_id).select2('data');
                var clientId = selectedClient[0].id;

                $(".bootbox-body .jobSelect").off('select2:select');

                if (select2IsInitialised('jobSelect')) {
                    $(".bootbox-body .jobSelect").select2('destroy');
                    $(".bootbox-body .jobSelect").html('');
                }

                $(".bootbox-body .jobSelect").select2({
                    closeOnSelect: true,
                    language: 'fr',
                    multiple: false,
                    placeholder: 'choisissez un projet',
                    tags: false,
                    minimumResultsForSearch: Infinity,
                    ajax: {
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        url: '../../API/Service.svc/GetJobsForClient',
                        dataType: 'json',
                        data: clientId,
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        }
                    }
                });
            });
        }
        return this;
    };

    init();

    return {
        init: init,
        getValue: function () {
            return $(_id).select2('data');
        },
        setValue: function (visitType) {
            $(_id).val(visitType);
            $(_id).trigger('change');

            return this;
        },
        distroy: function () {
        },
        disable: function () {
            $(_id).prop("disabled", true);
        },
        setColors: function (selection) {
            selection.forEach(function (r) {
                var query = $(".bootbox-body .select2-selection__choice[title='" + r.title + "']");

                if (query.length > 0)
                    query.css({ "background-color": r.color });
            });
        }
    }
}