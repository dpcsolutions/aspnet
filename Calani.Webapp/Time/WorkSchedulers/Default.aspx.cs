﻿using Calani.BusinessObjects.Calendars;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.WorkSchedulers;
using System;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

public partial class Settings_Profiles_Default : System.Web.UI.Page
{
    public PageHelper ph;
    public static long CurrentProfileId { get; set; }

    private static WorkScheduleManager mngr;
    private static WorkScheduleRecManager mngrRec;
    private static CalendarsManager mngrCalendars;
    public string CalendarEvents { get; set; }
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        CalendarEvents = "[]";

        ph.ItemLabel = Resources.Resource.Work_Schedule;
       
        mngr = new WorkScheduleManager(ph.CurrentOrganizationId);
        mngrRec = new WorkScheduleRecManager(ph.CurrentOrganizationId);
        mngrCalendars = new CalendarsManager(ph.CurrentOrganizationId);
        
        if (!IsPostBack)
        {
            ddlCalendars.DataSource = mngrCalendars.GetCalendarsDtos();
            ddlCalendars.DataBind();

            var data = mngr.GetScheduleDtos();
            listProfiles.DataSource = data;
            listProfiles.DataBind();
            ProfileRecords.Visible = data != null && data.Count > 0;
            if (CurrentProfileId < 1)
            {
                var item = data != null && data.Any() ? data.FirstOrDefault():null;

                CurrentProfileId = item!=null?item.Id:0;
                SelectedProfileName.Text = item != null ? item.Name:String.Empty;
            }

            var dto = data.FirstOrDefault(i => i.Id == CurrentProfileId);
            SelectedProfileName.Text = dto != null ? dto.Name : String.Empty;
            if (dto != null)
            {
                foreach (var calendar in dto.Calendars)
                {
                    var ddlItem = ddlCalendars.Items.FindByValue(calendar.Id.ToString());
                    if (ddlItem != null)
                        ddlItem.Selected = true;
                }
            }
        }
        if (CurrentProfileId > 0)
        {
            listProfileRecords.Visible = true;
            var recData = mngrRec.GetScheduleRecordsDto(CurrentProfileId);
            listProfileRecords.DataSource = recData;
            listProfileRecords.DataBind();
            btnDuplicate.Visible = true;
            var events = mngrRec.ConvertToCalendarEvents(recData);
            CalendarEvents = new JavaScriptSerializer().Serialize(events);
        }
        Title = ph.CMP.Name;
    }
    
    protected void bSaveProfile_OnClick(object sender, EventArgs e)
    {
        int id = 0; 
        id = Convert2.StrToInt(ProfileId.Value, id);
        var name = ProfileName.Value;
        var code = ProfileCode.Value;
        if(String.IsNullOrWhiteSpace(name)|| String.IsNullOrWhiteSpace(code))
            Response.Redirect(Request.Url.PathAndQuery);

        var profile = new Calani.BusinessObjects.Model.work_schedule_profiles()
        {
            name = name,
            code = code,
            organization_id = ph.CurrentOrganizationId,
        };
        SmartAction<work_schedule_profiles> ret;

        if (id < 1)
            ret = mngr.Add(profile);
        else
            ret = mngr.Update(id, profile);
        
        

        if (ret.Success)
        {
            CurrentProfileId = ret.Record.id;
            

            var data = mngr.GetScheduleDtos();

            listProfiles.DataSource = data;
            listProfiles.DataBind();

            ProfileName.Value = String.Empty;
            ProfileCode.Value = String.Empty;
        }
        else ph.DisplayError(ret.ErrorMessage);

        Response.Redirect(Request.Url.PathAndQuery);
    }

    protected void btnSaveProfileRec_OnClick(object sender, EventArgs e)
    {
        int id = 0;
        id = Convert2.StrToInt(RecId.Value, id);

        var color = recColor.Value;
        var name = recName.Value;

        var start = recStart.Value;

        var end = recEnd.Value;

        int rate = 100;
        rate = Convert2.StrToInt(recRate.Value, rate);
        

        if (String.IsNullOrWhiteSpace(name) || String.IsNullOrWhiteSpace(start) || String.IsNullOrWhiteSpace(end) ||rate<1)
            return;
        
        var holidays = recHolidays.Checked;

        var mo = !holidays && this.recMo.Checked;
        var tu = !holidays && this.recTu.Checked;
        var we = !holidays && this.recWe.Checked;
        var th = !holidays && this.recTh.Checked;
        var fr = !holidays && this.recFr.Checked;
        var sa = !holidays && this.recSa.Checked;
        var su = !holidays && this.recSu.Checked;

        var rec = new work_schedule_profiles_rec()
        {
            profile_id = CurrentProfileId,
            color = color,
            name = name,
            start = start,
            end = end,
            rate = rate,
            mo = mo,
            tu = tu,
            we = we,
            th = th,
            fr = fr,
            sa = sa,
            su = su,
            type = (int)(holidays?TimeCategoryType.ScheduleHoliday:TimeCategoryType.ScheduleRule),
            organization_id = ph.CurrentOrganizationId
                        
        };

       

        SmartAction<work_schedule_profiles_rec> ret;

        if (id < 1)
            ret = mngrRec.Add(rec);
        else
            ret = mngrRec.Update(id, rec);

        if (ret.Success)
        {
            var data = mngrRec.GetScheduleRecordsDto(CurrentProfileId);

            listProfileRecords.DataSource = data;
            listProfileRecords.DataBind();
            RecId.Value = String.Empty;
            recName.Value = String.Empty;
            recStart.Value = String.Empty;
            recEnd.Value = String.Empty;
        }
        Response.Redirect(Request.Url.PathAndQuery);
    }

    protected void btnDuplicateProfile_Click(object sender, EventArgs e)
    {
        
        CurrentProfileId = mngr.DuplicateProfile(CurrentProfileId);

        Response.Redirect(Request.Url.PathAndQuery);
    }

    protected void btnDeleteProfile_Click(object sender, EventArgs e)
    {
        long id = 0;
        id = Convert2.StrToLong(ProfileId.Value, id);
        if (id > 0)
            mngr.Delete(id);
        Response.Redirect(Request.Url.PathAndQuery);

    }

    protected void btnDeleteRecord_Click(object sender, EventArgs e)
    {
        long id = 0;
        id = Convert2.StrToLong(RecId.Value, id);
        if (id > 0)
            mngrRec.Delete(id);

        Response.Redirect(Request.Url.PathAndQuery);
    }

    protected void ProfileItem_OnClick(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        CurrentProfileId = Convert2.StrToLong(btn.CommandArgument, CurrentProfileId);
        Response.Redirect(Request.Url.PathAndQuery);
    }

   
    protected void bttSelectedCalendars_OnClick(object sender, EventArgs e)
    {
       
       var items = from ListItem li in ddlCalendars.Items where li.Selected select Int64.Parse(li.Value);

        mngr.UpdateProfileCalendarsSelection(CurrentProfileId, items);
    }
}