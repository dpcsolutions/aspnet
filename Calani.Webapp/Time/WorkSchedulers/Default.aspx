﻿<%@ Page Title="Profiles" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Settings_Profiles_Default" Codebehind="Default.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    <%--Canceled Seubi css for page to apply theme style--%>
    <style type="text/css">
        .table > thead > tr > th {
            border-bottom: none;  
            background: none; 
            color: black;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    var calendarEvents = <%= CalendarEvents %>;
</script>
<form runat="server">
    <div class="content">
        <div class="row">
                <div class="col-lg-12">
                </div>
                <!-- column -->
			    <div class="col-lg-12">
                    
                    <asp:Panel runat="server" ID="Profiles" class="panel panel-flat" >
						<div class="table-responsive panel-body">

                            <asp:ListView runat="server" ID="listProfiles" >
                                <LayoutTemplate>
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th runat="server" class="col-lg-6"><%= Resources.Resource.Name %></th>
                                            <th runat="server" class="col-lg-4"><%= Resources.Resource.Code %></th>
                                            <th runat="server" class="col-lg-2"><%= Resources.Resource.Action %></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr runat="server" id="itemPlaceholder" ></tr>
                                        </tbody>
                                    </table>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr id='<%# Eval("Id") %>'>
                                        <td>
                                            <%-- Data-bound content. --%>
                                                
                                            <input type="text" id="lstProfileId" runat="server" hidden="" value='<%# Bind("Id") %>'/>

                                            <asp:LinkButton ID="NameLabel" runat="server" Text='<%#Eval("Name") %>' CssClass="text-left text-default"
                                                            OnClick="ProfileItem_OnClick" CommandArgument='<%#Eval("Id")%>'/>
                                        </td>
                                        <td >
                                            <%-- Data-bound content. --%>
                                            <asp:LinkButton ID="CodeLabel" runat="server"  runat="server" Text='<%#Eval("Code") %>' CssClass="text-left text-default"
                                                            OnClick="ProfileItem_OnClick" CommandArgument='<%#Eval("Id")%>'/>
                                        </td>
                                        <td>
                                            <ul class="icons-list">
                                                    
                                                    <%-- Data-bound content. --%>
                                                        
                                                <li class="text-primary-600">
                                                    <a runat="server" href="#" data-target="#modal_profile" onclick='<%# "editProfile(\""+ Eval("Id") + "\",\""+Eval("Name") +"\",\""+Eval("Code")+  "\");" %>'>
                                                        <i class="icon-pencil7"></i>
                                                    </a>
                                                </li>
                                                <li class="text-danger-600">
                                                    <a href="#" onclick='<%# "deleteProfile(\""+ Eval("Id") + "\",\""+Eval("AssignedToOrg")+"\");" %>'><i class="icon-trash"></i></a>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </ItemTemplate>
							</asp:ListView>
                            <br/>
                            <div class="col-lg-2 pull-left">
                                <a onclick=" editProfile(0, '', '')"  class="btn btn-default pull-left"><i class="icon-plus2 position-left"></i><%= Resources.Resource.CreateWorkSchedule %></a>
                            </div>
                            <div class="col-lg-2 pull-right">
                                <asp:LinkButton runat="server" ID="btnDuplicate" class="btn btn-primary pull-right" OnClick="btnDuplicateProfile_Click" Visible="False"> 
                                    <i class="icon-copy position-left"></i> <%= Resources.Resource.Duplicate %>
                                </asp:LinkButton>
                            </div>

						</div>
                    </asp:Panel>

                    <asp:Panel runat="server" ID="ProfileRecords" class="panel panel-flat" Visible="False" >
                        <div class="timeline-row panel panel-flat">
                        <div class="panel-heading">
                            <h6 class="panel-title text-semibold"><%= Resources.Resource.Profile %>: <asp:Label runat="server" ID="SelectedProfileName"></asp:Label></h6>
                        </div>
                       
                        <div class="panel-body">
                                <div class="col-lg-12">
                                    <label class="col-form-label col-lg-1"><%= Resources.Resource.Calendars %>:</label>
                                    <div class="col-lg-10">
                                        <asp:ListBox runat="server" ID="ddlCalendars" SelectionMode="multiple" CssClass="form-control select" DataValueField="Id" DataTextField="Name" 
                                                     AppendDataBoundItems="True" ToolTip='<%$ Resources:Resource, Calendars %>' >
                                        </asp:ListBox>
                                    </div>
                                    <asp:Button runat="server" CssClass="btn btn-primary col-lg-1" Text='<%$ Resources:Resource, Save %>' OnClick="bttSelectedCalendars_OnClick"/>
                                </div>
                          
                        </div>
                        <div class=" panel-body">
                            <asp:ListView runat="server" ID="listProfileRecords" Visible="False">
                                <LayoutTemplate>
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th runat="server" class="col-lg-push-0"  scope="col"><%= Resources.Resource.Color %></th>
                                            <th runat="server" class="col-lg-push-2"  scope="col"><%= Resources.Resource.Name %></th>
                                            <th runat="server" class="col-lg-push-2"  scope="col"><%= Resources.Resource.Timetable %></th>
                                            <th runat="server" class="col-lg-push-1"  scope="col"><%= Resources.Resource.Rate %></th>
                                            <th runat="server" class="col-lg-5"  scope="col"><%= Resources.Resource.Days %></th>
                                            <th runat="server" class="col-lg-pull-0"  scope="col"><%= Resources.Resource.Holidays %></th>
                                            <th runat="server" class="col-lg-pull-0"  scope="col"><%= Resources.Resource.Action %></th>
                                                        
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr runat="server" id="itemPlaceholder" ></tr>
                                        </tbody>
                                    </table>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr >
                                        
                                        <td >
                                            <button type="button" class="btn btn-float btn-rounded" style="background-color:<%#Eval("Color") %>"></button>
                                        </td>
                                        <td >
                                            <asp:Label ID="Label1" runat="server" Text='<%#Eval("Name") %>' CssClass="text-right disabled"/>
                                        </td>
                                        <td >
                                            <asp:Label ID="CodeLabel" runat="server" Text='<%#Eval("Hours") %>' CssClass="text-right"/>
                                        </td>
                                        <td >
                                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("Rate")+"%" %>' CssClass="text-right"/>
                                        </td>
                                         <td class="btn-group btn-group-toggle" data-toggle="buttons" >
                                             <label class="btn btn-default <%#((bool)Eval("Mo") ) ? "btn-primary" : "" %> <%#((bool)Eval("Holidays") ) ? "disabled" : "" %>" >
                                                 <input type="checkbox" /><%= Resources.Resource.Mo %>
                                             </label>
                                             <label class="btn btn-default <%#((bool)Eval("Tu") ) ? "btn-primary" : "" %> <%#((bool)Eval("Holidays") ) ? "disabled" : "" %>">
                                                 <input type="checkbox" /><%= Resources.Resource.Tu %>
                                             </label>
                                             <label class="btn  btn-default <%#((bool)Eval("We") ) ? "btn-primary" : "" %> <%#((bool)Eval("Holidays") ) ? "disabled" : "" %>">
                                                 <input type="checkbox" /><%= Resources.Resource.We %>
                                             </label>
                                             <label class="btn btn-default <%#((bool)Eval("Th") ) ? "btn-primary" : "" %> <%#((bool)Eval("Holidays") ) ? "disabled" : "" %>">
                                                 <input type="checkbox" /><%= Resources.Resource.Th %>
                                             </label>
                                             <label class="btn btn-default  <%#((bool)Eval("Fr") ) ? "btn-primary" : "" %> <%#((bool)Eval("Holidays") ) ? "disabled" : "" %>">
                                                 <input type="checkbox" /><%= Resources.Resource.Fr %>
                                             </label>
                                             <label class="btn btn-default <%#((bool)Eval("Sa") ) ? "btn-primary" : "" %> <%#((bool)Eval("Holidays") ) ? "disabled" : "" %>">
                                                 <input type="checkbox" /><%= Resources.Resource.Sa %>
                                             </label>
                                             <label class="btn btn-default <%#((bool)Eval("Su") ) ? "btn-primary" : "" %> <%#((bool)Eval("Holidays") ) ? "disabled" : "" %>">
                                                 <input type="checkbox" /><%= Resources.Resource.Su %>
                                             </label>
                                         </td>
                                        <td >
                                            <div class="uniform-checker hover ">
                                                <span >
                                                    <input type="checkbox" class="form-input-styled" <%# Convert.ToBoolean(Eval("Holidays")) ? "checked" : "" %> data-fouc="" disabled="disabled"/>
                                                </span>
                                            </div>
                                          
                                            
                                        </td>
                                        <td>
                                            <ul class="icons-list">
                                                <li class="text-primary-600">
                                                    <a runat="server" class="text-left span-id"
                                                        onclick='<%# "editRec(\""+ Eval("Id") + "\",\""+Eval("Color") +"\",\""+Eval("Name") +"\",\""+
                                                                    Eval("Start")+"\",\""+Eval("End")+ "\""+",\""+Eval("Rate")+"\",\""+
                                                                    Eval("Mo")+"\",\""+Eval("Tu")+"\",\""+Eval("We")+"\",\""+Eval("Th")+"\",\""+
                                                                    Eval("Fr")+"\",\""+Eval("Sa")+"\",\""+Eval("Su")+"\",\""+Eval("Holidays")+ "\");" %>'>
                                                        <i class="icon-pencil7"></i>
                                                    </a>
                                                </li>
                                                <li class="text-danger-600">
                                                    <a runat="server" 
                                                        onclick='<%# "deleteRec(\""+ Eval("Id") + "\");" %>'>
                                                        <i class="icon-trash"></i>
                                                    </a>
                                                                
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                
                                <EmptyDataTemplate>
                                    <table class="table table-striped table-hover">
                                        <thead>
                                        <tr>
                                            <th runat="server" class="col-lg-push-0"  scope="col"><%= Resources.Resource.Color %></th>
                                            <th runat="server" class="col-lg-push-2"  scope="col"><%= Resources.Resource.Name %></th>
                                            <th runat="server" class="col-lg-push-2"  scope="col"><%= Resources.Resource.Timetable %></th>
                                            <th runat="server" class="col-lg-push-1"  scope="col"><%= Resources.Resource.Rate %></th>
                                            <th runat="server" class="col-lg-5"  scope="col"><%= Resources.Resource.Days %></th>
                                            <th runat="server" class="col-lg-pull-0"  scope="col"><%= Resources.Resource.Holidays %></th>
                                            <th runat="server" class="col-lg-pull-0"  scope="col"><%= Resources.Resource.Action %></th>
                                                        
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr runat="server" id="Tr1" ></tr>
                                        </tbody>
                                    </table>
                                </EmptyDataTemplate>
                            </asp:ListView>
						    </div>
                        <div class="panel-body">
                            <div class="col-lg-8"></div>
                            <div class="col-lg-4 text-right">
                                <div class="btn-group">
                                    <a onclick="editRec(0)"  class="btn btn-default"><i class="icon-plus2 position-left"></i><%= Resources.Resource.Add_line %></a>
                                   
                                </div>
                            </div>
                        </div>
                        
                        <div class='panel-body '>
                            <div id="calendar" class="fullcalendar-timeline"></div>
                        </div>

                        </div>
                    </asp:Panel>
                </div>
                <!-- /column -->

		</div> <!-- /row -->
        
        <div id="modal_delete_rec" class="modal fade">
            <div class="modal-dialog  modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
                    </div>

                    <div class="modal-body">
                        <h6 class="text-semibold"><%= Resources.Resource.Are_You_Sure_To_Delete %></h6>
                        <p><%= Resources.Resource.Delete_Is_Irreversible %></p>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
                        <asp:Button runat="server" type="submit" ID="btnDelete" OnClick="btnDeleteRecord_Click" CssClass="btn btn-primary btn-danger" 
                                   Text="<%$ Resources:Resource, Yes_Delete %>" />

                    </div>
                </div>
            </div>
        </div>
      
        <div id="modal_delete_prof" class="modal fade">
            <div class="modal-dialog  modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
                    </div>

                    <div class="modal-body" runat="server" ID="warnMessage">
                        <h6 class="text-semibold"><%= Resources.Resource.Are_You_Sure_To_Delete %></h6>
                        <p><%= Resources.Resource.Delete_Is_Irreversible %></p>
                    </div>
                    <div class="modal-body" runat="server" ID="pDefaultWorkSchedule">
                        <h6 class="text-semibold"><%= Resources.Resource.CantBeDeleted %></h6>
                        <label runat="server" class=""><%= Resources.Resource.WorkScheduleInUse %></label>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
                        <asp:Button runat="server" type="submit" ID="btnDeleteProfile" OnClick="btnDeleteProfile_Click" CssClass="btn btn-primary" 
                                    Text="<%$ Resources:Resource, Yes_Delete %>" />

                    </div>
                </div>
            </div>
        </div>
            
        <div id="modal_rec" class="modal fade" role="dialog" aria-hidden="true">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title text-center" id="createRecHeader"><%= Resources.Resource.Add_line %></h5>
                        <h5 class="modal-title text-center" id="editRecHeader"><%= Resources.Resource.Edit_Line %></h5>
                    </div>
                    <input type="text" id="RecId" runat="server" hidden="" />
                    <div class="modal-body">
                       <div class="row">
                            <div class="col-md-4">
                                <label><%= Resources.Resource.Name %></label>
                                <input type="text" id="recName" name="recName" runat="server" maxlength="45" class="form-control" />
                                <label id="recName-error" class="validation-error-label" for="recName" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                            </div>
                            <div class="col-md-3">
                                <label><%= Resources.Resource.StartTime %></label>
                                <input type="text" id="recStart" name="recStart"  runat="server" class="form-control" placeholder="hh:mm" onchange="onTimeChanged(event)"/>
                                <label id="recStart-error" class="validation-error-label" for="recStart" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                            </div>
                            <div class="col-md-3">
                                <label><%= Resources.Resource.EndTime %></label>
                                <input type="text" id="recEnd" name="recEnd" runat="server" class="form-control" placeholder="hh:mm" onchange="onTimeChanged(event)" />
                                <label id="recEnd-error" class="validation-error-label" for="recEnd" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                            </div>
                            <div class="col-md-2">
                                <label><%= Resources.Resource.Rate %>%</label>
                                <input type="number" id="recRate" name="recRate" runat="server" min="0" value="100" class="form-control" />
                                <label id="recRate-error" class="validation-error-label" for="recRate" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                            <input type="text" id="RecDateValidation" name="RecDateValidation" runat="server" hidden=""/>
                            <label id="RecDate-error" class="validation-error-label" for="RecDateValidation" style="display: none;"><%= Resources.Resource.StartEndDateValidationMessage %></label>
                            </div>
                        </div>
                        <hr/>
						
					  <div class="row">
                          <div class="col-md-2">
                              <label><%= Resources.Resource.Color %></label>
                              <br/>
                              <input type="text" id="recColor" runat="server" maxlength="8" hidden="" />
                              <button type="button" id="colorpad" class="btn btn-float btn-rounded" onclick="setRecColor()"></button>
                          </div>
						 <div class="col-md-8">
                                <label><%= Resources.Resource.Days %></label>
                                <br/>
                               <div class="btn-group btn-group-toggle" data-toggle="buttons" id="weekDays" >
                                    <label class="label-day btn btn-default" onclick="checkLabel(event)">
                                        <input type="checkbox" checked="" id="recMo" class="weeek-day" onchange="checkDay(event)" runat="server"/>
                                        <%= Resources.Resource.Mo %>
                                    </label>
                                    <label class="label-day btn btn-default" onclick="checkLabel(event)">
                                        <input type="checkbox" checked="" id="recTu" class="weeek-day" onchange="checkDay(event)" runat="server"/>
                                        <%= Resources.Resource.Tu %>
                                    </label>
                                    <label class="label-day btn btn-default" onclick="checkLabel(event)">
                                        <input type="checkbox" checked="" id="recWe" class="weeek-day" onchange="checkDay(event)" runat="server"/>
                                        <%= Resources.Resource.We %>
                                    </label>
                                    <label class="label-day btn btn-default" onclick="checkLabel(event)">
                                        <input type="checkbox" checked="" id="recTh" class="weeek-day" onchange="checkDay(event)" runat="server"/>
                                        <%= Resources.Resource.Th %>
                                    </label>
                                    <label class="label-day btn btn-default" onclick="checkLabel(event)">
                                        <input type="checkbox" checked="" id="recFr" class="weeek-day" onchange="checkDay(event)" runat="server"/>
                                        <%= Resources.Resource.Fr %>
                                    </label>
                                    <label class="label-day btn btn-default" onclick="checkLabel(event)">
                                        <input type="checkbox" checked="" id="recSa" class="weeek-day" onchange="checkDay(event)" runat="server"/>
                                        <%= Resources.Resource.Sa %>
                                    </label>
                                    <label class="label-day btn btn-default"  onclick="checkLabel(event)">
                                        <input type="checkbox" checked="" id="recSu" class="weeek-day"  onchange="checkDay(event)" runat="server"/>
                                        <%= Resources.Resource.Su %>
                                    </label>
                                </div>
                            </div>
                         <div class="col-md-2">
                                <label><%= Resources.Resource.PublicHoliday %></label>
                                <div class="radio">
                                        <input type="checkbox" id="recHolidays" onchange="checkHolidays(event)" runat="server"/>
                                </div>
                            </div>

						</div>
                        <br/>
                        <div class="modal-footer">
                            <asp:Button ID="Button1"  runat="server" OnClick="btnSaveProfileRec_OnClick"  OnClientClick="return  validateEditProfileRec()"
                                        CssClass="btn btn-primary" Text="<%$ Resources:Resource, Save %>" />  

                            <button type="button" class="btn btn-default" data-dismiss="modal"><%= Resources.Resource.Close %></button>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

        <div id="modal_profile" class="modal fade">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title text-center" id="createProfileHeader"><%= Resources.Resource.Create %> <%= Resources.Resource.Work_Schedule %></h5>
                        <h5 class="modal-title text-center" id="editProfileHeader"><%= Resources.Resource.EditProfile %></h5>
                    </div>
                    <div class="modal-body">
                        <input type="text" id="ProfileId" runat="server" hidden="" />
                        <div class="row">
                            <div class="col-md-2">
                                <h6><%= Resources.Resource.Name %></h6>
                            </div>
                            <div class="col-md-10">
                                <div class="input-group form-group">
                                    <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                    <input type="text" id="ProfileName" name="ProfileName" runat="server" maxlength="45" class="form-control" />
                                </div>
                                <label id="ProfileName-error" class="validation-error-label" for="ProfileName" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-2">
                                <h6><%= Resources.Resource.Code %></h6>
                            </div>
                            <div class="col-md-10">
                                <div class="input-group form-group">
                                    <span class="input-group-addon"><i class="icon-link"></i></span>
                                    <input type="text" id="ProfileCode" name="ProfileCode" runat="server" maxlength="45" class="form-control" />
                                </div>
                                <label id="ProfileCode-error" class="validation-error-label" for="ProfileCode" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="bSaveProfile"  runat="server" OnClick="bSaveProfile_OnClick" OnClientClick="return  validateEditProfile()"
                                    CssClass="btn btn-primary" Text="<%$ Resources:Resource, Save %>" />  
                        <button type="button" class="btn btn-default" data-dismiss="modal"><%= Resources.Resource.Close %></button>
                    </div>
                </div>
            </div>
        </div>
       
   
    </div><!-- /content -->

    </form>
    
    <script type="text/javascript">
        $(document).ready(function () {
            var id = <%= CurrentProfileId %>;
            $("tr#" + id).addClass("alpha-primary");
            setRecColor();
            $('.fullcalendar-timeline').fullCalendar({
                defaultView: 'timelineDay',
                contentHeight:"auto",
                aspectRatio:2,
                header: {
                    left: '',
                    center: '',
                    right: ''
                },
                editable: false,
                resourceLabelText: "<%= Resources.Resource.Days %>",
                resourceAreaWidth:'50px',
                resources: [
                    {
                        "id": "mo",
                        "title": "<%= Resources.Resource.Mo %>"
                    }, {
                        "id": "tu",
                        "title": "<%= Resources.Resource.Tu %>"
                    }, {
                        "id": "we",
                        "title": "<%= Resources.Resource.We %>"
                    }, {
                        "id": "th",
                        "title": "<%= Resources.Resource.Th %>"
                    }, {
                        "id": "fr",
                        "title": "<%= Resources.Resource.Fr %>"
                    }, {
                        "id": "sa",
                        "title": "<%= Resources.Resource.Sa %>"
                    }, {
                        "id": "su",
                        "title": "<%= Resources.Resource.Su %>"
                    }
                    , {
                        "id": "cal",
                        "title": "<%= Resources.Resource.Cal %>"
                    }
                ],
                events: calendarEvents

            });

        });

        function onTimeChanged(e) {
            if (e && e.target) {
                var newVal = "";
                var value = $(e.target).val();

                var m = moment(value, moment.HTML5_FMT.TIME);
                var res = m.isValid();
                if (res)
                    newVal = m.format(moment.HTML5_FMT.TIME);

                $(e.target).val(newVal);
            }

        }

        function validateEditProfile() {
            var res = true;
            var name = $('#<%= ProfileName.ClientID %>').val();
            var code = $('#<%= ProfileCode.ClientID %>').val();
            if (!name || !code) {
                res = false;

                if (!name)
                    $('#ProfileName-error').show();
                else
                    $('#ProfileName-error').hide();

                if (!code)
                    $('#ProfileCode-error').show();
                else
                    $('#ProfileCode-error').hide();
            }

            return res;
        }

        function editProfile(id, name, code) {
            $('#ProfileName-error').hide();
            $('#ProfileCode-error').hide();
            $('#<%= ProfileId.ClientID %>').val(id);
            $('#<%= ProfileName.ClientID %>').val(name);
            $('#<%= ProfileCode.ClientID %>').val(code);

            if (id > 0) {
                $('#createProfileHeader').hide();
                $('#editProfileHeader').show();
            } else {
                $('#createProfileHeader').show();
                $('#editProfileHeader').hide();
            }
            
            $('#modal_profile').modal('show');
        }

        function validateEditProfileRec() {
            
            var res = true;
            var name = $('#<%= recName.ClientID %>').val();
            var start = $('#<%= recStart.ClientID %>').val();
            var end = $('#<%= recEnd.ClientID %>').val();
            var rate = $('#<%= recRate.ClientID %>').val();
            if (!name || !start || !end || !rate) {
                res = false;

                if (!name)
                    $('#recName-error').show();
                else
                    $('#recName-error').hide();

                if (!start)
                    $('#recStart-error').show();
                else
                    $('#recStart-error').hide();

                if (!end)
                    $('#recEnd-error').show();
                else
                    $('#recEnd-error').hide();

                if (!rate)
                    $('#recRate-error').show();
                else
                    $('#recRate-error').hide();
            }
            else {
                $('#recName-error').hide();
                $('#recStart-error').hide();
                $('#recEnd-error').hide();
                $('#recRate-error').hide();

                var stT = moment(start, moment.HTML5_FMT.TIME);
                var etT = moment(end, moment.HTML5_FMT.TIME);

                var isOk = stT.isBefore(etT, 'minute');
                if (!isOk) {
                    res = false;
                    $('#RecDate-error').show();
                } else {
                    $('#RecDate-error').hide();
                }

            }

            return res;
        }
       
        function editRec(id, color, name, start, end, rate, mo, tu, we, th, fr, sa, su, full) {
            $('#RecDate-error').hide();
            $('#recName-error').hide();
            $('#recStart-error').hide();
            $('#recEnd-error').hide();
            $('#recRate-error').hide();
            $('#<%= RecId.ClientID %>').val(id);

            if (!color)
                color = id > 0 ? $('#<%= recColor.ClientID %>').val() : null;
            setRecColor(color);

            $('#<%= recName.ClientID %>').val(name);
            $('#<%= recStart.ClientID %>').val(start || '08:00');
            $('#<%= recEnd.ClientID %>').val(end || '18:00');;
            $('#<%= recRate.ClientID %>').val(rate || '100');

            SetCheckBoxValue($('#<%= recMo.ClientID %>').parent(), $('#<%= recMo.ClientID %>'), mo == 'True');
            SetCheckBoxValue($('#<%= recTu.ClientID %>').parent(), $('#<%= recTu.ClientID %>'), tu == 'True');
            SetCheckBoxValue($('#<%= recWe.ClientID %>').parent(), $('#<%= recWe.ClientID %>'), we == 'True');
            SetCheckBoxValue($('#<%= recTh.ClientID %>').parent(), $('#<%= recTh.ClientID %>'), th == 'True');
            SetCheckBoxValue($('#<%= recFr.ClientID %>').parent(), $('#<%= recFr.ClientID %>'), fr == 'True');
            SetCheckBoxValue($('#<%= recSa.ClientID %>').parent(), $('#<%= recSa.ClientID %>'), sa == 'True');
            SetCheckBoxValue($('#<%= recSu.ClientID %>').parent(), $('#<%= recSu.ClientID %>'), su == 'True');


            SetCheckBoxValue(null, $('#<%= recHolidays.ClientID %>'), full == 'True');
            checkHolidays();

            if (id > 0) {
                $('#createRecHeader').hide();
                $('#editRecHeader').show();
            } else {
                $('#createRecHeader').show();
                $('#editRecHeader').hide();
            }

            $('#modal_rec').modal('show');
        }
        
        function deleteRec(id) {
            $('#<%= RecId.ClientID %>').val(id);
            
            $('#modal_delete_rec').modal('show');
        }

        function deleteProfile(id, isOrgSet) {
            $('#<%= ProfileId.ClientID %>').val(id);

            if (isOrgSet.toLowerCase() == 'true') {
                $('#<%= pDefaultWorkSchedule.ClientID %>').show();
                $('#<%= btnDeleteProfile.ClientID %>').hide();
                $('#<%= warnMessage.ClientID %>').hide();
                
            } else {
                $('#<%= pDefaultWorkSchedule.ClientID %>').hide();
                $('#<%= btnDeleteProfile.ClientID %>').show();
                $('#<%= warnMessage.ClientID %>').show();
            }
            
            $('#modal_delete_prof').modal('show');
        }

        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }

        function setRecColor(color) {

             color = color || getRandomColor();
                
            $("#colorpad").css("background-color", color);

            $('#<%= recColor.ClientID %>').val(color);
            
        }
        
        function checkHolidays()
        {
            var checked = $('#<%= recHolidays.ClientID %>').is(':checked');
            
            if (checked) {
                $(".label-day").addClass('disabled');
            } else {
                $('.label-day').removeClass("disabled");
            }
        }
        

        function checkDay(e) {
            var checkbox = e.target;
            var label = $(checkbox).parent();

            if (!label || $(label).hasClass("disabled"))
                return;

            SetCheckBoxValue(label, checkbox, !checkbox.checked);
        }

        function checkLabel(e) {
            var label = e.target;

            if (!label || $(label).hasClass("disabled"))
                return;

            var checkbox = $(label).children()[0];
            SetCheckBoxValue(label, checkbox, !checkbox.checked);
        }

        function SetCheckBoxValue(label, checkbox, checked) {

            if (checked) {
                label && $(label).addClass('btn-primary');
            } else {
                label && $(label).removeClass('btn-primary');
            }
            checkbox.checked = checked;
            if (checkbox[0])
                checkbox[0].checked = checked;
        }

    </script>


</asp:Content>

  
