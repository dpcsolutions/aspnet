﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.Calendars;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;

public partial class Settings_Calendars_Default : System.Web.UI.Page
{
    public PageHelper ph;
    public static long CurrentCalendarId { get; set; }
    
    private static CalendarsManager mngr;
    private static CalendarsRecManager mngrRec;

    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        ph.ItemLabel = Resources.Resource.CalendarOptions;
        
        mngr = new CalendarsManager(ph.CurrentOrganizationId);
        mngrRec = new CalendarsRecManager(ph.CurrentOrganizationId);
        
        if (!IsPostBack)
        {
            
            var data = mngr.GetCalendarsDtos();
            if (CurrentCalendarId < 1)
            {
                var item = data != null && data.Any() ? data.FirstOrDefault():null;

                CurrentCalendarId = item!=null?item.Id:0;
                SelectedCalendarName.Text = item != null ? item.Name:String.Empty;
            }
            else
            {
                var item = data.FirstOrDefault(i => i.Id == CurrentCalendarId);
                SelectedCalendarName.Text = item != null ? item.Name : String.Empty;
            }

            listCalendars.DataSource = data;
            listCalendars.DataBind();
            CalendarRecords.Visible = data != null && data.Count > 0;


        }
        if (CurrentCalendarId > 0)
        {
            listCalendarRecords.Visible = true;
            var recData = mngrRec.GetCalendarRecordsDto(CurrentCalendarId);
            listCalendarRecords.DataSource = recData;
            listCalendarRecords.DataBind();
            btnDuplicate.Visible = true;
        }
        Title = ph.CMP.Name;
    }
    
    protected void bSaveCalendar_OnClick(object sender, EventArgs e)
    {
        int id = 0; 
        id = Convert2.StrToInt(CalendarId.Value, id);
        var name = CalendarName.Value;
        var code = CalendarCode.Value;
        if(String.IsNullOrWhiteSpace(name)|| String.IsNullOrWhiteSpace(code))
            Response.Redirect(Request.Url.PathAndQuery);

        var calendar = new Calani.BusinessObjects.Model.calendars()
        {
            name = name,
            code = code,
            organization_Id = ph.CurrentOrganizationId,
        };
        SmartAction<calendars> ret;

        if (id < 1)
            ret = mngr.Add(calendar);
        else
            ret = mngr.Update(id, calendar);
        
        

        if (ret.Success)
        {
            CurrentCalendarId = ret.Record.id;
            

            var data = mngr.GetCalendarsDtos();

            listCalendars.DataSource = data;
            listCalendars.DataBind();

            CalendarName.Value = String.Empty;
            CalendarCode.Value = String.Empty;
        }
        else ph.DisplayError(ret.ErrorMessage);

        Response.Redirect(Request.Url.PathAndQuery);
    }

    protected void btnSaveCalendarRec_OnClick(object sender, EventArgs e)
    {
        int id = 0;
        id = Convert2.StrToInt(RecId.Value, id);

        var name = RecName.Value;

        var start = RecStartDate.Value;

        if(String.IsNullOrWhiteSpace(name) || String.IsNullOrWhiteSpace(start))
            return;


        var rec = new calendars_rec()
        {
            calendar_id = CurrentCalendarId,
            name = name
        };

        var st = String.IsNullOrWhiteSpace(RecStartTime.Value)?"00:00:00": RecStartTime.Value.Trim() + ":00";

            start = String.Format("{0} {1}",start, st);
            rec.start_date = Convert2.ToDateWithTime(start, "picker");


        var et = String.IsNullOrWhiteSpace(RecEndTime.Value) ? "00:00:00" : RecEndTime.Value.Trim() + ":00";
        var end = RecEndDate.Value;

        if (!String.IsNullOrWhiteSpace(end))
        {
            end = String.Format("{0} {1}", end, et);

            rec.end_date = Convert2.ToDateWithTime(end, "picker");

        }




        SmartAction<calendars_rec> ret;

        if (id < 1)
            ret = mngrRec.Add(rec);
        else
            ret = mngrRec.Update(id, rec);

        if (ret.Success)
        {
            var data = mngrRec.GetCalendarRecordsDto(CurrentCalendarId);

            listCalendarRecords.DataSource = data;
            listCalendarRecords.DataBind();
            RecId.Value = String.Empty;
            RecName.Value = String.Empty;
            RecStartDate.Value = String.Empty;
            RecEndDate.Value = String.Empty;
        }
        Response.Redirect(Request.Url.PathAndQuery);
    }

    protected void btnDuplicateCalendar_Click(object sender, EventArgs e)
    {
        
        CurrentCalendarId = mngr.DuplicateCalendar(CurrentCalendarId);

        Response.Redirect(Request.Url.PathAndQuery);
    }

    protected void btnDeleteCalendar_Click(object sender, EventArgs e)
    {
        long id = 0;
        id = Convert2.StrToLong(CalendarId.Value, id);
        if (id > 0)
        {
            mngr.Delete(id);
            CurrentCalendarId = 0;
        }
        Response.Redirect(Request.Url.PathAndQuery);

    }

    protected void btnDeleteRecord_Click(object sender, EventArgs e)
    {
        long id = 0;
        id = Convert2.StrToLong(RecId.Value, id);
        if (id > 0)
        {
            mngrRec.Delete(id);
          
        }

        Response.Redirect(Request.Url.PathAndQuery);
    }

    protected void CalendarItem_OnClick(object sender, EventArgs e)
    {
        var btn = (LinkButton)sender;
        CurrentCalendarId = Convert2.StrToLong(btn.CommandArgument, CurrentCalendarId);
        Response.Redirect(Request.Url.PathAndQuery);
    }
}