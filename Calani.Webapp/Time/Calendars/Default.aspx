﻿<%@ Page Title="Calendars" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Settings_Calendars_Default" Codebehind="Default.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%--Canceled Seubi css for page to apply theme style--%>
    <style type="text/css">
        .table > thead > tr > th {
            border-bottom: none;  
            background: none; 
            color: black;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    var dateFormat = '<%= Resources.Resource.localisedDateFormat %>';
</script>
<form runat="server">
    <div class="content">
    
        <div class="row">
                <div class="col-lg-12">
                   
                </div>
            
			    <div class="col-lg-12">
                    <asp:Panel runat="server" ID="Calendar" class="panel panel-flat" >
						    <div class="table-responsive panel-body">

                                <asp:ListView runat="server" ID="listCalendars" >
                                    <LayoutTemplate>
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th runat="server" class="col-lg-6"><%= Resources.Resource.CalendarName %></th>
                                                <th runat="server" class="col-lg-4"><%= Resources.Resource.Code %></th>
                                                <th runat="server" class="col-lg-2"><%= Resources.Resource.Action %></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr runat="server" id="itemPlaceholder" ></tr>
                                            </tbody>
                                        </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr id='<%# Eval("Id") %>'>
                                            <td>
                                                <%-- Data-bound content. --%>
                                                
                                                <input type="text" id="lstCalendarId" runat="server" hidden="" value='<%# Bind("Id") %>'/>

                                                <asp:LinkButton ID="NameLabel" runat="server" Text='<%#Eval("Name") %>' CssClass="text-left text-default"
                                                                OnClick="CalendarItem_OnClick" CommandArgument='<%#Eval("Id")%>'/>
                                            </td>
                                            <td >
                                                <%-- Data-bound content. --%>
                                                <asp:LinkButton ID="CodeLabel" runat="server"  runat="server" Text='<%#Eval("Code") %>' CssClass="text-left text-default"
                                                                OnClick="CalendarItem_OnClick" CommandArgument='<%#Eval("Id")%>'/>
                                            </td>
                                            <td>
                                                <ul class="icons-list">
                                                    
                                                        <%-- Data-bound content. --%>
                                                        
                                                    <li class="text-primary-600">
                                                        <a runat="server" href="#" data-target="#modal_calendar" onclick='<%# "editCalendar(\""+ Eval("Id") + "\",\""+Eval("Name") +"\",\""+Eval("Code")+  "\");" %>'>
                                                            <i class="icon-pencil7"></i>
                                                        </a>
                                                    </li>
                                                    <li class="text-danger-600">
                                                        <a href="#" onclick='<%# "deleteCalendar(\""+ Eval("Id") + "\",\""+Eval("ScheduleIds")+"\");" %>'><i class="icon-trash"></i></a>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
								</asp:ListView>
                                <br/>
                                <div class="col-lg-2 pull-left">
                                    <a onclick=" editCalendar(0, '', '')"  class="btn btn-default pull-left"><i class="icon-plus2 position-left"></i><%= Resources.Resource.CreateCalendar %></a>
                                </div>
                                
                                <div class="col-lg-2 pull-right">
                                    
                                    <asp:LinkButton runat="server" ID="btnDuplicate" class="btn btn-primary pull-right" OnClick="btnDuplicateCalendar_Click" Visible="False">
                                        <i class="icon-copy position-right"></i> <%= Resources.Resource.Duplicate %>
                                    </asp:LinkButton>
                                </div>

						    </div>
					    </asp:Panel><!-- /panelEdit -->
                    
			    
                    <asp:Panel runat="server"   ID="CalendarRecords" class="timeline-row panel panel-flat" Visible="False" >
                        <div class="panel-heading">
                            <h6 class="panel-title text-semibold"><%= Resources.Resource.Calendar %>: <asp:Label runat="server" ID="SelectedCalendarName"></asp:Label></h6>
                        </div>
                        <div class="table-responsive panel-body">
                            <asp:ListView runat="server" ID="listCalendarRecords" Visible="True">
                                            <LayoutTemplate>
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th runat="server" class="col-lg-6"  scope="col"><%= Resources.Resource.EventName %></th>
                                                        <th runat="server" class="col-lg-2"  scope="col"><%= Resources.Resource.Start %></th>
                                                        <th runat="server" class="col-lg-2"  scope="col"><%= Resources.Resource.End %></th>
                                                        <th runat="server" class="col-lg-2"  scope="col"><%= Resources.Resource.Action %></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr runat="server" id="itemPlaceholder" ></tr>
                                                    </tbody>
                                                </table>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <tr >
                                                    <td runat="server"  >
                                                        <%-- Data-bound content. --%>
                                                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("Name") %>' CssClass="text-right"/>
                                                    </td>
                                                    <td runat="server" >
                                                        <%-- Data-bound content. --%>
                                                        <asp:Label ID="Label2" runat="server" Text='<%#Eval("StartDate") %>' CssClass="text-right"/>
                                                    </td>
                                                    <td runat="server" >
                                                        <%-- Data-bound content. --%>
                                                        <asp:Label ID="CodeLabel" runat="server" Text='<%#Eval("EndDate") %>' CssClass="text-right"/>
                                                    </td>
                                                    <td>
                                                        <ul class="icons-list">
                                                    
                                                            <%-- Data-bound content. --%>
                                                        
                                                            <li class="text-primary-600">
                                                                
                                                                <a runat="server" class="text-left"
                                                                   onclick='<%# "editRec(\""+ Eval("Id") + "\",\""+Eval("Name") +"\",\""+Eval("StartDateStr")+"\",\""+Eval("StartTimeStr")+
                                                                                "\",\""+Eval("EndDateStr")+"\",\""+Eval("EndTimeStr") + "\");" %>'>
                                                                    <i class="icon-pencil7"></i>
                                                                </a>
                                                            </li>
                                                            <li class="text-danger-600">
                                                                <a runat="server" 
                                                                   onclick='<%# "deleteRec(\""+ Eval("Id") + "\");" %>'>
                                                                    <i class="icon-trash"></i>
                                                                </a>
                                                                
                                                            </li>
                                                        </ul>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            
                                            <EmptyDataTemplate>
                                                <table class="table table-striped table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th runat="server" class="col-lg-6"  scope="col"><%= Resources.Resource.Name %></th>
                                                        <th runat="server" class="col-lg-4"  scope="col"><%= Resources.Resource.Days %></th>
                                                        <th runat="server" class="col-lg-2"  scope="col"><%= Resources.Resource.Action %></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr runat="server" id="itemPlaceholder" ></tr>
                                                    </tbody>
                                                </table>
                                            </EmptyDataTemplate>
                                        </asp:ListView>
						    </div>
                        <div class="panel-body">
                            <div class="col-lg-8"></div>
                            <div class="col-lg-4 text-right">
                                <div class="btn-group">
                                    <a onclick="editRec(0,'','','')"  class="btn btn-default"><i class="icon-plus2 position-left"></i><%= Resources.Resource.Add_line %></a>
                                   
                                </div>
                            </div>
                        </div>
					    </asp:Panel><!-- /panelEdit -->
                </div>
             
		</div> 
        
        <div id="modal_delete_rec" class="modal fade">
            <div class="modal-dialog  modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
                    </div>

                    <div class="modal-body">
                        <h6 class="text-semibold"><%= Resources.Resource.Are_You_Sure_To_Delete %></h6>
                        <p><%= Resources.Resource.Delete_Is_Irreversible %></p>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
                        <asp:Button runat="server" type="submit" ID="btnDelete" OnClick="btnDeleteRecord_Click" CssClass="btn btn-primary btn-danger" 
                                   Text="<%$ Resources:Resource, Yes_Delete %>" />

                    </div>
                </div>
            </div>
        </div>
      
        <div id="modal_delete_cal" class="modal fade">
            <div class="modal-dialog  modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
                    </div>
                    
                    
                    <div class="modal-body" runat="server" ID="warnMessage">
                        <h6 class="text-semibold"><%= Resources.Resource.Are_You_Sure_To_Delete %></h6>
                        <p><%= Resources.Resource.Delete_Is_Irreversible %></p>
                    </div>
                    <div class="modal-body" runat="server" ID="workSchedule">
                        <h6 class="text-semibold"><%= Resources.Resource.CantBeDeleted %></h6>
                        <label runat="server" class=""><%= Resources.Resource.CalendarInUse + ":" %>
                            <span id="workScheduleIds"></span>
                        </label>
                    </div>
                   

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
                        <asp:Button runat="server" type="submit" ID="bDelCal" OnClick="btnDeleteCalendar_Click" CssClass="btn btn-primary" 
                                    Text="<%$ Resources:Resource, Yes_Delete %>" />

                    </div>
                </div>
            </div>
        </div>
            
        <div id="modal_rec" class="modal fade">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title text-center" id="createRecHeader"><%= Resources.Resource.Add_line %></h5>
                        <h5 class="modal-title text-center" id="editRecHeader"><%= Resources.Resource.Edit_Line %></h5>
                    </div>
                   
                    <div class="modal-body">
                        <input type="text" id="RecId" runat="server" hidden="" />
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h6><%= Resources.Resource.Name %></h6>
                                    <div class="input-group form-group">
                                        <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                        <input type="text" id="RecName" name="RecName" maxlength="45" runat="server" class="form-control" />
                                    </div>
                                    <label id="RecName-error" class="validation-error-label" for="RecName" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <h6><%= Resources.Resource.Start %></h6>
                                    <div class="input-group form-group">
                                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                        <input type="text" id="RecStartDate" name="RecStartDate" runat="server" class="form-control daterange-single" />
                                    </div>
                                    <label id="RecStartDate-error" class="validation-error-label" for="RecStartDate" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                                </div>

                                <div class="col-sm-6">
                                    <h6><%= Resources.Resource.StartTime %></h6>
                                    <div class="input-group form-group">
                                        <span class="input-group-addon"><i class="icon-watch2"></i></span>
                                        <input type="text" id="RecStartTime" name="RecStartTime"  runat="server" class="form-control" value="8:00" placeholder="hh:mm" onchange="onTimeChanged(event)"/>
                                    </div>
                                    <label id="RecStartTime-error" class="validation-error-label" for="RecStartTime" style="display: none;"><%= Resources.Resource.Declined %></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <h6><%= Resources.Resource.End %></h6>
                                    <div class="input-group form-group">
                                        <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                        <input type="text" id="RecEndDate" name="RecEndDate" runat="server" class="form-control daterange-single" />
                                    </div>
                                    <label id="RecEndDate-error" class="validation-error-label" for="RecEndDate" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                                    <label id="RecDate-error" class="validation-error-label" for="RecEndDate" style="display: none;"><%= Resources.Resource.StartEndDateValidationMessage %></label>
                                </div>

                                <div class="col-sm-6">
                                    <h6><%= Resources.Resource.EndTime %></h6>
                                    <div class="input-group form-group">
                                        <span class="input-group-addon"><i class="icon-watch2"></i></span>
                                        <input type="text" id="RecEndTime" name="RecEndTime"  runat="server" class="form-control " value="18:00" placeholder="hh:mm" onchange="onTimeChanged(event)"/>
                                    </div>
                                    <label id="RecEndTime-error" class="validation-error-label" for="RecEndTime" style="display: none;"><%= Resources.Resource.Declined %></label>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="Button1"  runat="server" OnClick="btnSaveCalendarRec_OnClick"  OnClientClick="return  validateEditCalendarRec()"
                                    CssClass="btn btn-primary" Text="<%$ Resources:Resource, Save %>" />  

                        <button type="button" class="btn btn-default" data-dismiss="modal"><%= Resources.Resource.Close %></button>
                    </div>
                </div>
            </div>
           </div>
        </div>

        <div id="modal_calendar" class="modal fade">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title text-center" id="createCalendarHeader"><%= Resources.Resource.CreateCalendar %></h5>
                        <h5 class="modal-title text-center" id="editCalendarHeader"><%= Resources.Resource.EditCalendar %></h5>
                    </div>
                    <div class="modal-body">
                             <div class="form-group row">
                                <div class="col-md-2">
                                    <h6><%= Resources.Resource.Name %></h6>
                                </div>
                                <div class="col-md-10  form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                        <input type="text" id="CalendarName" name="CalendarName" runat="server" maxlength="45" class="form-control " />
                                    </div>
                                    <label id="CalendarName-error" class="validation-error-label" for="CalendarName" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>

                                </div>
                            </div>
                        
                            <div class="row">
                                <div class="col-md-2">
                                    <h6><%= Resources.Resource.Code %></h6>
                                </div>
                                <div class="col-md-10">
                                    <div class="input-group form-group">
                                        <span class="input-group-addon"><i class="icon-link"></i></span>
                                        <input type="text" id="CalendarCode" name="CalendarCode" runat="server" maxlength="45" class="form-control" />
                                    </div>
                                    <label id="CalendarCode-error" class="validation-error-label" for="CalendarCode" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                                </div>
                            </div>
                        <input type="text" id="CalendarId" runat="server" hidden="" />
                       
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="bSaveCalendar"  runat="server" OnClick="bSaveCalendar_OnClick" OnClientClick="return  validateEditCalendar()"
                                    CssClass="btn btn-primary" Text="<%$ Resources:Resource, Save %>" />  
                        <button type="button" class="btn btn-default" data-dismiss="modal"><%= Resources.Resource.Close %></button>
                    </div>
                </div>
            </div>
        </div>
       
   
    </div><!-- /content -->

    </form>
    
    <script type="text/javascript">

        $(document).ready(function () {

            var id = <%= CurrentCalendarId %>;
            $("tr#" + id).addClass("alpha-primary");
        });
        function validateEditCalendar() {
            var res = true;
            var name = $('#<%= CalendarName.ClientID %>').val();
            var code = $('#<%= CalendarCode.ClientID %>').val();
            if (!name || !code) {
                res = false;

                if (!name) 
                    $('#CalendarName-error').show();
                else 
                    $('#CalendarName-error').hide();

                    if (!code) 
                    $('#CalendarCode-error').show();
                else 
                    $('#CalendarCode-error').hide();
            }
           
            return res;
        }
        function editCalendar(id, name, code) {
            $('#CalendarName-error').hide();
            $('#CalendarCode-error').hide();
            $('#<%= CalendarId.ClientID %>').val(id);
            $('#<%= CalendarName.ClientID %>').val(name);
            $('#<%= CalendarCode.ClientID %>').val(code);

            if (id > 0) {
                $('#createCalendarHeader').hide();
                $('#editCalendarHeader').show();
            } else {
                $('#createCalendarHeader').show();
                $('#editCalendarHeader').hide();
            }
            
            $('#modal_calendar').modal('show');
        }

        function onTimeChanged(e) {
            if (e && e.target) {
                var newVal = "";
                var value = $(e.target).val();

                var m = moment(value, moment.HTML5_FMT.TIME);
                var res = m.isValid();
                if (res)
                    newVal = m.format(moment.HTML5_FMT.TIME);

                $(e.target).val(newVal);
            }
           
        }
       
        function validateEditCalendarRec() {
            
            var res = true;
            var name = $('#<%= RecName.ClientID %>').val();
            var start = $('#<%= RecStartDate.ClientID %>').val();
            var end = $('#<%= RecEndDate.ClientID %>').val();
            if (!name || !start || !end) {
                res = false;

                if (!name)
                    $('#RecName-error').show();
                else
                    $('#RecName-error').hide();

                if (!start)
                    $('#RecStartDate-error').show();
                else
                    $('#RecStartDate-error').hide();

                if (!end)
                    $('#RecEndDate-error').show();
                else
                    $('#RecEndDate-error').hide();
            }
            else {
                $('#RecName-error').hide();
                $('#RecStartDate-error').hide();
                $('#RecEndDate-error').hide();
                $('#RecStartTime-error').hide();
                $('#RecEndTime-error').hide();
                var st = $('#<%= RecStartTime.ClientID %>').val();
                var et = $('#<%= RecEndTime.ClientID %>').val();
                var startDate = start + st;
                var endDate = end +  et;


                var stT = moment(startDate, dateFormat.toUpperCase() + moment.HTML5_FMT.TIME);
                var etT = moment(endDate, dateFormat.toUpperCase() + moment.HTML5_FMT.TIME);

                var isOk = etT.diff(stT, 'minutes')>=0;
                //stT.isBefore(etT, 'minute');
                
                if (!isOk) {
                    res = false;
                    $('#RecDate-error').show();
                } else {
                    $('#RecDate-error').hide();
                }
                
            }

            return res;
        }

        function editRec(id, name, start, startTime, end, endTime) {
            $('#RecName-error').hide();
            $('#RecStartDate-error').hide();
            $('#RecEndDate-error').hide();
            $('#RecStartTime-error').hide();
            $('#RecEndTime-error').hide();
            $('#RecDate-error').hide();
            $('#<%= RecId.ClientID %>').val(id);
            $('#<%= RecName.ClientID %>').val(name);
            $('#<%= RecStartDate.ClientID %>').val(start);
            $('#<%= RecStartTime.ClientID %>').val(startTime);
            $('#<%= RecEndDate.ClientID %>').val(end);
            $('#<%= RecEndTime.ClientID %>').val(endTime);
            

            if (id > 0) {
                $('#createRecHeader').hide();
                $('#editRecHeader').show();
            } else {
                $('#createRecHeader').show();
                $('#editRecHeader').hide();
            }
            $('#modal_rec').modal('show');
        }


        function deleteRec(id) {
            $('#<%= RecId.ClientID %>').val(id);
            
            $('#modal_delete_rec').modal('show');
        }

        function deleteCalendar(id, ids) {
            $('#<%= CalendarId.ClientID %>').val(id);



            if (ids) {
                $('#<%= workSchedule.ClientID %>').show();
                $('#<%= bDelCal.ClientID %>').hide();
                $('#<%= warnMessage.ClientID %>').hide();
                $('#workScheduleIds').text(ids);
            } else {
                $('#<%= workSchedule.ClientID %>').hide();
                $('#<%= bDelCal.ClientID %>').show();
                $('#<%= warnMessage.ClientID %>').show();
            }


            $('#modal_delete_cal').modal('show');
        }

    </script>


</asp:Content>

  
