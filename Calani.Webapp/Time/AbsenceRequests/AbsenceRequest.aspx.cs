﻿using Calani.BusinessObjects.AbsenceRequests;
using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Scheduler;
using Calani.BusinessObjects.TimeManagement;
using Calani.BusinessObjects.TimeManagement.Models;
using Calani.BusinessObjects.TimeManagement.Services;
using Calani.BusinessObjects.WorkSchedulers;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

public partial class Absence_Request_Edit : System.Web.UI.Page
{
    private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

    public PageHelper ph;

    private AbsenceRequestsManager _absenceRequestManager;
    private EmployeesManager.EmployeesContractsManager _contractsManager;
    private EmployeesManager _employeesManager;
    private OrganizationManager _organizationManager;
    private VacationReportService _vacationReportService;

    protected WorkingTimeCalculator WorkingTimeCalculator;

    public double RemainingVacationDays = 0;
    public bool IsAdmin = false;

    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    public string CurrentId { get; set; }

    public ValidationEnum ValidationStatus { get; set; }
    public string CancellationStatus { get; private set; }

    public double UserWorkingRate { get; set; }
    public string EmployeeName { get; set; }
    public string StartTime { get; set; }
    public string EndTime { get; set; }
    public double HalfWorkingDayHours { get; set; }
    public WorkDayModel WorkDay { get; set; }
    public TimesheetReportModel TimesheetsReport { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        _absenceRequestManager = new AbsenceRequestsManager(ph.CurrentOrganizationId);
        _contractsManager = new EmployeesManager.EmployeesContractsManager(ph.CurrentOrganizationId);
        _employeesManager = new EmployeesManager(ph.CurrentOrganizationId);
        _organizationManager = new OrganizationManager(ph.CurrentOrganizationId);
        _organizationManager.Load();
        var vacationCarriedOverDaysManager = new VacationCarriedOverDaysManager(ph.CurrentOrganizationId);
        
        TimesheetsReport = new TimesheetReportModel();
        var workScheduleRecManager = new WorkScheduleRecManager(ph.CurrentOrganizationId);
        var workScheduleService = new WorkScheduleService(_organizationManager, workScheduleRecManager);
        WorkDay = workScheduleService.GetWorkDaySettings();
        WorkingTimeCalculator = new WorkingTimeCalculator(WorkDay);

        _vacationReportService = new VacationReportService(_employeesManager, _contractsManager, _organizationManager,
            _absenceRequestManager, vacationCarriedOverDaysManager, WorkingTimeCalculator);
        
        ph.ItemLabel = Resources.Resource.AbsenceRequest;

        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
            btnDelete, btnMore, btnMoreDdl, lblTitle, linkBackList, linkNew);

        //CurrentId = "-1";

        IsAdmin = ph.CurrentUserType != ContactTypeEnum.User_Employee;
        panelValidation.Enabled = IsAdmin;
        panelValidation.Visible = IsAdmin;
        panelCancellation.Enabled = false;
        panelCancellation.Visible = false;
        panelCancellationValidation.Visible = false;
        panelCancellationValidation.Enabled = false;

        if (ph.CurrentId != null)
        {
            CurrentId = ph.CurrentId.ToString();
        }

        if (!IsPostBack && ph.CurrentId != null)
        {
            ph.SetStateEdit(Resources.Resource.EditAbsenceRequest);
        }

        if (ph.CurrentId == null)
        {
            ph.SetStateNew(Resources.Resource.NewAbsenceRequest);
        }

        if (!IsPostBack)
        {
            Load();
        }
      
    }

    #region CRUD operations

    private void Load()
    {
        ddlAbsenceCategory.DataSource = GetAbsenceCategoriesDataSource();
        ddlAbsenceCategory.DataBind();

        DisableValidationButtons();

        long employeeId = 0;

        if (!string.IsNullOrEmpty(CurrentId))
        {
            var entity = LoadDataFromEntity(long.Parse(CurrentId));
            employeeId = entity.employeeId;
        }
        else if(IsAdmin)
        {          
            employeeId = ph.CurrentUserId;
            BindEmployeesDdl(selectedEmployeeId : ph.CurrentUserId);
            ToggleEmployeesDdlVisibility(ddlVisible: true);
            StartTime = string.Empty;
            EndTime = string.Empty;
        }
        else
        {
            employeeId = ph.CurrentUserId;
            EmployeeName = ph.CurrentUserName;
            ToggleEmployeesDdlVisibility(ddlVisible: false);
        }

        SetContractData(employeeId);
        SetTimesheetReport(employeeId);
        HalfWorkingDayHours = WorkingTimeCalculator.WorkingDayDuration.TotalHours / 2 * UserWorkingRate;
    }

    private absence_requests LoadDataFromEntity(long currentId)
    {
        var entity = _absenceRequestManager.Get(currentId);

        ToggleEmployeesDdlVisibility(ddlVisible: false);

        EmployeeName = entity.contacts4 != null ? entity.contacts4.firstName + " " + entity.contacts4.lastName : string.Empty;

        tbxComment.Value = entity.comment;
        var dateFormat = "yyyy/MM/dd";
        var timeFormat = "HH:mm";
        var startDate = entity.startDate;
        var endDate = entity.endDate;
        hdfStartDate.Value = startDate.ToString(dateFormat, CultureInfo.InvariantCulture);
        hdfEndDate.Value = endDate.ToString(dateFormat, CultureInfo.InvariantCulture);
        StartTime = startDate.ToString(timeFormat, CultureInfo.InvariantCulture);
        hdfStartTime.Value = StartTime;
        EndTime = endDate.ToString(timeFormat, CultureInfo.InvariantCulture);
        hdfEndTime.Value = EndTime;
        ddlAbsenceCategory.ClearSelection();
        var categoryItem = ddlAbsenceCategory.Items.FindByValue(entity.absenceCategory.ToString());
        if (categoryItem != null)
        {
            categoryItem.Selected = true;
        }

        var model = new AbsenceRequestGridModel(entity, WorkingTimeCalculator);
        var isOwnRequest = ph.CurrentUserId == model.EmployeeId;
        ValidationStatus = model.ValidationStatus;
        switch (ValidationStatus)
        {
            case ValidationEnum.Pending:
                EnableValidationButtons();

                switch (model.CancellationStatus)
                {
                    case CancellationStatusEnum.CancellationRejected:
                        CancellationStatus = ResourceHelper.Get(model.CancellationStatus.GetDescription());
                        panelCancellation.Visible = false;
                        panelCancellation.Enabled = false;
                        break;
                    case CancellationStatusEnum.Cancelled:
                        DisableSaveButton();
                        EnableAdminDeleteButton();
                        panelCancellation.Enabled = false;
                        panelCancellation.Visible = false;
                        panelValidation.Enabled = false;
                        panelValidation.Visible = false;
                        panelCancellationValidation.Enabled = false;
                        panelCancellationValidation.Visible = false;
                        break;
                    case CancellationStatusEnum.None:
                    case CancellationStatusEnum.CancellationRequested:
                    default:
                        panelCancellation.Enabled = false;
                        panelCancellation.Visible = false;
                        break;
                }

                break;
            case ValidationEnum.Approved:
                DisableSaveButton();
                EnableAdminDeleteButton();

                switch (model.CancellationStatus)
                {
                    case CancellationStatusEnum.CancellationRequested:
                        panelCancellationValidation.Visible = IsAdmin;
                        panelCancellationValidation.Enabled = IsAdmin;
                        panelValidation.Visible = false;
                        CancellationStatus = ResourceHelper.Get(model.CancellationStatus.GetDescription());
                        break;
                    case CancellationStatusEnum.Cancelled:
                    case CancellationStatusEnum.CancellationRejected:
                        CancellationStatus = ResourceHelper.Get(model.CancellationStatus.GetDescription());
                        panelCancellation.Visible = false;
                        panelCancellation.Enabled = false;
                        break;
                    case CancellationStatusEnum.None:
                    default:
                        panelCancellation.Enabled = isOwnRequest;
                        panelCancellation.Visible = isOwnRequest;
                        break;
                }

                break;
            case ValidationEnum.Rejected:
            default:
                DisableSaveButton();
                EnableAdminDeleteButton();
                break;
        }

        return entity;
    }

    private void BindEmployeesDdl(long selectedEmployeeId)
    {
        ddlEmployee.DataSource = GetEmployeesDatasource();
        ddlEmployee.DataBind();

        var currentEmployee = ddlEmployee.Items.FindByValue(selectedEmployeeId.ToString());
        if (currentEmployee != null)
        {
            currentEmployee.Selected = true;
        }
    }

    private void RequestCancellation()
    {
        if (!string.IsNullOrEmpty(CurrentId))
        {
            var entity = _absenceRequestManager.Get(long.Parse(CurrentId));
            if (entity.approvedAt.HasValue && entity.rejectedAt == null)
            {
                entity.cancellationRequestedAt = DateTime.UtcNow;
                entity.updatedAt = DateTime.UtcNow;
                entity.updatedBy = ph.CurrentUserId;

                var result = _absenceRequestManager.SaveOrUpdate(entity.id, entity);
                if (result.Success)
                {
                    var message = Resources.Resource.CancelAwaitingApproval_Var;
                    ph.DisplaySuccess(message);
                    Load();
                }
                else
                {
                    ph.DisplayError(Resources.Resource.ProcessingError);
                }
            }
        }
    }

    private void ApproveCancellation()
    {
        if (!string.IsNullOrEmpty(CurrentId))
        {
            var entity = _absenceRequestManager.Get(long.Parse(CurrentId));
            if (entity.approvedAt.HasValue && entity.rejectedAt == null)
            {
                entity.cancellationApprovedAt = DateTime.UtcNow;
                entity.cancellationApprovedBy = ph.CurrentUserId;
                entity.approvedAt = null;
                entity.approvedBy = null;

                var result = _absenceRequestManager.SaveOrUpdate(entity.id, entity);
                if (result.Success)
                {
                    var message = Resources.Resource.CancelApproved_Var;
                    ph.DisplaySuccess(message);
                    Load();
                }
                else
                {
                    ph.DisplayError(Resources.Resource.ProcessingError);
                }
            }
        }
    }

    private void RejectCancellation()
    {
        if (!string.IsNullOrEmpty(CurrentId))
        {
            var entity = _absenceRequestManager.Get(long.Parse(CurrentId));
            if (entity.approvedAt.HasValue && entity.rejectedAt == null)
            {
                entity.cancellationRejectedAt = DateTime.UtcNow;
                entity.cancellationRejectedBy = ph.CurrentUserId;
                entity.approvedAt = null;
                entity.approvedBy = null;

                var result = _absenceRequestManager.SaveOrUpdate(entity.id, entity);
                if (result.Success)
                {
                    var message = Resources.Resource.CancelApproved_Var;
                    ph.DisplaySuccess(message);
                    Load();
                }
                else
                {
                    ph.DisplayError(Resources.Resource.ProcessingError);
                }
            }
        }
    }

    private void Update(ValidationEnum? status = null)
    {
        if (!string.IsNullOrEmpty(CurrentId))
        {
            var startDate = GetStartDate();
            var endDate = GetEndDate();

            var entity = _absenceRequestManager.Get(long.Parse(CurrentId));

            string validationMessage;
            if (!IsDurationValid(entity.employeeId, startDate, endDate, out validationMessage))
            {
                ph.DisplayError(validationMessage);
                return;
            }

            entity.startDate = startDate;
            entity.endDate = endDate;
            entity.comment = tbxComment.Value;
            entity.absenceCategory = int.Parse(ddlAbsenceCategory.SelectedValue);
            entity.updatedAt = DateTime.UtcNow;
            entity.updatedBy = ph.CurrentUserId;

            if (status.HasValue && entity.approvedAt == null && entity.rejectedAt == null)
            {
                switch (status)
                {
                    case ValidationEnum.Approved:
                        entity.rejectedAt = null;
                        entity.rejectedBy = null;
                        entity.approvedBy = ph.CurrentUserId;
                        entity.approvedAt = DateTime.UtcNow;
                        break;
                    case ValidationEnum.Rejected:
                        entity.approvedAt = null;
                        entity.approvedBy = null;
                        entity.rejectedBy = ph.CurrentUserId;
                        entity.rejectedAt = DateTime.UtcNow;
                        break;
                    default:
                        entity.rejectedAt = null;
                        entity.rejectedBy = null;
                        entity.approvedAt = null;
                        entity.approvedBy = null;
                        break;
                }
            }

            var result = _absenceRequestManager.SaveOrUpdate(entity.id, entity);
            if (result.Success)
            {
                var message = (status.HasValue && status == ValidationEnum.Approved) ? Resources.Resource.Approved_var :
                    (status.HasValue && status == ValidationEnum.Rejected) ? Resources.Resource.Rejected_var :
                    Resources.Resource.Updated_var;

                ph.DisplaySuccess(message);
                Load();
            }
            else
            {
                ph.DisplayError(Resources.Resource.ProcessingError);
            }
        }
    }

    private void Create()
    {
        var startDate = GetStartDate();
        var endDate = GetEndDate();
        var employeeId = ddlEmployee.Enabled ? long.Parse(ddlEmployee.SelectedValue) : ph.CurrentUserId;

        string validationMessage;
        if (!IsDurationValid(employeeId, startDate, endDate, out validationMessage))
        {
            ph.DisplayError(validationMessage);
            return;
        }

        var requests = _absenceRequestManager.List();
        var duplicate = requests.FirstOrDefault(x => 
            x.startDate < endDate && startDate < x.endDate
               && x.employeeId == employeeId
               && x.rejectedBy == null
               && x.cancellationApprovedBy == null);
        if (duplicate != null)
        {
            ph.DisplayError(Resources.Resource.DuplicateAbsenceRequest + $" <a href=\"AbsenceRequest.aspx?id={duplicate.id}\">{Resources.Resource.Open}</a>");
            return;
        }

        var entity = new absence_requests
        {
            startDate = startDate,
            endDate = endDate,
            absenceCategory = int.Parse(ddlAbsenceCategory.SelectedValue),
            comment = tbxComment.Value,
            employeeId = employeeId,
            createdAt = DateTime.UtcNow,
            createdBy = ph.CurrentUserId,
            organizationId = ph.CurrentOrganizationId
        };

        if (IsAdmin && ph.CurrentUserId != employeeId)
        {
            // If admin creates an absence request for another user, make it validated
            entity.approvedAt = DateTime.UtcNow;
            entity.approvedBy = ph.CurrentUserId;
        }

        var result = _absenceRequestManager.SaveOrUpdate(-1, entity);

        if (result.Success && result.Record.id > 0)
        {
            ph.DisplaySuccess(Resources.Resource.Created_var);
            CurrentId = result.Record.id.ToString();

            Load();

            DisableValidationButtons();
        }
        else
        {
            ph.DisplayError(Resources.Resource.ProcessingError);
        }
    }
    
    protected void Delete()
    {
        var entity = _absenceRequestManager.Get(long.Parse(CurrentId));
        if(entity == null || (!IsAdmin && entity.approvedAt != null))
        {
            ph.DisplayError(Resources.Resource.ProcessingError);
            return;
        }

        var ret = _absenceRequestManager.Delete(ph.CurrentId.Value);
        if (ret.Success)
        {
            panelEdit.Visible = false;
            ph.DisplaySuccess(Resources.Resource.Deleted_var);
        }
    }

    #endregion

    #region Buttons (triggers)
    
    protected void btnSave_Click(object sender, EventArgs e)
    {
        btnSave.Enabled = false;
        if (!string.IsNullOrEmpty(CurrentId))
        {
            Update();
        }
        else 
        {
            Create();
        }
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null)
        {
            Delete();
        }
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null)
        {
            Update(ValidationEnum.Approved);
        }
    }

    protected void btnReject_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null)
        {
            Update(ValidationEnum.Rejected);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        if(ph.CurrentId != null)
        {
            RequestCancellation();
        }
    }

    protected void ddlEmployee_SelectedIndexChanged(object sender, EventArgs e)
    {
        var selectedEmployeeId = long.Parse(ddlEmployee.SelectedValue);
        SetContractData(selectedEmployeeId);
        SetTimesheetReport(selectedEmployeeId);
    }

    protected void btnApprovecancellation_Click(object sender, EventArgs e)
    {
        ApproveCancellation();
    }

    protected void btnRejectCancellation_Click(object sender, EventArgs e)
    {
        RejectCancellation();
    }

    #endregion

    #region Private Methods

    private void ToggleEmployeesDdlVisibility(bool ddlVisible)
    {
        ddlEmployee.Visible = ddlVisible;
        ddlEmployee.Enabled = ddlVisible;
        employeeIcon.Visible = ddlVisible;
        employeeName.Visible = !ddlVisible;
    }

    private void SetContractData(long employeeId)
    {
        var report = new VacationReportModel();

        try
        {
            report = _vacationReportService.GetAnnualVacationReport(employeeId);
        }
        catch(Exception ex)
        {
            _log.Error(ex.Message, ex);

            report.TextReport = ex.Message;
        }

        var contract = _contractsManager.GetActiveContractDto(employeeId);
        UserWorkingRate = contract != null ? (double)contract.Rate / 100 : 1;
        RemainingVacationDays = report.VacationDaysRemaining;
    }

    private void SetTimesheetReport(long employeeId)
    {
        var timesheetReportService = new TimesheetReportService(
                                            _contractsManager,
                                            new TimeSheetsService(new TimeSheetsManager(ph.CurrentOrganizationId)),
                                            new TimeSheetRecordsManager(ph.CurrentOrganizationId),
                                            new EmployeesOvertimeManager(ph.CurrentOrganizationId),
                                            new AbsenceRequestsManager(ph.CurrentOrganizationId, _contractsManager, _employeesManager),
                                            new OrganizationManager(ph.CurrentOrganizationId),
                                            new WorkScheduleRecManager(ph.CurrentOrganizationId));

        TimesheetsReport = timesheetReportService.CalculateTimesheetReportForPeriod(ph.CurrentOrganizationId, employeeId, new DateRange(DatesRangeEnum.Since1990));
    }

    private bool IsDurationValid(long employeeId, DateTime start, DateTime end, out string message)
    {
        if(start > end)
        {
            message = Resources.Resource.InvalidStartEndDates;
            return false;
        }

        message = null;
        return true;
    }

    private List<KeyValuePair<int, string>> GetAbsenceCategoriesDataSource()
    {
        var categories = Enum.GetValues(typeof(AbsenceCategoryEnum)) as AbsenceCategoryEnum[];
        var list = categories.Select(c => new KeyValuePair<int, string>((int)c, ResourceHelper.Get(c.GetDescription())));

        return list.ToList();
    }

    private List<KeyValuePair<long, string>> GetEmployeesDatasource()
    {
        var employees = _employeesManager.ListEmployees();
        var list = employees.Select(e => new KeyValuePair<long, string>(e.id, string.Format("{0} {1} ({2})", e.firstName, e.lastName, e.initials))).ToList();

        return list;
    }

    private DateTime GetStartDate()
    {
        var localDate = DateTime.ParseExact(hdfStartDate.Value, "yyyy/MM/dd", CultureInfo.InvariantCulture);
        TimeSpan? localTime = null;
        if(!string.IsNullOrWhiteSpace(hdfStartTime.Value))
        {
            var date = DateTime.ParseExact(hdfStartTime.Value, "HH:mm", CultureInfo.InvariantCulture);
            localTime = new TimeSpan(date.Hour, date.Minute, 0);
        }

        var startTime = localTime.HasValue ? localTime.Value : WorkingTimeCalculator.StartWorkingTime;
        var result = new DateTime(localDate.Year, localDate.Month, localDate.Day, startTime.Hours, startTime.Minutes, startTime.Seconds, 0);
        return result;
    }

    private DateTime GetEndDate()
    {
        var localDateTime = DateTime.ParseExact(hdfEndDate.Value, "yyyy/MM/dd", CultureInfo.InvariantCulture);
        TimeSpan? localTime = null;
        if (!string.IsNullOrWhiteSpace(hdfEndTime.Value))
        {
            var date = DateTime.ParseExact(hdfEndTime.Value, "HH:mm", CultureInfo.InvariantCulture);
            localTime = new TimeSpan(date.Hour, date.Minute, 0);
        }
        var endTime = localTime.HasValue ? localTime.Value : WorkingTimeCalculator.EndWorkingTime;
        var result = new DateTime(localDateTime.Year, localDateTime.Month, localDateTime.Day, endTime.Hours, endTime.Minutes, endTime.Seconds, 0);
        return result;
    }

    private void DisableValidationButtons()
    {
        panelValidation.Visible = false;
        panelValidation.Enabled = false;
        btnReject.CssClass = btnReject.Attributes["class"] + " disabled";
        btnApprove.CssClass = btnApprove.Attributes["class"] + " disabled";
    }

    private void EnableValidationButtons()
    {
        if (!IsAdmin) return;
        panelValidation.Visible = true;
        panelValidation.Enabled = true;
        btnReject.CssClass = btnReject.Attributes["class"].Replace(" disabled", "");
        btnApprove.CssClass = btnApprove.Attributes["class"].Replace(" disabled", "");
    }

    private void DisableSaveButton()
    {
        panelSaveDelete.Enabled = false;
        btnSave.CssClass = btnSave.Attributes["class"] + " disabled";
        btnMore.Disabled = true;
        btnMore.Attributes["class"] = btnMoreDdl.Attributes["class"] + " disabled";
    }

    private void EnableAdminDeleteButton()
    {
        if (!IsAdmin) return;
        panelAdminDelete.Enabled = true;
        panelAdminDelete.Visible = true;
    }

    #endregion Private Methods
}