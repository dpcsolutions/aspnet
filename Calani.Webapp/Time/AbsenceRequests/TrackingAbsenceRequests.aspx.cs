﻿using Calani.BusinessObjects.CustomerAdmin;
using System;

namespace Calani.Webapp.Time.AbsenceRequests
{
    public partial class TrackingAbsenceRequests : System.Web.UI.Page
    {
        public PageHelper ph;
        public string OptionsPeriod { get; set; }

        protected override void InitializeCulture()
        {
            ph = new PageHelper(this);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var orgMgr = new OrganizationManager(ph.CurrentOrganizationId);
            orgMgr.Load();

            OptionsPeriod = ph.RenderPeriodsListOption(preselect: orgMgr.DefaultPeriodFilter);
        }
    }
}