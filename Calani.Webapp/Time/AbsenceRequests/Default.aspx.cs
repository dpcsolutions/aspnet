﻿using Calani.BusinessObjects;
using Calani.BusinessObjects.AbsenceRequests;
using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Scheduler;
using Calani.BusinessObjects.WorkSchedulers;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Calani.BusinessObjects.Model;

public partial class Time_AbsenceRequests_Default : System.Web.UI.Page
{
    private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

    private AbsenceRequestsManager _absenceRequestsManager;
    private WorkingTimeCalculator _workingTimeCalculator;

    public PageHelper ph;
    public string OptionsPeriod { get; set; }
    public string Status { get; private set; }
    public string StringResourcesJson { get; private set; }
    

    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var orgMgr = new OrganizationManager(ph.CurrentOrganizationId);
        orgMgr.Load();
        var db = new CalaniEntities();

        var contractsManager = new EmployeesManager.EmployeesContractsManager(ph.CurrentOrganizationId);
        var employeesManager = new EmployeesManager(ph.CurrentOrganizationId);
        var organizationsManager = new OrganizationManager(ph.CurrentOrganizationId);
        var workScheduleRecManager = new WorkScheduleRecManager(ph.CurrentOrganizationId);
        var workScheduleService = new WorkScheduleService(organizationsManager, workScheduleRecManager);
        var workDay = workScheduleService.GetWorkDaySettings();
        _workingTimeCalculator = new WorkingTimeCalculator(workDay);
        _absenceRequestsManager = new AbsenceRequestsManager(ph.CurrentOrganizationId, contractsManager, employeesManager);
        
        var holidayList = db.calendars_rec
            .Where(x => x.name == "Férié" && x.calendars.organization_Id == ph.CurrentOrganizationId && x.recordStatus != -1)
            .Include(x => x.calendars)
            .ToList();
        
        long? filterEmployee = null;
        // remove records for employee
        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            filterEmployee = ph.CurrentUserId;
        }

        OptionsPeriod = ph.RenderPeriodsListOption(preselect: orgMgr.DefaultPeriodFilter);

        EnableViewState = false;
        if (!IsPostBack)
        {
            StringResourcesJson = SharedResource.ReadStringResources(CultureInfo.CurrentCulture);

            var data = _absenceRequestsManager.ListAbsenceRequests(approved: false, employee: filterEmployee)
                .OrderBy(entity => entity.employeeId).ThenBy(entity => entity.startDate)
                .Select(entity => new AbsenceRequestGridModel(entity, _workingTimeCalculator, holidayList))
                .ToList();

            var archives = _absenceRequestsManager.ListAbsenceRequests(approved: true, employee: filterEmployee)
                .OrderBy(entity => entity.employeeId).ThenBy(entity => entity.startDate)
                .Select(entity => new AbsenceRequestGridModel(entity, _workingTimeCalculator, holidayList))
                .ToList();

            data.AddRange(archives);

            foreach (var item in data)
            {
                item.AbsenceCategory = ResourceHelper.Get(item.AbsenceCategory);
                item.CancellationStatusTextLocalized = ResourceHelper.Get(item.CancellationStatusTextLocalized);
            }

            grid.DataSource = data;
            grid.DataBind();
            lblNoData.Visible = data.Count == 0;

            List<string> rowData = new List<string>() { "date", "status" };
            new Html5GridView(grid, false, rowData, addDateOrder: false);
        }

        var status = GetStatusFilterFromUrl();
        if (!string.IsNullOrEmpty(status))
        {
            Status = status;
        }
    }

    private string GetStatusFilterFromUrl()
    {
        var value = Context.Request.Params.Get("status");

        if (!string.IsNullOrEmpty(value) &&
           !new string[] { "0", "1", "2", "3" }.Any(item => item.Equals(value, StringComparison.InvariantCulture)))
        {
            value = null;
        }

        return value;
    }
}