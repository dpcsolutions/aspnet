﻿<%@ Page Title="Edit Absence Request" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Absence_Request_Edit" Codebehind="AbsenceRequest.aspx.cs" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/assets/js/shared_business_logic/working_time_calculator.js") %>"></script>
	<!-- Include Mobiscroll -->
    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/assets/js/plugins/ui/mobiscroll.javascript.min.js") %>"></script>
	<link href="<%= Page.ResolveClientUrl("~/assets/css/mobiscroll.javascript.min.css") %>" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/assets/js/plugins/forms/selects/chosen.jquery.min.js") %>"></script>
	<link href="<%= Page.ResolveClientUrl("~/assets/css/chosen.min.css") %>" rel="stylesheet" type="text/css">

	<style type="text/css">
		.absence-request-datepicker {
			border: 1px solid #ddd;
			border-left: none;
			font-family: "Roboto", Helvetica Neue, Helvetica, Arial, sans-serif;
			font-size: 13px;
		}
	</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <form runat="server"  >

        <div class="content">

		    <div class="row">

                <!-- left column -->
			    <div class="col-sm-12 col-md-9">


                        <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Error %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblErrorMessage" />
						    </div>
					    </asp:Panel><!-- panelError -->

                        <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Done %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblSuccess" />
                                <br />
                                <strong><a href="." id="linkBackList" runat="server"></a></strong>
                                    <%= Resources.Resource.or %>
                                <a href="AbsenceRequest.aspx" id="linkNew" runat="server"></a>.
						    </div>
					    </asp:Panel><!-- panelSuccess -->
					    
					    <asp:Panel runat="server" ID="panelEdit" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><asp:Label runat="server" ID="lblTitle" /></h6>
						    </div>

						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">

										
										<% if (!string.IsNullOrEmpty(CancellationStatus)) {%>
										<div class="form-group">
											<label class="control-label col-lg-3"><%= Resources.Resource.CancellationStatus %></label>
											<label class="control-label col-lg-9"><%= CancellationStatus %></label>
										</div>
                                        <%}%>
											
										<div class="form-group">
										    <label class="control-label col-lg-3"><%= Resources.Resource.Employee %></label>
											<label runat="server" id="employeeName" class="control-label col-lg-9"><%= EmployeeName %></label>
										    <div class="col-lg-9">
											    <div class="input-group">
													<span class="input-group-addon" runat="server" id="employeeIcon"><i class="icon-user"></i></span>
													<asp:DropDownList runat="server" CssClass="form-control" ID="ddlEmployee"
                                                        DataValueField="Key" DataTextField="Value" OnSelectedIndexChanged="ddlEmployee_SelectedIndexChanged"
														AutoPostBack="true"/>
											    </div>
										    </div>
									    </div>

                                        <!-- date -->
									    <div class="form-group">
											<label class="control-label col-lg-3"><%= Resources.Resource.Start %></label>
										    <div class="col-lg-4">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-calendar"></i></span>
													<label class="" for="absence-request-range" style="margin: 0">
														<input mbsc-input id="absence-request-start" class="absence-request-datepicker"></input>
													</label>
											    </div>
										    </div>
											<div class="col-lg-3">
												<div class="input-group time-picker-container">
												    <span class="input-group-addon"><i class="fa fa-clock"></i></span>
													<label class="" for="absence-request-range" style="margin: 0">
														<input id="absence-request-start-time" class="form-control absence-request-datepicker" placeholder="hh:mm" disabled="disabled"
															value="<%= StartTime %>"/>
													</label>
											    </div>
											</div>
											<asp:HiddenField runat="server" ID="hdfStartDate" />
											<asp:HiddenField runat="server" ID="hdfStartTime" />
									    </div>

										<div class="form-group">
											<label class="control-label col-lg-3"><%= Resources.Resource.End %></label>
										    <div class="col-lg-4">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-calendar"></i></span>
													<label for="absence-request-range" style="margin: 0">
														<input mbsc-input id="absence-request-end" class="absence-request-datepicker"></input>
													</label>
											    </div>
										    </div>
											<div class="col-lg-3">
												<div class="input-group time-picker-container">
												    <span class="input-group-addon"><i class="fa fa-clock"></i></span>
													<label class="" for="absence-request-range" style="margin: 0">
														<input id="absence-request-end-time" class="form-control absence-request-datepicker" placeholder="hh:mm" disabled="disabled"
															value="<%= EndTime %>"/>
													</label>
											    </div>
											</div>
											<asp:HiddenField runat="server" ID="hdfEndDate" />
											<asp:HiddenField runat="server" ID="hdfEndTime" />
									    </div>

                                        <!-- /date -->

										<div class="form-group">
											<label class="control-label col-lg-3"><%= Resources.Resource.Duration %></label>
											<div>
												<label class="control-label col-lg-9">
												<strong id="durationDays">0</strong> <%= Resources.Resource.Days.ToLower() %>
												/
												<strong id="durationHours">0</strong> <%= Resources.Resource.Hours.ToLower() %>
											</label>
											</div>
										</div>

                                        <!-- absence category -->
									    <div class="form-group">
										    <label class="control-label col-lg-3"><%= Resources.Resource.Type %></label>
										    <div class="col-lg-9">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-user"></i></span>
                                                    <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlAbsenceCategory"
                                                        DataValueField="Key" DataTextField="Value" />
											    </div>
										    </div>
									    </div>
                                        <!-- /absence category -->

                                        <!-- comment -->
									    <div class="form-group">
										    <label class="control-label col-lg-3"><%= Resources.Resource.Comment %></label>
										    <div class="col-lg-9">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-font"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Comment %>' runat="server" id="tbxComment" />
											    </div>
										    </div>
									    </div>
                                        <!-- /comment -->

								    </fieldset>



                                    <!-- buttonsaction -->
								    <div class="buttonsaction text-right col-sm-12 col-md-12">

										<div class="col-xs-2">
												<a href="Default.aspx" class="btn" data-action="validate" >
													<i class="icon-arrow-left5 position-left"></i> <%= Resources.Resource.BackToList %>
												</a>
											</div>

										<div class="col-xs-5">
											<asp:Panel runat="server" ID="panelValidation">
												<asp:LinkButton runat="server" class="btn btn-success btnSave" OnClick="btnApprove_Click" ID="btnApprove">
													<i class="icon-user-check position-left"></i> <%= Resources.Resource.Approve %>
												</asp:LinkButton>
												<asp:LinkButton runat="server" class="btn btn-danger btnSave" OnClick="btnReject_Click" ID="btnReject">
													<i class="icon-user-cancel position-left"></i> <%= Resources.Resource.Reject %>
												</asp:LinkButton>
											</asp:Panel>
											<asp:Panel runat="server" ID="panelCancellationValidation">
												<asp:LinkButton runat="server" class="btn btn-success btnSave"  ID="btnApprovecancellation" OnClick="btnApprovecancellation_Click">
													<i class="icon-user-check position-left"></i> <%= Resources.Resource.ApproveCancellation %>
												</asp:LinkButton>
												<asp:LinkButton runat="server" class="btn btn-danger btnSave" ID="btnRejectCancellation" OnClick="btnRejectCancellation_Click">
													<i class="icon-user-cancel position-left"></i> <%= Resources.Resource.RejectCancellation %>
												</asp:LinkButton>
											</asp:Panel>
											<asp:Panel runat="server" ID="panelCancellation">
												<a href="javascript:void(0)" data-toggle="modal" data-target="#modal_cancel" class="btn btn-danger btnSave">
													<i class="fa fa-ban"></i> <%= Resources.Resource.Cancel %>
												</a>
											</asp:Panel>
										</div>
										

										
										<div class="col-xs-5">

											<asp:Panel runat="server" ID="panelSaveDelete" CssClass="float-right btn">
												<div class="btn-group">
													<asp:LinkButton runat="server" ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click" OnClientClick="return Validate(true);">
														<i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
													</asp:LinkButton>
                                                
													<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore"><span class="caret"></span></button>
													<ul class="dropdown-menu dropdown-menu-right" runat="server" id="btnMoreDdl">
														<li><a href="javascript:void(0)" data-toggle="modal" data-target="#modal_default"><i class="fa fa-trash"></i> <%= Resources.Resource.Delete %></a></li>
													</ul>
												</div>
											</asp:Panel>

											<asp:Panel runat="server" ID="panelAdminDelete" Visible="false" CssClass="float-right btn">
												<a href="javascript:void(0)" data-toggle="modal" data-target="#modal_default" class="btn btn-danger btnSave"><i class="fa fa-trash"></i> <%= Resources.Resource.Delete %></a>
											</asp:Panel>
											
										</div>
										
										
									</div>
                                    <!-- /buttonsaction -->


							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->
					    
           


			    </div>
			    <!-- /left column -->
                

                <!-- right column -->
                <div class="col-sm-12 col-md-6">

			    </div>
                <!-- /right column -->
						

		    </div> <!-- /row -->


					
            <!-- basic modal: confirm delete -->
		    <div id="modal_default" class="modal fade" role="dialog">
			    <div class="modal-dialog modal-dialog-centered" role="document">
				    <div class="modal-content">
					    <div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal">&times;</button>
						    <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
					    </div>

					    <div class="modal-body">
						    <h6 class="text-semibold"><%= Resources.Resource.Are_You_Sure_To_Delete %></h6>
						    <p><%= Resources.Resource.Delete_Is_Irreversible %></p>

					    </div>

					    <div class="modal-footer">
						    <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
                            <asp:Button runat="server" ID="btnDelete" OnClick="btnDelete_Click" CssClass="btn btn-primary" Text="<%$ Resources:Resource, Yes_Delete %>" />
					    </div>
				    </div>
			    </div>
		    </div>
		    <!-- /basic modal -->

			<!-- basic modal: confirm cancel -->
		    <div id="modal_cancel" class="modal fade" role="dialog">
			    <div class="modal-dialog modal-dialog-centered" role="document">
				    <div class="modal-content">
					    <div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal">&times;</button>
						    <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
					    </div>

					    <div class="modal-body">
						    <h6 class="text-semibold"><%= Resources.Resource.AreYouSureToCancel %></h6>
					    </div>

					    <div class="modal-footer">
						    <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No %></button>
                            <asp:Button runat="server" ID="Button1" OnClick="btnCancel_Click" CssClass="btn btn-primary" Text="<%$ Resources:Resource, Yes %>" />
					    </div>
				    </div>
			    </div>
		    </div>
		    <!-- /basic modal -->

			<!-- basic modal: not enough vacation days warning -->
		    <div id="modal_not_enough_vacation_days" class="modal fade" role="dialog">
			    <div class="modal-dialog modal-dialog-centered">
				    <div class="modal-content">
					    <div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal">&times;</button>
						    <h5 class="modal-title"><%= Resources.Resource.Warning %></h5>
					    </div>

					    <div class="modal-body">
						    <p>
								<%= Resources.Resource.NotEnoughVacationDays %>
								<span id="not_enough_vacation_days_message"></span>
						    </p>
					    </div>

					    <div class="modal-footer">
							<button type="button" class="btn btn-primary button-ok" data-dismiss="modal"><%= Resources.Resource.OK %></button>
							<button type="button" class="btn btn-primary button-cancel" data-dismiss="modal"><%= Resources.Resource.Cancel %></button>
							<asp:LinkButton class="btn btn-link button-save" runat="server" ID="btnSave2" OnClick="btnSave_Click"><%= Resources.Resource.Save %></asp:LinkButton>
					    </div>
				    </div>
			    </div>
		    </div>
		    <!-- /basic modal -->

			<!-- basic modal: not enough vacation days warning -->
		    <div id="modal_not_enough_hours_to_compensate" class="modal fade" role="dialog">
			    <div class="modal-dialog modal-dialog-centered">
				    <div class="modal-content">
					    <div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal">&times;</button>
						    <h5 class="modal-title"><%= Resources.Resource.Warning %></h5>
					    </div>

					    <div class="modal-body">
						    <p>
								<%= Resources.Resource.NotEnoughHoursToCompensateWarning %>
						    </p>
					    </div>

					    <div class="modal-footer">
							<button type="button" class="btn btn-primary button-ok" data-dismiss="modal"><%= Resources.Resource.OK %></button>
							<button type="button" class="btn btn-primary button-cancel" data-dismiss="modal"><%= Resources.Resource.Cancel %></button>
							<asp:LinkButton class="btn btn-link button-save" runat="server" ID="LinkButton1" OnClick="btnSave_Click"><%= Resources.Resource.Save %></asp:LinkButton>	    
					    </div>
				    </div>
			    </div>
		    </div>
		    <!-- /basic modal -->



        </div><!-- /content -->

    </form>
    <script type="text/javascript">

        var locale = lang4 == 'en-US' ? mobiscroll.localeEn : mobiscroll.localeFr;

        mobiscroll.setOptions({
            locale: locale,
            theme: 'ios',
            themeVariant: 'light'
        });

        var workingHoursCalculator = new WorkingTimeCalculator(<%= WorkDay.StartTime.Hours %>, <%= WorkDay.StartTime.Minutes %>, <%= WorkDay.EndTime.Hours %>, <%= WorkDay.EndTime.Minutes %>, <%= WorkDay.LunchDuration.TotalMinutes %>, <%= WorkDay.AbsenceDayDuration.TotalMinutes %>);

		var isAdmin = <%= IsAdmin.ToString().ToLowerInvariant() %>;

        var startDatePicker = mobiscroll.datepicker('#absence-request-start', {
            controls: ['calendar'],
			select: 'date',
			display: 'bubble',
			touchUi: true,
			onInit: function (event, inst) {
                var startDateString = '<%= hdfStartDate.Value %>';
				if (startDateString) {
                    var startDate = new Date(startDateString);
                    inst.setVal(startDate);
                }
            },
			onClose: function (event) {
				var date = event.value;
				document.getElementById('<%= hdfStartDate.ClientID %>').value = formatDate(date);
                SetVisibilityTimePickers(datesEqual(date, endDatePicker.value));
				var hours = UpdateDuration();
			},
        });

		var endDatePicker = mobiscroll.datepicker('#absence-request-end', {
            controls: ['calendar'],
            select: 'date',
		    touchUi: true,
            display: 'bubble',
			onInit: function (event, inst) {
                var endDateString = '<%= hdfEndDate.Value %>';				
				if (endDateString) {
					var endDate = new Date(endDateString);
                    inst.setVal(endDate);
                }
            },
			onClose: function (event) {
				var date = event.value;
				document.getElementById('<%= hdfEndDate.ClientID %>').value = formatDate(date);
                SetVisibilityTimePickers(datesEqual(date, startDatePicker.value));
				var hours = UpdateDuration();
            }
	   });

        var startTimePicker = $('input#absence-request-start-time')[0];
		var endTimePicker = $('input#absence-request-end-time')[0];

		$(startTimePicker).on('blur', function (e) {
            var value = e.target.value;
            var m = moment(value, moment.HTML5_FMT.TIME);
            var valid = m.isValid();

            if (!valid) {
				e.target.value = "";
                document.getElementById('<%= hdfStartTime.ClientID %>').value = "";
			}
			else {
                value = m.format(moment.HTML5_FMT.TIME);
                e.target.value = value;
				document.getElementById('<%= hdfStartTime.ClientID %>').value = value;

				// After the time value has been corrected to the right format, update duration
                UpdateDuration();
            }
        });

        $(endTimePicker).on('blur', function (e) {
            var value = e.target.value;
            var m = moment(value, moment.HTML5_FMT.TIME);
            var valid = m.isValid();

			if (!valid) {
				e.target.value = "";
				document.getElementById('<%= hdfEndTime.ClientID %>').value = "";
			}
			else {
				value = m.format(moment.HTML5_FMT.TIME);
                e.target.value = value;
				document.getElementById('<%= hdfEndTime.ClientID %>').value = value;

				// After the time value has been corrected to the right format, update duration
                UpdateDuration();
            }
        });

		async function Validate(save) {
            var hours = await UpdateDuration();
			var valid = ValidateVacationDuration(hours);
			
			valid || DisplayDurationWarning(hours / <%= WorkDay.AbsenceDayDuration.TotalHours %> - getRemainingVacationDays(), save);

			if (!valid) return valid;

			valid = ValidateRecoveryOfHoursDuration(hours);
            if (!valid) {
                DisplayHoursToCompensateWarning(save);
            }
			
			return valid;
		}

		function getAbcenseCategorySelected() {
			var absenceCategoriesOptions = document.getElementById("<%= ddlAbsenceCategory.ClientID %>");
            var absenceCategoryOption = $.grep(absenceCategoriesOptions, function (option) { return option.selected; });
			return absenceCategoryOption[0].value;
        }
		
		async function UpdateDuration() {
			if (!startDatePicker.value || !endDatePicker.value) return 0;
			
			let days = CalculateDurationInDays();
			
			let formData = getFullDateData();
			let holidayData = {days: 0, hours: 0};
			
			await $.ajax({
				type: "GET",
				contentType: "application/json; charset=utf-8",
				url: '../../API/Service.svc/GetHolidayDataForAbsence?startDate=' 
					+ moment(formData.startDate).format('YYYY-MM-DDTHH:mm:ss')
					+ '&endDate=' + moment(formData.endDate).format('YYYY-MM-DDTHH:mm:ss'),
				dataType: "json",
				success: function (res) {
					if (res.success === true){
						holidayData = $.parseJSON(res.data);
					}
				},
				error: function (e) {
					console.error(e.responseText);
				}
			});
			days -= holidayData.days;
			let hours = 0.0;
			let workingDayDurationMinutes = workingHoursCalculator.WorkingDayDurationMinutes;
			
			if (days === 1) {
				let startTime = startTimePicker.value;
				let endTime = startTimePicker.value;
				
				if (startTime !== '' && endTime !== '') {
	                let workingMinutes = workingHoursCalculator.getMinutesDifference(startTimePicker.value, endTimePicker.value);
	        
	                let halfWorkingDayMin = workingDayDurationMinutes / 2;
	                if (workingMinutes > halfWorkingDayMin) {
	                    workingMinutes -= workingHoursCalculator._lunchBreakDurationMin;
	                }
	        
	                if (workingMinutes >= workingDayDurationMinutes) {
	                    workingMinutes = workingDayDurationMinutes;
	                }
	                
	                hours = workingMinutes / 60;
					days = hours / workingHoursCalculator.WorkingDayDurationHours;
				} else {
					hours = days * workingDayDurationMinutes / 60;
				}
				
			} else {
				hours = days * workingDayDurationMinutes / 60;
			}
			hours = days > 0 ? hours : 0.0;
			
			document.getElementById('durationHours').innerText = (hours).toFixed(2);
			document.getElementById('durationDays').innerText = (days).toFixed(2);

			return hours;
		}

		function SetVisibilityTimePickers(visible) {
            $('#absence-request-start-time').attr('disabled', !visible);
			$('#absence-request-end-time').attr('disabled', !visible);

			if (!visible) {
                $(startTimePicker).val("");
				$(endTimePicker).val("");
            }
		}

		function ValidateVacationDuration(hours) {
			var valid = true;
			var absCategory = getAbcenseCategorySelected();
            var isVacation = absCategory == <%= ((int)Calani.BusinessObjects.Enums.AbsenceCategoryEnum.Vacation).ToString()%>;
			if (isVacation) {
                var days = (hours * 100 / workingHoursCalculator.WorkingDayDurationHours) / 100;
                valid = days <= getRemainingVacationDays();
            }
			
			return valid;
		}

        function ValidateRecoveryOfHoursDuration(hours) {
            var valid = true;
            var absCategory = getAbcenseCategorySelected();
            var isRecovery = absCategory == <%= ((int)Calani.BusinessObjects.Enums.AbsenceCategoryEnum.RecoveryOfHours).ToString()%>;
            if (isRecovery) {
                valid = hours <= getHoursToBeCompensated();
            }

            return valid;
        }

		function getRemainingVacationDays() {
			return <%= RemainingVacationDays.ToString("F", System.Globalization.CultureInfo.InvariantCulture) %>;
		}

        function getHoursToBeCompensated() {
            return <%= TimesheetsReport.TotalHoursToBeCompensated.ToString("F", System.Globalization.CultureInfo.InvariantCulture) %>;
        }

		function DisplayDurationWarning(overDays, save) {
            if (save) {
				$('#modal_not_enough_vacation_days .button-save,#modal_not_enough_vacation_days .button-cancel').show();
				$('#modal_not_enough_vacation_days .button-ok').hide();
			}
			else {
                $('#modal_not_enough_vacation_days .button-save,#modal_not_enough_vacation_days .button-cancel').hide();
                $('#modal_not_enough_vacation_days .button-ok').show();
            }
            
			$('#modal_not_enough_vacation_days').modal('show');
            document.getElementById('not_enough_vacation_days_message').innerText = '<%= Resources.Resource.YouExceedByXDays %>'.replace("{0}", overDays);
		}

        function DisplayHoursToCompensateWarning(save) {
            if (save) {
                $('#modal_not_enough_hours_to_compensate .button-save,#modal_not_enough_hours_to_compensate .button-cancel').show();
                $('#modal_not_enough_hours_to_compensate .button-ok').hide();
            }
            else {
                $('#modal_not_enough_hours_to_compensate .button-save,#modal_not_enough_hours_to_compensate .button-cancel').hide();
                $('#modal_not_enough_hours_to_compensate .button-ok').show();
			}

			$('#modal_not_enough_hours_to_compensate').modal('show');
        }

		function getFullDateData() {
            let startTimeObject = getTimepickerValue($(startTimePicker).val());
            let endTimeObject = getTimepickerValue($(endTimePicker).val());
            let timeEnabled = startTimeObject && endTimeObject;
            let endHour = timeEnabled ? endTimeObject.hours : workingHoursCalculator.EndWorkingHour;
            let endMin = timeEnabled ? endTimeObject.minutes : workingHoursCalculator.EndWorkingMin;
            let startHour = timeEnabled ? startTimeObject.hours : workingHoursCalculator.StartWorkingHour;
            let startMin = timeEnabled ? startTimeObject.minutes : workingHoursCalculator.StartWorkingMin;
			
			return {
				startDate: workingHoursCalculator.getSameDayDifferentTime(startDatePicker.value, startHour, startMin),
				endDate: workingHoursCalculator.getSameDayDifferentTime(endDatePicker.value, endHour, endMin)
			}
		}
		
		function CalculateDurationInWorkingHours() {
			let dateData = getFullDateData();
			let minutes = workingHoursCalculator.getWorkingMinutesBetween2Dates(dateData.startDate, dateData.endDate);
			let hours = minutes * 1.0 / 60;
            
			hours = Math.round(hours * 100) / 100;
			
			return hours;
		}

		function getTimepickerValue(value) {
			if (!value || typeof (value) !== 'string' || value.indexOf(":") == -1) {
				return null;
			}

            var m = moment(value, moment.HTML5_FMT.TIME);
			var valid = m.isValid();

			var hours = 0, minutes = 0;
			if (valid) {
                value = m.format(moment.HTML5_FMT.TIME);
                var parts = value.split(":");
                hours = 1 * parts[0];
                minutes = 1 * parts[1];
			}
            
			return {
				"hours": hours,
				"minutes": minutes,
			};
        }

		function CalculateDurationInWorkingDays() {
			var hours = CalculateDurationInWorkingHours();
            var days = hours * 1.0 / workingHoursCalculator.WorkingDayDurationHours;
            
			days = Math.round(days * 100) / 100;

			return days;
		}

		function CalculateDurationInDays() {
			let dateData = getFullDateData();

            if (dateData.startDate > dateData.endDate) {
              return 0; 
            }
            
            let days = 0;
            let currentDate = new Date(dateData.startDate);
            
            while (currentDate <= dateData.endDate) {
              const dayOfWeek = currentDate.getDay();
              if (dayOfWeek !== 0 && dayOfWeek !== 6) {
                days++;
              }
              currentDate.setDate(currentDate.getDate() + 1);
            }

			return days;
		}

		function formatDate(d) {
			if (!d) return '';

			var month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();

			if (month.length < 2) {
				month = '0' + month;
			}
			if (day.length < 2)  {
				day = '0' + day;
			}

			return [year, month, day].join('/');
		}

        function formatTime(d) {
            if (!d) return '';

            var hours = '' + d.getHours(),
                minutes = '' + d.getMinutes();

            if (hours.length < 2) {
                hours = '0' + hours;
            }
            if (minutes.length < 2) {
                minutes = '0' + minutes;
            }

            return [hours, minutes].join(':');
        }

		setTimeout(function () {
			if (isAdmin) {
				Validate(false);
			}
			else {
                UpdateDuration();
			}

            SetVisibilityTimePickers(datesEqual(startDatePicker.value, endDatePicker.value));
			
		}, 500);

		$(function(){
			$("#<%= ddlEmployee.ClientID %>").chosen();
		});

    </script>
</asp:Content>