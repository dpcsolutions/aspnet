﻿<%@ Page Title="Expenses" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Time_AbsenceRequests_Default" Codebehind="Default.aspx.cs" %>

<%@ Register Src="~/assets/Controls/ExpenseApprovedAmountLabel.ascx" TagPrefix="uc1" TagName="ExpenseApprovedAmountLabel" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        #absence-report .progress-bar{
            background-color: #4CAF50;
        }        
    </style>
</asp:Content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
    <div class="btn-group" runat="server" id="quickmenu">
		<button type="button" class="btn bg-gesmobile btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
		</button>

		<ul class="dropdown-menu dropdown-menu-right">
			<li><a href="AbsenceRequest.aspx"><i class="icon-plus2"></i> <%= Resources.Resource.CreateAbsence %></a></li>
		</ul>
	</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">


        <div class="content" id="app">


                    <div class="row">
                <div class="col-sm-12 col-md-4">

                    <!-- User card with thumb and social icons at the bottom -->
                    <div class="panel">
                        <div class="panel-body">
                            <div class="row text-center">

                                <div class="col-xs-3"  v-bind:class="{ 'bordered alpha-primary': statusFilter=='0' }">
                                    <a href="" class="text-muted"  v-on:click="setStatusFilter($event, '0')" >
                                        <p><i class="icon-question6 icon-2x display-inline-block text-primary tada infinite" id="icoCountDraft" runat="server"></i></p>
                                        <h5 class="text-semibold no-margin" id="lblCountToApproved" runat="server">.</h5>
                                        <strong class="text-size-small"><%= Resources.Resource.ToApproved %></strong>
                                    </a>
                                </div>


                                <div class="col-xs-3" v-bind:class="{ 'bordered alpha-primary': statusFilter=='1' }">
                                    <a href="" class="text-success" v-on:click="setStatusFilter($event, '1')" >
                                    <p><i class="icon-history icon-2x display-inline-block text-success tada infinite" id="icoCountOutdated" runat="server"></i></p>
                                    <h5 class="text-semibold no-margin text-muted" id="lblCountArchives" runat="server">...</h5>
                                    <strong class="text-muted text-size-small"><%= Resources.Resource.AbsenceRequest_Approved %></strong>
                                    </a>
                                </div>

                                <div class="col-xs-3" v-bind:class="{ 'bordered alpha-primary': statusFilter=='2' }">
                                    <a href="" class="text-success" v-on:click="setStatusFilter($event, '2')" >
                                    <p><i class="icon-cross2 icon-2x display-inline-block text-danger tada infinite" id="icoCountRejected" runat="server"></i></p>
                                    <h5 class="text-semibold no-margin text-muted" id="lblCountRejected" runat="server">...</h5>
                                    <strong class="text-muted text-size-small"><%= Resources.Resource.AbsenceRequest_Rejected %></strong>
                                    </a>
                                </div>

                                <div class="col-xs-3" v-bind:class="{ 'bordered alpha-primary': statusFilter=='3' }">
                                    <a href="" class="text-success" v-on:click="setStatusFilter($event, '3')" >
                                    <p><i class="icon-trash icon-2x display-inline-block text-danger tada infinite" id="i1" runat="server"></i></p>
                                    <h5 class="text-semibold no-margin text-muted" id="lblCountCanceled" runat="server">...</h5>
                                    <strong class="text-muted text-size-small"><%= Resources.Resource.CanceledMultiple %></strong>
                                    </a>
                                </div>

                            </div>
                        </div>

                        <div class="panel-footer text-center no-padding">
                            <div class="row">
                            </div>
                        </div>
                    </div>
                    <!-- /user card with thumb and social icons at the bottom -->

                </div>


                <div class="col-sm-6 col-md-4">
                    <div class="panel" id="absence-report">
                        <div class="panel-body">
                            <div class="row media no-margin text-center">
                                <p class="text-bold"><%= Resources.Resource.VacationReport %></p>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <div class="progress">
                                      <div id="vacation-progress-bar" class="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-5">
                                <div class="col-sm-3 col-md-3 text-bold" id="vacation-report-summary"></div>
                                <div class="col-sm-9 col-md-9">
                                    <ul class="text-right">
                                        <li class="selectboxit-option">
                                            <%= Resources.Resource.RemainingDays %> : <span id="vacation-report-vacation-days-remaining"></span>
                                        </li>
                                        <li class="selectboxit-option">
                                            <%= Resources.Resource.CarriedOverDays %> : <span id="vacation-report-carried-over-days"></span>
                                        </li>
                                        <li class="selectboxit-option">
                                            <%= Resources.Resource.UnpaidLeave %> : <span id="vacation-report-unpaid-days"></span>
                                        </li>
                                    </ul>
                                </div>
                                <div id="annual-vacation-report-text-hidden" style="display:none">></div>
                            </div>                            
                        </div>
                    </div>
                </div>


                <div class="col-sm-7 col-md-4">
                    <div class="panel panel-body panel-body-accent">
                        <div class="media no-margin">
                            <div>
								<i class="fa fa-calendar-check-o" aria-hidden="true"></i> <%= Resources.Resource.Select_Period %>
							</div>
							<div>
								<select id="ddlPeriod" class="select" onchange="setPeriodFilter()">
									<%= OptionsPeriod %>
								</select>
							</div>
                        </div>
                    </div>
                </div>
            </div><!-- /row -->

					<div>
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
										<strong><%= ph.CMP.Name %></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

							<div class="panel-body">
                                <asp:Label runat="server" ID="lblNoData" Visible="false">
                                    <%= Resources.Resource.NoAbsenceRequestsYet %>
                                </asp:Label>
                                <asp:GridView runat="server" ID="grid" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:HyperLinkField DataTextField="Id" HeaderText="#" DataNavigateUrlFields="id"  ItemStyle-CssClass="text-nowrap p-5" DataNavigateUrlFormatString="AbsenceRequest.aspx?id={0}" />
                                        <asp:BoundField DataField="EmployeeName" HeaderText="Employee"  ItemStyle-CssClass="p-5" HtmlEncode="false"  />
                                        <asp:BoundField DataField="AbsenceCategory" HeaderText="Category"  ItemStyle-CssClass="p-5" HtmlEncode="false"  />
                                        <asp:BoundField DataField="date" HeaderText="StartDate" HtmlEncode="false"  ItemStyle-CssClass="p-5"  DataFormatString = "{0:dd/MM/yyyy}"/>
                                        <asp:BoundField DataField="EndDate" HeaderText="EndDate" HtmlEncode="false"  ItemStyle-CssClass="p-5" DataFormatString = "{0:dd/MM/yyyy}"/>
                                        <asp:BoundField DataField="CancellationStatusTextLocalized" HeaderText="CancellationStatus" HtmlEncode="false"  ItemStyle-CssClass="p-5"/>
                                        <asp:BoundField DataField="Comment" HeaderText="Comment"   ItemStyle-CssClass="p-5"/>
                                        <asp:BoundField DataField="DurationDays" HeaderText="Duration"   ItemStyle-CssClass="p-3" DataFormatString = "{0:0.##}"/>
                                    </Columns>
                                </asp:GridView>
                            </div><!-- panel-body -->

                            <div class="panel-footer text-right no-padding">
						    	<div class="row" style="margin:10px">
						    		<a href="AbsenceRequest.aspx" class="btn btn-default"><i class="icon-plus2 position-left"></i> 
                                        <%=Resources.Resource.AddNewAbsenceRequest %>
      
						    		</a>
						    	</div>
							</div><!-- panel-footer -->

                        </div>
                    </div>
        </div><!-- /content -->

    </form>
    <script type="text/javascript">
        var INIT_STATUS = <%= Status == null ? "null" : String.Format("'{0}'", Status) %>;
    </script>

    <script type="text/javascript" src="Default.js?<%= Guid.NewGuid().ToString("n") %>"></script>
    <%--<script type="text/javascript" src="Default.js"></script>--%>
</asp:Content>

