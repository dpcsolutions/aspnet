﻿var _vacationDataTable = null;
var _unforeseenDataTable = null;
var _trainingDataTable = null;
var _otherDataTable = null;
var _hoursAndOfferedDataTable = null;
var _militaryDataTable = null;

periodChanged();

function periodChanged() {

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: getDataUrl(),
        dataType: "json",

        success: function (response) {
            _vacationDataTable && _vacationDataTable.destroy();
            _unforeseenDataTable && _unforeseenDataTable.destroy();
            _trainingDataTable && _trainingDataTable.destroy();
            _otherDataTable && _otherDataTable.destroy();
            _hoursAndOfferedDataTable && _hoursAndOfferedDataTable.destroy();
            _militaryDataTable && _militaryDataTable.destroy();

            _vacationDataTable = initVacationDataTable(true, false, false, undefined, 0, response.data);
            _unforeseenDataTable = initUnforeseenDataTable(true, false, false, undefined, 0, response.data);
            _trainingDataTable = initTrainingDataTable(true, false, false, undefined, 0, response.data);
            _otherDataTable = initOtherDataTable(true, false, false, undefined, 0, response.data);
            _hoursAndOfferedDataTable = initHoursAndOfferedDataTable(true, false, false, undefined, 0, response.data);
            _militaryDataTable = initMilitaryDataTable(true, false, false, undefined, 0, response.data);
        },
        error: function (a, b, c) {
            console.error(a.responseText);
        }
    });    
}

function initVacationDataTable(firstColDesc, isGrouping, footerTotal, pageLength, aggregatedColumnIdx, data) {
    var dt = $('#vacations-datatable');
    var options = getDataTableOptions(dt, firstColDesc, isGrouping, footerTotal, pageLength, aggregatedColumnIdx, data);
    options.data = data;
    options.columns = [
        {
            "data": "EmployeeId",
            render: function (id) { return "<a href=\"../../Ressources/Employees/Edit.aspx?id=" + id + "\">"+ id +"</a>"; }
        },
        { "data": "EmployeeName" },
        { "data": "VacationReport.Summary" },
        {
            "data": "VacationReport.VacationDaysRemaining",
            "render": displayDouble
        },
        {
            "data": "VacationReport.CarriedOverDays",
            "render": displayDouble
        },
        {
            "data": "VacationReport.UnpaidDays",
            "render": displayDouble
        }
    ];

    return dt.DataTable(options);
}

function initUnforeseenDataTable(firstColDesc, isGrouping, footerTotal, pageLength, aggregatedColumnIdx, data) {
    var dt = $('#unforeseen-datatable');
    var options = getDataTableOptions(dt, firstColDesc, isGrouping, footerTotal, pageLength, aggregatedColumnIdx, data);
    options.data = data;
    options.columns = [
        {
            "data": "EmployeeId",
            render: function (id) { return "<a href=\"../../Ressources/Employees/Edit.aspx?id=" + id + "\">" + id + "</a>"; }
        },
        { "data": "EmployeeName" },
        {
            "data": "DiseaseDays",
            "render": displayDouble
        },
        {
            "data": "AccidentDays",
            "render": displayDouble
        }
    ];

    return dt.DataTable(options);
}

function initTrainingDataTable(firstColDesc, isGrouping, footerTotal, pageLength, aggregatedColumnIdx, data) {
    var dt = $('#training-datatable');
    var options = getDataTableOptions(dt, firstColDesc, isGrouping, footerTotal, pageLength, aggregatedColumnIdx, data);
    options.data = data;
    options.columns = [
        {
            "data": "EmployeeId",
            render: function (id) { return "<a href=\"../../Ressources/Employees/Edit.aspx?id=" + id + "\">" + id + "</a>"; }
        },
        { "data": "EmployeeName" },
        {
            "data": "InternalTrainingDays",
            "render": displayDouble
        },
        {
            "data": "ExternalTrainingDays",
            "render": displayDouble
        }
    ];

    return dt.DataTable(options);
}

function initOtherDataTable(firstColDesc, isGrouping, footerTotal, pageLength, aggregatedColumnIdx, data) {
    var dt = $('#other-datatable');
    var options = getDataTableOptions(dt, firstColDesc, isGrouping, footerTotal, pageLength, aggregatedColumnIdx, data);
    options.data = data;
    options.columns = [
        {
            "data": "EmployeeId",
            render: function (id) { return "<a href=\"../../Ressources/Employees/Edit.aspx?id=" + id + "\">" + id + "</a>"; }
        },
        { "data": "EmployeeName" },
        {
            "data": "MovingHomeDays",
            "render": displayDouble
        },
        {
            "data": "BirthDays",
            "render": displayDouble
        },
        {
            "data": "WeddingDays",
            "render": displayDouble
        },
        {
            "data": "DeathDays",
            "render": displayDouble
        },
    ];

    return dt.DataTable(options);
}

function initHoursAndOfferedDataTable(firstColDesc, isGrouping, footerTotal, pageLength, aggregatedColumnIdx, data) {
    var dt = $('#hours-and-offered-datatable');
    var options = getDataTableOptions(dt, firstColDesc, isGrouping, footerTotal, pageLength, aggregatedColumnIdx, data);
    options.data = data;
    options.columns = [
        {
            "data": "EmployeeId",
            render: function (id) { return "<a href=\"../../Ressources/Employees/Edit.aspx?id=" + id + "\">" + id + "</a>"; }
        },
        { "data": "EmployeeName" },
        {
            "data": "RecoveryOfHoursDays",
            "render": displayDouble
        },
        {
            "data": "PublicHolidayDays",
            "render": displayDouble
        },
    ];

    return dt.DataTable(options);
}

function initMilitaryDataTable(firstColDesc, isGrouping, footerTotal, pageLength, aggregatedColumnIdx, data) {
    var dt = $('#military-datatable');
    var options = getDataTableOptions(dt, firstColDesc, isGrouping, footerTotal, pageLength, aggregatedColumnIdx, data);
    options.data = data;
    options.columns = [
        {
            "data": "EmployeeId",
            render: function (id) { return "<a href=\"../../Ressources/Employees/Edit.aspx?id=" + id + "\">" + id + "</a>"; }
        },
        { "data": "EmployeeName" },
        { "data": "MilitarySummary" },
        {
            "data": "MilitaryRemainingDays",
            "render": displayDouble
        },
    ];

    return dt.DataTable(options);
}

function getDataUrl() {
    var dates = getPeriodFilterDates();
    console.log(dates);
    var url = "../../API/Service.svc/GetAbsencesReports?startTicks=" + dates[0] + "&endTicks=" + dates[1];
    return url;
}