﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master"  AutoEventWireup="true" CodeBehind="TrackingAbsenceRequests.aspx.cs" Inherits="Calani.Webapp.Time.AbsenceRequests.TrackingAbsenceRequests" %>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
    <div class="btn-group" runat="server" id="quickmenu">
		<button type="button" class="btn bg-gesmobile btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
		</button>

		<ul class="dropdown-menu dropdown-menu-right">
			<li><a href="Edit.aspx"><i class="icon-plus2"></i> <%= Resources.Resource.Add_new %> <%= Resources.Resource.AbsenceRequest %></a></li>
		</ul>
	</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
		<div class="content" id="app">
			<div class="row">
				<div class="col-sm-5 col-md-8">&nbsp;</div>

				<div class="col-sm-7 col-md-4">
                    <div class="panel panel-body panel-body-accent">
                        <div class="media no-margin">
                            <div>
								<i class="fa fa-calendar-check-o" aria-hidden="true"></i> <%= Resources.Resource.Select_Period %>
							</div>
							<div>
								<select id="ddlPeriod" class="select" onchange="periodChanged()">
									<%= OptionsPeriod %>
								</select>
							</div>
                        </div>
                    </div>
                </div>
			</div>

			<div>
				<%--Vacation--%>
				<div class="panel panel-white">
					<div class="panel-heading">
						<h6 class="panel-title">
							<a name="quotes">
								<strong><%= Resources.Resource.Vacation %></strong>
							</a>
						</h6>
					</div>

					<div class="panel-body">
						<%--<asp:Label runat="server" ID="lblNoData" Visible="false">
							<%= Resources.Resource.NoAbsenceRequestsYet %>
						</asp:Label>--%>
						<table id="vacations-datatable" class="table table-hover dataTable no-footer">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><%= Resources.Resource.Employee %></th>
                                    <th><%= Resources.Resource.Summary %></th>
                                    <th><%= Resources.Resource.RemainingDays %></th>
                                    <th><%= Resources.Resource.CarriedOverDays %></th>
                                    <th><%= Resources.Resource.UnpaidLeave %></th>
                                </tr>
                            </thead>
						</table>
					</div>

				</div>

				<%--Unforeseen--%>
				<div class="panel panel-white">
					<div class="panel-heading">
						<h6 class="panel-title">
							<a name="quotes">
								<strong><%= Resources.Resource.Unforeseen %></strong>
							</a>
						</h6>
					</div>

					<div class="panel-body">
						<%--<asp:Label runat="server" ID="lblNoData" Visible="false">
							<%= Resources.Resource.NoAbsenceRequestsYet %>
						</asp:Label>--%>
						<table id="unforeseen-datatable" class="table table-hover dataTable no-footer">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><%= Resources.Resource.Employee %></th>
                                    <th><%= Resources.Resource.AbsenceCategory_Disease %></th>
                                    <th><%= Resources.Resource.AbsenceCategory_Accident %></th>
                                </tr>
                            </thead>
						</table>
					</div>

				</div>

				<%--Training--%>
				<div class="panel panel-white">
					<div class="panel-heading">
						<h6 class="panel-title">
							<a name="quotes">
								<strong><%= Resources.Resource.Training %></strong>
							</a>
						</h6>
					</div>

					<div class="panel-body">
						<%--<asp:Label runat="server" ID="lblNoData" Visible="false">
							<%= Resources.Resource.NoAbsenceRequestsYet %>
						</asp:Label>--%>
						<table id="training-datatable" class="table table-hover dataTable no-footer">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><%= Resources.Resource.Employee %></th>
                                    <th><%= Resources.Resource.AbsenceCategory_InternalTraining %></th>
                                    <th><%= Resources.Resource.AbsenceCategory_ExternalTraining %></th>
                                </tr>
                            </thead>
						</table>
					</div>

				</div>

				<%--Other--%>
				<div class="panel panel-white">
					<div class="panel-heading">
						<h6 class="panel-title">
							<a name="quotes">
								<strong><%= Resources.Resource.Other %></strong>
							</a>
						</h6>
					</div>

					<div class="panel-body">
						<%--<asp:Label runat="server" ID="lblNoData" Visible="false">
							<%= Resources.Resource.NoAbsenceRequestsYet %>
						</asp:Label>--%>
						<table id="other-datatable" class="table table-hover dataTable no-footer">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><%= Resources.Resource.Employee %></th>
                                    <th><%= Resources.Resource.AbsenceCategory_MovingHome %></th>
                                    <th><%= Resources.Resource.AbsenceCategory_Birth %></th>
                                    <th><%= Resources.Resource.AbsenceCategory_Wedding %></th>
                                    <th><%= Resources.Resource.AbsenceCategory_Death %></th>
                                </tr>
                            </thead>
						</table>
					</div>

				</div>

				<%--Hours and offered--%>
				<div class="panel panel-white">
					<div class="panel-heading">
						<h6 class="panel-title">
							<a name="quotes">
								<strong><%= Resources.Resource.HoursAndOffered %></strong>
							</a>
						</h6>
					</div>

					<div class="panel-body">
						<%--<asp:Label runat="server" ID="lblNoData" Visible="false">
							<%= Resources.Resource.NoAbsenceRequestsYet %>
						</asp:Label>--%>
						<table id="hours-and-offered-datatable" class="table table-hover dataTable no-footer">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><%= Resources.Resource.Employee %></th>
                                    <th><%= Resources.Resource.AbsenceCategory_RecoveryOfHours %></th>
                                    <th><%= Resources.Resource.AbsenceCategory_PublicHoliday %></th>
                                </tr>
                            </thead>
						</table>
					</div>

				</div>

				<%--Military--%>
				<div class="panel panel-white">
					<div class="panel-heading">
						<h6 class="panel-title">
							<a name="quotes">
								<strong><%= Resources.Resource.AbsenceCategory_Military %></strong>
							</a>
						</h6>
					</div>

					<div class="panel-body">
						<%--<asp:Label runat="server" ID="lblNoData" Visible="false">
							<%= Resources.Resource.NoAbsenceRequestsYet %>
						</asp:Label>--%>
						<table id="military-datatable" class="table table-hover dataTable no-footer">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th><%= Resources.Resource.Employee %></th>
                                    <th><%= Resources.Resource.Summary %></th>
                                    <th><%= Resources.Resource.RemainingDays %></th>
                                </tr>
                            </thead>
						</table>
					</div>

				</div>
			</div>
		</div>
	</form>
	<script type="text/javascript" src="tracking-absence-requests.js?<%= Guid.NewGuid().ToString("n") %>"></script>
</asp:Content>