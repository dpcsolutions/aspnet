﻿var txtTotalFor = STRING_RESOURCES.TotalFor;
var _periodFilterFrom = null;
var _periodFilterTo = null;
var _statusFilter = INIT_STATUS != null && typeof (INIT_STATUS) !== 'undefined' ? INIT_STATUS : '0';

var _dataTable = null;

function setPeriodFilter() {
    _periodFilterFrom = null;
    _periodFilterTo = null;

    var pval = $('#ddlPeriod').val();
    if (pval != null && pval.indexOf(';') > 0) {
        pval = pval.split(';');
        if (pval.length == 2) {
            _periodFilterFrom = pval[0];
            _periodFilterTo = pval[1];

            if (_periodFilterFrom != "*")
                _periodFilterFrom = parseInt(_periodFilterFrom);
            else
                _periodFilterFrom = null;

            if (_periodFilterTo != "*")
                _periodFilterTo = parseInt(_periodFilterTo);
            else
                _periodFilterTo = null;
        }
    }

    refreshLines();
    loadVacationReportForEmployee(CURRENT_USER_ID);
}

function refreshLines() {
    _dataTable.draw();
}

function refreshLiveStats() {

    let countToApproved = 0;
    let countArchives = 0;
    let countRejected = 0;
    let countCanceled = 0;

    let rowsId = _dataTable.rows().eq(0);

    let searchText = $('.dataTables_filter input').val();
    
    for (let i = 0; i < rowsId.length; i++) {

        let row = _dataTable.row(rowsId[i]);
        let lastUpdate = parseInt(row.node().attributes['data-date'].value);

        if (_periodFilterFrom != null) {
            if (lastUpdate < _periodFilterFrom)
                continue;
        }
        if (_periodFilterTo != null) {
            if (lastUpdate >= _periodFilterTo)
                continue;
        }

        let rowStatus = row.node().attributes["data-status"].value;

        let data = row.data();
        if (rowStatus != null) {
            let matchesSearch = true;
            if (searchText) matchesSearch = data.some(element => element.toLowerCase().includes(searchText.toLowerCase()));
            
            switch (rowStatus) {
                case "0":
                    if (matchesSearch) countToApproved++;
                    break;
                case "1":
                    if (matchesSearch) countArchives++;
                    break;
                case "2":
                    if (matchesSearch) countRejected++;
                    break;
                case "3":
                    if (matchesSearch) countCanceled++;
                    break;
            }
        }
    }

    $('#ContentPlaceHolder1_lblCountToApproved').html(countToApproved);
    $('#ContentPlaceHolder1_lblCountArchives').html(countArchives);
    $('#ContentPlaceHolder1_lblCountRejected').html(countRejected);
    $('#ContentPlaceHolder1_lblCountCanceled').html(countCanceled);
}

var groupElement = $(
    '<div class="dataTables_length pull-right  ml-20 pl-20">' +
    '<label class="col-form-label">' + STRING_RESOURCES.Grouping +
    '<select id="ddlGroupingGrid" class="ml-10 width-200">' +
    '<option value="0">--</option>' +
    '<option value="1">' + STRING_RESOURCES.Employee + '</option>' +
    '<option value="3">' + STRING_RESOURCES.Date + '</option>' +
    '</label ></select></div>');


groupElement.on('change', function (e) {

    var indx = $("#ddlGroupingGrid").val();

    if (typeof indx !== "undefined" && indx != null && indx > 0) {
        _dataTable.order.fixed({
            pre: [indx, 'desc']
        });

        _dataTable.rowGroup().dataSrc(indx);
        _dataTable.rowGroup().enable().draw();

    } else {
        _dataTable.order.fixed({

        });
        _dataTable.rowGroup().disable().draw();
        _dataTable.order([0, 'asc']).draw();
    }


    _dataTable.draw();
});

function appendGroupingList() {
    $(groupElement).appendTo("#ContentPlaceHolder1_grid_filter");
}

function loadVacationReportForEmployee(employeeId) {

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: geVacationReportUrl(employeeId),
        dataType: "json",

        success: function (response) {            
            var report = JSON.parse(response.data);
            
            $("#vacation-report-summary")[0].innerText = report.Summary;
            $("#vacation-report-vacation-days-remaining")[0].innerText = (Math.round(report.VacationDaysRemaining * 10) / 10);
            $("#vacation-report-carried-over-days")[0].innerText = (Math.round(report.CarriedOverDays * 10) / 10);
            $("#vacation-report-unpaid-days")[0].innerText = (Math.round(report.UnpaidDays * 10) / 10);
            $("#annual-vacation-report-text-hidden")[0].innerText = report.TextReport;
            $("#vacation-progress-bar").css("width", report.VacationTakenPercent + '%');
        },
        error: function (a, b, c) {
            console.error(a.responseText);
        }
    });
}

function geVacationReportUrl(employeeId) {
    var dates = getPeriodFilterDates();
    var url = "../../API/Service.svc/GetVacationReportForEmployee?startTicks=" + dates[0] + "&endTicks=" + dates[1] + "&employeeId=" + employeeId;
    return url;
}

function filterItem(settings, data, dataIndex) {

    var visible = true;

    var lastUpdate = parseInt(settings.aoData[dataIndex].nTr.getAttribute('data-date'));
    if (_periodFilterFrom != null) {
        if (lastUpdate < _periodFilterFrom)
            visible = false;
    }
    if (_periodFilterTo != null) {
        if (lastUpdate >= _periodFilterTo)
            visible = false;
    }

    settings.aoData[dataIndex].nTr.setAttribute('data-anystatusFilter', visible);

    var status = settings.aoData[dataIndex].nTr.getAttribute('data-status');
    if (_statusFilter != null) {
        if (status != _statusFilter)
            visible = false;
    }

    return visible;
}

$(function () {
    
    // client side filters
    $.fn.dataTable.ext.search.push(filterItem);
    // -->

    _dataTable = initDataTable($('#ContentPlaceHolder1_grid'), true, true, false, undefined, 7);
    _dataTable.search('');
    _dataTable.on('draw', function () {
        appendGroupingList();
        refreshLiveStats();
    });
  

    setPeriodFilter();


    setTimeout(function () {
        $("select", groupElement).val("0").trigger('change');
    }, 20);
    
});

new Vue({

    el: '#app',

    data: {
        statusFilter: INIT_STATUS != null && typeof (INIT_STATUS) !== 'undefined' ? INIT_STATUS : '0'
    },
    methods: {
        setStatusFilter: function (e, f) {
            this.statusFilter = f;
            _statusFilter = f;
            _dataTable.draw();
            e && e.preventDefault();
        }
    }
});