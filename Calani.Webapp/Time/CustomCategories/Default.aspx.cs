﻿using System;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.WorkSchedulers;

public partial class Time_Custom_Profiles_Default : System.Web.UI.Page
{
    public PageHelper ph;

    private static WorkScheduleRecManager mngrRec;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        ph.ItemLabel = Resources.Resource.Custom_Categories;
       
        mngrRec = new WorkScheduleRecManager(ph.CurrentOrganizationId);

        if (!IsPostBack)
        {
           
        }
        var recData = mngrRec.GetScheduleRecordsDto(null);
        listRecords.DataSource = recData;
        listRecords.DataBind();
        
        Title = ph.CMP.Name;
    }
    
  
    protected void btnSaveProfileRec_OnClick(object sender, EventArgs e)
    {
        int id = 0;
        id = Convert2.StrToInt(RecId.Value, id);

        var name = recName.Value;


        int rate = 100;
        rate = Convert2.StrToInt(recRate.Value, rate);
        

        if (String.IsNullOrWhiteSpace(name) ||rate<-100)
            return;

        var typeStr = ddlType.SelectedItem.Value;

        TimeCategoryType type = TimeCategoryType.Predefined;

        type = (TimeCategoryType)Enum.Parse(typeof(TimeCategoryType), typeStr);

        var rec = new work_schedule_profiles_rec()
        {
            name = name,
            rate = rate,
            type = (int)(type),
            organization_id = ph.CurrentOrganizationId
        };

       

        SmartAction<work_schedule_profiles_rec> ret;

        if (id < 1)
            ret = mngrRec.Add(rec);
        else
            ret = mngrRec.Update(id, rec);

        if (ret.Success)
        {
            var data = mngrRec.GetScheduleRecordsDto(null);

            listRecords.DataSource = data;
            listRecords.DataBind();
            RecId.Value = String.Empty;
            recName.Value = String.Empty;
        }
        Response.Redirect(Request.Url.PathAndQuery);
    }

  
    protected void btnDeleteRecord_Click(object sender, EventArgs e)
    {
        long id = 0;
        id = Convert2.StrToLong(RecId.Value, id);
        if (id > 0)
        {
            var rec = mngrRec.Get(id);
            if (rec.name != "Férié")
            {
                mngrRec.Delete(id);
            }
        }

        Response.Redirect(Request.Url.PathAndQuery);
    }
}