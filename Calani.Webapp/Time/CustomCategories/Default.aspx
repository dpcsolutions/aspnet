﻿<%@ Page Title="Time Categories" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Time_Custom_Profiles_Default" Codebehind="Default.aspx.cs" %>
<%@ Import Namespace="Calani.BusinessObjects.Model" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    <%--Canceled Seubi css for page to apply theme style--%>
    <style type="text/css">
        .table > thead > tr > th {
            border-bottom: none;  
            background: none; 
            color: black;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<form runat="server">
    <div class="content">
        <div class="row">
                <div class="col-lg-12">
                </div>
                <!-- column -->
			    <div class="col-lg-12">
                    
                    <asp:Panel runat="server" ID="ProfileRecords" class="panel panel-flat" >
                        <div class="timeline-row panel panel-flat">
                            <div class="panel-heading">
                                <h6 class="panel-title text-semibold"><%= Resources.Resource.Custom_Categories %></h6>
                            </div>
                        
                            <div class="panel-body">
                                <asp:ListView runat="server" ID="listRecords" Visible="True">
                                    <LayoutTemplate>
                                        <table class="table table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th runat="server" class="col-lg-push-2"  scope="col"><%= Resources.Resource.Name %></th>
                                                <th runat="server" class="col-lg-push-1"  scope="col"><%= Resources.Resource.Rate %></th>
                                                <th runat="server" class="col-lg-pull-0"  scope="col"><%= Resources.Resource.Type %></th>
                                                <th runat="server" class="col-lg-pull-0"  scope="col"><%= Resources.Resource.Action %></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr runat="server" id="itemPlaceholder" ></tr>
                                            </tbody>
                                        </table>
                                    </LayoutTemplate>
                                    <ItemTemplate>
                                        <tr >
                                            <td >
                                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("Name") %>' CssClass="text-right disabled"/>
                                            </td>
                                      
                                            <td >
                                                <asp:Label ID="Label2" runat="server" Text='<%#Eval("Rate")+"%" %>' CssClass="text-right"/>
                                            </td>
                                        
                                            <td >
                                                <span runat="server" Visible='<%# (TimeCategoryType)Enum.Parse(typeof(TimeCategoryType),Eval("Type").ToString()) == TimeCategoryType.ScheduleRule%>'
                                                    class="label label-success "><%= Resources.Resource.Work_Schedule %> </span>

                                                <span runat="server" id="ScheduleHoliday" runat="server" Visible='<%# (TimeCategoryType)Enum.Parse(typeof(TimeCategoryType),Eval("Type").ToString()) == TimeCategoryType.ScheduleHoliday%>'
                                                    class="label label-danger  "><%= Resources.Resource.Holidays %> 
                                                </span>
                                                <span runat="server" id="Custom" runat="server" Visible='<%# (TimeCategoryType)Enum.Parse(typeof(TimeCategoryType),Eval("Type").ToString()) == TimeCategoryType.Custom%>'
                                                    class="label label-default   "><%= Resources.Resource.System %> 
                                                </span>
                                                <span runat="server" id="Auto" runat="server" Visible='<%# (TimeCategoryType)Enum.Parse(typeof(TimeCategoryType),Eval("Type").ToString()) == TimeCategoryType.Auto%>'
                                                    class="label label-default   "><%= Resources.Resource.System %> 
                                                </span>
                                                <span runat="server" id="Predefined" runat="server" Visible='<%# (TimeCategoryType)Enum.Parse(typeof(TimeCategoryType),Eval("Type").ToString()) == TimeCategoryType.Predefined%>'
                                                    class="label label-primary"><%= Resources.Resource.Predefined %>                                          
                                                </span>

                                                 <span runat="server" id="Span2" runat="server" Visible='<%# (TimeCategoryType)Enum.Parse(typeof(TimeCategoryType),Eval("Type").ToString()) == TimeCategoryType.Vacation%>'
                                                    class="label label-success"><%= Resources.Resource.Vacation %>                                          
                                                </span>
                                                <span runat="server" id="Span1" runat="server" Visible='<%# (TimeCategoryType)Enum.Parse(typeof(TimeCategoryType),Eval("Type").ToString()) == TimeCategoryType.Normal%>'
                                                    class="label label-primary"><%= Resources.Resource.Normal %>                                          
                                                </span>                                               
                                                <span runat="server" id="Span3" runat="server" Visible='<%# (TimeCategoryType)Enum.Parse(typeof(TimeCategoryType),Eval("Type").ToString()) == TimeCategoryType.Overtime%>'
                                                    class="label label-primary"><%= Resources.Resource.Overtime %>                                          
                                                </span>
                                                <span runat="server" id="Span5" runat="server" Visible='<%# (TimeCategoryType)Enum.Parse(typeof(TimeCategoryType),Eval("Type").ToString()) == TimeCategoryType.Unpaid%>'
                                                    class="label label-info"><%= Resources.Resource.Unpaid %>                                          
                                                </span>
                                                 <span runat="server" id="Span4" runat="server" Visible='<%# (TimeCategoryType)Enum.Parse(typeof(TimeCategoryType),Eval("Type").ToString()) == TimeCategoryType.Ill%>'
                                                    class="label label-danger "><%= Resources.Resource.Ill %>                                          
                                                </span>
                                                <span runat="server" id="Span6" runat="server" Visible='<%# (TimeCategoryType)Enum.Parse(typeof(TimeCategoryType),Eval("Type").ToString()) == TimeCategoryType.Lunch%>'
                                                    class="label label-success "><%= Resources.Resource.Lunch %>                                          
                                                </span>
                                                <span runat="server" id="Span7" runat="server" Visible='<%# (TimeCategoryType)Enum.Parse(typeof(TimeCategoryType),Eval("Type").ToString()) == TimeCategoryType.TimeOfDay%>'
                                                    class="label label-success "><%= Resources.Resource.TimeOfDay %>                                          
                                                </span>
                                                 <span runat="server" id="Span8" runat="server" Visible='<%# (TimeCategoryType)Enum.Parse(typeof(TimeCategoryType),Eval("Type").ToString()) == TimeCategoryType.Accident%>'
                                                    class="label label-danger "><%= Resources.Resource.Accident %>                                          
                                                </span>
                                                 <span runat="server" id="Span9" runat="server" Visible='<%# (TimeCategoryType)Enum.Parse(typeof(TimeCategoryType),Eval("Type").ToString()) == TimeCategoryType.Justified%>'
                                                    class="label label-info "><%= Resources.Resource.Justified %>                                          
                                                </span>
                                                 <span runat="server" id="Span10" runat="server" Visible='<%# (TimeCategoryType)Enum.Parse(typeof(TimeCategoryType),Eval("Type").ToString()) == TimeCategoryType.Holiday%>'
                                                    class="label label-success "><%= Resources.Resource.Holiday %>                                          
                                                </span>
                                            </td> 
                                            <td>
                                                <ul class="icons-list" runat="server" Visible='<%# (int)Enum.Parse(typeof(TimeCategoryType),Eval("Type").ToString()) >= (int)TimeCategoryType.Predefined%>'>
                                                    <li class="text-primary-600">
                                                        <a runat="server" class="text-left span-id"
                                                            onclick='<%# "editRec(\""+ Eval("Id") + "\",\""+Eval("Name") +"\",\""+Eval("Rate")+ "\",\""+(int)Enum.Parse(typeof(TimeCategoryType),Eval("Type").ToString()) +"\");" %>'>
                                                            <i class="icon-pencil7"></i>
                                                        </a>
                                                    </li>
                                                    <li class="text-danger-600">
                                                        <a runat="server" class="text-left"
                                                            onclick='<%# "deleteRec(\"" + Eval("Id") + "\", \"" + Eval("Name") + "\");" %>'>
                                                            <i class="icon-trash"></i>
                                                        </a>
                                                                
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                
                                    <EmptyDataTemplate>
                                        <table class="table table-striped table-hover">
                                            <thead>
                                            <tr>
                                                <th runat="server" class="col-lg-push-2"  scope="col"><%= Resources.Resource.Name %></th>
                                                <th runat="server" class="col-lg-push-1"  scope="col"><%= Resources.Resource.Rate %></th>
                                                <th runat="server" class="col-lg-pull-0"  scope="col"><%= Resources.Resource.Type %></th>
                                                <th runat="server" class="col-lg-pull-0"  scope="col"><%= Resources.Resource.Action %></th>
                                                        
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr runat="server" id="Tr1" ></tr>
                                            </tbody>
                                        </table>
                                    </EmptyDataTemplate>
                                </asp:ListView>
						        </div>
                            <div class="panel-body">
                                <div class="col-lg-8"></div>
                                <div class="col-lg-4 text-right">
                                    <div class="btn-group">
                                        <a onclick="editRec(0)"  class="btn btn-default"><i class="icon-plus2 position-left"></i><%= Resources.Resource.Add_line %></a>
                                   
                                    </div>
                                </div>
                            </div>

                        </div>
                    </asp:Panel>
                </div>
                <!-- /column -->

		</div> <!-- /row -->
        
        <div id="modal_delete_rec" class="modal fade">
            <div class="modal-dialog  modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
                    </div>

                    <div class="modal-body">
                        <h6 class="text-semibold"><%= Resources.Resource.Are_You_Sure_To_Delete %></h6>
                        <p><%= Resources.Resource.Delete_Is_Irreversible %></p>

                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
                        <asp:Button runat="server" type="submit" ID="btnDelete" OnClick="btnDeleteRecord_Click" CssClass="btn btn-primary btn-danger" 
                                   Text="<%$ Resources:Resource, Yes_Delete %>" />

                    </div>
                </div>
            </div>
        </div>
      
                   
        <div id="modal_rec" class="modal fade" role="dialog" aria-hidden="true">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title text-center" id="createRecHeader"><%= Resources.Resource.Add_line %></h5>
                        <h5 class="modal-title text-center" id="editRecHeader"><%= Resources.Resource.Edit_Line %></h5>
                    </div>
                    <input type="text" id="RecId" runat="server" hidden="" />
                    <div class="modal-body">
                       <div class="row">
                            <div class="col-md-5">
                                <label><%= Resources.Resource.Name %></label>
                                <input type="text" id="recName" name="recName" runat="server" maxlength="45" class="form-control" />
                                <label id="recName-error" class="validation-error-label" for="recName" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                            </div>                         
                            <div class="col-md-4">
                                <label><%= Resources.Resource.Rate %>%</label>
                                <input type="number" id="recRate" name="recRate" runat="server" min="-100" value="100" class="form-control" />
                                <label id="recRate-error" class="validation-error-label" for="recRate" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                            </div>
                           <%-- <div class="col-md-3">
                                <label><%= Resources.Resource.Type %></label><br />
                                <span runat="server" class="label label-primary"><%= Resources.Resource.Predefined %></span>
                            </div>--%>
                             <div class="col-md-3">
                                <label><%= Resources.Resource.Type %></label><br />

                               
                                 <asp:DropDownList runat="server" CssClass="form-control"  id="ddlType" onchange="validateRecType();">
                                     <asp:ListItem Text="<%$ Resources:Resource, Predefined %>" Value="4" />
                                     <asp:ListItem Text="<%$ Resources:Resource, Vacation %>" Value="5" />
                                     <asp:ListItem Text="<%$ Resources:Resource, Normal %>" Value="6" />
                                     <asp:ListItem Text="<%$ Resources:Resource, Overtime %>" Value="7" />
                                     <asp:ListItem Text="<%$ Resources:Resource, Unpaid %>" Value="8" />
                                     <asp:ListItem Text="<%$ Resources:Resource, Ill %>" Value="9" />
                                     <asp:ListItem Text="<%$ Resources:Resource, Lunch %>" Value="10" />
                                     <asp:ListItem Text="<%$ Resources:Resource, TimeOfDay %>" Value="11" />
                                     <asp:ListItem Text="<%$ Resources:Resource, Accident %>" Value="12" />
                                     <asp:ListItem Text="<%$ Resources:Resource, Justified %>" Value="13" />
                                     <asp:ListItem Text="<%$ Resources:Resource, Holiday %>" Value="14" />
                                 </asp:DropDownList>

                                
                            </div>

                        </div>
                        <br/>
                        <div class="modal-footer">
                            <asp:Button ID="Button1"  runat="server" OnClick="btnSaveProfileRec_OnClick"  OnClientClick="return  validateEditProfileRec()"
                                        CssClass="btn btn-primary" Text="<%$ Resources:Resource, Save %>" />  

                            <button type="button" class="btn btn-default" data-dismiss="modal"><%= Resources.Resource.Close %></button>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>       
    </div><!-- /content -->

    </form>
    
    <script type="text/javascript">
        function validateEditProfileRec() {
            
            var res = true;
            var name = $('#<%= recName.ClientID %>').val();
          
            var rate = $('#<%= recRate.ClientID %>').val();
            if (!name || !start || !end || !rate) {
                res = false;

                if (!name)
                    $('#recName-error').show();
                else
                    $('#recName-error').hide();

              

                if (!rate)
                    $('#recRate-error').show();
                else
                    $('#recRate-error').hide();
            }
            else {
                $('#recName-error').hide();
             
                $('#recRate-error').hide();

            }

            return res;
        }

        function validateRecType() {
            var ddl = $('#<%= ddlType.ClientID %>').val();
            
            if (ddl == 8)
                $('#<%= recRate.ClientID %>').val(0);

        }
       
        function editRec(id, name, rate, type) {
         
            $('#recName-error').hide();
         
            $('#recRate-error').hide();
            $('#<%= RecId.ClientID %>').val(id);

            if (type)
                $('#<%= ddlType.ClientID %>').val(type);
            else
                $('#<%= ddlType.ClientID %>').val(4);

            $('#<%= recName.ClientID %>').val(name);
            $('#<%= recRate.ClientID %>').val(rate || '100');

            
            if (id > 0) {
                $('#createRecHeader').hide();
                $('#editRecHeader').show();
            } else {
                $('#createRecHeader').show();
                $('#editRecHeader').hide();
            }

            $('#modal_rec').modal('show');
        }
        
        function deleteRec(id, name) {
            if (name === 'Férié'){
                alert("You cannot delete this category");
                return;
            }
            
            $('#<%= RecId.ClientID %>').val(id);
            $('#modal_delete_rec').modal('show');
        }

     

    </script>


</asp:Content>

  
