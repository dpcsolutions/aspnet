﻿using Calani.BusinessObjects;
using Calani.BusinessObjects.Calendars;
using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Departments;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.FrontEndModels.Scheduler;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.OtherResources;
using Calani.BusinessObjects.Projects;
using Calani.BusinessObjects.Scheduler;
using Calani.BusinessObjects.Scheduler.Models;
using Calani.BusinessObjects.WorkSchedulers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

public partial class Time_Scheduler_Default : System.Web.UI.Page
{
    private readonly int DefaultweeklyHours = 40;
    public readonly string CalendarCellHeightPx = "48";

    public PageHelper ph;

    public bool IsAdmin { get; private set; }

    private OrganizationManager _organizationManager;
    private EmployeesManager _employeesManager;
    private OtherResourcesManager _otherResManager;
    private ProjectsManager _projectsManager;
    private CalendarsRecManager _calendarsRecManager;
    private DepartmentsManager _departmentManager;
    protected List<EmployeeModel> Employees { get; set; }
    protected List<OtherResourceModel> OtherResources { get; set; }

    public string ResourcesJson { get; set; }
    public string ProjectsJson { get; set; }
    public string ClientsJson { get; set; }
    public string AllocationsJson { get; set; }
    public string StringResourcesJson { get; set; }
    public string InvalidDatesJson { get; set; }
    public bool FullWeek { get; set; }
    public WorkDayModel WorkDay { get; set; }

    public string BaseUrl { get; set; }
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Title = Resources.Resource.Scheduler;

        IsAdmin = ph.CurrentUserType != ContactTypeEnum.User_Employee;
        var curYear = new DateRange(DatesRangeEnum.CurrentYear);

        _organizationManager = new OrganizationManager(ph.CurrentOrganizationId);
        _organizationManager.Load();
        _employeesManager = new EmployeesManager(ph.CurrentOrganizationId);
        _otherResManager = new OtherResourcesManager(ph.CurrentOrganizationId);
        _departmentManager = new DepartmentsManager(ph.CurrentOrganizationId);
        _projectsManager = new ProjectsManager(ph.CurrentOrganizationId);
        _calendarsRecManager = new CalendarsRecManager(ph.CurrentOrganizationId);
        var workScheduleRecManager = new WorkScheduleRecManager(ph.CurrentOrganizationId);
        var workScheduleService = new WorkScheduleService(_organizationManager, workScheduleRecManager);
        WorkDay = workScheduleService.GetWorkDaySettings();

        var resources = PrepareResourcesCollection();
        var projects = PrepareProjectsCollection();
        var clients = PrepareClientsCollection(projects);
        var allocations = PrepareAllocationsCollection(DatesRangeEnum.CurrentMonth);
        var invalidDates = _calendarsRecManager.GetCalendarRecordsDto().Select(r => new InvalidDateModel(r)).ToList();
        
        ResourcesJson = JsonConvert.SerializeObject(resources);        
        ProjectsJson = JsonConvert.SerializeObject(projects);
        ClientsJson = JsonConvert.SerializeObject(clients);
        AllocationsJson = JsonConvert.SerializeObject(allocations);
        InvalidDatesJson = JsonConvert.SerializeObject(invalidDates);
        StringResourcesJson = SharedResource.ReadStringResources(CultureInfo.CurrentCulture);

        FullWeek = _organizationManager.TimeSheetType == (int)TimesheetTypeEnum.DetailedFullWeek;

        BaseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/');
    }

    private List<ClientModel> PrepareClientsCollection(List<ProjectModel> projects)
    {
        var clients = new Dictionary<long, string>()
        {
            { 0, "" }
        };

        foreach(var project in projects)
        {
            if (!clients.ContainsKey(project.ClientId)) 
            {
                clients.Add(project.ClientId, project.ClientName);
            }
        }

        return clients.OrderBy(p => p.Value).Select(p => new ClientModel(p.Key, p.Value)).ToList();
    }

    private List<ProjectModel> PrepareProjectsCollection()
    {
        var projects = new List<ProjectModel>() { new ProjectModel { Id = 0, ClientName = "" } };

        projects.AddRange(_projectsManager.ListProjectsForScheduler()
            .Select(entity => new ProjectModel(entity)).ToList());
        
        return projects;
    }

    private List<ResourceAllocationModel> PrepareAllocationsCollection(DatesRangeEnum period)
    {
        var dateRange = new DateRange(period);

        var service = new ResourcesAllocationsService(ph.CurrentOrganizationId, ph.CurrentUserId);
        var models = service.ListResourceAllocationsForScheduler(dtFrom: dateRange.Start, dtTo: dateRange.End, 
            cultureCode: CultureInfo.CurrentCulture.Name);
        
        return models;
    }    

    private List<ResourceModel> PrepareResourcesCollection()
    {
        var resources = new List<ResourceModel>();

        var employees = new EmployeeModel()
        {
            Id = "0",
            Group = "",
            Name = Resources.Resource.Employees,
            Children = LoadEmployees(DefaultweeklyHours)
        };

        employees.RemainingTimeMin = employees.SumChildrenHours();

        var otherResources = new OtherResourceModel()
        {
            Id = "0",
            Group = "",
            Name = Resources.Resource.OtherResources,
            Children = LoadOtherResources()
        };

        otherResources.RemainingTimeMin = otherResources.SumChildrenHours();

        //departments
        var departments = new DepartmentModel()
        {
            Id = "0",
            Group = "",
            Name = "Department",//Resources.Resource.OtherResources,
            Children = LoadDepartmentss()
        };
        departments.RemainingTimeMin = departments.SumChildrenHours();
        resources.Add(employees);
        resources.Add(otherResources);
        resources.Add(departments);

        return resources;
    }

    private List<EmployeeModel> LoadEmployees(int defaultWeeklyHours)
    {
        return _employeesManager.ListActiveUsers()
            .Select(entity => new EmployeeModel(entity, defaultWeeklyHours))
            .OrderBy(e => e.LastName)
            .ToList();
    }

    private List<OtherResourceModel> LoadOtherResources()
    {
        return _otherResManager.List()
            .Select(entity => new OtherResourceModel(entity))
            .OrderBy(r => r.Name)
            .ToList();
    }
    private List<DepartmentModel> LoadDepartmentss()
    {
        var dep = _departmentManager.List();
        return dep
            .Select(entity => new DepartmentModel(entity))
            .OrderBy(r => r.Name)
            .ToList();
    }

}