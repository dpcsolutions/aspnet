﻿/* 
 * NOT USED
 * Chart
 *   implemented through fake events (allocations) of type:'chart' bound to parent resources with ids "emp_0" and "othres_0"
 * 
 * */

function getChartEventOnDate(date, resourceId, calendarInst) {
    var dateZeroTime = getDateZeroTime(date);
    var items = calendarInst.getEvents(dateZeroTime).filter(function (a) { return a.type && a.type === "chart" && a.resource === resourceId && a.start.getTime() == dateZeroTime.getTime() });
    return items.length > 0 ? items[0] : null;
}

function addOrUpdateChart(allocation, action, calendarInst) {

    return;

    var startDate = new Date(allocation.start);
    var endDate = new Date(allocation.end);

    var currentDate = startDate;
    var nextDay, chartEvent;
    var chartEvent;
    var parentResourceId = getParentResourceId(allocation.resource);

    while (currentDate <= endDate) {

        chartEvent = getChartEventOnDate(currentDate, parentResourceId, calendarInst);

        if (chartEvent) {

            if (action === "add") chartEvent.count++;
            if (action === "delete") {
                chartEvent.count = chartEvent.count <= 1 ? 0 : chartEvent.count - 1;
            }

            calendarInst.updateEvent(chartEvent);
        }
        else if (action === "add") {

            chartEvent = {
                resource: parentResourceId,
                color: '#A8A8A8',
                start: currentDate,
                end: currentDate,
                title: '',
                type: 'chart',
                count: 1
            };

            calendarInst.addEvent(chartEvent);
        }

        nextDay = getNextDayDate(currentDate);
        currentDate = nextDay;
    }
}

function createChartAllocations(calendarInst) {

    return;

    var allAllocations = calendarInst.getEvents();

    for (var i in allAllocations) {
        addOrUpdateChart(allAllocations[i], "add", calendarInst);
    }

}

function calculateChartHeightPx(chartAllocation) {
    var containerHeightPx = CELL_HEIGHT_PX;
    var maxAllocationsPerResource = 4;
    var countChildResources = getResource(chartAllocation.resource).children.length;
    var maxAllocations = countChildResources * maxAllocationsPerResource;
    var actualAllocations = chartAllocation.count;

    var heightPx = Math.round(containerHeightPx * actualAllocations / maxAllocations);

    return heightPx;
}