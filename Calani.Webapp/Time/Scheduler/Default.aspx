﻿<%@ Page Title="Scheduler" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Time_Scheduler_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/assets/js/shared_business_logic/working_time_calculator.js") %>"></script>
    <!-- Include Mobiscroll -->
    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/assets/js/plugins/ui/mobiscroll.javascript.min.js") %>"></script>
    <link href="<%= Page.ResolveClientUrl("~/assets/css/mobiscroll.javascript.min.css") %>" rel="stylesheet" type="text/css">

    <style type="text/css">

        .content {
          padding: 0 20px 20px 20px; 
        }
        .md-employee-allocations .mbsc-timeline-resource,
        .md-employee-allocations .mbsc-timeline-resource-col {
            width: 235px;
        }

        .employee-shift-day {
            font-size: 14px;
            font-weight: 600;
            opacity: .6;
        }

        .employee-allocations-popup .mbsc-popup .mbsc-popup-header {
            padding-top: 8px;
            padding-bottom: 8px;
        }

        .md-employees-name {
            font-size: 12px;
        }

        .md-employees-group {
            font-size: 10px;
        }

        .md-employees-parent td{
            padding-top: 5px;
        }

        td.md-employees-cont {
            padding-left: 2px;
            max-height: 40px;
            width: 80%;
        }

        td.md-employees-hours {
            color: green;
            width: 10%;
            padding-right: 5px;
        }

        td.md-employees-hours.red {
            color: red;
        }

        td.md-employees-button{
            width: 10%;
            padding: 10px 5px 0 0;
        }

        td.md-employees-button .btn.btn-default{
            color: #000;
            padding: 5px;
            min-height: 0px;
        }

        button.mbsc-btn-outline.btn-add-event {
            color: #000;
            border: 2px solid #ddd;
            border-radius: 3px;
            font-size: 11px;
            margin-right: 4px;
        }

        .md-switching-timeline-view-cont .mbsc-segmented {
            max-width: 350px;
            margin: 0 auto;
            padding: 1px;
        }

        .md-employees-picker {
            flex: 1 0 auto;
        }

        .md-employees-nav {
            width: 200px;
        }

        .md-employee-allocations .mbsc-timeline-resource-title {
            padding: 0;
            width: 100%;
        }

        .md_add_button {
            background-color: gainsboro;
            float: left;
            display: flex;
            font-size: 20pt;
            line-height: 20pt;
            color: white;
            font-weight: bold;
        }

        .mb-calendar-view-switch .mbsc-ios.mbsc-segmented-button.mbsc-button {
            font-size: 14px;
        }

        .absence-image {
          display: block;
          margin-left: auto;
          margin-right: auto;
          width: 30%;
        }

    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="content">
        <div class="panel panel-white">

            <div class="panel-heading">
                <div class="mbsc-form">
                    <div class="mbsc-form-group mb-calendar-view-switch">
                        <label>
                            <%= Resources.Resource.Week %> 
                            <input mbsc-segmented type="radio" name="calendarView" id="week-view-button" checked />
                        </label>
                        <label>
                            <%= Resources.Resource.Month %> 
                            <input mbsc-segmented type="radio" name="calendarView" id="month-view-button" />
                        </label>
                    </div>
                </div>
            </div>

            <div class="panel-body">
                <div class="md-employee-allocations">
                    <div id="demo-employee-allocations-calendar"></div>
                </div>
            </div>
        
            <div class="panel-footer text-right no-padding">
		        <div class="row" style="margin:10px">
                    <a href="/calani/Projects/PreviewPdf.ashx?q=scheduler" target="_blank" class="btn btn-default" id="href-download-pdf">
                        <i class="icon-file-pdf position-left"></i> 
                        <%= Resources.Resource.Download %> (PDF)
			        </a>
			        <a href="/calani/Time/AbsenceRequests/AbsenceRequest.aspx" class="btn btn-default" runat="server" ID="addUserButton">
                        <i class="icon-plus2 position-left"></i> 
                        <%= Resources.Resource.AbsenceRequest %>                                   
			        </a>
		        </div>
	        </div><!-- panel-footer -->
        </div>
    </div>

    <div id="message-popup-employee">
        <div class="mbsc-form-group">
            <label for="project-autocomplete" id="warning-popup-message-employee" class="text-danger">
            </label>
        </div>
    </div>

    <div id="message-popup-other-resource">
        <div class="mbsc-form-group">
            <label for="project-autocomplete" id="warning-popup-message-other-resource" class="text-danger">
            </label>
        </div>
    </div>

<div id="modal_form_vertical2" class="modal-content" style="display:none">
    <div class="modal-header bg-green">
        <h5 class="modal-title" id="allocation-popup-title">Title</h5>
    </div>
    <div class="modal-body">
        <form id="modal_form_vertical" action="#">
            <div class="modal-body">
                <div class="form-group">
                    <div class="row hidden" id="error_both_title_project_empty">
                        <div class="col-sm-12">
                            <label class="dialog_error">Either Title or Project has to be provided.</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label><%= Resources.Resource.Title %></label>
                                        <input type="text" id="title-input" class="form-control description" placeholder="<%= Resources.Resource.Optional %>"></input>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group dialogClientSelection">
                    <div class="row">
                        <div class="col-sm-6">
                            <label><%= Resources.Resource.Client %></label>
                            <select class="clientSelect" tabindex="-1" aria-hidden="true"></select>
                        </div>
                        <div class="col-sm-6">
                            <label><%= Resources.Resource.Project %></label>
                            <select class="jobSelect" placeholder="choisissez un client" tabindex="-1" aria-hidden="true"></select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row hidden" id="error_enddate_before_startdate">
                        <div class="col-sm-12">
                            <label class="dialog_error"><%= Resources.Resource.StartEndDatesValidationMessage %></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label><%= Resources.Resource.Start %></label>
                            <div class="input-group">
                                <input type="text" class="form-control start-date" value="">
                                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                                <input type="text" class="form-control start-time" value="">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <label><%= Resources.Resource.End %></label>
                            <div class="input-group">
                                <input type="text" class="form-control end-date" value="">
                                <span class="input-group-addon"><i class="icon-calendar22"></i></span>
                                <input type="text" class="form-control end-time" value="">
                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row checkbox-type-of-recurring">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="checkbox" id="all-day-checkbox" />
                            <%= Resources.Resource.AllDay %>
                        </div>
                    </div>
                </div>
                <div class="row checkbox-type-of-recurring">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label> <%= Resources.Resource.Duration %></label>
                            <span id="allocation-hours-temp">0h</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label><%= Resources.Resource.Comment %></label>
                                    <input type="text" id="comment-input" class="form-control description" placeholder="<%= Resources.Resource.WriteComment %>"></input>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </form>
        <div id="sweet_prompt"></div>
    </div>
</div>

    <script type="text/javascript">
        var CURRENT = {
            organizationId: <%= ph.CurrentOrganizationId %>,
            userId: <%= ph.CurrentUserId %>,
            isAdmin: <%= IsAdmin.ToString().ToLowerInvariant() %>,
            baseUrl: '<%= BaseUrl %>',
            localizedDateFormat: '<%= ph.localisedDateFormat %>',
            fullWeek: <%= FullWeek.ToString().ToLowerInvariant() %>,
            workDay: {
                startWorkingHour: <%= WorkDay.StartTime.Hours %>,
                startWorkingMin: <%= WorkDay.StartTime.Minutes %>,
                endWorkingHour: <%= WorkDay.EndTime.Hours %>,
                endWorkingMin: <%= WorkDay.EndTime.Minutes %>,
                lunchBreakDurationMin: <%= WorkDay.LunchDuration.TotalMinutes %>
            }
        };

        var CELL_HEIGHT_PX = <%= CalendarCellHeightPx %>;
        var SCHEDULER_READONLY = <%= (!IsAdmin).ToString().ToLowerInvariant() %>;

        var projects = <%= ProjectsJson %>;
        var clients = <%= ClientsJson %>;
        var employees = <%= ResourcesJson %>;
        var allocations = <%= AllocationsJson %>;
        var invalid = <%= InvalidDatesJson %>;

    </script>

    <%--<script type="text/javascript" src="/calani/scheduler/scheduler.js?<%= Guid.NewGuid().ToString("n") %>"></script>--%>
    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Time/Scheduler/scheduler.js") %>"></script>

</asp:Content>

