﻿var workingTimeCalculatorInst = new WorkingTimeCalculator(CURRENT.workDay.startWorkingHour, CURRENT.workDay.startWorkingMin, CURRENT.workDay.endWorkingHour, CURRENT.workDay.endWorkingMin, CURRENT.workDay.lunchBreakDurationMin);
var EMP_PREFIX = "emp_";
var OTHRES_PREFIX = "othres_";
var DEPARTMENTS_PREFIX = "depa_";
var calendar;
var allocationWarningPopupOtherResource;
var allocationWarningPopupEmployee;
var formatDate = mobiscroll.util.datetime.formatDate;
var weekViewButton = document.getElementById('week-view-button');
var monthViewButton = document.getElementById('month-view-button');
var downloadPdfLink = document.getElementById('href-download-pdf');
var READONLY = SCHEDULER_READONLY ? true : false;
var popupControls;

/* Controls initialization */

var locale = lang4 == 'en-US' ? mobiscroll.localeEn : mobiscroll.localeFr;
locale.dateFormatLong = "D DDD MMM YYYY";

var currWeek = workingTimeCalculatorInst.getCurrentWeekStartAndEndDates();
setDownloadPdfUrl(lang4, currWeek[0], currWeek[1]);

mobiscroll.setOptions({
    locale: locale,
    theme: 'ios',
    themeVariant: 'light'
});

calendar = mobiscroll.eventcalendar('#demo-employee-allocations-calendar', {
    view: {
        timeline: {
            type: 'week',
            eventList: true,
            startDay: 1,
            endDay: CURRENT.fullWeek === true ? 7 : 5
        },
    },
    height: 500,
    data: allocations,
    dragToCreate: false,
    dragToResize: false,
    dragToMove: false,
    clickToCreate: false,
    exclusiveEndDates: true,
    resources: employees,
    invalid: invalid,

    onInit: function (event, inst) {
        //createChartAllocations(inst);
        refreshAvailableTimeForResources(getDayOfCurrentWeek(1), getDayOfCurrentWeek(7), inst);
    },

    renderScheduleEventContent: function (event) {
        var icon = event.original.icon ? '<i class="fa fa-' + event.original.icon + ' position-left"></i>' : '';
        var content = icon + event.title;
        return content;
    },

    onEventClick: function (args, inst) {
        if (args.event.type != 'project') return;

        openEditPopup(args);
    },

    renderResource: function (resource) {
        var cssClass = resource.group ? "md-employees-child" : "md-employees-parent";

        var percent = Math.round(resource.timePercent * 10) / 10;

        return '<table class="' + cssClass + '"><tr>' +
            (resource.group && !READONLY ? '<td class="md-employees-button"><a href="" class="btn btn-default" onClick="openAddPopup(\'' + resource.id + '\'); return false;"><i class="icon-plus2"></i></a></td>' : '<td class="md-employees-button"></td>') +
            '<td class="md - employees - cont">' +
            '<div class="md-employees-name">' + resource.name + '</div>' +
            (resource.group ? '<div class="md-employees-group">(' + STRING_RESOURCES[resource.group] + ')</div>' : '') + '</td>' +
            //(CURRENT.isAdmin ? (resource.minutes > 0 ? ('<td class="md-employees-hours">' + percent + '%</td>') : ('<td class="md-employees-hours red">' + percent + '%</td>')) : '') +
            '</tr></table>';
    },

    //renderScheduleEvent: function (ev, inst) {

    //    if (ev.original.type === 'chart') {
    //        var height = calculateChartHeightPx(ev.original);
    //        return '<div style="background-color: ' + ev.color + '; margin-top: ' + (CELL_HEIGHT_PX - height) + 'px; border-top: ' + height + 'px solid ' + ev.color + ';"></div>';
    //    }

    //    var icon = '';
    //    if (ev.original.type === 'absence') {
    //        icon = '<i class="fa fa-' + ev.original.icon +' position-left"></i>';
    //    }

    //    return '<div class="mbsc-schedule-event mbsc-ios mbsc-ltr mbsc-timeline-event mbsc-timeline-event-listing mbsc-timeline-event-start mbsc-timeline-event-end mbsc-schedule-event-all-day" data-id="'+ev.id+'" title="'+ev.title+'" style="width: 100%; color: rgb(226, 0, 0);"><div class="mbsc-schedule-event-background mbsc-timeline-event-background mbsc-schedule-event-all-day-background mbsc-ios" style="background: '+ev.color+';"></div><div class="mbsc-schedule-event-inner mbsc-ios mbsc-schedule-event-all-day-inner" style="color: rgb(0, 0, 0);"><div class="mbsc-schedule-event-title mbsc-schedule-event-all-day-title mbsc-ios">'+ icon + ev.title +'</div></div></div>';
    //},

    groupBy: 'group',

    onPageChange: function (event, inst) {
        loadAllocationsForPeriod(event.firstDay, event.lastDay);

        // Recalculate working hours for employees/resources
        //refreshAvailableTimeForResources(event.firstDay, event.lastDay, inst);

        setDownloadPdfUrl(lang4, event.firstDay, event.lastDay);
    }

});

allocationWarningPopupOtherResource = mobiscroll.popup('#message-popup-other-resource', {
    headerText: STRING_RESOURCES.Warning,
    buttons: [
        {
            text: STRING_RESOURCES.OK,
            keyCode: "Enter",
            handler: function () {
                allocationWarningPopupOtherResource.close();
            }
        }
    ]
});

allocationWarningPopupEmployee = mobiscroll.popup('#message-popup-employee', {
    headerText: STRING_RESOURCES.Warning,
});

monthViewButton.addEventListener('click', function () {
    calendar.setOptions({
        view: {
            timeline: {
                type: 'month',
                eventList: true,
                startDay: 1,
                endDay: 31
            },
        }
    });
});

weekViewButton.addEventListener('click', function () {
    calendar.setOptions({
        view: {
            timeline: {
                type: 'week',
                eventList: true,
                startDay: 1,
                endDay: 5
            },
        }
    });
});

function openAddPopup(resourceId) {

    if (READONLY) return;

    var resource = getResource(resourceId);

    var allocation = {
        id: 0,
        type: 'project',
        resource: resourceId
    };

    popupControls = initBootboxDialog("add", resource, allocation);
}

function openEditPopup(args) {

    if (READONLY) return;

    var allocation = getAllocation(args.event.id);
    if (!allocation || allocation.chart) return;

    var resource = getResource(allocation.resource);

    popupControls = initBootboxDialog("update", resource, allocation);

    setTimeout(setAllDayFromTimeInputs, 20);
}

function initBootboxDialog(actionStr, resourceObj, allocationObj) {

    var controls = {};

    var headerText = '<div>' + (actionStr == 'add' ? STRING_RESOURCES.NewProjectAllocation : STRING_RESOURCES.EditProjectAllocation) +
        '</div><div class="employee-shift-day">' + resourceObj.name + '</div>';
    $("#allocation-popup-title").html(headerText);

    var buttons = {
        cancel: {
            label: STRING_RESOURCES.Cancel,
            className: 'btn-info',
            callback: function () {
                bootbox.hideAll();
            }
        }
    };

    switch (actionStr) {
        case "add":
            buttons.add = {
                label: STRING_RESOURCES.Add,
                className: "bg-success",
                callback: function () {
                    updateAllocationProperties(allocationObj, controls);
                    addOrUpdateAllocation(allocationObj, "add");
                    return false;
                }
            };
            break;

        case "update":

            buttons.delete = {
                label: STRING_RESOURCES.Delete,
                className: "bg-danger",
                callback: function () {
                    deleteAllocationOnServer(allocationObj);
                }
            };

            buttons.update = {
                label: STRING_RESOURCES.Save,
                className: "bg-success",
                callback: function () {
                    var uneditedAllocation = updateAllocationProperties(allocationObj, controls);
                    addOrUpdateAllocation(allocationObj, "update", uneditedAllocation);
                    return false;
                },
            };

            break;

        default: break;
    }

    bootbox.dialog({
        message: document.getElementById("modal_form_vertical2").innerHTML,
        backdrop: false,
        onEscape: true,
        closeButton: true,
        buttons: buttons
    });

    var projectSelect2 = new jobSelect();
    projectSelect2.init(projects);

    var clientsSelect2 = new clientSelect(clients, {
        onChangeHandler: function () {

            var selectedClientId = clientsSelect2.getValue()[0].id;
            var filteredProjects = $.grep(projects, function (p) { return p.clientId == selectedClientId; });

            $(projectSelect2.selector).select2('destroy');
            $(projectSelect2.selector).html('');
            $(projectSelect2.selector).select2({
                data: filteredProjects
            })
        }
    });

    var dateTimePickersObj = initDateTimePickers(allocationObj, resourceObj);

    $(".bootbox-body #all-day-checkbox").change(function () { onAllDayChanged(resourceObj.rate); });

    controls = Object.assign(controls, dateTimePickersObj);
    controls.projectSelect = projectSelect2;
    controls.clientSelect = clientsSelect2;
    controls.commentInput = ".bootbox-body #comment-input";
    controls.titleInput = ".bootbox-body #title-input";
    controls.allDayCheckbox = ".bootbox-body #all-day-checkbox";
    
    if (actionStr == 'update') {

        var project = allocationObj.projectId ? getProject(allocationObj.projectId) : null;
        if (project) {
            clientsSelect2.setValue(project.clientId);
            projectSelect2.setValue(allocationObj.projectId);
        }
        $(controls.commentInput).val(allocationObj.comment);
        $(controls.titleInput).val(allocationObj.title);
        displayAllocationDurationInfo(resourceObj.rate);
    }

    return controls;
}

function initDateTimePickers(allocationObj, resourceObj) {
    var onChangeCallback = function () {
        displayAllocationDurationInfo(resourceObj.rate);
        
        var startDate = getStartDate();
        var endDate = getEndDate();

        validateStartEndDates(startDate, endDate);
        setAllDayFromTimeInputs(startDate, endDate);
    };

    return {
        startDatePicker: initDateInput('start-date', allocationObj.start, onChangeCallback),
        startTimeInput: initTimeInput('.bootbox-body .start-time', allocationObj.start, onChangeCallback),
        endDatePicker: initDateInput('end-date', allocationObj.end, onChangeCallback),
        endTimeInput: initTimeInput('.bootbox-body .end-time', allocationObj.end, onChangeCallback)
    };
}

function initDateInput(id, date, onChangeCallback) {
    var dateObj = typeof (date) === 'string' ? new Date(date) : date;
    return new datePick(id, CURRENT.localizedDateFormat, { locale: locale, startDate: dateObj, onApplyCallback: onChangeCallback });
}

function initTimeInput(selector, date, onChangeCallback) {

    var dateObj = typeof (date) === 'string' ? new Date(date) : date;

    var timeInput = $(selector);
    $(timeInput).val(moment(dateObj).format("HH:mm"));
    timeInput.blur(function () {
        checkHHMM($(this));
    });

    if (onChangeCallback) {
        timeInput.change(onChangeCallback);
    }
    
    timeInput.mask('00:00');

    return selector;
}

function getDateFromInputs(dateControlName, timeControlName) {
    var tempDateTimeFormat = "D/M/YYYY HH:mm";
    var time = $(popupControls[timeControlName]).val();
    var start = moment($(popupControls[dateControlName].id).val(), CURRENT.localizedDateFormat).format(tempDateTimeFormat).replace("00:00", time);
    var dateObj = new Date(moment(start, tempDateTimeFormat).format());

    return dateObj;
}

function getStartDate() {
    return getDateFromInputs('startDatePicker', 'startTimeInput');
}

function getEndDate() {
    return getDateFromInputs('endDatePicker', 'endTimeInput');
}

function validateAllocation(allocation) {
    var validDates = validateStartEndDates(allocation.start, allocation.end);
    var validTitle = validateTitle(allocation);
    return validDates && validTitle;
}

function validateStartEndDates(startDate, endDate) {
    
    var valid = startDate < endDate;
    var warningMsg = $(".bootbox-body #error_enddate_before_startdate");
    valid ? warningMsg.addClass("hidden") : warningMsg.removeClass("hidden");

    return valid;
}

function validateTitle(allocation) {

    var hasTitle = allocation && typeof (allocation.title) != 'undefined' && allocation.title != null && allocation.title != "";
    var hasProject = allocation && typeof (allocation.projectId) != 'undefined' && allocation.projectId != null && allocation.projectId > 0;
    var valid = hasTitle || hasProject;
    var warningMsg = $(".bootbox-body #error_both_title_project_empty");
    valid ? warningMsg.addClass("hidden") : warningMsg.removeClass("hidden");

    return valid;
}

function updateAllocationProperties(allocation, controls) {

    var uneditedAllocation = Object.assign({}, allocation);

    allocation.comment = $(controls.commentInput).val();
    allocation.start = getStartDate();
    allocation.end = getEndDate();

    var selectedOption = controls.projectSelect.getValue();
    var project = selectedOption && selectedOption.length > 0 && selectedOption[0].id != "0" ?
                    getProject(selectedOption[0].id) : null;

    allocation.projectId = project ? project.id : null;
    allocation.title = $(controls.titleInput).val();
    allocation.title = allocation.title == "" ? (project ? project.text : "") : allocation.title;
    allocation.color = project ? project.color : "#DEDEDE";

    return uneditedAllocation;
}

function addOrUpdateAllocation(allocation, action, uneditedAllocation) {

    if (!validateAllocation(allocation)) {
        allocation = Object.assign(allocation, uneditedAllocation);
        return;
    }

    if (isAllocationPossible(allocation, uneditedAllocation)) {

        postAllocationToServer(allocation, action, uneditedAllocation);
    }
    else {
        // Process overlapping / overtime allocation
        if (isForEmployee(allocation)) {
            // Employee
            setWarningMessage(allocation, uneditedAllocation);

            allocationWarningPopupEmployee.setOptions({
                buttons: [
                    'cancel',
                    {
                        text: STRING_RESOURCES.OkContinue,
                        keyCode: "Enter",
                        handler: function () {
                            postAllocationToServer(allocation, action, uneditedAllocation);
                            allocationWarningPopupEmployee.close();
                            bootbox.hideAll();
                        },
                        cssClass: "text-danger"
                    }
                ]
            });

            allocationWarningPopupEmployee.open();
        }
        else {
            // Other Resource
            setWarningMessage(allocation);
            allocationWarningPopupOtherResource.open();
        }
    }
}

function displayAllocationDurationInfo(resourceRate) {
    setTimeout(function () {
        var startDate = getStartDate();
        var endDate = getEndDate();
        var hours = workingTimeCalculatorInst.getWorkingMinutesBetween2Dates(startDate, endDate) * resourceRate / 60;
        hours = Math.round(hours * 10) / 10;
        $(".bootbox-body #allocation-hours-temp").text(hours + 'h');
    }, 20);
}

function setWarningMessage(allocation, uneditedAllocation) {
    var isEmployee = isForEmployee(allocation);
    var resourceGroupStr = isEmployee ?
        (STRING_RESOURCES["Employee"] ?? "").toLowerCase() :
        (STRING_RESOURCES["Resource_"] ?? "").toLowerCase();

    var message = [];
    if (checkIfOverlappingAllocationsExist(allocation)) {
        message.push((STRING_RESOURCES.ProjectHasBeenAssigned ?? "").replace("{0}", resourceGroupStr));
    }

    var remainingTimeMin = calculateRemainingResourceTimeAfterAllocationMin(allocation, uneditedAllocation);
    if (remainingTimeMin < 0) {
        var remainingTimeHours = Math.round(-remainingTimeMin * 10 / 60) / 10;
        var mess = (STRING_RESOURCES.ExceedNumberOfHours ?? "").replace("{0}", resourceGroupStr).replace("{1}", remainingTimeHours);
        message.push(mess);
    }

    var id = isEmployee ? "warning-popup-message-employee" : "warning-popup-message-other-resource";
    document.getElementById(id).innerHTML = message.join('<br/>');
}

/* All Day checkbox */

function isAllDayChecked() {
    return $(popupControls.allDayCheckbox)[0].checked;
}

function onAllDayChanged(resourceRate) {
    if (isAllDayChecked()) {
        setRangeValuesForAllDay();
    }

    displayAllocationDurationInfo(resourceRate);
}

function setRangeValuesForAllDay() {
    $(".bootbox-body .start-time").val("09:00");
    $(".bootbox-body .end-time").val("18:00");
}

function setAllDay(value) {
    $(".bootbox-body #all-day-checkbox")[0].checked = value;
}

function setAllDayFromTimeInputs(startDate, endDate) {

    startDate = (startDate && typeof (startDate) === 'object') ? startDate : getStartDate();
    endDate = (endDate && typeof (endDate) === 'object') ? endDate : getEndDate();

    var allDay = startDate.getHours() == workingTimeCalculatorInst.StartWorkingHour && startDate.getMinutes() == 0 &&
        endDate.getHours() == workingTimeCalculatorInst.EndWorkingHour && endDate.getMinutes() == 0;
    setAllDay(allDay);
}

/* Ajax methods */

function restoreDeleteAllocationOnServer(allocation) {

    if (READONLY) return;

    $.ajax({
        type: "GET",
        url: '../../API/Service.svc/RestoreDeletedResourceAllocation?id=' + allocation.id,
        dataType: "json",
        success: function (res) {
            console.log(res);
            allocations.push(allocation);
            calendar.addEvent(allocation);
            allocateTimeForResource(allocation);

            //addOrUpdateChart(allocation, "add", calendar.getInst());
        },
        error: function (a, b, c) {
            alert(a.responseText);
        }
    });
}

function deleteAllocationOnServer(allocation) {

    if (READONLY) return;

    $.ajax({
        type: "GET",
        url: '../../API/Service.svc/DeleteResourceAllocation?id=' + allocation.id,
        dataType: "json",
        success: function (res) {
            console.log(res);

            // save a local reference to the deleted event
            var deletedAllocation = allocation;

            deallocateTimeForResource(allocation);

            calendar.removeEvent(allocation);

            // Remove allocation from collection
            var index = allocations.findIndex(function (item) { return item.id == allocation.id; });
            allocations.splice(index, 1);
            //addOrUpdateChart(allocation, "delete", calendar.getInst());
            loadAllocationsForPeriod(getMonthStartDates(allocation.start), getMonthEndDates(allocation.start));
            mobiscroll.snackbar({
                button: {
                    action: function () {
                        restoreDeleteAllocationOnServer(deletedAllocation);
                    },
                    text: STRING_RESOURCES.Undo
                },
                message: STRING_RESOURCES.EventDeleted
            });
        },
        error: function (a, b, c) {
            alert(a.responseText);
        }
    });
}

function postAllocationToServer(allocation, action, uneditedAllocation) {
    if (READONLY) return;

    var model = {};
    model["Id"] = allocation.id;
    model["ProjectId"] = allocation.projectId;
    model["StartDateString"] = allocation.start.getTime ? allocation.start.getTime() : Date.parse(allocation.start);
    model["EndDateString"] = allocation.end.getTime ? allocation.end.getTime() : Date.parse(allocation.end);
    model["Comment"] = allocation.comment;
    model["Title"] = allocation.title;
    model["CurrentUserId"] = CURRENT.userId;
    model["OrganizationId"] = CURRENT.organizationId;

    // Transform employees' and other resources ids into numbers
    if (allocation.resource.includes(EMP_PREFIX)) {
        model["OtherResourceId"] = null;
        model["DepartmentId"] = null;
        model["EmployeeId"] = allocation.resource.replace(EMP_PREFIX, "");
    }
    else if (allocation.resource.includes(OTHRES_PREFIX)) {
        model["EmployeeId"] = null;
        model["DepartmentId"] = null;
        model["OtherResourceId"] = allocation.resource.replace(OTHRES_PREFIX, "");
    }
    else if (allocation.resource.includes(DEPARTMENTS_PREFIX)) {
        model["EmployeeId"] = null;
        model["OtherResourceId"] = null;
        model["DepartmentId"] = allocation.resource.replace(DEPARTMENTS_PREFIX, "");
    }
    else {
        Console.error("Invalid allocation", allocation);
        return;
    }

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../API/Service.svc/AddOrUpdateResourceAllocation',
        data: JSON.stringify(model),
        dataType: "json",
        success: function (res) {
            
            if (res.success) {
                if (action === "add") {
                    allocation.id = res.id;
                    if (!allocation.resource.includes(DEPARTMENTS_PREFIX)) {
                        allocations.push(allocation);
                        calendar.addEvent(allocation);
                        allocateTimeForResource(allocation);
                    }
                    else {
                        loadAllocationsForPeriod(getMonthStartDates(allocation.start), getMonthEndDates(allocation.start));
                    }
                    
                }
                else if (action === "update") {
                    if (!allocation.resource.includes(DEPARTMENTS_PREFIX)) {
                        calendar.updateEvent(allocation);
                        deallocateTimeForResource(uneditedAllocation);
                        allocateTimeForResource(allocation);
                    }
                    else {
                        loadAllocationsForPeriod(getMonthStartDates(allocation.start), getMonthEndDates(allocation.start));
                    }
                   
                }
            }
            else {

                if (action === "update") {
                    calendar.updateEvent(uneditedAllocation);
                }

                console.error(res.message);
                console.log(res);
            }
            
            bootbox.hideAll();
            //addOrUpdateChart(allocation, "add", calendar.getInst());
        },
        error: function (a, b, c) {
            console.error(a.responseText);
        }
    });
}

function loadAllocationsForPeriod(startDate, endDate) {    
    if (!startDate || !endDate) return;
    var start = startDate.getTime ? startDate.getTime() : Date.parse(startDate);
    var end = endDate.getTime ? endDate.getTime() : Date.parse(endDate);
    var format = 'YYYY-MM-DDTHH:mm:ss';
    var startStr = moment(start).format(format);
    var endStr = moment(end).format(format);


    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '../../API/Service.svc/ListResourceAllocations?dtFrom=' + startStr + '&dtTo=' + endStr + '&locale=' + (lang4 ?? "fr-CH"),
        dataType: "json",

        success: function (res) {
            
            
            if (res.success) {

                var newAllocations = $.parseJSON(res.data);
                allocations = newAllocations;
                calendar.setEvents(allocations);
            }
            else {
                console.error(res.message);
                console.log(res);
            }
        },
        error: function (a, b, c) {
            console.error(a.responseText);
        }
    });
}

function getParentResourceId(resourceId) {
    return (resourceId.includes(EMP_PREFIX) ? EMP_PREFIX : resourceId.includes(OTHRES_PREFIX) ? OTHRES_PREFIX : DEPARTMENTS_PREFIX) + "0";
}

function getAllocation(id) {
    var foundItem = allocations.find(function (res) { return res.id == id });
    return foundItem;
}

function getProject(id) {
    var foundItem = projects.find(function (res) { return res.id == id });
    return foundItem;
}

function getResource(id) {
    var flattened = [];
    for (var i in employees) {
        var res = employees[i];
        if (!res.group && res.children) {
            flattened = flattened.concat(res.children);
            flattened.push(res);
        }
        else {
            flattened.push(res);
        }
    }
    var foundItem = flattened.find(function (res) { return res.id == id });
    return foundItem;
}

function isForEmployee(allocation) {
    return allocation.resource.toLowerCase().includes("emp_");
}

/* 
 * Date and time
 * Time allocation
 */

function getDateZeroTime(date) {
    return workingTimeCalculatorInst.getDateZeroTime(date);
}

function getNextDayDate(date) {
    return workingTimeCalculatorInst.getNextDayDate(date);
}

function getDayOfCurrentWeek(day) {
    var today = getDateZeroTime(new Date);
    var dayToday = today.getDay();
    var date = today.getDate() - (dayToday == 0 ? 7 : dayToday) + day;
    return new Date(today.setDate(date));
}

function getMonthStartDates(date) {
    var inputDate = new Date(date);
    var year = inputDate.getFullYear();
    var month = inputDate.getMonth();
    return new Date(year, month, 1, 0, 0, 0);
}

function getMonthEndDates(date) {
    var inputDate = new Date(date);
    var year = inputDate.getFullYear();
    var month = inputDate.getMonth();
    var endDate = new Date(year, month + 1, 0, 23, 59, 59);
    return new Date(year, month + 1, 0, 23, 59, 59);
}


function getWorkingMinutesSameDay(date1, date2) {
    /* date1 and date2 are expected to be the same day, different time */
    return workingTimeCalculatorInst.getWorkingMinutesSameDay(date1, date2);
}

function getWorkingMinutesBetween2Dates(date1, date2) {
    return workingTimeCalculatorInst.getWorkingMinutesBetween2Dates(date1, date2);
}

function calculateAllocatedTime(allocation, resource) {
    var minutes = Math.round(getWorkingMinutesBetween2Dates(allocation.start, allocation.end) * resource.rate);
    return minutes;
}

function allocateTimeForResource(allocation) {
    var resource = getResource(allocation.resource);
    var minutes = calculateAllocatedTime(allocation, resource);
    resource.minutes -= minutes;
    resource.timePercent = resource.minutesTotal > 0 ? (resource.minutes * 100 / resource.minutesTotal) : 0;
}

function deallocateTimeForResource(allocation) {
    var resource = getResource(allocation.resource);
    var minutes = calculateAllocatedTime(allocation, resource);
    resource.minutes += minutes;
    resource.timePercent = resource.minutesTotal > 0 ? (resource.minutes * 100 / resource.minutesTotal) : 0;
}

function refreshAvailableTimeForResources(startDate, endDate, calendarInst) {
    var workingTimeMin = getWorkingMinutesBetween2Dates(startDate, endDate);
    var allAllocations = calendarInst.getEvents(startDate, endDate);

    for (var groupIndex in employees) {
        var sum = 0;
        for (var index in employees[groupIndex].children) {

            var resource = employees[groupIndex].children[index];
            var allocations = allAllocations.filter(function (a) { return a.resource == resource.id; });

            // Sum up allocated time for the resource
            var allocatedTimeMin = allocations.reduce(function (sum, a) {
                return sum + calculateAllocatedTime(a, resource);
            }, 0);

            var availableTimeMin = Math.round(workingTimeMin * resource.rate);
            resource.minutes = availableTimeMin - allocatedTimeMin;
            resource.timePercent = availableTimeMin > 0 ? (resource.minutes * 100 / availableTimeMin) : 0;
            resource.minutesTotal = availableTimeMin;

            sum += resource.minutes;
            //console.log(resource.name + ": available=" + availableTimeMin + " allocated=" + allocatedTimeMin);
        }

        employees[groupIndex].minutes = sum;
    }
}

function isAllocationPossible(allocation, uneditedAllocation) {
    var res = isEnoughTimeForAllocation(allocation, uneditedAllocation) &&
        !checkIfOverlappingAllocationsExist(allocation);
    return res;
}

function isEnoughTimeForAllocation(allocation, uneditedAllocation) {
    var remainingTime = calculateRemainingResourceTimeAfterAllocationMin(allocation, uneditedAllocation);
    return remainingTime >= 0;
}

function calculateRemainingResourceTimeAfterAllocationMin(allocation, uneditedAllocation) {
    var resource = getResource(allocation.resource);
    var minutesNew = calculateAllocatedTime(allocation, resource);
    var minutesOld = uneditedAllocation ? calculateAllocatedTime(uneditedAllocation, resource) : 0;
    return resource.minutes + minutesOld - minutesNew;
}

function checkIfOverlappingAllocationsExist(allocation) {
    var overlappingAllocations = $.grep(allocations, function (a) { return a.resource == allocation.resource && checkOverlap(a, allocation); });
    return overlappingAllocations.length > 0;

}

function checkOverlap(allocation1, allocation2) {
    if (allocation1.id == allocation2.id) return false;

    var start1 = (typeof (allocation1.start) == "string") ? new Date(allocation1.start) : allocation1.start;
    var start2 = (typeof (allocation2.start) == "string") ? new Date(allocation2.start) : allocation2.start;
    var end1 = (typeof (allocation1.end) == "string") ? new Date(allocation1.end) : allocation1.end;
    var end2 = (typeof (allocation2.end) == "string") ? new Date(allocation2.end) : allocation2.end;

    var overlap = !(end1 <= start2 || end2 <= start1);

    return overlap;
}

/*
 *  Other
 */

function setDownloadPdfUrl(locale, startDate, endDate) {

    var days = (endDate - startDate) / (1000 * 60 * 60 * 24);
    if (days <= 7) {
        endDate = moment(startDate).add(CURRENT.fullWeek === true ? 6 : 4, 'days');
    }
    else {
        endDate = moment(endDate).add(-1, 'days');
    }

    var params = [];

    if (locale) {
        params.push("c=" + locale);
    }

    if (startDate && endDate) {
        params.push("from=" + moment(startDate).format("DD-MM-YYYY"));
        params.push("to=" + moment(endDate).format("DD-MM-YYYY"));

    }

    downloadPdfLink.href = "/calani/Projects/PreviewPdf.ashx?q=scheduler&" + params.join("&");
}