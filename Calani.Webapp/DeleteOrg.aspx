﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="DeleteOrg" Codebehind="DeleteOrg.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">

        <div class="content">

		    <div class="row">

                <!-- left column -->
			    <div class="col-sm-12 col-md-12">


                        <asp:Panel runat="server" ID="panelError" class="panel panel-danger panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title">
                                    Are you sure to delete all this organization ?
							    </h6>
						    </div>

						    <div class="panel-body">



                                <!-- name -->
								<div class="form-group">
									<label class="control-label col-lg-2">ID</label>
									<div class="col-lg-10">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-database"></i></span>
											<asp:TextBox runat="server" ID="lblID" ReadOnly="true" CssClass="form-control" Value="666"></asp:TextBox>
										</div>
									</div>
								</div>
                                <!-- /name -->


                                <div>
                                    &nbsp;
                                </div>

                                <!-- buttonsaction -->
								    <div class="buttonsaction text-right" runat="server" id="panelActions">
										<label class="control-label col-lg-2"></label>
										<div class="col-lg-10">
											<div class="btn-group">
                                                <asp:LinkButton runat="server" ID="btnSave" class="btn btn-danger" OnClick="btnSave_Click" >
                                                    <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                                </asp:LinkButton>
                                                
									            
											</div>
										</div>
									</div>
                                    <!-- /buttonsaction -->

						    </div>
					    </asp:Panel><!-- panelError -->

                      
           


			    </div>
			    <!-- /left column -->
                

                <!-- right column -->
                <div class="col-sm-12 col-md-4">
                    
			    </div>
                <!-- /right column -->
						

		    </div> <!-- /row -->


					




        </div><!-- /content -->

    </form>
   
</asp:Content>

