﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Calani.BusinessObjects.Generic;

public partial class MasterPageNoSession : System.Web.UI.MasterPage
{
    public CurrentMenuProperty CMP { get; set; }

    public string CurrentLanguage2
    {
        get
        {
            string ret = "en";
            try
            {
                ret = System.Threading.Thread.CurrentThread.CurrentCulture.ToString().ToLower().Substring(0, 2);
            }
            catch { }
            return ret;
        }
    }

    public string CurrentLanguage4
    {
        get
        {
            string ret = "en-US";
            try
            {
                ret = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
                if (ret == "fr-CH") ret = "fr-FR";
            }
            catch { }
            return ret;
        }
    }

    public string MomentDefaultDateFormat
    {
        get { return Tools.MomentDefaultDateFormat; }
    }
   
    public string MomentDefaultDateShortTimeFormat
    {
        get { return Tools.MomentDefaultDateShortTimeFormat; }
    }

    public string LanguageSwitch { get; set; }
    public string UserName { get; set; }
    public bool IsNewTutorials { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {

        LabelPageTitle.Text = this.Page.Title;
        LabelCopyYear.Text = ConfigurationSettings.AppSettings["copyYear"];
        HyperLinkCopy.Text = ConfigurationSettings.AppSettings["copyrightText"];
        HyperLinkCopy.NavigateUrl = ConfigurationSettings.AppSettings["copyrightUrl"];

        lnkDoc.HRef = System.Configuration.ConfigurationManager.AppSettings["DocumentationUrl"];

       
       
            btnHelp.HRef = "http://help.gesmobile.ch/";
        
    }


}