﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Settings_Emails_EmailTemplate : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        btnPreview.Attributes["data-target"] = "#" + modal_default.ClientID;
    }

    public string DefaultContent { get; set; }
    public string DefaultTitle { get; set; }

    public string JsDefinition { get; set; }


    public string TemplateName { get; set; }

    public void Render(string templateName)
    {
        TemplateName = templateName;

        PageHelper ph = new PageHelper(this.Page);
        ph.DefineLanguage();

        Calani.BusinessObjects.Membership.LoginManager lmgr = new Calani.BusinessObjects.Membership.LoginManager();
        var user = lmgr.GetUser(HttpContext.Current.User.Identity.Name);
        Calani.BusinessObjects.Settings.DocumentTemplateManager mgr = new Calani.BusinessObjects.Settings.DocumentTemplateManager(user.id, System.Threading.Thread.CurrentThread.CurrentUICulture);
        var tpl = mgr.Get(templateName);
        if (tpl.UserTemplate != null)
        {
            tbxContent.Value = tpl.UserTemplate.content;
            tbxSubject.Text = tpl.UserTemplate.title;
        }
        else
        {
            tbxContent.Value = tpl.DefaultContent;
            tbxSubject.Text = tpl.DefaultTitle;
        }

        panelSubject.Visible = (tpl.DefaultTitle != null);
        

        lblTitle.Text = tpl.Title;
        spanVarsdescription.InnerHtml = tpl.VariableDescriptions.Replace("\n", "\n<br/>");

        EmailTemplateJsDefinition def = new EmailTemplateJsDefinition();
        def.DefaultContent = tpl.DefaultContent;
        def.DefaultTitle = tpl.DefaultTitle;
        JsDefinition = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(def);
    }



    public void Save(string templateName)
    {
        if (!String.IsNullOrWhiteSpace(tbxContent.Value))
        {
            Calani.BusinessObjects.Membership.LoginManager lmgr = new Calani.BusinessObjects.Membership.LoginManager();
            var user = lmgr.GetUser(HttpContext.Current.User.Identity.Name);
            Calani.BusinessObjects.Settings.DocumentTemplateManager mgr = new Calani.BusinessObjects.Settings.DocumentTemplateManager(user.id, System.Threading.Thread.CurrentThread.CurrentUICulture);

            mgr.Update(templateName, tbxContent.Value, tbxSubject.Text);
        }
    }
}

public class EmailTemplateJsDefinition
{
    public string DefaultContent { get; set; }
    public string DefaultTitle { get; set; }
}