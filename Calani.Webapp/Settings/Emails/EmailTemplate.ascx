﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Settings_Emails_EmailTemplate" Codebehind="EmailTemplate.ascx.cs" %>

<div runat="server" id="modal_default" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<div class="modal-body">
                <asp:Label runat="server" ID="lblPreview"></asp:Label>
		    </div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.Close %></button>
			</div>
		</div>
	</div>
</div>

<div class="panel panel-flat">
	<div class="panel-heading">
		<h6 class="panel-title"><asp:Label runat="server" ID="lblTitle" /></h6>
        <div class="heading-elements">
			<button type="button" class="btn btn-default" id="btnReset" runat="server">
                <i class="icon-reset position-left"></i>
                <%= Resources.Resource.Reset %>
			</button>
            <button type="button" class="btn bg-teal-400" runat="server" id="btnPreview" 
                data-toggle="modal" data-target="toto">
                <i class="icon-play4 position-left"></i>
                <%= Resources.Resource.Preview %>
            </button>
		</div>
	</div>

	<div class="panel-body">


        <div class="form-horizontal" runat="server" id="panelSubject">

			<fieldset class="content-group">

                <!-- name -->
				<div class="form-group">
					<label class="control-label col-lg-2"><%= Resources.Resource.Subject %></label>
					<div class="col-lg-10">
						<div class="input-group">
							<span class="input-group-addon"><i class="icon-bubble9"></i></span>
							<asp:TextBox CssClass="form-control" runat="server" ID="tbxSubject" />
						</div>
					</div>
				</div>
                <!-- /name -->

            </fieldset>
        </div>

        <textarea runat="server" id="tbxContent" class="form-control" rows="7">     
        </textarea>


        

    </div>

    <div class="panel-footer">
        <div class="heading-elements">
            <span class="heading-text small" runat="server" id="spanVarsdescription">
                
            </span>
        </div>
	</div>


    <script type="text/javascript">



        $(function () {

            var <%= btnReset.ClientID %>Definition = <%= JsDefinition %>;

                $("#<%= btnReset.ClientID %>").click(function (e) {

                    swal({
                        text: "<%= Resources.Resource.ResetToDefault %>",
                        
                        showCancelButton: true,
                        cancelButtonText: "<%= Resources.Resource.No_Cancel %>",
                        
                        confirmButtonText: "<%= Resources.Resource.YesReset %>"
                    })
                    
                    .then((value) => {
                        
                        
                        if (value.value != null && value.value == true) {
                            $("#<%= tbxContent.ClientID %>").val(<%= btnReset.ClientID %>Definition.DefaultContent);
                            $("#<%= tbxSubject.ClientID %>").val(<%= btnReset.ClientID %>Definition.DefaultTitle);
                        }
                            
                        });


                });


                $("#<%= btnPreview.ClientID %>").click(function (e) {
                    var text = "";
                    if ($("#<%= tbxSubject.ClientID %>").length > 0) {
                        text += "<h5>"
                        text += $("#<%= tbxSubject.ClientID %>").val();
                        text += "</h5><div>";
                    }
                    text += $("#<%= tbxContent.ClientID %>").val();
                    text += "</div>";

                    for (var i = 0; i < variables.length; i++) {
                        var a = "$" + variables[i].name;
                        var b = variables[i].value;


                        if (a == "$doctype" && '<%= TemplateName %>' == "SendQuote") {
                            b = "<%= Resources.Resource.Quote %>";
                        }
                        if (a == "$doctype" && '<%= TemplateName %>' == "SendInvoice") {
                            b = "<%= Resources.Resource.Invoice %>";
                        }

                        if (b == null) b = '';
                        while (text.indexOf(a) >= 0) {
                            text = text.replace(a, b);
                        }
                    }
                    while (text.indexOf('\n') >= 0) {
                        text = text.replace('\n', '<br />');
                    }
                    $('#<%= lblPreview.ClientID %>').html(text);
                });


                $("#<%= spanVarsdescription.ClientID %> code").css('cursor', 'pointer');
                $("#<%= spanVarsdescription.ClientID %> code").data("tbx", $('#<%= tbxContent.ClientID %>'));
                $("#<%= spanVarsdescription.ClientID %> code").click(function () {


                    var span = $(this);
                    var el = $(span.data("tbx"));
                    var newText = span.html() + " ";

                    var start = el.prop("selectionStart");
                    var end = el.prop("selectionEnd");
                    var text = el.val();
                    var before = text.substring(0, start);
                    var after = text.substring(end, text.length);
                    el.val(before + newText + after);
                    el[0].selectionStart = el[0].selectionEnd = start + newText.length;
                    el.focus();



                    alert(tbx);
                })

        });

        </script>
</div>