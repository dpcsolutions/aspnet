﻿<%@ Page Title="Email templates" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Settings_Emails_Default" Codebehind="Default.aspx.cs" %>
<%@ Register Src="~/Settings/Emails/EmailTemplate.ascx" TagPrefix="uc1" TagName="EmailTemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<form runat="server">
    <div class="content">
            <script type="text/javascript">
                var variables = <%= JsonVariables %> ;
            </script>

         <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Done %></h6>
						    </div>

						    <div class="panel-body">
							    <%= Resources.Resource.ChangesSaved %>
						    </div>
		</asp:Panel><!-- panelSuccess -->

            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <uc1:EmailTemplate runat="server" id="tplSignature" />
                </div>
                <div class="col-sm-12 col-md-6">
                    
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <uc1:EmailTemplate runat="server" id="tplSendQuote" />
                </div>
                <div class="col-sm-12 col-md-6">
                    <uc1:EmailTemplate runat="server" id="tplSendInvoice" />
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <uc1:EmailTemplate runat="server" id="tplSendReminder" />
                </div>
                <div class="col-sm-12 col-md-6">
                    <uc1:EmailTemplate runat="server" id="tplSendDeliveryNote" />
                </div>
            </div>


            


            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <asp:Panel runat="server" ID="panel3" class="panel panel-white">




                        <div class="panel-body">


                            

                            <!-- buttonsaction -->
								    <div class="buttonsaction text-right">
										<label class="control-label col-lg-2"></label>
										<div class="col-lg-10">
											<div class="btn-group">
                                                <asp:LinkButton runat="server" ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click">
                                                    <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                                </asp:LinkButton>
                                               
											</div>
										</div>
									</div>
                            <!-- /buttonsaction -->
                        </div>
                        

                    </asp:Panel>
                </div>
            </div>
       
    </div>
</form>
</asp:Content>
