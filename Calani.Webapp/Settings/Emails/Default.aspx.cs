﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.DocGenerator;

public partial class Settings_Emails_Default : System.Web.UI.Page
{
    public string JsonVariables { get; set; }

    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack) Load();

        var vars = ph.GetVariablesForTemplates();
        vars.Add(new JsVariableForTemplate { name = "doctype", value = Resources.Resource.Quote });
        vars.Add(new JsVariableForTemplate { name = "docnr", value = "123456" });
        vars.Add(new JsVariableForTemplate { name = "reminder", value = "2" });
        vars.Add(new JsVariableForTemplate { name = "expdate", value = "12/12/2099" });
        vars.Add(new JsVariableForTemplate { name = "contacttitle", value = Resources.Resource.Title_Mr });
        vars.Add(new JsVariableForTemplate { name = "contactfn", value = "John" });
        vars.Add(new JsVariableForTemplate { name = "contactln", value = "Doe" });


        Calani.BusinessObjects.Contacts.EmployeesManager mgr = new Calani.BusinessObjects.Contacts.EmployeesManager(ph.CurrentOrganizationId);
        Calani.BusinessObjects.CustomerAdmin.OrganizationManager org = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(ph.CurrentOrganizationId);
        org.Load();
        var me = mgr.Get(ph.CurrentUserId);
        if (me != null)
        {
            vars.Add(new JsVariableForTemplate { name = "myfn", value = me.firstName});
            vars.Add(new JsVariableForTemplate { name = "myln", value = me.lastName });
            vars.Add(new JsVariableForTemplate { name = "mytitle", value = me.title });
            vars.Add(new JsVariableForTemplate { name = "myemail", value = me.primaryEmail });
            vars.Add(new JsVariableForTemplate { name = "myphone", value = me.primaryPhone });
            vars.Add(new JsVariableForTemplate { name = "mymobile", value = me.secondaryPhone });



            vars.Add(new JsVariableForTemplate { name = "mycompany", value = org.CompanyName });
            vars.Add(new JsVariableForTemplate { name = "mycompstreet", value = org.AdrStreet });
            vars.Add(new JsVariableForTemplate { name = "mycompstrnr", value = org.AdrStreetNb });
            vars.Add(new JsVariableForTemplate { name = "mycompzip", value = org.AdrZipCode });
            vars.Add(new JsVariableForTemplate { name = "mycompcity", value = org.AdrCity });
            vars.Add(new JsVariableForTemplate { name = "mycompcountry", value = org.AdrCountry });
            vars.Add(new JsVariableForTemplate { name = "mycompstate", value = org.AdrState });
            vars.Add(new JsVariableForTemplate { name = "mycompfax", value = org.Telecopy });
            vars.Add(new JsVariableForTemplate { name = "mycompaddr2", value = org.AdrStreet2 });

        }

        JsonVariables = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(vars);
    }

    private void Load()
    {

        tplSignature.Render("Signature");
        tplSendInvoice.Render("SendInvoice");
        tplSendQuote.Render("SendQuote");
        tplSendReminder.Render("SendReminder");
        tplSendDeliveryNote.Render("SendDeliveryNote");

    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        tplSignature.Save("Signature");
        tplSendInvoice.Save("SendInvoice");
        tplSendQuote.Save("SendQuote");
        tplSendReminder.Save("SendReminder");
        tplSendDeliveryNote.Save("SendDeliveryNote");
        panelSuccess.Visible = true;
        Load();

        
    }
}

