﻿<%@ WebHandler Language="C#" Class="Auto" %>

using System;
using System.Web;

public class Auto : IHttpHandler {

    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";

        string from = context.Request.Params["from"];
        string to = context.Request.Params["to"];

        if (!String.IsNullOrEmpty(from) && !String.IsNullOrEmpty(to))
        {
            Calani.BusinessObjects.Currency.CurrencyConverter cc = new Calani.BusinessObjects.Currency.CurrencyConverter();
            double d = cc.Convert(1, from, to);
            context.Response.Write(d.ToString(System.Globalization.CultureInfo.InvariantCulture));
        }
        else
        {
            context.Response.Write("Hello you. don't try to do that :)");
        }
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}