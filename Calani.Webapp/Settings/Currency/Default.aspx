﻿<%@ Page Title="ForeignCurrencies" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Settings_Currency_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
        <div class="content">
            <div>

                <div class="panel panel-white">
                            
				    <div class="panel-heading">
					    <h6 class="panel-title">
						    <a name="quotes">
							    <i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
							    <strong><%= Resources.Resource.ForeignCurrencies %></strong>
						    </a>
					    </h6>
				    </div><!-- panel-heading -->


                    <!-- http://localhost/calani/Settings/Global/ -->
                    <div class="panel-body">
                        <%= Resources.Resource.YourMainCurrencyIs %> <asp:Label ID="lblCurrentCurrency" runat="server" Font-Bold="true"></asp:Label> 
                        (<%= Resources.Resource.ToChangeItSee %> <a href="../Global/"><%= Resources.Resource.GlobalOptions %></a>).
                        <%= Resources.Resource.ThisPageAllowYouToSetForeignCurrencies %> 
                    </div>
                    
				    <div class="panel-body">
                        <asp:GridView runat="server" ID="grid" AutoGenerateColumns="false">
                            <Columns>
                                <asp:HyperLinkField DataTextField="currency1" HeaderText="CurrencyConverter" DataNavigateUrlFields="id" DataNavigateUrlFormatString="Edit.aspx?id={0}"/>
                                <asp:BoundField DataField="symbol" HeaderText="CurrencySymbol" />
                                <asp:BoundField DataField="rate" HeaderText="Rate" ItemStyle-CssClass="text-right"  HeaderStyle-CssClass="text-right" />
                            </Columns>
                        </asp:GridView>
                    </div><!-- panel-body -->

                    <div class="panel-footer text-right no-padding">
					    <div class="row" style="margin:10px">
						    <a href="Edit.aspx" class="btn btn-default"><i class="icon-plus2 position-left"></i> 
                                <%=Resources.Resource.Add_new_currency %>
                                        
						    </a>
					    </div>
				    </div><!-- panel-footer -->

                </div>

            </div>
        </div>
    </form>
</asp:Content>

