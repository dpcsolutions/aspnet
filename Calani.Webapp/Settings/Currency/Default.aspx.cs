﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Settings_Currency_Default : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Calani.BusinessObjects.Currency.CurrenciesManager mgr = new Calani.BusinessObjects.Currency.CurrenciesManager(ph.CurrentOrganizationId);
        var list = mgr.List();

        //grid:
        grid.DataSource = list;
        grid.DataBind();
        new Html5GridView(grid);


        Calani.BusinessObjects.CustomerAdmin.OrganizationManager omgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(ph.CurrentOrganizationId);
        omgr.Load();
        lblCurrentCurrency.Text =  omgr.Currency;
    }
}