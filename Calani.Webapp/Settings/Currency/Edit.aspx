﻿<%@ Page Title="Currency" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Settings_Currency_Edit" Codebehind="Edit.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <form runat="server">

        <div class="content">

		    <div class="row">

                <!-- left column -->
			    <div class="col-sm-12 col-md-6">

                        <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Error %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblErrorMessage" />
						    </div>
					    </asp:Panel><!-- panelError -->

                        <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Done %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblSuccess" />
                                <br />
                                <strong><a href="." id="linkBackList" runat="server"></a></strong>
                                    <%= Resources.Resource.or %>
                                <a href="Edit.aspx" id="linkNew" runat="server"></a>.
						    </div>
					    </asp:Panel><!-- panelSuccess -->

					    
					    <asp:Panel runat="server" ID="panelEdit" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><asp:Label runat="server" ID="lblTitle" /></h6>
						    </div>

						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">

                                        <!-- name -->
									    <div class="form-group">
										    <label class="control-label col-md-2"><%= Resources.Resource.Currency %></label>
										    <div class="col-md-5">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-cash2"></i></span>
												    <input type="text" class="form-control" runat="server" id="tbxCurrencyFrom" disabled="disabled" />
											    </div>
										    </div>
                                            <div class="col-md-5">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-cash2"></i></span>
												    <asp:DropDownList type="text" class="form-control" runat="server" id="ddlCurrencyTo">
                                                        <asp:ListItem Value="EUR" Text="EUR (€)" />
                                                        <asp:ListItem Value="USD" Text="USD ($)" />
                                                        <asp:ListItem Value="CAD" Text="CAD (C$)" />
                                                        <asp:ListItem Value="GBP" Text="GBP (£)" />
                                                        <asp:ListItem Value="YEN" Text="YEN (¥)" />
                                                        <asp:ListItem Value="RUB" Text="RUB (₽)" />
                                                        <asp:ListItem Value="INR" Text="INR (₹)" />
                                                        <asp:ListItem Value="XAF" Text="XAF (Fr)" />
                                                        <asp:ListItem Value="ISK" Text="ISK (kr)" />
                                                        <asp:ListItem Value="SEK" Text="SEK (kr)" />
                                                        <asp:ListItem Value="DKK" Text="DKK (kr)" />
                                                        <asp:ListItem Value="CHF" Text="CHF" />
												    </asp:DropDownList>
											    </div>
										    </div>
									    </div>
                                        <!-- /name -->

										
                                        <!-- rate -->
									    <div class="form-group">
										    <label class="control-label col-md-2"><%= Resources.Resource.RateFixed %></label>
										    <div class="col-md-5">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-balance"></i></span>
												    <input type="number" class="form-control" step="0.000001" placeholder='<%$ Resources:Resource, Rate_Tooltip %>' runat="server" id="tbxRate" min="0" max="999" />
                                                    
											    </div>
										    </div>
                                            <div class="col-md-5">
                                                <a href="javascript:void(0)" class="btn btn-default" id="btnAutoRate">
                                                    <i class="fa fa-refresh"></i> Taux FOREX
                                                </a>
                                            </div>
									    </div>
                                        <!-- /rate -->
                                        <script type="text/javascript">

                                            $(function() {

                                                $('#btnAutoRate').click(function () {

                                                    $('#btnAutoRate').prop("disabled",true);
                                                    var from = $('#ContentPlaceHolder1_tbxCurrencyFrom').val();
                                                    var to = $('#ContentPlaceHolder1_ddlCurrencyTo').val();

                                                    $.get( "Auto.ashx?from="+from+"&to="+to, function( data ) {
                                                        $('#ContentPlaceHolder1_tbxRate').val(data);
                                                        $('#btnAutoRate').prop("disabled",false);
                                                    });

                                                });

                                            });

                                        </script>
												


								    </fieldset>

											



                                    <!-- buttonsaction -->
								    <div class="buttonsaction text-right">
										<label class="control-label col-lg-2"></label>
										<div class="col-lg-10">
											<div class="btn-group">
                                                <asp:LinkButton runat="server" ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click">
                                                    <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                                </asp:LinkButton>
                                                
									            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore"><span class="caret"></span></button>
									            <ul class="dropdown-menu dropdown-menu-right" runat="server" id="btnMoreDdl">
                                                    <li><a href="javascript:void(0)" data-toggle="modal" data-target="#modal_default"><i class="fa fa-trash"></i> <%= Resources.Resource.Delete %></a></li>
												</ul>
											</div>
										</div>
									</div>
                                    <!-- /buttonsaction -->


							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->
					    


                        


			    </div>
			    <!-- /left column -->
                

                <!-- right column -->
                <div class="col-sm-12 col-md-6">

			    </div>
                <!-- /right column -->
						

		    </div> <!-- /row -->


					
            <!-- basic modal: confirm delete -->
		    <div id="modal_default" class="modal fade">
			    <div class="modal-dialog">
				    <div class="modal-content">
					    <div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal">&times;</button>
						    <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
					    </div>

					    <div class="modal-body">
						    <h6 class="text-semibold"><%= Resources.Resource.Are_You_Sure_To_Delete %></h6>
						    <p><%= Resources.Resource.Delete_Is_Irreversible %></p>

					    </div>

					    <div class="modal-footer">
						    <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
                            <asp:Button runat="server" ID="btnDelete" OnClick="btnDelete_Click" CssClass="btn btn-primary" Text="<%$ Resources:Resource, Yes_Delete %>" />

					    </div>
				    </div>
			    </div>
		    </div>
		    <!-- /basic modal -->



        </div><!-- /content -->

    </form>

</asp:Content>


