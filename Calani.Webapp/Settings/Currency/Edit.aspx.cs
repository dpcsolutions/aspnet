﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Settings_Currency_Edit : System.Web.UI.Page
{

    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ph.ItemsLabel = Resources.Resource.currencies_list;
        ph.ItemLabel = Resources.Resource.Currency;
        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
            btnDelete, btnMore, btnMoreDdl, lblTitle, linkBackList, linkNew);

        if (!IsPostBack && ph.CurrentId != null)
        {
            Load();
            ph.SetStateEdit(Resources.Resource.Edit_currency);
        }

        if (ph.CurrentId == null)
        {
            New();
            ph.SetStateNew(Resources.Resource.Add_new_currency);
        }
    }

    private void New()
    {
        Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(ph.CurrentOrganizationId);
        mgr.Load();
        tbxCurrencyFrom.Value = mgr.Currency;
        DisableToCurrenty(mgr.Currency);
    }

    public void SetCurrencyControl(string ccc)
    {
        string from = ccc.Substring(0, 3);
        string to = ccc.Substring(3);

        tbxCurrencyFrom.Value = from;
        ddlCurrencyTo.SelectedValue = to;

        DisableToCurrenty(from);
    }

    private void DisableToCurrenty(string to)
    {
        foreach (var item in ddlCurrencyTo.Items)
        {
            ListItem li = item as ListItem;
            if(li != null)
            {
                if (li.Value == to) li.Enabled = false;
            }
        }
    }

    public string GetCurrencyControl()
    {
        return tbxCurrencyFrom.Value + ddlCurrencyTo.SelectedValue;
    }


    public string GetSymbolControl()
    {
        string ret = ddlCurrencyTo.SelectedValue;
        ListItem li = ddlCurrencyTo.SelectedItem as ListItem;
        if(li != null)
        {
            if (li.Text.Contains("("))
            {
                ret = li.Text.Split('(')[1];
                ret = ret.Replace(")", "");
                ret = ret.Trim();
            }
        }
        return ret;
    }



    #region CRUD operations
    // Load from db and bind controls
    private void Load()
    {
        Calani.BusinessObjects.Currency.CurrenciesManager mgr = new Calani.BusinessObjects.Currency.CurrenciesManager(ph.CurrentOrganizationId);
        var tax = mgr.Get(ph.CurrentId.Value);

        SetCurrencyControl(tax.currency1);
        tbxRate.Value = Convert2.ToString(tax.rate);
    }

    // Create a new db record from controls values
    private void Update()
    {
        Calani.BusinessObjects.Currency.CurrenciesManager mgr = new Calani.BusinessObjects.Currency.CurrenciesManager(ph.CurrentOrganizationId);

        var ret = mgr.Update(ph.CurrentId.Value,
                        new Calani.BusinessObjects.Model.currency
                        {
                            currency1 = GetCurrencyControl(),
                            symbol = GetSymbolControl(),
                            rate = Convert2.ToDouble(tbxRate.Value, 0, 2, 0, 999),
                            organizationId = ph.CurrentOrganizationId
                        });
        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Updated_var);
            Load();
        }
        else ph.DisplayError(ret.ErrorMessage);
    }


    // Update existing db record from new controls values
    private void Create()
    {
        Calani.BusinessObjects.Currency.CurrenciesManager mgr = new Calani.BusinessObjects.Currency.CurrenciesManager(ph.CurrentOrganizationId);

        var ret = mgr.Add(new Calani.BusinessObjects.Model.currency
        {
            currency1 = GetCurrencyControl(),
            symbol = GetSymbolControl(),
            rate = Convert2.ToDouble(tbxRate.Value, 0, 8, 0, 999),
            organizationId = ph.CurrentOrganizationId,
            recordStatus = 0
        });
        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Created_var);
            btnSave.Enabled = false;
        }
        else ph.DisplayError(ret.ErrorMessage);
    }

    protected void Delete()
    {
        Calani.BusinessObjects.Currency.CurrenciesManager mgr = new Calani.BusinessObjects.Currency.CurrenciesManager(ph.CurrentOrganizationId);
        var ret = mgr.Delete(ph.CurrentId.Value);
        if (ret.Success)
        {
            panelEdit.Visible = false;
            ph.DisplaySuccess(Resources.Resource.Deleted_var);
        }
    }
    #endregion



    #region Buttons (triggers)
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) Update();
        else Create();
    }


    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) Delete();
    }
    #endregion





}