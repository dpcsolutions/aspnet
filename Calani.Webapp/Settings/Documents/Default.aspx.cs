﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.DocGenerator;

public partial class Settings_Documents_Default : System.Web.UI.Page
{
    public string JsonVariables { get; set; }

    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack) Load();

        var vars = ph.GetVariablesForTemplates();
        vars.Add(new JsVariableForTemplate { name = "doctype", value = Resources.Resource.Quote });
        vars.Add(new JsVariableForTemplate { name = "docnr", value = "123456" });
        vars.Add(new JsVariableForTemplate { name = "expdate", value = "12/12/2099" });
        vars.Add(new JsVariableForTemplate { name = "contacttitle", value = Resources.Resource.Title_Mr });
        vars.Add(new JsVariableForTemplate { name = "contactfn", value = "John" });
        vars.Add(new JsVariableForTemplate { name = "contactln", value = "Doe" });
        JsonVariables = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(vars);
    }

    private void Load()
    {

        tplQuoteFooter.Render("PdfQuoteFooter");
        tplInvoiceFooter.Render("PdfInvoiceFooter");
        tplDeliveryNoteFooter.Render("PdfDeliveryNoteFooter");
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        tplQuoteFooter.Save("PdfQuoteFooter");
        tplInvoiceFooter.Save("PdfInvoiceFooter");
        tplDeliveryNoteFooter.Save("PdfDeliveryNoteFooter");
        panelSuccess.Visible = true;
        Load();


    }
}