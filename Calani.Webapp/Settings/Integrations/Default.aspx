﻿<%@ Page Title="Integrations" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Settings_Integrations_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="content">

		    <div class="row">

                <!-- left column -->
			    <div class="col-sm-12 col-md-6">

                        <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Error %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblErrorMessage" />
						    </div>
					    </asp:Panel><!-- panelError -->

                        <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Done %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblSuccess" />.
						    </div>
					    </asp:Panel><!-- panelSuccess -->

					    
					    <asp:Panel runat="server" ID="panelEdit" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title">Winbiz Cloud</h6>
						    </div>


						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">


                                        <div class="form-group">
									        <label class="control-label col-lg-5">Company Name</label>													
									        <div class="col-lg-7">
										        <div class="input-group" runat="server">
											        <span class="input-group-addon"><i class="icon-bell3"></i></span>
											        <input type="text" step="1" runat="server" value="" class="form-control"  id="tbxWinbizCompanyName" />
										        </div>
									        </div>
								        </div>

                                       <div class="form-group">
									        <label class="control-label col-lg-5">User Name</label>													
									        <div class="col-lg-7">
										        <div class="input-group" runat="server">
											        <span class="input-group-addon"><i class="icon-user"></i></span>
											        <input type="text" step="1" runat="server" value="" class="form-control"  id="tbxWinbizUserName" />
										        </div>
									        </div>
								        </div>

                                        <div class="form-group">
									        <label class="control-label col-lg-5">User Password</label>													
									        <div class="col-lg-7">
										        <div class="input-group" runat="server">
											        <span class="input-group-addon"><i class="icon-key"></i></span>
											        <input type="text" step="1" runat="server" value="" class="form-control"  id="tbxWinbizPassword" />
										        </div>
									        </div>
								        </div>

                                        <div class="form-group">
									        <label class="control-label col-lg-5">API Key</label>													
									        <div class="col-lg-7">
										        <div class="input-group" runat="server">
											        <span class="input-group-addon"><i class="icon-link"></i></span>
											        <input type="text" step="1" runat="server" value="" class="form-control"  id="tbxWinbizApiKey" />
										        </div>
									        </div>
								        </div>
												


								    </fieldset>



                                    


							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->

                    </div>
                </div>
        </div>
    
</asp:Content>

