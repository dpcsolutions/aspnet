﻿<%@ Page Title="GlobalOptions" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Settings_Global_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<form runat="server">

        <div class="content">

		    <div class="row">

                <!-- left column -->
			    <div class="col-sm-12 col-md-6">

                        <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Error %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblErrorMessage" />
						    </div>
					    </asp:Panel><!-- panelError -->

                        <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Done %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblSuccess" />.
						    </div>
					    </asp:Panel><!-- panelSuccess -->

					    
					    <asp:Panel runat="server" ID="panelEdit" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.CalendarOptions %></h6>
						    </div>


						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">


                                        <div class="form-group" id="panelMainDiscount" runat="server">
									        <label class="control-label col-lg-5"><%= Resources.Resource.QuoteValidityPeriod %></label>													
									        <div class="col-lg-7">
										        <div class="input-group" runat="server">
											        <span class="input-group-addon"><i class="icon-bell3"></i></span>
											        <input type="number" step="1" runat="server" value="" class="form-control"  id="tbxQuoteValidity" min="3" max="730" />
                                                    <span class="input-group-addon"><%= Resources.Resource.Days %></span>
										        </div>
									        </div>
								        </div>

                                        <div class="form-group" id="Div1" runat="server">
									        <label class="control-label col-lg-5"><%= Resources.Resource.InvoiceValidityPeriod %></label>													
									        <div class="col-lg-7">
										        <div class="input-group" runat="server">
											        <span class="input-group-addon"><i class="icon-bell3"></i></span>
											        <input type="number" step="1" runat="server" value="" class="form-control"  id="tbxInvoiceValidity" min="3" max="730" />
                                                    <span class="input-group-addon"><%= Resources.Resource.Days %></span>
										        </div>
									        </div>
								        </div>

										<div class="form-group" id="Div14" runat="server">
									        <label class="control-label col-lg-5"><%= Resources.Resource.UnpaidInvoiceReminderPeriod %></label>													
									        <div class="col-lg-7">
										        <div class="input-group" runat="server">
											        <span class="input-group-addon"><i class="icon-bell3"></i></span>
											        <input type="number" step="1" runat="server" value="" class="form-control"  id="tbxUnpaidInvoiceReminderPeriod" min="1" max="730" />
                                                    <span class="input-group-addon"><%= Resources.Resource.Days %></span>
										        </div>
									        </div>
								        </div>

                                        <div class="form-group" id="Div2" runat="server"  data-popup="tooltip" data-placement="left" title='<%$ Resources:Resource, FinancialYearAnniversary_Tooltip %>'>
									        <label class="control-label col-lg-5"><%= Resources.Resource.FinancialYearAnniversary %></label>													
									        <div class="col-lg-7">
										        <div class="input-group" runat="server">
											        <span class="input-group-addon"><i class="icon-bell3"></i></span>
											        <input type="text" step="1" runat="server" value="" class="form-control daterange-single"  id="tbxFinancialYearStart"  />
										        </div>
									        </div>
								        </div>
										

								    </fieldset>


							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->



                    <asp:Panel runat="server" ID="panel1" class="panel panel-white">
						 <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.CurrencyOptions %></h6>
						    </div>


						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">

                                        <div class="form-group" id="Div3" runat="server">
									        <label class="control-label col-lg-5"><%= Resources.Resource.Currency %></label>													
									        <div class="col-lg-7">
										        <div class="input-group" runat="server">
											        <span class="input-group-addon"><i class="icon-coins"></i></span>
											        <asp:DropDownList runat="server" CssClass="form-control"  id="ddlCurrency">
                                                        <asp:ListItem Text="€" Value="€" />
                                                        <asp:ListItem Text="CHF" Value="CHF" />
                                                        <asp:ListItem Text="$" Value="$" />
                                                        <asp:ListItem Text="£" Value="£" />
											        </asp:DropDownList>
										        </div>
									        </div>
								        </div>
									    
								    </fieldset>
							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->
					
					
                    <asp:Panel runat="server" ID="panel5" class="panel panel-white">
                        <div class="panel-heading">
                            <h6 class="panel-title"><%= Resources.Resource.PdfOptions %></h6>
                        </div>


                        <div class="panel-body">

                            <div class="form-horizontal">

                                <fieldset class="content-group">


                                    <div class="form-group" id="Div7" runat="server">
                                        <label class="control-label col-lg-5"><%= Resources.Resource.ShowContactInfoInInvoicesAndQuotes %></label>													
                                        <div class="col-lg-7">
                                            <div class="input-group" runat="server">
                                                <label class="checkbox-inline">
                                                    <asp:CheckBox runat="server" ID="cbxShowContactInfoInInvoices" CssClass="checked control-primary" Checked="true" />
                                                </label>
                                            </div>
                                        </div>
                                    </div>
									<div class="form-group" id="Div13" runat="server">
                                        <label class="control-label col-lg-5"><%= Resources.Resource.PutArticlesInBold %></label>													
                                        <div class="col-lg-7">
                                            <div class="input-group" runat="server">
                                                <label class="checkbox-inline">
                                                    <asp:CheckBox runat="server" ID="cbxArticlesInBold" CssClass="checked control-primary" Checked="false" />                                                   
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
									    <label class="control-label col-lg-5"><%= Resources.Resource.Hide0DiscountColOnPdf %></label>													
									    <div class="col-lg-7">
										    <div class="input-group" runat="server">
											    <label class="checkbox-inline">
													<asp:CheckBox runat="server" ID="cbxHide0DiscountCol" CssClass="checked control-primary" />
											    </label>
										    </div>
									    </div>
								    </div>
									<div class="form-group">
									    <label class="control-label col-lg-5"><%= Resources.Resource.HideQuotesAcceptancePdf %></label>													
									    <div class="col-lg-7">
										    <div class="input-group" runat="server">
											    <label class="checkbox-inline">
													<asp:CheckBox runat="server" ID="cbxHideQuoteAcceptancePdf" CssClass="checked control-primary" />
											    </label>
										    </div>
									    </div>
								    </div>
									<div class="form-group">
									   <label class="control-label col-lg-5" title="<%= Resources.Resource.HideVatOnPdfInfo %>"><%= Resources.Resource.HideVatOnPdfIfSameUsed %></label>													
									    <div class="col-lg-7">
										    <div class="input-group" runat="server">
											    <label class="checkbox-inline">
													<asp:CheckBox runat="server" ID="cbxHideVatOnPdf" CssClass="checked control-primary" />
											    </label>
										    </div>
									    </div>
								    </div>
									<div class="form-group">
									   <label class="control-label col-lg-5" title="<%= Resources.Resource.HideCodeOnPdf %>"><%= Resources.Resource.HideCodeOnPdf %></label>													
									    <div class="col-lg-7">
										    <div class="input-group" runat="server">
											    <label class="checkbox-inline">
													<asp:CheckBox runat="server" ID="cbxHideCodeOnPdf" CssClass="checked control-primary" />
											    </label>
										    </div>
									    </div>
								    </div>
									<div class="form-group">
									    <label class="control-label col-lg-5"><%= Resources.Resource.ChangeTitleDeliveryNoteTo %></label>				
										<div class="col-lg-7">
										    <div class="input-group" runat="server">
											    <span class="input-group-addon"><i class="fa fa-file-text"></i></span>
											    <asp:TextBox runat="server" ID="tbxDeliveryNotePdfTitle" CssClass="form-control" MaxLength="200" />
										    </div>
									    </div>
								    </div>
									<div class="form-group">
									    <label class="control-label col-lg-5"><%= Resources.Resource.PdfOptionDeliveryNotes %></label>
										<div class="col-lg-7">
										        <div class="input-group" runat="server">
											        <span class="input-group-addon"><i class="fa fa-tag"></i></span>
											        <asp:DropDownList runat="server" ID="ddlDeliveryNotePdfType" CssClass="form-control" DataValueField="Id" DataTextField="Name" />
										        </div>
									        </div>
								    </div>
                                </fieldset>
                            </div>
                        </div>
                    </asp:Panel><!-- /panelEdit -->
					
                    <asp:Panel runat="server" ID="panel6" class="panel panel-white">
                        <div class="panel-heading">
                            <h6 class="panel-title"><%= Resources.Resource.Company_Settings %></h6>
                        </div>


                        <div class="panel-body">

                            <div class="form-horizontal">

                                <fieldset class="content-group">

                                    <div class="form-group" id="Div11" runat="server">
                                        <label class="control-label col-lg-5"><%= Resources.Resource.EmployeeCanViewAllEvents %></label>													
                                        <div class="col-lg-7">
                                            <div class="input-group" runat="server">
                                                <label class="checkbox-inline">
                                                    <asp:CheckBox runat="server" ID="cbxEmployeeCanViewAllEvents" CssClass="checked control-primary" Checked="False" />
                                                    
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- <div class="form-group" id="Div25" runat="server"> --%>
                                    <%--     <label class="control-label col-lg-5"><%= Resources.Resource.EmployeeCanViewAllSchedulers %></label>													 --%>
                                    <%--     <div class="col-lg-7"> --%>
                                    <%--         <div class="input-group" runat="server"> --%>
                                    <%--             <label class="checkbox-inline"> --%>
                                    <%--                 <asp:CheckBox runat="server" ID="cbxEmployeeCanViewAllSchedulers" CssClass="checked control-primary" Checked="False" /> --%>
                                    <%--             </label> --%>
                                    <%--         </div> --%>
                                    <%--     </div> --%>
                                    <%-- </div> --%>
                                    <div class="form-group" id="Div12" runat="server">
                                        <label class="control-label col-lg-5"><%= Resources.Resource.RoundAmount %></label>													
                                        <div class="col-lg-7">
                                            <div class="input-group" runat="server">
                                                <label class="checkbox-inline">
                                                    <asp:CheckBox runat="server" ID="cbxRoundAmount" CssClass="checked control-primary" Checked="False" />
                                                    
                                                </label>
                                            </div>
                                        </div>
                                    </div>

									<div class="form-group" id="Div16" runat="server">
                                        <label class="control-label col-lg-5"><%= Resources.Resource.DefaultPeriodFilter %></label>													
                                        <div class="col-lg-7">
                                            <div class="input-group" runat="server">
												<span class="input-group-addon"><i class="icon-calendar"></i></span>
                                                <asp:DropDownList runat="server" ID="ddlPeriodOptions"  CssClass="form-control"
													AppendDataBoundItems="True" DataTextField="Value" DataValueField="Key">
													<asp:ListItem Text='<%$ Resources:Resource, Choose %>' Value="0" />
												</asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>

									<div class="form-group">
									    <label class="control-label col-lg-5"><%= Resources.Resource.UseUnitColumn %></label>													
									    <div class="col-lg-7">
										    <div class="input-group" runat="server">
											    <label class="checkbox-inline">
													<asp:CheckBox runat="server" ID="cbxDisplayUnitColumnOnPdf" CssClass="checked control-primary" />
											    </label>
										    </div>
									    </div>
								    </div>

									<div class="form-group">
									    <label class="control-label col-lg-5"><%= Resources.Resource.HideBilledColumnForEmployees %></label>													
									    <div class="col-lg-7">
										    <div class="input-group" runat="server">
											    <label class="checkbox-inline">
													<asp:CheckBox runat="server" ID="cbxHideBilledColumnCustomers" CssClass="checked control-primary" />
											    </label>
										    </div>
									    </div>
								    </div>

									<div class="form-group">
									    <label class="control-label col-lg-5"><%= Resources.Resource.AddColumnOfTextInFrontOfItemsColumn %></label>													
									    <div class="col-lg-7">
										    <div class="input-group" runat="server">
											    <label class="checkbox-inline">
													<asp:CheckBox runat="server" ID="cbxDisplayServiceCustomNumber" CssClass="checked control-primary" />
											    </label>
										    </div>
									    </div>
								    </div>

									<div class="form-group">
									    <label class="control-label col-lg-5"><%= Resources.Resource.UsePriceIncludingVat %></label>													
									    <div class="col-lg-7">
										    <div class="input-group" runat="server">
											    <label class="checkbox-inline">
													<asp:CheckBox runat="server" ID="cbxUsePriceIncludingVat" CssClass="checked control-primary" />
											    </label>
										    </div>
									    </div>
								    </div>
	                                
                                </fieldset>
                            </div>
                        </div>
                    </asp:Panel><!-- /panelEdit -->

					
                    
			    </div>
			    <!-- /left column -->
                
                <!-- right column -->
			    <div class="col-sm-12 col-md-6">
                    <asp:Panel runat="server" ID="panel3" class="panel panel-white" Visible="false">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.QuoteOrDeliveryNote %></h6>
						    </div>


						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">


                                        <div class="form-group" id="Div5" runat="server">
									        <label class="control-label col-lg-5"><%= Resources.Resource.QuoteOrDeliveryNoteDescription %></label>													
									        <div class="col-lg-7">
										        <div class="input-group" runat="server">
											        <span class="input-group-addon"><i class="icon-upload"></i></span>
											        <asp:DropDownList runat="server" CssClass="form-control"  id="ddlQuoteOrDelivery">
                                                        <asp:ListItem Text='<%$ Resources:Resource, Quote %>' Value="Quote" />
                                                        <asp:ListItem Text='<%$ Resources:Resource, DeliveryNote %>' Value="DeliveryNote" />
                                                        
											        </asp:DropDownList>
										        </div>
									        </div>
								        </div>
										
											

								    </fieldset>

                                


							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->


                    <asp:Panel runat="server" ID="panel2" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.EmailAlerts %></h6>
						    </div>


						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">


                                        <div class="form-group" id="Div4" runat="server">
									        <label class="control-label col-lg-5"><%= Resources.Resource.SendLateInvoicesList %></label>													
									        <div class="col-lg-7">
										        <div class="input-group" runat="server">
											        <span class="input-group-addon"><i class="icon-calendar"></i></span>
											        <asp:DropDownList runat="server" CssClass="form-control"  id="ddlEmailAlertLateInvoicesDay">
                                                        <asp:ListItem Text='<%$ Resources:Resource, Never %>' Value="-1" />
                                                        <asp:ListItem Text='<%$ Resources:Resource, Monday %>' Value="1" />
                                                        <asp:ListItem Text='<%$ Resources:Resource, Tuesday %>' Value="2" />
                                                        <asp:ListItem Text='<%$ Resources:Resource, Wednesday %>' Value="3" />
                                                        <asp:ListItem Text='<%$ Resources:Resource, Thursday %>' Value="4" />
                                                        <asp:ListItem Text='<%$ Resources:Resource, Friday %>' Value="5" />
                                                        <asp:ListItem Text='<%$ Resources:Resource, Saturday %>' Value="6" />
                                                        <asp:ListItem Text='<%$ Resources:Resource, Sunday %>' Value="0" />
                                                        
											        </asp:DropDownList>
										        </div>
									        </div>
								        </div>
										
											

								    </fieldset>

                                


							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->
					
					  <asp:Panel runat="server" ID="panel4" class="panel panel-white">
                          <div class="panel-heading">
                              <h6 class="panel-title"><%= Resources.Resource.Time %></h6>
                          </div>
						    <div class="panel-body">
							    <div class="form-horizontal">
								    <fieldset class="content-group">
										<div class="form-group" id="Div8" runat="server">
                                            <label class="control-label col-lg-5"><%= Resources.Resource.StandardVacation %></label>	
                                            <div class="col-lg-7">
                                                <div class="input-group" runat="server">
                                                    <span class="input-group-addon"><i class="icon-sun3"></i></span>
                                                    <input type="number" step="1" runat="server" value="" class="form-control"  id="tbxVacation" min="0" max="100" />
                                                    <span class="input-group-addon"><%= Resources.Resource.Days %></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="Div6" runat="server">
                                            <label class="control-label col-lg-5"><%= Resources.Resource.WorkingHoursPerWeek %></label>	
                                            <div class="col-lg-7">
                                                <div class="input-group" runat="server">
                                                    <span class="input-group-addon"><i class="icon-bell3"></i></span>
                                                    <input type="number" step="any" runat="server" value="" class="form-control"  id="tbxWorkPerWeekHours" min="3" max="730" />
                                                    <span class="input-group-addon"><%= Resources.Resource.Hours %></span>
                                                </div>
                                            </div>
                                        </div>

										<div class="form-group" id="Div15" runat="server">
                                            <label class="control-label col-lg-5"><%= Resources.Resource.AbsenceDayDuration %></label>	
                                            <div class="col-lg-7">
                                                <div class="input-group" runat="server">
                                                    <span class="input-group-addon"><i class="icon-bell3"></i></span>
                                                    <input type="number" step="any" runat="server" value="" class="form-control"  id="tbxAbsenceDayHours" min="1" max="24" />
                                                    <span class="input-group-addon"><%= Resources.Resource.Hours %></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="DefaultWorkSchedule" runat="server">
                                            <label class="control-label col-lg-5"><%= Resources.Resource.DefaultWorkSchedule %></label>	
									        <div class="col-lg-7">
										        <div class="input-group" runat="server">
											        <span class="input-group-addon"><i class="icon-calendar"></i></span>
											        <asp:DropDownList runat="server" CssClass="form-control"  ID="ddlWorkSchedule"  AppendDataBoundItems="True" 
                                                                      DataTextField="Name" DataValueField="Id" >
                                                        <asp:ListItem Text='<%$ Resources:Resource, Choose %>' Value="0" />
                                                        
											        </asp:DropDownList>
										        </div>
									        </div>
								        </div>
										
                                        <div class="form-group" id="Div9" runat="server">
                                            <label class="control-label col-lg-5"><%= Resources.Resource.LunchDuration %></label>	
                                            <div class="col-lg-7">
                                                <div class="input-group" runat="server">
                                                    <span class="input-group-addon"><i class="icon-coffee"></i></span>
                                                    <input type="number" step="1" runat="server" value="" class="form-control"  id="tbxLunchDuration" min="0" max="180" />
                                                    <span class="input-group-addon"><%= Resources.Resource.Minutes %></span>
                                                </div>
                                            </div>
                                        </div>

										<div class="form-group" id="Div18" runat="server">
                                            <label class="control-label col-lg-5"><%= Resources.Resource.DailyBreakDuration %></label>	
                                            <div class="col-lg-7">
                                                <div class="input-group" runat="server">
                                                    <span class="input-group-addon"><i class="icon-coffee"></i></span>
                                                    <input type="number" step="1" runat="server" value="" class="form-control"  id="tbxDailyBreakDuration" min="0" max="180" />
                                                    <span class="input-group-addon"><%= Resources.Resource.Minutes %></span>
                                                </div>
                                            </div>
                                        </div>

										<div class="form-group" id="TimeSheetType" runat="server">
									        <label class="control-label col-lg-5"><%= Resources.Resource.TimeSheetType %></label>													
									        <div class="col-lg-7">
										        <div class="input-group" runat="server">
											        <span class="input-group-addon"><i class="icon-spinner9"></i></span>
											        <asp:DropDownList runat="server" CssClass="form-control"  id="ddlTimeSheetType">
											        </asp:DropDownList>
										        </div>
									        </div>
								        </div>
										<div class="form-group" id="LinesPerDayInTimesheet" runat="server">
									        <label class="control-label col-lg-5"><%= Resources.Resource.LinesPerDayInTimesheet %></label>													
									        <div class="col-lg-7">
										        <div class="input-group" runat="server">
											        <span class="input-group-addon"><i class="icon-spinner9"></i></span>
											        <asp:DropDownList runat="server" CssClass="form-control"  id="ddlLinesPerDayInTimesheet">
                                                        <asp:ListItem Text='1' Value="1" />
                                                        <asp:ListItem Text='2' Value="2" />
														<asp:ListItem Text='3' Value="3" />
														<asp:ListItem Text='4' Value="4" />
														<asp:ListItem Text='5' Value="5" />
											        </asp:DropDownList>
										        </div>
									        </div>
								        </div>

										<div class="form-group" id="UseWorkSchedule" runat="server">
									        <label class="control-label col-lg-5"><%= Resources.Resource.UseWorkSchedule %></label>													
									        <div class="col-lg-7">
										        <div class="input-group" runat="server">
											        <label class="checkbox-inline">
														<asp:CheckBox runat="server" ID="cbxUseWorkSchedule" CssClass="checked control-primary" Checked="true" />
														
											        </label>
										        </div>
									        </div>
								        </div>

										<div class="form-group">
									        <label class="control-label col-lg-5"><%= Resources.Resource.SendEmailToValidateTimesheet %></label>													
									        <div class="col-lg-7">
										        <div class="input-group" runat="server">
											        <span class="input-group-addon"><i class="icon-spinner9"></i></span>
											        <asp:DropDownList runat="server" CssClass="form-control"  id="ddlTimesheetEmailSetting" DataValueField="Id" DataTextField="Name">
											        </asp:DropDownList>
										        </div>
									        </div>
								        </div>

								    </fieldset>

                                


							    </div>
						    </div>
					    </asp:Panel>


					<asp:Panel runat="server" ID="panel7" class="panel panel-white">
                        <div class="panel-heading">
                            <h6 class="panel-title"><%= Resources.Resource.ProjectSettings %></h6>
                        </div>
						<div class="panel-body">
							<div class="form-horizontal">
								<fieldset class="content-group">

									<div class="form-group" id="Div10" runat="server">
                                        <label class="control-label col-lg-5"><%= Resources.Resource.EmployeeCanViewAllOpenProjects %></label>													
                                        <div class="col-lg-7">
                                            <div class="input-group" runat="server">
                                                <label class="checkbox-inline">
                                                    <asp:CheckBox runat="server" ID="cbxEmployeeCanViewAllOpenProjects" CssClass="checked control-primary" Checked="True" />
                                                    
                                                </label>
                                            </div>
                                        </div>
                                    </div>

									<div class="form-group" id="Div24" runat="server">
									    <label class="control-label col-lg-5"><%= Resources.Resource.EnableRefNumInheritance %></label>													
									    <div class="col-lg-7">
										    <div class="input-group" runat="server">
											    <label class="checkbox-inline">
													<asp:CheckBox runat="server" ID="cbxEnableRefNumInheritance" CssClass="checked control-primary" />
											    </label>
										    </div>
									    </div>
								    </div>

									<div class="form-group" id="Div17" runat="server">
									    <label class="control-label col-lg-5"><%= Resources.Resource.HideBillingElementsProject %></label>													
									    <div class="col-lg-7">
										    <div class="input-group" runat="server">
											    <label class="checkbox-inline">
													<asp:CheckBox runat="server" ID="cbxHideBillingElements" CssClass="checked control-primary" />
											    </label>
										    </div>
									    </div>
								    </div>
	                                
									<div class="form-group" id="Div20" runat="server">
									    <label class="control-label col-lg-5"><%= Resources.Resource.EmployeeCanCreateProjects %></label>													
									    <div class="col-lg-7">
										    <div class="input-group" runat="server">
											    <label class="checkbox-inline">
													<asp:CheckBox runat="server" ID="cbxEmployeeCanCreateProjects" CssClass="checked control-primary" />
											    </label>
										    </div>
									    </div>
								    </div>
	                                
									<div class="form-group" id="Div21" runat="server">
									    <label class="control-label col-lg-5"><%= Resources.Resource.DisplayTotalQuantityForProject %></label>													
									    <div class="col-lg-7">
										    <div class="input-group" runat="server">
											    <label class="checkbox-inline">
													<asp:CheckBox runat="server" ID="cbxDisplayTotalQuantity" CssClass="checked control-primary" />
											    </label>
										    </div>
									    </div>
								    </div>
	                                
									<div class="form-group" id="Div23" runat="server">
									    <label class="control-label col-lg-5"><%= Resources.Resource.ProhibitInvoicedDelivery %></label>													
									    <div class="col-lg-7">
										    <div class="input-group" runat="server">
											    <label class="checkbox-inline">
													<asp:CheckBox runat="server" ID="cbxDisableInvoicedOnly" CssClass="checked control-primary" />
											    </label>
										    </div>
									    </div>
								    </div>

								</fieldset>
							</div>
						</div>
					</asp:Panel>
                </div>
                <!-- /right column -->

		    </div> <!-- /row -->

            <div class="row">

                <div class="col-sm-12 col-md-12">
                    <asp:Panel runat="server" ID="panelAction" class="panel panel-white">

						            <div class="panel-body">

                                        <!-- buttonsaction -->
								        <div class="buttonsaction text-right">
										    <label class="control-label col-lg-2"></label>
										    <div class="col-lg-10">
											    <div class="btn-group">
                                                    <asp:LinkButton runat="server" ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click">
                                                        <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                                    </asp:LinkButton>
                                                

											    </div>
										    </div>
									    </div>
                                        <!-- /buttonsaction -->

                                     </div>

                        </asp:Panel>
                    </div>

            </div>


        </div><!-- /content -->

    </form>

</asp:Content>

