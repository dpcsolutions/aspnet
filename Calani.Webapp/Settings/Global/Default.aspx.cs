﻿using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.FrontEndModels;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.UI.WebControls;

public partial class Settings_Global_Default : System.Web.UI.Page
{
    public PageHelper ph;
    private Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ph.ItemLabel = Resources.Resource.CalendarOptions;
        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
           null, null, null, null, null, null);
        mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(ph.CurrentOrganizationId);

        if (!IsPostBack)
        {
            Load();
            ph.SetStateEdit(Resources.Resource.Edit_var_id);

            if(!mgr.UseWorkSchedule)
            {
                DefaultWorkSchedule.Visible = false;
            }
            else
            {
                var scheduleMgr = new Calani.BusinessObjects.WorkSchedulers.WorkScheduleManager(ph.CurrentOrganizationId);

                var schedulersList = scheduleMgr.GetScheduleDtos();
                ddlWorkSchedule.DataSource = schedulersList;
                ddlWorkSchedule.DataBind();
            }

            if (mgr.AssignedWorkScheduleId.HasValue)
            {
                var ddlItem = ddlWorkSchedule.Items.FindByValue(mgr.AssignedWorkScheduleId.ToString());
                if (ddlItem != null)
                    ddlItem.Selected = true;
            }

            ddlTimesheetEmailSetting.DataSource = new List<UnitModel>()
            {
                new UnitModel { Id = (long)TimesheetEmailOptionsEnum.AlwaysNotifyOnValidation, Name = Resources.Resource.SendEmailForTimesheetValidationAndModification },
                new UnitModel { Id = (long)TimesheetEmailOptionsEnum.NotifyOnValidationOnlyIfChanged, Name = Resources.Resource.SendEmailOnlyForValidationWithModification },
            };
            ddlTimesheetEmailSetting.DataBind();
            var timesheetEmailOption = ddlTimesheetEmailSetting.Items.FindByValue(mgr.TimesheetEmailOption.ToString());
            if (timesheetEmailOption != null)
            {
                timesheetEmailOption.Selected = true;
            }

            ddlPeriodOptions.DataSource = GetPeriodFilterOptions();
            ddlPeriodOptions.DataBind();

            ddlDeliveryNotePdfType.DataSource = new List<UnitModel>()
            {
                new UnitModel { Id = (long)TypeOfDeliveryNotePdfEnum.Unpriced, Name = ResourceHelper.Get(TypeOfDeliveryNotePdfEnum.Unpriced.GetDescription()) },
                new UnitModel { Id = (long)TypeOfDeliveryNotePdfEnum.Priced, Name = ResourceHelper.Get(TypeOfDeliveryNotePdfEnum.Priced.GetDescription()) },
            };
            ddlDeliveryNotePdfType.DataBind();
        }

        Title = ph.CMP.Name;

    }



    #region CRUD operations
    // Load from db and bind controls
    private void Load()
    {


        
        mgr.Load();


        tbxFinancialYearStart.Value = Convert2.DateToStrForInput(mgr.FinancialYearStart, "picker", DateTime.Now.Year);
        tbxInvoiceValidity.Value = mgr.DelayInvoice.ToString();
        tbxQuoteValidity.Value = mgr.DelayQuote.ToString();
        tbxUnpaidInvoiceReminderPeriod.Value = mgr.UnpaidInvoiceReminderDays.ToString();
        ddlCurrency.SelectedValue = mgr.Currency;
        ddlEmailAlertLateInvoicesDay.SelectedValue = mgr.EmailAlertLateInvoicesDay.ToString();
        tbxWorkPerWeekHours.Value = mgr.WeekWorkHours.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture);
        tbxLunchDuration.Value = mgr.LunchDuration.ToString();
        tbxDailyBreakDuration.Value = mgr.DailyBreakDuration.ToString();
        tbxAbsenceDayHours.Value = mgr.AbsenceDayDurationHours.ToString(System.Globalization.CultureInfo.InvariantCulture);

        tbxVacation.Value = mgr.Vacation.ToString();

        var timesheetTypes = Enum.GetValues(typeof(TimesheetTypeEnum));
        ddlTimeSheetType.DataSource = ((TimesheetTypeEnum[])timesheetTypes)
            .Select(type => new KeyValuePair<int, string>((int)type, Resources.Resource.ResourceManager.GetString(((TimesheetTypeEnum)type).GetDescription())))
            .ToList(); ;
        ddlTimeSheetType.DataTextField = "Value";
        ddlTimeSheetType.DataValueField = "Key";
        ddlTimeSheetType.DataBind();

        ddlTimeSheetType.SelectedValue = mgr.TimeSheetType.ToString();
        ddlLinesPerDayInTimesheet.SelectedValue = mgr.TimeSheetLinesPerDay.ToString();
        
        cbxUseWorkSchedule.Checked = mgr.UseWorkSchedule;

        cbxShowContactInfoInInvoices.Checked = mgr.ShowContactInfoInInvoices;
        cbxArticlesInBold.Checked = mgr.PutArticlesInBoldInPdf;
        cbxEmployeeCanViewAllOpenProjects.Checked = mgr.EmployeeCanViewAllOpenProjects;
        cbxEmployeeCanViewAllEvents.Checked = mgr.EmployeeCanViewAllEvents;
        // cbxEmployeeCanViewAllSchedulers.Checked = mgr.EmployeeCanViewAllSchedulers;
        cbxEmployeeCanCreateProjects.Checked = mgr.EmployeeCanCreateProjects;
        cbxDisplayTotalQuantity.Checked = mgr.DisplayTotalQuantity;
        cbxDisableInvoicedOnly.Checked = mgr.DisableInvoicedOnly;
        cbxRoundAmount.Checked = mgr.RoundAmount;
        cbxEnableRefNumInheritance.Checked = mgr.EnableRefNumberInheritance;
        cbxHideBillingElements.Checked = mgr.HideBillingElementsOnProject;
        cbxHideBilledColumnCustomers.Checked = mgr.HideBillingColumnOnCustomers;
        cbxHide0DiscountCol.Checked = mgr.Hide0DiscountColOnPdf;
        cbxDisplayUnitColumnOnPdf.Checked = !mgr.HideUnitColumn;
        cbxHideQuoteAcceptancePdf.Checked = mgr.HideQuoteAcceptancePdf;
        cbxHideVatOnPdf.Checked = mgr.HideVatPdf;
        cbxHideCodeOnPdf.Checked = mgr.HideCodeOnPdf;
        cbxDisplayServiceCustomNumber.Checked = mgr.DisplayServiceCustomNumber;
        cbxUsePriceIncludingVat.Checked = mgr.UsePriceIncludingVat;

        if (mgr.BeforeInvoice == "DeliveryNote")
        {
            ddlQuoteOrDelivery.SelectedIndex = 1;
        }

        if(mgr.DefaultPeriodFilter != PeriodFilterOptionsEnum.None)
        {
            ddlPeriodOptions.SelectedValue = ((byte)mgr.DefaultPeriodFilter).ToString();
        }

        ddlDeliveryNotePdfType.SelectedValue = ((sbyte)mgr.DeliveryNotePdfType).ToString();
        tbxDeliveryNotePdfTitle.Text = mgr.DeliveryNotePdfTitle;
    }

    // Create a new db record from controls values
    private void Update()
    {
        var mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(ph.CurrentOrganizationId);
        try
        {
            mgr.FinancialYearStart = Convert2.ToDate(tbxFinancialYearStart.Value, "picker").Value;
        }
        catch { }

        try
        {
            mgr.DelayInvoice = Convert.ToInt32(tbxInvoiceValidity.Value);
            if (mgr.DelayInvoice < 3) 
                mgr.DelayInvoice = 3;

            if (mgr.DelayInvoice > 730) 
                mgr.DelayInvoice = 730;
        }
        catch { }

        try
        {
            mgr.DelayQuote = Convert.ToInt32(tbxQuoteValidity.Value);
            if (mgr.DelayQuote < 3)
                mgr.DelayQuote = 3;

            if (mgr.DelayQuote > 730) 
                mgr.DelayQuote = 730;

        }
        catch { }

        try
        {
            mgr.UnpaidInvoiceReminderDays = Convert.ToInt32(tbxUnpaidInvoiceReminderPeriod.Value);
            if (mgr.UnpaidInvoiceReminderDays < 0)
                mgr.UnpaidInvoiceReminderDays = 0;

            if (mgr.DelayQuote > 365)
                mgr.DelayQuote = 365;

        }
        catch { }

        mgr.Currency = ddlCurrency.SelectedValue;

        mgr.TimeSheetType = Convert.ToInt32(ddlTimeSheetType.SelectedValue);

        mgr.TimeSheetLinesPerDay = Convert.ToInt32(ddlLinesPerDayInTimesheet.SelectedValue);
        mgr.UseWorkSchedule = cbxUseWorkSchedule.Checked;
        mgr.ShowContactInfoInInvoices = cbxShowContactInfoInInvoices.Checked;
        mgr.PutArticlesInBoldInPdf = cbxArticlesInBold.Checked;

        mgr.EmployeeCanViewAllOpenProjects = cbxEmployeeCanViewAllOpenProjects.Checked;
        mgr.EmployeeCanViewAllEvents = cbxEmployeeCanViewAllEvents.Checked;
        // mgr.EmployeeCanViewAllSchedulers = cbxEmployeeCanViewAllSchedulers.Checked;
        mgr.EmployeeCanCreateProjects = cbxEmployeeCanCreateProjects.Checked;
        mgr.DisplayTotalQuantity = cbxDisplayTotalQuantity.Checked;
        mgr.DisableInvoicedOnly = cbxDisableInvoicedOnly.Checked;
        mgr.RoundAmount = cbxRoundAmount.Checked;

        var vacation = 20;
        mgr.Vacation = Convert2.StrToInt(tbxVacation.Value, vacation);

        double weekWorkHours = 40.0;
        mgr.WeekWorkHours = Convert2.ToDouble(tbxWorkPerWeekHours.Value, weekWorkHours,2);
        mgr.AbsenceDayDurationHours = Convert2.ToDouble(tbxAbsenceDayHours.Value, 8, 2);

        var lunchDuration = 0;
        mgr.LunchDuration = Convert2.StrToInt(tbxLunchDuration.Value, lunchDuration);

        var dailyBreakDuration = 0;
        mgr.DailyBreakDuration = Convert2.StrToInt(tbxDailyBreakDuration.Value, dailyBreakDuration);

        long selSchedlureId = 0;
        selSchedlureId = Convert2.StrToLong(ddlWorkSchedule.SelectedValue, selSchedlureId);
        if (selSchedlureId > 0)
            mgr.AssignedWorkScheduleId = selSchedlureId;

        int timesheetEmailOption = 0;
         mgr.TimesheetEmailOption = Convert2.StrToInt(ddlTimesheetEmailSetting.SelectedValue, timesheetEmailOption);

        int emailAlertLateInvoicesDay = 1;
        Int32.TryParse(ddlEmailAlertLateInvoicesDay.SelectedValue, out emailAlertLateInvoicesDay);
        mgr.EmailAlertLateInvoicesDay = emailAlertLateInvoicesDay;

        byte defaultPeriodOption = 0;
        byte.TryParse(ddlPeriodOptions.SelectedValue, out defaultPeriodOption);
        if(defaultPeriodOption >= 0)
        {
            mgr.DefaultPeriodFilter = (PeriodFilterOptionsEnum)defaultPeriodOption;
        }

        mgr.EnableRefNumberInheritance = cbxEnableRefNumInheritance.Checked;
        mgr.HideBillingElementsOnProject = cbxHideBillingElements.Checked;
        mgr.HideBillingColumnOnCustomers = cbxHideBilledColumnCustomers.Checked;
        mgr.Hide0DiscountColOnPdf = cbxHide0DiscountCol.Checked;
        mgr.HideUnitColumn = !cbxDisplayUnitColumnOnPdf.Checked;
        mgr.HideQuoteAcceptancePdf = cbxHideQuoteAcceptancePdf.Checked;
        mgr.HideVatPdf = cbxHideVatOnPdf.Checked;
        mgr.HideCodeOnPdf = cbxHideCodeOnPdf.Checked;
        mgr.DeliveryNotePdfTitle = !string.IsNullOrWhiteSpace(tbxDeliveryNotePdfTitle.Text) ? tbxDeliveryNotePdfTitle.Text : null;
        mgr.DisplayServiceCustomNumber = cbxDisplayServiceCustomNumber.Checked;
        mgr.UsePriceIncludingVat = cbxUsePriceIncludingVat.Checked;

        byte deliveryNotePdfType = 0;
        byte.TryParse(ddlDeliveryNotePdfType.SelectedValue, out deliveryNotePdfType);
        if (deliveryNotePdfType >= 0)
        {
            mgr.DeliveryNotePdfType = (TypeOfDeliveryNotePdfEnum)deliveryNotePdfType;
        }

        if (ddlQuoteOrDelivery.SelectedIndex == 1)
            mgr.BeforeInvoice = "DeliveryNote";

        var ret = mgr.Save(false, false, true, false);
        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.OptionsUpdated);
            Load();
        }
        else ph.DisplayError(ret.ErrorMessage);
    }



    #endregion



    #region Buttons (triggers)
    protected void btnSave_Click(object sender, EventArgs e)
    {
        Update();
    }


    #endregion

    IDictionary<byte, string> GetPeriodFilterOptions()
    {
        var res = new Dictionary<byte, string>();

        var options = Enum.GetValues(typeof(PeriodFilterOptionsEnum));
        foreach(PeriodFilterOptionsEnum o in options)
        {
            var description = o.GetDescription();
            if (string.IsNullOrEmpty(description)) continue;
            res.Add((byte)o, ResourceHelper.Get(description));
        }

        return res;
    }

}