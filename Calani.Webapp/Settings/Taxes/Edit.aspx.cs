﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Settings_Taxes_Edit : System.Web.UI.Page
{

    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ph.ItemLabel = Resources.Resource.Tax;
        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
            btnDelete, btnMore, btnMoreDdl, lblTitle, linkBackList, linkNew);

        if (!IsPostBack && ph.CurrentId != null)
        {
            Load();
            ph.SetStateEdit(Resources.Resource.Edit_tax);
        }

        if (ph.CurrentId == null)
        {
            ph.SetStateNew(Resources.Resource.Add_new_tax);
        }
    }



    #region CRUD operations
    // Load from db and bind controls
    private void Load()
    {
        Calani.BusinessObjects.CustomerAdmin.TaxesManager mgr = new Calani.BusinessObjects.CustomerAdmin.TaxesManager(ph.CurrentOrganizationId);
        var tax = mgr.Get(ph.CurrentId.Value);

        tbxName.Value = tax.name;
        tbxRate.Value = Convert2.ToString(tax.rate);
    }

    // Create a new db record from controls values
    private void Update()
    {
        Calani.BusinessObjects.CustomerAdmin.TaxesManager mgr = new Calani.BusinessObjects.CustomerAdmin.TaxesManager(ph.CurrentOrganizationId);

        var ret = mgr.Update(ph.CurrentId.Value,
                        new Calani.BusinessObjects.Model.taxes
                        {
                            name = tbxName.Value,
                            rate = Convert2.ToDouble(tbxRate.Value, 0, 2, 0, 999),
                            organizationId = ph.CurrentOrganizationId
                        });
        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Updated_var);
            Load();
        }
        else ph.DisplayError(ret.ErrorMessage);
    }


    // Update existing db record from new controls values
    private void Create()
    {
        Calani.BusinessObjects.CustomerAdmin.TaxesManager mgr = new Calani.BusinessObjects.CustomerAdmin.TaxesManager(ph.CurrentOrganizationId);

        var ret = mgr.Add(new Calani.BusinessObjects.Model.taxes
        {
            name = tbxName.Value,
            rate = Convert2.ToDouble(tbxRate.Value, 0, 2, 0, 999),
            organizationId = ph.CurrentOrganizationId
        });
        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Created_var);
            btnSave.Enabled = false;
        }
        else ph.DisplayError(ret.ErrorMessage);
    }

    protected void Delete()
    {
        Calani.BusinessObjects.CustomerAdmin.TaxesManager mgr = new Calani.BusinessObjects.CustomerAdmin.TaxesManager(ph.CurrentOrganizationId);
        var ret = mgr.Delete(ph.CurrentId.Value);
        if (ret.Success)
        {
            panelEdit.Visible = false;
            ph.DisplaySuccess(Resources.Resource.Deleted_var);
        }
    }
    #endregion



    #region Buttons (triggers)
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) Update();
        else Create();
    }


    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) Delete();
    }
    #endregion





}