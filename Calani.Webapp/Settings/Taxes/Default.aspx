﻿<%@ Page Title="Taxes" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Settings_Taxes_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">


        <div class="content">
					<div>

                        <div class="panel panel-white">
						    <div class="panel-body" style="padding-bottom:5px">
								    <div class="form-horizontal" action="#">
									    <fieldset class="content-group">
										    <div class="form-group" style="margin-bottom:0">
											    <label class="control-label col-lg-5"><%=Resources.Resource.Default_VAT %></label>
											    <div class="col-lg-5">
												    <div class="input-group">
													    <span class="input-group-addon"><i class="fa fa-angle-right"></i></span>
													    <asp:DropDownList runat="server" ID="ddlDefaultTax" CssClass="select form-control"
                                                         DataValueField="id" DataTextField="name" />	
												    </div>
											    </div>
                                                <div class="col-lg-2">
                                                    <asp:LinkButton runat="server" ID="btnChanceDefault" class="btn btn-primary" OnClick="btnChanceDefault_Click">
                                                        <i class="icon-floppy-disk position-left"></i> <%=Resources.Resource.Update %>
                                                    </asp:LinkButton>
                                                </div>
										    </div>
									    </fieldset>
								    </div>
						    </div>
					    </div>

						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
										<strong><%= ph.CMP.Name %></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

							<div class="panel-body">
                                <asp:GridView runat="server" ID="grid" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:HyperLinkField DataTextField="name" HeaderText="Name" DataNavigateUrlFields="id" DataNavigateUrlFormatString="Edit.aspx?id={0}"/>
                                        <asp:BoundField DataField="rate" HeaderText="Rate" DataFormatString="{0:F2} %" HtmlEncode="false" ItemStyle-CssClass="text-right"  HeaderStyle-CssClass="text-right" />
                                        <asp:BoundField DataField="lastChange" HeaderText="Last_Rate_Change" ItemStyle-CssClass="text-right"  HeaderStyle-CssClass="text-right" />
                                    </Columns>
                                </asp:GridView>
                            </div><!-- panel-body -->

                            <div class="panel-footer text-right no-padding">
						    	<div class="row" style="margin:10px">
						    		<a href="Edit.aspx" class="btn btn-default"><i class="icon-plus2 position-left"></i> 
                                        <%=Resources.Resource.Add_new_tax %>
                                        
						    		</a>
						    	</div>
							</div><!-- panel-footer -->

                        </div>


                        
                    </div>
        </div><!-- /content -->

    </form>
</asp:Content>

