﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Settings_Taxes_Default : System.Web.UI.Page
{

    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        
        
        Calani.BusinessObjects.CustomerAdmin.TaxesManager mgr = new Calani.BusinessObjects.CustomerAdmin.TaxesManager(ph.CurrentOrganizationId);
        var list = mgr.List();

        //grid:
        grid.DataSource = list;
        grid.DataBind();
        new Html5GridView(grid);

        //default dropdown:
        
        if (!IsPostBack)
        {
            ddlDefaultTax.DataSource = list;
            var id = (from r in list where r.isDefault == true select r.id).FirstOrDefault();
            if (id > 0) ddlDefaultTax.SelectedValue = id.ToString();
            ddlDefaultTax.DataBind();
        }
        

    }

    protected void btnChanceDefault_Click(object sender, EventArgs e)
    {
        Calani.BusinessObjects.CustomerAdmin.TaxesManager mgr = new Calani.BusinessObjects.CustomerAdmin.TaxesManager(ph.CurrentOrganizationId);
        long? l = Convert2.ToNullableLong(ddlDefaultTax.SelectedValue);
        if(l != null) mgr.SetDefautTax(l.Value);
        
    }
}