﻿<%@ WebHandler Language="C#" Class="TestBvr" %>

using System;
using System.Web;

public class TestBvr : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {


        long orgid = PageHelper.GetCurrentOrganizationId(context.Session);
        Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(orgid);
        mgr.Load();

        if(mgr.BvrMode == 0)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write("Les BVR ne sont pas activés. BVR are not enabled.");
        }
        else
        {
            Calani.BusinessObjects.DocGenerator.BvrPdfGenerator bvr = new Calani.BusinessObjects.DocGenerator.BvrPdfGenerator();
            bvr.Init(mgr, context.Server.MapPath("~/App_Data/bvr"));
            bvr.Amount = 0.00;
            bvr.FromLine1 = "Test BVR";
            bvr.FromLine2 = "Adresse client 0";
            bvr.FromLine3 = "00000 Test";
            bvr.RefNumberNum = "0";
            bvr.ToIban = mgr.BankAccount1_Iban;
            bvr.Label = "Test GesMobile";

            int itmp = 0;

            string bvrType = context.Request["typ"];
            string bvrPrinting = context.Request["prt"];
            int mode = 0;
            int type = 0;

            if (!String.IsNullOrWhiteSpace(bvrType) && !String.IsNullOrWhiteSpace(bvrPrinting))
            {

                if (Int32.TryParse(bvrType, out itmp)) mode = itmp;
                if (Int32.TryParse(bvrPrinting, out itmp)) type = itmp;

                bvr.SetModeAndType(mode, type, context.Server.MapPath("~/App_Data/bvr"));
            }

            if (type <= 0)
            {
                string bvrTopMargin = context.Request["tmg"];
                if (!String.IsNullOrWhiteSpace(bvrTopMargin))
                {
                    if (Int32.TryParse(bvrTopMargin, out itmp))
                    {
                        bvr.TopMargin = itmp;
                    }
                }

                string bvrLeftMargin = context.Request["lmg"];
                if (!String.IsNullOrWhiteSpace(bvrLeftMargin))
                {
                    if (Int32.TryParse(bvrLeftMargin, out itmp))
                    {
                        bvr.LeftMargin = itmp;
                    }
                }
            }

            string bvrSenderId = context.Request["sid"];
            if (!String.IsNullOrWhiteSpace(bvrSenderId)) bvr.RefNumberBank = bvrSenderId;

            string bvrAccountId = context.Request["aid"];
            if (!String.IsNullOrWhiteSpace(bvrAccountId)) bvr.Account = bvrAccountId;

            if (mode == 2)
            {

                string bvrBankLine1 = context.Request["bl1"];
                if (!String.IsNullOrWhiteSpace(bvrBankLine1)) bvr.ToBankLine1 = bvrBankLine1;

                string bvrBankLine2 = context.Request["bl2"];
                if (!String.IsNullOrWhiteSpace(bvrBankLine2)) bvr.ToBankLine2 = bvrBankLine2;
            }

            byte[] pdf = bvr.AppendToPdf(null);

            context.Response.ContentType = "application/pdf";
            context.Response.BinaryWrite(pdf);
        }
    }


    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}