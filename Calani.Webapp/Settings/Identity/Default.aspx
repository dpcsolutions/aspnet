﻿<%@ Page Title="Identity" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Settings_Identity_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<form runat="server">

        <div class="content">

		    <div class="row">

                <!-- left column -->
			    <div class="col-sm-12 col-md-6">

                        <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Error %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblErrorMessage" />
						    </div>
					    </asp:Panel><!-- panelError -->

                        <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Done %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblSuccess" />.
						    </div>
					    </asp:Panel><!-- panelSuccess -->

					    
					    <asp:Panel runat="server" ID="panelEdit" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><asp:Label runat="server" ID="lblTitle" /></h6>
						    </div>


						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">

                                        <!-- name -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Name %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-gear"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, CompanyName %>' runat="server" id="tbxName" />
											    </div>
										    </div>
									    </div>
                                        <!-- /name -->

                                        <!-- vat -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.VATNumber %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-bank"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, VATNumber %>' runat="server" id="tbxVatNumber" />
											    </div>
										    </div>
									    </div>
                                        <!-- /vat -->


                                        <!-- email -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.CompanyEmail %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-envelope"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, CompanyEmail_Tooltip %>' runat="server" id="tbxEmail" />
											    </div>
										    </div>
									    </div>
                                        <!-- /email -->


                                        <!-- phone -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.CompanyPhone %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-phone"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, CompanyPhone_Tooltip %>' runat="server" id="tbxPhone" />
											    </div>
										    </div>
									    </div>
                                        <!-- /phone -->

                                        <!-- vat -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Website %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-globe"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Website %>' runat="server" id="tbxWebsite" />
											    </div>
										    </div>
									    </div>
                                        <!-- /vat -->


                                        <!-- telecopy -->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.Telecopy %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-fax"></i></span>
													<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Telecopy %>' runat="server" id="tbxTelecopy">
												</div>
											</div>
										</div>
                                        <!-- /telecopy -->
										
												


								    </fieldset>



                                    


							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->



                        <asp:Panel runat="server" ID="panelLoog" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.CompanyLogo %></h6>
						    </div>


						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">

                                        <div class="form-group">
                                            <label class="col-lg-2 control-label"><%= Resources.Resource.Logo %></label>
                                            <div class="col-lg-10">
                                                
                                                <asp:Image runat="server" ID="imgLogo" style="width:150px; height:150px; border:1px solid grey;"  />

                                                
                                            </div>
                                        </div>


                                        <!-- image -->
                                        <div class="form-group">
									        <label class="col-lg-2 control-label text-semibold"><%= Resources.Resource.ChangeLogo %></label>
									        <div class="col-lg-10">
										        <asp:FileUpload runat="server" id="fileImage" CssClass="file-input" accept=".jpg, .jpeg, .png, .gif" />
                                                <script type="text/javascript">


                                                    $(function () {

                                                        function readURL(input) {

                                                            if (input.files && input.files[0]) {
                                                                var reader = new FileReader();

                                                                reader.onload = function (e) {
                                                                    $('#<%= imgLogo.ClientID %>').attr('src', e.target.result);
                                                                    $('#<%= imgLogo.ClientID %>').css('opacity', 0.5);
                                                                }

                                                                reader.readAsDataURL(input.files[0]);
                                                            }

                                                            $('#alertImageNeedSave').show();

                                                        }


                                                        $("#<%= fileImage.ClientID %>").change(function() {
                                                          readURL(this);
                                                        });


                                                    });

                                                    
                                                </script>
                                                <span class="help-block">
                                                    <%= Resources.Resource.RecommendedLogoFileType %>: <code>PNG</code>.
                                                    <%= Resources.Resource.RecommendedLogoSize %>: <code>400*400 px</code>.</span>
									        </div>
                                            
								        </div>
                                        <!-- /image -->


                                        <div class="alert alert-warning no-border" id="alertImageNeedSave" style="display:none">
										    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only"><%=Resources.Resource.Close %></span></button>
										    <%=Resources.Resource.LogoResizedAfterSave %>
								        </div>
										
												


								    </fieldset>



                                    


							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->


                        
					    <asp:Panel runat="server" ID="PanelOwner" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.AppOwner %></h6>
						    </div>


						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">

                                                <!-- street and nr -->
												<div class="form-group">
													<label class="control-label col-lg-5"><%= Resources.Resource.CompanyAdministrator %></label>
													<div class="col-lg-7">
														<div class="input-group">
															<span class="input-group-addon"><i class="icon-hat"></i></span>
															<asp:DropDownList runat="server" ID="ddlOwner" CssClass="form-control"
                                                                 DataTextField="Name" DataValueField="Id" />
														</div>
													</div>
													
												</div>
                                                <!-- /street and nr -->
                                        </fieldset>
                                    </div>

                            </div>
                        </asp:Panel>


                        


			    </div>
			    <!-- /left column -->
                

                <!-- right column -->
                <div class="col-sm-12 col-md-6">
                    <asp:Panel runat="server" ID="panelAddress" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Headquarters %></h6>
						    </div>


						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">

                                                <!-- street and nr -->
												<div class="form-group">
													<label class="control-label col-lg-2"><%= Resources.Resource.Street %></label>
													<div class="col-lg-7">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Street_name %>' runat="server" id="tbxAdrStreetName" />
														</div>
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<span class="input-group-addon"><%= Resources.Resource.Street_nr %></span>
															<input type="text" class="form-control" placeholder="" runat="server" id="tbxAdrStreetNr" />
														</div>
													</div>
												</div>
                                                <!-- /street and nr -->

                                                <!-- second line -->
												<div class="form-group">
													<label class="control-label col-lg-2"></label>
													<div class="col-lg-10">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Address_2nd_line %>'  runat="server" id="tbxAdr2ndLine" />
														</div>
													</div>
												</div>
                                                <!-- /second line -->

                                               <!-- postal box -->
												<div class="form-group">
													<label class="control-label col-lg-2"><%= Resources.Resource.PostalBox2 %></label>
													<div class="col-lg-4">
														<div class="input-group">
															<span class="input-group-addon"><i class="icon-mailbox"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, PostalBox %>'  runat="server" id="tbxPostalBox" />
														</div>
													</div>
												</div>
                                                <!-- /postal box -->


                                                <!-- city and npa -->
												<div class="form-group">
													<label class="control-label col-lg-2"><%= Resources.Resource.Cityname %></label>
													<div class="col-lg-4">
														<div class="input-group">
													<span class="input-group-addon">#</span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Postalcode %>' runat="server" id="tbxAdrPostalCode" />
														</div>
													</div>
													<div class="col-lg-6">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-map-pin"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Cityname %>' runat="server" id="tbxAdrCityName" />
														</div>
													</div>
												</div>
                                                <!-- /city and npa -->

                                                <!-- state and country -->
												<div class="form-group">
													<label class="control-label col-lg-2"></label>
													<div class="col-lg-4">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-shield"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, State %>' runat="server" id="tbxAdrState" />
														</div>
													</div>
													<div class="col-lg-6">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-flag-o"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Country %>' runat="server" id="tbxAdrCountry" />
														</div>
													</div>
												</div>
                                                <!-- /state and country -->




											</fieldset>



                                    


							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->



                    <asp:Panel runat="server" ID="panel1" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.BankDetails %></h6>
						    </div>


						    <div class="panel-body">

							    <div class="form-horizontal">

                                   

								    <fieldset class="content-group">

                                                <!-- bank1 currency -->
												<div class="form-group">
													<label class="control-label col-lg-7"><strong><%= Resources.Resource.PrimaryAccount %></strong></label>
													<div class="col-lg-5">
														<div class="input-group">
															<span class="input-group-addon"><%= Resources.Resource.Currency %></span>
                                                            <asp:DropDownList runat="server" CssClass="form-control select"  id="ddlBank1_Currency">
                                                                <asp:ListItem Text="€" Value="€" />
                                                                <asp:ListItem Text="CHF" Value="CHF" />
                                                                <asp:ListItem Text="$" Value="$" />
                                                                <asp:ListItem Text="£" Value="£" />
											                </asp:DropDownList>
														</div>
													</div>
												</div>
                                                <!-- /bank1 currency -->

                                                <!-- bank name -->
												<div class="form-group">
													<label class="control-label col-lg-4"><%= Resources.Resource.BankName %></label>
													<div class="col-lg-8">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-bank"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, BankName %>'  runat="server" id="ddlBank1_BankName" />
														</div>
													</div>
												</div>
                                                <!-- /bank name -->   

                                                <!-- account name -->
												<div class="form-group">
													<label class="control-label col-lg-4"><%= Resources.Resource.AccountName %></label>
													<div class="col-lg-8">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-user"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, AccountName %>'  runat="server" id="ddlBank1_AccountName" />
														</div>
													</div>
												</div>
                                                <!-- /account name -->  

                                               <!-- iban name -->
												<div class="form-group">
													<label class="control-label col-lg-4"><%= Resources.Resource.Iban %></label>
													<div class="col-lg-8">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-circle"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Iban %>'  runat="server" id="ddlBank1_Iban" />
														</div>
													</div>
												</div>
                                                <!-- /iban name -->  

                                                <!-- swift name -->
												<div class="form-group">
													<label class="control-label col-lg-4"><%= Resources.Resource.Swift %></label>
													<div class="col-lg-8">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-circle"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Swift %>'  runat="server" id="ddlBank1_Swift" />
														</div>
													</div>
												</div>
                                                <!-- /swift name -->  

                                        </fieldset>

                                    <hr />

                                        <fieldset class="content-group">

                                                <!-- bank1 currency -->
												<div class="form-group">
													<label class="control-label col-lg-7"><strong><%= Resources.Resource.SecondaryAccount %></strong></label>
													<div class="col-lg-5">
														<div class="input-group">
															<span class="input-group-addon"><%= Resources.Resource.Currency %></span>
                                                            <asp:DropDownList runat="server" CssClass="form-control select"  id="ddlBank2_Currency">
                                                                <asp:ListItem Text="€" Value="€" />
                                                                <asp:ListItem Text="CHF" Value="CHF" />
                                                                <asp:ListItem Text="$" Value="$" />
                                                                <asp:ListItem Text="£" Value="£" />
											                </asp:DropDownList>
														</div>
													</div>
												</div>
                                                <!-- /bank1 currency -->

                                                <!-- bank name -->
												<div class="form-group">
													<label class="control-label col-lg-4"><%= Resources.Resource.BankName %></label>
													<div class="col-lg-8">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-bank"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, BankName %>'  runat="server" id="ddlBank2_BankName" />
														</div>
													</div>
												</div>
                                                <!-- /bank name -->   

                                                <!-- account name -->
												<div class="form-group">
													<label class="control-label col-lg-4"><%= Resources.Resource.AccountName %></label>
													<div class="col-lg-8">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-user"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, AccountName %>'  runat="server" id="ddlBank2_AccountName" />
														</div>
													</div>
												</div>
                                                <!-- /account name -->  

                                               <!-- iban name -->
												<div class="form-group">
													<label class="control-label col-lg-4"><%= Resources.Resource.Iban %></label>
													<div class="col-lg-8">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-circle"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Iban %>'  runat="server" id="ddlBank2_Iban" />
														</div>
													</div>
												</div>
                                                <!-- /iban name -->  

                                                <!-- swift name -->
												<div class="form-group">
													<label class="control-label col-lg-4"><%= Resources.Resource.Swift %></label>
													<div class="col-lg-8">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-circle"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Swift %>'  runat="server" id="ddlBank2_Swift" />
														</div>
													</div>
												</div>
                                                <!-- /swift name -->  

                                        </fieldset>


                                    </div>
                            </div>
                    </asp:Panel>



                    <asp:Panel runat="server" ID="panel2" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.SwissQR %></h6>
						    </div>


						    <div class="panel-body">

                                <p class="text-muted">
                                    <%= Resources.Resource.SwissQRExp1 %>
                                    <br />
                                    <%= Resources.Resource.SwissQRExp2 %>
                                </p>

							    <div class="form-horizontal">

                                   

								    <fieldset class="content-group">

                                                <!-- bvr type -->
												<div class="form-group">
													<label class="control-label col-lg-7"><strong><%= Resources.Resource.SwissQRType %></strong></label>
													<div class="col-lg-5">
														<div class="input-group">
															<span class="input-group-addon"><i class="icon-attachment"></i></span>
                                                            <asp:DropDownList runat="server" CssClass="form-control select"  id="ddlBvrType">
                                                                <asp:ListItem Value="0" Text='<%$ Resources:Resource, BvrNo %>' />
                                                                <asp:ListItem Value="10" Text='<%$ Resources:Resource, QR %>' />
											                </asp:DropDownList>
														</div>
													</div>
												</div>
                                                <!-- /bvr type -->

                                                <div id="panel_bvrinfo">

                                                    <!-- bvr printing -->
												    <div class="form-group">
													    <label class="control-label col-lg-4"><%= Resources.Resource.Printing %></label>
													    <div class="col-lg-8">
														    <div class="input-group">
															    <span class="input-group-addon"><i class="icon-printer"></i></span>
															    <asp:DropDownList runat="server" CssClass="form-control select"  id="ddlBvrPrinting">
                                                                    <asp:ListItem Value="1" Text='<%$ Resources:Resource, BvrFullPrint %>' />
                                                                    <asp:ListItem Value="0" Text='<%$ Resources:Resource, BvrNoBackgroundPrint %>' />
                                                                    <asp:ListItem Value="-1" Text='<%$ Resources:Resource, BvrNoBackgroundNoAccountPrint %>' />
											                    </asp:DropDownList>
														    </div>
													    </div>
												    </div>
                                                    <!-- /bvr printing -->  


                                                    <div id="panel_bvrmargin">
                                                        <!-- bvr margin -->
												        <div class="form-group">
													        <label class="control-label col-lg-4"><%= Resources.Resource.TopMargin %></label>
													        <div class="col-lg-8">
														        <div class="input-group">
															        <span class="input-group-addon"><i class="icon-arrow-down7"></i></span>
															        <input type="number" class="form-control"  runat="server" id="tbxBvrTopMargin" value="0" />
														        </div>
													        </div>
												        </div>
                                                        <!-- /bvr printing --> 

                                                        <!-- bvr margin -->
												        <div class="form-group">
													        <label class="control-label col-lg-4"><%= Resources.Resource.LeftMargin %></label>
													        <div class="col-lg-8">
														        <div class="input-group">
															        <span class="input-group-addon"><i class="icon-arrow-right7"></i></span>
															        <input type="number" class="form-control"  runat="server" id="tbxBvrLeftMargin" value="0" />
														        </div>
													        </div>
												        </div>
                                                        <!-- /bvr printing --> 
                                                    </div>

                                                    <!-- bvr sender id -->
												    <div class="form-group">
													    <label class="control-label col-lg-4"><%= Resources.Resource.BvrSenderID %></label>
													    <div class="col-lg-8">
														    <div class="input-group">
															    <span class="input-group-addon"><i class="fa fa-bank"></i></span>
															    <input type="text" id="tbxBvrSenderId" class="form-control"  runat="server" placeholder='<%$ Resources:Resource, BvrSenderID_description %>' min="0" max="999999" />
														    </div>
													    </div>
												    </div>
                                                    <!-- /bvr sender id --> 

                                                    <!-- bvr sender id -->
												    <div class="form-group">
													    <label class="control-label col-lg-4"><%= Resources.Resource.BvrAccountPartID %></label>
													    <div class="col-lg-8">
														    <div class="input-group">
															    <span class="input-group-addon"><i class="fa fa-bank"></i></span>
															    <input type="text" id="tbxBvrAccountId" class="form-control"  runat="server" placeholder='<%$ Resources:Resource, BvrAccountPartID_description %>'  />
														    </div>
													    </div>
												    </div>
                                                    <!-- /bvr sender id --> 

                                                    <div id="panel_bvrbank">
                                                        <!-- bvr bank line 1 -->
												        <div class="form-group">
													        <label class="control-label col-lg-4"><%= Resources.Resource.BvrBankLine1 %></label>
													        <div class="col-lg-8">
														        <div class="input-group">
															        <span class="input-group-addon"><i class="fa fa-bank"></i></span>
															        <input type="text" id="tbxBvrBankLine1" class="form-control"  runat="server" placeholder='<%$ Resources:Resource, BvrBankLine1_description %>'  />
														        </div>
													        </div>
												        </div>
                                                        <!-- /bvr bank line 1  --> 

                                                        <!-- bvr bank line 2 -->
												        <div class="form-group">
													        <label class="control-label col-lg-4"><%= Resources.Resource.BvrBankLine2 %></label>
													        <div class="col-lg-8">
														        <div class="input-group">
															        <span class="input-group-addon"><i class="fa fa-bank"></i></span>
															        <input type="text" id="tbxBvrBankLine2" class="form-control"  runat="server" placeholder='<%$ Resources:Resource, BvrBankLine2_description %>'  />
														        </div>
													        </div>
												        </div>
                                                        <!-- /bvr bank line 2 --> 
                                                    </div>


                                                    <div class="form-group">
                                                         <label class="control-label col-lg-4"></label>
                                                            <div class="col-lg-8" style="text-align:right">
                                                                <a href="TestBvr.ashx" id="btnPreviewBvr" target="_blank" class="btn bg-teal-400">
                                                                    <b><i class="icon-file-pdf"></i></b>
                                                                    <%= Resources.Resource.BvrTest %>
                                                                </a>
                                                            </div>
                                                    </div>
                                                </div>



                                        


                                        </fieldset>
                                        <script type="text/javascript">
                                            function toggleBvrType() {
                                                var v = $('#ContentPlaceHolder1_ddlBvrType').val();
                                                if (parseInt(v) == 0) {
                                                    $('#panel_bvrinfo').hide();
                                                }
                                                else {
                                                    $('#panel_bvrinfo').show();

                                                    if (parseInt(v) == 2 || parseInt(v) == 12) {
                                                        $('#panel_bvrbank').show();
                                                    }
                                                    else {
                                                        $('#panel_bvrbank').hide();
                                                    }
                                                }

                                                v = $('#ContentPlaceHolder1_ddlBvrPrinting').val();
                                                if (parseInt(v) == 1 || parseInt(v) == 11)
                                                {
                                                    $('#panel_bvrmargin').hide();
                                                }
                                                else {
                                                    $('#panel_bvrmargin').show();
                                                }
                                            }

                                            $('#ContentPlaceHolder1_ddlBvrType').change(function () {
                                                toggleBvrType();
                                            });

                                            $('#ContentPlaceHolder1_ddlBvrPrinting').change(function () {
                                                toggleBvrType();
                                            });


                                            $('#btnPreviewBvr').click(function (event) {
                                                event.preventDefault();

                                                var url = 'TestBvr.ashx';
                                                url += '?typ=' + $('#ContentPlaceHolder1_ddlBvrType').val();
                                                url += '&prt=' + $('#ContentPlaceHolder1_ddlBvrPrinting').val();
                                                url += '&tmg=' + $('#ContentPlaceHolder1_tbxBvrTopMargin').val();
                                                url += '&lmg=' + $('#ContentPlaceHolder1_tbxBvrLeftMargin').val();
                                                url += '&sid=' + encodeURI($('#ContentPlaceHolder1_tbxBvrSenderId').val());
                                                url += '&aid=' + encodeURI($('#ContentPlaceHolder1_tbxBvrAccountId').val());
                                                url += '&bl1=' + encodeURI($('#ContentPlaceHolder1_tbxBvrBankLine1').val());
                                                url += '&bl2=' + encodeURI($('#ContentPlaceHolder1_tbxBvrBankLine2').val());


                                                window.open(url, "PreviewPdfPopup", "width=500,height=600");

                                            });

                                            toggleBvrType();
                                        </script>




                                    </div>

                                </div>
                        </asp:Panel>



                     
			    </div>
                <!-- /right column -->
						

		    </div> <!-- /row -->


            <div class="row">

                <div class="col-sm-12 col-md-6">

                </div>

                <div class="col-sm-12 col-md-6">
                    <asp:Panel runat="server" ID="panelAction" class="panel panel-white">

						            <div class="panel-body">

                                        <!-- buttonsaction -->
								        <div class="buttonsaction text-right">
										    <label class="control-label col-lg-2"></label>
										    <div class="col-lg-10">
											    <div class="btn-group">
                                                    <asp:LinkButton runat="server" ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click">
                                                        <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                                    </asp:LinkButton>
                                                

											    </div>
										    </div>
									    </div>
                                        <!-- /buttonsaction -->

                                     </div>

                        </asp:Panel>
                    </div>

            </div>

					



        </div><!-- /content -->

    </form>

</asp:Content>

