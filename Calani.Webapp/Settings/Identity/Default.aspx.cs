﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Settings_Identity_Default : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ph.ItemLabel = Resources.Resource.Customer;
        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
           null, null, null, lblTitle, null, null);

        if (!IsPostBack)
        {
            Load();
            ph.SetStateEdit(Resources.Resource.Edit_var_id);
        }

        Title = ph.CMP.Name;
        lblTitle.Text = Resources.Resource.My_Company_Identity;

        imgLogo.ImageUrl = Page.ResolveClientUrl("~/assets/images/db/OrgLogo.ashx?dt=" + DateTime.Now.Ticks);

    }



    #region CRUD operations
    // Load from db and bind controls
    private void Load()
    {
        Calani.BusinessObjects.Contacts.EmployeesManager emgr = new Calani.BusinessObjects.Contacts.EmployeesManager(ph.CurrentOrganizationId);
        var admins = (from r in emgr.ListAdmins()
                      select new
                      {
                          Name = (r.firstName + " " + r.lastName).Trim(),
                          Id = r.id
                      }).ToList();
        ddlOwner.DataSource = admins;
        
        

        Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(ph.CurrentOrganizationId);
        mgr.Load();

        tbxName.Value = mgr.CompanyName;
        tbxEmail.Value = mgr.Email;
        tbxPhone.Value = mgr.Phone;
        tbxTelecopy.Value = mgr.Telecopy;
        tbxVatNumber.Value = mgr.VatNumber;
        tbxWebsite.Value = mgr.WebSite;

        tbxAdrStreetName.Value = mgr.AdrStreet;
        tbxAdrStreetNr.Value = mgr.AdrStreetNb;
        tbxAdr2ndLine.Value = mgr.AdrStreet2;
        tbxAdrCityName.Value = mgr.AdrCity;
        tbxAdrPostalCode.Value = mgr.AdrZipCode;
        tbxAdrState.Value = mgr.AdrState;
        tbxAdrCountry.Value = mgr.AdrCountry;
        tbxPostalBox.Value = mgr.AdrPostalBox;

        ddlBank1_Currency.SelectedValue = mgr.BankAccount1_Currency;
        ddlBank1_BankName.Value = mgr.BankAccount1_BankName;
        ddlBank1_AccountName.Value = mgr.BankAccount1_AccountName;
        ddlBank1_Iban.Value = mgr.BankAccount1_Iban;
        ddlBank1_Swift.Value = mgr.BankAccount1_Swift;

        ddlBank2_Currency.SelectedValue = mgr.BankAccount2_Currency;
        ddlBank2_BankName.Value = mgr.BankAccount2_BankName;
        ddlBank2_AccountName.Value = mgr.BankAccount2_AccountName;
        ddlBank2_Iban.Value = mgr.BankAccount2_Iban;
        ddlBank2_Swift.Value = mgr.BankAccount2_Swift;


        ddlBvrType.SelectedValue = mgr.BvrMode.ToString(); // no ? post ? bank?
        ddlBvrPrinting.SelectedValue = mgr.BvrType.ToString(); // background or not ?
        tbxBvrTopMargin.Value = mgr.BvrTopMargin.ToString();
        tbxBvrLeftMargin.Value = mgr.BvrLeftMargin.ToString();
        tbxBvrSenderId.Value = mgr.BvrSenderBank;
        tbxBvrAccountId.Value = mgr.BvrAccount;
        tbxBvrBankLine1.Value = mgr.BvrBankLine1;
        tbxBvrBankLine2.Value = mgr.BvrBankLine2;


        if (mgr.OwnerUserId != null && (from r in admins where r.Id == mgr.OwnerUserId.Value select r).Count() > 0)
        {
            ddlOwner.SelectedValue = mgr.OwnerUserId.Value.ToString();
        }
        ddlOwner.DataBind();
    }

    // Create a new db record from controls values
    private void Update()
    {
        Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(ph.CurrentOrganizationId);
        mgr.CompanyName = tbxName.Value;
        mgr.Email = tbxEmail.Value;
        mgr.Phone = tbxPhone.Value;
        mgr.Telecopy = tbxTelecopy.Value;

        mgr.AdrStreet = tbxAdrStreetName.Value;
        mgr.AdrStreetNb = tbxAdrStreetNr.Value;
        mgr.AdrStreet2 =  tbxAdr2ndLine.Value;
        mgr.AdrCity = tbxAdrCityName.Value;
        mgr.AdrZipCode = tbxAdrPostalCode.Value;
        mgr.AdrState = tbxAdrState.Value;
        mgr.AdrCountry = tbxAdrCountry.Value;
        mgr.AdrPostalBox = tbxAdrPostalCode.Value;
        mgr.VatNumber = tbxVatNumber.Value;
        mgr.WebSite = tbxWebsite.Value;


        mgr.BankAccount1_AccountName = ddlBank1_AccountName.Value;
        mgr.BankAccount1_BankName = ddlBank1_BankName.Value;
        mgr.BankAccount1_Currency = ddlBank1_Currency.Text;
        mgr.BankAccount1_Iban = ddlBank1_Iban.Value;
        mgr.BankAccount1_Swift = ddlBank1_Swift.Value;

        mgr.BankAccount2_AccountName = ddlBank2_AccountName.Value;
        mgr.BankAccount2_BankName = ddlBank2_BankName.Value;
        mgr.BankAccount2_Currency = ddlBank2_Currency.Text;
        mgr.BankAccount2_Iban = ddlBank2_Iban.Value;
        mgr.BankAccount2_Swift = ddlBank2_Swift.Value;

        if (!String.IsNullOrWhiteSpace(ddlOwner.SelectedValue))
        {
            mgr.OwnerUserId = Convert.ToInt64(ddlOwner.SelectedValue);
        }

        try
        {
            ImageUploadExtractor upload = new ImageUploadExtractor(fileImage);
            upload.Mime.Add("image/png");
            upload.Mime.Add("image/jpeg");
            upload.Mime.Add("image/gif");
            byte[] binImage = upload.Extract();
            if (binImage != null)
            {
                mgr.Logo = binImage;
            }
        }
        catch (Exception ex)
        {
            Calani.BusinessObjects.Generic.WarningReport.Report("Try to upload invalid image format", ex);
        }


        mgr.BvrMode = Convert2.StrToInt(ddlBvrType.SelectedValue, 0);
        mgr.BvrType = Convert2.StrToInt(ddlBvrPrinting.SelectedValue, 0);
        mgr.BvrTopMargin = Convert2.StrToInt(tbxBvrTopMargin.Value, 0);
        mgr.BvrLeftMargin = Convert2.StrToInt(tbxBvrLeftMargin.Value, 0);
        mgr.BvrSenderBank = tbxBvrSenderId.Value;
        mgr.BvrAccount = tbxBvrAccountId.Value;
        mgr.BvrBankLine1 = tbxBvrBankLine1.Value;
        mgr.BvrBankLine2 = tbxBvrBankLine2.Value;


        var ret = mgr.Save(true, true, false, true);
        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Company_Settings_Updated);
            Load();
        }
        else ph.DisplayError(ret.ErrorMessage);
    }



    #endregion



    #region Buttons (triggers)
    protected void btnSave_Click(object sender, EventArgs e)
    {
        Update();
    }


    #endregion





}