﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Settings_Departments_Edit : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
        ph.ItemLabel = Resources.Resource.Department;
        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
            btnDelete, btnMore, btnMoreDdl, lblTitle, linkBackList, linkNew);


        if (!IsPostBack && ph.CurrentId != null)
        {
            Load();
            ph.SetStateEdit(Resources.Resource.Edit_department);
        }

        if (ph.CurrentId == null)
        {
            ph.SetStateNew(Resources.Resource.Add_new_Department);
        }
    }

    


    #region CRUD operations
    // Load from db and bind controls
    private void Load()
    {
        Calani.BusinessObjects.Departments.DepartmentsManager mgr = new Calani.BusinessObjects.Departments.DepartmentsManager(ph.CurrentOrganizationId);
        var item = mgr.Get(Convert.ToInt16(ph.CurrentId.Value));

        tbxName.Value = item.name;
        tbxCode.Value = item.code;
        tbxDescription.Value = item.description;
    }

    // Create a new db record from controls values
    private void Update()
    {
        if (String.IsNullOrWhiteSpace(tbxName.Value))
        {
            ph.DisplayError(Resources.Resource.Error_Department_Name_Mandatory);
            return;
        }
        Calani.BusinessObjects.Departments.DepartmentsManager mgr = new Calani.BusinessObjects.Departments.DepartmentsManager(ph.CurrentOrganizationId);

        var ret = mgr.Update(Convert.ToInt16(ph.CurrentId.Value),
                        new Calani.BusinessObjects.Model.departments
                        {
                            organizationId = ph.CurrentOrganizationId,
                            name = tbxName.Value,
                            code = tbxCode.Value,
                            description = tbxDescription.Value,
                            recordStatus = 0

                        });
        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Updated_var);
            Load();
        }
        else ph.DisplayError(ret.ErrorMessage);
    }

    private long? MinusOneIsNull(long? v)
    {
        if (v != null && v.Value == -1) return null;
        return v;
    }


    // Update existing db record from new controls values
    private void Create()
    {
        if (String.IsNullOrWhiteSpace(tbxName.Value))
        {
            ph.DisplayError(Resources.Resource.Error_Department_Name_Mandatory);
            return;
        }
        Calani.BusinessObjects.Departments.DepartmentsManager mgr = new Calani.BusinessObjects.Departments.DepartmentsManager(ph.CurrentOrganizationId);

        var ret = mgr.Add(new Calani.BusinessObjects.Model.departments
        {
            name = tbxName.Value,
            code = tbxCode.Value,
            description = tbxDescription.Value,
            organizationId = ph.CurrentOrganizationId,
            recordStatus = 0
        });
        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Created_var);
            btnSave.Enabled = false;
        }
        else ph.DisplayError(ret.ErrorMessage);
    }

    protected void Delete()
    {
        Calani.BusinessObjects.Departments.DepartmentsManager mgr = new Calani.BusinessObjects.Departments.DepartmentsManager(ph.CurrentOrganizationId);
        var ret = mgr.Delete(Convert.ToInt16(ph.CurrentId.Value));
        if (ret.Success)
        {
            panelEdit.Visible = false;
            ph.DisplaySuccess(Resources.Resource.Deleted_var);
        }
    }
    #endregion



    #region Buttons (triggers)
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) Update();
        else Create();
    }


    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) Delete();
    }
    #endregion





}