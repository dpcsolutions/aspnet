﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Projects_Extractor_Default" Codebehind="Default.aspx.cs" %>
<%@ Import Namespace="System.Globalization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
        <div class="content">
		    <div>
			    <div class="panel panel-white">
                            
				    <div class="panel-heading">
					    <h6 class="panel-title">
							    <i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
							    <strong><%= Resources.Resource.ExportAllData %></strong>
					    </h6>
				    </div><!-- panel-heading -->

                            

				    <div class="panel-body">

                        <div class="row" style="margin-bottom:20px">
                            <div class="col-sm-12 col-md-12">
                            <%= Resources.Resource.ExportAllDataDescription %>
                            </div>
                                   
                        </div>


                        <div class="row">
                            <div class="col-sm-4 col-md-4">
                                <label class="checkbox-inline">
                                    <asp:CheckBox runat="server" ID="cbxQuotes" CssClass="checked control-primary" />
								    <%= Resources.Resource.Quote %>
							    </label>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <label class="checkbox-inline">
                                    <asp:CheckBox runat="server" ID="cbxJobs" CssClass="checked control-primary" />
								    <%= Resources.Resource.Opened_Job %>
							    </label>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <label class="checkbox-inline">
                                    <asp:CheckBox runat="server" ID="cbxInvoice" CssClass="checked control-primary" Checked="true" />
								    <%= Resources.Resource.Invoice %>
							    </label>
                            </div>
                        </div>
                                
                                
                                


                        <div class="row">
                            <div class="col-sm-12 col-md-12" style="padding-top:20px;">
                                <button type="button" class="btn btn-primary" id="btnDownload"><i class="icon-file-download position-left"></i> <%= Resources.Resource.Download_Excel_File %></button>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="panel panel-white">
                            
				    <div class="panel-heading">
					    <h6 class="panel-title">
							    <i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
							    <strong><%= Resources.Resource.ExportAllPdfInvoices %></strong>
					    </h6>
				    </div><!-- panel-heading -->

                            

				    <div class="panel-body">

                        <div class="row" style="margin-bottom:20px">
                            <div class="col-sm-12 col-md-12">
                            <%= Resources.Resource.ExportAllPdfInvoicesDescription %>
                            </div>
                                   
                        </div>


                       <div class="row">
                            <div class="col-sm-4 col-md-4">
                                <label class="checkbox-inline">
                                    <asp:CheckBox runat="server" ID="cbxPdfInvoicePaid" CssClass="checked control-primary" Checked="true" />
								    <%= Resources.Resource.Paid %>
							    </label>
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <label class="checkbox-inline">
                                    <asp:CheckBox runat="server" ID="cbxPdfInvoiceToPay" CssClass="checked control-primary" />
								    <%= Resources.Resource.ToPay %>
							    </label>
                            </div>
                            
                        </div>

                        <br />


                        <div class="row">


                            <fieldset class="content-group">

                                <div class="form-group" runat="server" data-placement="right">
								    <label class="control-label col-lg-2"><%= Resources.Resource.FromDate %> :</label>
								    <div class="col-lg-4">
									    <div class="input-group">
										    <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                            <input type="text" id="tbxPdfInvoiceFrom" runat="server" 
                                                   class="form-control daterange-single"/>
									    </div>
								    </div>
							    </div>

                            </fieldset>
                        
                            <fieldset class="content-group">

                                <div class="form-group" runat="server"  data-placement="right">
								    <label class="control-label col-lg-2"><%= Resources.Resource.ToDate %> :</label>
								    <div class="col-lg-4">
									    <div class="input-group">
										    <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                            <input type="text" id="tbxPdfInvoiceTo" runat="server" 
                                                   class="form-control daterange-single" />
									    </div>
								    </div>
							    </div>

                            </fieldset>
                        </div>
                                
                                
                        <div class="row">
                            <div class="col-sm-4 col-md-4">
                                <label class="checkbox-inline">
                                    <asp:CheckBox runat="server" ID="cbxDisableBvr" CssClass="checked control-primary" Checked="false" />
								    <%= Resources.Resource.DisabledQr %>
							    </label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 col-md-12" style="padding-top:20px;">
                                <button type="button" class="btn btn-primary" id="btnDownloadPdfInvoices"><i class="icon-file-download position-left"></i> <%= Resources.Resource.Download_All_Pdf %></button>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="panel panel-white">
                            
				    <div class="panel-heading">
					    <h6 class="panel-title">
							    <i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
							    <strong><%= Resources.Resource.ExportAllTimeSheets %></strong>
					    </h6>
				    </div><!-- panel-heading -->

                            

				    <div class="panel-body">

                        <div class="row" style="margin-bottom:10px">
                            <div class="col-sm-12 col-md-12">
                            <%= Resources.Resource.ExportAllTimeSheetsDescription %>
                            </div>
                        </div>
                        
                        <div class="row">
                            <fieldset class="content-group">
                                <div class="form-group" runat="server" data-placement="right">
                                    <label class="control-label col-lg-2" style="padding-top: 27px"><%= Resources.Resource.FromDate %> :</label>
                                    <div class="col-lg-4" style="padding-top: 27px">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                            <input type="text" id="timeSheetsExcelDateFrom" runat="server" 
                                                   class="form-control daterange-single"/>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <i class="icon-users4"></i> <label><%= Resources.Resource.Employees %></label>
		                                <select id="ddlEmployee" data-placeholder="~<%= Resources.Resource.All %>~" >
                                            <%= OptionsEmployees %>
        								</select>
                                    </div>
                                </div>
                             </fieldset>
                        </div>
                        
                        <div class="row">
                            <fieldset class="content-group">
                                <div class="form-group" runat="server"  data-placement="right">
                                    <label class="control-label col-lg-2"><%= Resources.Resource.ToDate %> :</label>
                                    <div class="col-lg-4">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                            <input type="text" id="timeSheetsExcelDateTo" runat="server" 
                                                   class="form-control daterange-single" />
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-12 col-md-12" >
                                <button type="button" class="btn btn-primary mr-15" id="btnDownloadTs"><i class="icon-file-download position-left"></i> <%= Resources.Resource.Download_Excel_File %></button>
                                <button type="button" class="btn btn-primary" id="btnDownloadDailyReport"><i class="icon-file-download position-left"></i> <%= Resources.Resource.DownloadDailyReport %></button>
                            </div>
                        </div>
                    </div>
                </div>

        </div>
        </div>
    
        <script type="text/javascript">
        
            $('#ddlEmployee').select2({
                multiple: false,
                minimumResultsForSearch: 1
            });
        
            $(function () {
                $('#btnDownload').click(function () {
                    var url = "Get.ashx?q=";
                    if ($('#ContentPlaceHolder1_cbxQuotes').is(':checked')) url += "1";
                    url += "&j=";
                    if ($('#ContentPlaceHolder1_cbxJobs').is(':checked')) url += "1";
                    url += "&i=";
                    if ($('#ContentPlaceHolder1_cbxInvoice').is(':checked')) url += "1";
                    window.location = url;
                });

                $('#btnDownloadTs').click(function () {
                    let url = "GetTs.ashx";
                    url += `?empid=${$("#ddlEmployee").val()}`;
                    url += `&from=${$("#ContentPlaceHolder1_timeSheetsExcelDateFrom").val()}`;
                    url += `&to=${$("#ContentPlaceHolder1_timeSheetsExcelDateTo").val()}`;
                    url += "&c=<%=CultureInfo.CurrentCulture.TwoLetterISOLanguageName%>";
                    
                    window.location = url;
                });
                
                $('#btnDownloadDailyReport').click(function () {
                    let url = "GetDailyReport.ashx";
                    url += `?empid=${$("#ddlEmployee").val()}`;
                    url += `&from=${$("#ContentPlaceHolder1_timeSheetsExcelDateFrom").val()}`;
                    url += `&to=${$("#ContentPlaceHolder1_timeSheetsExcelDateTo").val()}`;
                    url += "&c=<%=CultureInfo.CurrentCulture.TwoLetterISOLanguageName%>";
                    
                    window.location = url;
                });

                 $('#btnDownloadPdfInvoices').click(function () {
                    var url = "BatchPdf.ashx";
                    url += "?p=";
                    if ($('#ContentPlaceHolder1_cbxPdfInvoicePaid').is(':checked')) url += "1";
                    url += "&t=";
                    if ($('#ContentPlaceHolder1_cbxPdfInvoiceToPay').is(':checked')) url += "1";
                    url += "&fr=";
                    url += $('#ContentPlaceHolder1_tbxPdfInvoiceFrom').val();
                    url += "&to=";
                    url += $('#ContentPlaceHolder1_tbxPdfInvoiceTo').val();
                    url += "&nobvr=";
                    if ($('#ContentPlaceHolder1_cbxDisableBvr').is(':checked')) url += "1";
                    window.location = url;
                });
             });
        </script>
    </form>
</asp:Content>

