﻿using System;
using System.Linq;
using System.Text;

public partial class Projects_Extractor_Default : System.Web.UI.Page
{
    public PageHelper ph;

    public string OptionsEmployees { get; set; }
    
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
        
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            tbxPdfInvoiceFrom.Value = DateTime.Now.AddYears(-1).AddDays(1).ToString("dd/MM/yyyy");
            tbxPdfInvoiceTo.Value = DateTime.Now.AddDays(1).ToString("dd/MM/yyyy");
            
            timeSheetsExcelDateFrom.Value = DateTime.Now.AddMonths(-1).ToString("dd/MM/yyyy");
            timeSheetsExcelDateTo.Value = DateTime.Now.ToString("dd/MM/yyyy");
        
            var employeesManager = new Calani.BusinessObjects.Contacts.EmployeesManager(ph.CurrentOrganizationId);
            var sb = new StringBuilder();
            
            sb.Append("<option value='0'>~" + Resources.Resource.All + "~</option>");
            foreach (var x in employeesManager.List())
            {
                sb.Append("<option value=\"" + x.id + "\">" + "[" + x.initials + "] " + x.firstName + " " + x.lastName + "</option>");
            }
            
            OptionsEmployees = sb.ToString();
        }
    }
}