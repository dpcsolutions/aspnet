﻿<%@ WebHandler Language="C#" Class="BatchPdf" %>

using System;
using System.Web;
using System.IO;
using System.IO.Compression;
using System.Linq;

public class BatchPdf : IHttpHandler, System.Web.SessionState.IRequiresSessionState {

    public void ProcessRequest(HttpContext context)
    {
        System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("fr-CH");

        bool paid = context.Request.Params["p"] != null && context.Request.Params["p"] == "1";
        bool topay = context.Request.Params["t"] != null && context.Request.Params["t"] == "1";
        bool nobvr = context.Request.Params["nobvr"] != null && context.Request.Params["nobvr"] == "1";

        DateTime dtFrom = new DateTime(2000, 1, 1);
        DateTime dtTo = new DateTime(3000, 1, 1);

        DateTime.TryParseExact(context.Request.Params["fr"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeLocal, out dtFrom);
        DateTime.TryParseExact(context.Request.Params["to"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeLocal, out dtTo);

        dtFrom = dtFrom.Date;
        dtTo = dtTo.Date.AddDays(1).AddSeconds(-1);
        long org = PageHelper.GetCurrentOrganizationId(context.Session);
        long user = PageHelper.GetUserId(context.Session);
        Calani.BusinessObjects.Projects.ProjectsManager mgr = new Calani.BusinessObjects.Projects.ProjectsManager(org);
        var projects = mgr.ListInvoices(org);


        string txtID = "ID";
        string txtNumber = Calani.BusinessObjects.SharedResource.Resource.GetString("Number", culture);
        string txtCustID = Calani.BusinessObjects.SharedResource.Resource.GetString("Customer", culture) + " ID";
        string txtCust = Calani.BusinessObjects.SharedResource.Resource.GetString("Customer", culture);
        string txtAmount = Calani.BusinessObjects.SharedResource.Resource.GetString("Amount", culture);
        string txtDate = Calani.BusinessObjects.SharedResource.Resource.GetString("Date", culture);
        string txtPaymentDate = Calani.BusinessObjects.SharedResource.Resource.GetString("Payments", culture) + " " + Calani.BusinessObjects.SharedResource.Resource.GetString("Date", culture);
        string txtDue  = Calani.BusinessObjects.SharedResource.Resource.GetString("Due", culture);


        using (var memoryStream = new MemoryStream())
        {
            using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
            {

                Calani.BusinessObjects.DocGenerator.ExcelTableGenerator table = new Calani.BusinessObjects.DocGenerator.ExcelTableGenerator();
                table.FitColumns = true;
                table.Borders = true;
                table.Table = new System.Data.DataTable();

                table.Table.Columns.Add(txtID);
                table.Table.Columns.Add(txtNumber);
                table.Table.Columns.Add(txtCustID);
                table.Table.Columns.Add(txtCust);
                table.Table.Columns.Add(txtAmount);
                table.Table.Columns.Add(txtDate);
                table.Table.Columns.Add(txtPaymentDate);
                table.Table.Columns.Add(txtDue);
                
                foreach (var project in projects)
                {
                    string payment = "?";

                    bool include = false;
                    if (project.dateFullyPaid.HasValue)
                    {
                        payment = Calani.BusinessObjects.SharedResource.Resource.GetString("Paid", culture) +  " " + project.dateFullyPaid.Value.ToString("dd-MM-yyyy");
                        if (paid)
                        {
                            var d = project.dateFullyPaid.Value;
                            if (d >= dtFrom && d <= dtTo)
                            {
                                include = true;
                            }
                        }
                    }
                    else if (project.dateInvoiceDue != null)
                    {
                        payment = Calani.BusinessObjects.SharedResource.Resource.GetString("ToPay", culture) + " " + project.invoiceDue.Value;
                        if (topay)
                        {
                            include = true;

                            var d = project.dateInvoiceDue.Value;
                            if (d >= dtFrom && d <= dtTo)
                            {
                                include = true;
                            }
                        }
                    }
                    

                    if (include)
                    {
                        string fileName = project.internalId + " - " + payment + " - " + project.id + ".pdf";
                        var demoFile = archive.CreateEntry(fileName);

                        Calani.BusinessObjects.DocGenerator.InvoiceGenerator gen = new Calani.BusinessObjects.DocGenerator.InvoiceGenerator(culture, user);
                        if (nobvr) 
                            gen.DisableBvr = true;

                        gen.ProjectId = project.id;


                        byte[] data = gen.GenerateDocument();

                        using (var entryStream = demoFile.Open())
                        {
                            entryStream.Write(data, 0, data.Length);
                        }

                        System.Data.DataRow r = table.Table.NewRow();
                        r[txtID] = project.id;
                        r[txtNumber] = project.internalId;
                        r[txtCustID] = project.clientId;
                        r[txtCust] = project.clientName;
                        r[txtAmount] = project.total;
                        r[txtDate] = project.dateInvoicingOpen;
                        r[txtPaymentDate] = project.dateFullyPaid;
                        r[txtDue] = project.invoiceDue;
                        table.Table.Rows.Add(r);
                    }

                }

                var list = table.GenerateDocument();
                if (list.Length > 0)
                {
                    string fileName = "_Listing.xlsx";
                    var demoFile = archive.CreateEntry(fileName);
                    using (var entryStream = demoFile.Open())
                    {
                        entryStream.Write(list, 0, list.Length);
                    }
                }
                
            }
            
            context.Response.ContentType = "application/zip";
            context.Response.AddHeader("content-disposition", "Attachment; filename=Exports.zip");

            var alldata = memoryStream.ToArray();

            context.Response.OutputStream.Write(alldata, 0, alldata.Length);

        }

    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}