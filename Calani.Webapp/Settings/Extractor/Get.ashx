﻿<%@ WebHandler Language="C#" Class="Get" %>

using System;
using System.Web;

public class Get : IHttpHandler, System.Web.SessionState.IRequiresSessionState {

    public void ProcessRequest (HttpContext context) {
       

        bool quote = context.Request["q"] != null && context.Request["q"] == "1";
        bool job = context.Request["j"] != null && context.Request["j"] == "1";
        bool invoice = context.Request["i"] != null && context.Request["i"] == "1";


        long org = PageHelper.GetCurrentOrganizationId(context.Session);
        Calani.BusinessObjects.Projects.ProjectsDataExtractor extractor = new Calani.BusinessObjects.Projects.ProjectsDataExtractor(org);
        byte[] data = extractor.Extract(quote, job, invoice);

        context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        context.Response.AddHeader("content-disposition", "Attachment; filename=GesMobile-Export-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");

        context.Response.OutputStream.Write(data, 0, data.Length);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}