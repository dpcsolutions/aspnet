﻿<%@ WebHandler Language="C#" Class="GetDailyReport" %>

using System;
using System.Globalization;
using System.Web;

public class GetDailyReport : IHttpHandler, System.Web.SessionState.IRequiresSessionState {

    public void ProcessRequest (HttpContext context)
    {
        var employeeId = context.Request.Params["empid"] != null ? long.Parse(context.Request.Params["empid"]) : 0;
        var culture = context.Request.Params["c"];
        var localizer = Calani.BusinessObjects.SharedResource.Resource;
        
        var fromDate = context.Request.Params["from"] != null 
            ? DateTime.ParseExact(context.Request.Params["from"], "dd/MM/yyyy", null) 
            : DateTime.Now;
        
        var toDate = context.Request.Params["to"] != null 
            ? DateTime.ParseExact(context.Request.Params["to"], "dd/MM/yyyy", null) 
            : DateTime.Now;
        
        var org = PageHelper.GetCurrentOrganizationId(context.Session);
        var extractor = new Calani.BusinessObjects.TimeManagement.TimeSheetsExtractor(org);
        var data = extractor.ExtractDaily(fromDate, toDate, employeeId);

        context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        context.Response.AddHeader("content-disposition", "Attachment; filename=GesMobile-" 
            + localizer.GetString("TimeSheetDailyExport", CultureInfo.GetCultureInfo(culture)) + "-" 
            + DateTime.Now.ToString("dd-MM-yyyy_HH-mm", CultureInfo.InvariantCulture) + ".xlsx");

        context.Response.OutputStream.Write(data, 0, data.Length);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}