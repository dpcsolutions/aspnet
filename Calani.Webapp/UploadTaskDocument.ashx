﻿<%@ WebHandler Language="C#" Class="UploadTaskDocument" %>

using System;
using System.Web;

public class UploadTaskDocument : IHttpHandler,System.Web.SessionState.IRequiresSessionState {

	public void ProcessRequest (HttpContext context) {
		long taskId = -1;
		long.TryParse(context.Request.Params["taskId"], out taskId);

		Calani.BusinessObjects.Attachments.AttachmentsManager mgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
		mgr.OrganizationId = PageHelper.GetCurrentOrganizationId(context.Session);
		mgr.UserId = PageHelper.GetUserId(context.Session);
		foreach (string s in context.Request.Files)
		{
			HttpPostedFile file = context.Request.Files[s];
			string fileName = file.FileName;
			string fileExtension = file.ContentType;

			if (!string.IsNullOrEmpty(fileName))
			{
				fileExtension = System.IO.Path.GetExtension(fileName);
				if(mgr.UploadAttachmentByTask(file, taskId))
				{
					context.Response.ContentType = "text/plain";
					context.Response.Write("Uploaded");
				}
			}
		}

	}

	public bool IsReusable {
		get {
			return false;
		}
	}

}