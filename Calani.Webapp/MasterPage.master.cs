﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using Calani.BusinessObjects.Generic;
using System.Globalization;
using Calani.BusinessObjects;

public partial class MasterPage : System.Web.UI.MasterPage
{
    public CurrentMenuProperty CMP { get; set; }

    public string CurrentLanguage2
    {
        get
        {
            string ret = "en";
            try
            {
                ret = System.Threading.Thread.CurrentThread.CurrentCulture.ToString().ToLower().Substring(0, 2);
            }
            catch { }
            return ret;
        }
    }

    public string CurrentLanguage4
    {
        get
        {
            string ret = "en-US";
            try
            {
                ret = System.Threading.Thread.CurrentThread.CurrentCulture.ToString();
                if (ret == "fr-CH") ret = "fr-FR";
            }
            catch { }
            return ret;
        }
    }

    public string MomentDefaultDateFormat
    {
        get { return Tools.MomentDefaultDateFormat; }
    }
   
    public string MomentDefaultDateShortTimeFormat
    {
        get { return Tools.MomentDefaultDateShortTimeFormat; }
    }

    public string LanguageSwitch { get; set; }
    public string UserName { get; set; }
    public long UserId { get; set; }
    public bool IsNewTutorials { get; set; }

    public string StringResourcesJson { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {

        LabelPageTitle.Text = this.Page.Title;
        LabelCopyYear.Text = ConfigurationSettings.AppSettings["copyYear"];
        HyperLinkCopy.Text = ConfigurationSettings.AppSettings["copyrightText"];
        HyperLinkCopy.NavigateUrl = ConfigurationSettings.AppSettings["copyrightUrl"];

        lnkDoc.HRef = System.Configuration.ConfigurationManager.AppSettings["DocumentationUrl"];

        HtmlMenuGenerator htmlMenu = new HtmlMenuGenerator();
        htmlMenu.Page = this.Page;
        menu.InnerHtml = htmlMenu.RenderMenuHtml(PageHelper.GetCurrentOrganizationId(this.Page.Session));
        CMP = htmlMenu.GetCurrentMenuProperty();
        breadcrumb_line.InnerHtml = htmlMenu.RenderBreadCrumbHtml();
        LanguageSwitch = HtmlLanguageSwitch.Render(this.Page.Request.Url.AbsoluteUri, System.Threading.Thread.CurrentThread.CurrentCulture.ToString(), Page.ResolveClientUrl("~/assets/"));

        UserId = PageHelper.GetUserId(this.Page.Session);
        UserName = PageHelper.GetUserName(this.Page.Session);
        if(PageHelper.IsNewTutorials)
        {
            IsNewTutorials = PageHelper.IsNewTutorials;
            PageHelper.IsNewTutorials = false;
        }

        (this.Page as System.Web.UI.Page).Title += " &bull; " + ConfigurationSettings.AppSettings["title"];

        if (!String.IsNullOrWhiteSpace(CMP.Help))
        {
            btnHelp.HRef = CMP.Help;
        }
        else
        {
            btnHelp.HRef = "http://help.gesmobile.ch/";
        }

        StringResourcesJson = SharedResource.ReadStringResources(CultureInfo.CurrentCulture);
    }


}