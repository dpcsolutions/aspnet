﻿
new Vue({
    
    el: '#app',
    vuetify: new Vuetify(),
    data() {
        return {
            tutorials: tutorials,
            loaded: true
        };
    },
    
    methods: {
        decodeText: function (text) {
            return decodeURIComponent(decodeURIComponent(text));
        },
        finishTutorial: function (tid) {
          
            $("#collapse" + tid).click();

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '../API/Service.svc/FinishTutorial',
                data: JSON.stringify(tid),
                processData: false,
                dataType: "json",
                success: function (response) {
                }
            });
        }
    },
    mounted: function () {
        var self = this;

        $(".theme--light").removeClass("theme--light");

        $(".steps-basic-web-tutorials").steps({
            headerTag: "h6",
            bodyTag: "fieldset",
            transitionEffect: "fade",
            enableAllSteps: true,
            titleTemplate: '<span class="number">#index#</span> #title#',
            labels: {
                next: tutrx[0],
                previous: tutrx[1],
                finish: tutrx[2]
            },
            onFinished: function (event, currentIndex) {
                var tid = $(this).data("tid");
                self.finishTutorial(tid);
            }
        });

        setTimeout(function () {
            $.each(self.tutorials, function (i, v) {
                if (v.Viewed > 0)
                    $("#collapse" + v.Id).click();
            });
        }, 100);
       
        /*var self = this;
        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '../API/Service.svc/GetWebTutorials',
            dataType: "json",
            success: function (data) {
                self.tutorials = data;
                self.loaded = true;
            },
            error: function (error) {
                console.log(error);
            }
        });*/
    }
});