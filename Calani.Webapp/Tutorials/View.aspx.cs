﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web.UI.HtmlControls;
using Calani.BusinessObjects.Admin;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Calani.BusinessObjects.Model;

public partial class Tutorials_View_Default : System.Web.UI.Page
{
   

    private static TutorialsManager mngr;
    public PageHelper ph;
    public string TutorialsList { get; set; }
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        mngr = new TutorialsManager();

        var list = mngr.GeTutorialsList(ph.CurrentUserId, TutorialsType.Web, true, true).OrderBy(i=>i.Viewed).ThenByDescending(i=>i.Id).ToList();
         
        TutorialsList = JsonConvert.SerializeObject(list);

    }
}