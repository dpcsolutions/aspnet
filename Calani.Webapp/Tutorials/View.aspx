﻿<%@ Page Title="View" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Tutorials_View_Default" Codebehind="View.aspx.cs" %>

<asp:content contentplaceholderid="head" runat="server">
    <link rel="stylesheet" href="<%= Page.ResolveClientUrl("~//assets/css/vuetify.min.css") %>" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.1.45/css/materialdesignicons.min.css" type="text/css" />
</asp:content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
   
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        var txtTotalForPeriod = "<%= Resources.Resource.TotalForPeriod %>";
      
    </script>
	 <v-app class="content"  id="app">
          <v-container fluid>
          <div class="panel panel-white"  v-for="tutorial in tutorials" :key="tutorial.Id" v-if="loaded">
               <v-sheet  class="" elevation="8" >
                   <div class="panel-body" >
				  <div class="panel-heading" >
					  <h6 class="panel-title">{{tutorial.Name}}</h6>
					  <div class="heading-elements">
						  <ul class="icons-list" >
							  <li><a data-action="collapse" :id="'collapse'+tutorial.Id"></a></li>
						  </ul>
					  </div>
				  </div>
                  
				  <form class="steps-basic-web-tutorials" action="#" :data-tid="tutorial.Id">
					  
					  <template v-for="screen in tutorial.Screens" :key="screen.Id">
						  <h6>{{screen.title}}</h6>
                          <fieldset>
                              <v-layout row wrap align-center justify-center>
                                 
                                    <v-card  :color="'grey lighten-4'" class="ma-4"  min-height="600" min-width="800"  >
                                        <v-card-title primary-title class="justify-center">{{screen.Title}}</v-card-title>
                                        
                                        <v-card-subtitle class="text-center"  v-html="decodeText(screen.Explanation)"></v-card-subtitle>

                                        <v-card-text class="text-center">
                                            <img class="text-center" width="100%" v-bind:src="screen.Image" />-
                                        </v-card-text>

                                    </v-card> 
								</v-layout>
							</fieldset>
					  </template>
                          
							
				  </form>

			  </div>
                   
               </v-sheet>
          </div>
          </v-container>
	 </v-app>

        <!-- /content -->
        
   
    <script type="text/javascript">
        var headers = [
            { text: "Id", value: "id" },
            { text: "<%= Resources.Resource.Description %>", value: "name" },
            { text: "<%= Resources.Resource.Screens %>", value: "screensCount" },
            
            { text: "<%= Resources.Resource.MinVersion %>", value: "minVersion" },
        ]
        var tutorials = <%= TutorialsList %>;
        $(function () {
           
        });
       
    </script>
    <script type="module" src="View.js?<%= Guid.NewGuid().ToString("n") %>"></script>
    <%--<script type="module" src="View.js"></script>--%>

</asp:Content>
