﻿<%@ Page Title="Account" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Profile_Account_Default" Codebehind="Default.aspx.cs" %>

<%@ Register Src="~/assets/Controls/PasswordAdvisor.ascx" TagPrefix="uc1" TagName="PasswordAdvisor" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <form runat="server" autocomplete="off">

        <div class="content">

		    <div class="row">

                <!-- left column -->
			    <div class="col-sm-12 col-md-8">


                        <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Error %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblErrorMessage" />
						    </div>
					    </asp:Panel><!-- panelError -->

                        <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Done %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblSuccess" />
                                
						    </div>
					    </asp:Panel><!-- panelSuccess -->
					    
					    <asp:Panel runat="server" ID="panelEdit" class="panel panel-white">
						    <div class="panel-heading">
							
						    </div>

						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">

                                        <!-- title -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Title %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                                    <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlTitle" Enabled="false">
                                                        <asp:ListItem Value="Mr" Text='<%$ Resources:Resource, Title_Mr %>' />
                                                        <asp:ListItem Value="Mrs" Text='<%$ Resources:Resource, Title_Mrs %>' />
                                                        <asp:ListItem Value="Miss" Text='<%$ Resources:Resource, Title_Miss %>' />
                                                        <asp:ListItem Value="Professor" Text='<%$ Resources:Resource, Title_Professor %>' />
                                                        <asp:ListItem Value="Dr" Text='<%$ Resources:Resource, Title_Dr %>' />
                                                    </asp:DropDownList>
											    </div>
										    </div>
									    </div>
                                        <!-- /title -->

                                        <!-- Lastname -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Lastname %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-font"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Lastname %>' runat="server" id="tbxLastName" autocomplete="false"/>
											    </div>
										    </div>
									    </div>
                                        <!-- /Lastname -->

                                        <!-- Firstname -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Firstname %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-font"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Firstname %>' runat="server" id="tbxFirstName" autocomplete="false"/>
											    </div>
										    </div>
									    </div>
                                        <!-- /Firstname -->

                                        <!-- Initials -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Initials %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-vcard"></i></span>
                                                    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Initials_3_letters %>' runat="server" id="tbxInitials"  disabled="disabled" autocomplete="false"/>
											    </div>
                                                <em class="small" runat="server" id="lblWarningManagerInitials"><%= Resources.Resource.OnlyYourManagerCanChangeIt %></em>
										    </div>
									    </div>
                                        <!-- /Initials -->

										
                                        <!-- title -->
									    <div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.UserTitle %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-collaboration"></i></span>
                                                    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, UserTitle_Tooltip %>' runat="server" id="tbxUsrTitle" disabled="disabled" autocomplete="false"/>
												</div>
                                                <em class="small" runat="server" id="lblWarningManagerUsrTitle"><%= Resources.Resource.OnlyYourManagerCanChangeIt %></em>
											</div>
										</div>
                                        <!-- /internalId -->
										
                                        <!-- Dual authentication -->
									    <div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.DualAuthEmailSetting %></label>
											<div class="col-lg-10">
												<asp:CheckBox ID="chkDualAuth" runat="server" />
											</div>
										</div>
                                        <!-- /Dual authentication -->

								    </fieldset>

                                    <fieldset class="content-group">
										<legend class="text-bold"><%= Resources.Resource.Contact %></legend>

                                        <!-- email -->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.Email %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-envelope"></i></span>
                                                    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, PrimaryEmail_Tooltip %>' runat="server" id="tbxEmail" disabled="disabled" autocomplete="false"/>
												</div>
                                                <em class="small" runat="server" id="lblWarningManagerEmail"><%= Resources.Resource.OnlyYourManagerCanChangeIt %></em>
											</div>
										</div>
                                        <!-- /email -->

                                        <!-- phone1 -->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.Phone %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-phone2"></i></span>
                                                    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Phone %>' runat="server" id="tbxPhone1" autocomplete="false"/>
												</div>
											</div>
										</div>
                                        <!-- /phone1 -->

                                        <!-- phone2 -->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.MobilePhone %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-iphone"></i></span>
                                                    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, MobilePhone %>' runat="server" id="tbxPhone2" autocomplete="false"/>
												</div>
											</div>
										</div>
                                        <!-- /phone2 -->

                                        <!-- email2 -->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.AltEmail %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-envelope"></i></span>
                                                    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, AltEmail_Tooltip %>'  runat="server" id="tbxEmail2" autocomplete="false"/>
												</div>
											</div>
										</div>
                                        <!-- /email2 -->
                                    </fieldset>


                                    <fieldset class="content-group">
										<legend class="text-bold"><%= Resources.Resource.ChangeMyPassword %></legend>

                                        <div>
                                            <em class="small">
                                                <%= Resources.Resource.TypeToChangePassword %>
                                                <br />
                                                &nbsp;
                                            </em>
                                        </div>
                                        

                                        <!-- password -->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.Password %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-lock5"></i></span>
                                                    <input type="password" class="form-control" placeholder='<%$ Resources:Resource, Password %>' runat="server" id="tbxPassword" autocomplete="new-password"/>
												</div>
											</div>
										</div>
                                        <!-- /password -->

                                        <div class="form-group">
                                            <label class="control-label col-lg-2"></label>
                                            <div class="col-lg-10">
                                                <uc1:PasswordAdvisor runat="server" ID="PasswordAdvisor" PasswordInput="ContentPlaceHolder1_tbxPassword" />
                                            </div>      
                                        </div>
                                        

                                        <!-- password conf-->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.PasswordConfirmation %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-lock5"></i></span>
                                                    <input type="password" class="form-control" placeholder='<%$ Resources:Resource, PasswordConfirmation_Tooltip %>' runat="server" id="tbxPassword2" autocomplete="new-password"/>
												</div>
											</div>
										</div>
                                        <!-- /password conf -->

                                      
												

									</fieldset>




                                    <!-- buttonsaction -->
								    <div class="buttonsaction text-right">
										<label class="control-label col-lg-2"></label>
										<div class="col-lg-10">
											<div class="btn-group">
                                                <asp:LinkButton runat="server" ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click">
                                                    <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                                </asp:LinkButton>
                                                
									            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore"><span class="caret"></span></button>

											</div>
										</div>
									</div>
                                    <!-- /buttonsaction -->


							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->
					    
           


			    </div>
			    <!-- /left column -->
                

                <!-- right column -->
                <div class="col-sm-12 col-md-4">
                    
			    </div>
                <!-- /right column -->
						

		    </div> <!-- /row -->


					




        </div><!-- /content -->

    </form>
    <script type="text/javascript">

        var _tbxFirstName;
        var _tbxLastName;
        var _tbxInitials;

        function refreshInitials()
        {
            if (true)
            {
                var inits = _tbxFirstName.val();
                if (inits.length > 1) inits = inits.substring(0, 1);
                inits += _tbxLastName.val();
                if (inits.length > 3) inits = inits.substring(0, 3);
                inits = inits.toUpperCase();
                _tbxInitials.val(inits);
            }
        }

        $(function () {

            _tbxFirstName = $('#ContentPlaceHolder1_tbxFirstName');
            _tbxLastName = $('#ContentPlaceHolder1_tbxLastName');
            _tbxInitials = $('#ContentPlaceHolder1_tbxInitials');

            _tbxFirstName.keyup(refreshInitials);
            _tbxLastName.keyup(refreshInitials);
            
            

        });
    </script>
</asp:Content>




