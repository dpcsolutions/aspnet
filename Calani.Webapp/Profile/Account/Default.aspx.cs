﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Profile_Account_Default : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);

        
        
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
        {
            tbxInitials.Disabled = false;
            tbxUsrTitle.Disabled = false;
            tbxEmail.Disabled = false;

            lblWarningManagerInitials.Visible = false;
            lblWarningManagerUsrTitle.Visible = false;
            lblWarningManagerEmail.Visible = false;
        }


        ph.ItemLabel = Resources.Resource.MyAccount;
        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
            null, btnMore, null, null, null, null);
        if (ph.CurrentUserStaff)
        {
            panelEdit.Visible = false;
            ph.DisplayError("This page doest not works through a STAFF access");
        }


        if (!IsPostBack)
        {
            Load();
          //  ph.SetStateEdit(Resources.Resource.Edit_var_id);
        }

        if (ph.CurrentId == null)
        {
        //    ph.SetStateNew(Resources.Resource.Add_new_var);
        }

        this.Page.Title = Resources.Resource.MyAccount;

        //lblTitle.Text = Resources.Resource.MyAccount;
    }




    // Load from db and bind controls
    private void Load()
    {
        Calani.BusinessObjects.Contacts.EmployeesManager mgr = new Calani.BusinessObjects.Contacts.EmployeesManager(ph.CurrentOrganizationId);
        var item = mgr.Get(ph.CurrentUserId);



        tbxLastName.Value = item.lastName;
        tbxFirstName.Value = item.firstName;
        tbxInitials.Value = item.initials;
        tbxUsrTitle.Value = item.title;
        chkDualAuth.Checked = item.enableDualAuthentication;
        tbxEmail.Value = item.primaryEmail;
        tbxPassword.Value = "";
        tbxPassword2.Value = "";
        tbxPhone1.Value = item.primaryPhone;
        tbxPhone2.Value = item.secondaryPhone;
        tbxEmail2.Value = item.secondaryEmail;
        ddlTitle.SelectedValue = item.civtitle;




    }

    // Create a new db record from controls values
    private void Update()
    {
        if (!String.IsNullOrWhiteSpace(tbxPassword.Value)
            && !String.IsNullOrWhiteSpace(tbxPassword2.Value)
            && tbxPassword.Value != tbxPassword2.Value)
        {
            ph.DisplayError(Resources.Resource.Error_Password_Mismatch);
            tbxPassword.Value = "";
            tbxPassword2.Value = "";
            return;
        }

        
        if (!String.IsNullOrWhiteSpace(tbxPassword.Value)
            && !String.IsNullOrWhiteSpace(tbxPassword2.Value)
            && !Calani.BusinessObjects.Generic.PasswordAdvisor.IsEnoughtStrength(tbxPassword.Value) )
        {
            ph.DisplayError(Resources.Resource.Error_NotSecuredEnought);
            tbxPassword.Value = "";
            tbxPassword2.Value = "";
            return;
        }

        Calani.BusinessObjects.Contacts.EmployeesManager mgr = new Calani.BusinessObjects.Contacts.EmployeesManager(ph.CurrentOrganizationId);

        string pass = mgr.Get(ph.CurrentUserId).password;
        if (!String.IsNullOrWhiteSpace(tbxPassword.Value))
        {
            pass = Calani.BusinessObjects.Generic.PasswordTools.Hash(tbxPassword.Value);
        }

        var currentRecord = mgr.Get(ph.CurrentUserId);

        var ret = mgr.Update(ph.CurrentUserId,
                        new Calani.BusinessObjects.Model.contacts
                        {
                            
                            firstName = tbxFirstName.Value,
                            lastName = tbxLastName.Value,

                            
                            
                            password = pass,
                            primaryPhone = tbxPhone1.Value,
                            secondaryPhone = tbxPhone2.Value,
                            secondaryEmail = tbxEmail2.Value,
                            civtitle = ddlTitle.SelectedValue,
                            title = tbxUsrTitle.Value,
                            enableDualAuthentication = chkDualAuth.Checked,
                            initials = tbxInitials.Value,
                            primaryEmail = tbxEmail.Value,

                            type = currentRecord.type, // no change
                            weeklyHours = currentRecord.weeklyHours, //no change
                            

                            organizationId = ph.CurrentOrganizationId
                        });
        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Updated_var);
            Load();
        }
        else ph.DisplayError(ret.ErrorMessage);
    }





    protected void btnSave_Click(object sender, EventArgs e)
    {
        Update();
    }









}