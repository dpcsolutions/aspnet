﻿<%@ Page Title="Preferences" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Profile_Preferences_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<form runat="server">
    <div class="content">
        
 


        <div class="row">

            <div class="col-sm-12 col-md-12">

                

                <div class="panel panel-default">
			        <div class="panel-heading">
				        <h6 class="panel-title"><%= Resources.Resource.Dashboard %></h6>
				        <div class="heading-elements">
					        <ul class="icons-list">
					            <li><a data-action="collapse"></a></li>
					        </ul>
				        </div>
			        </div>
								
			        <div class="panel-body">
				        
                        
                        <div class="row ddlDashRow1" style="margin-bottom:10px">
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash1" runat="server" CssClass="select ddlDash" />
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash2" runat="server" CssClass="select ddlDash" />
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash3" runat="server" CssClass="select ddlDash" />
                            </div>
                           
                        </div>
                        
                        <div class="row ddlDashRow2" style="margin-bottom:10px">
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash4" runat="server" CssClass="select ddlDash" />
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash5" runat="server" CssClass="select ddlDash" />
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash6" runat="server" CssClass="select ddlDash" />
                            </div>
                            
                        </div>
                        
                        <div class="row ddlDashRow3" style="margin-bottom:10px">
                            
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash7" runat="server" CssClass="select ddlDash" />
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash8" runat="server" CssClass="select ddlDash" />
                            </div>

                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash9" runat="server" CssClass="select ddlDash" />
                            </div>
                        </div>
                        <div class="row ddlDashRow4" style="margin-bottom:10px">
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash10" runat="server" CssClass="select ddlDash" />
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash11" runat="server" CssClass="select ddlDash" />
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash12" runat="server" CssClass="select ddlDash" />
                            </div>
                        </div>

                        <div class="row ddlDashRow5" style="margin-bottom:10px">
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash13" runat="server" CssClass="select ddlDash" />
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash14" runat="server" CssClass="select ddlDash" />
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash15" runat="server" CssClass="select ddlDash" />
                            </div>
                           
                        </div>
                        
                        <div class="row ddlDashRow6" style="margin-bottom:10px">
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash16" runat="server" CssClass="select ddlDash" />
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash17" runat="server" CssClass="select ddlDash" />
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash18" runat="server" CssClass="select ddlDash" />
                            </div>
                            
                        </div>
                        
                        <div class="row ddlDashRow7" style="margin-bottom:10px">
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash19" runat="server" CssClass="select ddlDash" />
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash20" runat="server" CssClass="select ddlDash" />
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash21" runat="server" CssClass="select ddlDash" />
                            </div>
                          
                        </div>
                        <div class="row ddlDashRow8" style="margin-bottom:10px">
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash22" runat="server" CssClass="select ddlDash" />
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash23" runat="server" CssClass="select ddlDash" />
                            </div>
                            <div class="col-sm-4 col-md-4">
                                <asp:DropDownList ID="ddlDash24" runat="server" CssClass="select ddlDash" />
                            </div>
                        </div>

                        <script type="text/javascript">
                            $(function () {

                                function toggleDdlDashRow() {
                                    
                                    var lastDdlIndex = 1;
                                    for (var i = 1; i <= 24; i++) {
                                        var ddlVal = $('#ContentPlaceHolder1_ddlDash' + i).val()
                                        if (ddlVal != null && ddlVal != "" && i > lastDdlIndex) lastDdlIndex = i;
                                    }

                                    rows = Math.ceil(lastDdlIndex / 4) + 1;

                                    for (var i = 1; i <= 6; i++) {
                                        if (i <= rows) {
                                            $('.ddlDashRow' + i).show();
                                        }
                                        else {
                                            $('.ddlDashRow' + i).hide();
                                        }
                                    }

                                }

                                $('.ddlDash').change(toggleDdlDashRow);
                                toggleDdlDashRow();

                            });
                        </script>



			        </div>
		        </div>

                <div class="panel">
                    <div class="panel-body">
                        <!-- buttonsaction -->
						<div class="buttonsaction text-right" runat="server" id="panelAction">
							<label class="control-label col-lg-2"></label>
							<div class="col-lg-10">
								<div class="btn-group">
                                    <asp:LinkButton runat="server" ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click">
                                        <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                    </asp:LinkButton>
								</div>
							</div>
						</div>
                        <!-- /buttonsaction -->
                    </div>
                </div>
            </div>
        </div>
        

    </div>

    
</form>
</asp:Content>

