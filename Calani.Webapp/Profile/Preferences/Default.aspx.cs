﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Profile_Preferences_Default : System.Web.UI.Page
{
    PageHelper ph;

    List<DropDownList> widgetDdls;
    List<string> widgetsModules;
    string[] adminWidgets = new [] {"QuotesTurnover","TurnoverPotential","InvoicesTurnover", "WorkReports" };

    protected void Page_Load(object sender, EventArgs e)
    {
        ph = new PageHelper(this);

        widgetDdls = new List<DropDownList>
        {
            ddlDash1, ddlDash2, ddlDash3, ddlDash4, ddlDash5, ddlDash6, ddlDash7, ddlDash8, ddlDash9, ddlDash10,
            ddlDash11, ddlDash12, ddlDash13, ddlDash14, ddlDash15, ddlDash16, ddlDash17, ddlDash18, ddlDash19, ddlDash20,
            ddlDash21, ddlDash22, ddlDash23, ddlDash24
        };

        widgetsModules = (from r in new System.IO.DirectoryInfo(Server.MapPath("~/Dashboard/Widgets")).GetFiles("*.ascx").ToList()
                          select r.Name.Split('.').FirstOrDefault()).ToList();

        

        if (!IsPostBack)
        {
            Load();
        }
    }

    private void Load()
    {
        var widgetsModulesWithName = (from r in widgetsModules
                                      orderby r
                                      select new { name = r, label = 
                                      ResourceHelper.Get(r)
                                      }).ToList();

        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            widgetsModulesWithName = widgetsModulesWithName.Where(w => !adminWidgets.Contains(w.name)).ToList();
        }

            widgetsModulesWithName.Insert(0, new { name = "", label = "-" });

        Calani.BusinessObjects.Membership.UserVariableManager vars = new Calani.BusinessObjects.Membership.UserVariableManager(ph.CurrentUserId);

        List<string> dashboardPreferences = vars.Load("DashboardWidgets", "Customers;Projects;Diary;TimeSheet;Expenses;Quotes;Invoices;Scheduler;AbsenceRequest").Split(';').ToList();

        for (int i = 0; i < 24; i++)
        {
            widgetDdls[i].DataSource = widgetsModulesWithName;
            widgetDdls[i].DataValueField = "name";
            widgetDdls[i].DataTextField = "label";

            string pref = "";
            if(dashboardPreferences.Count - 1 > i)
            {
                pref = dashboardPreferences[i];
                if (!widgetsModules.Contains(dashboardPreferences[i]))
                    pref = "";
            }

            widgetDdls[i].SelectedValue = pref;

            widgetDdls[i].DataBind();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        Update();
        Load();
    }

    private void Update()
    {
        Calani.BusinessObjects.Membership.UserVariableManager vars = new Calani.BusinessObjects.Membership.UserVariableManager(ph.CurrentUserId);

        string dashboardPreferences = "";

        for (int i = 0; i < 24; i++)
        {
            if (!String.IsNullOrWhiteSpace(widgetDdls[i].SelectedValue))
            {
                dashboardPreferences += widgetDdls[i].SelectedValue;
            }
            if (i < 23) dashboardPreferences += ";";
        }

        vars.Save("DashboardWidgets", dashboardPreferences);

    }
}