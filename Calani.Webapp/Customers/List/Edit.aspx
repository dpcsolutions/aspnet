﻿<%@ Page Title="Edit customer" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Customers_List_Edit" Codebehind="Edit.aspx.cs" %>
<%@ Register src="../../assets/Controls/ContactTypeLabel.ascx" tagname="ContactTypeLabel" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>


<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
    <div class="btn-group" runat="server" id="quickmenu">
		<button type="button" class="btn bg-gesmobile btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
		</button>

		<ul class="dropdown-menu dropdown-menu-right">
			<li>
                <a runat="server" id="linkAddAddress2">
                    <i class="icon-city"></i> 
                    <%= Resources.Resource.Add_new %>
                    <%= Resources.Resource.Address %>
                </a>
			</li>
			<li>
                <a runat="server" id="linkAddContact2">
                    <i class="icon-address-book2"></i> 
                    <%= Resources.Resource.Add_new %>
                    <%= Resources.Resource.Contact %>
                </a>
			</li>
		</ul>
	</div>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <form runat="server">

        <div class="content">

           

		    <div class="row">

                <!-- left column -->
			    <div class="col-sm-12 col-md-6">

                        <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Error %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblErrorMessage" />
						    </div>
					    </asp:Panel><!-- panelError -->

                        <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Done %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblSuccess" />
                                <br />
                                <strong><a href="." id="linkBackList" runat="server"></a></strong>
                                    <%= Resources.Resource.or %>
                                <a href="Edit.aspx" id="linkNew" runat="server"></a>.
						    </div>
					    </asp:Panel><!-- panelSuccess -->

					    
					    <asp:Panel runat="server" ID="panelEdit" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><asp:Label runat="server" ID="lblTitle" /></h6>
						    </div>


						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">

                                        <!-- name -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Name %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-gear"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, CompanyName %>' runat="server" id="tbxName" />
											    </div>
										    </div>
									    </div>
                                        <!-- /name -->

                                        <!-- type  -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Type %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-user-plus"></i></span>
                                                    <asp:DropDownList runat="server" CssClass="form-control select"  id="ddlType">
                                                        <asp:ListItem Value="1" Text='<%$ Resources:Resource, Company %>' />
                                                        <asp:ListItem Value="0" Text='<%$ Resources:Resource, PrivateIndividual %>' />
											        </asp:DropDownList>	
											    </div>
										    </div>
									    </div>
                                        <!-- /type -->

                                        <!-- vat nr -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.VATNumber %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-gear"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, VATNumber %>' runat="server" id="tbxVatNumber" />
											    </div>
										    </div>
									    </div>
                                        <!-- /vat nr -->

										
												


								    </fieldset>

											



                                    <!-- buttonsaction -->
								    <div class="buttonsaction text-right" runat="server" id="panelActions">
										<label class="control-label col-lg-2"></label>
										<div class="col-lg-10">
											<div class="btn-group">
                                                <asp:LinkButton runat="server" ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click">
                                                    <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                                </asp:LinkButton>
                                                
									            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore"><span class="caret"></span></button>
									            <ul class="dropdown-menu dropdown-menu-right" runat="server" id="btnMoreDdl">
                                                    <li><a href="javascript:void(0)" data-toggle="modal" data-target="#modal_default"><i class="fa fa-trash"></i> <%= Resources.Resource.Delete %></a></li>
												</ul>
											</div>
										</div>
									</div>
                                    <!-- /buttonsaction -->


							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->
					    


                        


			    </div>
			    <!-- /left column -->
                

                <!-- right column -->
                <div class="col-sm-12 col-md-6">

			    </div>
                <!-- /right column -->
						

		    </div> <!-- /row -->

            <div class="row"  runat="server" id="divContacts">
                <div class="col-sm-12 col-md-12">
                <asp:Panel runat="server" ID="panel2" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><i class="icon-address-book2 display-inline-block"></i> <%= Resources.Resource.Customer_Contacts %></h6>
						    </div>


						    <div class="panel-body">
                                    
                                    <asp:GridView runat="server" ID="gridContacts"  AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:HyperLinkField DataTextField="lastName" HeaderText="Lastname" DataNavigateUrlFields="clientId,id" DataNavigateUrlFormatString="Contact.aspx?c={0}&id={1}"/>
                                            <asp:BoundField DataField="firstName" HeaderText="Firstname"  />
                                            <asp:BoundField DataField="primaryEmail" HeaderText="Email"  />
                                            <asp:BoundField DataField="primaryPhone" HeaderText="Phone"  />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <uc1:ContactTypeLabel ID="atl" runat="server" Type='<%# Bind("type") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                            
                            </div>

                            <div class="panel-footer text-right no-padding" runat="server" id="panelAddContact">
						    	<div class="row" style="margin:10px">
						    		<a href="" class="btn btn-default" id="linkAddContact" runat="server"><i class="icon-plus2 position-left"></i> 
                                        <%= Resources.Resource.Add_new %>
                                        <%= Resources.Resource.Contact %>
						    		</a>
						    	</div>
							</div><!-- panel-footer -->

                </asp:Panel>
            
                </div>
            </div>

            <div class="row" runat="server" id="divAddresses">
                <div class="col-sm-12 col-md-12">
                    
                        <asp:Panel runat="server" ID="panel1" class="panel panel-white">
						            <div class="panel-heading">
							            <h6 class="panel-title"><i class="icon-city display-inline-block"></i>  <%= Resources.Resource.Customer_Addresses %></h6>
						            </div>


						            <div class="panel-body">
                                            <asp:GridView runat="server" ID="gridAddresses" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:HyperLinkField DataTextField="caption" HeaderText="Caption" DataNavigateUrlFields="clientId,id" DataNavigateUrlFormatString="Address.aspx?c={0}&id={1}"/>
                                                    <asp:BoundField DataField="street" HeaderText="Street"  />
                                                    <asp:BoundField DataField="nb" HeaderText="Street_nr"  />
                                                    <asp:BoundField DataField="city" HeaderText="Cityname"  />
                                                    <asp:BoundField DataField="npa" HeaderText="Postalcode"  />
                                                </Columns>
                                            </asp:GridView>
                                    </div>

                                    <div class="panel-footer text-right no-padding" runat="server" id="panelAddAddress">
						    	        <div class="row" style="margin:10px">
						    		        <a href="" class="btn btn-default" id="linkAddAddress" runat="server"><i class="icon-plus2 position-left"></i> 
                                                <%= Resources.Resource.Add_new %>
                                                <%= Resources.Resource.Address %>
						    		        </a>
						    	        </div>
							        </div><!-- panel-footer -->

                        </asp:Panel>
                    </div>
            </div>
					
            <!-- basic modal: confirm delete -->
		    <div id="modal_default" class="modal fade">
			    <div class="modal-dialog">
				    <div class="modal-content">
					    <div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal">&times;</button>
						    <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
					    </div>

					    <div class="modal-body">
						    <h6 class="text-semibold"><%= Resources.Resource.Are_You_Sure_To_Delete_Client %></h6>
                            <p><%= Resources.Resource.DeleteCustomer_AlsoDeleteLinkedItems %> :</p>
                            <ul>
                                <li><%= Resources.Resource.Quotes %> : <asp:Label runat="server" ID="lblCountQuotes" /></li>
                                <li><%= Resources.Resource.Invoices %> : <asp:Label runat="server" ID="lblCountInvoices" /></li>
                                <li><%= Resources.Resource.Projects %> : <asp:Label runat="server" ID="lblCountProjects" /></li>
                            </ul>
                            <p><%= Resources.Resource.DeleteCustomer_PleaseBackupBefore %>.</p>
						    <p><strong><%= Resources.Resource.Delete_Is_Irreversible %></strong></p>
                            

					    </div>

					    <div class="modal-footer">
						    <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
                            <asp:Button runat="server" ID="btnDelete" OnClick="btnDelete_Click" CssClass="btn btn-primary" Text="<%$ Resources:Resource, Yes_Delete %>" />

					    </div>
				    </div>
			    </div>
		    </div>
		    <!-- /basic modal -->



        </div><!-- /content -->

    </form>

</asp:Content>

