﻿<%@ Page Title="Address" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Customers_List_Address" Codebehind="Address.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <form runat="server">

        <div class="content">

		    <div class="row">

                <!-- left column -->
			    <div class="col-sm-12 col-md-6">

                        <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Error %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblErrorMessage" />
						    </div>
					    </asp:Panel><!-- panelError -->

                        <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Done %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblSuccess" />
                                <br />
                                <strong><a href="." id="linkBackList" runat="server"></a></strong>
                                    <%= Resources.Resource.or %>
                                <a href="Edit.aspx" id="linkNew" runat="server"></a>.
						    </div>
					    </asp:Panel><!-- panelSuccess -->

					    
					    <asp:Panel runat="server" ID="panelEdit" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><asp:Label runat="server" ID="lblTitle" /></h6>
						    </div>


						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">

                                         <!-- caption line -->
												<div class="form-group">
													<label class="control-label col-lg-2"><%= Resources.Resource.Description %></label>
													<div class="col-lg-10">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-font"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Caption %>'  runat="server" id="tbxCaption" />
														</div>
													</div>
												</div>
                                                <!-- /caption -->

                                                <!-- street and nr -->
												<div class="form-group">
													<label class="control-label col-lg-2"><%= Resources.Resource.Street %></label>
													<div class="col-lg-7">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Street_name %>' runat="server" id="tbxAdrStreetName" />
														</div>
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<span class="input-group-addon"><%= Resources.Resource.Street_nr %></span>
															<input type="text" class="form-control" placeholder="" runat="server" id="tbxAdrStreetNr" />
														</div>
													</div>
												</div>
                                                <!-- /street and nr -->

                                                <!-- second line -->
												<div class="form-group">
													<label class="control-label col-lg-2"></label>
													<div class="col-lg-10">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Address_2nd_line %>'  runat="server" id="tbxAdr2ndLine" />
														</div>
													</div>
												</div>
                                                <!-- /second line -->

                                                <!-- postal box -->
												<div class="form-group">
													<label class="control-label col-lg-2"><%= Resources.Resource.PostalBox2 %></label>
													<div class="col-lg-4">
														<div class="input-group">
															<span class="input-group-addon"><i class="icon-mailbox"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, PostalBox %>'  runat="server" id="tbxPostalBox" />
														</div>
													</div>
												</div>
                                                <!-- /postal box -->


                                                <!-- city and npa -->
												<div class="form-group">
													<label class="control-label col-lg-2"><%= Resources.Resource.Cityname %></label>
													<div class="col-lg-4">
														<div class="input-group">
													<span class="input-group-addon">#</span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Postalcode %>' runat="server" id="tbxAdrPostalCode" />
														</div>
													</div>
													<div class="col-lg-6">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-map-pin"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Cityname %>' runat="server" id="tbxAdrCityName" />
														</div>
													</div>
												</div>
                                                <!-- /city and npa -->

                                                <!-- state and country -->
												<div class="form-group">
													<label class="control-label col-lg-2"></label>
													<div class="col-lg-4">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-shield"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, State %>' runat="server" id="tbxAdrState" />
														</div>
													</div>
													<div class="col-lg-6">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-flag-o"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Country %>' runat="server" id="tbxAdrCountry" />
														</div>
													</div>
												</div>
                                                <!-- /state and country -->




											</fieldset>

											



                                    <!-- buttonsaction -->
								    <div class="buttonsaction text-right" runat="server" id="panelAction">
										<label class="control-label col-lg-2"></label>
										<div class="col-lg-10">
											<div class="btn-group">
                                                <asp:LinkButton runat="server" ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click">
                                                    <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                                </asp:LinkButton>
                                                
									            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore"><span class="caret"></span></button>
									            <ul class="dropdown-menu dropdown-menu-right" runat="server" id="btnMoreDdl">
                                                    <li><a href="javascript:void(0)" data-toggle="modal" data-target="#modal_default"><i class="fa fa-trash"></i> <%= Resources.Resource.Delete %></a></li>
												</ul>
											</div>
										</div>
									</div>
                                    <!-- /buttonsaction -->


							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->
					    


                        


			    </div>
			    <!-- /left column -->
                

                <!-- right column -->
                <div class="col-sm-12 col-md-6">

			    </div>
                <!-- /right column -->
						

		    </div> <!-- /row -->

					
            <!-- basic modal: confirm delete -->
		    <div id="modal_default" class="modal fade">
			    <div class="modal-dialog">
				    <div class="modal-content">
					    <div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal">&times;</button>
						    <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
					    </div>

					    <div class="modal-body">
						    <h6 class="text-semibold"><%= Resources.Resource.Are_You_Sure_To_Delete %></h6>
						    <p><%= Resources.Resource.Delete_Is_Irreversible %></p>

					    </div>

					    <div class="modal-footer">
						    <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
                            <asp:Button runat="server" ID="btnDelete" OnClick="btnDelete_Click" CssClass="btn btn-primary" Text="<%$ Resources:Resource, Yes_Delete %>" />

					    </div>
				    </div>
			    </div>
		    </div>
		    <!-- /basic modal -->



        </div><!-- /content -->

    </form>

</asp:Content>

