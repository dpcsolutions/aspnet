﻿<%@ Page Title="Add a new customer" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Customers_List_New" Codebehind="New.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <form runat="server">

        <div class="content">

		    <div class="row">

                <!-- left column -->
			    <div class="col-sm-12 col-md-6">

                        <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Add_new_customer %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblErrorMessage" />
						    </div>
					    </asp:Panel><!-- panelError -->

                        <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Done %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblSuccess" />
                                <br />
                                <strong><a href="." id="linkBackList" runat="server"></a></strong>
                                    <%= Resources.Resource.or %>
                                <a href="New.aspx" id="linkNew" runat="server"></a>.
						    </div>
					    </asp:Panel><!-- panelSuccess -->

					    
					    <asp:Panel runat="server" ID="panelEdit" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><i class="fa fa-font display-inline-block"></i> <asp:Label runat="server" ID="lblTitle" /></h6>
						    </div>


						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group" style="min-height:110px;">


                                        <!-- type  -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Type %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-user-plus"></i></span>
                                                    <asp:DropDownList runat="server" CssClass="form-control select"  id="ddlType">
                                                        <asp:ListItem Value="1" Text='<%$ Resources:Resource, Company %>' />
                                                        <asp:ListItem Value="0" Text='<%$ Resources:Resource, PrivateIndividual %>' />
											        </asp:DropDownList>	
											    </div>
										    </div>
									    </div>
                                        <!-- /type -->
                                        

                                        <!-- name -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Name %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-font"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, CompanyName %>' runat="server" id="tbxName" />
											    </div>
										    </div>
									    </div>
                                        <!-- /name -->

                                        <!-- vat nr -->
									    <div class="form-group" id="panel_val">
										    <label class="control-label col-lg-2"><%= Resources.Resource.VATNumber %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-balance-scale"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, VATNumber %>' runat="server" id="tbxVatNumber" />
											    </div>
										    </div>
									    </div>
                                        <!-- /vat nr -->

										
									    


								    </fieldset>

											



                                    


							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->
					    


                        <asp:Panel runat="server" ID="panel1" class="panel panel-white">
						            <div class="panel-heading">
							            <h6 class="panel-title"><i class="icon-city display-inline-block"></i> <%= Resources.Resource.Primary_Address %></h6>
						            </div>


                                    <div class="panel-body">
                                           
                                        <div class="form-horizontal">

                                            <fieldset class="content-group">

                                                <!-- caption line -->
												<div class="form-group">
													<label class="control-label col-lg-2"><%= Resources.Resource.Description %></label>
													<div class="col-lg-10">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-font"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Caption %>'  runat="server" id="tbxCaption" />
														</div>
													</div>
												</div>
                                                <!-- /caption -->


                                                <!-- street and nr -->
												<div class="form-group">
													<label class="control-label col-lg-2"><%= Resources.Resource.Street %></label>
													<div class="col-lg-7">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Street_name %>' runat="server" id="tbxAdrStreetName" />
														</div>
													</div>
													<div class="col-lg-3">
														<div class="input-group">
															<span class="input-group-addon"><%= Resources.Resource.Street_nr %></span>
															<input type="text" class="form-control" placeholder="" runat="server" id="tbxAdrStreetNr" />
														</div>
													</div>
												</div>
                                                <!-- /street and nr -->

                                                <!-- second line -->
												<div class="form-group">
													<label class="control-label col-lg-2"></label>
													<div class="col-lg-10">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Address_2nd_line %>'  runat="server" id="tbxAdr2ndLine" />
														</div>
													</div>
												</div>
                                                <!-- /second line -->


                                                <!-- city and npa -->
												<div class="form-group">
													<label class="control-label col-lg-2"><%= Resources.Resource.Cityname %></label>
													<div class="col-lg-4">
														<div class="input-group">
													<span class="input-group-addon">#</span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Postalcode %>' runat="server" id="tbxAdrPostalCode" />
														</div>
													</div>
													<div class="col-lg-6">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-map-pin"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Cityname %>' runat="server" id="tbxAdrCityName" />
														</div>
													</div>
												</div>
                                                <!-- /city and npa -->

                                                <!-- state and country -->
												<div class="form-group">
													<label class="control-label col-lg-2"></label>
													<div class="col-lg-4">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-shield"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, State %>' runat="server" id="tbxAdrState" />
														</div>
													</div>
													<div class="col-lg-6">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-flag-o"></i></span>
															<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Country %>' runat="server" id="tbxAdrCountry" />
														</div>
													</div>
												</div>
                                                <!-- /state and country -->

                                                
                                            </fieldset>
                                        </div>
                                    </div>




                        </asp:Panel>


			    </div>
			    <!-- /left column -->
                

                <!-- right column -->
                <div class="col-sm-12 col-md-6">
                    <asp:Panel runat="server" ID="panel2" class="panel panel-white">
						            <div class="panel-heading">
							            <h6 class="panel-title"><i class="icon-address-book2 display-inline-block"></i> <%= Resources.Resource.Primary_Contact %></h6>
						            </div>


						            <div class="panel-body">
                                           
                                        <div class="form-horizontal">

                                            <fieldset class="content-group">

                                                <!-- title -->
									            <div class="form-group">
										            <label class="control-label col-lg-2"><%= Resources.Resource.Title %></label>
										            <div class="col-lg-10">
											            <div class="input-group">
												            <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                                            <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlTitle">
                                                                <asp:ListItem Value="Mr" Text='<%$ Resources:Resource, Title_Mr %>' />
                                                                <asp:ListItem Value="Mrs" Text='<%$ Resources:Resource, Title_Mrs %>' />
                                                                <asp:ListItem Value="Miss" Text='<%$ Resources:Resource, Title_Miss %>' />
                                                                <asp:ListItem Value="Professor" Text='<%$ Resources:Resource, Title_Professor %>' />
                                                                <asp:ListItem Value="Dr" Text='<%$ Resources:Resource, Title_Dr %>' />
                                                            </asp:DropDownList>
											            </div>
										            </div>
									            </div>
                                                <!-- /title -->


                                                <!-- name -->
									            <div class="form-group">
										            <label class="control-label col-lg-2"><%= Resources.Resource.Lastname %></label>
										            <div class="col-lg-10">
											            <div class="input-group">
												            <span class="input-group-addon"><i class="fa fa-font"></i></span>
												            <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Lastname %>' runat="server" id="tbxLastName" />
											            </div>
										            </div>
									            </div>
                                                <!-- /name -->

                                                <!-- name -->
									            <div class="form-group">
										            <label class="control-label col-lg-2"><%= Resources.Resource.Firstname %></label>
										            <div class="col-lg-10">
											            <div class="input-group">
												            <span class="input-group-addon"><i class="fa fa-font"></i></span>
												            <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Firstname %>' runat="server" id="tbxFirstName" />
											            </div>
										            </div>
									            </div>
                                                <!-- /name -->


    


										
                                                <!-- title -->
									            <div class="form-group">
											        <label class="control-label col-lg-2"><%= Resources.Resource.UserTitle %></label>
											        <div class="col-lg-10">
												        <div class="input-group">
													        <span class="input-group-addon"><i class="icon-collaboration"></i></span>
													        <input type="text" class="form-control" placeholder='<%$ Resources:Resource, UserTitle_Tooltip %>' runat="server" id="tbxUsrTitle">
												        </div>
											        </div>
										        </div>
                                                <!-- /internalId -->
												


								            </fieldset>



                                            <fieldset class="content-group">
										        <legend class="text-bold"><%= Resources.Resource.Contact %></legend>

                                                <!-- email -->
										        <div class="form-group">
											        <label class="control-label col-lg-2"><%= Resources.Resource.Email %></label>
											        <div class="col-lg-10">
												        <div class="input-group">
													        <span class="input-group-addon"><i class="icon-envelope"></i></span>
													        <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Email %>' runat="server" id="tbxEmail">
												        </div>
											        </div>
										        </div>
                                                <!-- /email -->

                                        
                                                <!-- email2 -->
										        <div class="form-group">
											        <label class="control-label col-lg-2"><%= Resources.Resource.AltEmail %></label>
											        <div class="col-lg-10">
												        <div class="input-group">
													        <span class="input-group-addon"><i class="icon-envelope"></i></span>
													        <input type="text" class="form-control" placeholder='<%$ Resources:Resource, AltEmail_Tooltip %>'  runat="server" id="tbxEmail2">
												        </div>
											        </div>
										        </div>
                                                <!-- /email2 -->


                                                <!-- phone1 -->
										        <div class="form-group">
											        <label class="control-label col-lg-2"><%= Resources.Resource.Phone %></label>
											        <div class="col-lg-10">
												        <div class="input-group">
													        <span class="input-group-addon"><i class="icon-phone2"></i></span>
													        <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Phone %>' runat="server" id="tbxPhone1">
												        </div>
											        </div>
										        </div>
                                                <!-- /phone1 -->

                                                <!-- phone2 -->
										        <div class="form-group">
											        <label class="control-label col-lg-2"><%= Resources.Resource.MobilePhone %></label>
											        <div class="col-lg-10">
												        <div class="input-group">
													        <span class="input-group-addon"><i class="icon-iphone"></i></span>
													        <input type="text" class="form-control" placeholder='<%$ Resources:Resource, MobilePhone %>' runat="server" id="tbxPhone2">
												        </div>
											        </div>
										        </div>
                                                <!-- /phone2 -->

                                                <!-- telecopy -->
										        <div class="form-group">
											        <label class="control-label col-lg-2"><%= Resources.Resource.Telecopy %></label>
											        <div class="col-lg-10">
												        <div class="input-group">
													        <span class="input-group-addon"><i class="fa fa-fax"></i></span>
													        <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Telecopy %>' runat="server" id="tbxTelecopy">
												        </div>
											        </div>
										        </div>
                                                <!-- /telecopy -->

                                            </fieldset>

                                        </div>

                                       
                                            
                                    </div>


                        </asp:Panel>
			    </div>
                <!-- /right column -->
						

		    </div> <!-- /row -->




            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <asp:Panel runat="server" ID="panel3" class="panel panel-white">




                        <div class="panel-body">


                            <div class="form-group">
								<div class="input-group">
										<span class="input-group-addon">
											<input type="checkbox" class="styled" name="input-addon-checkbox" id="cbxAllow2">
										</span> 
										<input type="text" class="form-control" placeholder='<%$ Resources:Resource, AllowCustomerInfoStorage %>' disabled="disabled" runat="server" id="cbxAllow">
									</div>
							</div>

	                        

                        <!-- buttonsaction -->
								    <div class="buttonsaction text-left">
										
											<div class="btn-group">
                                                <asp:LinkButton runat="server" ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click" OnClientClick="return validation()">
                                                    <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Add_new %>&nbsp;<%= Resources.Resource.Customer %>
                                                </asp:LinkButton>
                                               
											</div>
									</div>
                        <!-- /buttonsaction -->
                        </div>

                        <script type="text/javascript">

                            function isEmptyOrSpaces(str){
                                return str === null || str.match(/^ *$/) !== null;
                            }

                            function refreshName() {
                                if (parseInt($('#ContentPlaceHolder1_ddlType').val()) == 0) {

                                    var l = $('#ContentPlaceHolder1_tbxLastName').val();
                                    var f = $('#ContentPlaceHolder1_tbxFirstName').val();
                                    var s = "";
                                    if (!isEmptyOrSpaces(l)) s += l;
                                    if (!isEmptyOrSpaces(l) && !isEmptyOrSpaces(f)) s += " ";
                                    if (!isEmptyOrSpaces(f)) s += f;
                                    $('#ContentPlaceHolder1_tbxName').val(s);
                                }
                            }

                            $('#ContentPlaceHolder1_tbxLastName').change(refreshName);
                            $('#ContentPlaceHolder1_tbxFirstName').change(refreshName);
                            $('#ContentPlaceHolder1_tbxLastName').keyup(refreshName);
                            $('#ContentPlaceHolder1_tbxFirstName').keyup(refreshName);


                            $('#ContentPlaceHolder1_ddlType').change(function () {
                                onDdlTypeChange();
                            });

                            function onDdlTypeChange() {

                                if (parseInt($('#ContentPlaceHolder1_ddlType').val()) == 1) {
                                    $('#panel_val').show();
                                    $('#ContentPlaceHolder1_tbxName').attr('disabled', false);
                                    $('#ContentPlaceHolder1_tbxName').attr('placeholder', "<%= Resources.Resource.CompanyName %>");

                                }
                                else
                                {
                                    $('#panel_val').hide();
                                    $('#ContentPlaceHolder1_tbxName').attr('disabled', true);
                                    $('#ContentPlaceHolder1_tbxName').attr('placeholder', "<%= Resources.Resource.FillOppositeContact %>");
                                    refreshName();
                                }

                            }

                            onDdlTypeChange();
                            refreshName();

						</script>
                        

                    </asp:Panel>
                </div>
            </div>




        </div><!-- /content -->

    </form>
    <script type="text/javascript">
        function validation() {
            if ($("#cbxAllow2").is(':checked')) {
                return true;
            }
            else {
                alert("<%= Resources.Resource.AllowCustomerInfoStorageAlert %>");
                return false;
            }
        }
    </script>

</asp:Content>


