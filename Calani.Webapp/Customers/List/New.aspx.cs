﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Customers_List_New : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    public long CustomerId
    {
        get
        {
            if (ph.CurrentId != null)
                return ph.CurrentId.Value;

            return long.MaxValue;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // page not available for employee
        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            Response.Redirect(".");
        }
        // -->

        ph.ItemLabel = Resources.Resource.Customer;
        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
            null, null, null, lblTitle, linkBackList, linkNew);


        if (!IsPostBack)
        {
            ph.SetStateNew(Resources.Resource.Add_new_var);
        }

        this.Title = lblTitle.Text = Resources.Resource.Add_new_customer;

    }



    #region CRUD operations


    // Update existing db record from new controls values
    private void Create()
    {
        /*
        if(String.IsNullOrWhiteSpace(tbxAdrCityName.Value))
        {
            ph.DisplayError(Resources.Resource.ERR_Client_AddressMissing);
            return;
        }
        if (String.IsNullOrWhiteSpace(tbxLastName.Value))
        {
            ph.DisplayError(Resources.Resource.ERR_Client_ContactLastName);
            return;
        }
        if (String.IsNullOrWhiteSpace(tbxEmail.Value))
        {
            ph.DisplayError(Resources.Resource.ERR_Client_ContactEmail);
            return;
        }*/

        if (ddlType.SelectedValue == "0")
        {
            tbxName.Value = tbxLastName.Value + " " + tbxFirstName.Value;
        }

        bool isCompany = ddlType.SelectedValue == "1";

        Calani.BusinessObjects.CustomerAdmin.ClientsManager mgr = new Calani.BusinessObjects.CustomerAdmin.ClientsManager(ph.CurrentOrganizationId);
        var newitem = new Calani.BusinessObjects.Model.clients
        {
            companyName = tbxName.Value,
            vatNumber = tbxVatNumber.Value,
            organizationId = ph.CurrentOrganizationId,
            isCompany = isCompany
        };
        

        var ret = mgr.Add(newitem);
        if (ret.Success)
        {

            if (String.IsNullOrWhiteSpace(tbxAdrStreetName.Value) || tbxAdrStreetName.Value.Length < 2)
            {
                if (String.IsNullOrWhiteSpace(tbxAdrCityName.Value) || tbxAdrCityName.Value.Length < 2)
                {
                    if (String.IsNullOrWhiteSpace(tbxCaption.Value) || tbxCaption.Value.Length < 2)
                    {
                        tbxCaption.Value = "(Default)";

                    }

                }
            }
            
            if (String.IsNullOrWhiteSpace(tbxLastName.Value) || tbxLastName.Value.Length < 2) tbxLastName.Value = "("+ Resources.Resource.MainContact + ")";
            //if (String.IsNullOrWhiteSpace(tbxEmail.Value) || !Calani.BusinessObjects.Generic.EmailValidator.IsEmailValid(tbxEmail.Value)) tbxEmail.Value = "c" + ret.Record.id + "@gesmobile.ch";

            try
            {
                Calani.BusinessObjects.Contacts.ClientAddressesManager addrMgr = new Calani.BusinessObjects.Contacts.ClientAddressesManager(ph.CurrentOrganizationId, ret.Record.id);

                var addr = addrMgr.Add(new Calani.BusinessObjects.Model.addresses
                {
                    caption = tbxCaption.Value,
                    street = tbxAdrStreetName.Value,
                    nb = tbxAdrStreetNr.Value,
                    street2 = tbxAdr2ndLine.Value,
                    npa = tbxAdrPostalCode.Value,
                    city = tbxAdrCityName.Value,
                    state = tbxAdrState.Value,
                    country = tbxAdrCountry.Value,
                    postbox = tbxAdrPostalCode.Value,

                    clientId = ret.Record.id,
                    organizationId = ph.CurrentOrganizationId,

                    type = Convert.ToInt32(Calani.BusinessObjects.Contacts.AddressTypeEnum.CustomerAddress)
                });
            }
            catch { }

            try
            {
                Calani.BusinessObjects.Contacts.ClientContactsManager ctcMgr = new Calani.BusinessObjects.Contacts.ClientContactsManager(ph.CurrentOrganizationId, ret.Record.id);

                var ctc = ctcMgr.Add(new Calani.BusinessObjects.Model.contacts
                {
                    lastName = tbxLastName.Value,
                    firstName = tbxFirstName.Value,
                    civtitle = ddlTitle.SelectedValue,
                    title = tbxUsrTitle.Value,
                    primaryEmail = tbxEmail.Value,
                    secondaryEmail = tbxEmail2.Value,
                    primaryPhone = tbxPhone1.Value,
                    secondaryPhone = tbxPhone2.Value,
                    telecopy = tbxTelecopy.Value,
                    clientId = ret.Record.id,
                    organizationId = ph.CurrentOrganizationId,
                    type = Convert.ToInt32(Calani.BusinessObjects.Contacts.ContactTypeEnum.Contact_CustomerMain),
                    
                });

            }
            catch { }

            ph.DisplaySuccess(Resources.Resource.Created_var);
            btnSave.Enabled = false;
            Response.Redirect("Details.aspx?id=" + ret.Record.id);
        }
        else ph.DisplayError(ret.ErrorMessage);
    }

    protected void Delete()
    {
        Calani.BusinessObjects.CustomerAdmin.ClientsManager mgr = new Calani.BusinessObjects.CustomerAdmin.ClientsManager(ph.CurrentOrganizationId);
        var ret = mgr.Delete(ph.CurrentId.Value);
        if (ret.Success)
        {
            panelEdit.Visible = false;
            ph.DisplaySuccess(Resources.Resource.Deleted_var);
        }
    }
    #endregion



    #region Buttons (triggers)
    protected void btnSave_Click(object sender, EventArgs e)
    {
        Create();
    }


    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) Delete();
    }
    #endregion





}