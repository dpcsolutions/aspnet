﻿using Calani.BusinessObjects.CustomerAdmin;
using System;
using System.Linq;
using System.Web.UI.WebControls;

public partial class Customers_List_Default : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    public string OptionsPeriod { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        var orgMgr = new OrganizationManager(ph.CurrentOrganizationId);
        orgMgr.Load();

        #region datefilters
        DateTime f = new DateTime(DateTime.Now.Year, 1, 1); 
        DateTime? t = null;
        string selection = "";

        long temp = 0;
        if (Request.Params["f"] != null )
        {
            if (long.TryParse(Request.Params["f"], out temp))
            {
                f = new DateTime(temp);
                selection = temp.ToString();
            }
            else
                selection = "*";
        }
        else 
        {
            selection = f.Ticks.ToString(); 
        }

        selection += ";";

        if (Request.Params["t"] != null && long.TryParse(Request.Params["t"], out temp))
        {
            t = new DateTime(temp);
            selection += temp;
        }
        else { selection += "*"; }
        #endregion



        OptionsPeriod = ph.RenderPeriodsListOption(selection, preselect: orgMgr.DefaultPeriodFilter);

        


        Calani.BusinessObjects.CustomerAdmin.ClientsManager mgr = new Calani.BusinessObjects.CustomerAdmin.ClientsManager(ph.CurrentOrganizationId);
        long? employeeFilter = null;

        if (PageHelper.GetUserType(Session) == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
            employeeFilter = PageHelper.GetUserId(Session);

        var data = mgr.ListExtended(f, t, employeeFilter);


        grid.DataSource = data;

        lblActiveCustomersCount.InnerText = (from r in data where (r.CountInvoices + r.CountJobs + r.CountQuotes) > 0 select r).Count().ToString();
        lblCustomersCount.InnerText = ResourceHelper.Get("Active_Customers_of", data.Count.ToString());
        lblJobsCount.InnerText = (from r in data select r.CountJobs).Sum().ToString();
        
        double billed = (from r in data where r.SumInvoicedTotalWithoutTaxes != null select r.SumInvoicedTotalWithoutTaxes.Value).Sum();
        
        //lblBilled.InnerText = .ToString("#.##", System.Globalization.CultureInfo.InvariantCulture);
        lblBilled.InnerText = String.Format("{0:0,0.00}", billed);
        

        new Html5GridView(grid);



        // remove rights for employee
        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            panelAdd.Visible = false;
            quickmenu.Visible = false;
            panelStatsTotalBilled.Visible = false;
            foreach (var col in grid.Columns)
            {
                var bcol = col as BoundField;
                if(bcol != null)
                {
                    if (bcol.DataField == "SumInvoicedTotal")
                        bcol.Visible = false;

                    if (bcol.DataField == "SumInvoicedDue")
                        bcol.Visible = false;

                    if(bcol.DataField == "SumInvoicedTotalWithoutTaxes")
                    {
                        bcol.Visible = !orgMgr.HideBillingColumnOnCustomers;
                    }
                    
                }


                var tcol = col as TemplateField;
                if (tcol != null)
                {
                    if (tcol.ItemStyle.CssClass.Contains("admin"))
                        tcol.Visible = false;
                }

            }

        }
        // -->

    }
}