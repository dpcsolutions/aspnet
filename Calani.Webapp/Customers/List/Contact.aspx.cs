﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Customers_List_Contact : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    long customerId;


    protected void Page_Load(object sender, EventArgs e)
    {
        customerId = Convert.ToInt64(Request.Params["c"]);

        



        Calani.BusinessObjects.CustomerAdmin.ClientsManager cmgr = new Calani.BusinessObjects.CustomerAdmin.ClientsManager(ph.CurrentOrganizationId);
        var customer = cmgr.Get(customerId);

        string custName = customer!=null? customer.companyName: String.Empty;

        ph.ItemLabel = Resources.Resource.Contact;
        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
            btnDelete, btnMore, btnMoreDdl, lblTitle, linkBackList, linkNew,
            custName);

        if (!IsPostBack && ph.CurrentId != null)
        {
            Load();
            ph.SetStateEdit(Resources.Resource.Edit_var_id);
        }

        if (ph.CurrentId == null)
        {
            ph.SetStateNew(Resources.Resource.Add_new_var);
        }

        linkBackList.InnerText = Resources.Resource.BackToCustomerDetails;
        linkBackList.HRef = "Edit.aspx?id=" + customerId;
        linkNew.HRef = "Contact.aspx?c=" + customerId;


        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            panelActions.Visible = false;
            tbxEmail.Disabled = true;
            tbxEmail2.Disabled = true;
            tbxFirstName.Disabled = true;
            tbxLastName.Disabled = true;
            tbxPhone1.Disabled = true;
            tbxPhone2.Disabled = true;
            tbxTelecopy.Disabled = true;
            tbxUsrTitle.Disabled = true;
            cbxMain.Enabled = false;
            ddlTitle.Enabled = false;
        }

    }



    #region CRUD operations
    // Load from db and bind controls
    private void Load()
    {
        Calani.BusinessObjects.Contacts.ClientContactsManager mgr = new Calani.BusinessObjects.Contacts.ClientContactsManager(ph.CurrentOrganizationId, customerId);
        var item = mgr.Get(ph.CurrentId.Value);

        tbxLastName.Value = item.lastName;
        tbxFirstName.Value = item.firstName;
        tbxUsrTitle.Value = item.title;
        tbxEmail.Value = item.primaryEmail;
        tbxEmail2.Value = item.secondaryEmail;
        tbxPhone1.Value = item.primaryPhone;
        tbxPhone2.Value = item.secondaryPhone;
        tbxTelecopy.Value = item.telecopy;
        ddlTitle.SelectedValue = item.civtitle;

        if (item.type != null && item.type == Convert.ToInt32(Calani.BusinessObjects.Contacts.ContactTypeEnum.Contact_CustomerMain))
        {
            cbxMain.Checked = true;
        }
        else
        {
            cbxMain.Checked = false;
        }

    }

    private int GetCheckedType()
    {
        if(cbxMain.Checked == true) return Convert.ToInt32(Calani.BusinessObjects.Contacts.ContactTypeEnum.Contact_CustomerMain);
        return Convert.ToInt32(Calani.BusinessObjects.Contacts.ContactTypeEnum.Contact_CustomerOther);
    }

    // Create a new db record from controls values
    private void Update()
    {
        Calani.BusinessObjects.Contacts.ClientContactsManager mgr = new Calani.BusinessObjects.Contacts.ClientContactsManager(ph.CurrentOrganizationId, customerId);

        var ret = mgr.Update(ph.CurrentId.Value,
                        new Calani.BusinessObjects.Model.contacts
                        {
                            lastName = tbxLastName.Value,
                            firstName = tbxFirstName.Value,
                            title = tbxUsrTitle.Value,
                            primaryEmail = tbxEmail.Value,
                            secondaryEmail = tbxEmail2.Value,
                            primaryPhone = tbxPhone1.Value,
                            secondaryPhone = tbxPhone2.Value,
                            telecopy = tbxTelecopy.Value,
                            clientId = customerId,
                            organizationId = ph.CurrentOrganizationId,
                            civtitle = ddlTitle.SelectedValue,
                            type = GetCheckedType()

                        });
        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Updated_var);
            Load();
        }
        else ph.DisplayError(ret.ErrorMessage);
    }


    // Update existing db record from new controls values
    private void Create()
    {
        Calani.BusinessObjects.Contacts.ClientContactsManager mgr = new Calani.BusinessObjects.Contacts.ClientContactsManager(ph.CurrentOrganizationId, customerId);

        var ret = mgr.Add(new Calani.BusinessObjects.Model.contacts
        {
            lastName = tbxLastName.Value,
            firstName = tbxFirstName.Value,
            title = tbxUsrTitle.Value,
            primaryEmail = tbxEmail.Value,
            secondaryEmail = tbxEmail2.Value,
            primaryPhone = tbxPhone1.Value,
            secondaryPhone = tbxPhone2.Value,
            telecopy = tbxTelecopy.Value,
            clientId = customerId,
            organizationId = ph.CurrentOrganizationId,
            civtitle = ddlTitle.SelectedValue,
            type = GetCheckedType()
        });
        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Created_var);
            btnSave.Enabled = false;
        }
        else ph.DisplayError(ret.ErrorMessage);
    }

    protected void Delete()
    {
        Calani.BusinessObjects.Contacts.ClientContactsManager mgr = new Calani.BusinessObjects.Contacts.ClientContactsManager(ph.CurrentOrganizationId, customerId);
        var ret = mgr.Delete(ph.CurrentId.Value);
        if (ret.Success)
        {
            panelEdit.Visible = false;
            ph.DisplaySuccess(Resources.Resource.Deleted_var);
        }
    }
    #endregion



    #region Buttons (triggers)
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) Update();
        else Create();
    }


    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) Delete();
    }
    #endregion





}