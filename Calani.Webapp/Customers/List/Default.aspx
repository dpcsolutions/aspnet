﻿<%@ Page Title="Customers" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Customers_List_Default" Codebehind="Default.aspx.cs" %>
<%@ Register src="../../assets/Controls/CountIconLabel.ascx" tagname="CountIconLabel" tagprefix="cil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
    <div class="btn-group" runat="server" id="quickmenu">
		<button type="button" class="btn bg-gesmobile btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
		</button>

		<ul class="dropdown-menu dropdown-menu-right">
			<li><a href="New.aspx"><i class="icon-plus2"></i> <%= Resources.Resource.Add_new %> <%= Resources.Resource.Customer %></a></li>
            <li class="divider"></li>
            <li><a href="../Import/"><i class="icon-file-upload"></i> <%= Resources.Resource.ImportFile %></a></li>
		</ul>

	</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">


        <div class="content">
                    

					<div class="row">
						<div class="col-sm-6 col-md-3">
							<div class="panel panel-body panel-body-accent">
								<div class="media" style="margin-bottom:4px;">
									<div class="media-left media-middle">
										<i class="icon-users2 icon-3x text-pink-400"></i>
									</div>

									<div class="media-body text-right">
										<h3 class="no-margin text-semibold" runat="server" ID="lblActiveCustomersCount">.</h3>
										<span class="text-uppercase text-size-mini text-muted">
                                            <span runat="server" ID="lblCustomersCount">.</span>
										</span>
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-6 col-md-3">
							<div class="panel panel-body panel-body-accent">
								<div class="media" style="margin-bottom:4px;">
									<div class="media-left media-middle">
										<i class="icon-checkmark icon-3x text-success-400"></i>
									</div>

									<div class="media-body text-right">
										<h3 class="no-margin text-semibold" runat="server" ID="lblJobsCount">.</h3>
										<span class="text-uppercase text-size-mini text-muted"><%= Resources.Resource.Done_Jobs %></span>
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-6 col-md-3">
							<div class="panel panel-body panel-body-accent" runat="server" id="panelStatsTotalBilled">
								<div class="media" style="margin-bottom:4px;">
									<div class="media-left media-middle">
										<i class="icon-coins icon-3x text-success-400"></i>
									</div>

									<div class="media-body text-right">
										<h3 class="no-margin text-semibold" runat="server" ID="lblBilled">.</h3>
										<span class="text-uppercase text-size-mini text-muted"><%= Resources.Resource.Total_Billed %></span>
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-6 col-md-3">
							<div class="panel panel-body panel-body-accent">
								<div class="media no-margin">
									
									<div>
									<i class="fa fa-calendar-check-o" aria-hidden="true"></i> <%= Resources.Resource.Select_Period %>
									</div>
									<div>
										<select id="ddlPeriod" class="select" onchange="setPeriodFilter()">
												<%= OptionsPeriod %>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
                        <div class="col-sm-12 col-md-12">
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
										<strong><%= ph.CMP.Name %></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

							<div class="panel-body">
                                <asp:GridView runat="server" ID="grid" AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:HyperLinkField DataTextField="companyName" HeaderText="Name" DataNavigateUrlFields="id" DataNavigateUrlFormatString="Details.aspx?id={0}"/>
                                        <asp:BoundField DataField="ContactName" HeaderText="Contact"  />
                                        <asp:BoundField DataField="SumInvoicedTotalWithoutTaxes" HeaderText="Billed" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right" DataFormatString="{0:0,0.00}" />
                                        <asp:BoundField DataField="SumInvoicedDue" HeaderText="Due" ItemStyle-ForeColor="Red" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right" DataFormatString="{0:0,0.00}" />
                                        <asp:TemplateField HeaderText="Sent_Quotes" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right admin">
                                            <ItemTemplate>
                                                <cil:CountIconLabel runat="server" Id="cq" Count='<%# Bind("CountQuotes") %>' Icon="icon-upload" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
										<asp:TemplateField HeaderText="DeliveryNotes" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right admin">
                                            <ItemTemplate>
                                                <cil:CountIconLabel runat="server" Id="cn" Count='<%# Bind("CountDeliveryNotes") %>' Icon="icon-box" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Started_Jobs" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right admin">
                                            <ItemTemplate>
                                                <cil:CountIconLabel runat="server" Id="cj" Count='<%# Bind("CountJobs") %>' Icon="icon-cog5 " />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sent_Invoices" HeaderStyle-CssClass="text-right" ItemStyle-CssClass="text-right admin">
                                            <ItemTemplate>
                                                <cil:CountIconLabel runat="server" Id="ci" Count='<%# Bind("CountInvoices") %>' Icon="icon-file-check2" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div><!-- panel-body -->

                            <div class="panel-footer text-right no-padding" id="panelAdd" runat="server">
						    	<div class="row" style="margin:10px">

                                    <a href="../Import/" class="btn btn-default"><i class="icon-file-upload position-left"></i> 
                                        <%= Resources.Resource.ImportFile %>
						    		</a>

						    		<a href="New.aspx" class="btn btn-default"><i class="icon-plus2 position-left"></i> 
                                        <%= Resources.Resource.Add_new_customer %>
                                        
						    		</a>
						    	</div>
							</div><!-- panel-footer -->

                        </div>

                        </div>
                    </div>
        </div><!-- /content -->

    </form>
    <script type="text/javascript">
        var _periodFilterFrom = null;
        var _periodFilterTo = null;



        function setPeriodFilter() {
            _periodFilterFrom = "";
            _periodFilterTo = "";

            var pval = $('#ddlPeriod').val();
            if (pval != null && pval.indexOf(';') > 0) {
                pval = pval.split(';');
                if (pval.length == 2) {
                    _periodFilterFrom = pval[0];
                    _periodFilterTo = pval[1];

                    if (_periodFilterFrom != "*") _periodFilterFrom = parseInt(_periodFilterFrom);
                    else _periodFilterFrom = "";
                    if (isNaN(_periodFilterFrom)) _periodFilterFrom = "";

                    if (_periodFilterTo != "*") _periodFilterTo = parseInt(_periodFilterTo);
                    else _periodFilterTo = "";
                    if (isNaN(_periodFilterTo)) _periodFilterTo = "";
                }
            }

            

            //alert(_periodFilterFrom);
            //alert(_periodFilterTo);
            var q = "?f=" + _periodFilterFrom + "&t=" + _periodFilterTo;
            window.location = q;
        }
 
    </script>
</asp:Content>
