﻿<%@ Page Title="Customer Details" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Customers_List_Details" Codebehind="Details.aspx.cs" %>
<%@ Register src="../../assets/Controls/RatingViewer.ascx" tagname="RatingViewer" tagprefix="rv" %>
<%@ Register src="../../assets/Controls/ProjectStatusLabel.ascx" tagname="ProjectStatusLabel" tagprefix="psl" %>
<%@ Register src="../../assets/Controls/ProjectVisitsLabel.ascx" tagname="ProjectVisitsLabel" tagprefix="pvl" %>
<%@ Register src="../../assets/Controls/ProjectNextVisitLabel.ascx" tagname="ProjectNextVisitLabel" tagprefix="pnvl" %>
<%@ Register src="../../assets/Controls/RemainingVisitsLabel.ascx" tagname="RemainingVisitsLabel" tagprefix="rvl" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
    <div class="btn-group" runat="server" id="quickmenu">
		<button type="button" class="btn bg-gesmobile btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
		</button>

		<ul class="dropdown-menu dropdown-menu-right">
			<li>
                <a href="Edit.aspx?id=<%=ph.CurrentId %>">
                    <i class="fa fa-edit"></i> 
                    <%= Resources.Resource.Details %> <%= Resources.Resource.Customer %>
			    </a>
			</li>
            <li class="divider"></li>
            <li runat="server" id="qmAddQuote">
                <a href="../../Projects/Quotes/Job.aspx?cust=<%=ph.CurrentId %>">
                    <i class="icon-upload"></i> 
                    <%= Resources.Resource.Create %> <%= Resources.Resource.Quote %>
			    </a>
			</li>
            <li runat="server" id="qmAddJob">
                <a href="../../Projects/Opens/Job.aspx?cust=<%=ph.CurrentId %>">
                    <i class="icon-cog5"></i> 
                    <%= Resources.Resource.Create %> <%= Resources.Resource.Job %>
			    </a>
			</li>
            <li runat="server" id="qmAddInvoice">
                <a href="../../Projects/Invoices/Job.aspx?cust=<%=ph.CurrentId %>">
                    <i class="icon-file-check2"></i> 
                    <%= Resources.Resource.Create %> <%= Resources.Resource.Invoice %>
			    </a>
			</li>
		</ul>
	</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <form runat="server">

        <div class="content">
            
            <div class="row">


							<div class="col-sm-6 col-md-6">
								<div class="panel panel-body minh-150">
									<div class="media">
										<div class="media-left">
											<i class="fa fa-user-circle-o icon-2x text-pink" style="font-size:32px;"></i>
										</div>

										<div class="media-body">
											<h6 class="media-heading" runat="server" id="lblCompanyName">.</h6>
											<p class="media-heading" runat="server" id="lblMainContactName"></p>
											<p class="media-heading" runat="server" id="lblMainContactPhone"></p>
											<p runat="server" id="lblMainContactEmail"></p>
											<div>
												<asp:HyperLink runat="server" ID="linkEdit" class="btn btn-primary">
                                                    <i class="fa fa-edit"></i> <%=Resources.Resource.Details %>

												</asp:HyperLink>
											</div>
										</div>
									</div>
								</div>
							</div>


							<div class="col-sm-6 col-md-3">
								

							<!-- User card with thumb and social icons at the bottom -->
							<div class="panel">
								<div class="panel-body minh-130">
									<div class="row text-center">

                                        <div class="col-xs-3 text-green" runat="server" id="blocCountQuotes">
											<p><i class="icon-upload icon-2x display-inline-block"></i></p>
											<h5 class="text-semibold no-margin" id="lblCountQuotes">.</h5>
											<strong class="text-muted text-size-small"><a href="#quotes" class="text-green"><%=Resources.Resource.Quote %></a></strong>
										</div>

										 <div class="col-xs-3 text-green" runat="server" id="Div1">
											<p><i class="icon-box icon-2x display-inline-block"></i></p>
											<h5 class="text-semibold no-margin" id="lblCountNotes">.</h5>
											<strong class="text-muted text-size-small"><a href="#notes" class="text-green"><%=Resources.Resource.DeliveryNotes %></a></strong>
										</div>
										
										<div class="col-xs-3 text-green">
											<p><i class="icon-cog5 icon-2x display-inline-block"></i></p>
											<h5 class="text-semibold no-margin" id="lblCountJobs">.</h5>
											<strong class="text-size-small"><a href="#jobs" class="text-green"><%=Resources.Resource.Jobs %></a></strong>
										</div>

										<div class="col-xs-3 text-green" runat="server" id="blocCountInvoices">
											<p><i class="icon-file-check2 icon-2x display-inline-block"></i></p>
											<h5 class="text-semibold no-margin" id="lblCountInvoices">.</h5>
											<strong class="text-muted text-size-small"><a href="#invoices" class="text-green"><%=Resources.Resource.Invoices %></a></strong>
										</div>
									</div>									
								</div>

						    	<div class="panel-footer text-center no-padding">
							    	<div class="row">
							    				<i class="fa fa-calendar"></i> 
                                                <asp:HyperLink runat="server" ID="linkNextVisit" />
							    				
							    	</div>
								</div>
							</div>
							<!-- /user card with thumb and social icons at the bottom -->

						</div>





						<div class="col-sm-6 col-md-3">
							<div class="panel panel-body panel-body-accent">
								<div class="media no-margin">
										
									<div>
									<i class="fa fa-calendar-check-o" aria-hidden="true"></i> <%= Resources.Resource.Select_Period %>
									</div>
									<div>
										<select id="ddlPeriod" class="select" onchange="setPeriodFilter()">
												<%= OptionsPeriod %>
									    </select>
									</div>
								</div>
							</div>
						</div>

                </div>

            <div class="col-sm-12 col-md-12">


                <asp:Panel ID="PanelQuotes" runat="server">
                <div class="row" id="quotes">
                    <a name="quotes"></a>
					<div class="panel panel-white">
						<div class="panel-heading">
							<h6 class="panel-title">
								<a name="jobs">
									<i class="icon-upload display-inline-block text-green"></i>
									<strong><%= Resources.Resource.Quotes %></strong>
								</a>
							</h6>
							<div class="heading-elements">
								<ul class="icons-list">
					                <li><a data-action="collapse"></a></li>
					            </ul>
				            </div>
						</div>

						<div class="panel-body">
                            <asp:Label runat="server" ID="lblNoQuotes" Visible="false">
                                <%= Resources.Resource.NoQuotesYet %>
                            </asp:Label>
                            <asp:GridView runat="server" ID="gridQuotes" AutoGenerateColumns="False" EnableViewState="false">
                                    <Columns>
                                        <asp:HyperLinkField DataTextField="internalId" HeaderText="Number" ItemStyle-CssClass="text-nowrap" DataNavigateUrlFields="id" DataNavigateUrlFormatString="../../Projects/Quotes/Job.aspx?id={0}"/>
                                        <asp:BoundField DataField="clientName" HeaderText="Customer"  />
                                        <asp:BoundField DataField="description" HeaderText="Description"  />
                                        <asp:BoundField DataField="total" HeaderText="Amount" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right" DataFormatString="{0:F2}" />
                                        <asp:TemplateField HeaderText="Confidence" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center text-nowrap">
                                            <ItemTemplate>
                                                <rv:RatingViewer ID="rv" Value='<%# Eval("rating") %>'  runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <psl:ProjectStatusLabel ID="psl" runat="server" ProjectStatus='<%# Bind("ProjectStatus") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Date" HeaderText="Date"  />
                                    </Columns>
                                </asp:GridView>
                        </div>

                        <div class="panel-footer text-right no-padding">
							<div class="row" style="margin:10px">
							    <a href="../../Projects/Quotes/Job.aspx?cust=<%=ph.CurrentId %>" class="btn btn-default"><i class="icon-plus2 position-left"></i> 
                                    <%= Resources.Resource.Create %>
                                    <%= Resources.Resource.Quote %>
							    </a>
							</div>
						</div>
					</div>
							
				</div>	
                </asp:Panel>

				  <asp:Panel ID="Panel1" runat="server">
                <div class="row" id="deliveryNotes">
                    <a name="quotes"></a>
					<div class="panel panel-white">
						<div class="panel-heading">
							<h6 class="panel-title">
								<a name="jobs">
									<i class="icon-box display-inline-block text-green"></i>
									<strong><%= Resources.Resource.DeliveryNotes %></strong>
								</a>
							</h6>
							<div class="heading-elements">
								<ul class="icons-list">
					                <li><a data-action="collapse"></a></li>
					            </ul>
				            </div>
						</div>

						<div class="panel-body">
                            <asp:Label runat="server" ID="lblNoNotes" Visible="false">
                                <%= Resources.Resource.NoData %>
                            </asp:Label>
                            <asp:GridView runat="server" ID="gridDeliveryNotes" AutoGenerateColumns="False" EnableViewState="false">
                                    <Columns>
                                        <asp:HyperLinkField DataTextField="internalId" HeaderText="Number" ItemStyle-CssClass="text-nowrap" DataNavigateUrlFields="id" DataNavigateUrlFormatString="../../Projects/Quotes/Job.aspx?id={0}"/>
                                        <asp:BoundField DataField="clientName" HeaderText="Customer"  />
                                        <asp:BoundField DataField="description" HeaderText="Description"  />
                                        <asp:BoundField DataField="total" HeaderText="Amount" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right" DataFormatString="{0:F2}" />
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <psl:ProjectStatusLabel ID="psl" runat="server" ProjectStatus='<%# Bind("ProjectStatus") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Date" HeaderText="Date"  />
                                    </Columns>
                                </asp:GridView>
                        </div>

                        <div class="panel-footer text-right no-padding">
							<div class="row" style="margin:10px">
							    <a href="../../Projects/DeliveryNotes/Job.aspx?cust=<%=ph.CurrentId %>" class="btn btn-default"><i class="icon-plus2 position-left"></i> 
                                    <%= Resources.Resource.Add_new %>
							    </a>
							</div>
						</div>
					</div>
							
				</div>	
                </asp:Panel>


                <div class="row" id="jobs">
                    <a name="jobs"></a>
					<div class="panel panel-white">
						<div class="panel-heading">
							<h6 class="panel-title">
								<a name="jobs">
									<i class="icon-cog5 display-inline-block text-green"></i>
									<%= Resources.Resource.Jobs %>
								</a>
							</h6>
							<div class="heading-elements">
								<ul class="icons-list">
					                <li><a data-action="collapse"></a></li>
					            </ul>
				            </div>
						</div>

						<div class="panel-body">
                            <asp:Label runat="server" ID="lblNoJobs" Visible="false">
                                <%= Resources.Resource.NoQuotesYet %>
                            </asp:Label>
                            <asp:GridView runat="server" ID="gridJobs" AutoGenerateColumns="False" EnableViewState="false">
                                    <Columns>
                                        <asp:HyperLinkField DataTextField="internalId" HeaderText="Number" ItemStyle-CssClass="text-nowrap" DataNavigateUrlFields="id" DataNavigateUrlFormatString="../../Projects/Opens/Job.aspx?id={0}"/>
                                        <asp:BoundField DataField="clientName" HeaderText="Customer"  />
                                        <asp:BoundField DataField="description" HeaderText="Description"  />
                                        <asp:TemplateField HeaderText="Visits" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center full-td">
                                            <ItemTemplate>
                                                <pvl:ProjectVisitsLabel ID="pvl" runat="server" DoneVisits='<%# Bind("DoneVisits") %>' PlannedVisits='<%# Bind("plannedVisits") %>' ContractualUnitsCount='<%# Bind("contractualUnitsCount") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Next_Visit"  HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center full-td">
                                            <ItemTemplate>
                                                <pnvl:ProjectNextVisitLabel ID="pnvl" runat="server" NextVisitDate='<%# Bind("nextVisitDate") %>' NextVisitContacts='<%# Bind("nextVisitContacts") %>' NextVisitMissingRessource='<%# Bind("NextVisitMissingRessource") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remaining_Visits"  HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <rvl:RemainingVisitsLabel ID="pnvl" runat="server" RemainingVisits='<%# Bind("remainingVisits") %>'  />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <psl:ProjectStatusLabel ID="psl" runat="server" ProjectStatus='<%# Bind("ProjectStatus") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Date" HeaderText="Date"  />

                                    </Columns>
                                </asp:GridView>
                        </div>

                        <div class="panel-footer text-right no-padding" runat="server" id="PanelNewJob">
							<div class="row" style="margin:10px">
							    <a href="../../Projects/Opens/Job.aspx?cust=<%=ph.CurrentId %>" class="btn btn-default"><i class="icon-plus2 position-left"></i> 
                                    <%= Resources.Resource.Start_New %>
                                    <%= Resources.Resource.Job %>
							    </a>
							</div>
						</div>
					</div>
							
				</div>	

                <asp:Panel runat="server" ID="PanelInvoices">
                <div class="row" id="invoices">
                    <a name="invoices"></a>
					<div class="panel panel-white">
						<div class="panel-heading">
							<h6 class="panel-title">
								<a name="jobs">
									<i class="icon-file-check2 display-inline-block text-green"></i>
									<strong><%= Resources.Resource.Invoices %></strong>
								</a>
							</h6>
							<div class="heading-elements">
								<ul class="icons-list">
					                <li><a data-action="collapse"></a></li>
					            </ul>
				            </div>
						</div>

						<div class="panel-body">
                            <asp:Label runat="server" ID="lblNoInvoices" Visible="false">
                                <%= Resources.Resource.NoInvoicesYet %>
                            </asp:Label>
                            <asp:GridView runat="server" ID="gridInvoices" AutoGenerateColumns="False" EnableViewState="false">
                                    <Columns>
                                        <asp:HyperLinkField DataTextField="internalId" HeaderText="Number" ItemStyle-CssClass="text-nowrap" DataNavigateUrlFields="id" DataNavigateUrlFormatString="../../Projects/Invoices/Job.aspx?id={0}"/>
                                        <asp:BoundField DataField="clientName" HeaderText="Customer"  />
                                        <asp:BoundField DataField="description" HeaderText="Description"  />
                                        <asp:BoundField DataField="dateInvoicing" HeaderText="Date" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right" DataFormatString="{0:dd.MM.yy}" />
                                        <asp:BoundField DataField="total" HeaderText="Total" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right" DataFormatString="{0:F2}" />
                                        <asp:BoundField DataField="dateInvoiceDue" HeaderText="To_pay_before" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right" DataFormatString="{0:dd.MM.yy}" />
                                        <asp:BoundField DataField="invoiceDue" HeaderText="Remaining" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right" DataFormatString="{0:F2}" />
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <psl:ProjectStatusLabel ID="psl" runat="server" ProjectStatus='<%# Bind("ProjectStatus") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Date" HeaderText="Date"  />

                                    </Columns>
                                </asp:GridView>
                        </div>

                        <div class="panel-footer text-right no-padding">
							<div class="row" style="margin:10px">
							    <a href="../../Projects/Invoices/Job.aspx?cust=<%=ph.CurrentId %>" class="btn btn-default"><i class="icon-plus2 position-left"></i> 
                                    <%=Resources.Resource.Create %>
                                    <%=Resources.Resource.Invoice %>
							    </a>
							</div>
						</div>
					</div>
							
				</div>		
                </asp:Panel>
        </div>


            </div>

    </form>
    <script type="text/javascript" src="Details.js?<%= Guid.NewGuid().ToString("n") %>"></script>
</asp:Content>

