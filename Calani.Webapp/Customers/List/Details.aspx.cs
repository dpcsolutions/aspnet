﻿using Calani.BusinessObjects.CustomerAdmin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Customers_List_Details : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    public string OptionsPeriod { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (ph.CurrentId == null) Response.Redirect(".");

        var orgMgr = new OrganizationManager(ph.CurrentOrganizationId);
        orgMgr.Load();

        OptionsPeriod = ph.RenderPeriodsListOption(preselect: orgMgr.DefaultPeriodFilter);

        Calani.BusinessObjects.CustomerAdmin.ClientsManager mgr = new Calani.BusinessObjects.CustomerAdmin.ClientsManager(ph.CurrentOrganizationId);
        var customer = mgr.Get(ph.CurrentId.Value);
        var mainContact = mgr.GetClientMainContact(customer.id);


        this.Title = Resources.Resource.Customer_board + ": " + customer.companyName;
        lblCompanyName.InnerText = customer.companyName;
        linkEdit.NavigateUrl = "Edit.aspx?id=" + ph.CurrentId;

        if(mainContact != null)
        {
            lblMainContactName.InnerText = mainContact.firstName;
            lblMainContactName.InnerText += " ";
            lblMainContactName.InnerText = mainContact.lastName;
            lblMainContactName.InnerText = lblMainContactName.InnerText.Trim();

            if(!String.IsNullOrWhiteSpace(mainContact.primaryPhone))
                lblMainContactPhone.InnerText = mainContact.primaryPhone;
            else if (!String.IsNullOrWhiteSpace(mainContact.secondaryPhone))
                lblMainContactPhone.InnerText = mainContact.secondaryPhone;



            lblMainContactEmail.InnerText = mainContact.primaryEmail;
        }

        Calani.BusinessObjects.Projects.ProjectsManager pmgr = new Calani.BusinessObjects.Projects.ProjectsManager(ph.CurrentOrganizationId);
        if (PageHelper.GetUserType(Session) == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            pmgr.EmployeeFilter = PageHelper.GetUserId(Session);
        }

        var quotes = pmgr.ListQuotes(customer.id);

        var notes = pmgr.ListDeliveryNotes(customer.id);

        var jobs = pmgr.ListOpens(customer.id);

        var invoices = pmgr.ListInvoices(ph.CurrentOrganizationId, customer.id);
       
        var nextVisits = (from r in jobs where r.nextVisitDate != null select r).ToList();

        if(nextVisits.Count > 0)
        {
            
            linkNextVisit.Text = nextVisits.Count + " "+Resources.Resource.Planned_Visits+".";

            var next = (from r in jobs where r.nextVisitDate != null orderby r.nextVisitDate ascending select r).FirstOrDefault();
            if(next != null)
            {
                linkNextVisit.Text += "<br />"+Resources.Resource.Next2 + ": " + next.nextVisitDate.Value.ToString("dd.MM.yy - HH:mm") + ".";
                linkNextVisit.NavigateUrl = "../../Projects/Opens/Job.aspx?id=19";
            }

            linkNextVisit.Enabled = true;
            linkNextVisit.CssClass = "";
        }
        else
        {
            linkNextVisit.Enabled = false;
            linkNextVisit.Text = Resources.Resource.No_Visits_Scheduled;
            linkNextVisit.CssClass = "text-muted";
        }


        List<string> rowData = new List<string>() { "lastUpdate" };

        gridQuotes.DataSource = quotes;
        new Html5GridView(gridQuotes, false, rowData, "6;desc");
        lblNoQuotes.Visible = quotes.Count == 0;


        gridDeliveryNotes.DataSource = notes;
        new Html5GridView(gridDeliveryNotes, false, rowData, "5;desc");
        lblNoNotes.Visible = notes.Count == 0;

        gridJobs.DataSource = jobs;
        new Html5GridView(gridJobs, false, rowData, "7;desc");
        lblNoJobs.Visible = jobs.Count == 0;


        gridInvoices.DataSource = invoices;
        new Html5GridView(gridInvoices, false, rowData, "8;desc");
        lblNoInvoices.Visible = invoices.Count == 0;


        // remove rights for employee
        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            PanelInvoices.Visible = false;
            PanelQuotes.Visible = false;
            blocCountQuotes.Visible = false;
            blocCountInvoices.Visible = false;
            PanelNewJob.Visible = false;
            qmAddQuote.Visible = false;
            qmAddJob.Visible = false;
            qmAddInvoice.Visible = false;

        }
        // -->
    }
}