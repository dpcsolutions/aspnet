﻿var _periodFilterFrom = null;
var _periodFilterTo = null;

var _dataTableQuotes = null;
var _dataTableNotes = null;
var _dataTableJobs = null;
var _dataTableInvoices = null;

function setPeriodFilter() {
    _periodFilterFrom = null;
    _periodFilterTo = null;

    var pval = $('#ddlPeriod').val();
    if (pval != null && pval.indexOf(';') > 0) {
        pval = pval.split(';');
        if (pval.length == 2) {
            _periodFilterFrom = pval[0];
            _periodFilterTo = pval[1];

            if (_periodFilterFrom != "*") _periodFilterFrom = parseInt(_periodFilterFrom);
            else _periodFilterFrom = null;

            if (_periodFilterTo != "*") _periodFilterTo = parseInt(_periodFilterTo);
            else _periodFilterTo = null;
        }
    }

    refreshLines();
}


function refreshLines() {
    _dataTableQuotes.draw();
    _dataTableNotes.draw();
    _dataTableJobs.draw();
    _dataTableInvoices.draw();
}

function refreshLiveStats() {



    var countQuotes = 0;
    var countNotes = 0;
    var countJobs = 0;
    var countInvoices = 0;



    var rowsId = _dataTableQuotes.rows().eq(0);
    if (rowsId != null) {
        for (var i = 0; i < rowsId.length; i++) {
            var row = _dataTableQuotes.row(rowsId[i]);

            if (row.node().attributes["data-anystatusfilter"] != null && row.node().attributes["data-anystatusfilter"].value == "true") {
                countQuotes++;
            }
        }
    }

    rowsId = _dataTableNotes.rows().eq(0);
    if (rowsId != null) {
        for (var i = 0; i < rowsId.length; i++) {
            var row = _dataTableNotes.row(rowsId[i]);

            if (row.node().attributes["data-anystatusfilter"] != null && row.node().attributes["data-anystatusfilter"].value == "true") {
                countNotes++;
            }
        }
    }

    rowsId = _dataTableJobs.rows().eq(0);
    if (rowsId != null) {
        for (var i = 0; i < rowsId.length; i++) {
            var row = _dataTableJobs.row(rowsId[i]);

            if (row.node().attributes["data-anystatusfilter"] != null && row.node().attributes["data-anystatusfilter"].value == "true") {
                countJobs++;
            }
        }
    }

    rowsId = _dataTableInvoices.rows().eq(0);
    if (rowsId != null) {

        for (var i = 0; i < rowsId.length; i++) {
            var row = _dataTableInvoices.row(rowsId[i]);

            if (row.node().attributes["data-anystatusfilter"] != null && row.node().attributes["data-anystatusfilter"].value == "true") {
                countInvoices++;
            }
        }
    }



    $('#lblCountQuotes').html(countQuotes);
    $('#lblCountNotes').html(countNotes);
    $('#lblCountJobs').html(countJobs);
    $('#lblCountInvoices').html(countInvoices);


}

$(function () {

    // client side filters
    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {

            var visible = true;


            var lastUpdate = parseInt(settings.aoData[dataIndex].nTr.getAttribute('data-lastupdate'));
            if (_periodFilterFrom != null) {
                if (lastUpdate < _periodFilterFrom) visible = false;
            }
            if (_periodFilterTo != null) {
                if (lastUpdate >= _periodFilterTo) visible = false;
            }

            settings.aoData[dataIndex].nTr.setAttribute('data-anystatusFilter', visible);

            return visible;
        }
    );
    // -->

    _dataTableQuotes = initDataTable($('#ContentPlaceHolder1_gridQuotes'), true);
    _dataTableNotes = initDataTable($('#ContentPlaceHolder1_gridDeliveryNotes'), true);
    _dataTableJobs = initDataTable($('#ContentPlaceHolder1_gridJobs'), true);
    _dataTableInvoices = initDataTable($('#ContentPlaceHolder1_gridInvoices'), true);

    _dataTableQuotes.on('draw', function () {
        refreshLiveStats();
    });

    _dataTableNotes.on('draw', function () {
        refreshLiveStats();
    });
    
    _dataTableJobs.on('draw', function () {
        refreshLiveStats();
    });
    _dataTableInvoices.on('draw', function () {
        refreshLiveStats();
    });


    setPeriodFilter();

    // scroll to anchor smooth
    $('a[href*=#]').click(function (event) {
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
        event.preventDefault();
    });
    // -->


    
});