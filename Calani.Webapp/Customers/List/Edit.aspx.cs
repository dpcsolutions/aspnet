﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Customers_List_Edit : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }


    public long CustomerId
    {
        get
        {
            if(ph.CurrentId != null)
                return ph.CurrentId.Value;

            return long.MaxValue;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ph.ItemLabel = Resources.Resource.Customer;
        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
            btnDelete, btnMore, btnMoreDdl, lblTitle, linkBackList, linkNew);

        if (!IsPostBack && ph.CurrentId != null)
        {
            Load();
            ph.SetStateEdit(Resources.Resource.Edit_var_id);
            divAddresses.Visible = true;
            divContacts.Visible = true;
        }

        if (ph.CurrentId == null)
        {
            //ph.SetStateNew(Resources.Resource.Add_new_var);
            //divAddresses.Visible = false;
            panelEdit.Visible = false;
            divAddresses.Visible = true;
            divContacts.Visible = true;
            Response.Redirect(".");
            // disable create


        }


        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            tbxName.Disabled = true;
            tbxVatNumber.Disabled = true;
            tbxVatNumber.Disabled = true;
            panelActions.Visible = false;
            quickmenu.Visible = false;
            panelAddAddress.Visible = false;
            panelAddContact.Visible = false;

        }
    }



    #region CRUD operations
    // Load from db and bind controls
    private void Load()
    {
        Calani.BusinessObjects.CustomerAdmin.ClientsManager mgr = new Calani.BusinessObjects.CustomerAdmin.ClientsManager(ph.CurrentOrganizationId);
        var item = mgr.Get(ph.CurrentId.Value);

        tbxName.Value = item.companyName;
        tbxVatNumber.Value = item.vatNumber;
        if (item.isCompany == true) ddlType.SelectedValue = "1";
        else ddlType.SelectedValue = "0";

        Calani.BusinessObjects.Contacts.ClientAddressesManager adrmgr = new Calani.BusinessObjects.Contacts.ClientAddressesManager(ph.CurrentOrganizationId, item.id);
        var addresses = adrmgr.List();
        foreach (var a in addresses)
        {
            if (String.IsNullOrEmpty(a.caption)) a.caption = a.city;
            if (String.IsNullOrEmpty(a.caption)) a.caption = "(Default)";

        }
        gridAddresses.DataSource = addresses;
        gridAddresses.DataBind();
        

        new Html5GridView(gridAddresses);

        Calani.BusinessObjects.Contacts.ClientContactsManager ctcmgr = new Calani.BusinessObjects.Contacts.ClientContactsManager(ph.CurrentOrganizationId, item.id);
        var contacts = ctcmgr.List();
        foreach (var contact in contacts)
        {
            if (String.IsNullOrWhiteSpace(contact.primaryPhone)) contact.primaryPhone = contact.secondaryPhone;
        }
        gridContacts.DataSource = contacts;
        gridContacts.DataBind();
        new Html5GridView(gridContacts);

        linkAddAddress.HRef = "Address.aspx?c=" + item.id;
        linkAddAddress2.HRef = "Address.aspx?c=" + item.id;
        linkAddContact.HRef = "Contact.aspx?c=" + item.id;
        linkAddContact2.HRef = "Contact.aspx?c=" + item.id;

        Calani.BusinessObjects.Projects.ProjectsManager prjMgr = new Calani.BusinessObjects.Projects.ProjectsManager(ph.CurrentOrganizationId);

        if (ph.CurrentId != null)
        {
            lblCountInvoices.Text = prjMgr.CountCustomerInvoices(ph.CurrentId.Value).ToString();
            lblCountQuotes.Text = prjMgr.CountCustomerQuotes(ph.CurrentId.Value).ToString();
            lblCountProjects.Text = prjMgr.CountCustomerOpenProjets(ph.CurrentId.Value).ToString();
        }
    }

    // Create a new db record from controls values
    private void Update()
    {
        Calani.BusinessObjects.CustomerAdmin.ClientsManager mgr = new Calani.BusinessObjects.CustomerAdmin.ClientsManager(ph.CurrentOrganizationId);

        var ret = mgr.Update(ph.CurrentId.Value,
                        new Calani.BusinessObjects.Model.clients
                        {
                            companyName = tbxName.Value,
                            vatNumber = tbxVatNumber.Value,
                            organizationId = ph.CurrentOrganizationId,
                            isCompany = ddlType.SelectedValue == "1"
                        });
        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Updated_var);
            Load();
        }
        else ph.DisplayError(ret.ErrorMessage);
    }




    protected void Delete()
    {
        Calani.BusinessObjects.CustomerAdmin.ClientsManager mgr = new Calani.BusinessObjects.CustomerAdmin.ClientsManager(ph.CurrentOrganizationId);
        var c = mgr.Get(ph.CurrentId.Value);
        var addresses = c.addresses;
        var contacts = c.contacts;

        var ret = mgr.Delete(ph.CurrentId.Value);

        Calani.BusinessObjects.Contacts.ClientContactsManager ctmgr = new Calani.BusinessObjects.Contacts.ClientContactsManager(ph.CurrentOrganizationId, c.id);
        foreach (var contact in contacts)
        {
            ctmgr.Delete(contact.id);
        }
        Calani.BusinessObjects.Contacts.ClientAddressesManager admgr = new Calani.BusinessObjects.Contacts.ClientAddressesManager(ph.CurrentOrganizationId, c.id);
        foreach (var address in addresses)
        {
            admgr.Delete(address.id);
        }
        

       

        if (ret.Success)
        {
            panelEdit.Visible = false;
            divAddresses.Visible = false;
            divContacts.Visible = false;
            ph.DisplaySuccess(Resources.Resource.Deleted_var);
        }
    }
    #endregion



    #region Buttons (triggers)
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) Update();

    }


    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) Delete();
    }
    #endregion





}