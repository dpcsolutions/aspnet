﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Customers_List_Address : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    long customerId;


    protected void Page_Load(object sender, EventArgs e)
    {
        customerId = Convert.ToInt64(Request.Params["c"]);


        Calani.BusinessObjects.CustomerAdmin.ClientsManager cmgr = new Calani.BusinessObjects.CustomerAdmin.ClientsManager(ph.CurrentOrganizationId);
        string custName = cmgr.Get(customerId).companyName;

        ph.ItemLabel = Resources.Resource.Address;
        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
            btnDelete, btnMore, btnMoreDdl, lblTitle, linkBackList, linkNew,
            custName);

        if (!IsPostBack && ph.CurrentId != null)
        {
            Load();
            ph.SetStateEdit(Resources.Resource.Edit_var_id);
        }

        if (ph.CurrentId == null)
        {
            ph.SetStateNew(Resources.Resource.Add_new_var);
        }


        linkBackList.InnerText = Resources.Resource.BackToCustomerDetails;
        linkBackList.HRef = "Edit.aspx?id=" + customerId;
        linkNew.HRef = "Address.aspx?c=" + customerId;


        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            panelAction.Visible = false;
            tbxAdr2ndLine.Disabled = true;
            tbxAdrCityName.Disabled = true;
            tbxAdrCountry.Disabled = true;
            tbxAdrPostalCode.Disabled = true;
            tbxAdrState.Disabled = true;
            tbxAdrStreetName.Disabled = true;
            tbxAdrStreetNr.Disabled = true;
            tbxPostalBox.Disabled = true;

        }
    }


    #region CRUD operations
    // Load from db and bind controls
    private void Load()
    {
        Calani.BusinessObjects.Contacts.ClientAddressesManager mgr = new Calani.BusinessObjects.Contacts.ClientAddressesManager(ph.CurrentOrganizationId, customerId);
        var item = mgr.Get(ph.CurrentId.Value);

        tbxAdrStreetName.Value = item.street;
        tbxAdrStreetNr.Value = item.nb;
        tbxAdr2ndLine.Value = item.street2;
        tbxAdrPostalCode.Value = item.npa;
        tbxAdrCityName.Value = item.city;
        tbxAdrState.Value = item.state;
        tbxAdrCountry.Value = item.country;
        tbxAdrPostalCode.Value = item.postbox;
        tbxCaption.Value = item.caption;

    }

    // Create a new db record from controls values
    private void Update()
    {
        Calani.BusinessObjects.Contacts.ClientAddressesManager mgr = new Calani.BusinessObjects.Contacts.ClientAddressesManager(ph.CurrentOrganizationId, customerId);

        var ret = mgr.Update(ph.CurrentId.Value,
                        new Calani.BusinessObjects.Model.addresses
                        {
                            street = tbxAdrStreetName.Value,
                            nb = tbxAdrStreetNr.Value,
                            street2 = tbxAdr2ndLine.Value,
                            npa = tbxAdrPostalCode.Value,
                            city = tbxAdrCityName.Value,
                            state = tbxAdrState.Value,
                            country = tbxAdrCountry.Value,
                            postbox=tbxAdrPostalCode.Value,
                            caption=tbxCaption.Value,

                            clientId = customerId,
                            organizationId = ph.CurrentOrganizationId
                        });
        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Updated_var);
            Load();
        }
        else ph.DisplayError(ret.ErrorMessage);
    }


    // Update existing db record from new controls values
    private void Create()
    {
        Calani.BusinessObjects.Contacts.ClientAddressesManager mgr = new Calani.BusinessObjects.Contacts.ClientAddressesManager(ph.CurrentOrganizationId, customerId);

        var ret = mgr.Add(new Calani.BusinessObjects.Model.addresses
        {
            street = tbxAdrStreetName.Value,
            nb = tbxAdrStreetNr.Value,
            street2 = tbxAdr2ndLine.Value,
            npa = tbxAdrPostalCode.Value,
            city = tbxAdrCityName.Value,
            state = tbxAdrState.Value,
            country = tbxAdrCountry.Value,
            postbox = tbxAdrPostalCode.Value,
            caption=tbxCaption.Value,

            clientId = customerId,
            organizationId = ph.CurrentOrganizationId
        });
        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Created_var);
            btnSave.Enabled = false;
        }
        else ph.DisplayError(ret.ErrorMessage);
    }

    protected void Delete()
    {
        Calani.BusinessObjects.Contacts.ClientAddressesManager mgr = new Calani.BusinessObjects.Contacts.ClientAddressesManager(ph.CurrentOrganizationId, customerId);
        var ret = mgr.Delete(ph.CurrentId.Value);
        if (ret.Success)
        {
            panelEdit.Visible = false;
            ph.DisplaySuccess(Resources.Resource.Deleted_var);
        }
    }
    #endregion



    #region Buttons (triggers)
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) Update();
        else Create();
    }


    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) Delete();
    }
    #endregion





}