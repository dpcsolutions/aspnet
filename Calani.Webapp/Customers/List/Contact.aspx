﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Customers_List_Contact" Codebehind="Contact.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <form runat="server">

        <div class="content">

		    <div class="row">

                <!-- left column -->
			    <div class="col-sm-12 col-md-6">

                        <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Error %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblErrorMessage" />
						    </div>
					    </asp:Panel><!-- panelError -->

                        <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Done %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblSuccess" />
                                <br />
                                <strong><a href="." id="linkBackList" runat="server"></a></strong>
                                    <%= Resources.Resource.or %>
                                <a href="Edit.aspx" id="linkNew" runat="server"></a>.
						    </div>
					    </asp:Panel><!-- panelSuccess -->

					    
					    <asp:Panel runat="server" ID="panelEdit" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><asp:Label runat="server" ID="lblTitle" /></h6>
						    </div>


						    <div class="panel-body">

							    <div class="form-horizontal">

                                    <fieldset class="content-group">

                                        <!-- title -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Title %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                                    <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlTitle">
                                                        <asp:ListItem Value="Mr" Text='<%$ Resources:Resource, Title_Mr %>' />
                                                        <asp:ListItem Value="Mrs" Text='<%$ Resources:Resource, Title_Mrs %>' />
                                                        <asp:ListItem Value="Miss" Text='<%$ Resources:Resource, Title_Miss %>' />
                                                        <asp:ListItem Value="Professor" Text='<%$ Resources:Resource, Title_Professor %>' />
                                                        <asp:ListItem Value="Dr" Text='<%$ Resources:Resource, Title_Dr %>' />
                                                    </asp:DropDownList>
											    </div>
										    </div>
									    </div>
                                        <!-- /title -->



                                        <!-- name -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Lastname %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-font"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Lastname %>' runat="server" id="tbxLastName" />
											    </div>
										    </div>
									    </div>
                                        <!-- /name -->

                                        <!-- name -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Firstname %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-font"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Firstname %>' runat="server" id="tbxFirstName" />
											    </div>
										    </div>
									    </div>
                                        <!-- /name -->


										
                                        <!-- title -->
									    <div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.UserTitle %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-collaboration"></i></span>
													<input type="text" class="form-control" placeholder='<%$ Resources:Resource, UserTitle_Tooltip %>' runat="server" id="tbxUsrTitle">
												</div>
											</div>
										</div>
                                        <!-- /internalId -->
												


								    </fieldset>



                                    <fieldset class="content-group">
										<legend class="text-bold"><%= Resources.Resource.Contact %></legend>

                                        <!-- email -->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.Email %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-envelope"></i></span>
													<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Email %>' runat="server" id="tbxEmail">
												</div>
											</div>
										</div>
                                        <!-- /email -->

                                        
                                        <!-- email2 -->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.AltEmail %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-envelope"></i></span>
													<input type="text" class="form-control" placeholder='<%$ Resources:Resource, AltEmail_Tooltip %>'  runat="server" id="tbxEmail2">
												</div>
											</div>
										</div>
                                        <!-- /email2 -->


                                        <!-- phone1 -->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.Phone %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-phone2"></i></span>
													<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Phone %>' runat="server" id="tbxPhone1">
												</div>
											</div>
										</div>
                                        <!-- /phone1 -->

                                        <!-- phone2 -->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.MobilePhone %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-iphone"></i></span>
													<input type="text" class="form-control" placeholder='<%$ Resources:Resource, MobilePhone %>' runat="server" id="tbxPhone2">
												</div>
											</div>
										</div>
                                        <!-- /phone2 -->

                                        <!-- telecopy -->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.Telecopy %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-fax"></i></span>
													<input type="text" class="form-control" placeholder='<%$ Resources:Resource, Telecopy %>' runat="server" id="tbxTelecopy">
												</div>
											</div>
										</div>
                                        <!-- /telecopy -->

                                    </fieldset>



                                    <fieldset class="content-group">
										<legend class="text-bold"><%= Resources.Resource.Preferences %></legend>
                                         <!-- main -->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.MainContact_label %></label>
											<div class="col-lg-10">
												<div class="input-group">
												    <span class="input-group-addon">
                                                        <asp:CheckBox runat="server" CssClass="styled" ID="cbxMain" />
												    </span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, MainContact_tooltip %>' disabled="disabled" runat="server" id="cbxMaintt">
											    </div>
											</div>
										</div>
                                        <!-- /main -->
                                    </fieldset>

											



                                    <!-- buttonsaction -->
								    <div class="buttonsaction text-right" runat="server" id="panelActions">
										<label class="control-label col-lg-2"></label>
										<div class="col-lg-10">
											<div class="btn-group">
                                                <asp:LinkButton runat="server" ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click">
                                                    <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                                </asp:LinkButton>
                                                
									            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore"><span class="caret"></span></button>
									            <ul class="dropdown-menu dropdown-menu-right" runat="server" id="btnMoreDdl">
                                                    <li><a href="javascript:void(0)" data-toggle="modal" data-target="#modal_default"><i class="fa fa-trash"></i> <%= Resources.Resource.Delete %></a></li>
												</ul>
											</div>
										</div>
									</div>
                                    <!-- /buttonsaction -->


							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->
					    


                        


			    </div>
			    <!-- /left column -->
                

                <!-- right column -->
                <div class="col-sm-12 col-md-6">

			    </div>
                <!-- /right column -->
						

		    </div> <!-- /row -->

					
            <!-- basic modal: confirm delete -->
		    <div id="modal_default" class="modal fade">
			    <div class="modal-dialog">
				    <div class="modal-content">
					    <div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal">&times;</button>
						    <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
					    </div>

					    <div class="modal-body">
						    <h6 class="text-semibold"><%= Resources.Resource.Are_You_Sure_To_Delete %></h6>
						    <p><%= Resources.Resource.Delete_Is_Irreversible %></p>

					    </div>

					    <div class="modal-footer">
						    <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
                            <asp:Button runat="server" ID="btnDelete" OnClick="btnDelete_Click" CssClass="btn btn-primary" Text="<%$ Resources:Resource, Yes_Delete %>" />

					    </div>
				    </div>
			    </div>
		    </div>
		    <!-- /basic modal -->



        </div><!-- /content -->

    </form>

</asp:Content>

