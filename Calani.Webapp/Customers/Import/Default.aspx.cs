﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.Imports;

public partial class Customers_Import_Default : System.Web.UI.Page
{
    public PageHelper ph;
    SheetFileContent cs;

    protected void Page_Load(object sender, EventArgs e)
    {
        ph = new PageHelper(this);

        if (!IsPostBack) DeleteOldFiles();

        if (ph.CurrentUserType != Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
        {
            Response.Redirect("../List/");
            return;
        }
    }

    protected void btnUploadNext_Click(object sender, EventArgs e)
    {
        panelIntro.Visible = false;
        panelUpload.Visible = false;

        string path = System.IO.Path.Combine(Calani.BusinessObjects.Imports.FileImportManager.DIR,
            ph.CurrentOrganizationId.ToString(),
            hfFile.Value
            );


        panelSetupColumns.Visible = true;


        tbxColumnsToImport.Value = "";
        cs = Calani.BusinessObjects.Imports.FileImportManager.Load(path);
        System.Data.DataTable dt = ConvertToTable(cs);
        grid.DataSource = dt;
        grid.PreRender += Grid_PreRender;
        grid.RowCreated += Grid_RowCreated;
        grid.DataBind();

    }

    private void Grid_RowCreated(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.Header)
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                string colname = "";
                if (cs != null && cs.Columns != null && i < cs.Columns.Count) colname = cs.Columns[i];
                string html = "";
                html += "<div>";
                html += BuildSelect(i);
                html += "</div>";
                html += "<div style=\"white-space:nowrap; text-align:center;\"> (" + colname + ") </div>";
                e.Row.Cells[i].Text = html;
            }

        }
    }

    private string BuildSelect(int col)
    {
        StringBuilder sb = new StringBuilder();

        sb.AppendLine("<select class=\"form-control select ddlCol\" id=\"ddlCol" + col + "\">");


        sb.AppendLine("<option value=\"\">");
        sb.AppendLine("-");
        sb.AppendLine("</option>");


        sb.AppendLine("<option " + GetSelected(col, 0, "CustName") + " > ");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.Customer));
        sb.AppendLine("</option>");

        sb.AppendLine("<option " + GetSelected(col, 1, "IsCompany") + " > ");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.Company));
        sb.AppendLine("</option>");

        sb.AppendLine("<option " + GetSelected(col, 2, "LastName") + ">");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.Lastname));
        sb.AppendLine("</option>");

        sb.AppendLine("<option " + GetSelected(col, 3, "FirstName") + ">");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.Firstname));
        sb.AppendLine("</option>");

        sb.AppendLine("<option " + GetSelected(col, 4, "Title") + ">");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.Title));
        sb.AppendLine("</option>");

        sb.AppendLine("<option " + GetSelected(col, 5, "Email") + ">");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.Email));
        sb.AppendLine("</option>");

        sb.AppendLine("<option " + GetSelected(col, 6, "Email2") + ">");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.Email) + " 2");
        sb.AppendLine("</option>");

        sb.AppendLine("<option " + GetSelected(col, 7, "Phone") + " > ");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.Phone));
        sb.AppendLine("</option>");

        sb.AppendLine("<option " + GetSelected(col, 8, "Phone2") + ">");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.Phone) + " 2");
        sb.AppendLine("</option>");

        sb.AppendLine("<option " + GetSelected(col, 9, "Telecopy") + ">");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.Telecopy));
        sb.AppendLine("</option>");

        sb.AppendLine("<option " + GetSelected(col, 10, "Service") + ">");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.Caption));
        sb.AppendLine("</option>");

        sb.AppendLine("<option " + GetSelected(col, 11, "Street") + ">");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.Street));
        sb.AppendLine("</option>");

        sb.AppendLine("<option " + GetSelected(col, 12, "StreetNr") + " > ");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.Street_nr));
        sb.AppendLine("</option>");

        sb.AppendLine("<option " + GetSelected(col, 13, "Street2") + " > ");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.Street) + " 2");
        sb.AppendLine("</option>");

        sb.AppendLine("<option " + GetSelected(col, 14, "PostalBox") + " > ");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.PostalBox));
        sb.AppendLine("</option>");

        sb.AppendLine("<option " + GetSelected(col, 15, "Postalcode") + " > ");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.Postalcode));
        sb.AppendLine("</option>");

        sb.AppendLine("<option " + GetSelected(col, 16, "City") + " > ");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.Cityname));
        sb.AppendLine("</option>");

        sb.AppendLine("<option " + GetSelected(col, 17, "State") + " > ");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.State));
        sb.AppendLine("</option>");

        sb.AppendLine("<option " + GetSelected(col, 18, "Country") + ">");
        sb.AppendLine(HttpUtility.HtmlEncode(Resources.Resource.Country));
        sb.AppendLine("</option>");


        sb.AppendLine("</select>");

        return sb.ToString();

    }

    private string GetSelected(int col, int x, string value)
    {
        if (col == x)
        {

            tbxColumnsToImport.Value += value + ";";
            return "value=\"" + value + "\" selected =\"selected\"";
        }
        return "value=\"" + value + "\" ";
    }

    private void Grid_PreRender(object sender, EventArgs e)
    {

    }

    private DataTable ConvertToTable(SheetFileContent cs)
    {
        DataTable dt = new DataTable();

        foreach (var item in cs.Columns)
        {
            dt.Columns.Add(item);
        }

        int rows = cs.Rows.Count;
        if (rows > 5) rows = 5;

        for (int i = 0; i < rows; i++)
        {
            var r = cs.Rows[i];
            var dtr = dt.NewRow();
            for (int j = 0; j < cs.Columns.Count; j++)
            {
                dtr[j] = r.GetCell(j);
            }
            dt.Rows.Add(dtr);
        }

        return dt;
    }


    protected void btnSetColumns_Click(object sender, EventArgs e)
    {
        string path = System.IO.Path.Combine(Calani.BusinessObjects.Imports.FileImportManager.DIR,
            ph.CurrentOrganizationId.ToString(),
            hfFile.Value
            );
        List<string> columns = tbxColumnsToImport.Value.Split(';').ToList();

        List<Calani.BusinessObjects.CustomerAdmin.ImportedDataItem> imports = new List<Calani.BusinessObjects.CustomerAdmin.ImportedDataItem>();

        if(columns.Count > 1)
        {
            cs = Calani.BusinessObjects.Imports.FileImportManager.Load(path);
            Calani.BusinessObjects.CustomerAdmin.ClientsManager mgr = new Calani.BusinessObjects.CustomerAdmin.ClientsManager(ph.CurrentOrganizationId);
            imports = mgr.BatchImport(columns, cs.Rows);
        }

        panelSetupColumns.Visible = false;
        panelFinish.Visible = true;
        lblImportCount.Text = imports.Count.ToString();
        repeatedCustomers.DataSource = imports;
        repeatedCustomers.DataBind();

        try
        {
            System.IO.File.Delete(path);
            DeleteOldFiles();
        }
        catch { }


    }

    private void DeleteOldFiles()
    {
        System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(System.IO.Path.Combine(Calani.BusinessObjects.Imports.FileImportManager.DIR,
            ph.CurrentOrganizationId.ToString()));
        try
        {
            if (!di.Exists) di.Create();
        }
        catch { }
        foreach (var item in di.GetFiles("cust_*.xml"))
        {
            try
            {
                if (DateTime.Now.Subtract(item.CreationTime).TotalMinutes > 20)
                {
                    item.Delete();
                }
            }
            catch { }
        }

    }
}