﻿<%@ Page Title="Balances" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Customers_Balances_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<form runat="server">
    <div class="content">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
					<div class="panel-body text-center">
						<div class="icon-object border-grey text-grey"><i class="icon-bubble-dots3"></i></div>
						<h5 class="text-semibold"><%= Resources.Resource.Work_in_progress %></h5>
						<p class="mb-15"><%= Resources.Resource.Feature_soon_message %></p>
					</div>
				</div>
            </div>
        </div>
    </div>
</form>
</asp:Content>

