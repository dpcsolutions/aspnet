﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FirstLogin : System.Web.UI.Page
{
    public string LanguageSwitch { get; set; }

    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {


        LanguageSwitch = HtmlLanguageSwitch.Render(this.Page.Request.Url.AbsoluteUri, System.Threading.Thread.CurrentThread.CurrentCulture.ToString(), Page.ResolveClientUrl("~/assets/"));


        LabelCopyYear.Text = ConfigurationSettings.AppSettings["copyYear"];
        HyperLinkCopy.Text = ConfigurationSettings.AppSettings["copyrightText"];
        HyperLinkCopy.NavigateUrl = ConfigurationSettings.AppSettings["copyrightUrl"];

        this.Title = ConfigurationSettings.AppSettings["title"];

    }
}