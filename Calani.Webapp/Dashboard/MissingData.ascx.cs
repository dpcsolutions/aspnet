﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Dashboard_MissingData : System.Web.UI.UserControl
{
    PageHelper ph;
    protected void Page_Load(object sender, EventArgs e)
    {
        ph = new PageHelper(this.Page);

        Calani.BusinessObjects.Membership.UserVariableManager mgr = new Calani.BusinessObjects.Membership.UserVariableManager(ph.CurrentUserId);
        string data = mgr.Load("HideMissingDataTutorial", "0");
        if(data == "1")
        {
            this.Visible = false;
            return;
        }

        list.InnerHtml = "";
        long org = ph.CurrentOrganizationId;

        int problems = 0;
        Calani.BusinessObjects.Model.CalaniEntities db = new Calani.BusinessObjects.Model.CalaniEntities();

        // company id
        Calani.BusinessObjects.CustomerAdmin.OrganizationManager orgmgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(org);
        orgmgr.Load();
        if (String.IsNullOrWhiteSpace(orgmgr.AdrCity))
        {
            AddMessages(false,
                            Resources.Resource.WhereToStart_companyid_problem
                            .Replace("#link", Page.ResolveClientUrl("~/Settings/Identity/"))
                            );
            problems++;
        }
        else
        {
            AddMessages(true, Resources.Resource.WhereToStart_companyid_ok);
        }
        // -->

        // customers ?
        if ((from r in db.clients where r.organizationId == org && r.recordStatus >= 0 select r).Count() == 0)
        {
            AddMessages(false, 
                Resources.Resource.WhereToStart_customer_problem
                .Replace("#link", Page.ResolveClientUrl("~/Customers/List/New.aspx"))
                );
            problems++;
        }
        else
        {
            AddMessages(true, Resources.Resource.WhereToStart_customer_ok);
        }
        // -->

        // services ?
        int servicesTypeFilter = Convert.ToInt32(Calani.BusinessObjects.CustomerAdmin.ServiceType.BillableServices);
        if ((from r in db.services where r.organizationId == org && r.type == servicesTypeFilter && r.recordStatus >= 0 select r).Count() == 0)
        {
            AddMessages(false, 
                Resources.Resource.WhereToStart_service_problem
                .Replace("#link", Page.ResolveClientUrl("~/Ressources/Services/Edit.aspx"))
                );
            problems++;
        }
        else
        {
            AddMessages(true, Resources.Resource.WhereToStart_service_ok);
        }
        // -->


        
        
        // job
        if ((from r in db.jobs where r.organizationId == org && r.recordStatus >= 0 select r).Count() == 0)
        {
            AddMessages(false, 
                Resources.Resource.WhereToStart_job_problem
                .Replace("#link1", Page.ResolveClientUrl("~/Projects/Quotes/Job.aspx"))
                .Replace("#link2", Page.ResolveClientUrl("~/Projects/Opens/Job.aspx"))
                );
            problems++;

        }
        else
        {
            AddMessages(true, Resources.Resource.WhereToStart_job_ok);
        }
        // -->

        panelClose.Visible = (problems == 0);
        





        lnkDoc.HRef = System.Configuration.ConfigurationManager.AppSettings["DocumentationUrl"];
    }

    private void AddMessages(bool success, string label)
    {
        string html = "<li>\r\n";
        if (success)
        {
            html += "<i class=\"icon-checkmark-circle text-success position-left\"></i>\r\n";
        }
        else
        {
            html += "<i class=\"icon-radio-unchecked text-primary position-left\"></i>\r\n";
        }
        html += label;
        html += "\r\n";
        html += "</li>";
        list.InnerHtml += html;
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        this.Visible = false;
        Calani.BusinessObjects.Membership.UserVariableManager mgr = new Calani.BusinessObjects.Membership.UserVariableManager(ph.CurrentUserId);
        mgr.Save("HideMissingDataTutorial", "1");
    }
}