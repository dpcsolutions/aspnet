﻿<%@ Page Title="Dashboard" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Dashboard_Default" Codebehind="Default.aspx.cs" %>
<%@ Register Src="~/Dashboard/MissingData.ascx" TagPrefix="uc1" TagName="MissingData" %>
<%@ Register Src="~/assets/Controls/FirstLoginWizard.ascx" TagPrefix="uc1" TagName="FirstLoginWizard" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<form runat="server">
    

    <uc1:FirstLoginWizard runat="server" ID="FirstLoginWizard" />




    <asp:Panel runat="server" ID="panelEmail">
       <div class="content content content-no-padding">

            <div class="panel panel-body" style="background-color: #f5e9ce">
                <div>
	                <div class="media-left media-middle">
			                <i class="icon-envelop3 icon-3x text-muted no-edge-top"></i>
		                </div>

		                <div class="media-body">
			                <h6 class="media-heading text-semibold"><%= Resources.Resource.NotValidatedEmail %> <asp:Label runat="server" ID="lblEmail" /></h6>
			                <span class="text-muted">
                                <%= Resources.Resource.NotValidatedEmail_description %>
                                <asp:HyperLink runat="server" ID="linkEditUser" CssClass="text-brown"><%= Resources.Resource.ChangeEmail %>.</asp:HyperLink>

			                </span>
		                </div>

		                <div class="media-right media-middle">
			                <asp:LinkButton runat="server" CssClass="btn bg-brown" id="btnResendEmail" OnClick="btnResendEmail_Click">
                                <i class="glyphicon glyphicon-send"></i>
                                <%= Resources.Resource.ResendEmail %>
			                </asp:LinkButton>
		                </div>
	                </div>


            </div>
        </div>

    </asp:Panel>

    

    <div class="content" style="padding-bottom:0" id="divmobile" runat="server">
    <div class="panel panel-body" style="background:#dfd3c5">
            <div class="row">
                <div class="col-sm-4 col-md-4">
                    <h3 style="color:#894a53">
                        Gagnez aussi en mobilité
                        <br />
                        Utilisez <strong>GesMobile</strong> sur votre smartphone
                    </h3>
                </div>
                <div class="col-sm-4 col-md-4">
                    <h5 style="color:#894a53">
                    Gardez avec vous le strict nécessaire:
                    <br />- votre planning
                    <br />- vos activités
                    <br />- vos dépenses
                    </h5>
                </div>
                <div class="col-sm-4 col-md-4">
                    <h5 style="text-align:right">
                        <a href="https://itunes.apple.com/ch/app/gesmobile/id1397336205?l=fr&mt=8" target="_blank" class="btn btn-primary" style="background:#894a53; border-color:#894a53;">
                            <i class="icon-apple2"></i>
                            App Store
			            </a>
                    </h5>
                    <h5 style="text-align:right">
                        <a href="https://play.google.com/store/apps/details?id=ch.digitalpencorp.gesmobile" target="_blank" class="btn btn-primary" style="background:#894a53; border-color:#894a53;">
                            <i class="icon-android"></i>
                            Play Store
			            </a>
                    </h5>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        
        <div class="row">
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel1"></asp:Panel>
            </div>
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel2"></asp:Panel>
            </div>
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel3"></asp:Panel>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel4"></asp:Panel>
            </div>
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel5"></asp:Panel>
            </div>
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel6"></asp:Panel>
            </div>
            
        </div>
        <div class="row">
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel7"></asp:Panel>
            </div>
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel8"></asp:Panel>
            </div>
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel9"></asp:Panel>
            </div>
           
        </div>
        <div  class="row">
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel10"></asp:Panel>
            </div>
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel11"></asp:Panel>
            </div>
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel12"></asp:Panel>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel13"></asp:Panel>
            </div>
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel14"></asp:Panel>
            </div>
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel15"></asp:Panel>
            </div>
           
        </div>
        <div class="row">
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel16"></asp:Panel>
            </div>
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel17"></asp:Panel>
            </div>
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel18"></asp:Panel>
            </div>
          
        </div>
        <div class="row">
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel19"></asp:Panel>
            </div>
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel20"></asp:Panel>
            </div>
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel21"></asp:Panel>
            </div>
            
        </div>
        <div class="row">
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel22"></asp:Panel>
            </div>
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel23"></asp:Panel>
            </div>
            <div class="col-sm-4 col-md-4">
                <asp:Panel runat="server" ID="Panel24"></asp:Panel>
            </div>
        </div>


        <div class="row" style="margin-top:100px">

            <div class="col-sm-12 col-md-12">

                <p style="text-align:center">
                    <%= Resources.Resource.CanCustomizeDashIn %>:
                    <asp:HyperLink runat="server" NavigateUrl="~/Profile/Preferences/">
                        <%= Resources.Resource.PreferencesPage %>
                    </asp:HyperLink>
                </p>

                
            </div>
        </div>


        
        

    </div>

    
</form>
</asp:Content>

