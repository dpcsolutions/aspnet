﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Dashboard_Default : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = ph.CMP.Name;

        Calani.BusinessObjects.Membership.LoginManager lmgr = new Calani.BusinessObjects.Membership.LoginManager();
        panelEmail.Visible = lmgr.IsEmailValidated(ph.CurrentUserId) == false;
        if (panelEmail.Visible)
        {
            lblEmail.Text = lmgr.GetEmail(ph.CurrentUserId);
            linkEditUser.NavigateUrl = "~/Ressources/Employees/Edit.aspx?id=" + ph.CurrentUserId;
        }

        Calani.BusinessObjects.Membership.UserVariableManager vars = new Calani.BusinessObjects.Membership.UserVariableManager(ph.CurrentUserId);
        
        var widgets = "Customers;Projects;Planning;TimeSheet;Expenses;Quotes;Invoices";

        string dashboardWidgets = vars.Load("DashboardWidgets", widgets);

        List<Panel> panels = new List<Panel>
        {
            Panel1, Panel2, Panel3, Panel4, Panel5, Panel6, Panel7, Panel8, Panel9, Panel10,
            Panel11, Panel12, Panel13, Panel14, Panel15, Panel16, Panel17, Panel18, Panel19, Panel20,
            Panel21, Panel22, Panel23, Panel24
        };

        List<string> controls = dashboardWidgets.Split(';').ToList();

        int index = 0;

        foreach (string control in controls)
        {
            if (index < panels.Count)
            {

                string controlPath = "~/Dashboard/Widgets/" + control + ".ascx";

                if (System.IO.File.Exists(Server.MapPath(controlPath)))
                {
                    var ctrl = Page.LoadControl(controlPath);

                    string panelId = "Panel" + (index+1);


                    Panel panel = panels[index];

                    panel.Controls.Add(ctrl);
                    
                }

                index++;
            }
        }


        // hide mobile banner
        if (panelEmail.Visible)
        {
            // always, if email not validated
            divmobile.Visible = false;
        }
        else
        {
            // when mobile ever connected, hide it
            Calani.BusinessObjects.Model.CalaniEntities db = new Calani.BusinessObjects.Model.CalaniEntities();
            if ((from r in db.contacts
                 where r.organizationId == ph.CurrentOrganizationId
                && r.id == ph.CurrentUserId
                && !String.IsNullOrEmpty(r.notificationToken)
                 select r).Count() > 0)
            {
                divmobile.Visible = false;
            }
        }
    }

    protected void btnResendEmail_Click(object sender, EventArgs e)
    {
        Calani.BusinessObjects.Membership.LoginManager lmgr = new Calani.BusinessObjects.Membership.LoginManager();
        string email = lmgr.GetEmail(ph.CurrentUserId);




        lmgr.RecoverPassword(email);
        btnResendEmail.Visible = false;
    }
}