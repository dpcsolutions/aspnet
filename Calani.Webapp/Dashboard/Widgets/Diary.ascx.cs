﻿using Calani.BusinessObjects.AbsenceRequests;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Dashboard_Widgets_Diary : System.Web.UI.UserControl
{

    public string maincolor = "blue";

    public string background { get; set; }
    public string foreground { get; set; }
    public string iconanimation { get; set; }
    public string panelborder { get; set; }



    protected void Page_Load(object sender, EventArgs e)
    {
        long userId = PageHelper.GetUserId(Session);
        long orgId = PageHelper.GetCurrentOrganizationId(Session);

        List<PlanningWidgetItem> visits = GetVisits(orgId, userId);

        bool isdark = false;
        if (visits != null && visits.Count > 0)
        {   
            repeater.DataSource = visits;
            repeater.DataBind();

            lblNoVisits.Visible = false;
            isdark = true;
        }
        else
        {
            lblNoVisits.Visible = true; 
        }


        var absenceRequestsListItems = GetAbsenceRequests(orgId, userId);
        if (absenceRequestsListItems.Any())
        {
            repeaterAbsenceRequests.DataSource = absenceRequestsListItems;
            repeaterAbsenceRequests.DataBind();
            lblNoVisits.Visible = false;
            isdark = true;
        }

        if (isdark)
        {
            background = "white";
            foreground = maincolor + "-800";
            iconanimation = "tada infinite animated";
            panelborder = "border-left-xlg border-left-" + foreground;

        }
        else
        {
            background = "light-grey";
            foreground = "slate";
            iconanimation = "";

        }

    }

    private List<PlanningWidgetItem> GetVisits(long orgId, long userId)
    {
        Calani.BusinessObjects.Projects.JobsManager mgr = new Calani.BusinessObjects.Projects.JobsManager(orgId);
        var visits2 = mgr.GetNextVisits(userId, null, 4);
        List<PlanningWidgetItem> visits = null;
        if (visits2 != null) visits = (from r in visits2
                                       where r.dateStart != null
                                       select new PlanningWidgetItem(r)).ToList();

        return visits;
    }

    private List<PlanningWidgetItem> GetAbsenceRequests(long orgId, long userId)
    {
        var absenceRequestsManager = new AbsenceRequestsManager(orgId);
        var absenceRequests = absenceRequestsManager.ListAbsenceRequests(approved: true, employee: userId);
        var absenceRequestsListItems = absenceRequests.Select(ar => new PlanningWidgetItem(ar));

        return absenceRequestsListItems.ToList();
    }

    public class PlanningWidgetItem
    {
        public long id { get; set; }
        public DateTime dateStart { get; set; }
        public DateTime dateEnd { get; set; }
        public string label { get; set; }

        public PlanningWidgetItem()
        {

        }

        public PlanningWidgetItem(visits v)
        {
            id = v.id;
            dateStart = v.dateStart.Value;
            if(v.jobs != null)
            {
                label = v.jobs.clients.companyName;
            }
            else if(v.services != null)
            {
                label = v.services.name;
            }

            int maxlen = 20;

            if(!String.IsNullOrWhiteSpace(label) && label.Length > maxlen)
            {
                label = label.Substring(0, maxlen) + "...";
            }
        }

        public PlanningWidgetItem(absence_requests absenceRequest)
        {
            id = absenceRequest.id;
            dateStart = absenceRequest.startDate;
            dateEnd = absenceRequest.endDate;
            label = ResourceHelper.Get(((AbsenceCategoryEnum)absenceRequest.absenceCategory).GetDescription());
        }
    }
}