﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Dashboard_Widgets_Expenses" Codebehind="Expenses.ascx.cs" %>
<div class="panel panel-body bg-<%= background %> panel-dash-widget  <%= panelborder %>">
	<div class="media no-margin">
		<div class="media-body">
			<h4 class="no-margin text-semibold"><asp:HyperLink runat="server" NavigateUrl="~/Costs/Expenses/" ID="lnkTitle"><%= Resources.Resource.Expenses %></asp:HyperLink></h4>
		

            <div>
                <div><em class="text-size-small text-slate"><%= Resources.Resource.MyExpensesThisMonth %>:</em></div>
                <div>
                    <span class="text-size-small  text-<%= foreground %> "><asp:HyperLink NavigateUrl="~/Costs/Expenses/" runat="server" ID="lnkExpenses1" >
                            <i class="icon-question6"></i>
                            <%= Resources.Resource.Awaiting %>  :
                            <asp:Label runat="server" ID="lblMonthSentAwaiting" />
                    </asp:HyperLink></span>
                </div>
                <div>
                    <span class="text-size-small  text-<%= foreground %> "><asp:HyperLink NavigateUrl="~/Costs/Expenses/Default.aspx" runat="server" ID="lnkExpenses2">
                            <i class="icon-checkmark"></i>
                            <%= Resources.Resource.Approved %>  :
                            <asp:Label runat="server" ID="lblMonthApprovedExpenses" />
                    </asp:HyperLink></span>
                </div>
                
            </div>

            <asp:Panel runat="server" style="margin-top: 10px" ID="panelManager">
                <div><em class="text-size-small text-slate"><%= Resources.Resource.TeamExpenses %>:</em></div>
                <span class="text-size-small <%= iconanimation %> text-<%= foreground %>">
                    <i class="icon-question6"></i>
                    <%= Resources.Resource.ToApproved %>  :
                    <asp:Label runat="server" ID="lblTeamToValidate" />
                </span>
            </asp:Panel>
		</div>

		<div class="media-right ">
			<i class="icon-wallet icon-3x text-<%= foreground %> <%= iconanimation %>"></i>
		</div>
	</div>
</div>