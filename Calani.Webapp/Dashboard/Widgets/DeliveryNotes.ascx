﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Dashboard_Widgets_DeliveryNotes" Codebehind="DeliveryNotes.ascx.cs" %>
<div class="panel panel-body bg-light-grey panel-dash-widget">
	<div class="media no-margin">
		<div class="media-body">
			<h4 class="no-margin text-semibold">
                <asp:HyperLink runat="server" NavigateUrl="~/Projects/DeliveryNotes/" ID="lnkTitle" class="text-slate"><%= Resources.Resource.DeliveryNotes %></asp:HyperLink>
			</h4>
		

            <div>
                <div>
                    <span class="text-size-small text-slate">
                        <asp:HyperLink NavigateUrl="~/Projects/DeliveryNotes/" runat="server" ID="lnkExpenses1" class="text-size-small text-slate">
                        <i class="icon-question6"></i>
                        <%= Resources.Resource.Draft %>  :
                        <asp:Label runat="server" ID="lblDrafts" />
                    </asp:HyperLink></span>
                </div>
                <div>
                    <span class="text-size-small  text-slate">
                        <asp:HyperLink NavigateUrl="~/Projects/DeliveryNotes/" runat="server" ID="lnkExpenses2" class="text-size-small text-slate">
                        <i class="icon-envelope"></i>
                        <%= Resources.Resource.Sent %>  :
                        <asp:Label runat="server" ID="lblSent" />
                    </asp:HyperLink></span>
                </div>
                <div>
                    <span class="text-size-small  text-slate">
                        <asp:HyperLink NavigateUrl="~/Projects/DeliveryNotes/" runat="server" ID="HyperLink1" class="text-size-small text-slate">
                        <i class="icon-checkmark"></i>
                        <%= Resources.Resource.Approved %>  :
                        <asp:Label runat="server" ID="lblApproved" />
                    </asp:HyperLink></span>
                </div>
            </div>

		</div>

		<div class="media-right ">
            <i class="icon-box icon-3x text-slate"></i>
		</div>
	</div>
</div>