﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Dashboard_Widgets_Scheduler" Codebehind="Scheduler.ascx.cs" %>
<div class="panel panel-body bg-<%= background %> panel-dash-widget <%= panelborder %>">
	<div class="media no-margin">
		<div class="media-body">
			<h4 class="no-margin text-semibold">
				<asp:HyperLink runat="server" NavigateUrl="~/scheduler" class="text-<%= foreground %>"><%= Resources.Resource.Scheduler %></asp:HyperLink>
			</h4>
		

            <div runat="server">
                <div>
                    <a href="../scheduler" class="text-slate"><%= Resources.Resource.Schedule %></a>
                </div>
                
            </div>
		</div>

		<div class="media-right ">
			<i class="fa fa-calendar-check icon-3x text-<%= foreground %> <%= iconanimation %>"></i>
		</div>
	</div>
</div>