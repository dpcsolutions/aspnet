﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Dashboard_Widgets_Projects" Codebehind="Projects.ascx.cs" %>
<div class="panel panel-body bg-<%= background %> panel-dash-widget <%= panelborder %>">
	<div class="media no-margin">
		<div class="media-body">
			<h4 class="no-margin text-semibold"><asp:HyperLink runat="server" NavigateUrl="~/Projects/Opens/"  id="linkTitle"> <%= Resources.Resource.Projects %></asp:HyperLink></h4>
			
            <div runat="server" id="lineAccepted">
                <asp:HyperLink runat="server" NavigateUrl="~/Projects/Opens/?f=JobFinished" id="linkAccepted">
                    <i class="icon-finish display-inline-block <%= iconanimation %> text-<%= foreground %> animated"></i>
                    <asp:Label runat="server" ID="lblCountAccepted" />&nbsp;<%= Resources.Resource.Finished_many %> 
                </asp:HyperLink>
            </div>
            <div runat="server" id="lineDraft">
                <asp:HyperLink runat="server" NavigateUrl="~/Projects/Opens/?f=JobOpened" id="linkDraft">
                    <i class="icon-play4 display-inline-block  text-<%= foreground %>"></i>
                    <asp:Label runat="server" ID="lblCountDraft" />&nbsp;<%= Resources.Resource.Open_Jobs_many %> 
                </asp:HyperLink>
            </div>
            <div style="margin-top:10px" id="panelNew" runat="server">
                <span class="text-size-small text-muted">
                    <asp:HyperLink runat="server" NavigateUrl="~/Projects/Quotes/Job.aspx" id="linkNew">
                        <i class="fa fa-plus-circle"></i>
                        <%= Resources.Resource.CreateANew %> 
                    </asp:HyperLink>
                </span>
            </div>
            <div>
                <span class="text-size-small text-muted">
                    <asp:HyperLink runat="server" id="linkLast">
                        <i class="fa fa-history"></i>
                        <asp:Label runat="server" ID="lblLastNr" />
                    </asp:HyperLink>
                </span>
            </div>

		</div>

		<div class="media-right">
			<i class="icon-cog5 icon-3x text-<%= foreground %> <%= iconanimation %>"></i>
		</div>
	</div>
</div>