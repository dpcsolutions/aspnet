﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Dashboard_Widgets_TimeSheet" Codebehind="TimeSheet.ascx.cs" %>
<div class="panel panel-body bg-<%= background %> panel-dash-widget <%= panelborder %>">
	<div class="media no-margin">
		<div class="media-body">
			<h4 class="no-margin text-semibold"><asp:HyperLink runat="server" NavigateUrl="~/Time/Sheets/" class="text-<%= foreground %>"><%= Resources.Resource.Time_Sheets %></asp:HyperLink></h4>
			
            <div>
                <span class="text-size-small text-slatee">
                    <asp:Label runat="server" ID="lblCurrentWeekDescription" />
                </span>

            </div>
            <div runat="server" id="lineAccepted" class="text-size-small">
                <asp:HyperLink runat="server" id="linkCurrentWeek">
                    <i class="icon-pencil3 display-inline-block tada"></i>
                    <asp:Label runat="server" ID="lblCurrentWeek" />
                </asp:HyperLink>
            </div>

            <div style="margin-top:10px" runat="server" id="panelManager">
                <div>
                    <span class="text-size-small">
                        <asp:HyperLink runat="server" NavigateUrl="~/Time/Sheets/?s=currentweek" CssClass="text-slate">
                            
                            <%= Resources.Resource.TeamManagement %> 
                        </asp:HyperLink>
                    </span>
                </div>
                <div>
                    <span class="text-size-small text-slate">
                        <asp:HyperLink runat="server" NavigateUrl="~/Time/Sheets/?s=tovalidate" >
                            <i class="icon-question6 <%= iconanimation %>"></i>
                            <%= Resources.Resource.ToValidate %>  :
                            <asp:Label runat="server" ID="lblToValidateCount" />&nbsp;<%= Resources.Resource.Time_Sheet.ToLower() %>
                        </asp:HyperLink>
                    </span>
                </div>
            </div>
		</div>

		<div class="media-right ">
			<i class="icon-file-spreadsheet icon-3x text-<%= foreground %> <%= iconanimation %>"></i>
		</div>
	</div>
</div>