﻿using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Projects;
using System;

public partial class Dashboard_Widgets_WorkReports : System.Web.UI.UserControl
{
    public string maincolor = "green";

    public string background { get; set; }
    public string foreground { get; set; }
    public string iconanimation { get; set; }
    public string panelborder { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {

        var orgId = PageHelper.GetCurrentOrganizationId(Session);
        var orgMgr = new OrganizationManager(orgId);
        orgMgr.Load();


       // Calani.BusinessObjects.Projects.ProjectsManager mgr = new Calani.BusinessObjects.Projects.ProjectsManager(PageHelper.GetCurrentOrganizationId(Session));
        var mgr = new WorkReportsManager(orgId);

        if (PageHelper.GetUserType(Session) == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            mgr.EmployeeFilter = PageHelper.GetUserId(Session);
        }
       
        lblCountDraft.Text = mgr.GetStat(WorkReportStatus.Draft).ToString();
        lblCountValidated.Text = mgr.GetStat(WorkReportStatus.Validated).ToString();


        

        long count = 0;
        long.TryParse(lblCountValidated.Text, out count);
        if (PageHelper.GetUserType(Session) == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            long.TryParse(lblCountDraft.Text, out count);

        }

        bool isdark =  count > 0;

        if (isdark)
        {
            background = "white";
            foreground = maincolor + "-800";
            iconanimation = "tada infinite ";
            panelborder = "border-left-xlg border-left-" + foreground;

        }
        else
        {
            background = "light-grey";
            foreground = "slate";
            iconanimation = "";

        }

        linkAccepted.CssClass = "text-" + foreground;
        linkDraft.CssClass = "text-" + foreground;
        linkLast.CssClass = "text-slate";
        linkTitle.CssClass = "text-" + foreground;

        var lastReport = mgr.GetLatest(WorkReportStatus.Draft, orgMgr.CompanyName);
        linkLast.NavigateUrl = String.Format("~/Projects/WorkReport/ReportDetails.aspx?pid={0}&rid={1}", lastReport.ProjectId, lastReport.Id) ;
        lblLastNr.Text = lastReport.Title;

        count = 0;
        long.TryParse(lblCountValidated.Text, out count);
        lineAccepted.Visible = count > 0;

        count = 0;
        long.TryParse(lblCountDraft.Text, out count);
        lineDraft.Visible = count > 0;
               
    }
}