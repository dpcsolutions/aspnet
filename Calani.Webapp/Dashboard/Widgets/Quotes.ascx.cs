﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Dashboard_Widgets_Quotes : System.Web.UI.UserControl
{
    public string maincolor = "green";

    public string background { get; set; }
    public string foreground { get; set; }
    public string iconanimation { get; set; }
    public string panelborder { get; set; }
    


    protected void Page_Load(object sender, EventArgs e)
    {


        if (PageHelper.GetUserType(Session) != Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
        {
            Visible = false;
            return;
        }

        Calani.BusinessObjects.Projects.ProjectsManager mgr = new Calani.BusinessObjects.Projects.ProjectsManager(PageHelper.GetCurrentOrganizationId(Session));
        lblLastNr.Text = mgr.GetStat("Quote", null, "Last");
        lblCountDraft.Text = mgr.GetStat("Quote", "Draft", "Count");
        lblCountAccepted.Text = mgr.GetStat("Quote", "Accepted", "Count");

        long count = 0;
        long.TryParse(lblCountAccepted.Text, out count);



        bool isdark =  count > 0;

        if (isdark)
        {
            background = "white";
            foreground = maincolor + "-800";
            iconanimation = "tada infinite";
            panelborder = "border-left-xlg border-left-"+ foreground;
 
        }
        else
        {
            background = "light-grey";
            foreground = "slate";
            iconanimation = "";
    
        }

        linkTitle.CssClass = "text-" + foreground;
        linkAccepted.CssClass = "text-" + foreground;
        linkDraft.CssClass = "text-" + foreground;

        linkLast.NavigateUrl = "~/Projects/Quotes/Job.aspx?id=" + mgr.GetStat("Quote", null, "LastId");


        count = 0;
        long.TryParse(lblCountAccepted.Text, out count);
        lineAccepted.Visible = count > 0;

        count = 0;
        long.TryParse(lblCountDraft.Text, out count);
        lineDraft.Visible = count > 0;

        
    }
}