﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Dashboard_Widgets_Invoices" Codebehind="Invoices.ascx.cs" %>
<div class="panel panel-body bg-<%= background %> panel-dash-widget <%= panelborder %>">
	<div class="media no-margin">
		<div class="media-body">
			<h4 class="no-margin text-semibold"><asp:HyperLink runat="server" NavigateUrl="~/Projects/Invoices/" id="linkTitle"> <%= Resources.Resource.Invoices %></asp:HyperLink></h4>
			
            <div runat="server" id="lineAccepted">
                <asp:HyperLink runat="server" NavigateUrl="~/Projects/Invoices/?f=InvoicePaymentLate" id="linkAccepted">
                    <i class="icon-bell2 display-inline-block <%= iconanimation %> text-<%= foreground %> animated"></i>
                    <asp:Label runat="server" ID="lblCountAccepted" />&nbsp;<%= Resources.Resource.Late_many %> 
                </asp:HyperLink>
            </div>
            <div runat="server" id="lineDraft">
                <asp:HyperLink runat="server" NavigateUrl="~/Projects/Invoices/?f=InvoicePaymentDue" id="linkDraft">
                    <i class="icon-bell-check display-inline-block text-<%= foreground %>"></i>
                    <asp:Label runat="server" ID="lblCountDraft" />&nbsp;<%= Resources.Resource.Pending_many %> 
                </asp:HyperLink>
            </div>
            <div style="margin-top:10px">
                <span class="text-size-small text-muted">
                    <asp:HyperLink runat="server" NavigateUrl="~/Projects/Invoices/Job.aspx" id="linkNew">
                        <i class="fa fa-plus-circle"></i>
                        <%= Resources.Resource.CreateANew %> 
                    </asp:HyperLink>
                </span>
            </div>
            <div>
                <span class="text-size-small text-muted">
                    <asp:HyperLink runat="server" id="linkLast">
                        <i class="fa fa-history"></i>
                        <asp:Label runat="server" ID="lblLastNr" />
                    </asp:HyperLink>
                </span>
            </div>

		</div>

		<div class="media-right">
			<i class="icon-file-check2 icon-3x text-<%= foreground %> <%= iconanimation %>"></i>
		</div>
	</div>
</div>