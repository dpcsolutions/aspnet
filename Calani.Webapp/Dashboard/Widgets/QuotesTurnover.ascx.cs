﻿using Calani.BusinessObjects.Projects;
using System;
using System.Linq;
using Calani.BusinessObjects.Widgets;
using Newtonsoft.Json;
using Calani.BusinessObjects.Generic;

public partial class Dashboard_Widgets_QuotesTurnover : System.Web.UI.UserControl
{
    public string background { get; set; }
    public string panelborder { get; set; }
    public string data { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {

        var orgId = PageHelper.GetCurrentOrganizationId(Session);

        var mgr = new ProjectsManager(orgId);

        var orgMgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(orgId);
        orgMgr.Load();


        /*if (PageHelper.GetUserType(Session) == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            mgr.EmployeeFilter = PageHelper.GetUserId(Session);
        }*/

        var currYear = new DateRange(Calani.BusinessObjects.Enums.DatesRangeEnum.CurrentYear);

        var quotesRecords = mgr.ListQuotes(custid: null, dateRange: currYear).ToList();        

        var totalQuoteDraft = quotesRecords.Where(r => r.ProjectStatus == ProjectStatus.QuoteDraft).Sum(s => s.totalWithoutTaxes * (s.rating / 0.05) / 100 );
        var totalQuoteSent = quotesRecords.Where(r => r.ProjectStatus == ProjectStatus.QuoteSent).Sum(s => s.totalWithoutTaxes * (s.rating / 0.05) / 100);
        var totalQuoteOutDated = quotesRecords.Where(r => r.ProjectStatus == ProjectStatus.QuoteOutDated).Sum(s => s.totalWithoutTaxes * (s.rating / 0.05) / 100);

        var quotesTotal = totalQuoteDraft + totalQuoteSent + totalQuoteOutDated;



        linkTitle.Text = Resources.Resource.QuotesTracking;
        linkSum.Text = String.Format("{0:N1} {1}", quotesTotal, orgMgr.Currency);

        var chart = new Chart
        {
            cols = new object[]
            {
                new { id = "Project", type = "string", label = "Project" },
                new { id = "total", type = "number", label = "total" }
            },
            rows = new object[]
            {
                new { c = new object[] { new { v = Resources.Resource.Draft }, new { v = totalQuoteDraft, f = Resources.Resource.Draft + "," + String.Format("{0:N0}", totalQuoteDraft) } } },
                new { c = new object[] { new { v = Resources.Resource.Sent}, new { v = totalQuoteSent, f = Resources.Resource.Sent + "," + String.Format("{0:N0}", totalQuoteSent) } } },
                new { c = new object[] { new { v = Resources.Resource.Outdated}, new { v = totalQuoteOutDated, f = Resources.Resource.Outdated + "," + String.Format("{0:N0}", totalQuoteOutDated) } } },

            }
        };



        data = JsonConvert.SerializeObject(chart);
    }
}