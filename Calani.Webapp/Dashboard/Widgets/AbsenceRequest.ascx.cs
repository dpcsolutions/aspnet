﻿using Calani.BusinessObjects.AbsenceRequests;
using Calani.BusinessObjects.Enums;
using System;

public partial class Dashboard_Widgets_AbsenceRequest : System.Web.UI.UserControl
{
    public string maincolor = "blue";

    public string background { get; set; }
    public string foreground { get; set; }
    public string iconanimation { get; set; }
    public string panelborder { get; set; }

    public int ApprovedRequestsCount { get; private set; }
    public int AwaitingRequestsCount { get; private set; }

    private AbsenceRequestsManager absenceRequestsManager;

    protected void Page_Load(object sender, EventArgs e)
    {
        long userId = PageHelper.GetUserId(Session);
        long orgId = PageHelper.GetCurrentOrganizationId(Session);

        absenceRequestsManager = new AbsenceRequestsManager(orgId);

        var isdark = false;

        if (isdark)
        {
            background = "white";
            foreground = maincolor + "-800";
            iconanimation = "tada infinite animated";
            panelborder = "border-left-xlg border-left-" + foreground;
        }
        else
        {
            background = "light-grey";
            foreground = "slate";
            iconanimation = "";
        }

        ApprovedRequestsCount = absenceRequestsManager.CountAbsenceRequestsForDashboard(userId, ValidationEnum.Approved);
        AwaitingRequestsCount = absenceRequestsManager.CountAbsenceRequestsForDashboard(userId, ValidationEnum.Pending);
    }
}