﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Dashboard_Widgets_Projects : System.Web.UI.UserControl
{
    public string maincolor = "green";

    public string background { get; set; }
    public string foreground { get; set; }
    public string iconanimation { get; set; }
    public string panelborder { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {




        Calani.BusinessObjects.Projects.ProjectsManager mgr = new Calani.BusinessObjects.Projects.ProjectsManager(PageHelper.GetCurrentOrganizationId(Session));
        if (PageHelper.GetUserType(Session) == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            mgr.EmployeeFilter = PageHelper.GetUserId(Session);
        }
        lblLastNr.Text = mgr.GetStat("Open", null, "Last");
        lblCountDraft.Text = mgr.GetStat("Open", "Opened", "Count");
        lblCountAccepted.Text = mgr.GetStat("Open", "Finished", "Count");


        

        long count = 0;
        long.TryParse(lblCountAccepted.Text, out count);
        if (PageHelper.GetUserType(Session) == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            long.TryParse(lblCountDraft.Text, out count);

        }

        bool isdark =  count > 0;

        if (isdark)
        {
            background = "white";
            foreground = maincolor + "-800";
            iconanimation = "tada infinite";
            panelborder = "border-left-xlg border-left-" + foreground;

        }
        else
        {
            background = "light-grey";
            foreground = "slate";
            iconanimation = "";

        }

        linkAccepted.CssClass = "text-" + foreground;
        linkDraft.CssClass = "text-" + foreground;
        linkNew.CssClass = "text-slate";
        linkLast.CssClass = "text-slate";
        linkTitle.CssClass = "text-" + foreground;

        linkLast.NavigateUrl = "~/Projects/Opens/Job.aspx?id=" + mgr.GetStat("Open", null, "LastId");


        count = 0;
        long.TryParse(lblCountAccepted.Text, out count);
        lineAccepted.Visible = count > 0;

        count = 0;
        long.TryParse(lblCountDraft.Text, out count);
        lineDraft.Visible = count > 0;

        if (PageHelper.GetUserType(Session) == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            panelNew.Visible = false;
        }
    }
}