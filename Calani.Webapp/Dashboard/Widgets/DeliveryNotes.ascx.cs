﻿using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Projects;
using System;
using System.Linq;

public partial class Dashboard_Widgets_DeliveryNotes : System.Web.UI.UserControl
{
    public string maincolor = "purple";
    public const string background = "light-grey";
    public const string foreground = "slate";
    public const string iconanimation = "";
    public const string panelborder = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        long userId = PageHelper.GetUserId(Session);
        long orgId = PageHelper.GetCurrentOrganizationId(Session);
        Calani.BusinessObjects.Contacts.ContactTypeEnum type = PageHelper.GetUserType(Session);

        var mgr = new ProjectsManager(orgId);
        var org = new OrganizationManager(orgId);
            
        var svc = new JobsService(mgr);
        
        org.Load();

        long? creatorId = null;
        if(type == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            creatorId = userId;
        }

        var dateRange = new DateRange(Calani.BusinessObjects.Enums.DatesRangeEnum.CurrentYear);
        var exp = svc.ListDeliveryNotes(dateRange: dateRange, assigneeId: null, creatorId: creatorId);

        var draftsCount = exp.Count(dn => dn.Status.Equals("DeliveryNoteDraft", StringComparison.OrdinalIgnoreCase));
        var sentCount = exp.Count(dn => dn.Status.Equals("DeliveryNoteSent", StringComparison.OrdinalIgnoreCase));
        var approvedCount = exp.Count(dn => dn.Status.Equals("DeliveryNoteAccepted", StringComparison.OrdinalIgnoreCase));

        lblDrafts.Text = draftsCount.ToString();
        lblSent.Text = sentCount.ToString();
        lblApproved.Text = approvedCount.ToString();

        lblDrafts.CssClass = "text-" + foreground;
        lblSent.CssClass = "text-" + foreground;
        lblApproved.CssClass = "text-" + foreground;
    }
}