﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.Costs;
using Calani.BusinessObjects.CustomerAdmin;

public partial class Dashboard_Widgets_Expenses : System.Web.UI.UserControl
{
    public string maincolor = "purple";


    public string background { get; set; }
    public string foreground { get; set; }
    public string iconanimation { get; set; }
    public string panelborder { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        long userId = PageHelper.GetUserId(Session);
        long orgId = PageHelper.GetCurrentOrganizationId(Session);
        Calani.BusinessObjects.Contacts.ContactTypeEnum type = PageHelper.GetUserType(Session);

        ExpenseManager mgr = new ExpenseManager(orgId);
        List<ExpenseExtended> exp = null;
        var org = new OrganizationManager(orgId);
        org.Load();
        

        if (type == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
        {
            panelManager.Visible = true;

            exp = mgr.ListExpenses2(false, null);
            var validate = string.Format("{0:0.00#} {1}", (from r in exp select r.price).Sum(), org.Currency);
            lblTeamToValidate.Text = validate;
        }
        else
        {
            panelManager.Visible = false;
        }

        


        DateTime m = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        exp = mgr.ListExpenses2(false, userId, m, null);

        var awsum = (from r in exp select r.price).Sum();

        var awaiting = string.Format("{0:0.00#} {1}", awsum, org.Currency);

        lblMonthSentAwaiting.Text = awaiting;

        exp = mgr.ListExpenses2(true, userId, m, null);

        var approved = string.Format("{0:0.00#} {1}", (from r in exp select r.price).Sum(), org.Currency);

        lblMonthApprovedExpenses.Text = approved;


        int count = 0;
        if (type == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
        {
            Int32.TryParse(lblTeamToValidate.Text, out count);
        }
        bool isdark = count > 0;

        


        if (isdark)
        {
            background = "white";
            foreground = maincolor + "-800";
            iconanimation = "tada infinite animated";
            panelborder = "border-left-xlg border-left-" + foreground;

        }
        else
        {
            background = "light-grey";
            foreground = "slate";
            iconanimation = "";

        }

        
        

        lblMonthSentAwaiting.CssClass = "text-" + foreground;
        lblMonthApprovedExpenses.CssClass = "text-" + foreground;
        lnkTitle.CssClass = "text-" + foreground;
        lnkExpenses1.CssClass = "text-" + foreground;
        lnkExpenses2.CssClass = "text-" + foreground;

    }
}