﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Dashboard_Widgets_TurnoverPotential" Codebehind="TurnoverPotential.ascx.cs" %>
<div class="panel panel-body bg-<%= background %> panel-dash-widget <%= panelborder %>">
	<div class="media no-margin">
		<div class="media-body">
			<h4 class="no-margin text-semibold text-center"><asp:HyperLink runat="server" NavigateUrl=""  id="linkTitle"> </asp:HyperLink>
                <br/>
                <asp:HyperLink runat="server" NavigateUrl="" id="linkSum"> </asp:HyperLink>
			</h4>
			
            <!-- Basic pie charts -->

									<div class="chart-container text-center">
										<div class="display-inline-block" id="google-pie-potential-turnover"></div>
									</div>
					<!-- /basic pie charts -->
		</div>
	</div>
</div>
<script type="text/javascript">

    // Initialize chart
    google.load("visualization", "1", { packages: ["corechart"] });
    google.setOnLoadCallback(drawPie);


    // Chart settings    
    function drawPie() {

        
        var data = new google.visualization.DataTable(<%=data%>);
        
        // Options
        var options_potential_pie = {
            height: 200,
            width: 200,
            legend:  'none',
            pieSliceText: 'value-and-percentage',
            pieSliceTextStyle: {
                color: 'black'
            },
            chartArea: {
               
                width: '95%',
                height: '95%'
            },
            animation: {
                duration: 1000,
                easing: 'out'
            },
            colors: ['#9dc3e6', '#d9d9d9', '#ffd966']
        };


        // Instantiate and draw our chart, passing in some options.
        var pie = new google.visualization.PieChart($('#google-pie-potential-turnover')[0]);
        pie.draw(data, options_potential_pie);
    }
</script>