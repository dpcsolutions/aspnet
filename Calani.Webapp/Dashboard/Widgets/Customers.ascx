﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Dashboard_Widgets_Customers" Codebehind="Customers.ascx.cs" %>
<div class="panel panel-body bg-light-grey panel-dash-widget">
	<div class="media no-margin">
		<div class="media-body">
			<h4 class="no-margin text-semibold"><asp:HyperLink runat="server" NavigateUrl="~/Customers/List/" class="text-slate">
                <%= Resources.Resource.Customers %></asp:HyperLink></h4>
			<div>
                <span class="text-size-small">
                    <%= Resources.Resource.RegistratedCustomers %>: <%=Count %>
			    </span>
            </div>
            <div id="panelAdd" runat="server">
                <span class="text-size-small text-muted">
                    <asp:HyperLink runat="server" ID="linkNew" CssClass="text-slate" NavigateUrl="../../Customers/List/New.aspx" />
			    </span>
            </div>
            <asp:Repeater runat="server" ID="repeatedCustomers">
                <ItemTemplate>
                    <div>
                        <span class="text-size-small">
                            <a href="../Customers/List/Details.aspx?id=<%# Eval("id")  %>" class="text-slate">
                                <i class='fa fa-history'></i> <%# Eval("companyName")  %> 
                            </a>
			            </span>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            
		</div>

		<div class="media-right ">
			<i class="fa fa-user-circle-o icon-3x text-slate"></i>
		</div>
	</div>
</div>