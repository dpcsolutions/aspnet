﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Dashboard_Widgets_TimeSheet : System.Web.UI.UserControl
{

    public string maincolor = "blue";

    public string background { get; set; }
    public string foreground { get; set; }
    public string iconanimation { get; set; }
    public string panelborder { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        bool isdark = false;
        int week = Calani.BusinessObjects.Generic.CalendarTools.GetNonIsoWeekOfYear(DateTime.Now);

        lblCurrentWeekDescription.Text = Resources.Resource.CurrentWeek + " " + Resources.Resource.Nr + " " +  week;

        Calani.BusinessObjects.TimeManagement.TimeSheetsManager mgr = new Calani.BusinessObjects.TimeManagement.TimeSheetsManager(PageHelper.GetCurrentOrganizationId(Session));

        long currentUser = PageHelper.GetUserId(Session);

        var sheet = mgr.GetTimeSheet(currentUser, DateTime.Now.Year, week);

        if(sheet != null)
        {
            linkCurrentWeek.NavigateUrl = "~/Time/Sheets/Sheet.aspx?id=" + sheet.id;
            var totalDuration = (from r in sheet.timesheetrecords where r.duration != null select r.duration.Value).Sum();

            string text = Resources.Resource.OnMyPersonnalSheet;
            text += ": " + Math.Round(totalDuration, 2, MidpointRounding.AwayFromZero).ToString(System.Globalization.CultureInfo.InvariantCulture) + "h";
            lblCurrentWeek.Text = text;

        }
        else
        {
            linkCurrentWeek.NavigateUrl = "~/Time/Sheets/Sheet.aspx";

            lblCurrentWeek.Text = Resources.Resource.CreateMyPersonnalSheet;
        }

        if(PageHelper.GetUserType(Session) == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            panelManager.Visible = false;
        }
        else
        {
            panelManager.Visible = true;

            double? th;
            var sheetsToValidate = mgr.List(out th,false, null, null, null);
            int countCurrentWeek = (from r in sheetsToValidate
                                    where r.year == DateTime.Now.Year
                                    && r.week == week
                                    select r).Count();
            int countToValidate = sheetsToValidate.Count - countCurrentWeek;

            if (countToValidate > 0) isdark = true;

            lblToValidateCount.Text = countToValidate.ToString();

        }


        if (isdark)
        {
            background = "white";
            foreground = maincolor + "-800";
            iconanimation = "tada infinite";
            panelborder = "border-left-xlg border-left-" + foreground;

        }
        else
        {
            background = "light-grey";
            foreground = "slate";
            iconanimation = "";

        }

    }
}