﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Dashboard_Widgets_Customers : System.Web.UI.UserControl
{
    public long Count { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        Calani.BusinessObjects.CustomerAdmin.ClientsManager mgr = new Calani.BusinessObjects.CustomerAdmin.ClientsManager(PageHelper.GetCurrentOrganizationId(Session));
        Count = mgr.Count();

        var lasts = mgr.GetLasts(3);
        if (lasts != null && lasts.Count > 0)
        {
            /*linkLast.Text = "<i class='fa fa-history'></i> " + last.companyName;
            linkLast.NavigateUrl = "Customers/Details.aspx?id=" + last.id;
            */

            repeatedCustomers.DataSource = lasts;
            repeatedCustomers.DataBind();
        }
        else
        {
            repeatedCustomers.Visible = false;
            //linkNew.Text = "<i class='fa fa-plus-circle'></i> " + Resources.Resource.Add_first_customer;
        }

        linkNew.Text = "<i class='fa fa-plus-circle'></i> " + Resources.Resource.Add_new;

        if (PageHelper.GetUserType(Session) != Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
            panelAdd.Visible = false;
    }
}