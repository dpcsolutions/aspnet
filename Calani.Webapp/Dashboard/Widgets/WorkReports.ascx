﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Dashboard_Widgets_WorkReports" Codebehind="WorkReports.ascx.cs" %>
<div class="panel panel-body bg-<%= background %> panel-dash-widget <%= panelborder %>">
	<div class="media no-margin">
		<div class="media-body">
			<h4 class="no-margin text-semibold"><asp:HyperLink runat="server" NavigateUrl="~/Projects/WorkReport/"  id="linkTitle"> <%= Resources.Resource.WorkReports %></asp:HyperLink></h4>
			
            <div runat="server" id="lineAccepted">
                <asp:HyperLink runat="server" NavigateUrl="~/Projects/WorkReport/" id="linkAccepted">
                    <i class="icon-finish display-inline-block <%= iconanimation %> text-<%= foreground %>"></i>
                    <asp:Label runat="server" ID="lblCountValidated" />&nbsp;<%= Resources.Resource.Validated %> 
                </asp:HyperLink>
            </div>
            <div runat="server" id="lineDraft">
                <asp:HyperLink runat="server" NavigateUrl="~/Projects/WorkReport/" id="linkDraft">
                    <i class="icon-play4 display-inline-block  text-<%= foreground %>"></i>
                    <asp:Label runat="server" ID="lblCountDraft" />&nbsp;<%= Resources.Resource.Draft %> 
                </asp:HyperLink>
            </div>
           
            <div>
                <span class="text-size-small text-muted">
                    <asp:HyperLink runat="server" id="linkLast">
                        <i class="fa fa-history"></i>
                        <asp:Label runat="server" ID="lblLastNr" />
                    </asp:HyperLink>
                </span>
            </div>

		</div>

		<div class="media-right">
			<i class="icon-grid5 icon-3x text-<%= foreground %> <%= iconanimation %>"></i>
		</div>
	</div>
</div>