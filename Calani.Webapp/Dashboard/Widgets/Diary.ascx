﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Dashboard_Widgets_Diary" Codebehind="Diary.ascx.cs" %>
<div class="panel panel-body bg-<%= background %> panel-dash-widget <%= panelborder %>">
	<div class="media no-margin">
		<div class="media-body">
			<h4 class="no-margin text-semibold"><asp:HyperLink runat="server" NavigateUrl="~/Time/Diary/" class="text-<%= foreground %>"><%= Resources.Resource.Diary %></asp:HyperLink></h4>
		

            <div runat="server">
                <div>
                    <span runat="server" id="lblNoVisits" visible="false"><%= Resources.Resource.NothingSheduled %></span>
                    <asp:Repeater runat="server" ID="repeater">
                        <ItemTemplate>
                            <div>
                                <span class="text-size-small">
                                    <a href="../Time/Scheduler/Default.aspx?visitId=<%# Eval("id")  %>" class="text-slate">
                                        <i class='icon-next <%= iconanimation %>'></i>
                                        <%# Eval("dateStart","{0:dd/MM - HH:mm}")  %>:
                                        <%# Eval("label")  %>
                                    </a>
			                    </span>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                

                <div>
                    <asp:Repeater runat="server" ID="repeaterAbsenceRequests">
                        <ItemTemplate>
                            <div>
                                <span class="text-size-small">
                                    <a href="../Time/AbsenceRequests/AbsenceRequest.aspx?id=<%# Eval("id")  %>" class="text-slate">
                                        <i class='icon-next <%= iconanimation %>'></i>
                                        <%# Eval("dateStart","{0:dd/MM - HH:mm}")  %>- 
                                        <%# Eval("dateEnd","{0:dd/MM - HH:mm}")  %>: 
                                        <%# Eval("label")  %>
                                    </a>
			                    </span>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
		</div>

		<div class="media-right ">
			<i class="fa fa-calendar icon-3x text-<%= foreground %> <%= iconanimation %>"></i>
		</div>
	</div>
</div>