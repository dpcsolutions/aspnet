﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Dashboard_Widgets_Quotes" Codebehind="Quotes.ascx.cs" %>
<div class="panel panel-body bg-<%= background %> panel-dash-widget <%= panelborder %>">
	<div class="media no-margin">
		<div class="media-body">
			<h4 class="no-margin text-semibold"><asp:HyperLink runat="server" NavigateUrl="~/Projects/Quotes/" id="linkTitle"> <%= Resources.Resource.Quotes %></asp:HyperLink></h4>
			
            <div runat="server" id="lineAccepted">
                <asp:HyperLink runat="server" NavigateUrl="~/Projects/Quotes/?f=QuoteAccepted" id="linkAccepted">
                    <i class="icon-checkmark display-inline-block <%= iconanimation %> text-<%= foreground %> "></i>
                    <asp:Label runat="server" ID="lblCountAccepted" />&nbsp;<%= Resources.Resource.Accepted_many %> 
                </asp:HyperLink>
            </div>
            <div runat="server" id="lineDraft">
                <asp:HyperLink runat="server" NavigateUrl="~/Projects/Quotes/?f=QuoteDraft" id="linkDraft">
                    <i class="icon-floppy-disk display-inline-block tada infinite text-<%= foreground %>"></i>
                    <asp:Label runat="server" ID="lblCountDraft" />&nbsp;<%= Resources.Resource.Draft_many %> 
                </asp:HyperLink>
            </div>
            <div style="margin-top:10px">
                <span class="text-size-small">
                    <asp:HyperLink runat="server" NavigateUrl="~/Projects/Quotes/Job.aspx" id="linkNew" CssClass="text-slate">
                        <i class="fa fa-plus-circle"></i>
                        <%= Resources.Resource.CreateANew %> 
                    </asp:HyperLink>
                </span>
            </div>
            <div>
                <span class="text-size-small">
                    <asp:HyperLink runat="server" id="linkLast" CssClass="text-slate">
                        <i class="fa fa-history"></i>
                        <asp:Label runat="server" ID="lblLastNr" />
                    </asp:HyperLink>
                </span>
            </div>

		</div>

		<div class="media-right">
			<i class="icon-upload icon-3x text-<%= foreground %> <%= iconanimation %> "></i>
		</div>
	</div>
</div>