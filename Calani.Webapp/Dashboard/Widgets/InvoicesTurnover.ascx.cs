﻿using Calani.BusinessObjects.Projects;
using System;
using System.Linq;
using Calani.BusinessObjects.Widgets;
using Newtonsoft.Json;
using Calani.BusinessObjects.Generic;

public partial class Dashboard_Widgets_InvoicesTurnover : System.Web.UI.UserControl
{
    public string background { get; set; }
    public string panelborder { get; set; }
    public string data { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {


        var orgId = PageHelper.GetCurrentOrganizationId(Session);

        var mgr = new ProjectsManager(orgId);

        var orgMgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(orgId);

        orgMgr.Load();

        /*if (PageHelper.GetUserType(Session) == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            mgr.EmployeeFilter = PageHelper.GetUserId(Session);
        }*/

        var invoicesRecords = mgr.ListInvoices(orgId, null, new DateRange(Calani.BusinessObjects.Enums.DatesRangeEnum.CurrentYear));

        /*var invoiceRecordsDraft = invoicesRecords.Where(r => r.ProjectStatus == ProjectStatus.InvoiceDraft).ToList();
        var totalInvoiceDraft = invoiceRecordsDraft.Sum(s => s.totalWithoutTaxes);
        */
        var totalInvoiceDraft = 0;//they are not included on the list.

        var invoiceRecordsPaymentLate = invoicesRecords.Where(r => r.ProjectStatus == ProjectStatus.InvoicePaymentLate).ToList();
        var totalInvoicePaymentLate = invoiceRecordsPaymentLate.Sum(s => s.totalWithoutTaxes);

        var invoiceRecordsPaymentDue = invoicesRecords.Where(r => r.ProjectStatus == ProjectStatus.InvoicePaymentDue).ToList();
        var totalInvoicePaymentDue = invoiceRecordsPaymentDue.Sum(s => s.totalWithoutTaxes);

        var invoiceRecordsPaid = invoicesRecords.Where(r => r.ProjectStatus == ProjectStatus.InvoicePaid).ToList();
        var totalInvoicePaid = invoiceRecordsPaid.Sum(s => s.totalWithoutTaxes);

        var invoicesTotal = totalInvoiceDraft + totalInvoicePaymentLate + totalInvoicePaymentDue + totalInvoicePaid;


        linkTitle.Text = Resources.Resource.TurnoverExclTax;
        linkSum.Text = String.Format("{0:N1} {1}",  invoicesTotal, orgMgr.Currency);
        var chart = new Chart
        {
            cols = new object[]
            {
                new { id = "Project", type = "string", label = "Project" },
                new { id = "total", type = "number", label = "total" }
            },
            rows = new object[]
            {
                new { c = new object[] { new { v = Resources.Resource.Paid}, new { v = totalInvoicePaid, f = Resources.Resource.Paid + "," + String.Format("{0:N0}", totalInvoicePaid) } } },
                new { c = new object[] { new { v = Resources.Resource.Draft }, new { v = totalInvoiceDraft, f = Resources.Resource.Draft + "," + String.Format("{0:N0}", totalInvoiceDraft) } } },
                new { c = new object[] { new { v = Resources.Resource.Pending}, new { v = totalInvoicePaymentDue, f = Resources.Resource.Pending + "," + String.Format("{0:N0}", totalInvoicePaymentDue) } } },
                new { c = new object[] { new { v = Resources.Resource.Late}, new { v = totalInvoicePaymentLate, f = Resources.Resource.Late + "," + String.Format("{0:N0}", totalInvoicePaymentLate) } } },
            }
        };



        data = JsonConvert.SerializeObject(chart);
    }
}