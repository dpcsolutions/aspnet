﻿using Calani.BusinessObjects.Projects;
using System;
using System.Linq;
using Calani.BusinessObjects.Widgets;
using Newtonsoft.Json;
using Calani.BusinessObjects.Generic;

public partial class Dashboard_Widgets_TurnoverPotential : System.Web.UI.UserControl
{
    public string background { get; set; }
    public string panelborder { get; set; }
    public string data { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {


        var orgId = PageHelper.GetCurrentOrganizationId(Session);

        var mgr = new ProjectsManager(orgId);

        var orgMgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(orgId);

        orgMgr.Load();

        /*if (PageHelper.GetUserType(Session) == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            mgr.EmployeeFilter = PageHelper.GetUserId(Session);
        }*/

        var currYear = new DateRange(Calani.BusinessObjects.Enums.DatesRangeEnum.CurrentYear);
        var quotesRecords = mgr.ListQuotes(custid: null, dateRange: currYear).ToList();

        var totalQuoteDraft = quotesRecords.Where(r => r.ProjectStatus == ProjectStatus.QuoteDraft).Sum(s => s.totalWithoutTaxes * (s.rating / 0.05) / 100);
        var totalQuoteSent = quotesRecords.Where(r => r.ProjectStatus == ProjectStatus.QuoteSent).Sum(s => s.totalWithoutTaxes * (s.rating / 0.05) / 100);
        var totalQuoteOutDated = quotesRecords.Where(r => r.ProjectStatus == ProjectStatus.QuoteOutDated).Sum(s => s.totalWithoutTaxes * (s.rating / 0.05) / 100);

        var quotesTotal = totalQuoteDraft + totalQuoteSent + totalQuoteOutDated;

        var jobsRecords = mgr.ListOpens().Where(r => r.dateJobValidity == null || r.dateJobValidity >= currYear.Start && r.dateJobValidity <= currYear.End).ToList();

        var totalJobOPened = jobsRecords.Where(r => r.ProjectStatus == ProjectStatus.JobOpened).Sum(s => s.totalWithoutTaxes);
        var totalJobJobOutDated = jobsRecords.Where(r => r.ProjectStatus == ProjectStatus.JobOutDated).Sum(s => s.totalWithoutTaxes);

        var jobsTotal = totalJobOPened + totalJobJobOutDated;

        var invoicesRecords = mgr.ListInvoices(orgId, null, currYear);

        var totalInvoiceDraft = invoicesRecords.Where(r => r.ProjectStatus == ProjectStatus.InvoiceDraft).Sum(s => s.totalWithoutTaxes);

        var totalInvoicePaymentLate = invoicesRecords.Where(r => r.ProjectStatus == ProjectStatus.InvoicePaymentLate).Sum(s => s.totalWithoutTaxes);

        var totalInvoicePaid = invoicesRecords.Where(r => r.ProjectStatus == ProjectStatus.InvoicePaid).Sum(s => s.totalWithoutTaxes);

        var totalInvoiceDue = invoicesRecords.Where(r => r.ProjectStatus == ProjectStatus.InvoicePaymentDue).Sum(s => s.totalWithoutTaxes);

        var invoicesTotal = totalInvoiceDraft + totalInvoicePaymentLate+ totalInvoicePaid+ totalInvoiceDue;

        linkTitle.Text = Resources.Resource.PotentialTurnover;
        linkSum.Text = String.Format("{0:N1} {1}", quotesTotal + jobsTotal + invoicesTotal, orgMgr.Currency);

        var chart = new Chart
        {
            cols = new object[]
            {
                new { id = "Project", type = "string", label = "Project" },
                new { id = "total", type = "number", label = "total" }
            },
            rows = new object[]
            {
                new { c = new object[] { new { v = Resources.Resource.Quotes }, new { v = quotesTotal, f = Resources.Resource.Quotes+"," + String.Format("{0:N0}", quotesTotal) } } },
                new { c = new object[] { new { v = Resources.Resource.Jobs }, new { v = jobsTotal, f = Resources.Resource.Jobs + "," + String.Format("{0:N0}", jobsTotal) } } },
                new { c = new object[] { new { v = Resources.Resource.Invoices }, new { v = invoicesTotal, f = Resources.Resource.Invoices +  "," + String.Format("{0:N0}", invoicesTotal) } } }
            }
        };



        data = JsonConvert.SerializeObject(chart);
    }
}