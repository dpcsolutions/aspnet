﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Dashboard_Widgets_AbsenceRequest" Codebehind="AbsenceRequest.ascx.cs" %>
<div class="panel panel-body bg-<%= background %> panel-dash-widget <%= panelborder %>">
	<div class="media no-margin">
		<div class="media-body">
			<h4 class="no-margin text-semibold">
				<asp:HyperLink runat="server" NavigateUrl="~/Time/AbsenceRequests" class="text-<%= foreground %>"><%= Resources.Resource.AbsenceRequest %></asp:HyperLink></h4>
		

            <div runat="server">
				<div>
                    <span class="text-size-small  text-<%= foreground %> ">
                        <asp:HyperLink NavigateUrl="~/Time/AbsenceRequests?status=0" runat="server" ID="lnkExpenses1" >
                            <i class="icon-question6"></i>
                            <%= Resources.Resource.Awaiting %> : <%= AwaitingRequestsCount %>
                    </asp:HyperLink></span>
                </div>
                <div>
                    <span class="text-size-small  text-<%= foreground %> "><asp:HyperLink NavigateUrl="~/Time/AbsenceRequests?status=1" runat="server" ID="lnkExpenses2">
                            <i class="icon-checkmark"></i>
                            <%= Resources.Resource.Approved %> : <%= ApprovedRequestsCount %>
                    </asp:HyperLink></span>
                </div>
                <div>
                    <asp:HyperLink runat="server" NavigateUrl="~/Time/AbsenceRequests/AbsenceRequest.aspx" class="text-<%= foreground %>"><%= Resources.Resource.NewRequest %></asp:HyperLink>
                </div>
            </div>
		</div>

		<div class="media-right ">
			<i class="fa fa-umbrella-beach icon-3x text-<%= foreground %> <%= iconanimation %>"></i>
		</div>
	</div>
</div>