﻿using System;

public partial class Dashboard_Widgets_Scheduler : System.Web.UI.UserControl
{

    public string maincolor = "blue";

    public string background { get; set; }
    public string foreground { get; set; }
    public string iconanimation { get; set; }
    public string panelborder { get; set; }



    protected void Page_Load(object sender, EventArgs e)
    {
        long userId = PageHelper.GetUserId(Session);
        long orgId = PageHelper.GetCurrentOrganizationId(Session);

        var isdark = false;

        if (isdark)
        {
            background = "white";
            foreground = maincolor + "-800";
            iconanimation = "tada infinite animated";
            panelborder = "border-left-xlg border-left-" + foreground;

        }
        else
        {
            background = "light-grey";
            foreground = "slate";
            iconanimation = "";

        }

    }
}