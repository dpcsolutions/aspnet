﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Dashboard_Widgets_InvoicesTurnover" Codebehind="InvoicesTurnover.ascx.cs" %>
<div class="panel panel-body bg-<%= background %> panel-dash-widget <%= panelborder %>">
	<div class="media no-margin">
		<div class="media-body">
			<h4 class="no-margin text-semibold text-center"><asp:HyperLink runat="server" NavigateUrl="~/Projects/Invoices/" id="linkTitle"> </asp:HyperLink>
                <br/>
                <asp:HyperLink runat="server" NavigateUrl="~/Projects/Invoices/" id="linkSum"> </asp:HyperLink>
			</h4>
			
            <!-- Basic pie charts -->

									<div class="chart-container text-center">
										<div class="display-inline-block" id="google-pie-invoices-turnover"></div>
									</div>
					<!-- /basic pie charts -->
		</div>
	</div>
</div>
<script type="text/javascript">

    // Initialize chart
    google.load("visualization", "1", { packages: ["corechart"] });
    google.setOnLoadCallback(drawPie);


    // Chart settings    
    function drawPie() {

        
        var data = new google.visualization.DataTable(<%=data%>);
        
        // Options
        var options_pie = {
            height: 200,
            width: 200,
            /*legend: 'labeled',
            pieSliceText: 'none',*/
            legend: 'none',
            pieSliceText: 'value-and-percentage',
            pieSliceTextStyle: {
                color: 'black'
            },
            chartArea: {
               
                width: '95%',
                height: '95%'
            },
            colors: ['#ffd966', '#ffe699', '#fff2cc', '#d9d9d9']
        };


        // Instantiate and draw our chart, passing in some options.
        var pie = new google.visualization.PieChart($('#google-pie-invoices-turnover')[0]);
        pie.draw(data, options_pie);
    }
</script>