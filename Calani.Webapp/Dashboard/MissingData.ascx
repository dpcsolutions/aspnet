﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Dashboard_MissingData" Codebehind="MissingData.ascx.cs" %>
<div class="content content content-no-padding">

    <div class="panel panel-body">
        <div>
	        <div class="media-left media-middle">
			        <i class="icon-lifebuoy icon-3x text-muted no-edge-top"></i>
		        </div>

		        <div class="media-body">
			        <h6 class="media-heading text-semibold"><%= Resources.Resource.WhereToStart %></h6>
			        <span class="text-muted"><%= Resources.Resource.WhereToStart_description %></span>
		        </div>

		        <div class="media-right media-middle">
			        <a href="#" target="_blank" class="btn btn-primary" runat="server" id="lnkDoc">
                        <i class="icon-book"></i>
                        <%= Resources.Resource.UserManual %>
			        </a>
		        </div>
	        </div>
            <div class="panel-body" style="padding-bottom:20px">
	            <ul class="list list-icons" runat="server" id="list">
		
	            </ul>
            </div>
            <div runat="server" id="panelClose" class="" style="padding-bottom:20px">
                <asp:LinkButton runat="server" ID="btnClose" Text="<%$ Resources:Resource, Close %>" OnClick="btnClose_Click" />
            </div>
    </div>
</div>
