﻿using Calani.BusinessObjects.Admin;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Projects;
using log4net;

public partial class Login : System.Web.UI.Page
{
    private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
    public string LanguageSwitch { get; set; }

    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        LanguageSwitch = HtmlLanguageSwitch.Render(this.Page.Request.Url.AbsoluteUri, System.Threading.Thread.CurrentThread.CurrentCulture.ToString(), Page.ResolveClientUrl("~/assets/"));

        LabelCopyYear.Text = ConfigurationSettings.AppSettings["copyYear"];
        HyperLinkCopy.Text = ConfigurationSettings.AppSettings["copyrightText"];
        HyperLinkCopy.NavigateUrl = ConfigurationSettings.AppSettings["copyrightUrl"];

        this.Title = ConfigurationSettings.AppSettings["title"];

    }

    protected void btnStaffLogin_Click(object sender, EventArgs e)
    {
        Calani.BusinessObjects.Membership.LoginManager mgr = new Calani.BusinessObjects.Membership.LoginManager();
        var usr = mgr.LoginStaff(tbxLogin.Value, tbxPassword.Value, tbxStaffCode.Value);
        if(usr != null)
        {
            Session.Add("staff", "staff");
            Session.Add("primaryEmail", usr.primaryEmail);
            PageHelper.InitUserSessions(usr.primaryEmail, Session,true);
            
            FormsAuthentication.SetAuthCookie(usr.primaryEmail, false);
            Response.Redirect("SystemAdmin/ClientsList.aspx");


            /*

            // show organizations grid
            loginForm.Visible = false;
            loginStaff.Visible = false;
            staffSelector.Visible = true;



            Calani.BusinessObjects.Admin.PlaformCustomers pmgr = new Calani.BusinessObjects.Admin.PlaformCustomers();
            gridSelector.DataSource = pmgr.GetCustomers();
            gridSelector.DataBind();
            new Html5GridView(gridSelector, true, null);
            
             */
        }
    }

    private bool SendAuthToken(contacts user)
    {
        var result = true;
        var db = new CalaniEntities();
        var token = new dualauthenticationtokens
        {
            contactId = user.id,
            isUsed = false,
            validUntill = DateTime.Now.AddMinutes(10),
            token = GenerateRandomToken(8)
        };
        
        db.dualauthenticationtokens.Add(token);
        db.SaveChanges();
        var sendEmail = new SendEmail
        {
            subject = Resources.Resource.DualAuthEmailSubject,
            body = string.Format(Resources.Resource.DualAuthEmailBody, $"<b style=\"font-size: 18px;\">{token.token}</b>"),
            toEmail = user.primaryEmail,
            toContact = user.id,
            attachments = new List<long>()
        };
        
        var sender = new ProjectEmailDocumentsSender(); 
        sender.Email = sendEmail;
        
        var orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);

        if (!EmailSendingValidationManager.ValidateEmailLimit(orgId))
            return false;
        
        sender.OrganizationId = orgId;
        sender.From = "no-reply@gesmobile.contact";
        sender.FromUserId = user.id;
        
        var exMessage = "";
        try
        {
            sender.Send(false);
        }
        catch (Exception ex)
        {
            _log.Error(ex.Message, ex);
            result = false;
            exMessage = ex.Message;
        }
        finally
        {        
            EmailSendingValidationManager.CountEmailSent(orgId);
            _log.Info($"SendMail. subject:{sendEmail.subject},  orgId:{orgId}, From:{sender.From}, userId:{sender.FromUserId}, result: {(result?"success":exMessage)}");
        }
        
        
        return result;
    }

    protected void btnDualAuthLogin_Click(object sender, EventArgs e)
    {
        Calani.BusinessObjects.Membership.LoginManager mgr = new Calani.BusinessObjects.Membership.LoginManager();

        var usr = mgr.Login(tbxLogin.Value, tbxPassword.Value);
        if (usr == null) return;
        
        var db = new CalaniEntities();
        var userToken = tbxDualAuthCode.Value;
        
        var token = db.dualauthenticationtokens.FirstOrDefault(x =>
            x.token == userToken 
            && x.isUsed == false
            && x.contactId == usr.id
            && x.validUntill > DateTime.Now
        );
        
        if (token != null)
        {
            token.isUsed = true;
            db.SaveChanges();
            
            Session.Add("staff", "staff");
            Session.Add("primaryEmail", usr.primaryEmail);
            PageHelper.InitUserSessions(usr.primaryEmail, Session,true);
        
            FormsAuthentication.SetAuthCookie(usr.primaryEmail, false);
            Response.Redirect("SystemAdmin/ClientsList.aspx");
        }
        else
        {
            tbxDualAuthCode.Value = "";
            lblError2.Visible = true;
            lblAlert.Visible = false;
        }
    }
    
    protected void btnResendToken_Click(object sender, EventArgs e)
    {
        var mgr = new Calani.BusinessObjects.Membership.LoginManager();
        var usr = mgr.Login(tbxLogin.Value, tbxPassword.Value);
        
        if (SendAuthToken(usr)) {
            lblAlert.Visible = true;
        }
        
        btnResendToken.Enabled = false;
    }
    
    static string GenerateRandomToken(int length)
    {
        const string allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var random = new Random();
        return new string(Enumerable.Repeat(allowedChars, length)
            .Select(s => s[random.Next(s.Length)]).ToArray());
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        bool cookie = cbxRemember.Checked;

        Calani.BusinessObjects.Membership.LoginManager mgr = new Calani.BusinessObjects.Membership.LoginManager();

        try
        {
            var usr = mgr.Login(tbxLogin.Value, tbxPassword.Value);
            if (usr != null)
            {
                if (usr.type > 90)
                {
                    cookie = false;

                    PageHelper p = new PageHelper(this);
                    PageHelper.InitUserSessions(usr.primaryEmail, Session);
                    
                    loginForm.Visible = false;
                    loginStaff.Visible = true;
                }
                else if (usr.enableDualAuthentication)
                {
                    PageHelper p = new PageHelper(this);
                    PageHelper.InitUserSessions(usr.primaryEmail, Session);
                    
                    var db = new CalaniEntities();
                    
                    var token = db.dualauthenticationtokens.FirstOrDefault(x =>
                        x.isUsed == false
                        && x.contactId == usr.id
                        && x.validUntill > DateTime.Now
                    );

                    loginForm.Visible = false;
                    dualAuthLogin.Visible = true;
                    
                    if (token == null)
                    {
                        if (SendAuthToken(usr))
                        {
                            lblAlert.Visible = true;
                        }
                    }
                }
                else
                {
                    PageHelper p = new PageHelper(this);
                    PageHelper.InitUserSessions(usr.primaryEmail, Session);

                    if (usr.organizationId.HasValue)
                    {
                        var tMngr = new TutorialsManager();
                        var isNewTutorials = tMngr.IsNewTutorials(usr.id);

                        PageHelper.IsNewTutorials = isNewTutorials;
                    }


                    if (PageHelper.GetUserType(Session) == Calani.BusinessObjects.Contacts.ContactTypeEnum.Partner_Manager)
                    {
                        FormsAuthentication.SetAuthCookie(usr.primaryEmail, cookie);
                        Response.Redirect("~/PartnershipMgr");
                    }
                    else
                    {
                        FormsAuthentication.RedirectFromLoginPage(usr.primaryEmail, cookie);
                    }
                }
                lblError.Visible = false;

                ph.UpdateSubscription();

            }
            else
            {
                tbxPassword.Value = "";
                lblError.Visible = true;
                
            }
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            
        }
    }

    protected void gridSelector_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if(e.CommandName == "Use")
        {
            long orgId = 0;
            if(long.TryParse(e.CommandArgument.ToString(), out orgId))
            {
                Calani.BusinessObjects.Membership.LoginManager mgr = new Calani.BusinessObjects.Membership.LoginManager();
                var usr = mgr.LoginStaff(tbxLogin.Value, tbxPassword.Value, tbxStaffCode.Value);

                PageHelper p = new PageHelper(this);
                PageHelper.InitUserSessions(usr.primaryEmail, Session, true);
                Session["UserOrganizationId"] = orgId.ToString();
                Session["UserType"] = Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin;
                Session["UserType2"] = Calani.BusinessObjects.Contacts.ContactTypeEnum.PlatformAdmin_Support;
                Session["UserName"] = "[STAFF] " + Session["UserName"];
                FormsAuthentication.RedirectFromLoginPage(usr.primaryEmail, false);
                
            }
        }
           
    }
}