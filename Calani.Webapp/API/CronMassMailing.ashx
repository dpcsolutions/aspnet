﻿<%@ WebHandler Language="C#" Class="CronMassMailing" %>

using System;
using System.Web;
using System.Collections.Generic;
using System.Linq;

public class CronMassMailing : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        context.Response.Write("Massmailing");

        List<Calani.BusinessObjects.Model.contacts> contacts = null;

        Calani.BusinessObjects.MassMailing.MassMailingScheduler schd = new Calani.BusinessObjects.MassMailing.MassMailingScheduler();

        List<string> mails = new List<string>();

      

       /* contacts = schd.QueryNeverConnectedSinceSubscription(6);
        schd.Send(contacts, mails, "massmail_neverconnected6daysaftersub", "ges_07_sans_connexion_6jours.html");
        mails.AddRange((from r in contacts select r.primaryEmail).Distinct().ToList());

        contacts = schd.QueryNeverConnectedSinceSubscription(2);
        schd.Send(contacts, mails, "massmail_neverconnected2daysaftersub", "ges_06_sans_connexion_2jours.html");
        mails.AddRange((from r in contacts select r.primaryEmail).Distinct().ToList());*/

        contacts = schd.QueryExpirationDays(-60);
        schd.Send(contacts, null, "massmail_renewtrial", "ges_13_60_days_to_expire.txt");
        mails.AddRange((from r in contacts select r.primaryEmail).Distinct().ToList());

        var activeOrgs = schd.QueryActiveOrgIds();

        foreach (var orgId in activeOrgs)
        {
            schd.QueryAndSendNotValidatedTimeSheets(orgId,-7);
        }

        /*contacts = schd.QueryEndOfTrialDays(3, true);
       schd.Send(contacts, mails, "massmail_trialend", "ges_12_pas_abo_fin_essai.html");
       mails.AddRange((from r in contacts select r.primaryEmail).Distinct().ToList());

       contacts = schd.QueryEndOfTrialDays(0, true);
       schd.Send(contacts, mails, "massmail_renewtrial", "ges_11_prolongation_essai.html");
       mails.AddRange((from r in contacts select r.primaryEmail).Distinct().ToList());

       contacts = schd.QueryLastLoginFromDays(6);
       schd.Send(contacts, mails, "massmail_lastloginsince6days", "ges_10_derniere_connexion_6jours.html");
       mails.AddRange((from r in contacts select r.primaryEmail).Distinct().ToList());

       contacts = schd.QueryLastLoginFromDays(3);
       schd.Send(contacts, mails, "massmail_lastloginsince3days", "ges_09_derniere_connexion_3jours.html");
       mails.AddRange((from r in contacts select r.primaryEmail).Distinct().ToList());

       contacts = schd.QueryNeverConnectedSinceSubscription(15);
       schd.Send(contacts, mails, "massmail_neverconnected15daysaftersub", "ges_08_prolongation.html");
       mails.AddRange((from r in contacts select r.primaryEmail).Distinct().ToList()); contacts = schd.QueryEndOfTrialDays(-1, true);
       schd.Send(contacts, mails, "massmail_endtrial15days", "ges_04_rappel2_fin_essai.html");
       mails.AddRange((from r in contacts select r.primaryEmail).Distinct().ToList());

       contacts = schd.QueryEndOfTrialDays(-4, null);
       schd.Send(contacts, mails, "massmail_endtrial11days", "ges_03_rappel1_fin_essai.html");
       mails.AddRange((from r in contacts select r.primaryEmail).Distinct().ToList());*/
        
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}