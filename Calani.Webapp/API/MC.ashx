﻿<%@ WebHandler Language="C#" Class="MC" %>

using System;
using System.Web;

public class MC : IHttpHandler
{

    public void ProcessRequest(HttpContext context) {
        context.Response.ContentType = "text/plain";




        string test = context.Request.Params["q"];
        try
        {
            Calani.BusinessObjects.MailChimp.MailChimp mc = new Calani.BusinessObjects.MailChimp.MailChimp(System.Configuration.ConfigurationManager.AppSettings["MailChimpToken"],
                System.Configuration.ConfigurationManager.AppSettings["MailChimpDataCenter"]);
            string ret = mc.AddSubscriber(
                System.Configuration.ConfigurationManager.AppSettings["MailChimpListId"],
                test,
                "TEST",
                "TEST");
            context.Response.Write("Ret:" + ret);
        } catch (Exception ex)
        {
            context.Response.Write("Error:");
            context.Response.Write(ex.Message);
            context.Response.Write(ex.StackTrace);
        }

    }


    public bool IsReusable {
        get {
            return false;
        }
    }

}