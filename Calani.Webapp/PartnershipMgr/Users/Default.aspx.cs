﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PartnershipMgr_Users_Default : System.Web.UI.Page
{
    public PageHelper ph { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        ph = new PageHelper(this);

        if (!IsPostBack)
        {
            Calani.BusinessObjects.Partnership.PartnershipProviderManager mgr = new Calani.BusinessObjects.Partnership.PartnershipProviderManager(ph.CurrentOrganizationId);
            grid.DataSource = mgr.ListUsers();
            new Html5GridView(grid);
        }
    }
}