﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PartnershipMgr_Users_Invite : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);

        
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) Load();
    }


    private void Load()
    {
        Calani.BusinessObjects.Partnership.PartnershipProviderManager mgr = new Calani.BusinessObjects.Partnership.PartnershipProviderManager(ph.CurrentOrganizationId);
        var codes = mgr.ListCodes();
        List<string> available = new List<string>();
        foreach (var code in codes)
        {
            if (code.Limit == 0)
            {
                available.Add(code.Code);
            }
            else if(code.UsersCount < code.Limit)
            {
                available.Add(code.Code);
            }
        }
        ddlCode.DataSource = available;
        ddlCode.DataBind();
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        ddlCode.Enabled = false;
        tbxEmail.Disabled = true;

        panelEdit.Visible = false;
        panelResult.Visible = true;
        notif_success.Visible = false;
        notif_error.Visible = false;
        lblResAccount.Text = "";
        lblResCode.Text = "";
        lblResEmail.Text = "";
        lblResSent.Text = "";

        string code = ddlCode.SelectedValue;

        Calani.BusinessObjects.Partnership.PartnershipProviderManager mgr = new Calani.BusinessObjects.Partnership.PartnershipProviderManager(ph.CurrentOrganizationId);
        var res = mgr.Invite(tbxEmail.Value, code, Server.MapPath("~/App_Data/dbschema/common/common.sql"));

        if(res.Result == Calani.BusinessObjects.Partnership.PartnerInvitationStatus.NotValidEmail)
        {
            notif_error.Visible = true;
            notif_error_title.Text = "Erreur";
            notif_error_text.Text = "L'adresse email est invalide";
            lblResEmail.Text = tbxEmail.Value;

        }
        else if(res.Result == Calani.BusinessObjects.Partnership.PartnerInvitationStatus.ExistingAccountIncompatible)
        {
            notif_error.Visible = true;
            notif_error_title.Text = "Erreur";
            notif_error_text.Text = "L'email correspond déja à un rôle sur GesMobile incompatible avec votre invitation. Essayez avec un autre email ?";
            lblResEmail.Text = tbxEmail.Value;
        }
        else if (res.Result == Calani.BusinessObjects.Partnership.PartnerInvitationStatus.CodeSent)
        {
            notif_success.Visible = true;
            notif_success_text.Text = "Invité!";
            notif_error_text.Text = "L'email correspondait déja à une entreprise inscrite sur GesMobile. Votre code partenaire vient d'être envoyé à l'utilisateur pour lui proposer de se rattacher à votre partenariat.";
            lblResEmail.Text = tbxEmail.Value;
            lblResCode.Text = code;
            lblResSent.Text = "Code partenaire";
            lblResAccount.Text = "Déja inscrit";
        }
        else if (res.Result == Calani.BusinessObjects.Partnership.PartnerInvitationStatus.AccountCreated)
        {
            notif_success.Visible = true;
            notif_success_text.Text = "Invité!";
            notif_error_text.Text = "Un nouveau compte GesMobile vient d'être créé pour cet email. Un email vient d'être envoyé à l'utilisateur pour qu'il définisse son mot de passe.";
            lblResEmail.Text = tbxEmail.Value;
            lblResCode.Text = code;
            lblResSent.Text = "Définir mot de passe";
            lblResAccount.Text = "Nouveau";
        }
        else
        {
            notif_success.Visible = true;
            notif_success_text.Text = "Erreur";
            notif_error_text.Text = "Invitation impossible suite à une erreur technique. Merci de contacter le support GesMobile.";
            lblResEmail.Text = tbxEmail.Value;
        }
    }
}