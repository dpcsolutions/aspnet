﻿<%@ Page Title="Users" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="PartnershipMgr_Users_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">


        <div class="content">
					<div>
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
										<strong><%= ph.CMP.Name %></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

                            

							<div class="panel-body">
                                <asp:GridView runat="server" ID="grid" AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:BoundField DataField="Id" HeaderText="#"   />
                                        <asp:BoundField DataField="Code" HeaderText="Code"   />
                                        <asp:BoundField DataField="OrganizationName" HeaderText="OrganizationName"   />
                                        <asp:BoundField DataField="Email" HeaderText="Email"   />
                                        <asp:BoundField DataField="Phone" HeaderText="Phone"   />
                                        <asp:BoundField DataField="SubscriptionStart" HeaderText="SubscriptionStart"   />
                                        <asp:BoundField DataField="Users" HeaderText="Users"   />

                                        
                                    </Columns>
                                </asp:GridView>
                            </div><!-- panel-body -->

                            <div class="panel-footer text-right no-padding">
						    	<div class="row" style="margin:10px">
						    		<a href="Invite.aspx" class="btn btn-default"><i class="icon-plus2 position-left"></i> 
                                        <%= Resources.Resource.Invite %>
						    		</a>
						    	</div>
							</div><!-- panel-footer -->

                            

                        </div>
                    </div>
        </div><!-- /content -->

    </form>
</asp:Content>

