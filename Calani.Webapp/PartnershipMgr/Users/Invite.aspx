﻿<%@ Page Title="Invite" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="PartnershipMgr_Users_Invite" Codebehind="Invite.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">

        <div class="content">

		    <div class="row">

                <!-- left column -->
			    <div class="col-sm-12 col-md-9">


                        <asp:Panel runat="server" ID="panelResult" class="panel panel-white" Visible="false">
						    <div class="panel-heading">
							    <h6 class="panel-title"><asp:Label runat="server" ID="Label1" Text='<%$ Resources:Resource, Invite %>' /></h6>
						    </div>

                            <p runat="server" id="notif_success" visible="false">
                                <div class="alert alert-success no-border">
                                    <asp:Label runat="server" ID="notif_success_title" CssClass="text-semibold"></asp:Label>
                                    <asp:Label runat="server" ID="notif_success_text"></asp:Label>
								</div>
                            </p>
                            <p runat="server" id="notif_error" visible="false">
                                <div class="alert alert-danger no-border" >
									<asp:Label runat="server" ID="notif_error_title" CssClass="text-semibold"></asp:Label>
                                    <asp:Label runat="server" ID="notif_error_text"></asp:Label>
								</div>
                            </p>
						    <div class="panel-body">
                                <table class="table">
                                    <tr>
                                        <td>Email</td>
                                        <td><asp:Label runat="server" ID="lblResEmail"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Code</td>
                                        <td><asp:Label runat="server" ID="lblResCode"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Account</td>
                                        <td><asp:Label runat="server" ID="lblResAccount"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>Sent</td>
                                        <td><asp:Label runat="server" ID="lblResSent"></asp:Label></td>
                                    </tr>
                                </table>
                            </div>

                            <p style="text-align:center;">
                                <a href="Invite.aspx"><%= Resources.Resource.NewInvitation %></a>
                            </p>
                       </asp:Panel>
                        
					    
					    <asp:Panel runat="server" ID="panelEdit" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><asp:Label runat="server" ID="lblTitle" Text='<%$ Resources:Resource, Invite %>' /></h6>
						    </div>

						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">

                                        <!-- email -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Email %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
												    <input type="text" class="form-control"  runat="server" id="tbxEmail" />
											    </div>
										    </div>
									    </div>
                                        <!-- /email -->

                                        <!-- code -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Code %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-ticket"></i></span>												    
                                                    <asp:DropDownList runat="server" ID="ddlCode" CssClass="form-controls select">

                                                    </asp:DropDownList>
											    </div>
										    </div>
									    </div>
                                        <!-- /code -->

                                        </fieldset>


                                    <!-- buttonsaction -->
								    <div class="buttonsaction text-right">
										<label class="control-label col-lg-2"></label>
										<div class="col-lg-10">
											<div class="btn-group">
                                                <asp:LinkButton runat="server" ID="btnSend" class="btn btn-primary" OnClick="btnSend_Click">
                                                    <i class="fa fa-envelope position-left"></i> <%= Resources.Resource.VerifyAndSend %>
                                                </asp:LinkButton>
                                                
									            
											</div>
										</div>
									</div>
                                    <!-- /buttonsaction -->
                                    </div>
                                </div>

                        </asp:Panel>

                    </div>

                </div>

            </div>

        </form>
</asp:Content>

