﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PartnershipMgr_Invoices_Default : System.Web.UI.Page
{
    public PageHelper ph { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        ph = new PageHelper(this);

        if (!IsPostBack)
        {
            BindList();

        }
    }

    private void BindList()
    {
        Calani.BusinessObjects.Partnership.PartnershipInvoicesManager mgr = new Calani.BusinessObjects.Partnership.PartnershipInvoicesManager(ph.CurrentOrganizationId);
        var listMonthes = mgr.ExtractData(false); // pour le combo

        var debugcomplet = mgr.ExtractData(false); // pour le combo
        ddlMonths.Items.Clear();
        foreach (var item in listMonthes)
        {
            string txt = new DateTime(item.Year, item.Month, 1).ToString("MMMM yyyy");

            ddlMonths.Items.Add(new ListItem
            {
                Text = txt,
                Value = item.Year + "-" + item.Month
            });
        }
    }

    protected void btnGet_Click(object sender, EventArgs e)
    {
        string val = ddlMonths.SelectedValue;
        if (!String.IsNullOrWhiteSpace(val))
        {
            Calani.BusinessObjects.Partnership.PartnershipInvoicesManager mgr = new Calani.BusinessObjects.Partnership.PartnershipInvoicesManager(ph.CurrentOrganizationId);
            int year = Convert.ToInt32(val.Split('-').FirstOrDefault());
            int month = Convert.ToInt32(val.Split('-').LastOrDefault());

            var data = mgr.ExtractMonth(year, month);

            DataGrid.DataSource = data.Items;
            DataGrid.DataBind();

            new Html5GridView(DataGrid);

            double total = (from r in data.Items select r.Total).Sum();

            tbxTotal.Text = String.Format("{0:F2}", total);
        }
    }
}