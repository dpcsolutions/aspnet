﻿<%@ Page Title="Invoices" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="PartnershipMgr_Invoices_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<form runat="server">


        <div class="content">
					<div>
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
										<strong><%= ph.CMP.Name %></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

                            

							<div class="panel-body">
                                

                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        
                                        <asp:DropDownList runat="server" ID="ddlMonths" CssClass="form-control select">
                   
                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-sm-12 col-md-6">
                                        
						    		    <asp:LinkButton runat="server" ID="btnGet" OnClick="btnGet_Click" CssClass="btn btn-default"><i class="icon-database-refresh position-left"></i> 
                                            <%= Resources.Resource.Get %>
						    		    </asp:LinkButton>
                                     </div>
						    	</div>


                            </div><!-- panel-body -->
                            

                            <div class="panel-body">
                                <asp:GridView runat="server" ID="DataGrid" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Customer">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="LabelCustomerId"  Text="<%# Bind('CustomerId') %>"></asp:Label>
                                                - <asp:Label runat="server" ID="LabelCustomerName"  Text="<%# Bind('CustomerName') %>"></asp:Label>
                                                <br /><em><asp:Label runat="server" ID="LabelCustomerOwnerEmail"  Text="<%# Bind('CustomerOwnerEmail') %>"></asp:Label></em>


                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Customer">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="LabelCode"  Text="<%# Bind('Code') %>"></asp:Label>
                                                <br />
                                                <em>
                                                    <%= Resources.Resource.Plan %> :<asp:Label runat="server" ID="LabelBasePrice"  Text='<%# Eval("BasePrice","{0:F2}") %>'></asp:Label>
                                                    + <%= Resources.Resource.ExtraLicense %>: <asp:Label runat="server" ID="LabelExtraUserPrice"  Text='<%# Eval("ExtraUserPrice","{0:F2}") %>'></asp:Label>
                                                </em>


                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:BoundField DataField="ExtraUserLicences" HeaderText="ExtraLicences"
                                            ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right" />

                                        <asp:BoundField DataField="Total" HeaderText="Total"
                                            ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right" DataFormatString="{0:F2}" />

                                        
                                     </Columns>
                                </asp:GridView>
                            </div>

                            <div class="panel-body">
                                <div class="col-sm-6 col-md-6">
                                </div>
                                <div class="col-sm-3 col-md-3 text-right">
                                    <label><strong><%= Resources.Resource.Total %>:</strong></label>
                                </div>
                                <div class="col-sm-3 col-md-3">
                                    <asp:TextBox runat="server" ID="tbxTotal" CssClass="form-control text-right"></asp:TextBox>
                                </div>
                            </div>

                        </div>
                    </div>
        </div><!-- /content -->

    </form>
</asp:Content>

