﻿<%@ Page Title="Codes" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="PartnershipMgr_Codes_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">


        <div class="content">
					<div>
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
										<strong><%= ph.CMP.Name %></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

                            

							<div class="panel-body">
                                <asp:GridView runat="server" ID="grid" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:HyperLinkField DataTextField="Code" HeaderText="Code" DataNavigateUrlFields="Code" DataNavigateUrlFormatString="Edit.aspx?id={0}"/>
                                        <asp:BoundField DataField="Limit" HeaderText="Limit"   />
                                        <asp:BoundField DataField="UsersCount" HeaderText="UsersCount"   />
                                        <asp:BoundField DataField="PlanPrice" HeaderText="PlanPrice"   />
                                        <asp:BoundField DataField="ExtraUserPrice" HeaderText="ExtraUserPrice"   />
                                        <asp:TemplateField HeaderText="Active" >
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" Checked='<%# Bind("Active") %>' ID="c" Enabled="False"   />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        
                                    </Columns>
                                </asp:GridView>
                            </div><!-- panel-body -->

                            <div class="panel-footer text-right no-padding">
						    	<div class="row" style="margin:10px">
						    		<a href="Edit.aspx" class="btn btn-default"><i class="icon-plus2 position-left"></i> 
                                        <%= Resources.Resource.Add_new %>
                                        <%= Resources.Resource.Code %>
						    		</a>
						    	</div>
							</div><!-- panel-footer -->

                        </div>
                    </div>
        </div><!-- /content -->

    </form>
</asp:Content>

