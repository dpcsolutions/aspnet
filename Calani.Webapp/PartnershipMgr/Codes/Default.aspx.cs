﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PartnershipMgr_Codes_Default : System.Web.UI.Page
{
    public PageHelper ph { get; private set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        ph = new PageHelper(this);

        if(!IsPostBack)
        {
            Load();
        }
    }

    private void Load()
    {
        Calani.BusinessObjects.Partnership.PartnershipProviderManager mgr = new Calani.BusinessObjects.Partnership.PartnershipProviderManager(ph.CurrentOrganizationId);
        var codes = mgr.ListCodes();

        grid.DataSource = codes;
        new Html5GridView(grid);
    }
}