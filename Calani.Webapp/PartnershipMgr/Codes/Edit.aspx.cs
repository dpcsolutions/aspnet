﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PartnershipMgr_Codes_Edit : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ph.ItemLabel = Resources.Resource.Code;
        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
            btnDelete, btnMore, btnMoreDdl, lblTitle, linkBackList, linkNew);




        if (!IsPostBack && ph.CurrentTextId != null)
        {
            ph.SetStateEdit(Resources.Resource.Edit_var_id);

            Load();
        }

        if (!IsPostBack && ph.CurrentTextId == null)
        {
            New();
            ph.SetStateNew(Resources.Resource.Add_new_var);
        }
    }




    #region CRUD operations
    // Load from db and bind controls
    private void Load()
    {
        Calani.BusinessObjects.Partnership.PartnershipProviderManager mgr = new Calani.BusinessObjects.Partnership.PartnershipProviderManager(ph.CurrentOrganizationId);
        var item = mgr.Get(ph.CurrentTextId);
        if (item != null)
        {
            tbxCode.Value = item.Code;
            tbxCode.Disabled = true;

            cbxActive.Checked = item.Active;

            tbxLimit.Value = item.Limit.ToString();

            tbxQueryCount.Value = item.UsersCount.ToString();

            tbxQueryPlanPrice.Value = item.PlanPrice.ToString();
            tbxQueryExtraUserPrice.Value = item.ExtraUserPrice.ToString();


            btnMore.Visible = (item.UsersCount == 0);
            btnMoreDdl.Visible = (item.UsersCount == 0);
            
        }
    }

    private void New()
    {

        Calani.BusinessObjects.Partnership.PartnershipProviderManager mgr = new Calani.BusinessObjects.Partnership.PartnershipProviderManager(ph.CurrentOrganizationId);


        tbxCode.Value = "";
        tbxCode.Disabled = false;

        cbxActive.Checked = true;

        tbxLimit.Value = "0";

        tbxQueryCount.Value = "0";

        tbxQueryPlanPrice.Value = mgr.DefaultPlanPrice.ToString();
        tbxQueryExtraUserPrice.Value = mgr.DefaultUserPrice.ToString();


        btnMore.Visible = false;
        btnMoreDdl.Visible = false;
    }

    // Create a new db record from controls values
    private void Update()
    {

        Calani.BusinessObjects.Partnership.PartnershipProviderManager mgr = new Calani.BusinessObjects.Partnership.PartnershipProviderManager(ph.CurrentOrganizationId);

        int limit = 0;
        int.TryParse(tbxLimit.Value, out limit);

        bool success = mgr.UpdateCode(ph.CurrentTextId,
                        limit,
                        cbxActive.Checked);
        if (success)
        {
            ph.DisplaySuccess(Resources.Resource.Updated_var);
            Load();
        }
        else ph.DisplayError("Error. Please contact support");
    }


    // Update existing db record from new controls values
    private void Create()
    {
        Calani.BusinessObjects.Partnership.PartnershipProviderManager mgr = new Calani.BusinessObjects.Partnership.PartnershipProviderManager(ph.CurrentOrganizationId);

        int limit = 0;
        int.TryParse(tbxLimit.Value, out limit);

        bool success = false;
        string error = "Error. Please contact support";

        try
        {
            success = mgr.NewCode(tbxCode.Value,
                            limit,
                            cbxActive.Checked);
        }
        catch (Exception ex)
        {
            success = false;
            error = ex.Message;
        }

        if (success)
        {
            ph.DisplaySuccess(Resources.Resource.Created_var);
            btnSave.Enabled = false;
        }
        else ph.DisplayError(error);
    }

    protected void Delete()
    {
        Calani.BusinessObjects.Partnership.PartnershipProviderManager mgr = new Calani.BusinessObjects.Partnership.PartnershipProviderManager(ph.CurrentOrganizationId);

       
        var ret = mgr.Delete(ph.CurrentTextId);
        if (ret)
        {
            panelEdit.Visible = false;
            ph.DisplaySuccess(Resources.Resource.Deleted_var);
        }
    }
    #endregion



    #region Buttons (triggers)
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ph.CurrentTextId != null) Update();
        else Create();
    }


    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (ph.CurrentTextId != null) Delete();
    }
    #endregion





}