﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="PartnershipMgr_Codes_Edit" Codebehind="Edit.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <form runat="server">

        <div class="content">

		    <div class="row">

                <!-- left column -->
			    <div class="col-sm-12 col-md-9">


                        <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Error %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblErrorMessage" />
						    </div>
					    </asp:Panel><!-- panelError -->

                        <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Done %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblSuccess" />
                                <br />
                                <strong><a href="." id="linkBackList" runat="server"></a></strong>
                                    <%= Resources.Resource.or %>
                                <a href="Edit.aspx" id="linkNew" runat="server"></a>.
						    </div>
					    </asp:Panel><!-- panelSuccess -->
					    
					    <asp:Panel runat="server" ID="panelEdit" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><asp:Label runat="server" ID="lblTitle" /></h6>
						    </div>

						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">

                                        <!-- name -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Code %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-font"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Code %>' runat="server" id="tbxCode" />
											    </div>
										    </div>
									    </div>
                                        <!-- /name -->

										
												


								    </fieldset>

                                    <legend class="text-bold"><%= Resources.Resource.Distribution %></legend>


                                    <fieldset class="content-group">

                                        <!-- active -->
									    <div class="form-group">
                                            <label class="control-label col-lg-2"><%= Resources.Resource.Active %></label>
										    <div class="col-lg-10">
								                <div class="input-group">
										            <span class="input-group-addon">
                                                        <i class="icon-ticket"></i>
                                                        
										            </span> 
                                                    <span class="form-control">
										                <asp:CheckBox runat="server" CssClass="styled" ID="cbxActive" />
                                                        <%= Resources.Resource.RegistrationsOpened %>
                                                    </span>
									            </div>
                                            </div>
							            </div>
                                        <!-- /active -->


                                        <!-- name -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.LimitExplained %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-ticket"></i></span>
												    <input type="number" class="form-control" placeholder='<%$ Resources:Resource, Limit %>' runat="server" id="tbxLimit" />
											    </div>
										    </div>
									    </div>
                                        <!-- /name -->

										
												


								    </fieldset>


                                    <legend class="text-bold"><%= Resources.Resource.Statistics %></legend>
                                    <fieldset>


                                        <!-- count -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.UsersCount %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-users"></i></span>
												    <input type="text" class="form-control"  runat="server" id="tbxQueryCount" disabled="disabled" />
											    </div>
										    </div>
									    </div>
                                        <!-- /count -->

                                    </fieldset>


                                    <legend class="text-bold"><%= Resources.Resource.Price %></legend>
                                    <fieldset>

                                        

                                        <!-- count -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.PlanPrice %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-money"></i></span>
												    <input type="text" class="form-control"  runat="server" id="tbxQueryPlanPrice" disabled="disabled" />
											    </div>
										    </div>
									    </div>
                                        <!-- /count -->

                                        <!-- count -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.ExtraUserPrice %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-money"></i></span>
												    <input type="text" class="form-control"  runat="server" id="tbxQueryExtraUserPrice" disabled="disabled" />
											    </div>
										    </div>
									    </div>
                                        <!-- /count -->

                                    </fieldset>



                                    <!-- buttonsaction -->
								    <div class="buttonsaction text-right">
										<label class="control-label col-lg-2"></label>
										<div class="col-lg-10">
											<div class="btn-group">
                                                <asp:LinkButton runat="server" ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click">
                                                    <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                                </asp:LinkButton>
                                                
									            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore"><span class="caret"></span></button>
									            <ul class="dropdown-menu dropdown-menu-right" runat="server" id="btnMoreDdl">
                                                    <li><a href="javascript:void(0)" data-toggle="modal" data-target="#modal_default"><i class="fa fa-trash"></i> <%= Resources.Resource.Delete %></a></li>
												</ul>
											</div>
										</div>
									</div>
                                    <!-- /buttonsaction -->


							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->
					    
           


			    </div>
			    <!-- /left column -->
                

                <!-- right column -->
                <div class="col-sm-12 col-md-6">

			    </div>
                <!-- /right column -->
						

		    </div> <!-- /row -->


					
            <!-- basic modal: confirm delete -->
		    <div id="modal_default" class="modal fade">
			    <div class="modal-dialog">
				    <div class="modal-content">
					    <div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal">&times;</button>
						    <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
					    </div>

					    <div class="modal-body">
						    <h6 class="text-semibold"><%= Resources.Resource.Are_You_Sure_To_Delete %></h6>
						    <p><%= Resources.Resource.Delete_Is_Irreversible %></p>

					    </div>

					    <div class="modal-footer">
						    <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
                            <asp:Button runat="server" ID="btnDelete" OnClick="btnDelete_Click" CssClass="btn btn-primary" Text="<%$ Resources:Resource, Yes_Delete %>" />

					    </div>
				    </div>
			    </div>
		    </div>
		    <!-- /basic modal -->



        </div><!-- /content -->

    </form>

</asp:Content>

