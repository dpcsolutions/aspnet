﻿<%@ WebHandler Language="C#" Class="UploadDocument" %>

using System;
using System.Web;
using Calani.BusinessObjects.Attachments;

public class UploadDocument : IHttpHandler, System.Web.SessionState.IRequiresSessionState {

    public void ProcessRequest (HttpContext context) {

        long projectId = -1;
        long.TryParse(context.Request.Params["project"], out projectId);

        Calani.BusinessObjects.Attachments.AttachmentsManager mgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
        mgr.OrganizationId = PageHelper.GetCurrentOrganizationId(context.Session);

        foreach (string s in context.Request.Files)
        {
            HttpPostedFile file = context.Request.Files[s];
            string fileName = file.FileName;
            
            if (!string.IsNullOrEmpty(fileName))
            {
                FileTypeDefinition ftype = null;
                var name = mgr.UploadAttachmentTemp(null,null,file,null, ref ftype);
                context.Response.ContentType = "text/plain";
                context.Response.Write(name);
            }
        }
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}