﻿SELECT 

organizations.id AS id_entreprise,
organizations.name AS entreprise,
organizations.website AS entreptrise_site,
organizations.userLimit AS licences_utilisateurs,
organizations.activationCode IS NULL AS compte_active,
organizations.activationDate AS compte_date_activation,
organizations.subscriptionEnd AS fin_periode_essai,
organizations.subscriptionId AS id_paiement,
organizations.knowus AS connu_comment,


contacts.type LIKE 1 AS employe,
contacts.type LIKE 2 AS admin,
contacts.id AS id_utilisateur,
contacts.firstName AS prenom,
contacts.lastName AS nom,
/*contacts.dateOfBirth AS date_naissance,*/
contacts.primaryPhone AS tel,
contacts.primaryEmail AS email,
contacts.lastlogin AS derniereconnexion,

(SELECT COUNT(*) FROM clients WHERE clients.recordstatus = 0 AND organizationId = organizations.id) AS clients_enregistres,


(SELECT COUNT(*) FROM jobs WHERE jobs.recordstatus = 0 AND organizationId = organizations.id AND dateOpenJob IS NULL AND dateInvoicing IS NULL) AS devis_nombre,
(SELECT COUNT(*) FROM jobs WHERE jobs.recordstatus = 0 AND organizationId = organizations.id AND dateOpenJob IS NULL AND dateInvoicing IS NULL AND dateQuoteAccepted IS NOT NULL) AS devis_nombre_acceptes,
/*(SELECT MAX(dateQuoteSent) FROM jobs WHERE jobs.recordstatus = 0 AND  organizationId = organizations.id AND dateOpenJob IS NULL AND dateInvoicing IS NULL) AS devis_dernier_envoi,*/


(SELECT COUNT(*) FROM jobs WHERE jobs.recordstatus = 0 AND organizationId = organizations.id AND dateOpenJob IS NOT NULL AND dateInvoicing IS NULL) AS projets_nombre,
(SELECT COUNT(*) FROM jobs WHERE jobs.recordstatus = 0 AND organizationId = organizations.id AND dateOpenJob IS NOT NULL AND dateJobDone IS NOT NULL AND dateInvoicing IS NULL) AS projets_termines_nombre,
(SELECT COUNT(*) FROM jobs WHERE jobs.recordstatus = 0 AND organizationId = organizations.id AND dateOpenJob IS NOT NULL AND dateJobDone IS NULL AND dateInvoicing IS NULL) AS projets_encours_nombre,

(SELECT COUNT(*) FROM visits 
LEFT JOIN jobs AS jj ON (visits.jobId = jj.id)
WHERE jj.organizationId = organizations.id AND visits.recordStatus = 0
AND dateStart < NOW()) AS visites_passes_nombre,

(SELECT COUNT(*) FROM visits 
LEFT JOIN jobs AS jj ON (visits.jobId = jj.id)
WHERE jj.organizationId = organizations.id AND visits.recordStatus = 0
AND dateStart > NOW()) AS visites_futures_nombre,


(SELECT COUNT(*) FROM jobs WHERE jobs.recordstatus = 0 AND organizationId = organizations.id AND dateOpenJob IS NOT NULL AND dateInvoicing IS NOT NULL) AS factures_nombres,
(SELECT COUNT(*) FROM jobs WHERE jobs.recordstatus = 0 AND organizationId = organizations.id AND dateOpenJob IS NOT NULL AND dateInvoicing IS NOT NULL AND dateFullyPaid IS NOT NULL) AS factures_payes_nombres,
(SELECT COUNT(*) FROM jobs WHERE jobs.recordstatus = 0 AND organizationId = organizations.id AND dateOpenJob IS NOT NULL AND dateInvoicing IS NOT NULL AND dateInvoiceDue < NOW()) AS factures_retard_nombres,

(SELECT COUNT(*) FROM timesheets WHERE recordStatus = 0 AND organizationId = organizations.id AND acceptedDate IS NULL) AS timesheet_encours,
(SELECT COUNT(*) FROM timesheets WHERE recordStatus = 0 AND organizationId = organizations.id AND acceptedDate IS NOT NULL) AS timesheet_valides,

(SELECT COUNT(*) FROM expenses 
	LEFT JOIN contacts AS expcon ON (expcon.id = expenses.employeeId)
	WHERE expenses.recordStatus = 0 AND expcon.organizationId = organizations.id AND approvedmount IS NULL) AS expenses_encours,
(SELECT COUNT(*) FROM expenses 
	LEFT JOIN contacts AS expcon ON (expcon.id = expenses.employeeId)
	WHERE expenses.recordStatus = 0 AND expcon.organizationId = organizations.id AND approvedmount IS NOT NULL) AS expenses_validees



FROM contacts
LEFT JOIN organizations ON (organizations.id = contacts.organizationId)

WHERE (contacts.type = 1 /*admin*/ OR contacts.type = 2 /*employe*/)
AND contacts.recordStatus = 0 AND organizations .recordStatus = 0 

ORDER BY organizations.id, contacts.id