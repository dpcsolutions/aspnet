﻿-- taxes
-- taxes

INSERT INTO taxes(name, rate, organizationId) VALUES('TVA 8.1', 8.1, $$organizationId$$);
INSERT INTO taxes(name, rate, organizationId) VALUES('TVA 3.8', 3.8, $$organizationId$$);
INSERT INTO taxes(name, rate, organizationId) VALUES('TVA 2.6', 2.6, $$organizationId$$);

INSERT INTO work_schedule_profiles_rec (organization_id, name, rate, mo, tu, we, th, fr, sa, su, type) VALUES ($$organizationId$$, 'Férié', 100, 0, 0, 0, 0, 0, 0, 0, 14);

UPDATE organizations set delayQuote = 30, delayInvoice = 30 where id = $$organizationId$$;
-- services général
INSERT INTO services(name, internalId, unitPrice, taxId, description, organizationId, type, recordStatus) VALUES	
								('Marketing','MKT',0,NULL,'Activités de marketing', $$organizationId$$,2,0),
								('Vente','VNT',0,NULL,'Activités de vente', $$organizationId$$,2,0),
								('Ressources Humaines','RH',0,NULL,'Ressources humaines', $$organizationId$$,2,0),
								('Logistique','LOG',0,NULL,'Activités liées à la logistique', $$organizationId$$,2,0),
								('Inventaire','INV',0,NULL,'Activités liées à l\'inventaire', $$organizationId$$,2,0),
								('Administration','ADM',0,NULL,'Activités administratives', $$organizationId$$,2,0),
								('Finance','FIN',0,NULL,'Finance, comptabilité, facturation', $$organizationId$$,2,0),
								('Autre','AUT',0,NULL,'Diverses activités non spécifiques', $$organizationId$$,2,0);


-- services
/*
INSERT INTO services(name, internalId, unitPrice, taxId, description, organizationId, type) VALUES ('Windows installation', 'WIN', 150, 5, 'Install and activate Microsoft Windows on the computer with all updates.' , 1, 1);

-- client

INSERT INTO clients(companyName, organizationId) VALUES('Nestlé', 1);


-- org employees --
INSERT INTO `contacts` (`firstName`,`lastName`,`primaryPhone`,`secondaryPhone`,`primaryEmail`,`secondaryEmail`,`clientId`,`organizationId`,`type`,`password`,`status`,`role`,`title`,`initials`,`account`,`recordStatus`,`weeklyHours`) VALUES ('Sarah','Conor','+41 78 123 11 22',NULL,'sco@acme.ch',NULL,NULL,1,2,'9A8605F7C6C0C263E60F7E9F58DA56AFC9E18929',NULL,NULL,NULL,'SCO',NULL,0,42);

-- client contact --
INSERT INTO `contacts` (`firstName`,`lastName`,`primaryPhone`,`secondaryPhone`,`primaryEmail`,`secondaryEmail`,`clientId`,`organizationId`,`type`,`password`,`status`,`role`,`title`,`initials`,`account`,`recordStatus`) VALUES ('Janet','Bow','+99 99 999 99 99',NULL,'jbo@mycompan.ch',NULL,1,1,10,NULL,NULL,NULL,NULL,NULL,NULL,0);

-- org admin --
INSERT INTO `contacts` (`firstName`,`lastName`,`primaryPhone`,`secondaryPhone`,`primaryEmail`,`secondaryEmail`,`clientId`,`organizationId`,`type`,`password`,`status`,`role`,`title`,`initials`,`account`,`recordStatus`) VALUES ('Sebadmin','Delorg','+99 99 999 99 99',NULL,'test@test.ch',NULL,NULL,1,1,'9A8605F7C6C0C263E60F7E9F58DA56AFC9E18929', NULL,NULL,NULL,'SDE',NULL,0, NULL, NULL, NULL, 0, 'b97np2');
UPDATE organizations SET owner = 7 WHERE id = 1;

-- projects --
INSERT INTO `jobs` (`id`,`internalId`,`clientId`,`description`,`rating`,`discount`,`total`,`street`,`street2`,`nb`,`npa`,`city`,`country`,`sentToName`,`sentToEmail`,`internalNote`,`dateCreated`,`dateQuoteSent`,`dateQuoteAccepted`,`dateQuoteRefused`,`dateQuoteValidity`,`dateOpenJob`,`dateJobHeld`,`dateJobDone`,`dateJobValidity`,`dateInvoicing`,`dateFullyPaid`,`dateInvoiceDue`,`type`,`parentId`,`invoiceDue`,`contractualUnitsCount`,`organizationId`,`lastUpdate`,`recordStatus`) VALUES (1,'DRAFT',1,'Test draft 1',3,NULL,1100,'Rue du chat','','12',1071,'Chexbres','CH','John Doe','john.doe@chats.ch',NULL,'2017-02-05 11:00:00',NULL,NULL,NULL,'2019-01-01 00:00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,1,'2017-02-05 11:00:00',0);

-- visits --
INSERT INTO `visits` (`id`,`jobId`,`dateStart`,`dateEnd`,`visitStatus`,`recordStatus`) VALUES (1,13,'2018-02-04 08:00:00','2018-02-04 09:00:00',1,0);
INSERT INTO `visits_contacts` (`visitId`,`contactId`) VALUES (7,4);


-- adresses client and contacts --
INSERT INTO `addresses` (`street`, `nb`, `npa`, `city`, `state`, `country`, `clientId`, `organizationId`, `type`, `recordStatus`) VALUES ('Rue de la Gare', '2', '9999', 'WaterCity', 'VD', 'Suisse', '1', '1', '2', '0');

-- timesheet

INSERT INTO `timesheets` (`employeeId`, `year`, `week`, `sentDate`, `organizationId`, `recordStatus`) VALUES ('4', '2018', '1', '2018-01-03 12:00:00', '1', '0');


INSERT INTO `timesheetrecords` (`timesheetId`, `startDate`, `endDate`, `rate`, `duration`) VALUES ('1', '2018-01-02 08:00:00', '2018-01-02 10:00:00', '3', '2');

INSERT INTO `calani`.`visits_contacts` (`visitId`, `contactId`) VALUES ('4', '1');
*/
