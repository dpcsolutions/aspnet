CREATE TABLE `tasklists` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `internalId` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `clientid` bigint(20) NOT NULL,
  `contactId` bigint(20) NOT NULL,
  `jobId` bigint(20) NOT NULL,
  `isCompleted` tinyint(1) NOT NULL,
  `isTemplate` tinyint(1) NOT NULL DEFAULT '0',
  `cancelReason` text,
  `recordStatus` int(11) NOT NULL,
  `listStatus` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contactId` (`contactId`),
  KEY `clientid` (`clientid`),
  KEY `jobId` (`jobId`),
  CONSTRAINT `tasklists_ibfk_1` FOREIGN KEY (`contactId`) REFERENCES `contacts` (`id`),
  CONSTRAINT `tasklists_ibfk_2` FOREIGN KEY (`clientid`) REFERENCES `clients` (`id`),
  CONSTRAINT `tasklists_ibfk_3` FOREIGN KEY (`jobId`) REFERENCES `jobs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

CREATE TABLE `taskliststemplates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `taskListId` bigint(20) NOT NULL,
  `templateId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `taskListId` (`taskListId`),
  KEY `templateId` (`templateId`),
  CONSTRAINT `taskliststemplates_ibfk_1` FOREIGN KEY (`taskListId`) REFERENCES `tasklists` (`id`),
  CONSTRAINT `taskliststemplates_ibfk_2` FOREIGN KEY (`templateId`) REFERENCES `templates` (`templateId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

CREATE TABLE taskListAnswers
(
    id BIGINT(20) NOT NULL AUTO_INCREMENT,
    taskListId BIGINT(20) NOT NULL,
    questionId BIGINT(20) NOT NULL,
    answer VARCHAR(2048) NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (questionId) REFERENCES questions (questionId),
    FOREIGN KEY (taskListId) REFERENCES taskLists (id)
) ENGINE=InnoDB;

CREATE TABLE `jobs_templates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `jobId` bigint(20) NOT NULL,
  `templateId` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobId` (`jobId`),
  KEY `templateId` (`templateId`),
  CONSTRAINT `jobs_templates_ibfk_1` FOREIGN KEY (`jobId`) REFERENCES `jobs` (`id`),
  CONSTRAINT `jobs_templates_ibfk_2` FOREIGN KEY (`templateId`) REFERENCES `templates` (`templateId`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
