DROP PROCEDURE IF EXISTS ListTimesheetsExtForManager;
        DELIMITER //
CREATE PROCEDURE ListTimesheetsExtForManager(
    OrgId BIGINT,
    AlreadyValidated TINYINT,
    StartWeek INT,
    StartYear INT,
    EndWeek INT,
    EndYear INT,
    EmployeeId BIGINT,
    SheetType INT
)
BEGIN
    SELECT
        t.*,
        TRIM(CONCAT(c.lastName, ' ', c.firstName)) AS EmployeeName,
        c.initials AS EmployeeInitials,
        GetIso8601MondayOfWeek(t.year, t.week) AS sheetDate,
        CASE
            WHEN t.acceptedDate IS NULL THEN
                CASE
                    WHEN GetIso8601MondayOfWeek(t.year, t.week) BETWEEN
                        DATE_SUB(CURDATE(), INTERVAL (DAYOFWEEK(CURDATE()) - 1) DAY)
                        AND DATE_ADD(CURDATE(), INTERVAL (7 - DAYOFWEEK(CURDATE())) DAY)
                        THEN 'currentweek'
                    ELSE 'tovalidate'
                    END
            ELSE 'validated'
            END AS sheetStatus,
        c.weeklyHours AS AttendedDuration,
        GetDurationByTimesheetId(t.id) AS TotalDuration,
        GetCommentNumberTimesheetId(t.id) AS CommentNumber
    FROM timeSheets AS t
             INNER JOIN Contacts AS c ON t.employeeId = c.Id
    WHERE
        t.recordStatus = 0
      AND t.organizationId = OrgId
      AND (AlreadyValidated IS NULL
        OR (AlreadyValidated = 1 AND t.acceptedDate IS NOT NULL)
        OR (AlreadyValidated = 0 AND t.acceptedDate IS NULL))
      AND ((StartWeek < 0) OR (t.week >= StartWeek AND t.year >= StartYear))
      AND ((EndWeek < 0) OR (t.year < EndYear) OR (t.year = EndYear AND t.week <= EndWeek))
      AND ((EmployeeId < 0) OR (t.employeeId = EmployeeId));
END  //
 DELIMITER ;
