﻿CREATE TABLE `form_data` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `employeeId` BIGINT(20) DEFAULT NULL,
  `data` VARCHAR(800) DEFAULT NULL,
  `forDate` DATETIME DEFAULT NULL,
  `organizationId` BIGINT(20) DEFAULT NULL,
  `recordStatus` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_formdata_contacts_idx` (`employeeId`),
  KEY `fk_formdata_organizations_idx` (`organizationId`),
  CONSTRAINT `fk_formdata_contacts` FOREIGN KEY (`employeeId`) REFERENCES `contacts` (`id`),
  CONSTRAINT `fk_formdata_organizations` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=INNODB AUTO_INCREMENT=716 DEFAULT CHARSET=utf8;

ALTER TABLE `calani`.`form_data`   
	CHANGE `employeeId` `employeeId` BIGINT(20) NOT NULL,
	CHANGE `organizationId` `organizationId` BIGINT(20) NOT NULL;

ALTER TABLE `calani`.`partnership`   
	CHANGE `partner_invoicing` `partner_invoicing` TINYINT(1) UNSIGNED DEFAULT 0 NULL COMMENT 'if 1 the final user payment by braintree is disabled and replaced by a invoice for the customer',
	CHANGE `active` `active` TINYINT(1) UNSIGNED DEFAULT 1 NULL;

