CREATE TABLE IF NOT EXISTS calani.other_resources (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  code VARCHAR(50) NOT NULL,
  name VARCHAR(200) NOT NULL,
  description VARCHAR(500) NULL,
  weeklyHours DOUBLE NOT NULL,
  organizationId BIGINT(20) NOT NULL,
  recordStatus INT(11) NOT NULL DEFAULT 0,
  created_at DATETIME NOT NULL,
  created_by BIGINT(20) NOT NULL,
  updated_at DATETIME NULL,
  updated_by BIGINT(20) NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX code_UNIQUE (code ASC),
  INDEX other_resources_organizations_FK_idx (organizationId ASC),
  INDEX other_resources_contacts_createdby_FK_idx (created_by ASC),
  INDEX other_resources_contacts_updatedby_FK_idx (updated_by ASC),
  CONSTRAINT other_resources_organizations_FK
    FOREIGN KEY (organizationId)
    REFERENCES calani.organizations (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT other_resources_contacts_createdby_FK
    FOREIGN KEY (created_by)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT other_resources_contacts_updatedby_FK
    FOREIGN KEY (updated_by)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
