﻿USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='color') > 0,
    "SELECT 0",
    "alter table jobs 
	ADD COLUMN color VARCHAR(10) NULL DEFAULT NULL;
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;