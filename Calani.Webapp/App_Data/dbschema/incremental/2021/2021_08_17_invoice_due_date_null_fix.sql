use calani;

update jobs j
join organizations o on o.id = j.organizationId
set j.dateInvoiceDue = date_add(j.dateCreated, interval o.delayInvoice day)
where  dateInvoicing is not null and dateInvoiceDue is null and j.id >0;