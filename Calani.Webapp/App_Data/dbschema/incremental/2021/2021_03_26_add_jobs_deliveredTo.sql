USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='deliveredTo') > 0,    
                         "SELECT 0",
    "alter table jobs     
    add column deliveredTo varchar(500)
    
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;


