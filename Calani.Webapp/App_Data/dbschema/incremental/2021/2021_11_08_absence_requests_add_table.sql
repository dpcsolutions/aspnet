CREATE TABLE IF NOT EXISTS calani.absence_requests (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  organizationId BIGINT(20) NOT NULL,
  employeeId BIGINT(20) NOT NULL,
  absenceCategory INT NOT NULL,
  startDate DATETIME NOT NULL,
  endDate DATETIME NOT NULL,
  comment VARCHAR(500) NULL,
  recordStatus INT NOT NULL DEFAULT 0,
  approvedAt DATETIME NULL,
  approvedBy BIGINT(20) NULL,
  rejectedAt DATETIME NULL,
  rejectedBy BIGINT(20) NULL,
  createdAt DATETIME NOT NULL,
  createdBy BIGINT(20) NOT NULL,
  updatedAt DATETIME NULL,
  updatedBy BIGINT(20) NULL,
  PRIMARY KEY (id),
  INDEX absence_requests_organizations_FK_idx (organizationId ASC),
  INDEX absence_requests_contacts_employee_FK_idx (employeeId ASC),
  INDEX absence_requests_contacts_createdby_FK_idx (createdBy ASC),
  INDEX absence_requests_contacts_updatedby_FK_idx (updatedBy ASC),
  INDEX absence_requests_contacts_approvedby_FK_idx (approvedBy ASC),
  INDEX absence_requests_contacts_rejectedby_FK_idx (rejectedBy ASC),
  CONSTRAINT absence_requests_organizations_FK
    FOREIGN KEY (organizationId)
    REFERENCES calani.organizations (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT absence_requests_contacts_employee_FK
    FOREIGN KEY (employeeId)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT absence_requests_contacts_createdby_FK
    FOREIGN KEY (createdBy)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT absence_requests_contacts_updatedby_FK
    FOREIGN KEY (updatedBy)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT absence_requests_contacts_approvedby_FK
    FOREIGN KEY (approvedBy)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT absence_requests_contacts_rejectedby_FK
    FOREIGN KEY (approvedBy)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
