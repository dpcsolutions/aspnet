update services_jobs sj
join jobs j on j.id = sj.jobId
join contacts c on binary trim(substring_index(substring_index(serviceName, ',', -1), '-', 1)) = binary concat(c.firstName, ' ', c.lastName) and c.recordStatus = 0 and c.lastlogin is not null
set sj.timesheetDate = str_to_date(substr(serviceName, 1, 10), '%d.%m.%Y'), sj.contactId = c.id
where j.dateInvoicing is not null and sj.id>0 and j.id>0 ;




