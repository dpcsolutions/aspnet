USE calani;

SET @sql_cancellationRequestedAt = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='absence_requests' and column_name='cancellationRequestedAt') > 0,
    "SELECT 0",
    "alter table absence_requests 
	ADD COLUMN cancellationRequestedAt DATETIME NULL DEFAULT NULL;
    "));
    
SET @sql_cancellationApprovedAt = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='absence_requests' and column_name='cancellationApprovedAt') > 0,
    "SELECT 0",
    "alter table absence_requests 
    ADD COLUMN cancellationApprovedAt DATETIME NULL DEFAULT NULL,
    ADD COLUMN cancellationApprovedBy BIGINT(20) NULL,
    ADD INDEX absence_requests_contacts_cancelapprovedby_FK_idx (cancellationApprovedBy ASC),
    ADD CONSTRAINT absence_requests_contacts_cancelapprovedby_FK
    FOREIGN KEY (cancellationApprovedBy)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    "));
    
SET @sql_cancellationRejectedAt = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='absence_requests' and column_name='cancellationRejectedAt') > 0,
    "SELECT 0",
    "alter table absence_requests 
    ADD COLUMN cancellationRejectedAt DATETIME NULL DEFAULT NULL,
    ADD COLUMN cancellationRejectedBy BIGINT(20) NULL,
    ADD INDEX absence_requests_contacts_cancellationrejectedat_FK_idx (cancellationRejectedAt ASC),
    ADD CONSTRAINT absence_requests_contacts_cancelrejectedby_FK
    FOREIGN KEY (cancellationRejectedBy)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    "));

PREPARE stmt1 FROM @sql_cancellationRequestedAt;
EXECUTE stmt1;

PREPARE stmt2 FROM @sql_cancellationApprovedAt;
EXECUTE stmt2;

PREPARE stmt3 FROM @sql_cancellationRejectedAt;
EXECUTE stmt3;