CREATE TABLE IF NOT EXISTS calani.resource_allocations (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  organizationId BIGINT(20) NOT NULL,
  employeeId BIGINT(20) NULL,
  otherResourceId BIGINT(20) NULL,
  projectId BIGINT(20) NULL,
  startDate DATETIME NOT NULL,
  endDate DATETIME NOT NULL,
  title VARCHAR(200) NULL,
  comment VARCHAR(200) NULL,
  recordStatus INT NOT NULL DEFAULT 0,
  createdAt DATETIME NOT NULL,
  createdBy BIGINT(20) NOT NULL,
  updatedAt DATETIME NULL,
  updatedBy BIGINT(20) NULL,
  PRIMARY KEY (id),
  INDEX resource_allocations_organizations_FK_idx (organizationId ASC),
  INDEX resource_allocations_other_resources_FK_idx (otherResourceId ASC),
  INDEX resource_allocations_contacts_employee_FK_idx (employeeId ASC),
  INDEX resource_allocations_jobs_FK_idx (projectId ASC),
  INDEX resource_allocations_contacts_createdby_FK_idx (createdBy ASC),
  INDEX resource_allocations_contacts_updatedby_FK_idx (updatedBy ASC),
  CONSTRAINT resource_allocations_organizations_FK
    FOREIGN KEY (organizationId)
    REFERENCES calani.organizations (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT resource_allocations_other_resources_FK
    FOREIGN KEY (otherResourceId)
    REFERENCES calani.other_resources (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT resource_allocations_contacts_employee_FK
    FOREIGN KEY (employeeId)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT resource_allocations_jobs_FK
    FOREIGN KEY (projectId)
    REFERENCES calani.jobs (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT resource_allocations_contacts_createdby_FK
    FOREIGN KEY (createdBy)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT resource_allocations_contacts_updatedby_FK
    FOREIGN KEY (updatedBy)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
