USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='round_amount') > 0,
    "SELECT 0",
    "alter table organizations 
	add column round_amount tinyint(1) NOT NULL DEFAULT 0;
       
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;