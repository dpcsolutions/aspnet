
USE calani;

CREATE TABLE IF NOT EXISTS calani.pay_type (
  id BIGINT NOT NULL AUTO_INCREMENT,
  type VARCHAR(45) NOT NULL,
  recordStatus INT NOT NULL DEFAULT 0,
  PRIMARY KEY (id));


INSERT IGNORE  INTO pay_type (id, type) VALUES ('1', 'Cash') ;
INSERT IGNORE  INTO pay_type (id, type) VALUES ('2', 'Card');



SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='expenses' and column_name='paytypeId') > 0,    
                         "SELECT 0",
    "
ALTER TABLE expenses
ADD COLUMN paytypeId BIGINT NOT NULL DEFAULT 1 AFTER currencyRate,
ADD COLUMN vat1 DOUBLE NULL DEFAULT NULL AFTER paytypeId,
ADD COLUMN vat2 DOUBLE NULL DEFAULT NULL AFTER vat1,
ADD COLUMN vat3 DOUBLE NULL DEFAULT NULL AFTER vat2,
ADD INDEX fk_expences_paytype_idx (paytypeId ASC) ,
ADD CONSTRAINT fk_expences_paytype
  FOREIGN KEY (paytypeId)
  REFERENCES pay_type (id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;

