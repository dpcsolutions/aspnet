USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='services_jobs' and column_name='contactId') > 0,    
                         "SELECT 0",
    "alter table services_jobs     
    add column contactId BIGINT NULL,
    ADD COLUMN timesheetDate DATE NULL,
    ADD INDEX fk_services_jobs_contacts_idx (contactId ASC),
    ADD CONSTRAINT fk_services_jobs_contacts
  FOREIGN KEY (contactId)
  REFERENCES contacts (id)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;


