USE calani;

ALTER TABLE organizations
ADD COLUMN display_total_quantity BIT(1) DEFAULT 0 NOT NULL;
