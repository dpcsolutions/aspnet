USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='linkedjobid') > 0,
    "SELECT 0",
    "alter table calani.jobs 
	add column linkedjobid INT(50) NULL DEFAULT 0
    
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;