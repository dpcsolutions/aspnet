
USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='contacts' and column_name='departmentId') > 0,
    "SELECT 0",
    "alter table contacts 
    add column departmentId int DEFAULT NULL;"
));

PREPARE stmt FROM @sql;
EXECUTE stmt;
USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='contacts' and column_name='departmentId') > 0,
    "SELECT 0",
    "alter table contacts add CONSTRAINT fk_contacts_departments FOREIGN KEY (departmentId) REFERENCES departments(id);"
	
));

PREPARE stmt FROM @sql;
EXECUTE stmt;