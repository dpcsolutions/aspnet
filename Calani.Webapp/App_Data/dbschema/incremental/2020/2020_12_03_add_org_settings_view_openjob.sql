USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='employee_view_all_open_proj') > 0,
    "SELECT 0",
    "alter table organizations 
	add column employee_view_all_open_proj tinyint(1) NOT NULL DEFAULT 0;
       
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;