USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='lunch_duration') > 0,
    "SELECT 0",
    "alter table organizations 
	add column lunch_duration INT NOT NULL DEFAULT 60
    
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;
