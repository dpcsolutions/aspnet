USE calani;

CREATE TABLE IF NOT EXISTS  departments  (
  id int(11) NOT NULL AUTO_INCREMENT,
  name  varchar(100) DEFAULT NULL,
  code  varchar(45) DEFAULT NULL,
  description  varchar(200) DEFAULT NULL,
  organizationId  bigint(20) DEFAULT NULL,
  PRIMARY KEY ( id ),
  KEY  fk_departments_organizations  ( organizationId ),
  CONSTRAINT  fk_departments_organizations  FOREIGN KEY ( organizationId ) REFERENCES  organizations  ( id ) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;