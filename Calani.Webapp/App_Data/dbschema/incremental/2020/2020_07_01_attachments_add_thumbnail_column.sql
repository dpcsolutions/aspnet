USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='attachments' and column_name='thumbnail ') > 0,
    "SELECT 0",
    "alter table attachments 
	add column thumbnail longtext DEFAULT NULL
    
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;
