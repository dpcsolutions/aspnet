USE calani;

CREATE TABLE IF NOT EXISTS calendars (
  id bigint NOT NULL AUTO_INCREMENT,
  name varchar(45) DEFAULT NULL,
  code varchar(45) DEFAULT NULL,
  organization_Id BIGINT NOT NULL,
  updated_at DATETIME DEFAULT NULL,
  updated_by bigint DEFAULT NULL,
  recordStatus int default null,
  PRIMARY KEY (`id`),
  KEY `fk_calendars_contacts_idx` (`updated_by`),
  KEY `fk_calendars_organizations_idx` (`organization_Id`),
  CONSTRAINT `fk_calendars_contacts` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_calendars_organizations` FOREIGN KEY (`organization_Id`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS calendars_rec (
  id bigint NOT NULL AUTO_INCREMENT,
  calendar_id BIGINT NOT NULL,
  name varchar(45) DEFAULT NULL,
  start_date DATETIME DEFAULT NULL,
  end_date DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  updated_by BIGINT DEFAULT NULL,
  recordStatus INT DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_calendars_rec_contacts_idx` (`updated_by`),
  KEY `fk_calendars_rec_calendars_idx` (`calendar_id`),
  CONSTRAINT `fk_calendars_rec_contacts` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_calendars_rec_calendars` FOREIGN KEY (`calendar_id`) REFERENCES `calendars` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
