USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='expenses' and column_name='currencyRate') > 0,
    "SELECT 0",
    "alter table expenses 
    add column currencyRate double DEFAULT NULL;" 
    
));

PREPARE stmt FROM @sql;
EXECUTE stmt;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='expenses' and column_name='currency') > 0,
    "SELECT 0",
    "alter table expenses 
    add column currency varchar(6) DEFAULT NULL;" 
    
));

PREPARE stmt FROM @sql;
EXECUTE stmt;
