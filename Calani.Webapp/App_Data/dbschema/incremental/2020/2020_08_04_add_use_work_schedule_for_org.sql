USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='use_work_schedule') > 0,
    "SELECT 0",
    "alter table organizations 
	add column use_work_schedule tinyint(1) NOT NULL DEFAULT 1;
       
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;