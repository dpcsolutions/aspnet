USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='expenses' and column_name='currencyRate') > 0,
    "SELECT 0",
    "alter table expenses 
    add column currencyRate double DEFAULT NULL;" 
    
));

PREPARE stmt FROM @sql;
EXECUTE stmt;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='expenses' and column_name='currency') > 0,
    "SELECT 0",
    "alter table expenses 
    add column currency varchar(6) DEFAULT NULL;" 
    
));

PREPARE stmt FROM @sql;
EXECUTE stmt;


USE calani;

CREATE TABLE IF NOT EXISTS calendars (
  id bigint NOT NULL AUTO_INCREMENT,
  name varchar(45) DEFAULT NULL,
  code varchar(45) DEFAULT NULL,
  organization_Id BIGINT NOT NULL,
  updated_at DATETIME DEFAULT NULL,
  updated_by bigint DEFAULT NULL,
  recordStatus int default null,
  PRIMARY KEY (`id`),
  KEY `fk_calendars_contacts_idx` (`updated_by`),
  KEY `fk_calendars_organizations_idx` (`organization_Id`),
  CONSTRAINT `fk_calendars_contacts` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_calendars_organizations` FOREIGN KEY (`organization_Id`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS calendars_rec (
  id bigint NOT NULL AUTO_INCREMENT,
  calendar_id BIGINT NOT NULL,
  name varchar(45) DEFAULT NULL,
  start_date DATETIME DEFAULT NULL,
  end_date DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  updated_by BIGINT DEFAULT NULL,
  recordStatus INT DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_calendars_rec_contacts_idx` (`updated_by`),
  KEY `fk_calendars_rec_calendars_idx` (`calendar_id`),
  CONSTRAINT `fk_calendars_rec_contacts` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_calendars_rec_calendars` FOREIGN KEY (`calendar_id`) REFERENCES `calendars` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


USE calani;

CREATE TABLE IF NOT EXISTS work_schedule_profiles (
  id bigint NOT NULL AUTO_INCREMENT,
  name varchar(45) DEFAULT NULL,
  code varchar(45) DEFAULT NULL,
   organization_id BIGINT NOT NULL,
  updated_at DATETIME DEFAULT NULL,
  updated_by bigint DEFAULT NULL,
  recordStatus int default null,
  PRIMARY KEY (`id`),
  KEY `fk_profiles_contacts_idx` (`updated_by`),
  KEY `fk_work_schedule_profiles_organizations_idx` (`organization_Id`),  
  CONSTRAINT `fk_work_schedule_profiles_contacts`			FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_work_schedule_profiles_organizations`	FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS work_schedule_profiles_rec (
  id bigint NOT NULL AUTO_INCREMENT,
  profile_id BIGINT NULL,
  organization_id BIGINT NOT NULL,
  color varchar(8) DEFAULT NULL,
  name varchar(45) DEFAULT NULL,
  start varchar(8) DEFAULT NULL,
  end varchar(8) DEFAULT NULL,
  rate INT NOT NULL DEFAULT 100,
  mo BIT NOT NULL DEFAULT 0,
  tu BIT NOT NULL DEFAULT 0,
  we BIT NOT NULL DEFAULT 0,
  th BIT NOT NULL DEFAULT 0,
  fr BIT NOT NULL DEFAULT 0,
  sa BIT NOT NULL DEFAULT 0,
  su BIT NOT NULL DEFAULT 0,
  type int NOT NULL DEFAULT 0,
  updated_at DATETIME DEFAULT NULL,
  updated_by BIGINT DEFAULT NULL,
  recordStatus INT DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_work_schedule_profiles_rec_contacts_idx` (`updated_by`),
  KEY `fk_work_schedule_profiles_rec_organizations` (`organization_id`),
  CONSTRAINT `fk_work_schedule_profiles_rec_contacts` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_work_schedule_profiles_rec_organizations`	FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;




CREATE TABLE IF NOT EXISTS work_schedule_profiles_calendars (
  id bigint NOT NULL AUTO_INCREMENT,  
  calendar_id BIGINT DEFAULT NULL,
  profile_id BIGINT NOT NULL,
  updated_at DATETIME DEFAULT NULL,
  updated_by bigint DEFAULT NULL,
  recordStatus int default null,
  PRIMARY KEY (`id`),
  KEY `fk_profiles_calendars_work_schedule_profiles_idx` (`profile_id`),
  KEY `fk_profiles_calendars_calendars_idx` (`calendar_Id`),
  CONSTRAINT `fk_work_schedule_profiles`			FOREIGN KEY (`profile_id`) REFERENCES `work_schedule_profiles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_calendars`		FOREIGN KEY (`calendar_Id`) REFERENCES `calendars` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='contacts' and column_name='overtime_allowed') > 0,
    "SELECT 0",
    "alter table contacts 
	add column  overtime_allowed BIT NOT NULL DEFAULT 0 "));

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;

CREATE TABLE IF NOT EXISTS contacts_contracts (
  id bigint NOT NULL AUTO_INCREMENT,
  start_date DATETIME DEFAULT NULL,
  rate INT DEFAULT NULL,
  duration INT DEFAULT NULL, /*in minutes*/
  overtime_allowed BIT NOT NULL DEFAULT 0,
  contact_Id BIGINT NOT NULL,
  updated_at DATETIME DEFAULT NULL,
  updated_by bigint DEFAULT NULL,
  recordStatus int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_contracts_contacts_upd_idx` (`updated_by`),
  KEY `fk_contracts_contacts_idx` (`contact_Id`),
  CONSTRAINT `fk_contracts_contacts_upd` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contracts_contacts` FOREIGN KEY (`contact_Id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='organizations' and column_name='work_schedule_profile_id') > 0,
    "SELECT 0",
    "alter table organizations 
	add column week_work_duration INT NOT NULL DEFAULT 2400, 
    add column work_schedule_profile_id bigint DEFAULT NULL,       
    ADD CONSTRAINT fk_work_schedule_profiles_organization	FOREIGN KEY (work_schedule_profile_id) REFERENCES work_schedule_profiles (id) ON DELETE NO ACTION ON UPDATE NO ACTION
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='jobs' AND column_name='subtype') > 0,
    "SELECT 0",
    "alter table jobs 
    add column subtype varchar(15) DEFAULT NULL;" 
    
));

PREPARE stmt FROM @sql;
EXECUTE stmt;



USE calani;

PREPARE stmt FROM @sql;
EXECUTE stmt;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='expenses' and column_name='amountInCurrency') > 0,
    "alter table expenses 
    drop column amountInCurrency;" ,
    "SELECT 0"
    
));

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;

CREATE TABLE IF NOT EXISTS  departments  (
  id int(11) NOT NULL AUTO_INCREMENT,
  name  varchar(100) DEFAULT NULL,
  code  varchar(45) DEFAULT NULL,
  description  varchar(200) DEFAULT NULL,
  organizationId  bigint(20) DEFAULT NULL,
  PRIMARY KEY ( id ),
  KEY  fk_departments_organizations  ( organizationId ),
  CONSTRAINT  fk_departments_organizations  FOREIGN KEY ( organizationId ) REFERENCES  organizations  ( id ) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;



USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='contacts' and column_name='departmentId') > 0,
    "SELECT 0",
    "alter table contacts 
    add column departmentId int DEFAULT NULL;"
));

PREPARE stmt FROM @sql;
EXECUTE stmt;


USE calani;
SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='contacts' and column_name='departmentId') > 0,
    "SELECT 0",
    "alter table contacts add CONSTRAINT fk_contacts_departments FOREIGN KEY (departmentId) REFERENCES departments(id);"
	
));

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='contacts' and column_name='work_schedule_profile_id') > 0,
    "SELECT 0",
    "alter table contacts 
	 add column work_schedule_profile_id bigint DEFAULT NULL,       
    ADD CONSTRAINT fk_work_schedule_profile_contact	FOREIGN KEY (work_schedule_profile_id) REFERENCES work_schedule_profiles (id) ON DELETE NO ACTION ON UPDATE NO ACTION
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;


USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='departments' and column_name='recordStatus') > 0,
    "SELECT 0",
    "alter table departments 
    add column recordStatus int DEFAULT NULL;"
));

PREPARE stmt FROM @sql;
EXECUTE stmt;


USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='timesheetrecords' and column_name='work_schedule_profiles_rec_id') > 0,
    "SELECT 0",
    "alter table timesheetrecords 
    add column work_schedule_profiles_rec_id bigint DEFAULT NULL,       
	add column is_warning BIT NOT NULL DEFAULT 0,     
    ADD CONSTRAINT fk_work_schedule_profiles_rec_timesheetrecords	FOREIGN KEY (work_schedule_profiles_rec_id) REFERENCES work_schedule_profiles_rec (id) ON DELETE NO ACTION ON UPDATE NO ACTION
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='timesheets' and column_name='is_warning') > 0,
    "SELECT 0",
    "alter table timesheets   
	add column is_warning BIT NOT NULL DEFAULT 0    
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='timesheetrecords' and column_name='comment') > 0,    
                         "SELECT 0",
    "alter table timesheetrecords 
    add column comment longtext DEFAULT NULL,
	add column parent_id bigint DEFAULT NULL,
    add column order_index int DEFAULT NULL,
    ADD CONSTRAINT fk_timesheetrecords_timesheetrecords	FOREIGN KEY (parent_id) REFERENCES timesheetrecords (id) ON DELETE CASCADE ON UPDATE NO ACTION
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;

/*DROP TABLE IF EXISTS calani.timesheet_records_services;*/
/*
CREATE TABLE IF NOT EXISTS timesheet_records_services (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `timesheetId` bigint(20) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `visitId` bigint(20) DEFAULT NULL,
  `serviceId` bigint(20) DEFAULT NULL,
  `serviceName` varchar(45) DEFAULT NULL,
  `servicePrice` double DEFAULT NULL, 
  `duration` double DEFAULT NULL,
  `recordStatus` int(11) NOT NULL DEFAULT '0',
  `lastModificationDate` datetime DEFAULT NULL,
  `lastModifiedBy` bigint(20) DEFAULT NULL,
  `jobId` bigint(20) DEFAULT NULL,
  `serviceType` int(11) DEFAULT NULL,  
  `is_warning` bit(1) NOT NULL DEFAULT b'0',
  `comment` longtext,  
  PRIMARY KEY (`id`),
  KEY `fk_timesheet_records_services_timesheets_idx` (`timesheetId`),
  KEY `fk_lastModifiedBy_idx` (`lastModifiedBy`),
  KEY `fk_jobItem_jobId_idx` (`jobId`),
  KEY `fk_timesheet_records_services_service_idx` (`serviceId`),
  KEY `fk_timesheet_records_services_visit_idx` (`visitId`), 
  CONSTRAINT `fk_timesheet_records_services_job` FOREIGN KEY (`jobId`) REFERENCES `jobs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_timesheet_records_services_lastModifiedBy` FOREIGN KEY (`lastModifiedBy`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_timesheet_records_servicess_service` FOREIGN KEY (`serviceId`) REFERENCES `services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_timesheet_records_services_timesheets` FOREIGN KEY (`timesheetId`) REFERENCES `timesheets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_timesheet_records_services_visit` FOREIGN KEY (`visitId`) REFERENCES `visits` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

USE calani;
SET @sql = (SELECT IF( (SELECT COUNT(*) FROM timesheet_records_services ) > 0,
    "SELECT 0",
    "INSERT INTO	timesheet_records_services (timesheetId, date, visitId,serviceId, serviceName, servicePrice, duration, 
					recordStatus, lastModificationDate, lastModifiedBy, jobId, serviceType, is_warning, comment)
		SELECT	tr.timesheetId, tr.startDate, tr.visitId, tr.serviceId, tr.serviceName, tr.servicePrice, tr.duration, tr.recordStatus, 
			tr.lastModificationDate, tr.lastModifiedBy, tr.jobId, tr.serviceType, tr.is_warning, tr.comment
		FROM timesheetrecords tr WHERE serviceId>0 or jobId>0;
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;
*/

USE calani;


SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='work_schedule_profiles_rec' and column_name='organization_id') > 0,
    "SELECT 0",
    "ALTER TABLE work_schedule_profiles_rec 
 	 add column organization_id BIGINT NOT NULL ;  
	 alter table work_schedule_profiles_rec add CONSTRAINT fk_work_schedule_profiles_rec_organizations FOREIGN KEY (organization_id) REFERENCES organizations(id);
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;



SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='work_schedule_profiles_rec' and column_name='type') > 0,
    "SELECT 0",
    "ALTER TABLE work_schedule_profiles_rec 
    CHANGE holidays type INT NOT NULL DEFAULT 0,
	CHANGE profile_id profile_id BIGINT NULL;     
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM work_schedule_profiles_rec
						WHERE type in(2,3)) > 0,
    "SELECT 0",
    "insert into work_schedule_profiles_rec (profile_id,color,name,start,end,rate,mo,tu,we,th,fr,sa,su,type,updated_at,updated_by,recordStatus, organization_id)
SELECT null, null, 'Custom', null, null, 100, 0,0,0,0,0,0,0, 2, null, null, 0, o.id
from   organizations  o;

    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;


USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM work_schedule_profiles_rec
						WHERE type in(2,3)) > 0,
    "SELECT 0",
    "insert into work_schedule_profiles_rec (profile_id,color,name,start,end,rate,mo,tu,we,th,fr,sa,su,type,updated_at,updated_by,recordStatus, organization_id)
SELECT null, null, 'Auto', null, null, 0, 0,0,0,0,0,0,0, 3, null, null, 0, o.id
from   organizations  o;      

    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;






USE calani;

CREATE TABLE IF NOT EXISTS `tags`
             (
                          `id`             bigint(20) NOT NULL AUTO_INCREMENT
                        , `name`           varchar(50) DEFAULT NULL
                        , `type`           varchar(50) DEFAULT NULL
                        , `organizationId` bigint(20) DEFAULT NULL
                        , `recordStatus`   int(11) DEFAULT '0'
                        , PRIMARY KEY (`id`)
                        , KEY `fk_tags_organizations` (`organizationId`)
                        , CONSTRAINT `fk_tags_organizations` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`) ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
             )
             ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=LATIN1;
			 
			 
CREATE TABLE IF NOT EXISTS `tasks`
             (
                          `id`             bigint(20) NOT NULL AUTO_INCREMENT
                        , `title`          varchar(50) DEFAULT NULL
                        , `description`    varchar(50) DEFAULT NULL
                        , `ownerId`        bigint(20) DEFAULT NULL
                        , `prority`        int(11) DEFAULT NULL
                        , `status`         int(11) DEFAULT NULL
                        , `assignedTo`     bigint(20) DEFAULT NULL
                        , `progress`       int(11) DEFAULT NULL
                        , `organizationId` bigint(20) DEFAULT NULL
                        , `recordStatus`   int(11) DEFAULT NULL
                        , `dueDate`        date DEFAULT NULL
                        , `timeline`       varchar(50) DEFAULT NULL
                        , `tag`            bigint(20) DEFAULT NULL
                        , PRIMARY KEY (`id`)
                        , KEY `fk_tasks_organization` (`organizationId`)
                        , CONSTRAINT `fk_tasks_organization` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`) ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
             )
             ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=LATIN1;
			 
			 
CREATE TABLE IF NOT EXISTS `task_activity`
             (
                          `Id`             bigint(20) NOT NULL AUTO_INCREMENT
                        , `OrganizationId` bigint(20) DEFAULT NULL
                        , `TaskId`         bigint(20) NOT NULL
                        , `ContactId`      bigint(20) DEFAULT NULL
                        , `Message`        varchar(1000) DEFAULT NULL
                        , `CreatedAt`      datetime DEFAULT NULL
                        , PRIMARY KEY (`Id`)
                        , KEY `fk_task_activity_organization_idx` (`OrganizationId`)
                        , KEY `fk_task_activity_contact_idx` (`ContactId`)
                        , KEY `fk_task_activity_task_idx` (`TaskId`)
                        , CONSTRAINT `fk_task_activity_contact` FOREIGN KEY (`ContactId`) REFERENCES `contacts` (`id`) ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
     , CONSTRAINT `fk_task_activity_organization` FOREIGN KEY (`OrganizationId`) REFERENCES `organizations` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
     , CONSTRAINT `fk_task_activity_task` FOREIGN KEY (`TaskId`) REFERENCES `tasks` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
             )
             ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=LATIN1;
			 
			 
CREATE TABLE IF NOT EXISTS `task_attachments`
             (
                          `Id`             bigint(20) NOT NULL AUTO_INCREMENT
                        , `organizationId` bigint(20) DEFAULT NULL
                        , `taskId`         bigint(20) DEFAULT NULL
                        , `attachmentId`   bigint(20) DEFAULT NULL
                        , `contactId`      bigint(20) DEFAULT NULL
                        , PRIMARY KEY (`Id`)
                        , KEY `fk_task_organization_idx` (`organizationId`)
                        , KEY `fk_task_attachments_task_idx` (`taskId`)
                        , KEY `fk_task_attachments_attachment_idx` (`attachmentId`)
                        , KEY `fk_task_attachments_contact_idx` (`contactId`)
                        , CONSTRAINT `fk_task_attachments_attachment` FOREIGN KEY (`attachmentId`) REFERENCES `attachments` (`id`) ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
     , CONSTRAINT `fk_task_attachments_contact` FOREIGN KEY (`contactId`) REFERENCES `contacts` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
     , CONSTRAINT `fk_task_attachments_organization` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
     , CONSTRAINT `fk_task_attachments_task` FOREIGN KEY (`taskId`) REFERENCES `tasks` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
             )
             ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=LATIN1;
			 
			 
CREATE TABLE IF NOT EXISTS `task_contact_mapping`
             (
                          `id`             bigint(20) NOT NULL AUTO_INCREMENT
                        , `contactId`      bigint(20) DEFAULT NULL
                        , `taskId`         bigint(20) DEFAULT NULL
                        , `organizationId` bigint(20) DEFAULT NULL
                        , `recordStatus`   int(11) DEFAULT '0'
                        , PRIMARY KEY (`id`)
                        , KEY `fk_mtasks_contacts` (`contactId`)
                        , KEY `fk_mtasks_tasks` (`taskId`)
                        , KEY `fk_mtasks_organizations` (`organizationId`)
                        , CONSTRAINT `fk_mtasks_contacts` FOREIGN KEY (`contactId`) REFERENCES `contacts` (`id`) ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
     , CONSTRAINT `fk_mtasks_organizations` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
     , CONSTRAINT `fk_mtasks_tasks` FOREIGN KEY (`taskId`) REFERENCES `tasks` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
             )
             ENGINE=InnoDB AUTO_INCREMENT=292 DEFAULT CHARSET=LATIN1;
			 
			 
CREATE TABLE IF NOT EXISTS `task_pin_status`
             (
                          `Id`             bigint(20) NOT NULL AUTO_INCREMENT
                        , `OrganizationId` bigint(20) DEFAULT NULL
                        , `IsPinned`       tinyint(1) DEFAULT NULL
                        , `ContactId`      bigint(20) NOT NULL
                        , `TaskId`         bigint(20) NOT NULL
                        , PRIMARY KEY (`Id`)
                        , KEY `fk_task_pin_status_organization_idx` (`OrganizationId`)
                        , KEY `fk_task_pin_status_contact_idx` (`ContactId`)
                        , KEY `fk_task_pin_status_task_idx` (`TaskId`)
                        , CONSTRAINT `fk_task_pin_status_contact` FOREIGN KEY (`ContactId`) REFERENCES `contacts` (`id`) ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
     , CONSTRAINT `fk_task_pin_status_organization` FOREIGN KEY (`OrganizationId`) REFERENCES `organizations` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
     , CONSTRAINT `fk_task_pin_status_task` FOREIGN KEY (`TaskId`) REFERENCES `tasks` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
             )
             ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=LATIN1;
			 
			 
CREATE TABLE IF NOT EXISTS `task_tag_mapping`
             (
                          `id`     bigint(20) NOT NULL AUTO_INCREMENT
                        , `tagId`  bigint(20) DEFAULT NULL
                        , `taskId` bigint(20) DEFAULT NULL
                        , PRIMARY KEY (`id`)
                        , KEY `fk_mtags_tags` (`tagId`)
                        , KEY `fk_mtags_tasks` (`taskId`)
                        , CONSTRAINT `fk_mtags_tags` FOREIGN KEY (`tagId`) REFERENCES `tags` (`id`) ON
DELETE
       NO ACTION
ON
UPDATE
SET    NULL
     , CONSTRAINT `fk_mtags_tasks` FOREIGN KEY (`taskId`) REFERENCES `tasks` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
SET    NULL
             )
             ENGINE=InnoDB AUTO_INCREMENT=213 DEFAULT CHARSET=LATIN1;
			 
			 USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='organizations' and column_name='lunch_duration') > 0,
    "SELECT 0",
    "alter table organizations 
	add column lunch_duration INT NOT NULL DEFAULT 60
    
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;


USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='timesheetrecords' and column_name='lunch_duration') > 0,    
                         "SELECT 0",
    "alter table timesheetrecords 
    add column lunch_duration int DEFAULT 0,
    add column row_type int DEFAULT 0
    
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='tags' and column_name='color') > 0,
    "SELECT 0",
    "alter table tags   
	add column color varchar(50) DEFAULT NULL    
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;

SET @sql = (SELECT IF(
  (
    (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='tags' and column_name='module') > 0)
  AND 						
(SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS 
  WHERE table_schema = schema() and table_name = 'tags' AND COLUMN_NAME = 'type') = 'int',
  "SELECT 1",
  "ALTER TABLE `tags` CHANGE COLUMN `type` `type` INT(11) NULL DEFAULT NULL AFTER `name`, ADD COLUMN `module` INT(11) NULL DEFAULT NULL AFTER `type`;"  
));
PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='jobs' and column_name='comment') > 0,    
                         "SELECT 0",
    "alter table jobs 
    add column comment text,
    add column send_copy_email_to varchar(500)
    
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;


USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='services_jobs' and column_name='is_checked') > 0,    
                         "SELECT 0",
    "alter table services_jobs 
    add column is_checked TINYINT(1)  
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;



USE calani;

SELECT Count(*)
INTO @exists
FROM information_schema.tables 
WHERE table_schema = schema() and table_type = 'BASE TABLE' 
    AND table_name = 'mobile_tutorials';

SET @query = If(@exists>0,
    'RENAME TABLE mobile_tutorials TO tutorials',
    'select 0');

PREPARE stmt FROM @query;

EXECUTE stmt;

USE calani;
SELECT Count(*)
INTO @exists
FROM information_schema.tables 
WHERE table_schema = schema() and table_type = 'BASE TABLE'
    AND table_name = 'mobile_tutorials_screens';

SET @query = If(@exists>0,
    'RENAME TABLE mobile_tutorials_screens TO tutorials_screens',
    'select 0');

PREPARE stmt FROM @query;

EXECUTE stmt;


USE calani;

CREATE TABLE IF NOT EXISTS tutorials (
  id bigint NOT NULL AUTO_INCREMENT,
  name varchar(45) DEFAULT NULL,
  type int NOT NULL DEFAULT 0,
  min_version DECIMAL(8,4) NOT NULL DEFAULT 0,
  device_deployed int  DEFAULT NULL, 
  updated_at DATETIME DEFAULT NULL,
  updated_by bigint DEFAULT NULL,
  recordStatus int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_tutorials_contacts_idx` (`updated_by`), 
  CONSTRAINT `fk_tutorials_contacts` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION  
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

USE calani;

CREATE TABLE IF NOT EXISTS tutorials_screens (
  id bigint NOT NULL AUTO_INCREMENT,
  position INT NOT NULL,
  tutorial_id BIGINT NOT NULL,
  title varchar(500) DEFAULT NULL,
  explanation TEXT DEFAULT NULL,
  image LONGTEXT DEFAULT NULL,
  filename  varchar(1000) DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  updated_by BIGINT DEFAULT NULL,
  recordStatus INT DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_tutorials_screens_contacts_idx` (`updated_by`),
  KEY `fk_tutorials_screens_tutorials_idx` (`tutorial_id`),
  CONSTRAINT `fk_ctutorials_screens_contacts` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tutorials_screens_tutorials` FOREIGN KEY (`tutorial_id`) REFERENCES `tutorials` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

USE calani;

CREATE TABLE IF NOT EXISTS mobile_tutorials_deployed (
  id bigint NOT NULL AUTO_INCREMENT,  
  tutorial_id BIGINT NOT NULL,
  user_id BIGINT NOT NULL,
  version varchar(20),
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP, 
  PRIMARY KEY (`id`),
  KEY `fk_tutorials_deployed_contacts_idx` (`user_id`),
  KEY `fk_tutorials_deployed_tutorials_idx` (`tutorial_id`),
  CONSTRAINT `fk_tutorials_deployed_contacts` FOREIGN KEY (`user_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tutorials_deployed_tutorials` FOREIGN KEY (`tutorial_id`) REFERENCES `tutorials` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='tutorials' and column_name='type') > 0,
    "SELECT 0",
    "alter table tutorials 
	add column type INT NOT NULL DEFAULT 0    
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='jobs' AND column_name='totalWithoutTaxes') > 0,
    "SELECT 0",
    "alter table jobs 
    add column totalWithoutTaxes double DEFAULT NULL;" 
    
));

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='attachments' and column_name='properties') > 0,
    "SELECT 0",
    "alter table attachments 
	add column properties INT NOT NULL DEFAULT 0
    
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='attachments' and column_name='thumbnail ') > 0,
    "SELECT 0",
    "alter table attachments 
	add column thumbnail longtext DEFAULT NULL
    
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;
CREATE TABLE IF NOT EXISTS tutorials_stat (
  id bigint NOT NULL AUTO_INCREMENT,
  tutorial_id BIGINT NOT NULL,
  viewed_by BIGINT NOT NULL,
  viewed_date DATETIME DEFAULT NULL,

  recordStatus int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),

  KEY `fk_tutorials_stat_contacts_idx` (`viewed_by`),
  KEY `fk_tutorials_stat_tutorial_idx` (`tutorial_id`),

  CONSTRAINT `fk_tutorials_stat_contacts` FOREIGN KEY (`viewed_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tutorials_stat_tutorials` FOREIGN KEY (`tutorial_id`) REFERENCES `tutorials` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


USE calani;
SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='tutorials' and column_name='min_version' and data_type = 'varchar' ) > 0,
    
    "ALTER TABLE tutorials DROP COLUMN min_version;",
    "SELECT 0"));

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;
SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='tutorials' and column_name='min_version' and data_type = 'decimal' ) > 0,
    
    "SELECT 0",
    "ALTER TABLE tutorials ADD   min_version DECIMAL(8,4) NOT NULL DEFAULT 0 ;"));

PREPARE stmt FROM @sql;
EXECUTE stmt;



