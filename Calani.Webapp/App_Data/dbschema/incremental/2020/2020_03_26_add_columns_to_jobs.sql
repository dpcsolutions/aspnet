USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' AND column_name='totalWithoutTaxes') > 0,
    "SELECT 0",
    "alter table jobs 
    add column totalWithoutTaxes double DEFAULT NULL;" 
    
));

PREPARE stmt FROM @sql;
EXECUTE stmt;

