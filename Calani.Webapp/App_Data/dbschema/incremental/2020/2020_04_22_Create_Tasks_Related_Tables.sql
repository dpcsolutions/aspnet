USE calani;

CREATE TABLE IF NOT EXISTS `tags`
             (
                          `id`             bigint(20) NOT NULL AUTO_INCREMENT
                        , `name`           varchar(50) DEFAULT NULL
                        , `type`           varchar(50) DEFAULT NULL
                        , `organizationId` bigint(20) DEFAULT NULL
                        , `recordStatus`   int(11) DEFAULT '0'
                        , PRIMARY KEY (`id`)
                        , KEY `fk_tags_organizations` (`organizationId`)
                        , CONSTRAINT `fk_tags_organizations` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`) ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
             )
             ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=LATIN1;
			 
			 
CREATE TABLE IF NOT EXISTS `tasks`
             (
                          `id`             bigint(20) NOT NULL AUTO_INCREMENT
                        , `title`          varchar(50) DEFAULT NULL
                        , `description`    varchar(50) DEFAULT NULL
                        , `ownerId`        bigint(20) DEFAULT NULL
                        , `prority`        int(11) DEFAULT NULL
                        , `status`         int(11) DEFAULT NULL
                        , `assignedTo`     bigint(20) DEFAULT NULL
                        , `progress`       int(11) DEFAULT NULL
                        , `organizationId` bigint(20) DEFAULT NULL
                        , `recordStatus`   int(11) DEFAULT NULL
                        , `dueDate`        date DEFAULT NULL
                        , `timeline`       varchar(50) DEFAULT NULL
                        , `tag`            bigint(20) DEFAULT NULL
                        , PRIMARY KEY (`id`)
                        , KEY `fk_tasks_organization` (`organizationId`)
                        , CONSTRAINT `fk_tasks_organization` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`) ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
             )
             ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=LATIN1;
			 
			 
CREATE TABLE IF NOT EXISTS `task_activity`
             (
                          `Id`             bigint(20) NOT NULL AUTO_INCREMENT
                        , `OrganizationId` bigint(20) DEFAULT NULL
                        , `TaskId`         bigint(20) NOT NULL
                        , `ContactId`      bigint(20) DEFAULT NULL
                        , `Message`        varchar(1000) DEFAULT NULL
                        , `CreatedAt`      datetime DEFAULT NULL
                        , PRIMARY KEY (`Id`)
                        , KEY `fk_task_activity_organization_idx` (`OrganizationId`)
                        , KEY `fk_task_activity_contact_idx` (`ContactId`)
                        , KEY `fk_task_activity_task_idx` (`TaskId`)
                        , CONSTRAINT `fk_task_activity_contact` FOREIGN KEY (`ContactId`) REFERENCES `contacts` (`id`) ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
     , CONSTRAINT `fk_task_activity_organization` FOREIGN KEY (`OrganizationId`) REFERENCES `organizations` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
     , CONSTRAINT `fk_task_activity_task` FOREIGN KEY (`TaskId`) REFERENCES `tasks` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
             )
             ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=LATIN1;
			 
			 
CREATE TABLE IF NOT EXISTS `task_attachments`
             (
                          `Id`             bigint(20) NOT NULL AUTO_INCREMENT
                        , `organizationId` bigint(20) DEFAULT NULL
                        , `taskId`         bigint(20) DEFAULT NULL
                        , `attachmentId`   bigint(20) DEFAULT NULL
                        , `contactId`      bigint(20) DEFAULT NULL
                        , PRIMARY KEY (`Id`)
                        , KEY `fk_task_organization_idx` (`organizationId`)
                        , KEY `fk_task_attachments_task_idx` (`taskId`)
                        , KEY `fk_task_attachments_attachment_idx` (`attachmentId`)
                        , KEY `fk_task_attachments_contact_idx` (`contactId`)
                        , CONSTRAINT `fk_task_attachments_attachment` FOREIGN KEY (`attachmentId`) REFERENCES `attachments` (`id`) ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
     , CONSTRAINT `fk_task_attachments_contact` FOREIGN KEY (`contactId`) REFERENCES `contacts` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
     , CONSTRAINT `fk_task_attachments_organization` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
     , CONSTRAINT `fk_task_attachments_task` FOREIGN KEY (`taskId`) REFERENCES `tasks` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
             )
             ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=LATIN1;
			 
			 
CREATE TABLE IF NOT EXISTS `task_contact_mapping`
             (
                          `id`             bigint(20) NOT NULL AUTO_INCREMENT
                        , `contactId`      bigint(20) DEFAULT NULL
                        , `taskId`         bigint(20) DEFAULT NULL
                        , `organizationId` bigint(20) DEFAULT NULL
                        , `recordStatus`   int(11) DEFAULT '0'
                        , PRIMARY KEY (`id`)
                        , KEY `fk_mtasks_contacts` (`contactId`)
                        , KEY `fk_mtasks_tasks` (`taskId`)
                        , KEY `fk_mtasks_organizations` (`organizationId`)
                        , CONSTRAINT `fk_mtasks_contacts` FOREIGN KEY (`contactId`) REFERENCES `contacts` (`id`) ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
     , CONSTRAINT `fk_mtasks_organizations` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
     , CONSTRAINT `fk_mtasks_tasks` FOREIGN KEY (`taskId`) REFERENCES `tasks` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
             )
             ENGINE=InnoDB AUTO_INCREMENT=292 DEFAULT CHARSET=LATIN1;
			 
			 
CREATE TABLE IF NOT EXISTS `task_pin_status`
             (
                          `Id`             bigint(20) NOT NULL AUTO_INCREMENT
                        , `OrganizationId` bigint(20) DEFAULT NULL
                        , `IsPinned`       tinyint(1) DEFAULT NULL
                        , `ContactId`      bigint(20) NOT NULL
                        , `TaskId`         bigint(20) NOT NULL
                        , PRIMARY KEY (`Id`)
                        , KEY `fk_task_pin_status_organization_idx` (`OrganizationId`)
                        , KEY `fk_task_pin_status_contact_idx` (`ContactId`)
                        , KEY `fk_task_pin_status_task_idx` (`TaskId`)
                        , CONSTRAINT `fk_task_pin_status_contact` FOREIGN KEY (`ContactId`) REFERENCES `contacts` (`id`) ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
     , CONSTRAINT `fk_task_pin_status_organization` FOREIGN KEY (`OrganizationId`) REFERENCES `organizations` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
     , CONSTRAINT `fk_task_pin_status_task` FOREIGN KEY (`TaskId`) REFERENCES `tasks` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
       NO ACTION
             )
             ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=LATIN1;
			 
			 
CREATE TABLE IF NOT EXISTS `task_tag_mapping`
             (
                          `id`     bigint(20) NOT NULL AUTO_INCREMENT
                        , `tagId`  bigint(20) DEFAULT NULL
                        , `taskId` bigint(20) DEFAULT NULL
                        , PRIMARY KEY (`id`)
                        , KEY `fk_mtags_tags` (`tagId`)
                        , KEY `fk_mtags_tasks` (`taskId`)
                        , CONSTRAINT `fk_mtags_tags` FOREIGN KEY (`tagId`) REFERENCES `tags` (`id`) ON
DELETE
       NO ACTION
ON
UPDATE
SET    NULL
     , CONSTRAINT `fk_mtags_tasks` FOREIGN KEY (`taskId`) REFERENCES `tasks` (`id`)
ON
DELETE
       NO ACTION
ON
UPDATE
SET    NULL
             )
             ENGINE=InnoDB AUTO_INCREMENT=213 DEFAULT CHARSET=LATIN1;