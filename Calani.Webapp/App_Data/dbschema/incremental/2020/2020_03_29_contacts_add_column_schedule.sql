USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='contacts' and column_name='work_schedule_profile_id') > 0,
    "SELECT 0",
    "alter table contacts 
	 add column work_schedule_profile_id bigint DEFAULT NULL,       
    ADD CONSTRAINT fk_work_schedule_profile_contact	FOREIGN KEY (work_schedule_profile_id) REFERENCES work_schedule_profiles (id) ON DELETE NO ACTION ON UPDATE NO ACTION
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;

