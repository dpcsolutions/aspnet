USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='assigneeId') > 0,
    "SELECT 0",
    "alter table jobs 
    add column assigneeId bigint(20) DEFAULT NULL;"
));

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;
SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='assigneeId') > 0,
    "SELECT 0",
    "alter table jobs add CONSTRAINT fk_jobs_contacts_assignee FOREIGN KEY (assigneeId) REFERENCES contacts(id);"
	
));

PREPARE stmt FROM @sql;
EXECUTE stmt;