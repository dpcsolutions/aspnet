USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='work_schedule_profiles_rec' and column_name='type') > 0,
    "SELECT 0",
    "ALTER TABLE work_schedule_profiles_rec 
    CHANGE holidays type INT NOT NULL DEFAULT 0,
	CHANGE profile_id profile_id BIGINT NULL;     
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;



USE calani;


SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='work_schedule_profiles_rec' and column_name='organization_id') > 0,
    "SELECT 0",
    "ALTER TABLE work_schedule_profiles_rec 
 	 add column organization_id BIGINT NOT NULL   
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;
SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='work_schedule_profiles_rec' and column_name='organization_id') > 0,
    
    "alter table work_schedule_profiles_rec add CONSTRAINT fk_work_schedule_profiles_rec_organizations FOREIGN KEY (organization_id) REFERENCES organizations(id);",
	"SELECT 0"
	
));

PREPARE stmt FROM @sql;
EXECUTE stmt;



USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM work_schedule_profiles_rec
						WHERE type in(2,3)) > 0,
    "SELECT 0",
    "insert into work_schedule_profiles_rec (profile_id,color,name,start,end,rate,mo,tu,we,th,fr,sa,su,type,updated_at,updated_by,recordStatus, organization_id)
SELECT null, null, 'Custom', null, null, 100, 0,0,0,0,0,0,0, 2, null, null, 0, o.id
from   organizations  o;

    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;


USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM work_schedule_profiles_rec
						WHERE type in(2,3)) > 0,
    "SELECT 0",
    "insert into work_schedule_profiles_rec (profile_id,color,name,start,end,rate,mo,tu,we,th,fr,sa,su,type,updated_at,updated_by,recordStatus, organization_id)
SELECT null, null, 'Auto', null, null, 0, 0,0,0,0,0,0,0, 3, null, null, 0, o.id
from   organizations  o;      

    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;





