USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='timesheetrecords' and column_name='work_schedule_profiles_rec_id') > 0,
    "SELECT 0",
    "alter table timesheetrecords 
    add column work_schedule_profiles_rec_id bigint DEFAULT NULL,       
	add column is_warning BIT NOT NULL DEFAULT 0,     
    ADD CONSTRAINT fk_work_schedule_profiles_rec_timesheetrecords	FOREIGN KEY (work_schedule_profiles_rec_id) REFERENCES work_schedule_profiles_rec (id) ON DELETE NO ACTION ON UPDATE NO ACTION
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;

