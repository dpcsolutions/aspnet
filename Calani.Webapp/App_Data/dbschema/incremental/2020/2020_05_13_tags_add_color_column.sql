USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='tags' and column_name='color') > 0,
    "SELECT 0",
    "alter table tags   
	add column color varchar(50) DEFAULT NULL    
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;