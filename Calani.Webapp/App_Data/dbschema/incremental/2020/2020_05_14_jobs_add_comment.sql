USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='comment') > 0,    
                         "SELECT 0",
    "alter table jobs 
    add column comment text,
    add column send_copy_email_to varchar(500)
    
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;


USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='services_jobs' and column_name='is_checked') > 0,    
                         "SELECT 0",
    "alter table services_jobs 
    add column is_checked TINYINT(1)  
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;



