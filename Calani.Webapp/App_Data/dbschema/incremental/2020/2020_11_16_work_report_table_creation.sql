USE calani;

CREATE TABLE IF NOT EXISTS work_reports (
  id bigint NOT NULL AUTO_INCREMENT,
  title varchar(500) DEFAULT NULL,
  description TEXT DEFAULT NULL,
  job_id bigint NOT NULL,
  organization_id bigint NOT NULL,
  status int NOT NULL DEFAULT 0,
  type int NOT NULL DEFAULT 0,
  created_at DATETIME DEFAULT NULL,
  created_by bigint DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  updated_by bigint DEFAULT NULL,
  validated_at DATETIME DEFAULT NULL,
  validated_by bigint DEFAULT NULL,
  recordStatus int NOT NULL DEFAULT 0,
  pdf varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_work_reports_contacts_idx` (`updated_by`),
 CONSTRAINT `fk_work_reports_validate_contacts` FOREIGN KEY (`validated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_work_reports_contacts_updated` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_work_reports_contacts_created` FOREIGN KEY (`created_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_work_reports_jobs` FOREIGN KEY (`job_id`) REFERENCES `jobs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_work_reports_organizations` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
    
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

USE calani;
alter table work_reports change description description TEXT;

USE calani;
alter table tutorials_screens change explanation explanation TEXT;

