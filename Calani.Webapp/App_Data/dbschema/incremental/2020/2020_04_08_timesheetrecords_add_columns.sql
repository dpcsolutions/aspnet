USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='timesheetrecords' and column_name='comment') > 0,    
                         "SELECT 0",
    "alter table timesheetrecords 
    add column comment longtext DEFAULT NULL,
	add column parent_id bigint DEFAULT NULL,
    add column order_index int DEFAULT NULL,
    ADD CONSTRAINT fk_timesheetrecords_timesheetrecords	FOREIGN KEY (parent_id) REFERENCES timesheetrecords (id) ON DELETE CASCADE ON UPDATE NO ACTION
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;

