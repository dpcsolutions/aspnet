USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='show_contact_info_invoices') > 0,
    "SELECT 0",
    "alter table organizations 
	add column show_contact_info_invoices tinyint(1) NOT NULL DEFAULT 1;
       
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;