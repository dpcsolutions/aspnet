USE calani;
CREATE TABLE IF NOT EXISTS tutorials_stat (
  id bigint NOT NULL AUTO_INCREMENT,
  tutorial_id BIGINT NOT NULL,
  viewed_by BIGINT NOT NULL,
  viewed_date DATETIME DEFAULT NULL,

  recordStatus int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),

  KEY `fk_tutorials_stat_contacts_idx` (`viewed_by`),
  KEY `fk_tutorials_stat_tutorial_idx` (`tutorial_id`),

  CONSTRAINT `fk_tutorials_stat_contacts` FOREIGN KEY (`viewed_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tutorials_stat_tutorials` FOREIGN KEY (`tutorial_id`) REFERENCES `tutorials` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


USE calani;
SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='tutorials' and column_name='min_version' and data_type = 'varchar' ) > 0,
    
    "ALTER TABLE tutorials DROP COLUMN min_version;",
    "SELECT 0"));

PREPARE stmt FROM @sql;
EXECUTE stmt;

USE calani;
SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='tutorials' and column_name='min_version' and data_type = 'decimal' ) > 0,
    
    "SELECT 0",
    "ALTER TABLE tutorials ADD   min_version DECIMAL(8,4) NOT NULL DEFAULT 0 ;"));

PREPARE stmt FROM @sql;
EXECUTE stmt;
