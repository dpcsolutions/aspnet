USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='timesheet_lines_per_day') > 0,
    "SELECT 0",
    "alter table organizations 
	add column timesheet_lines_per_day INT NOT NULL DEFAULT 1;
       
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;