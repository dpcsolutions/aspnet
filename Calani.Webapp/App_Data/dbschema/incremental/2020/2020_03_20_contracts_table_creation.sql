USE calani;

CREATE TABLE IF NOT EXISTS contacts_contracts (
  id bigint NOT NULL AUTO_INCREMENT,
  start_date DATETIME DEFAULT NULL,
  rate INT DEFAULT NULL,
  duration INT DEFAULT NULL, /*in minutes*/
  overtime_allowed BIT NOT NULL DEFAULT 0,
  contact_Id BIGINT NOT NULL,
  updated_at DATETIME DEFAULT NULL,
  updated_by bigint DEFAULT NULL,
  recordStatus int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_contracts_contacts_upd_idx` (`updated_by`),
  KEY `fk_contracts_contacts_idx` (`contact_Id`),
  CONSTRAINT `fk_contracts_contacts_upd` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contracts_contacts` FOREIGN KEY (`contact_Id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;