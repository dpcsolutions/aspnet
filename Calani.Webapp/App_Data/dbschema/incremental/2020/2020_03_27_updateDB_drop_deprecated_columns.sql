USE calani;


PREPARE stmt FROM @sql;
EXECUTE stmt;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_schema = schema() and table_name='expenses' and column_name='amountInCurrency') > 0,
    "alter table expenses 
    drop column amountInCurrency;" ,
    "SELECT 0"
    
));

PREPARE stmt FROM @sql;
EXECUTE stmt;

