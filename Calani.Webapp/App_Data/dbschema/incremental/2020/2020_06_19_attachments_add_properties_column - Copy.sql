USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='attachments' and column_name='properties') > 0,
    "SELECT 0",
    "alter table attachments 
	add column properties INT NOT NULL DEFAULT 0
    
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;
