USE calani;

CREATE TABLE IF NOT EXISTS work_schedule_profiles (
  id bigint NOT NULL AUTO_INCREMENT,
  name varchar(45) DEFAULT NULL,
  code varchar(45) DEFAULT NULL,
   organization_id BIGINT NOT NULL,
  updated_at DATETIME DEFAULT NULL,
  updated_by bigint DEFAULT NULL,
  recordStatus int default null,
  PRIMARY KEY (`id`),
  KEY `fk_profiles_contacts_idx` (`updated_by`),
  KEY `fk_work_schedule_profiles_organizations_idx` (`organization_Id`),  
  CONSTRAINT `fk_work_schedule_profiles_contacts`			FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_work_schedule_profiles_organizations`	FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS work_schedule_profiles_rec (
  id bigint NOT NULL AUTO_INCREMENT,
  profile_id BIGINT NULL,
  organization_id BIGINT NOT NULL,
  color varchar(8) DEFAULT NULL,
  name varchar(45) DEFAULT NULL,
  start varchar(8) DEFAULT NULL,
  end varchar(8) DEFAULT NULL,
  rate INT NOT NULL DEFAULT 100,
  mo BIT NOT NULL DEFAULT 0,
  tu BIT NOT NULL DEFAULT 0,
  we BIT NOT NULL DEFAULT 0,
  th BIT NOT NULL DEFAULT 0,
  fr BIT NOT NULL DEFAULT 0,
  sa BIT NOT NULL DEFAULT 0,
  su BIT NOT NULL DEFAULT 0,
  type int NOT NULL DEFAULT 0,
  updated_at DATETIME DEFAULT NULL,
  updated_by BIGINT DEFAULT NULL,
  recordStatus INT DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_work_schedule_profiles_rec_contacts_idx` (`updated_by`),
  KEY `fk_work_schedule_profiles_rec_organizations` (`organization_id`),
  CONSTRAINT `fk_work_schedule_profiles_rec_contacts` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_work_schedule_profiles_rec_organizations`	FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS work_schedule_profiles_calendars (
  id bigint NOT NULL AUTO_INCREMENT,  
  calendar_id BIGINT DEFAULT NULL,
  profile_id BIGINT NOT NULL,
  updated_at DATETIME DEFAULT NULL,
  updated_by bigint DEFAULT NULL,
  recordStatus int default null,
  PRIMARY KEY (`id`),
  KEY `fk_profiles_calendars_work_schedule_profiles_idx` (`profile_id`),
  KEY `fk_profiles_calendars_calendars_idx` (`calendar_Id`),
  CONSTRAINT `fk_work_schedule_profiles`			FOREIGN KEY (`profile_id`) REFERENCES `work_schedule_profiles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_calendars`		FOREIGN KEY (`calendar_Id`) REFERENCES `calendars` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
