USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='vacation') > 0,
    "SELECT 0",
    "alter table organizations 
	add column vacation INT NOT NULL DEFAULT 20
    
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;


USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='contacts_contracts' and column_name='vacation_year') > 0,
    "SELECT 0",
    "alter table contacts_contracts 
	add column vacation_year INT NOT NULL DEFAULT 20,
    add column vacation_carried DECIMAL(4,2) NOT NULL DEFAULT 0,
    add column vacation_remains DECIMAL(4,2) NOT NULL DEFAULT 20,
    add column hour_rate DECIMAL(6,2) NOT NULL DEFAULT 0;
    
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;