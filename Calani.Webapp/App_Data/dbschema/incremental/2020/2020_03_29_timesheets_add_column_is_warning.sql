USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='timesheets' and column_name='is_warning') > 0,
    "SELECT 0",
    "alter table timesheets   
	add column is_warning BIT NOT NULL DEFAULT 0    
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;

