USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='contacts' and column_name='overtime_allowed') > 0,
    "SELECT 0",
    "alter table contacts 
	add column  overtime_allowed BIT NOT NULL DEFAULT 0 "));

PREPARE stmt FROM @sql;
EXECUTE stmt;
