USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='work_schedule_profile_id') > 0,
    "SELECT 0",
    "alter table organizations 
	add column week_work_duration INT NOT NULL DEFAULT 2400, 
    add column work_schedule_profile_id bigint DEFAULT NULL,       
    ADD CONSTRAINT fk_work_schedule_profiles_organization	FOREIGN KEY (work_schedule_profile_id) REFERENCES work_schedule_profiles (id) ON DELETE NO ACTION ON UPDATE NO ACTION
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;

