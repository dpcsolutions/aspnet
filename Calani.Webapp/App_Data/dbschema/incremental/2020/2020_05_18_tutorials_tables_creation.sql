USE calani;

SELECT Count(*)
INTO @exists
FROM information_schema.tables 
WHERE  table_type = 'BASE TABLE'
    AND table_name = 'mobile_tutorials';

SET @query = If(@exists>0,
    'RENAME TABLE mobile_tutorials TO tutorials',
    'select 0');

PREPARE stmt FROM @query;

EXECUTE stmt;

SELECT Count(*)
INTO @exists
FROM information_schema.tables 
WHERE  table_type = 'BASE TABLE'
    AND table_name = 'mobile_tutorials_screens';

SET @query = If(@exists>0,
    'RENAME TABLE mobile_tutorials_screens TO tutorials_screens',
    'select 0');

PREPARE stmt FROM @query;

EXECUTE stmt;


SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='tutorials' and column_name='type') > 0,
    "SELECT 0",
    "alter table tutorials 
	add column type INT NOT NULL DEFAULT 0    
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;




CREATE TABLE IF NOT EXISTS tutorials (
  id bigint NOT NULL AUTO_INCREMENT,
  name varchar(45) DEFAULT NULL,
  type int NOT NULL DEFAULT 0,
  min_version DECIMAL(8,4) NOT NULL DEFAULT 0,
  device_deployed int  DEFAULT NULL,
  
  updated_at DATETIME DEFAULT NULL,
  updated_by bigint DEFAULT NULL,
  recordStatus int NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_tutorials_contacts_idx` (`updated_by`),
 
  CONSTRAINT `fk_tutorials_contacts` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS tutorials_screens (
  id bigint NOT NULL AUTO_INCREMENT,
  position INT NOT NULL,
  tutorial_id BIGINT NOT NULL,
  title varchar(500) DEFAULT NULL,
  explanation TEXT DEFAULT NULL,
  image LONGTEXT DEFAULT NULL,
  filename  varchar(1000) DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  updated_by BIGINT DEFAULT NULL,
  recordStatus INT DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_tutorials_screens_contacts_idx` (`updated_by`),
  KEY `fk_tutorials_screens_tutorials_idx` (`tutorial_id`),
  CONSTRAINT `fk_ctutorials_screens_contacts` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tutorials_screens_tutorials` FOREIGN KEY (`tutorial_id`) REFERENCES `tutorials` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS mobile_tutorials_deployed (
  id bigint NOT NULL AUTO_INCREMENT,  
  tutorial_id BIGINT NOT NULL,
  user_id BIGINT NOT NULL,
  version varchar(20),
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP, 
  PRIMARY KEY (`id`),
  KEY `fk_tutorials_deployed_contacts_idx` (`user_id`),
  KEY `fk_tutorials_deployed_tutorials_idx` (`tutorial_id`),
  CONSTRAINT `fk_tutorials_deployed_contacts` FOREIGN KEY (`user_id`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tutorials_deployed_tutorials` FOREIGN KEY (`tutorial_id`) REFERENCES `tutorials` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
