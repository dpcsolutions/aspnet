USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='timesheet_type') > 0,
    "SELECT 0",
    "alter table organizations 
	add column timesheet_type INT NOT NULL DEFAULT 0;
       
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;