
USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='departments' and column_name='recordStatus') > 0,
    "SELECT 0",
    "alter table departments 
    add column recordStatus int DEFAULT NULL;"
));

PREPARE stmt FROM @sql;
EXECUTE stmt;
