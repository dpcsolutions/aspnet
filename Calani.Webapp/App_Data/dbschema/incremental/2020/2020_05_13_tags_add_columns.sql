USE calani;

SET @sql = (SELECT IF(
  (
    (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='tags' and column_name='module') > 0)
  AND 						
(SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS 
  WHERE TABLE_NAME = 'tags' AND COLUMN_NAME = 'type') = 'int',
  "SELECT 1",
  "ALTER TABLE `tags` CHANGE COLUMN `type` `type` INT(11) NULL DEFAULT NULL AFTER `name`, ADD COLUMN `module` INT(11) NULL DEFAULT NULL AFTER `type`;"  
));
PREPARE stmt FROM @sql;
EXECUTE stmt;