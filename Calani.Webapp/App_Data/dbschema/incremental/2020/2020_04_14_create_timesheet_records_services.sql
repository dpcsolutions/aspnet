USE calani;

/*CREATE TABLE IF NOT EXISTS timesheet_records_services (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `timesheetId` bigint(20) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `visitId` bigint(20) DEFAULT NULL,
  `serviceId` bigint(20) DEFAULT NULL,
  `serviceName` varchar(45) DEFAULT NULL,
  `servicePrice` double DEFAULT NULL, 
  `duration` double DEFAULT NULL,
  `recordStatus` int(11) NOT NULL DEFAULT '0',
  `lastModificationDate` datetime DEFAULT NULL,
  `lastModifiedBy` bigint(20) DEFAULT NULL,
  `jobId` bigint(20) DEFAULT NULL,
  `serviceType` int(11) DEFAULT NULL,  
  `is_warning` bit(1) NOT NULL DEFAULT b'0',
  `comment` longtext,  
  PRIMARY KEY (`id`),
  KEY `fk_timesheet_records_services_timesheets_idx` (`timesheetId`),
  KEY `fk_lastModifiedBy_idx` (`lastModifiedBy`),
  KEY `fk_jobItem_jobId_idx` (`jobId`),
  KEY `fk_timesheet_records_services_service_idx` (`serviceId`),
  KEY `fk_timesheet_records_services_visit_idx` (`visitId`), 
  CONSTRAINT `fk_timesheet_records_services_job` FOREIGN KEY (`jobId`) REFERENCES `jobs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_timesheet_records_services_lastModifiedBy` FOREIGN KEY (`lastModifiedBy`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_timesheet_records_servicess_service` FOREIGN KEY (`serviceId`) REFERENCES `services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_timesheet_records_services_timesheets` FOREIGN KEY (`timesheetId`) REFERENCES `timesheets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_timesheet_records_services_visit` FOREIGN KEY (`visitId`) REFERENCES `visits` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;*/


/*run only if table is empty to prevent dsuplicaates*/
/*SET @sql = (SELECT IF( (SELECT COUNT(*) FROM timesheet_records_services ) > 0,
    "SELECT 0",
    "INSERT INTO	timesheet_records_services (timesheetId, date, visitId,serviceId, serviceName, servicePrice, duration, 
					recordStatus, lastModificationDate, lastModifiedBy, jobId, serviceType, is_warning, comment)
		SELECT	tr.timesheetId, tr.startDate, tr.visitId, tr.serviceId, tr.serviceName, tr.servicePrice, tr.duration, tr.recordStatus, 
			tr.lastModificationDate, tr.lastModifiedBy, tr.jobId, tr.serviceType, tr.is_warning, tr.comment
		FROM timesheetrecords tr WHERE serviceId>0 or jobId>0;
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;
*/

/*DROP TABLE IF EXISTS calani.timesheet_records_services;*/

