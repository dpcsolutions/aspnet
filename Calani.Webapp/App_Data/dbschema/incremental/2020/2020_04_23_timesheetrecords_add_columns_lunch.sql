USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='timesheetrecords' and column_name='lunch_duration') > 0,    
                         "SELECT 0",
    "alter table timesheetrecords 
    add column lunch_duration int DEFAULT 0,
    add column row_type int DEFAULT 0
    
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;

