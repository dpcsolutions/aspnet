USE calani;

ALTER TABLE organizations
ADD COLUMN use_price_including_vat BIT(1) DEFAULT 0 NOT NULL;


ALTER TABLE services
ADD COLUMN price_including_vat BIT(1) DEFAULT 0 NOT NULL;