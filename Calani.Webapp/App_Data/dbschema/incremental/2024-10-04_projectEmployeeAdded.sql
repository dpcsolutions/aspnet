CREATE TABLE projectEmployees (
      id BIGINT PRIMARY KEY AUTO_INCREMENT,
      projectId BIGINT NOT NULL,
      employeeId BIGINT NOT NULL,
      recordStatus BIGINT NOT NULL,
      dateCreated DATETIME DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE projectEmployees
    ADD CONSTRAINT FK_Job FOREIGN KEY (projectId) REFERENCES jobs(id);

ALTER TABLE projectEmployees
    ADD CONSTRAINT FK_Contact FOREIGN KEY (employeeId) REFERENCES contacts(id);
