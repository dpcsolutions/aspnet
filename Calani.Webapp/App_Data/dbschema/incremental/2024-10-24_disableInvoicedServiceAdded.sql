USE calani;

ALTER TABLE organizations
    ADD COLUMN disable_invoiced_service BIT(1) DEFAULT 1 NOT NULL;
