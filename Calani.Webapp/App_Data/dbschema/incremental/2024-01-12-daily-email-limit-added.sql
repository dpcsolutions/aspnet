USE calani;

ALTER TABLE organizations
ADD COLUMN dailyMailsLimit INT DEFAULT 20 NOT NULL;


CREATE TABLE mailsquota (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    organisation_id BIGINT,
    date DATE,
    mails_sent INT,
    FOREIGN KEY (organisation_id) REFERENCES organizations(id)
);

-- UPDATE organizations
-- SET dailyMailsLimit = 50;

-- ALTER TABLE calani.organizations
-- MODIFY COLUMN dailyMailsLimit INT NOT NULL;
-- ALTER TABLE calani.organizations
-- ALTER COLUMN dailyMailsLimit SET DEFAULT 20;

-- UPDATE organizations
-- SET dailyMailsLimit = 50;
