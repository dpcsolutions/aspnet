CREATE TABLE DualAuthenticationTokens (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    contactId bigint NOT NULL,
    token NVARCHAR(255) NOT NULL,
    isUsed BOOLEAN NOT NULL,
    validUntill datetime NOT NULL,
    FOREIGN KEY (contactId) REFERENCES contacts(id)
);

ALTER TABLE contacts
ADD COLUMN enableDualAuthentication BIT DEFAULT 0 NOT NULL;

