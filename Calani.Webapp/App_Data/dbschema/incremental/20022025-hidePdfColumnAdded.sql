﻿USE calani;

SET @sql = (SELECT
IF ((SELECT COUNT(*)
     FROM INFORMATION_SCHEMA.COLUMNS
     WHERE table_name = 'organizations'
       and column_name = 'hide_code_on_pdf') > 0,
    "SELECT 0",
    "alter table organizations 
add column hide_code_on_pdf tinyint(1) NOT NULL DEFAULT 0;

"));

PREPARE stmt FROM @sql;
EXECUTE stmt;