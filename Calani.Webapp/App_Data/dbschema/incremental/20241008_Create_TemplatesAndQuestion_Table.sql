-- Dumping structure for table calani.templates
CREATE TABLE IF NOT EXISTS `templates` (
  `templateId` bigint(20) NOT NULL AUTO_INCREMENT,
  `listName` varchar(255) DEFAULT NULL,
  `dateCreated` datetime DEFAULT NULL,
  `organizationId` bigint(20) DEFAULT NULL,
  `lastUpdate` datetime DEFAULT NULL,
  `createdBy` bigint(20) DEFAULT NULL,
  `updatedBy` bigint(20) DEFAULT NULL,
  `recordStatus` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`templateId`),
  KEY `fk_templates_organization_idx` (`organizationId`),
  CONSTRAINT `fk_templates_organizations` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;


-- Dumping structure for table calani.questions
CREATE TABLE IF NOT EXISTS `questions` (
  `questionID` bigint(20) NOT NULL AUTO_INCREMENT,
  `templateId` bigint(20) NOT NULL,
  `question` varchar(200) NOT NULL,
  `descriptions` varchar(500) DEFAULT NULL,
  `responseType` varchar(100) DEFAULT NULL,
  `lastUpdate` datetime DEFAULT NULL,
  `createdBy` bigint(20) DEFAULT NULL,
  `updatedBy` bigint(20) DEFAULT NULL,
  `isMandatory` bit(1) DEFAULT b'0',
  `recordStatus` int(11) NOT NULL DEFAULT '0',
  `suspendedQID` bigint(20) NOT NULL,
  PRIMARY KEY (`questionID`),
  KEY `fk_questions_templates` (`templateId`),
  CONSTRAINT `fk_questions_templates` FOREIGN KEY (`templateId`) REFERENCES `templates` (`templateId`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


