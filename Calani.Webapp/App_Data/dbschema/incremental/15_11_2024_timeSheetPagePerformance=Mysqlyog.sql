-- DROP FUNCTION IF EXISTS  DaysInWeekForTemplate;
-- DROP FUNCTION IF EXISTS  GetIso8601MondayOfWeek;
-- DROP FUNCTION IF EXISTS  GetDurationByTimesheetId;
-- DROP FUNCTION IF EXISTS  GetDurationByTimesheetId;
-- DROP FUNCTION IF EXISTS  GetCommentNumberTimesheetId;
-- DROP PROCEDURE IF EXISTS  ListTimesheetsExtForManager;

DELIMITER //

CREATE FUNCTION DaysInWeekForTemplate(timesheetType INT)
RETURNS INT DETERMINISTIC
BEGIN
    RETURN CASE
        WHEN timesheetType = 1 THEN 4  -- DetailedWorkDays
        WHEN timesheetType = 2 THEN 5  -- SimplifiedEntry
        ELSE 6  -- Default, can be customized if needed
    END;
END //

DELIMITER ;

DELIMITER //

CREATE FUNCTION GetIso8601MondayOfWeek(YEAR INT, WEEK INT)
RETURNS DATE DETERMINISTIC
BEGIN
    RETURN DATE_ADD(MAKEDATE(YEAR, 1), INTERVAL (WEEK - 1) * 7 DAY);
END //

DELIMITER ;
DELIMITER $$

CREATE FUNCTION GetDurationByTimesheetId(TimesheetId BIGINT)
RETURNS DECIMAL(18,3) DETERMINISTIC
BEGIN
    DECLARE totalDuration DECIMAL(18,3) DEFAULT 0;
    
    -- Sum the calculated duration considering lunch and pause deductions
    SELECT 
        SUM(
            tr.duration * tr.rate / 100 - 
            (tr.row_type * (COALESCE(tr.lunch_duration, 0)) / 60) - 
            ((NOT tr.row_type) * (COALESCE(tr.pause_duration, 0)) / 60)
        ) 
    INTO totalDuration
    FROM timesheetrecords AS tr
    WHERE tr.timesheetId = TimesheetId
      AND tr.recordStatus = 0
      AND tr.duration <> 0
      AND tr.parent_id IS NULL;
    
    -- Return the total duration, ensuring it's 0 if NULL
    RETURN COALESCE(totalDuration, 0);
END $$

DELIMITER ;


DELIMITER $$

CREATE FUNCTION GetCommentNumberTimesheetId(TimesheetId BIGINT)
RETURNS INT DETERMINISTIC
BEGIN
    DECLARE totalComments INT DEFAULT 0;

    -- Count the comments where the timesheetId matches and comment is not empty
    SELECT COUNT(*) 
    INTO totalComments
    FROM timesheetrecords AS tr
    WHERE tr.timesheetId = TimesheetId
      AND tr.recordStatus = 0
      AND tr.comment <> '';

    RETURN totalComments;
END $$

DELIMITER ;

DELIMITER $$
-- DROP PROCEDURE IF EXISTS  ListTimesheetsExtForManager;
CREATE PROCEDURE ListTimesheetsExtForManager(
    OrgId BIGINT, 
    AlreadyValidated TINYINT, 
    StartWeek INT, 
    StartYear INT, 
    EndWeek INT, 
    EndYear INT, 
    EmployeeId BIGINT, 
    SheetType INT
)
BEGIN
    
    -- Start the SELECT query
    SELECT 
        t.*,
        TRIM(CONCAT(c.lastName, ' ', c.firstName)) AS EmployeeName,
        c.initials AS EmployeeInitials,
        GetIso8601MondayOfWeek(t.year, t.week) AS sheetDate,  -- Use the declared variable
        CASE
            WHEN t.acceptedDate IS NULL THEN 
                CASE
		    WHEN GetIso8601MondayOfWeek(t.year, t.week) IS NOT NULL AND t.rejectDate IS NULL 
			 AND CURDATE() <= DATE_ADD(GetIso8601MondayOfWeek(t.year, t.week), INTERVAL DaysInWeekForTemplate(SheetType) DAY) 
                    THEN 'currentweek'
                    ELSE 'tovalidate'
                END
            ELSE 'validated'
        END AS sheetStatus,
        c.weeklyHours AS AttendedDuration,
        GetDurationByTimesheetId(t.id) AS TotalDuration,
        GetCommentNumberTimesheetId(t.id) AS CommentNumber
    FROM timeSheets AS t
        INNER JOIN Contacts AS c ON t.employeeId = c.Id
    WHERE 
        t.recordStatus = 0
        AND t.organizationId = OrgId
        AND (
            AlreadyValidated IS NULL
            OR (AlreadyValidated = 1 AND t.acceptedDate IS NOT NULL) 
            OR (AlreadyValidated = 0 AND t.acceptedDate IS NULL)
        )
        AND (
            (StartWeek < 0) 
            OR (t.week >= StartWeek AND t.year >= StartYear)
        )
        AND (
            (EndWeek < 0) 
            OR (t.year < EndYear) 
            OR (t.year = EndYear AND t.week <= EndWeek)
        )
        AND (
            (EmployeeId < 0) 
            OR (t.employeeId = EmployeeId)
        );
END $$

DELIMITER ;
