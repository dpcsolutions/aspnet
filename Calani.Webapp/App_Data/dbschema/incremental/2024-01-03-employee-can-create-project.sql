USE calani;

ALTER TABLE organizations
ADD COLUMN employee_create_projects BIT(1) DEFAULT 0 NOT NULL;
