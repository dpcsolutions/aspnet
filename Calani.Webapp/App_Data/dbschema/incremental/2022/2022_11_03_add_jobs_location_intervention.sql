USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='location_intervention') > 0,    
                         "SELECT 0",
    "alter table jobs     
    add column location_intervention varchar(400)
    
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;


