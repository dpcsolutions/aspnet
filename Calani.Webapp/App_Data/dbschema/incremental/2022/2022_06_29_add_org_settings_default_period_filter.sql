USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='default_period_filter') > 0,
    "SELECT 0",
    "alter table organizations 
	add column default_period_filter TINYINT;
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;