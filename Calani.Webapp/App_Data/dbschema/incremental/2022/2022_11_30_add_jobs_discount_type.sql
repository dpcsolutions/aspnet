USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='discount_type') > 0,    
                         "SELECT 0",
    "alter table calani.jobs     
    add column discount_type tinyint
    
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;


