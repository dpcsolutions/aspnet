USE calani;

SET @sql_militaryDays = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='contacts' and column_name='militaryDays') > 0,
    "SELECT 0",
    "alter table calani.contacts 
	ADD COLUMN militaryDays DOUBLE NOT NULL DEFAULT 0;
    "));
    

PREPARE stmt1 FROM @sql_militaryDays;
EXECUTE stmt1;