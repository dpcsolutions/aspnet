USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='absence_day_duration_minutes') > 0,
    "SELECT 0",
    "alter table organizations 
	add column absence_day_duration_minutes int(11) NOT NULL DEFAULT 480;       
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;