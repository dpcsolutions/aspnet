USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='hide_gesmob_label') > 0,
    "SELECT 0",
    "alter table organizations 
	add column hide_gesmob_label BIT NOT NULL DEFAULT 0;
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;