USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='services' and column_name='unitId') > 0,
    "SELECT 0",
    "alter table calani.services add column unitId BIGINT(20);
     alter table calani.services add INDEX services_units_FK_idx (unitId ASC);
     alter table calani.services add CONSTRAINT services_units_FK
     FOREIGN KEY (unitId)
     REFERENCES calani.units (id)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION;
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;