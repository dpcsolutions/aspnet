USE calani;

SET @sql1 = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='services' and column_name='name') > 0,
    "ALTER TABLE services MODIFY name varchar(600) NOT NULL;",
    "SELECT 0"
    ));

PREPARE stmt1 FROM @sql1;
EXECUTE stmt1;

SET @sql2 = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='services_jobs' and column_name='serviceName') > 0,
    "ALTER TABLE services_jobs MODIFY serviceName varchar(600) NULL;",
    "SELECT 0"    
    ));

PREPARE stmt2 FROM @sql2;
EXECUTE stmt2;

SET @sql3 = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='timesheet_records_services' and column_name='serviceName') > 0,
    "ALTER TABLE timesheet_records_services MODIFY serviceName varchar(600) NULL;",
    "SELECT 0"
    ));

PREPARE stmt3 FROM @sql3;
EXECUTE stmt3;

SET @sql4 = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='timesheetrecords' and column_name='serviceName') > 0,
    "ALTER TABLE timesheetrecords MODIFY serviceName varchar(600) NULL;",
    "SELECT 0"
    ));

PREPARE stmt4 FROM @sql4;
EXECUTE stmt4;