USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='hide_0_discount_col') > 0,
    "SELECT 0",
    "alter table organizations 
	add column hide_0_discount_col BIT NOT NULL DEFAULT 0;
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;