USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='hide_unit_column') > 0,
    "SELECT 0",
    "alter table calani.organizations 
	add column hide_unit_column BIT NOT NULL DEFAULT 1;
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;