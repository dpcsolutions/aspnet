USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='timesheetrecords' and column_name='disable_import') > 0,
    "SELECT 0",
    "alter table timesheetrecords 
	add column disable_import BIT NOT NULL DEFAULT 0;
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;