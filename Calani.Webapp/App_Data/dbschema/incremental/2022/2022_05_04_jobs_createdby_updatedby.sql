USE calani;

SET @sql_createdBy = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='createdBy') > 0,
    "SELECT 0",
    "alter table jobs 
    ADD COLUMN createdBy BIGINT(20) NULL,
    ADD INDEX jobs_contacts_createdby_FK_idx (createdBy ASC),
    ADD CONSTRAINT jobs_contacts_createdby_FK
    FOREIGN KEY (createdBy)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    "));

SET @sql_updatedBy = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='updatedBy') > 0,
    "SELECT 0",
    "alter table jobs 
    ADD COLUMN updatedBy BIGINT(20) NULL,
    ADD INDEX jobs_contacts_updatedby_FK_idx (updatedBy ASC),
    ADD CONSTRAINT jobs_contacts_updatedby_FK
    FOREIGN KEY (updatedBy)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    "));

PREPARE stmt1 FROM @sql_createdBy;
EXECUTE stmt1;

PREPARE stmt2 FROM @sql_updatedBy;
EXECUTE stmt2;