USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='discount_percent') > 0,    
                         "SELECT 0",
    "alter table calani.jobs     
    add column discount_percent float
    
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;


