CREATE TABLE IF NOT EXISTS calani.units (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  stringResourceName VARCHAR(50),
  organizationId BIGINT(20),
  PRIMARY KEY (id),
  UNIQUE INDEX unit_name_UNIQUE (name ASC),
  INDEX units_organizations_FK_idx (organizationId ASC),
  CONSTRAINT units_organizations_FK
    FOREIGN KEY (organizationId)
    REFERENCES calani.organizations (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

INSERT IGNORE INTO calani.units (name, stringResourceName) VALUES ("dumpster", "Dumpster");
INSERT IGNORE INTO calani.units (name, stringResourceName) VALUES ("gl", null);
INSERT IGNORE INTO calani.units (name, stringResourceName) VALUES ("ha", null);
INSERT IGNORE INTO calani.units (name, stringResourceName) VALUES ("hours", "Hours");
INSERT IGNORE INTO calani.units (name, stringResourceName) VALUES ("m�", null);
INSERT IGNORE INTO calani.units (name, stringResourceName) VALUES ("m�", null);
INSERT IGNORE INTO calani.units (name, stringResourceName) VALUES ("Kg", null);
INSERT IGNORE INTO calani.units (name, stringResourceName) VALUES ("Km", null);
INSERT IGNORE INTO calani.units (name, stringResourceName) VALUES ("Ml", null);
INSERT IGNORE INTO calani.units (name, stringResourceName) VALUES ("pc", null);
INSERT IGNORE INTO calani.units (name, stringResourceName) VALUES ("To", null);
INSERT IGNORE INTO calani.units (name, stringResourceName) VALUES ("shipping", "Shipping");
