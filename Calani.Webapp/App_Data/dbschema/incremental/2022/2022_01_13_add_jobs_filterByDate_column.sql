USE calani;

SET @sql_filterByDate = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='filterByDate') > 0,
    "SELECT 0",
    "alter table jobs 
	ADD COLUMN filterByDate DATETIME NOT NULL DEFAULT 0;
    "));
    

PREPARE stmt1 FROM @sql_filterByDate;
EXECUTE stmt1;