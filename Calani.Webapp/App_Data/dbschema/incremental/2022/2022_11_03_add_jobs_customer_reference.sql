USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='customer_reference') > 0,    
                         "SELECT 0",
    "alter table jobs     
    add column customer_reference varchar(100)
    
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;


