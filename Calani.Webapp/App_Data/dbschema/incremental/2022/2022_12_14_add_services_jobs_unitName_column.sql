USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='services_jobs' and column_name='unitName') > 0,    
                         "SELECT 0",
    "alter table calani.services_jobs     
    add column unitName varchar(20)   
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;


