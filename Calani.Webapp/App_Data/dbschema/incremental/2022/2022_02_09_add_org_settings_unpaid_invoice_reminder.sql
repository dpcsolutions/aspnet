USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='unpaid_invoice_reminder_days') > 0,
    "SELECT 0",
    "alter table organizations 
	add column unpaid_invoice_reminder_days int(11) NOT NULL DEFAULT 0;       
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;