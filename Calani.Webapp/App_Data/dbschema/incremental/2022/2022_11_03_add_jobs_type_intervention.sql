USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='type_intervention') > 0,    
                         "SELECT 0",
    "alter table jobs     
    add column type_intervention varchar(100)
    
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;


