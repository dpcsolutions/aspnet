USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='delivery_note_pdf_type') > 0,    
                         "SELECT 0",
    "alter table organizations     
    add column delivery_note_pdf_type tinyint NOT NULL DEFAULT 0
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;


