USE calani;

SET @sql_validationStatus = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='expenses' and column_name='validationStatus') > 0,
    "SELECT 0",
    "alter table expenses
    ADD COLUMN validationStatus TINYINT(2) NOT NULL DEFAULT 0;
    "));

SET @sql_validatedBy = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='expenses' and column_name='validatedBy') > 0,
    "SELECT 0",
    "alter table expenses 
    ADD COLUMN validatedBy BIGINT(20) NULL,
    ADD INDEX expenses_contacts_validatedby_FK_idx (validatedBy ASC),
    ADD CONSTRAINT expenses_contacts_validatedby_FK
    FOREIGN KEY (validatedBy)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    "));

SET @sql_validatedAt = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='expenses' and column_name='validatedAt') > 0,
    "SELECT 0",
    "alter table expenses 
	ADD COLUMN validatedAt DATETIME NULL DEFAULT NULL;
    "));

PREPARE stmt1 FROM @sql_validationStatus;
EXECUTE stmt1;

PREPARE stmt2 FROM @sql_validatedBy;
EXECUTE stmt2;

PREPARE stmt3 FROM @sql_validatedAt;
EXECUTE stmt3;

update calani.expenses set validationStatus = 1 where recordStatus != -1 and approvedmount > 0;