ALTER TABLE jobs
ADD recurringInfoId BIGINT NULL UNIQUE;

CREATE TABLE recurringInfos (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    invoiceId BIGINT NOT NULL UNIQUE, 
    isActive BOOLEAN NOT NULL DEFAULT 0,
    frequencyCount INT NOT NULL,
    specificDayCount INT NOT NULL,
    specificMonthCount INT NOT NULL,
    frequencyType INT NOT NULL,
    isLastDay BOOLEAN NOT NULL DEFAULT 0,
    isSpecificDay BOOLEAN NOT NULL DEFAULT 0,
    nextDueDate DATETIME NOT NULL,
    nextExecutionDate DATETIME NOT NULL,
    isUntilNotice BOOLEAN NOT NULL DEFAULT 0,
    isSpecificMonth BOOLEAN NOT NULL DEFAULT 0,
    lastDueDate DATETIME NOT NULL,
    invoiceSendingType INT NOT NULL,
	backgroundId NVARCHAR(2048),
	daysInAdvance INT NOT NULL DEFAULT 0

);


ALTER TABLE recurringInfos
ADD CONSTRAINT FK_recurringInfos_jobs
FOREIGN KEY (invoiceId) REFERENCES jobs(id) ON DELETE CASCADE;

ALTER TABLE jobs
ADD CONSTRAINT FK_jobs_recurringInfos
FOREIGN KEY (recurringInfoId) REFERENCES recurringInfos(id) ON DELETE CASCADE;
