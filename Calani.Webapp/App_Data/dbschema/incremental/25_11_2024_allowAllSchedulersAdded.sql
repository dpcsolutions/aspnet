USE calani;

ALTER TABLE organizations
    ADD COLUMN give_all_empl_access_to_schedulers BIT(1) DEFAULT 0 NOT NULL;
