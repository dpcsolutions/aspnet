ALTER TABLE resource_allocations ADD COLUMN departmentId INT NULL;
ALTER TABLE resource_allocations ADD COLUMN parentId bigint NULL;


ALTER TABLE resource_allocations
ADD CONSTRAINT resource_allocations_department_FK
FOREIGN KEY (departmentId)
REFERENCES Departments(id);