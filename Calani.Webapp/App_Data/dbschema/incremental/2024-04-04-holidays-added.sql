INSERT INTO work_schedule_profiles_rec (organization_id, name, rate, mo, tu, we, th, fr, sa, su, type)
SELECT
    org.Id AS organization_id,
    'Férié' AS name,
    100 AS rate,
    0 AS mo,
    0 AS tu,
    0 AS we,
    0 AS th,
    0 AS fr,
    0 AS sa,
    0 AS su,
    14 AS type
FROM
    organizations org
LEFT JOIN
    work_schedule_profiles_rec wsp ON org.Id = wsp.organization_id
WHERE
    wsp.organization_id IS NULL;