USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='services_jobs' and column_name='recordStatus') > 0,
    "SELECT 0",
    "alter table services_jobs 
	add column recordStatus int(11) NOT NULL DEFAULT 0;
       
    "));

SET @sql_updatedAt = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='services_jobs' and column_name='updatedAt') > 0,
    "SELECT 0",
    "alter table services_jobs 
    ADD COLUMN updatedAt DATETIME NULL;
    "));

SET @sql_updatedBy = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='services_jobs' and column_name='updatedBy') > 0,
    "SELECT 0",
    "alter table services_jobs 
    ADD COLUMN updatedBy BIGINT(20) NULL,
    ADD INDEX services_jobs_contacts_updatedby_FK_idx (updatedBy ASC),
    ADD CONSTRAINT services_jobs_contacts_updatedby_FK
    FOREIGN KEY (updatedBy)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION;
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;

PREPARE stmt1 FROM @sql_updatedAt;
EXECUTE stmt1;

PREPARE stmt2 FROM @sql_updatedBy;
EXECUTE stmt2;