USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='hide_vat_pdf') > 0,
    "SELECT 0",
    "alter table calani.organizations 
	add column hide_vat_pdf BIT NOT NULL DEFAULT 0;
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;