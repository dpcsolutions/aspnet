USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='attachments' and column_name='contactId') > 0,
    "SELECT 0",
    "alter table calani.attachments add column contactId BIGINT(20);
     alter table calani.attachments add INDEX attachments_contacts_FK_idx (contactId ASC);
     alter table calani.attachments add CONSTRAINT attachments_contacts_FK
     FOREIGN KEY (contactId)
     REFERENCES calani.contacts (id)
     ON DELETE NO ACTION
     ON UPDATE NO ACTION;
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;