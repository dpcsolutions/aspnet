USE calani;

CREATE TABLE IF NOT EXISTS calani.contacts_attachments (
  Id bigint(20) NOT NULL AUTO_INCREMENT,
  organizationId bigint(20) DEFAULT NULL,
  userId bigint(20) DEFAULT NULL,
  attachmentId bigint(20) DEFAULT NULL,
  contactId bigint(20) DEFAULT NULL,
  PRIMARY KEY (Id),
  KEY fk_contacts_attachments_organization_idx (organizationId),
  KEY fk_contacts_attachments_user_idx (userId),
  KEY fk_contacts_attachments_attachment_idx (attachmentId),
  KEY fk_contacts_attachments_contact_idx (contactId),
  CONSTRAINT fk_contacts_attachments_attachment FOREIGN KEY (attachmentId) REFERENCES calani.attachments (id),
  CONSTRAINT fk_contacts_attachments_contact FOREIGN KEY (contactId) REFERENCES calani.contacts (id),
  CONSTRAINT fk_contacts_attachments_organization FOREIGN KEY (organizationId) REFERENCES calani.organizations (id),
  CONSTRAINT fk_contacts_attachments_user FOREIGN KEY (userId) REFERENCES calani.contacts (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
