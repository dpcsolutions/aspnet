USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='contacts_contracts' and column_name='overtime_rate') > 0,
    "SELECT 0",
    "alter table contacts_contracts
    add column overtime_rate DECIMAL(6,2) NOT NULL DEFAULT 0,
    add column overtime_rate2 DECIMAL(6,2) NOT NULL DEFAULT 0,
    add column overtime_rate2_threshold_hours DECIMAL(6,2) NOT NULL DEFAULT 0,
    add column overtime_rate2_enabled BIT NOT NULL DEFAULT 0;
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;