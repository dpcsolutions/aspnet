CREATE TABLE IF NOT EXISTS calani.contacts_overtime (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  total_hours_effectively_compensated DECIMAL(6,2) NOT NULL DEFAULT 0,
  total_hours_actually_paid DECIMAL(6,2) NOT NULL DEFAULT 0,
  contact_id BIGINT(20),
  PRIMARY KEY (id),
  UNIQUE INDEX contacts_overtime_contact_id_UNIQUE (contact_id ASC),
  INDEX contacts_overtime_contacts_FK_idx (contact_id ASC),
  CONSTRAINT contacts_overtime_contacts_FK
    FOREIGN KEY (contact_id)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);