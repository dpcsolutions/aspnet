USE calani;

CREATE TABLE IF NOT EXISTS calani.vacation_carried_over_days (
  id BIGINT(20) NOT NULL AUTO_INCREMENT,
  employee_id BIGINT(20) NOT NULL,
  year SMALLINT UNSIGNED NOT NULL,
  days DECIMAL(4,2) NOT NULL DEFAULT 0,
  organization_id BIGINT(20) NOT NULL,
  updated_at DATETIME NULL,
  updated_by BIGINT(20) NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX vacation_carried_over_days_employee_year_UNIQUE (employee_id, year),
  INDEX vacation_carried_over_days_organizations_FK_idx (organization_id ASC),
  INDEX vacation_carried_over_days_contacts_employee_id_FK_idx (employee_id ASC),
  INDEX vacation_carried_over_days_contacts_updatedby_FK_idx (updated_by ASC),
  CONSTRAINT vacation_carried_over_days_organizations_FK
    FOREIGN KEY (organization_id)
    REFERENCES calani.organizations (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT vacation_carried_over_days_contacts_employee_id_FK_idx
    FOREIGN KEY (employee_id)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT vacation_carried_over_days_contacts_updatedby_FK
    FOREIGN KEY (updated_by)
    REFERENCES calani.contacts (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
