USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='timesheet_email_option') > 0,
    "SELECT 0",
    "alter table calani.organizations 
	add column timesheet_email_option INT NOT NULL DEFAULT 0
    
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;
