USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='organizations' and column_name='hide_quote_acceptance_pdf') > 0,    
                         "SELECT 0",
    "alter table organizations     
    add column hide_quote_acceptance_pdf BIT NOT NULL DEFAULT 1
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;


