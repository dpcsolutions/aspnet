USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='skipMissingInvoiceNotif') > 0,    
                         "SELECT 0",
    "alter table calani.jobs     
    add column skipMissingInvoiceNotif bit not null default 0
    
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;
