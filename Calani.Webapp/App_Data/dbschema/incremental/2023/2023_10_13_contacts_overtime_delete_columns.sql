alter table calani.contacts_overtime drop column total_hours_worked;
alter table calani.contacts_overtime drop column total_accumulated_overtime_hours;
alter table calani.contacts_overtime drop column overtime_rate1_hours; 
alter table calani.contacts_overtime drop column overtime_rate2_hours;
alter table calani.contacts_overtime drop column total_hours_to_be_compensated; 