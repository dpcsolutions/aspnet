USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='services_jobs' and column_name='custom_number') > 0,
    "SELECT 0",
    "alter table services_jobs 
	add column custom_number VARCHAR(10) NULL;
    "));

PREPARE stmt FROM @sql;
EXECUTE stmt;