USE calani;

SET @sql = (SELECT IF( (SELECT EXISTS (SELECT 1 FROM calani.vacation_carried_over_days)) = 0,
	"INSERT INTO calani.vacation_carried_over_days (employee_id, year, days, organization_id, updated_at, updated_by)
    SELECT 
		cc.contact_Id,
        2022,
        (SELECT cc2.vacation_carried FROM calani.contacts_contracts cc2 JOIN calani.contacts c2 on cc2.contact_id = c2.id WHERE c2.id = c.id AND cc2.recordStatus = 0 ORDER BY cc2.start_date DESC LIMIT 1),
        c.organizationId,
        NOW(),
        @superadmin
	FROM calani.contacts_contracts cc JOIN calani.contacts c ON cc.contact_id = c.id GROUP BY c.id, c.organizationId;",
    "SELECT 0"
    ));

PREPARE stmt FROM @sql;
EXECUTE stmt;