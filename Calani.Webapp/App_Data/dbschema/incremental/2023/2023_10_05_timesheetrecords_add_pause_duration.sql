USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='timesheetrecords' and column_name='pause_duration') > 0,    
                         "SELECT 0",
    "alter table timesheetrecords 
    add column pause_duration int DEFAULT 0  
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;

