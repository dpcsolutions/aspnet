USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='discount_percent2') > 0,    
                         "SELECT 0",
    "alter table calani.jobs     
    add column discount_percent2 float
    
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='discount_percent3') > 0,    
                         "SELECT 0",
    "alter table calani.jobs     
    add column discount_percent3 float
    
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='discount_percent4') > 0,    
                         "SELECT 0",
    "alter table calani.jobs     
    add column discount_percent4 float
    
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;