USE calani;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='discount_type2') > 0,    
                         "SELECT 0",
    "alter table calani.jobs     
    add column discount_type2 tinyint
    
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='discount_type3') > 0,    
                         "SELECT 0",
    "alter table calani.jobs     
    add column discount_type3 tinyint
    
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;

SET @sql = (SELECT IF( (SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS 
						WHERE table_name='jobs' and column_name='discount_type4') > 0,    
                         "SELECT 0",
    "alter table calani.jobs     
    add column discount_type4 tinyint
    
	
    " ));

PREPARE stmt FROM @sql;
EXECUTE stmt;