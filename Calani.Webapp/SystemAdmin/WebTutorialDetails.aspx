﻿<%@ Page Title="TimeSheet" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Web_Tutorial_Details_Default" Codebehind="WebTutorialDetails.aspx.cs" %>

<asp:content contentplaceholderid="head" runat="server">
 <link rel="stylesheet" href="<%= Page.ResolveClientUrl("~//assets/css/vuetify.min.css") %>" type="text/css" />
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.1.45/css/materialdesignicons.min.css" type="text/css" />
</asp:content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
   
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
    <form runat="server">
        <div class="content"  id="app" >
            <v-app id="inspire">
                 <v-container >
                     <v-subheader>
                         <a name="quotes">
                             <i class="icon-grid5 display-inline-text "></i>
                             <strong><%= Resources.Resource.Details %></strong>
                         </a>
                     </v-subheader>
                     
                     <v-row>
                         <v-col cols="12" sm="8" md="8">
                             <v-text-field v-model="Name" label="<%= Resources.Resource.Description %>" validate-on-blur="true"  :rules="[rules.required]"></v-text-field>
                         </v-col>

                         <v-col cols="12" sm="2" md="2">
                             <v-text-field v-model="MinVersion" label="<%= Resources.Resource.MinVersion %>" placeholder="1.22" validate-on-blur="true" :rules="[rules.required, rules.version]"></v-text-field>
                         </v-col>
                         <v-col cols="12" sm="2" md="2">
                             <v-text-field v-model="Screens.length" disabled label="<%= Resources.Resource.Screens %>"></v-text-field>
                         </v-col>
                     </v-row>
                
                     <v-subheader>
                         <a name="quotes">
                             <i class="icon-stack-picture display-inline-block "></i>
                             <strong><%= Resources.Resource.Screens %></strong>
                         </a>
                     </v-subheader>
                    <v-sheet  class="" elevation="3" >
                                <div class="row">
                                    <div class="col-sm-7 col-md-8 col-lg-9">
                                        <v-slide-group v-model="model" class="pa-4" prev-icon="icon-circle-left2" next-icon="icon-circle-right2" show-arrows center-active>

                                            <draggable :list="itemsFiltered" tag="div" @start="drag = true" @end="drag = false" :component-data="getComponentData()">

                                                <v-slide-item  v-for="(el, index) in itemsFiltered" :value="el" :key="el.Id"  v-slot:default="{ active, toggle }">
                                                    <div class="text-center">
                                                        <v-card  :color="'grey lighten-4'" class="ma-4"  height="600" width="800" @click="onCardClick(el)">
                                                        <span class="hidden">{{el.Position=index}}</span>

                                                        <v-card-title class="justify-center p-20">{{el.Title}}</v-card-title>

                                                        <v-card-subtitle class="p-10 text-center"  v-html="el.Explanation">
                                                           
                                                        </v-card-subtitle>

                                                            <v-card-text>
                                                                <v-img class="white--text align-end" max-height="400"  :src="el.Image" ></v-img>
                                                            </v-card-text>
                                                        

                                                    </v-card> 
                                                        <v-btn rounded color="red" @click="removeItem(index)" dark><%= Resources.Resource.Delete %></v-btn>
                                                    </div>
                                                </v-slide-item>
                                            </draggable>
                                        </v-slide-group>  
                                        <v-dialog :retain-focus="false"    v-model="dialog" max-height="600" max-width="800" >
                                            <v-card class="mx-auto d-flex flex-column " height="600"  width="800" >                                                    
                                                    <v-card-title>
                                                        <v-text-field v-model="edited.Title"  single-line label="<%= Resources.Resource.Title %>"></v-text-field>
                                                    </v-card-title>
                                                    
                                                    <v-card-subtitle class="pb-0">
                                                          <%--<ckeditor :editor="editor" v-model="edited.Explanation" :config="editorConfig"></ckeditor>--%>
                                                        <vue-ckeditor type="classic" v-model="edited.Explanation" :config="editorConfig"></vue-ckeditor>
                                                    </v-card-subtitle>
                                                    <v-spacer></v-spacer>
                                                    <v-card-text >
                                                        <v-file-input dense accept="image/*"  single-line 
                                                            @change="fileUploaded" prepend-icon="icon-file-upload" v-model="edited.File" label="<%= Resources.Resource.ImportFile %>">
                                                        </v-file-input>
                                                    </v-card-text>

                                                 <v-divider class="pa-0 ma-0"></v-divider>
                                                 <v-card-actions >
                                                     <v-btn color="default" left="true"  @click="onCancelDialog"><%= Resources.Resource.Cancel %></v-btn>
                                                     <v-spacer></v-spacer>
                                                     <v-btn color="primary" depressed @click="onSaveDialog"><%= Resources.Resource.Save %></v-btn>
                                                     
                                                 </v-card-actions>
                                            </v-card>           

                                        </v-dialog>

                                       
                                    </div>
                                    <div class="col-sm-5 col-md-4 col-lg-3">
                                            <v-slide-group  class="pa-4">
                                                <div class="row">
                                                    <v-slide-item >
                                                        <div class="text-center">
                                                            <v-card :color="'grey lighten-5'" class="ma-4"  height="600" width="140" v-on:click="addItem">
                                                                <v-row class="fill-height" align="center" justify="center" >
                                                                    <v-scale-transition>
                                                                        <v-icon  size="64" v-text="'icon-plus-circle2'" ></v-icon>
                                                                    </v-scale-transition>
                                                                </v-row>
                                                            </v-card> 
                                                        </div>
                                                    </v-slide-item>
                                                </div>
                                            </v-slide-group> 
                                     </div>
                                </div>
                            </v-sheet>
                     <v-row class="p-20">
                            <div class="col-lg-6 pull-left text-left">
                                <v-btn text depressed class="btn btn-default" href="MobileTutorialsList.aspx">
                                <%= Resources.Resource.Cancel %>
                            </v-btn>
                            </div>
                            <div class="col-lg-6 pull-right text-right">
                                <v-btn depressed @click="saveWeb" color="primary" :disabled="!Screens || Screens.length<1 || !Name || MinVersion<0 ">
                                     <%= Resources.Resource.Save %>
                                </v-btn>
                              
                            </div>
                            
                        </v-row>
                </v-container> 
               
            </v-app>
        </div>
        <!-- /content -->
        <asp:TextBox runat="server" ID="tbxVueModel" CssClass="hidden"></asp:TextBox>
    </form> 
    <script type="text/javascript">
        var txtTotalForPeriod = "<%= Resources.Resource.TotalForPeriod %>";
        var tbxVueModel = <%= tbxVueModel.ClientID %>;
        var id = <%= CurentTutorialId %>;
        var type = 1;

    </script>
    <script type="text/javascript" src="TutorialDetails.js?<%= Guid.NewGuid().ToString("n") %>"></script>
    <%--<script type="text/javascript" src="TutorialDetails.js"></script>--%>
</asp:Content>
