﻿Vue.use(VueCkeditor.plugin, {
    editors: {
        'classic': CustomEditors.ClassicEditor
    }
})
var app = new Vue({
    el: '#app',
  
    vuetify: new Vuetify(),

    data: {
        model: null,
        dialog: false,
        edited: {},
        Id: 0,
        Name: '',
        MinVersion: '',
        DevicesDeployed: '',
        Screens: [],
        Type: 0,
        rules: {
            required: value => !!value || 'Required.',

            version: value => {
                var pattern = /^(\d+\.)?(\*|\d+)$/
                return pattern.test(value) || 'Invalid version.';
            }
        },
        editorConfig: {
            removePlugins: ['MediaEmbed'],
            mediaEmbed: {},
            placeholder: '',
            toolbar: {
                shouldNotGroupWhenFull: true,
                items: [
                    'heading',
                    '|',
                    'bold',
                    'italic',
                    '|',
                    'bulletedList',
                    'numberedList',
                    '|',
                    'insertTable',
                    '|',
                    'undo',
                    'redo'
                ]
            },
            table: {
                contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells']
            },
        }
        
    },
    computed: {
        itemsFiltered: {
            get() {
                return this.Screens;
            },
            set(data) {
                this.Screens = data;
            }
        },
        screensEncoded: function () {
            var encodedScreens = JSON.parse(JSON.stringify(this.Screens));
            return $.each(encodedScreens, function (i, v) {
                v.Explanation = encodeURIComponent(v.Explanation);
            });
        }

    },
    created() {
        this.getModel();
    },
    methods: {

        addItem: function (e) {
            this.Screens.push({ Id: 0, Title: '', Explanation: '', Image: '' });

            this.model = this.Screens[this.Screens.length - 1];

        },
        removeItem(index) {
            this.Screens.splice(index, 1);
        },
        
        fileUploaded: function (e) {
            if (!e) {

                this.edited.Image = '';
                this.edited.FileName = '';
                return;
            }
            var name = e.name;
            var reader = new FileReader();
            reader.readAsDataURL(e);

            reader.onload = () => {

                this.edited.Image = reader.result;
                this.edited.FileName = name;
            };
        },
        save: function (type) {
            var obj = {
                Id: this.$data.Id,
                Name: this.$data.Name,
                MinVersion: this.$data.MinVersion || 0,
                Screens: this.screensEncoded,
                Type: type
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '../API/Service.svc/SaveTutorial',
                data: JSON.stringify(obj),
                processData: false,
                dataType: "json",
                success: function (e) {
                    if (e.Type == 0)
                        window.location.href = "./MobileTutorialDetails.aspx?id=" + e.Id;
                    else
                        window.location.href = "./WebTutorialDetails.aspx?id=" + e.Id;
                }
            });
        },

        saveMob() {
            this.save(0);
        },

        saveWeb() {
            this.save(1);
        },

        onCardClick(el) {
            this.model = el;
            this.edited = Vue.util.extend({}, el);
            this.dialog = true;

        },
        onSaveDialog() {

            var exp = $('<div/>').html(this.edited.Explanation);
            $('table', exp).addClass('table table-bordered');
            $('ol, ul', exp).addClass('d-inline-block');
            
            this.edited.explanation = exp.html();
            this.model = Vue.util.extend(this.model, this.edited);
            this.edited = {};
            this.dialog = false;

        },
        onCancelDialog() {
            this.edited = {};
            this.dialog = false;
        },
        getComponentData() {
            return {
                on: {
                    /*change: this.handleChange,
                    input: this.inputChanged*/
                },
                attrs: {
                    class: 'row'
                },
                props: {
                    value: this.Name,
                }
            };
        },
        getModel: function () {
            var vm = this;
            $.ajax({
                url: '../API/Service.svc/GetTutorial',
                method: 'GET',
                data: {
                    id: id || 0,
                    type: type || 0
                },
                success: function (data) {
                    vm.Id = data.Id;
                    vm.Name = data.Name;
                    if (data.Screens)
                        vm.Screens = $.each(data.Screens, function (i, v) {
                            v.Explanation = decodeURIComponent(decodeURIComponent(decodeURIComponent(decodeURIComponent(v.Explanation))));
                        });

                    vm.MinVersion = data.MinVersion;
                    vm.DevicesDeployed = data.DevicesDeployed;
                    vm.Type = data.Type;
                    vm.status = data.Status;
                },
                error: function (result) {
                    console.log(result);
                }
            });
        }
       
    }
});
