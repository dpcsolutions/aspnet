﻿<%@ Page Title="TimeSheet" Language="C#" MasterPageFile="~/MasterPageNoSession.master" AutoEventWireup="true" Inherits="ClientDetails_Default" Codebehind="ClientDetails.aspx.cs" %>

<asp:content contentplaceholderid="head" runat="server">
 <link rel="stylesheet" href="<%= Page.ResolveClientUrl("~//assets/css/vuetify.min.css") %>" type="text/css" />
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.1.45/css/materialdesignicons.min.css" type="text/css" />
</asp:content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
   
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
    <form runat="server">
        <div class="content"  id="app" >
            <v-app id="inspire">
                 <v-container >
                     <v-subheader>
                         <a name="quotes">
                             <i class="icon-grid5 display-inline-text "></i>
                             <strong><%= Resources.Resource.Details %></strong>
                         </a>
                     </v-subheader>
                     
                    <v-sheet  class="" elevation="3" >
                        
                        <v-row class="ma-2 pa-2">
                            <v-col cols="12" sm="6" md="6">
                                <v-text-field v-model="name" label="Name" validate-on-blur="true"  :rules="[rules.required]"></v-text-field>
                            </v-col>

                            <v-col cols="12" sm="2" md="2">
                                <v-text-field type="number"  v-model="userLimit"  label="User Limit" validate-on-blur="true" :rules="[rules.required]"></v-text-field>
                            </v-col>
                            <v-col cols="12" sm="2" md="2">
                                <v-text-field type="number"  v-model="dailyMailsLimit"  label="Daily Mails Limit" validate-on-blur="true" :rules="[rules.required]"></v-text-field>
                            </v-col>
                            <v-col cols="12" sm="2" md="2">
                                <v-text-field v-model="creationDate" label="Creation Date" disabled ></v-text-field>
                            </v-col>
                        </v-row>
                        <v-row class="ma-2 pa-2">
                            <v-col cols="4" sm="4" md="4">
                                <v-menu
                                    ref="menu1"
                                  
                                    :close-on-content-click="false"
                                    transition="scale-transition"
                                    offset-y
                                    max-width="290px"
                                    min-width="auto"
                                >
                                    <template v-slot:activator="{ on, attrs }">
                                        <v-text-field
                                            v-model="trialEnd"
                                            label="License Deadline"
                                         
                                            persistent-hint
                                            prepend-icon="mdi-calendar"
                                            v-bind="attrs"
                                         
                                            v-on="on"
                                        ></v-text-field>
                                    </template>
                                    <v-date-picker
                                        v-model="trialEnd"
                                        no-title
                                        @input="menu1 = false"
                                    ></v-date-picker>
                                </v-menu>
                                
                            </v-col>
                            <v-col cols="4" sm="4" md="4">
                                
                                <v-menu
                                    ref="menu2"
                                  
                                    :close-on-content-click="false"
                                    transition="scale-transition"
                                    offset-y
                                    max-width="290px"
                                    min-width="auto"
                                >
                                    <template v-slot:activator="{ on, attrs }">
                                        <v-text-field
                                            v-model="subscriptionStart"
                                            label="Subscription Start"
                                         
                                            persistent-hint
                                            prepend-icon="mdi-calendar"
                                            v-bind="attrs"
                                         
                                            v-on="on"
                                        ></v-text-field>
                                    </template>
                                    <v-date-picker
                                        v-model="subscriptionStart"
                                        no-title
                                        @input="menu2 = false"
                                    ></v-date-picker>
                                </v-menu>
                            </v-col>
                            <v-col cols="4" sm="4" md="4">
                                
                                
                                <v-menu
                                    ref="menu3"
                                  
                                    :close-on-content-click="false"
                                    transition="scale-transition"
                                    offset-y
                                    max-width="290px"
                                    min-width="auto"
                                >
                                    <template v-slot:activator="{ on, attrs }">
                                        <v-text-field
                                            v-model="subscriptionEnd"
                                            label="Subscription End"
                                         
                                            persistent-hint
                                            prepend-icon="mdi-calendar"
                                            v-bind="attrs"
                                         
                                            v-on="on"
                                        ></v-text-field>
                                    </template>
                                    <v-date-picker
                                        v-model="subscriptionEnd"
                                        no-title
                                        @input="menu3 = false"
                                    ></v-date-picker>
                                </v-menu>
                            </v-col>
                        </v-row>
                        
                        <v-row class="ma-2 pa-2">
                            <v-col cols="4" sm="4" md="4">
                                <input type="checkbox" id="hide-gesmobile-label" v-model="hideGesmobileLabel" />
                                <label for="hide-gesmobile-label"><%= Resources.Resource.HideGesmobileLabel %></label>
                            </v-col>
                            <v-col cols="4" sm="4" md="4">
                            </v-col>
                            <v-col cols="4" sm="4" md="4">
                            </v-col>
                        </v-row>

                    </v-sheet>
                     <v-row class="p-20">
                            <div class="col-lg-4 pull-left text-left">
                                <v-btn text depressed class="btn btn-default" href="ClientsList.aspx">
                                <%= Resources.Resource.Cancel %>
                            </v-btn>
                            </div>
                            <div class="col-lg-4 pull-left text-left">
                                <v-btn depressed @click="save" color="primary" :disabled="!name || userLimit<0 || dailyMailsLimit<0">
                                     <%= Resources.Resource.Save %>
                                </v-btn>
                                
                            </div>
                         <div class="col-lg-4 pull-right text-right">
                             <v-btn depressed @click="disableOrg" color="red" :disabled="id<1" v-show="recordStatus==0">
                                 <%= Resources.Resource.Disable %>
                             </v-btn>
                             <v-btn depressed @click="enableOrg" color="green" :disabled="id<1" v-show="recordStatus!=0">
                                 <%= Resources.Resource.Active %>
                             </v-btn>
                         </div>
                         
                            
                        </v-row>
                </v-container> 
               
            </v-app>
        </div>
        <!-- /content -->
        <asp:TextBox runat="server" ID="tbxVueModel" CssClass="hidden"></asp:TextBox>
    </form> 
    <script type="text/javascript">
        var txtTotalForPeriod = "<%= Resources.Resource.TotalForPeriod %>";
        var tbxVueModel = <%= tbxVueModel.ClientID %>;
        var id = <%= CurentClientId %>;
        var type = 1;

    </script>
    <script type="text/javascript" src="ClientDetails.js?<%= Guid.NewGuid().ToString("n") %>"></script>
    <%--<script type="text/javascript" src="ClientDetails.js"></script>--%>
</asp:Content>
