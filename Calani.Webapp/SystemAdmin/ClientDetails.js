﻿var app = new Vue({
    el: '#app',
  
    vuetify: new Vuetify(),

    data: {
        
        id: 0,
        name: '',
        creationDate: '',
        userLimit: 0,
        dailyMailsLimit: 0,
        trialEnd: '',
        subscriptionStart: '',
        subscriptionEnd: '',
        recordStatus: 0,
        hideGesmobileLabel: false,
        rules: {
            required: value => !!value || 'Required.'

            }
        },
    
    created() {
        this.getModel();
    },
    methods: {

        save: function () {
            var obj = {
                id: this.$data.id,
                name: this.$data.name,
                userLimit: this.$data.userLimit || 0,
                dailyMailsLimit: this.$data.dailyMailsLimit || 0,
                subscriptionStart: this.$data.subscriptionStart,
                subscriptionEnd: this.$data.subscriptionEnd,
                trialEnd: this.$data.trialEnd,
                hideGesmobileLabel: this.$data.hideGesmobileLabel
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '../API/Service.svc/SaveOrganization',
                data: JSON.stringify(obj),
                processData: false,
                dataType: "json",
                success: function (e) {
                    window.location.href = "./ClientsList.aspx";
                },
                error: function (a, b, c) {

                    if (a && a.status == 401 || a && a.status == 400) {
                        window.location.href = '~/Login.aspx';
                    }
                }
            });
        },
        enableOrg: function() {
            var obj = {
                id: this.$data.id,
                name: this.$data.name,
                userLimit: this.$data.userLimit || 0,
                dailyMailsLimit: this.$data.dailyMailsLimit || 0,
                subscriptionStart: this.$data.subscriptionStart,
                subscriptionEnd: this.$data.subscriptionEnd,
                trialEnd: this.$data.trialEnd,
                hideGesmobileLabel: this.$data.hideGesmobileLabel,
                recordStatus: 0
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '../API/Service.svc/SaveOrganization',
                data: JSON.stringify(obj),
                processData: false,
                dataType: "json",
                success: function(e) {
                    window.location.href = "./ClientsList.aspx";
                },
                error: function(a, b, c) {

                    if (a && a.status == 401 || a && a.status == 400) {
                        window.location.href = '~/Login.aspx';
                    }
                }
            });
        },

        disableOrg: function () {
                var obj = {
                    id: this.$data.id,
                    name: this.$data.name,
                    userLimit: this.$data.userLimit || 0,
                    dailyMailsLimit: this.$data.dailyMailsLimit || 0,
                    subscriptionStart: this.$data.subscriptionStart,
                    /*subscriptionEnd: moment().format("YYYY-MM-DD"),
                    trialEnd: moment().format("YYYY-MM-DD"),*/
                    subscriptionEnd: this.$data.subscriptionEnd,
                    trialEnd: this.$data.trialEnd,
                    hideGesmobileLabel: this.$data.hideGesmobileLabel,
                    recordStatus: 1,
                }
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '../API/Service.svc/SaveOrganization',
                    data: JSON.stringify(obj),
                    processData: false,
                    dataType: "json",
                    success: function (e) {
                        window.location.href = "./ClientsList.aspx";
                    },
                    error: function (a, b, c) {

                        if (a && a.status == 401 || a && a.status == 400) {
                            window.location.href = '~/Login.aspx';
                        }
                    }
                });
        },

        getModel: function () {
            var vm = this;
            $.ajax({
                url: '../API/Service.svc/GetOrganization',
                method: 'GET',
                data: {
                    id: id || 0,
                    type: type || 0
                },
                success: function (data) {
                    vm.id = data.id;
                    vm.name = data.name;
                    vm.userLimit = data.userLimit;
                    vm.dailyMailsLimit = data.dailyMailsLimit;
                    vm.creationDate = data.creationDate;
                    vm.trialEnd = data.trialEnd;
                    vm.subscriptionStart = data.subscriptionStart;
                    vm.subscriptionEnd = data.subscriptionEnd;
                    vm.recordStatus = data.recordStatus;
                    vm.hideGesmobileLabel = data.hideGesmobileLabel;
                },
                error: function (a, b, c) {

                    if (a && a.status == 401 || a && a.status == 400) {
                        window.location.href = '~/Login.aspx';
                    }
                }
            });
        }
       
    }
});
