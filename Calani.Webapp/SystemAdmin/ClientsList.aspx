﻿<%@ Page Title="TimeSheet" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Clients_List_Default" Codebehind="ClientsList.aspx.cs" %>

<asp:content contentplaceholderid="head" runat="server">

 <link rel="stylesheet" href="<%= Page.ResolveClientUrl("~//assets/css/vuetify.min.css") %>" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.1.45/css/materialdesignicons.min.css" type="text/css" />

</asp:content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
   
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        var txtTotalForPeriod = "<%= Resources.Resource.TotalForPeriod %>";
      
    </script>
    <form runat="server">

        <div class="content"  id="app">
            <v-app>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <a name="quotes">
                                        <i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
                                        <strong> CRM Report</strong>
                                    </a>
                                </h6>
                            </div>
                            
                            <v-sheet  class="" elevation="3" >
                        
                            <v-row class="ma-2 pa-2">
                                <v-col cols="12" sm="12" md="12" class="justify-center" style="text-align:center;">
                                    <v-btn href="../Scripts/CrmReport.ashx" big color="info"  class="ma-2"
                                           >
                                        CRM Report
                                    </v-btn>

                                    </v-col>

                            
                            </v-row>

                            </v-sheet>
                            
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">
                                    <a name="quotes">
                                        <i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
                                        <strong>Organizations</strong>
                                    </a>
                                </h6>
                            </div>
                    <!-- panel-heading -->
                    
                  
                    <div class="panel-body">
                        
                         <v-card elevation="3">
                              <v-card-title>
                                  <v-text-field v-model="search" append-icon="mdi-magnify" label="Search" single-line outlined class="pa-1">
                                  </v-text-field>
                                  &nbsp;
                                  <v-select :items="calendarFilterItems"  v-model="selectedCalendarFilter" v-on:change="onCalendarFilterChanged" label= '<%= Resources.Resource.Select_Period %>' outlined class="pa-1"></v-select>
                                  
                              </v-card-title>

                             <v-data-table :headers="headers" :items="items" item-key="id"   :footer-props="footerProps" :sort-by.sync="sortBy"
                                           :sort-desc.sync="sortDesc"
                                 :options.sync="options" :server-items-length="totalItems" :loading="loading"   :search="search" class="elevation-1" >
                                 
                                 
                                 <template v-slot:item.use="{ item }">
                                     <v-btn class="ma-2" outlined color="indigo" small @click="useOrg(item.id)" >[Use]</v-btn>
                                 </template>

                                 <template v-slot:item.id="{ item }">
                                     <v-chip :color="getColor(item.subscriptionEnd, item.recordStatus)">
                                         {{ item.id }}
                                     </v-chip>
                                 </template>
                                 
                                 <template v-slot:item.actions="{ item }" @click="editOrg(item.id)">
                                     
                                     <v-btn class="ma-2" outlined small fab color="indigo" @click="editOrg(item.id)">
                                         <v-icon>mdi-pencil</v-icon>
                                     </v-btn>
                                     
                                 </template>
                             </v-data-table>
                         </v-card>
                    </div>

                    <!-- panel-body -->

                    <div class="panel-footer text-right no-padding">
                        <div class="row  pull-right m-10" >
                            <a href="MobileTutorialDetails.aspx" class="btn btn-default "><i class="icon-plus2 position-left"></i>
                                <%= Resources.Resource.Add_new %>
                            </a>
                        </div>
                    </div>
                    <!-- panel-footer -->

                </div>
            </div>
            </div>

            </v-app>
        </div>
        <!-- /content -->
        
    </form>
    <script type="text/javascript">
        var txtTotalForPeriod = "<%= Resources.Resource.TotalForPeriod %>";
        var headers = [
            { text: "Id", value: "id" },
            { text: "<%= Resources.Resource.Description %>", value: "name" },
            { text: "<%= Resources.Resource.Screens %>", value: "screensCount" },
            { text: "<%= Resources.Resource.DevicesDeployed %>", value: "devicesDeployed" },
            { text: "<%= Resources.Resource.MinVersion %>", value: "minVersion" },


        ]
    </script>
    <%--<script src="ClientsList.js" type="text/javascript" ></script>--%>
    <script type="text/javascript" src="ClientsList.js?<%= Guid.NewGuid().ToString("n") %>"></script>
</asp:Content>
