﻿
new Vue({
    
    el: '#app',
    vuetify: new Vuetify(),    
    data: {
        totalItems: 0,
        items: [],
        calendarFilterItems: [],
        selectedCalendarFilter:0,
        search: "",
        loading: true,
        options: {},
        sortBy: 'subscriptionEnd',
        sortDesc: true,
        footerProps: { 'items-per-page-options': [30, 50, 100] },
        headers: [
            { text: 'Use', value: 'use', sortable: false },
            { text: 'Id', align: 'start', value: 'id', },
            { text: 'Name', value: 'name', },
            { text: 'User Limit', value: 'userLimit', },
            { text: 'Daily Mails Limit', value: 'dailyMailsLimit', },
            { text: 'Creation Date', value: 'creationDate' },
            //{ text: 'activationDate', value: 'activationDate' },
            { text: 'Subscr Start', value: 'subscriptionStart' },
            //{ text: 'email', value: 'email', sortable: false },
            { text: 'Subscr End', value: 'subscriptionEnd',  },
            { text: 'Subscr Status', value: 'subscriptionStatus' },
            //{ text: 'trialEnd', value: 'trialEnd' },
           // { text: 'phone', value: 'phone', sortable: false },
            { text: 'Admin Name', value: 'adminName', sortable: false },
            { text: 'Admin Email', value: 'adminPrimaryEmail', sortable: false },
            // { text: 'adminSecondaryEmail', value: 'adminSecondaryEmail', sortable: false},
            { text: 'Admin Phone', value: 'adminPrimaryPhone', sortable: false },
            // { text: 'admiSecondaryPhone', value: 'admiSecondaryPhone', sortable: false},
            { text: 'Actions', value: 'actions', sortable: false },

        ],
    },
    watch: {
        options: {
            handler() {
                this.getData();
                   
            },
            deep: true,
        },
        search() {
            this.getData();
        }
    },
    mounted() {
        /*  this.getData()*/
        $(".theme--light").removeClass("theme--light");

    },
    created() {
        this.getCalendarFilterItems();
    },
    methods: {
        getData() {
            this.loading = true;

            var vm = this;
            $.ajax({
                url: '../API/Service.svc/GetOrganizationsList',
                method: 'GET',
                data: {
                    groupBy: vm.options.groupBy[0]||'',
                    groupDesc: vm.options.groupDesc[0] || false,
                    itemsPerPage: vm.options.itemsPerPage,
                    multiSort: vm.options.multiSort,
                    mustSort: vm.options.mustSort,
                    page: vm.options.page,
                    sortBy: vm.options.sortBy[0] || '',
                    sortDesc: vm.options.sortDesc[0] || false,
                    search: vm.search || '',
                    type: vm.selectedCalendarFilter
                },
                success: function (data) {
                    vm.items = data.items;
                    vm.totalItems = data.total;
                    vm.loading = false;
                },
                error: function (a, b, c) {
                    
                    if (a && a.status == 401 || a && a.status == 400) {
                        window.location.href = '~/Login.aspx';
                    }
                }
            });
        },
        getCalendarFilterItems() {

            var vm = this;
            $.ajax({
                url: '../API/Service.svc/GetCalendarFilterItems',
                method: 'GET',
                /*data: {
                    selected: 5
                },*/
                success: function (data) {
                    vm.calendarFilterItems = data;
                },
                error: function (result) {
                    console.log(result);
                }
            });
        },

        getColor(date, recordStatus) {

            if (recordStatus != 0)
                return 'brown';

            if (!date)
                return 'grey';

            var m = moment(date);


            var diff = m.diff(new Date(), 'days');



            if (diff<1)
                return 'red';

            else if (diff >= 60)
                return 'green';

            else if (diff >= 30)
                return 'yellow';

            else if (diff >= 0)
                return 'orange';
            
            else
                return 'yellow';
        },

        onCalendarFilterChanged() {
            this.getData();
        },
        editOrg(id) {
            window.location.href = "./ClientDetails.aspx?id=" + id;
        },
        useOrg(id) {

            __doPostBack('', id);
        },
    },


});