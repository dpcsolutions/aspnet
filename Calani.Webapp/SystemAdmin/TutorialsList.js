﻿
new Vue({
    
    el: '#app',
    vuetify: new Vuetify(),    
    data: {
        search: '',
        headers: headers,
        tutorials: tutorials,

    },
    methods: {
        rowClickM: function (e) {
            window.location.href = "./MobileTutorialDetails.aspx?id=" + e.id;
        },
        rowClickW: function (e) {
            window.location.href = "./WebTutorialDetails.aspx?id=" + e.id;
        },
    }
});