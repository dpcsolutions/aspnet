﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using Calani.BusinessObjects.Admin;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.PlaformAdmin;

public partial class Clients_List_Default : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        ClientScript.GetPostBackEventReference(this, "");

        var orgId = Request.Form["__EVENTARGUMENT"];


        if (!string.IsNullOrWhiteSpace(orgId))
        {
            UseOrg(orgId);
        }
    }

    private void UseOrg(string orgId)
    {




        Calani.BusinessObjects.Membership.LoginManager mgr = new Calani.BusinessObjects.Membership.LoginManager();

        var email = Session["primaryEmail"].ToString(); 

        var usr = mgr.GetUser(email);

        PageHelper p = new PageHelper(this);
        PageHelper.InitUserSessions(usr.primaryEmail, Session, true);
        Session["UserOrganizationId"] = orgId.ToString();
        Session["UserType"] = Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin;
        Session["UserType2"] = Calani.BusinessObjects.Contacts.ContactTypeEnum.PlatformAdmin_Support;
        Session["UserName"] = "[STAFF] " + Session["UserName"];
        FormsAuthentication.RedirectFromLoginPage(usr.primaryEmail, false);
    }
}