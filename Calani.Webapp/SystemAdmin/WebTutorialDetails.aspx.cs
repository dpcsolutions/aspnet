﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Newtonsoft.Json;
using Calani.BusinessObjects.Admin;
using Newtonsoft.Json.Serialization;
using Calani.BusinessObjects.Model;

public partial class Web_Tutorial_Details_Default : System.Web.UI.Page
{
    public PageHelper ph;
    public long CurentTutorialId { get; set; }

    private static TutorialsManager mngr;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    public string OptionsPeriod { get; set; }
    public double TotalHours { get; set; }
protected void Page_Load(object sender, EventArgs e)
    {
        long id = -1;
        long.TryParse(Request.Params["id"], out id);
        if (id == -1)
        {
            Response.Redirect(".");
            return;
        }

        CurentTutorialId = id;
    }

}