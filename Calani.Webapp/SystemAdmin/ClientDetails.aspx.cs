﻿using Calani.BusinessObjects.Admin;
using System;

public partial class ClientDetails_Default : System.Web.UI.Page
{
    public PageHelper ph;

    public long CurentClientId { get; set; }

    private static TutorialsManager mngr;

    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    public string OptionsPeriod { get; set; }

    public double TotalHours { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        long id = -1;
        long.TryParse(Request.Params["id"], out id);
        if (id == -1)
        {
            Response.Redirect(".");
            return;
        }

        CurentClientId = id;
    }

}