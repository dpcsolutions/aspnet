﻿<%@ Page Title="TimeSheet" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Mobile_Tutorials_List_Default" Codebehind="MobileTutorialsList.aspx.cs" %>

<asp:content contentplaceholderid="head" runat="server">

 <link rel="stylesheet" href="<%= Page.ResolveClientUrl("~//assets/css/vuetify.min.css") %>" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.1.45/css/materialdesignicons.min.css" type="text/css" />

</asp:content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
   
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        var txtTotalForPeriod = "<%= Resources.Resource.TotalForPeriod %>";
      
    </script>
    <form runat="server">

        <div class="content"  id="app">
            <v-app id="inspire">

            <div class="row">
                <div class="col-sm-12 col-md-12">
                <div class="panel panel-white">

                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <a name="quotes">
                                <i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
                                <strong><%= ph.CMP.Name %></strong>
                            </a>
                        </h6>
                    </div>
                    <!-- panel-heading -->

                    <div class="panel-body">
                        
                         <v-card elevation="3">
                             <v-card-title>
                                 <v-spacer></v-spacer>
                                 <v-text-field v-model="search" append-icon="mdi-magnify" label="Search"  single-line  hide-details>
                                 </v-text-field>
                             </v-card-title>
                             
                              <v-data-table :headers="headers" :items="tutorials" :search="search"  @click:row="rowClickM" class="table">
                             </v-data-table>
                            
                         </v-card>
                    </div>
                    <!-- panel-body -->

                    <div class="panel-footer text-right no-padding">
                        <div class="row  pull-right m-10" >
                            <a href="MobileTutorialDetails.aspx" class="btn btn-default "><i class="icon-plus2 position-left"></i>
                                <%= Resources.Resource.Add_new %>
                            </a>
                        </div>
                    </div>
                    <!-- panel-footer -->

                </div>
            </div>
            </div>

            </v-app>
        </div>
        <!-- /content -->
        
    </form>
    <script type="text/javascript">
        var txtTotalForPeriod = "<%= Resources.Resource.TotalForPeriod %>";
        var headers = [
            { text: "Id", value: "id" },
            { text: "<%= Resources.Resource.Description %>", value: "name" },
            { text: "<%= Resources.Resource.Screens %>", value: "screensCount" },
            { text: "<%= Resources.Resource.DevicesDeployed %>", value: "devicesDeployed" },
            { text: "<%= Resources.Resource.MinVersion %>", value: "minVersion" },


        ]
        var tutorials = <%= MobileTutorialsList %>;
    </script>
   <%-- <script type="module" src="TutorialsList.js"></script>--%>
    <script type="module" src="TutorialsList.js?<%= Guid.NewGuid().ToString("n") %>"></script>
</asp:Content>
