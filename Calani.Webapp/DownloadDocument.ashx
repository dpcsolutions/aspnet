﻿<%@ WebHandler Language="C#" Class="DownloadDocument" %>

using System;
using System.Web;

public class DownloadDocument : IHttpHandler, System.Web.SessionState.IReadOnlySessionState
{

    public void ProcessRequest(HttpContext context)
    {

        long id = -1;
        long orgId = -1;
        long rid = -1;

        long.TryParse(context.Request.Params["id"], out id);
        long.TryParse(context.Request.Params["orgId"], out orgId);
        long.TryParse(context.Request.Params["rid"], out rid);
        string token = context.Request.Params["t"];

        bool dl = true;
        if (context.Request.Params["a"] != null && context.Request.Params["a"] == "0")
            dl = false;

        if (id > 0 )
        {

            Calani.BusinessObjects.Attachments.AttachmentsManager mgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
            if (orgId > 0)
            {
                mgr.OrganizationId = orgId;//PageHelper.GetCurrentOrganizationId(context.Session);    
            }
            else
                mgr.OrganizationId = PageHelper.GetCurrentOrganizationId(context.Session);

            var f = mgr.Get(id);

            if(f == null)
            {
                context.Response.StatusCode = 404;
                return;
            }

            context.Response.ContentType = f.mimeType;
            if (dl)
            {
                context.Response.AddHeader("content-disposition", "Attachment; filename=" + f.NameWithExtension);
            }
            context.Response.WriteFile(f.FilePath);
        }
        else if (rid > 0)
        {

            if (orgId <1)
                orgId = PageHelper.GetCurrentOrganizationId(context.Session);

            var mgr = new Calani.BusinessObjects.Projects.WorkReportsManager(orgId);

            var filePath = mgr.GetReportPdfPath(rid);

            context.Response.ContentType = "application/pdf";
            context.Response.AddHeader("content-disposition", "Attachment; filename=report_" + rid+".pdf");
            context.Response.WriteFile(filePath);
        }
        else
        {

            context.Response.ContentType = "text/plain";
            context.Response.Write("Hello World");
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}