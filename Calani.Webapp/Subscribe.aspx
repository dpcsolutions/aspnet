<%@ Page Language="C#" AutoEventWireup="true" Inherits="Subscribe" Codebehind="Subscribe.aspx.cs" %>

<%@ Register Src="~/assets/Controls/PasswordAdvisor.ascx" TagPrefix="uc1" TagName="PasswordAdvisor" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>GES</title>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/ui/moment/moment.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/pickers/daterangepicker.js"></script>
	<script type="text/javascript" src="assets/js/plugins/pickers/anytime.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/pickers/pickadate/picker.js"></script>
	<script type="text/javascript" src="assets/js/plugins/pickers/pickadate/picker.date.js"></script>
	<script type="text/javascript" src="assets/js/plugins/pickers/pickadate/picker.time.js"></script>
	<script type="text/javascript" src="assets/js/plugins/pickers/pickadate/legacy.js"></script>
    <script type="text/javascript">
        $(function () {

            $('.daterange-single').pickadate({ 
                singleDatePicker: true,
                selectYears: 120,
                selectMonths: true,
                format: 'dd/mm/yyyy',
                max: true
            });
            
        });
    </script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<script type="text/javascript" src="assets/js/pages/login.js"></script>
	<!-- /theme JS files -->

    <style type="text/css">
        .bg-gesmobile
        {
            background: #934553;
            color:white;
        }

        .color-gesmobile
        {
            color: #934553;
        }

        .border-gesmobile
        {
            border-color: #934553;
        }

        body{
            background: url('assets/images/splash.jpg');
            width: 100%;
            height: 90vh;
            background-repeat: no-repeat;
            background-position: center center;
            background-size: cover;
            background-attachment: fixed;
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121845992-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-121845992-1');
    </script>
    


</head>

<body class="login-container">

        <ul class="nav navbar-nav navbar-right">
			<li class="dropdown language-switch">
				<%= LanguageSwitch %></li>
        </ul>

    <form runat="server">
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

                    <!-- Registration form -->
					<asp:Panel runat="server" ID="PanelRegistration">
						<div class="row">
							<div class="col-lg-6 col-lg-offset-3">
								<div class="panel registration-form">
									<div class="panel-body">
										<div class="text-center">
											<!--div class="icon-object border-success text-success"><i class="icon-plus3"></i></!--div-->
                                            <div><img src="assets/images/logo.png" style="width:140px;height:140px;" /></div>
											<h5 class="content-group-lg"><%= Resources.Resource.CreateAccount %> <small class="display-block"><%= Resources.Resource.AllFieldsAreRequired %></small></h5>
										</div>

                                        <div class="alert alert-danger alert-bordered" id="lblError" runat="server" visible="false">
								            <button type="button" class="close" data-dismiss="alert"><span>�</span><span class="sr-only"><%= Resources.Resource.Close %></span></button>
								            <span class="text-semibold"><asp:Label id="lblErrorText" runat="server" /></span>
							            </div>

										
                                        <div class="row">
											<div class="col-md-6">
												<div class="form-group has-feedback">
                                                    <label class="control-label text-muted"><i class="icon-circle-small"></i> <%= Resources.Resource.YourCompanyOrActivityName %></label>
                                                    <input type="text" class="form-control" runat="server" id="tbxCompanyName" placeholder='<%$ Resources:Resource, YourCompanyOrActivityName_tooltip %>'/>
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group has-feedback">

                                                        <label class="control-label text-muted"><i class="icon-circle-small"></i> <%= Resources.Resource.Phone %></label>
                                                        <input type="text" class="form-control" runat="server" id="tbxPhone" placeholder='<%$ Resources:Resource, WizardSample_Phone %>'/>
												</div>
											</div>
										</div>
                                        

                                        

                                        <div class="row">
											<div class="col-md-6">
												<div class="form-group has-feedback">
                                                    <label class="control-label text-muted"><i class="icon-circle-small"></i> <%= Resources.Resource.YourEmail %></label>
                                                    <input type="email" class="form-control" runat="server" id="tbxEmail" placeholder='<%$ Resources:Resource, YourEmail_tooltip %>'/>
													<div class="form-control-feedback">
														<i class="icon-mention text-muted"></i>
													</div>
												</div>
											</div>

											<div class="col-md-6">
												<div class="form-group has-feedback" style="margin-bottom:0">
                                                    <label class="control-label text-muted">
                                                         <i class="icon-circle-small"></i> <%= Resources.Resource.CreatePassword %> 
                                                    </label>

                                                    <div class="input-group">
                                                        <input type="password" class="form-control" runat="server" id="tbxPassword" placeholder='<%$ Resources:Resource, CreatePassword_tooltip %>'/>
                                                        <span class="input-group-btn">
													        <button class="btn btn-default" type="button" onmousedown="showpass(true)" onmouseup="showpass(false)">
                                                                <i class="icon-eye"></i>
                                                            </button>
												        </span>
                                                    </div>


													
												</div>
                                                <uc1:PasswordAdvisor runat="server" ID="PasswordAdvisor" PasswordInput="tbxPassword" />
											</div>
										</div>
                                        <script type="text/javascript">
                                            function showpass(show)
                                            {
                                                //var t = $('#tbxPassword').attr('type');
                                                if(show)
                                                {
                                                    $('#tbxPassword').attr('type', 'text');
                                                }
                                                else
                                                {
                                                    $('#tbxPassword').attr('type', 'password');
                                                }
                                            }
                                        </script>

										


                                       
                                        <div style="margin-top:30px">
                                           
                                            <div id="more">
										        <div class="row">
											        <div class="col-md-6">
												        <div class="form-group has-feedback">
                                                            <label class="control-label text-muted"><i class="icon-circle-small"></i> <%= Resources.Resource.Firstname %></label>
                                                            <input type="text" class="form-control" runat="server" id="tbxFirstname" placeholder='<%$ Resources:Resource, Firstname_tooltip %>'/>
													        <div class="form-control-feedback">
														        <i class="icon-user-check text-muted"></i>
													        </div>
												        </div>
											        </div>

											        <div class="col-md-6">
												        <div class="form-group has-feedback">
                                                            <label class="control-label text-muted"><i class="icon-circle-small"></i> <%= Resources.Resource.Lastname %></label>
                                                            <input type="text" class="form-control" runat="server" id="tbxLastname" placeholder='<%$ Resources:Resource, Lastname_tooltip %>'/>
													        <div class="form-control-feedback">
														        <i class="icon-user-check text-muted"></i>
													        </div>
												        </div>
											        </div>
										        </div>
                                                <div class="row">
											        <div class="col-md-6">
												        <div class="form-group has-feedback">
                                                            <label class="control-label text-muted"><i class="icon-circle-small"></i> <%= Resources.Resource.BirthDate %></label>
                                                            <input type="text" class="form-control daterange-single" runat="server" id="tbxBithdate" />
													        <div class="form-control-feedback">
														        <i class="icon-calendar text-muted"></i>
													        </div>
												        </div>
											        </div>
                                                    <div class="col-md-6">
												        <div class="form-group has-feedback">
                                                            <label class="control-label text-muted"><i class="icon-circle-small"></i> Comment avez-vous connu GesMobile?</label>
                                                            <asp:DropDownList runat="server" ID="ddlKnowUs" CssClass="form-control select">
                                                                <asp:ListItem Value="" Text="-" />
                                                                <asp:ListItem Value="Recherche" Text="Moteur de recherche (Google, Bing...)" />
                                                                <asp:ListItem Value="LinkedIn" Text="LinkedIn" />
                                                                <asp:ListItem Value="Facebook" Text="Facebook" />
                                                                <asp:ListItem Value="Instagram" Text="Instagram" />
                                                                <asp:ListItem Value="Youtube" Text="Youtube" />
                                                                <asp:ListItem Value="Presse" Text="Presse" />
                                                                <asp:ListItem Value="Bouche à oreille" Text="Bouche à oreille" />
                                                                <asp:ListItem Value="Autre" Text="Autre" />
                                                            </asp:DropDownList>
												        </div>
											        </div>
                                               </div>
                                            </div>
                                           
                                        </div>

										

										<div class="form-group" style="margin-top:30px">
											

											<div class="checkbox">
												<label>
													<asp:CheckBox runat="server" ID="cbxAcceptTerms" type="checkbox" class="styled" />
													<%= Resources.Resource.ConfirmingRead %> <a href="TermsAndConditions.aspx" target="_blank" class="color-gesmobile"><%= Resources.Resource.TermsAndConditions %></a>
												</label>
											</div>
										</div>

										<div class="text-right">
											<a href="Login.aspx" class="btn btn-link"><i class="icon-arrow-left13 position-left"></i> <%= Resources.Resource.BackToLogin %></a>
											<asp:Button runat="server" ID="btnCreateAccount" type="submit" CssClass="btn bg-gesmobile  ml-10"
                                                Text='<%$ Resources:Resource, CreateAccount %>' OnClick="btnCreateAccount_Click" OnClientClick="oncreateaccount()"/>
                                            
										</div>
									</div>
								</div>
							</div>
						</div>
                     
					</asp:Panel>
					<!-- /registration form -->
                    <!-- Footer -->
                    <div class="row  text-muted text-center">
                        &copy; <asp:Label runat="server" ID="LabelCopyYear" />. 
                        <asp:HyperLink runat="server" ID="HyperLinkCopy" />
                    </div>
                    <!-- /footer -->
				</div>
				<!-- /content area -->
               
			</div>
			<!-- /main content -->
            
          
		</div>
		<!-- /page content -->
        

	</div>
	<!-- /page container -->
        
       
    </form>



</body>
</html>