﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading;
using System.Globalization;

public partial class Developpers_Globalization_Default : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        
        lblCurrentLanguage.Text = Thread.CurrentThread.CurrentCulture.ToString();
        lblRh.Text = Resources.Resource.Test;
    }

   
}