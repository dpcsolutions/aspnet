﻿<%@ Page Title="Resources for Globalization" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Developpers_Globalization_Default"  Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <form runat="server">
    
        <h3>Current language</h3>

        <div>

            <strong>Thread culture:</strong>
            <code><asp:Label runat="server" ID="lblCurrentLanguage" /></code>

        </div>

        <div>
            <a href="?setlang=en-US">en-US</a> |
            <a href="?setlang=fr-FR">fr-FR</a> |
            <a href="?setlang=fr-CH">fr-CH</a>
        </div>


        <h3>Resource binding</h3>

        <div>

            <strong>runat=server control:</strong>
            <code><asp:Label runat="server" ID="tbxTest" Text='<%$ Resources:Resource, Test %>' /> </code>

        </div>

        <div>

            <strong>html injection</strong>
            <code><%= Resources.Resource.Test %></code>

        </div>

        <h3>Resource helper</h3>

        <div>

            <strong>Resources.Resource.Test:</strong>
            <code><asp:Label runat="server" ID="lblRh" /></code>

        </div>

    </form>

</asp:Content>

