﻿<%@ Page Title="Reset Database" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Developpers_ResetDb_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <form runat="server">

        <div class="content">

		    <div class="row">

                <!-- left column -->
			    <div class="col-sm-12 col-md-6">

					    
					    <asp:Panel runat="server" ID="panelEdit" class="panel panel-flat">
						    <div class="panel-heading">
							    <h5 class="panel-title">Reset database structure and data</h5>
						    </div>

						    <div class="panel-body">

							    
											
                                    <a href="#" data-toggle="modal" data-target="#modal_default" class="btn btn-primary">
                                       <i class="icon-floppy-disk position-left"></i> Go
                                    </a>


						    </div>
					    </asp:Panel><!-- /panelEdit -->
					    


                        <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title">Validation Error</h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblErrorMessage" />
						    </div>
					    </asp:Panel><!-- panelError -->

                        <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title">Done</h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblSuccess" />
						    </div>
					    </asp:Panel><!-- panelSuccess -->


			    </div>
			    <!-- /left column -->
                

                <!-- right column -->
                <div class="col-sm-12 col-md-6">

			    </div>
                <!-- /right column -->
						

		    </div> <!-- /row -->


					
            <!-- basic modal: confirm delete -->
		    <div id="modal_default" class="modal fade">
			    <div class="modal-dialog">
				    <div class="modal-content">
					    <div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal">&times;</button>
						    <h5 class="modal-title">Confirmation</h5>
					    </div>

					    <div class="modal-body">
						    <h6 class="text-semibold">Launch the reset ?</h6>
						    <p>Warning: The database will be <strong>DROPPED</strong> and <strong>RECREATED</strong>. All data of every customers will be <strong>lost</strong>. Sure ?</p>

					    </div>

					    <div class="modal-footer">
						    <button type="button" class="btn btn-link" data-dismiss="modal">No. Cancel.</button>
                            <asp:Button runat="server" ID="btnGo" CssClass="btn btn-primary" Text="Yes! I like to live dangerously :)" OnClick="btnGo_Click" />

					    </div>
				    </div>
			    </div>
		    </div>
		    <!-- /basic modal -->



        </div><!-- /content -->

    </form>

</asp:Content>

