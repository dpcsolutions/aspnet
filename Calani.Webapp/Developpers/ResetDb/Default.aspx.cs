﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.Generic;

public partial class Developpers_ResetDb_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(System.Configuration.ConfigurationManager.AppSettings["developperAccess"].ToLower() != "true")
        {
            Response.Redirect("~/");
        }
    }

    protected void btnGo_Click(object sender, EventArgs e)
    {

        SqlFileLauncher schema = new SqlFileLauncher();

        try
        {
            schema.SetQuery("DROP DATABASE `calani`;");
            schema.Launch();
        }
        catch { }

        try
        {
            schema.SetQuery("CREATE DATABASE `calani`;");
            schema.Launch();
        }
        catch { }

        schema.LoadFile(Server.MapPath("~/App_Data/dbschema/schema.sql"));
        schema.Launch();

        schema.LoadFile(Server.MapPath("~/App_Data/dbschema/demodata.sql"));
        schema.Launch();

        var di = new System.IO.DirectoryInfo(Calani.BusinessObjects.Attachments.AttachmentsManager.DIR);
        if (!di.Exists) di.Create();
        
        foreach (var subdi in di.GetDirectories())
        {
            try
            {
                subdi.Delete(true);
            }
            catch { }
        }

        DisplaySuccess("Schema restored");

    }

    private void DisplaySuccess(string message)
    {
        panelSuccess.Visible = true;
        lblSuccess.Text = message;
    }
    private void DisplayError(string error)
    {
        panelError.Visible = true;
        lblErrorMessage.Text = error;
    }
}