﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Developpers_Tests_Default" Codebehind="Default.aspx.cs" %>
<%@ Register Src="~/assets/Controls/FirstLoginWizard.ascx" TagPrefix="uc1" TagName="FirstLoginWizard" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
        <uc1:FirstLoginWizard runat="server" ID="FirstLoginWizard" />

        <asp:TextBox runat="server" ID="tbxTest" />
        <asp:Button runat="server" ID="btnGo" OnClick="btnGo_Click"  />
    </form>
</asp:Content>

