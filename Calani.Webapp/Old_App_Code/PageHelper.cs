﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;
using System.Threading;
using System.Globalization;
using System.Web.Security;
using System.Configuration;
using Calani.BusinessObjects;
using Calani.BusinessObjects.DocGenerator;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;

/// <summary>
/// Description résumée de PageHelper
/// </summary>
public class PageHelper
{
    public CurrentMenuProperty CMP { get; private set; }

    System.Web.UI.Page _page;
    public PageHelper(System.Web.UI.Page page)
    {
        _page = page;

        // restore session from cookie
        if (_page.Session.Count == 0)
        {
            if (!String.IsNullOrWhiteSpace(HttpContext.Current.User.Identity.Name))
            {
                InitUserSessions(HttpContext.Current.User.Identity.Name, _page.Session);
            }
        }
        // -->

        
        DefineLanguage();
        

        HtmlMenuGenerator htmlMenu = new HtmlMenuGenerator();
        htmlMenu.Page = _page;
        CMP = htmlMenu.GetCurrentMenuProperty();
        if (CMP != null)
        {
            page.Title = CMP.Name;

            if (!String.IsNullOrWhiteSpace(CMP.Roles))
            {
                string usertype = CurrentUserType.ToString().Split('_').LastOrDefault().ToLower();
                List<string> allowedRoles = CMP.Roles.ToLower().Split(';').ToList();

                if (!allowedRoles.Contains(usertype))
                {
                    page.Response.Redirect("~/");
                }
            }
        }


        CheckTrialEnds();
    }

    private void CheckTrialEnds()
    {
        if(IsTrialOrSubscriptionEnded())
        {
            if (!_page.AppRelativeVirtualPath.Contains("/Account/") 
                && !_page.AppRelativeVirtualPath.Contains("/Ressources/Employees/") 
                && !_page.AppRelativeVirtualPath.Contains("/Settings/"))
            {
                if (!GetUserStaff(_page.Session))
                {
                    _page.Response.Redirect("~/Account/");
                }
            }
        }
    }

    public bool IsTrialOrSubscriptionEnded()
    {

        Calani.BusinessObjects.Model.CalaniEntities db = new Calani.BusinessObjects.Model.CalaniEntities();
        var org = (from r in db.organizations where r.id == CurrentOrganizationId select r).FirstOrDefault();
        if (org != null)
        {
            if (org.subscriptionId != null)
            {
                if (org.subscriptionEnd != null)
                {
                    return (org.subscriptionEnd < DateTime.Now);
                }
            } else {
                if (org.trialEnd != null)
                {
                    return (org.trialEnd < DateTime.Now);
                }
            }
        }
        return false;

    }

    public bool IsTrialEnded()
    {

        Calani.BusinessObjects.Model.CalaniEntities db = new Calani.BusinessObjects.Model.CalaniEntities();
        var org = (from r in db.organizations where r.id == CurrentOrganizationId select r).FirstOrDefault();
        if (org != null)
        {
            if (org.trialEnd != null)
            {
                return (org.trialEnd < DateTime.Now);
            }
        }
        return false;

    }

    public bool IsSubscriptionEnded()
    {

        Calani.BusinessObjects.Model.CalaniEntities db = new Calani.BusinessObjects.Model.CalaniEntities();
        var org = (from r in db.organizations where r.id == CurrentOrganizationId select r).FirstOrDefault();
        if (org.subscriptionId != null)
        {
            if (org.subscriptionEnd != null)
            {
                return (org.subscriptionEnd < DateTime.Now);
            }
        }
        return false;

    }


    public void DefineLanguage()
    {
        DefineLanguage(_page);
        localisedDateFormat = SharedResource.Resource.GetString("localisedDateFormat", Thread.CurrentThread.CurrentCulture);
    }


    public static void DefineLanguage(System.Web.UI.Page page)
    {
        if (page.Request.Params["setlang"] != null)
        {
            string selectedLanguage = page.Request.Params["setlang"];
            page.Session["setlang"] = selectedLanguage;



            // save language to the db
            long user = GetUserId(page.Session);
            Calani.BusinessObjects.Membership.UserCultureManager.SaveUserCulture(user, selectedLanguage);
            // -->

        }

        if (page.Session["setlang"] != null)
        {
            page.UICulture = page.Session["setlang"].ToString();
            page.Culture = page.Session["setlang"].ToString();

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(page.Session["setlang"].ToString());
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(page.Session["setlang"].ToString());
        }
        else
        {
            if (page.Request.UserLanguages != null)
            {
                //Set the Language.
                string browserLanguage = page.Request.UserLanguages[0];

                page.UICulture = browserLanguage;
                page.Culture = browserLanguage;

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(browserLanguage);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(browserLanguage);
            }
        }

        
    }

    public void UpdateSubscription()
    {
        var db = new Calani.BusinessObjects.Model.CalaniEntities();
        var org = (from r in db.organizations where r.id == CurrentOrganizationId select r).FirstOrDefault();


        if (org == null)
            return;


        if ( org.subscriptionId != null)
        {
            PaymentProcessor pp = new PaymentProcessor();
            var sub = pp.getSubscription(org.subscriptionId);
            if (sub != null)
            {
                org.subscriptionStart = sub.CreatedAt;

                if (sub.Status == Braintree.SubscriptionStatus.ACTIVE)
                {
                    if (sub.NextBillingDate != null)
                    {
                        org.subscriptionEnd = sub.NextBillingDate;

                    }
                    org.subscriptionStatus = "BrainTree_Active";
                }
                else if (sub.DaysPastDue != null && sub.DaysPastDue.Value == 0)
                {

                    if (org.subscriptionEnd > DateTime.Now)
                    {
                        org.subscriptionEnd = DateTime.Now.AddDays(sub.DaysPastDue.Value * -1);
                        org.subscriptionStatus = "BrainTree_Past";
                    }
                }

                // une verif de plus au cas ou...
                if (sub.Status == Braintree.SubscriptionStatus.EXPIRED)
                {
                    org.subscriptionEnd = DateTime.Today;
                    org.subscriptionStatus = "BrainTree_Expired";
                }

                if (sub.Status == Braintree.SubscriptionStatus.CANCELED)
                {
                    org.subscriptionEnd = DateTime.Today;
                    org.subscriptionStatus = "BrainTree_Canceled";
                }
                // -->
                org.trialEnd = null;
            }
        }

        if(org.subscriptionStatus == "Partnership_Active")
        {
            int freeIncludedLicenses = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NewAccountUsersLimit"]);
            Calani.BusinessObjects.Contacts.EmployeesManager mgr = new Calani.BusinessObjects.Contacts.EmployeesManager(CurrentOrganizationId);
            int emp = mgr.CountEmployees(true) - freeIncludedLicenses;
            if (emp < 0) emp = 0;
            org.userLimit = emp;
        }
        db.SaveChanges();
    }

    /*
    private void Page_PreInit(object sender, EventArgs e)
    {
        

        DefineLanguage();
    }
    */

    public string CurrentTextId
    {
        get
        {
            string ret = null;
            if (_page.Request.Params["id"] != null)
            {
                ret = _page.Request.Params["id"];
            }
            return ret;
        }
    }

    public long? CurrentId
    {
        get
        {
            long? ret = null;
            if (_page.Request.Params["id"] != null)
            {
                long l = 0;
                if(long.TryParse(_page.Request.Params["id"], out l))
                {
                    ret = l;
                }
            }
            return ret;
        }
    }
    public string localisedDateFormat { get; set; }

    public long CurrentOrganizationId
    {
        get
        {
            return GetCurrentOrganizationId(_page.Session);
        }
    }


    public static long GetCurrentOrganizationId(System.Web.SessionState.HttpSessionState session)
    {
        if (session.Count == 0)
        {
            if (HttpContext.Current.User.Identity.Name != null)
            {
                InitUserSessions(HttpContext.Current.User.Identity.Name, session);
            }
        }


        return Convert.ToInt64(session["UserOrganizationId"]);
    }

    public Calani.BusinessObjects.Contacts.ContactTypeEnum CurrentUserType
    {
        get
        {
            return GetUserType(_page.Session);
        }
    }

    public static Calani.BusinessObjects.Contacts.ContactTypeEnum GetUserType(System.Web.SessionState.HttpSessionState session)
    {
        if (session.Count == 0)
        {
            if (HttpContext.Current.User.Identity.Name != null)
            {
                InitUserSessions(HttpContext.Current.User.Identity.Name, session);
            }
        }


        Calani.BusinessObjects.Contacts.ContactTypeEnum ret = (Calani.BusinessObjects.Contacts.ContactTypeEnum)Convert.ToInt32(session["UserType"]);

        return ret;
    }

    public static bool GetUserStaff(System.Web.SessionState.HttpSessionState session)
    {
        try
        {
            if (session["UserType2"] != null)
            {
                Calani.BusinessObjects.Contacts.ContactTypeEnum ret = (Calani.BusinessObjects.Contacts.ContactTypeEnum)Convert.ToInt32(session["UserType2"]);
                return ret == Calani.BusinessObjects.Contacts.ContactTypeEnum.PlatformAdmin_Support;
            }
        }
        catch { }
        return false;
    }
    public bool CurrentUserStaff
    {
        get
        {
            return GetUserStaff(_page.Session);
        }
    }

    public long CurrentUserId
    {
        get
        {
            return GetUserId(_page.Session);
        }
    }
    public static long GetUserId(System.Web.SessionState.HttpSessionState session)
    {
        if (session.Count == 0)
        {
            if (HttpContext.Current.User.Identity.Name != null)
            {
                InitUserSessions(HttpContext.Current.User.Identity.Name, session);
            }
        }


        return Convert.ToInt64(session["UserId"]);
    }

    public string CurrentUserName
    {
        get
        {
            return GetUserName(_page.Session);
        }
    }
    public static string GetUserName(System.Web.SessionState.HttpSessionState session)
    {
        if (session.Count == 0)
        {
            if (HttpContext.Current.User.Identity.Name != null)
            {
                InitUserSessions(HttpContext.Current.User.Identity.Name, session);
            }
        }


        if (session["UserName"] != null)
        {
            return session["UserName"].ToString();
        }
        return null;
    }

    public string ItemLabel { get; set; }
    public string ItemsLabel { get; set; }

    public string ParseLabelPlural(string s)
    {
        s = s.Replace("%ItemLabel%", ItemsLabel?? ItemLabel);

        string name = CurrentId.ToString();
        if (String.IsNullOrWhiteSpace(name) && CurrentTextId != null) 
            name = CurrentTextId; 
        if (parentObjectTitle != null)
            name = parentObjectTitle + " " + name;

        s = s.Replace("%CurrentId%", name);

        return s;
    }

    public string ParseLabel(string s, bool toLower = false)
    {
        if (toLower)
            s = s.Replace("%ItemLabel%", ItemLabel.ToLower());
        else
            s = s.Replace("%ItemLabel%", ItemLabel);

        string name = CurrentId.ToString();
        if (String.IsNullOrWhiteSpace(name) && CurrentTextId != null)
            name = CurrentTextId;
        if (parentObjectTitle != null)
            name = parentObjectTitle + " " + name;

        s = s.Replace("%CurrentId%", name);

        return s;
    }


    Panel panelSuccess;
    Label lblSuccess;
    Panel panelError;
    Label lblErrorMessage;
    Button btnDelete;
    HtmlButton btnMore;
    HtmlGenericControl btnMoreDdl;
    Label lblTitle;
    HtmlAnchor linkBackList;
    HtmlAnchor linkNew;
    string parentObjectTitle = null;

    public void InitCrudControls(Panel panelSuccess, Label lblSuccess,
        Panel panelError, Label lblErrorMessage,
        Button btnDelete, HtmlButton btnMore, HtmlGenericControl btnMoreDdl,
        Label lblTitle, HtmlAnchor linkBackList, HtmlAnchor linkNew,
        string parentObjectTitle = null)
    {
        this.panelSuccess = panelSuccess;
        this.lblSuccess = lblSuccess;
        this.panelError = panelError;
        this.lblErrorMessage = lblErrorMessage;
        this.btnDelete = btnDelete;
        this.btnMore = btnMore;
        this.btnMoreDdl = btnMoreDdl;
        this.lblTitle = lblTitle;
        this.linkBackList = linkBackList;
        this.linkNew = linkNew;
        this.parentObjectTitle = parentObjectTitle;

        if (linkBackList != null)
            linkBackList.InnerText = ParseLabelPlural(Resources.Resource.Back_to_var_list);

        if (linkNew != null)
            linkNew.InnerText = ParseLabel(Resources.Resource.Create_another_var, true);//.ToLowerInvariant();

    }

    public void DisplaySuccess(string message)
    {
        panelSuccess.Visible = true;
        message = ResourceHelper.Get(message);
        lblSuccess.Text = ParseLabel(message);
        panelError.Visible = false;
    }
    
    public void DisplayError(string error)
    {
        panelError.Visible = true;
        error = ResourceHelper.Get(error);
        lblErrorMessage.Text = ParseLabel(error);
        panelSuccess.Visible = false;
    }

    public void SetStateNew(string title)
    {
        _page.Title = ParseLabel(title);

        if(lblTitle != null) 
            lblTitle.Text = _page.Title;

        if(btnDelete != null) 
            btnDelete.Visible = false;

        if(btnMore != null) 
            btnMore.Visible = false;

        if (btnMoreDdl != null)
            btnMoreDdl.Visible = false;
    }

    public void SetStateEdit(string title)
    {
        _page.Title = ParseLabel(title);

        if (lblTitle != null) 
            lblTitle.Text = _page.Title;

        if (btnDelete != null) 
            btnDelete.Visible = true;

        if (btnMore != null)
            btnMore.Visible = true;

        if (btnMoreDdl != null)
            btnMoreDdl.Visible = true;
    }


    public string RenderCustomerListOption(bool addDefault = false, List<long> clients = null)
    {
        StringBuilder sb = new StringBuilder();
        Calani.BusinessObjects.CustomerAdmin.ClientsManager mgr = new Calani.BusinessObjects.CustomerAdmin.ClientsManager(this.CurrentOrganizationId);

        if(addDefault)
        {
            sb.Append("<option value='0'>" + Resources.Resource.Select___ + "</option>");
            sb.Append("<option value='-1'>" + Resources.Resource.None + "</option>");
        }

        foreach (var client in mgr.List())
        {
            if (clients != null)
            {
                if(clients.Contains(client.id))
                    sb.Append("<option value=\"" + client.id + "\">" + client.companyName + "</option>");
            }
            else
                sb.Append("<option value=\"" + client.id + "\">" + client.companyName + "</option>");
        }
        
        return sb.ToString();
    }

    public string RenderContactListOption()
    {
        StringBuilder sb = new StringBuilder();
        Calani.BusinessObjects.Contacts.EmployeesManager mgr = new Calani.BusinessObjects.Contacts.EmployeesManager(this.CurrentOrganizationId);
        foreach (var employee in mgr.List())
        {
            sb.Append("<option value=\"" + employee.id + "\">" + employee.firstName +' '+ employee.lastName + "</option>");
        }

        return sb.ToString();
    }


    public string RenderTagListOption()
    {
        StringBuilder sb = new StringBuilder();
        CalaniEntities db = new CalaniEntities();
        var organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
        var taglist =(from t in db.tags select t).Where(t => t.organizationId == organizationId && t.module == (int)Modules.Task || t.type == null ).ToList();
        foreach (var tag in taglist)
        {
            sb.Append("<option value=\"" + tag.id + "\">" + tag.name + "</option>");
        }

        return sb.ToString();
    }

    public int GetQuarter(DateTime dt)
    {
        return (dt.Month - 1) / 3 + 1;
    }

    public string RenderPeriodsListOption(string selection = null, bool withoutAny = false, PeriodFilterOptionsEnum preselect = PeriodFilterOptionsEnum.CurrentCalendarYear)
    {
        StringBuilder sb = new StringBuilder();

        int fiscYearDay = 1; // TODO a lire dans les preferences de la compagnie du user
        int fiscYearMonth = 1;  // TODO a lire dans les preferences de la compagnie du user
        int historyYears = 5; // calculer le nombre d'années + 1 depuis l'inscription de la compagnie du user (ou la date du plus vieux job)

        if(this.CurrentOrganizationId >= 0)
        {
            Calani.BusinessObjects.Model.CalaniEntities db = new Calani.BusinessObjects.Model.CalaniEntities();
            DateTime? fiscYearStart = (from r in db.organizations where r.id == this.CurrentOrganizationId select r.financialYearStart).FirstOrDefault();
            if(fiscYearStart != null)
            {
                fiscYearDay = fiscYearStart.Value.Day;
                fiscYearMonth = fiscYearStart.Value.Month;
            }
        }

        var thisMonth = new DateRange(DatesRangeEnum.CurrentMonth);
        DateTime prevMonthStart = thisMonth.Start.AddMonths(-1);
        int quarterNumber = (DateTime.Now.Month - 1) / 3 + 1;
        var thisQuarter = new DateRange(DatesRangeEnum.CurrentQuarter);

        DateTime prevQuarterStart = thisQuarter.Start.AddMonths(-3);
        var thisYear = new DateRange(DatesRangeEnum.CurrentYear);
        DateTime prevYearStart = new DateTime(DateTime.Now.Year-1, 1, 1);
        var options = new Dictionary<PeriodFilterOptionsEnum, string>() 
        {
            { PeriodFilterOptionsEnum.CurrentCalendarYear, GenerateOption(thisYear.Start.Ticks + ";" + thisYear.End.Ticks, Resources.Resource.Current_calendar_year + " (" + thisYear.Start.Year + ")", selection ?? thisYear.Start.Ticks + ";*") },
            { PeriodFilterOptionsEnum.CurrentMonth, GenerateOption(thisMonth.Start.Ticks+";"+ thisMonth.End.Ticks, Resources.Resource.Current_month + " (" + thisMonth.Start.ToString("MMM")+")", selection) },
            { PeriodFilterOptionsEnum.PrevMonth, GenerateOption(prevMonthStart.Ticks + ";" + thisMonth.Start.Ticks, Resources.Resource.Last_month + " (" + prevMonthStart.ToString("MMM") + ")", selection) },
            { PeriodFilterOptionsEnum.CurrentQuarter, GenerateOption(thisQuarter.Start.Ticks + ";"+thisQuarter.End.Ticks, Resources.Resource.Current_quarter +" (" + thisQuarter.Start.Year + " " + GetQuarterLabel(GetQuarter(thisQuarter.Start)) + ")", selection) },
            { PeriodFilterOptionsEnum.PreviousQuarter, GenerateOption(prevQuarterStart.Ticks + ";" + thisQuarter.Start.Ticks, Resources.Resource.Last_quarter + " (" + prevQuarterStart.Year + " " + GetQuarterLabel(GetQuarter(prevQuarterStart)) + ")", selection) },
            { PeriodFilterOptionsEnum.PrevCalendarYear, GenerateOption(prevYearStart.Ticks + ";" + thisYear.Start.Ticks, Resources.Resource.Last_calendar_year + " (" + prevYearStart.Year + ")", selection)  },
            { PeriodFilterOptionsEnum.AllPeriods, GenerateOption(new DateTime(1990, 1, 1).Ticks + ";" + thisMonth.End.Ticks, Resources.Resource.All_periods, selection) }
        };

        sb.Append("<optgroup label=\""+ Resources.Resource.Calendar+"\">");

        preselect = preselect == PeriodFilterOptionsEnum.None ? PeriodFilterOptionsEnum.CurrentCalendarYear : preselect;
        sb.Append(options[preselect]);

        if(preselect != PeriodFilterOptionsEnum.CurrentCalendarYear) sb.Append(options[PeriodFilterOptionsEnum.CurrentCalendarYear]);
        if (preselect != PeriodFilterOptionsEnum.CurrentMonth) sb.Append(options[PeriodFilterOptionsEnum.CurrentMonth]);
        if (preselect != PeriodFilterOptionsEnum.PrevMonth) sb.Append(options[PeriodFilterOptionsEnum.PrevMonth]);
        if (preselect != PeriodFilterOptionsEnum.CurrentQuarter) sb.Append(options[PeriodFilterOptionsEnum.CurrentQuarter]);
        if (preselect != PeriodFilterOptionsEnum.PreviousQuarter) sb.Append(options[PeriodFilterOptionsEnum.PreviousQuarter]);

        if (withoutAny == false && preselect != PeriodFilterOptionsEnum.AllPeriods)
            sb.Append(options[PeriodFilterOptionsEnum.AllPeriods]);

        if (quarterNumber > 3)//Second Quarter
        {
            DateTime secondQuarterStart = new DateTime(DateTime.Now.Year, (2 - 1) * 3 + 1, 1);

            sb.Append(GenerateOption(secondQuarterStart.Ticks + ";" + secondQuarterStart.AddMonths(3).Ticks, Resources.Resource.Quarter + " (" + secondQuarterStart.Year + " " + GetQuarterLabel(2) + ")", selection));

           
        }

        if (quarterNumber > 2)//First Quarter
        {
            DateTime firstQuarterStart = new DateTime(DateTime.Now.Year, 1, 1);

            sb.Append(GenerateOption(firstQuarterStart.Ticks + ";" + firstQuarterStart.AddMonths(3).Ticks, Resources.Resource.Quarter + " (" + firstQuarterStart.Year + " " + GetQuarterLabel(1) + ")", selection));
        }

        if (preselect != PeriodFilterOptionsEnum.PrevCalendarYear) sb.Append(options[PeriodFilterOptionsEnum.PrevCalendarYear]);

        sb.Append("</optgroup >");
        sb.Append("<optgroup label=\""+ Resources.Resource.Financial_years + "\">");
        for (int i = 0; i < historyYears; i++)
        {
            DateTime from = new DateTime(DateTime.Now.Year - i, fiscYearMonth, fiscYearDay);
            DateTime to = from.AddYears(1);
            string label = "";
            if (i == 0) label = " ("+ Resources.Resource.current + ")";
            sb.Append(GenerateOption(from.Ticks + ";" + to.Ticks, from.Year + label, selection));
        }
        

        sb.Append("</optgroup>");

        return sb.ToString();
    }

    private string GetQuarterLabel(int i)
    {
        return Resources.Resource.QuarterFormat.Replace("{0}", i.ToString());
    }
    

    private string GenerateOption(string value, string label, string selected)
    {
        string ret = "<option ";
        ret += "value=\""+value+"\"";
        
        if (selected == value) 
            ret += "selected=\"selected\"";

        ret += ">";
        ret += label;
        ret += "</option>";
        return ret;
    }


    public static void InitUserSessions(string email, System.Web.SessionState.HttpSessionState sessionBag, bool force=false)
    {
        if (!String.IsNullOrWhiteSpace(email))
        {
            SessionManager.InitUserSessions(email, sessionBag, force);

            long usr = GetUserId(sessionBag);
            string dbcult = Calani.BusinessObjects.Membership.UserCultureManager.GetUserCulture(usr);
            if (!String.IsNullOrWhiteSpace(dbcult))
            {
                sessionBag["setlang"] = dbcult;
            }

        }
    }

    public List<JsVariableForTemplate> GetVariablesForTemplates()
    {
        Calani.BusinessObjects.Membership.LoginManager mgr = new Calani.BusinessObjects.Membership.LoginManager();
        var user = mgr.GetUser(HttpContext.Current.User.Identity.Name);
        

        Calani.BusinessObjects.CustomerAdmin.OrganizationManager org = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(CurrentOrganizationId);
        org.Load();

        List<JsVariableForTemplate> vars = new List<JsVariableForTemplate>();
        vars.Add(new JsVariableForTemplate { name = "myfn", value = user.firstName });
        vars.Add(new JsVariableForTemplate { name = "myln", value = user.lastName });
        vars.Add(new JsVariableForTemplate { name = "mytitle", value = user.title });
        vars.Add(new JsVariableForTemplate { name = "myemail", value = user.primaryEmail });
        vars.Add(new JsVariableForTemplate { name = "myphone", value = user.primaryPhone });
        vars.Add(new JsVariableForTemplate { name = "mymobile", value = user.secondaryPhone });
        vars.Add(new JsVariableForTemplate { name = "mycompany", value = org.CompanyName });
        vars.Add(new JsVariableForTemplate { name = "mycompstreet", value = org.AdrStreet });
        vars.Add(new JsVariableForTemplate { name = "mycompstrnr", value = org.AdrStreetNb });
        vars.Add(new JsVariableForTemplate { name = "mycompzip", value = org.AdrZipCode });
        vars.Add(new JsVariableForTemplate { name = "mycompcity", value = org.AdrCity });
        vars.Add(new JsVariableForTemplate { name = "mycompcountry", value = org.AdrCountry });
        vars.Add(new JsVariableForTemplate { name = "mycompstate", value = org.AdrState });
        vars.Add(new JsVariableForTemplate { name = "mycompaddr2", value = org.AdrStreet2 });
        vars.Add(new JsVariableForTemplate { name = "mycompphone", value = org.Phone });
        vars.Add(new JsVariableForTemplate { name = "mycompfax", value = org.Telecopy });
        
        return vars;
    }

    public long ToUnixMilliseconds(DateTime dt)
    {
        dt = dt.ToUniversalTime();
        TimeSpan sp = dt.Subtract(new DateTime(1970, 1, 1));
        long unixTimestamp = Convert.ToInt64(sp.TotalMilliseconds);
        return unixTimestamp;
    }

    public string Currency
    {
        get
        {
            Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(CurrentOrganizationId);
            mgr.Load();
            string ret = mgr.Currency;
            
            if (String.IsNullOrEmpty(ret))
                ret = "€";

            return ret;
        }

    }

    public static bool IsNewTutorials { get; set; }

    public static List<CurrencyDescription> GetOrgCurrencies(long orgId)
    {
        var list = new List<CurrencyDescription>();
        
        Calani.BusinessObjects.Currency.CurrenciesManager currMgr = new Calani.BusinessObjects.Currency.CurrenciesManager(orgId);
        var currencies = currMgr.List();
        if (currencies.Count > 0)
        {
            Calani.BusinessObjects.CustomerAdmin.OrganizationManager orgmgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(orgId);
            orgmgr.Load();

            list.Add(new CurrencyDescription
            {
                id = null,
                description = orgmgr.Currency,
                symbol = orgmgr.Currency,
                currency1 = orgmgr.Currency,
                rate = 1,
                
            });

            foreach (var c in currencies)
            {
                list.Add(new CurrencyDescription
                {
                    id = c.id.ToString(),
                    description = c.symbol, 
                    symbol = c.symbol,
                    rate = c.rate,
                    currency1 = c.currency1
                });
            }
        }

        return list;
    }


    public bool HasValidPaymentMethod()
    {
        bool ret = false;
        PaymentProcessor pp = new PaymentProcessor();
        var cust = pp.getCustomer(PaymentProcessor.getClientId(this.CurrentOrganizationId));
        if(cust != null)
        {
            foreach (var cc in cust.CreditCards)
            {

                if(cc.IsExpired != null && cc.IsExpired == true)
                {
                    // expired
                }
                else
                {
                    ret = true;
                }
            }
        }
        return ret;
    }
}

public class CurrencyDescription
{
    public string description { get; set; }
    public string symbol { get; set; }
    public string id { get; set; }
    public double rate { get; set; }
    public string currency1 { get; set; }
}