﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

/// <summary>
/// Description résumée de HtmlLanguageSwitch
/// </summary>
public class HtmlLanguageSwitch
{

    public static List<string> _choices = new List<string> { "fr-CH", "en-US" };

    public static string Render(string absoluteUri, string sel, string asset)
    {
        var selLang = new System.Globalization.CultureInfo(sel);

        StringBuilder sb = new StringBuilder();
        sb.AppendLine("<a class=\"dropdown-toggle\" data-toggle=\"dropdown\">");
        sb.AppendLine("<img src=\"" + asset + "/images/flags/" + sel.Substring(0, 2).ToLower() + ".png\" class=\"position-left\" alt=\"\">");
        sb.AppendLine(GetDisplayName(selLang.NativeName));
        sb.AppendLine("<span class=\"caret\"></span>");
        sb.AppendLine("</a>");

        sb.AppendLine("<ul class=\"dropdown-menu\">");
        foreach (var c in _choices)
        {
            if (c != sel)
            {
                var cl = new System.Globalization.CultureInfo(c);
                sb.AppendLine("<li><a href=" + GetUpdatedUri(culture: c, absoluteUri: absoluteUri) + "><img src=\"" + asset + "/images/flags/" + c.Substring(0,2).ToLower() + ".png\"> " + GetDisplayName(cl.NativeName) + "</a></li>");
            }
        }
        
        sb.AppendLine("</ul>");
        return sb.ToString();
    }

    private static Uri GetUpdatedUri(string culture, string absoluteUri)
    {
        var uriBuilder = new UriBuilder(absoluteUri);
        var paramValues = HttpUtility.ParseQueryString(uriBuilder.Query);

        var values = paramValues.GetValues("setlang");
        if (values != null && values.Any())
        {
            paramValues.Remove("setlang");
        }

        paramValues.Add("setlang", culture);

        uriBuilder.Query = paramValues.ToString();

        return uriBuilder.Uri;
    }

    private static string GetDisplayName(string n)
    {
        n = n.Split('(').FirstOrDefault().Trim();
        n = n.ToLower();
        n = n.Substring(0, 1).ToUpper() + n.Substring(1);
        return n;
    }

}

