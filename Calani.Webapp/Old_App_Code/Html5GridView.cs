﻿using Calani.BusinessObjects.Generic;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Description résumée de Html5GridView
/// </summary>
public class Html5GridView
{
    List<string> _rowDataTags;
    private bool AddDateOrder;

    public Html5GridView(GridView gc, bool automaticDynamicTable=true, List<string> rowDataTags=null, string defaultSort = null,
        bool addDateOrder = true)
    {
        AddDateOrder = addDateOrder;

        _rowDataTags = rowDataTags;

        foreach (var col in gc.Columns)
        {
            DataControlField dc = col as DataControlField;
            if(dc != null)
            {
                dc.HeaderText = ResourceHelper.Get(dc.HeaderText);
                
            }
        }

        gc.RowDataBound += Gc_RowDataBound;
        
        gc.DataBind();
        if (gc.HeaderRow != null) 
            gc.HeaderRow.TableSection = TableRowSection.TableHeader;

        if (automaticDynamicTable)
            gc.CssClass = "table datatable-dynamic table-hover";
        else
            gc.CssClass = "table table-hover";

        gc.GridLines = GridLines.None;

        if (!String.IsNullOrEmpty(defaultSort))
        {
            gc.Attributes["data-defaultsort"] = defaultSort;
        }
        
        
    }

    static int CountStringOccurrences(string text, string pattern)
    {
        // Loop through all instances of the string 'text'.
        int count = 0;
        int i = 0;
        while ((i = text.IndexOf(pattern, i)) != -1)
        {
            i += pattern.Length;
            count++;
        }
        return count;
    }

    private void Gc_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Type t = e.Row.DataItem.GetType();
            if (_rowDataTags != null)
            {
                foreach (var r in _rowDataTags)
                {
                    
                    var p = t.GetProperty(r);
                    if(p!=null)
                    {
                        string str = "";
                        var o = p.GetValue(e.Row.DataItem);
                        if (o != null)
                        {
                            if (o.GetType() == typeof(DateTime))
                                o = ((DateTime)o).Ticks;
                            else
                            {
                                DateTime dt;
                                if (DateTime.TryParseExact(o.ToString(), Tools.DefaultDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None ,out dt))
                                {
                                    o = dt.Ticks;
                                }
                            }

                            str = o.ToString();
                        }

                        e.Row.Attributes["data-" + r] = str;

                        
                    }
                }
            }

            foreach (TableCell cell in e.Row.Cells)
            {
                string text = cell.Text;


                foreach (var ctrl in cell.Controls)
                {
                    if (ctrl.GetType().Name.Contains("assets_controls_ratingviewer_ascx"))
                    {
                        var o = ctrl.GetType().GetProperty("Value").GetValue(ctrl);
                        if(o != null)
                        {
                            string ostr = o.ToString();
                            cell.Attributes["data-order"] = ostr;
                        }
                    }

                    if (ctrl.GetType().Name.Contains("assets_controls_timesheetdates_ascx"))
                    {
                        var o = ctrl.GetType().GetProperty("Ticks").GetValue(ctrl);
                        if (o != null)
                        {
                            string ostr = o.ToString();
                            cell.Attributes["data-order"] = ostr;
                        }
                    }

                    
                }



                if (AddDateOrder && !String.IsNullOrWhiteSpace(text))
                {
                    DateTime dt = new DateTime(2000, 1, 1);
                    if (DateTime.TryParseExact(text, "dd/MM/yyyy", System.Globalization.CultureInfo.DefaultThreadCurrentCulture, System.Globalization.DateTimeStyles.AssumeLocal, out dt))
                    {
                        int timestamp = (Int32)(dt.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                        cell.Attributes["data-order"] = timestamp.ToString();

                        cell.Text = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                    if (DateTime.TryParseExact(text, "dd.MM.yyyy", System.Globalization.CultureInfo.DefaultThreadCurrentCulture, System.Globalization.DateTimeStyles.AssumeLocal, out dt))
                    {
                        int timestamp = (Int32)(dt.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                        cell.Attributes["data-order"] = timestamp.ToString();

                        cell.Text = dt.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                    if (DateTime.TryParseExact(text, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.DefaultThreadCurrentCulture, System.Globalization.DateTimeStyles.AssumeLocal, out dt))
                    {
                        int timestamp = (Int32)(dt.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                        cell.Attributes["data-order"] = timestamp.ToString();

                        cell.Text = dt.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }
                    if (DateTime.TryParseExact(text, "dd.MM.yyyy HH:mm:ss", System.Globalization.CultureInfo.DefaultThreadCurrentCulture, System.Globalization.DateTimeStyles.AssumeLocal, out dt))
                    {
                        int timestamp = (Int32)(dt.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                        cell.Attributes["data-order"] = timestamp.ToString();

                        cell.Text = dt.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    }



                }

                //cell.Attributes
                //cell.Text
            }
        }

        
    }
}