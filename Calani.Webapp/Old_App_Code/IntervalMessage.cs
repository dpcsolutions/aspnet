﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Description résumée de IntervalMessage
/// </summary>
public class IntervalMessage
{
    public static string Format(DateTime date)
    {
        string ret = "";
        TimeSpan sp = DateTime.Now.Subtract(date);
        if (sp.Days > 1) ret = sp.Days + " days ago";
        if (sp.Hours > 1) ret = sp.Hours + " hours ago";
        if (sp.Minutes > 1) ret = sp.Minutes + " minutes ago";
        return ret;
    }

    public static string FormatFutur(DateTime date)
    {
        string ret = "";
        TimeSpan sp = date.Subtract(DateTime.Now);
        if (sp.Days > 1) ret = sp.Days + " days left";
        return ret;
    }
}