﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Xml.Linq;
using System.Web;

/// <summary>
/// Summary description for HtmlMenuGenerator
/// </summary>
public class HtmlMenuGenerator
{


    public System.Web.UI.Page Page { get; set; }

    System.Text.StringBuilder sb = new System.Text.StringBuilder();
    
    private string ParseTitle(string title)
    {
        title = ResourceHelper.Get(title);
        return title;
    }

    public string RenderBreadCrumbHtml()
    {
        string file = Page.Server.MapPath("~/Web.sitemap");
        string xml = System.IO.File.ReadAllText(file);

        XDocument xdoc = XDocument.Parse(xml);
        XElement existingPageNode = GetCurrentPageNode(xdoc);

        List<XElement> stack = new List<XElement>();
        while(existingPageNode != null)
        {
            if (existingPageNode.Parent.Name.LocalName == "siteMap") break;

            stack.Add(existingPageNode);
            existingPageNode = existingPageNode.Parent;
            
        }

        System.Text.StringBuilder sb2 = new System.Text.StringBuilder();
        //sb2.Append("<ul class=\"breadcrumb\">");
        for (int i = stack.Count - 1; i >= 0; i--)
        {
            string title = stack[i].Attribute("title").Value;
            title = ParseTitle(title);


            string url = stack[i].Attribute("url").Value;
            string icon = "icon-home4";

            if (stack[i].Attribute("icon") != null && !String.IsNullOrWhiteSpace(stack[i].Attribute("icon").Value))
            {
                icon = stack[i].Attribute("icon").Value;
            }


            if (!url.StartsWith("#"))
            {
                sb2.Append("<li><a href=\"" + Page.ResolveClientUrl("~" + url) + "\"><i class=\"" + icon + " position-left\"></i> " + title + "</a></li>");
            }
            else
            {
                sb2.Append("<li><i class=\"" + icon + " position-left\"></i> " + title + "</li>");
            }
        }
        //sb2.Append("</ul>");

        return sb2.ToString();
    }
    public string RenderMenuHtml(long? orgId = null)
    {
        string file = Page.Server.MapPath("~/Web.sitemap");
        string xml = System.IO.File.ReadAllText(file);

        XDocument xdoc = XDocument.Parse(xml);
        XElement existingPageNode = GetCurrentPageNode(xdoc);

        XElement root = xdoc.FirstNode as XElement;
        root = root.Nodes().FirstOrDefault() as XElement;

        List<XElement> nodes = (from r in root.Nodes() select (XElement)r).ToList();

        var useWorkSchedule = true;
        if (orgId.HasValue)
        {
            var orgMgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(orgId.Value);
            orgMgr.Load();

            useWorkSchedule = orgMgr.UseWorkSchedule;
        }

        sb.AppendLine("<div class=\"category-content no-padding\">");
        sb.AppendLine("<ul class=\"navigation navigation-main navigation-accordion\">");
        RenderHtml(nodes, existingPageNode, useWorkSchedule);
        sb.AppendLine("</ul>");
        sb.AppendLine("</div>");

        return sb.ToString();
    }

    public CurrentMenuProperty GetCurrentMenuProperty()
    {
        CurrentMenuProperty ret = new CurrentMenuProperty();

        string file = Page.Server.MapPath("~/Web.sitemap");
        string xml = System.IO.File.ReadAllText(file);

        XDocument xdoc = XDocument.Parse(xml);
        XElement existingPageNode = GetCurrentPageNode(xdoc);

        if(existingPageNode != null)
        {
            ret.Name = existingPageNode.Attribute("title").Value;
           

            if(existingPageNode.Attribute("icon") != null && !String.IsNullOrWhiteSpace(existingPageNode.Attribute("icon").Value))
                ret.Icon = existingPageNode.Attribute("icon").Value;
            if (existingPageNode.Attribute("color") != null && !String.IsNullOrWhiteSpace(existingPageNode.Attribute("color").Value))
                ret.Color = existingPageNode.Attribute("color").Value;
            if (existingPageNode.Attribute("roles") != null && !String.IsNullOrWhiteSpace(existingPageNode.Attribute("roles").Value))
                ret.Roles = existingPageNode.Attribute("roles").Value;
            if (existingPageNode.Attribute("help") != null && !String.IsNullOrWhiteSpace(existingPageNode.Attribute("help").Value))
                ret.Help = existingPageNode.Attribute("help").Value;

            while (existingPageNode != null)
            {
                existingPageNode = existingPageNode.Parent;
                if (existingPageNode == null) break;

                if(ret.Icon == null)
                {
                    if (existingPageNode.Attribute("icon") != null && !String.IsNullOrWhiteSpace(existingPageNode.Attribute("icon").Value))
                        ret.Icon = existingPageNode.Attribute("icon").Value;
                }

                if (ret.Color == null)
                {
                    if (existingPageNode.Attribute("color") != null && !String.IsNullOrWhiteSpace(existingPageNode.Attribute("color").Value))
                        ret.Color = existingPageNode.Attribute("color").Value;
                }

                if (ret.Roles == null)
                {
                    if (existingPageNode.Attribute("roles") != null && !String.IsNullOrWhiteSpace(existingPageNode.Attribute("roles").Value))
                        ret.Roles = existingPageNode.Attribute("roles").Value;
                }

                if (ret.Help == null)
                {
                    if (existingPageNode.Attribute("help") != null && !String.IsNullOrWhiteSpace(existingPageNode.Attribute("help").Value))
                        ret.Help = existingPageNode.Attribute("help").Value;
                }

                if (ret.Icon != null && ret.Color != null) break;
            }

        }
        ret.Name = ParseTitle(ret.Name);
        return ret;
    }

    private XElement GetCurrentPageNode(XDocument xdoc)
    {
        var t = xdoc.Descendants().ToList();
        string existingUrl = this.Page.Request.Url.AbsolutePath;
        int existingUrlStart = existingUrl.IndexOf("/", 2);
        if (existingUrlStart > 0) existingUrl = existingUrl.Substring(existingUrlStart);
        if (existingUrl.ToLower().EndsWith("default.aspx"))
            existingUrl = existingUrl.Substring(0, existingUrl.Length - 12);

        XElement existingPageNode = (from r in xdoc.Descendants()
                                     where r.Attribute("url") != null
   && r.Attribute("url").Value.ToLower() == existingUrl.ToLower()
                                     select r).FirstOrDefault();

        if (existingPageNode == null)
        {
            existingPageNode = (from r in xdoc.Descendants()
                                where r.Attribute("alias") != null
                                    && r.Attribute("alias").Value.ToLower().Contains(existingUrl.ToLower() + ";")
                                select r).FirstOrDefault();
        }

        return existingPageNode;
    }


    private void RenderHtml(List<XElement> nodes, XElement current, bool useWorkSchedule)
    {

        foreach (XElement item in nodes)
        {
            string title = item.Attribute("title").Value;
            string otitle = title;
            title = ParseTitle(title);
            string url = item.Attribute("url").Value;
            string color = "";
            string icon = "icon-home4";
            string roles = "";

            if (!useWorkSchedule && url == "/Time/WorkSchedulers/")
            {
                continue;
            }
            var skip = url == "#DEVELOPPERS" && ConfigurationManager.AppSettings["developperAccess"].ToLower() != "true";

            if (!skip)
            {
                if (item.Attribute("icon") != null && !String.IsNullOrWhiteSpace(item.Attribute("icon").Value))
                {
                    icon = item.Attribute("icon").Value;
                }

                if (item.Attribute("roles") != null && !String.IsNullOrWhiteSpace(item.Attribute("roles").Value))
                {
                    roles = item.Attribute("roles").Value;
                }

                if (item.Attribute("color") != null && !String.IsNullOrWhiteSpace(item.Attribute("color").Value))
                {
                    if (item.DescendantNodes().Contains(current))
                    {
                        //color = "bg-" + item.Attribute("color").Value + "-800 text-white";
                        color = "bg-gesmobile text-white";
                    }

                    else if(item == current)
                    {
                        //color = "bg-" + item.Attribute("color").Value + "-800 text-white";
                        color = "bg-gesmobile text-white";
                    }
                }

                bool isactive = false;
                bool isNavigationHeader = false;
                bool isroleallowed = true;
                if (url == "/#MYCOMPANY" && current == null) 
                    isactive = true;

                if (!String.IsNullOrWhiteSpace(roles))
                {
                    string usertype = PageHelper.GetUserType(Page.Session).ToString().Split('_').LastOrDefault().ToLower();
                    List<string> allowedRoles = roles.ToLower().Split(';').ToList();

                    if (!allowedRoles.Contains(usertype))
                    {
                        isroleallowed = false;
                    }
                }

                if (isroleallowed)
                {
                    if (current != null && current.Attribute("url").Value.ToLower().Equals(item.Attribute("url").Value.ToLower()))
                    {
                        isactive = true;
                    }

                    if (otitle == "Home")
                    {
                        sb.AppendLine("<li class=\"navigation-header\"><span>" + title + "</span>");
                        isNavigationHeader = true;
                    }
                    else
                    {

                        if (isactive)
                        {
                            sb.AppendLine("<li class=\"active\">");
                        }
                        else
                        {
                            sb.AppendLine("<li>");
                        }



                        string cssactive = "";
                        if (isactive) cssactive = " active";

                        string absurl = Page.ResolveClientUrl("~" + url);
                        if (url.StartsWith("#")) absurl = "#";
                        sb.AppendLine("<a href=\"" + absurl + "\" class=\"" + color + cssactive + "\"><i class=\"" + icon + "\"></i> <span>" + title + "</span></a>");
                    }


                    if (item.Nodes().Count() > 0)
                    {
                        if (!isNavigationHeader) sb.AppendLine("<ul>");
                        List<XElement> subNodes = (from r in item.Nodes() where r as XElement != null select (XElement)r).ToList();
                        RenderHtml(subNodes, current, useWorkSchedule);
                        if (!isNavigationHeader) sb.AppendLine("</ul>");
                    }


                    sb.AppendLine("</li>");

                }
            }
        }

    }


   
}

public class CurrentMenuProperty
{
    public string Color { get; set; }
    public string Icon { get; set; }
    public string Name { get; set; }
    public string Roles { get; set; }

    public string Help { get; set; }
}