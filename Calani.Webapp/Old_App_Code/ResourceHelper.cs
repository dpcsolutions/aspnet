﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web;

/// <summary>
/// Description résumée de ResourceHelper
/// </summary>
public class ResourceHelper
{
    static ResourceManager _resourceManager;

    public static string Get(string name)
    {
        if (name == null) return null;

        if (_resourceManager == null)
        {
            Assembly assembly = System.Reflection.Assembly.Load("App_GlobalResources");
            _resourceManager = new ResourceManager("Resources.Resource", assembly);
        }

        string ret = name;
        ret = _resourceManager.GetString(name);
        if (ret == null) ret = name;
        return ret;
    }

    public static string Get(string name, params object[] options)
    {
        string s = Get(name);
        for (int i = 0; i < options.Length; i++)
        {
            s = s.Replace("$" + i, options[i].ToString());
        }
        return s;
    }
}