﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Description résumée de ImageUploadExtractor
/// </summary>
public class ImageUploadExtractor
{
    FileUpload _f;

    public List<string> Mime { get; set; }
    public ImageUploadExtractor(FileUpload f)
    {
        _f = f;
        Mime = new List<string>();
    }

    public byte[] Extract()
    {
        if (_f.PostedFile == null) return null;
        if (_f.PostedFile.ContentLength == 0) return null;

        bool allowed = (from r in Mime where r.ToLower() == _f.PostedFile.ContentType.ToLower() select r).Count() >= 1;
        if (allowed)
        {
            byte[] ret = _f.FileBytes;
            return ret;
        }
        else
        {
            throw new Exception("File type " + _f.FileContent + " not allowed");
        }
    }
}