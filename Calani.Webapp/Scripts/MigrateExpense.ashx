﻿<%@ WebHandler Language="C#" Class="MigrateExpense" %>

using System;
using System.Web;
using System.Linq;
using System.IO;

public class MigrateExpense : IHttpHandler {

    string dir;

    public void ProcessRequest (HttpContext context) {
        context.Response.ContentType = "text/plain";
        dir = context.Server.MapPath("~/App_Data/storage/expenses");

        /*
        var db = new Calani.BusinessObjects.Model.CalaniEntities();
        var exp = (from r in db.expenses where r.picture != null select r).ToList();
        foreach (var e in exp)
        {
            long id = e.id;
            long org = e.contacts.organizationId.Value;
            string pict = e.picture;
            StoreExpense(id, org, pict);

           
             context.Response.Write("\r\nOrg " + org + ";\tId " + id);
        }
        */
       
    }


    void StoreExpense(long id, long org, string pict)
    {
        FileInfo fi = new FileInfo(Path.Combine(dir, org.ToString(), id.ToString() + ".jpg"));
        DirectoryInfo di = fi.Directory;
        if (!di.Exists) di.Create();
        byte[] bin = Convert.FromBase64String(pict);
        System.IO.File.WriteAllBytes(fi.FullName, bin);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}