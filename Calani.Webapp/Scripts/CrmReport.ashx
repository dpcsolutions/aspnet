﻿<%@ WebHandler Language="C#" Class="CrmReport" %>

using System;
using System.Web;

public class CrmReport : IHttpHandler, System.Web.SessionState.IRequiresSessionState {

    public void ProcessRequest (HttpContext context)
    {

        if (context.Session != null && context.Session["staff"] != null && context.Session["staff"].ToString().Equals("staff"))
        {

            string sql = context.Server.MapPath("~/App_Data/dbschema/crmreport.sql");
            sql = System.IO.File.ReadAllText(sql);

            Calani.BusinessObjects.Generic.SqlFileLauncher sqldata = new Calani.BusinessObjects.Generic.SqlFileLauncher();
            var dt = sqldata.LaunchReport(sql);

            Calani.BusinessObjects.DocGenerator.ExcelTableGenerator gen = new Calani.BusinessObjects.DocGenerator.ExcelTableGenerator();
            gen.Table = dt;
            byte[] bin = gen.GenerateDocument();

            context.Response.OutputStream.Write(bin, 0, bin.Length);

            context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            context.Response.AddHeader("content-disposition", "Attachment; filename=CrmReport-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");


        }

    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}