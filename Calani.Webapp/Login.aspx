﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="Login" Codebehind="Login.aspx.cs" %>
<!DOCTYPE html>
<html lang="en">
<head runat="server">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
	<title>GES</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="assets/css/colors.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<script type="text/javascript" src="assets/js/pages/login.js"></script>
	<!-- /theme JS files -->

    <style type="text/css">
        .bg-gesmobile {
            background: #934553;
            color: white;
        }

        .color-gesmobile
        {
            color: #934553;
        }

        body{
            background: url('assets/images/splash.jpg');
            width: 100%;
            height: 90vh;
            background-repeat: no-repeat;
            background-position: center center;
            background-size: cover;
            background-attachment: fixed;
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121845992-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-121845992-1');
    </script>

</head>

<body class="login-container">

        <ul class="nav navbar-nav navbar-right">
			<li class="dropdown language-switch">
				<%= LanguageSwitch %>
			</li>
        </ul>

    <form runat="server">
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Advanced login -->
					<div>

                        <div class="panel panel-body login-form" runat="server" id="dualAuthLogin" visible="false">
                            <div class="text-center">
                        		<div class="icon-object border-slate-300 text-slate-300"><i class="icon-shield2"></i></div>
                        		<h5 class="content-group">Double authentification<small class="display-block"><%= Resources.Resource.DualAuthPageMessage%></small></h5>
                        	</div>
	                        
                            <div class="alert alert-danger alert-bordered" id="lblError2" runat="server" visible="false">
								<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only"><%= Resources.Resource.Close %></span></button>
								<span class="text-semibold"><%= Resources.Resource.WrongCode %></span>
							</div>
	                        
                            <div class="alert alert-success alert-bordered" id="lblAlert" runat="server" visible="false">
								<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only"><%= Resources.Resource.Close %></span></button>
								<span class="text-semibold"><%= Resources.Resource.CodeSent %></span>
							</div>
	                        
                            <div class="form-group has-feedback has-feedback-left">
                        		<input value="" type="text" class="form-control" placeholder='Authentification code' runat="server" id="tbxDualAuthCode">
                        		<div class="form-control-feedback">
                        			<i class="icon-lock2 text-muted"></i>  
                        		</div>
                        	</div>
                            <div class="form-group">
                        		<asp:Button runat="server" id="btnDualAuthLogin" type="submit" CssClass="btn bg-gesmobile btn-block"  OnClick="btnDualAuthLogin_Click" Text='Login' />
                        	</div>

							<div class="form-group">
								<asp:Button runat="server" id="btnResendToken" type="submit" CssClass="btn btn-default btn-block content-group"  OnClick="btnResendToken_Click" Text='<%$ Resources:Resource, ResendСode %>' />
							</div>
                        </div>

                        <div class="panel panel-body login-form" runat="server" id="loginStaff" visible="false">
                            <div class="text-center">
								<div class="icon-object border-slate-300 text-slate-300"><i class="icon-hat"></i></div>
								<h5 class="content-group">Staff access <small class="display-block">Check your emails</small></h5>
							</div>
                            <div class="form-group has-feedback has-feedback-left">
								<input type="password" class="form-control" placeholder='Staff code' runat="server" id="tbxStaffCode">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>
                            <div class="form-group">
								<asp:Button runat="server" id="btnStaffLogin" type="submit" CssClass="btn bg-gesmobile btn-block"  OnClick="btnStaffLogin_Click" Text='Staff Login' />
							</div>
                        </div>
						
                        <div class="panel" runat="server" id="staffSelector" visible="false">
                            <div class="panel-body">
                                <div class="text-center">
								    <div class="icon-object border-slate-300 text-slate-300"><i class="icon-hat"></i></div>
								    <h5 class="content-group">Staff access <small class="display-block">Choose an organization</small></h5>
							    </div>

                                <p style="text-align:center;">
                                    <a href="Scripts/CrmReport.ashx" class="btn btn-primary">
                                        <!--i class="icon-excel"></!--i-->
                                        CRM Report
			                        </a>
                                </p>

                                <asp:GridView runat="server" ID="gridSelector" AutoGenerateColumns="false" OnRowCommand="gridSelector_RowCommand">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="Connect" runat="server" CausesValidation="True" CommandName="Use" Text="[Use]" CommandArgument='<%# Bind("OrgId") %>' />     
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="ID" DataField="OrgId" />
                                        <asp:BoundField HeaderText="Name" DataField="Name" />
                                        <asp:BoundField HeaderText="Login activation" DataField="ActivationDateTime" />
                                        <asp:BoundField HeaderText="Trial End" DataField="TrialEnd" />
                                        <asp:BoundField HeaderText="Subscription End" DataField="SubscriptionEnd" />
                                        <asp:BoundField HeaderText="Subscription Users" DataField="SubscriptionUsers" />
                                        <asp:BoundField HeaderText="Subscription Status" DataField="SubscriptionStatus" />
                                        <asp:BoundField HeaderText="Subscription Id" DataField="SubscriptionId" />
                                        
                                    </Columns>
                                </asp:GridView>
                                <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/assets/js/plugins/tables/datatables/datatables.min.js") %>"></script>
                                <script type="text/javascript">
                                    function initDataTable(dt, firstColDesc) {

                                        var initOrder = null;
                                        if (firstColDesc) initOrder = [[0, "desc"]];
                                        else initOrder = [[0, "asc"]];

                                        return dt.DataTable({
                                            buttons: {
                                                buttons: [
                                                    {
                                                        extend: 'copyHtml5',
                                                        className: 'btn btn-default',
                                                        exportOptions: {
                                                            columns: [0, ':visible']
                                                        }
                                                    },
                                                    {
                                                        extend: 'excelHtml5',
                                                        className: 'btn btn-default',
                                                        exportOptions: {
                                                            columns: ':visible'
                                                        }
                                                    },
                                                    {
                                                        extend: 'colvis',
                                                        text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
                                                        className: 'btn bg-blue btn-icon'
                                                    }
                                                ]
                                            },
                                            fixedHeader: true,
                                            order: initOrder
                                        });
                                    }
                                    $(function () {
                                        initDataTable($('.datatable-dynamic'));
                                    });
                                </script>
                            </div>
                        </div>
						<div class="panel panel-body login-form" runat="server" id="loginForm">
							<div class="text-center">
								<!--div class="icon-object border-slate-300 text-slate-300"><i class="icon-user"></i></!--div-->
                                <div><img src="assets/images/logo.png" style="width:140px;height:140px;" /></div>
								<h5 class="content-group"><%= Resources.Resource.LoginToApp %> <small class="display-block"><%= Resources.Resource.YourCredentials %></small></h5>
							</div>


                            <div class="alert alert-danger alert-bordered" id="lblError" runat="server" visible="false">
								<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only"><%= Resources.Resource.Close %></span></button>
								<span class="text-semibold"><%= Resources.Resource.WrongCredentials %></span>
							</div>

							<div class="form-group has-feedback has-feedback-left">
                                <input type="text" class="form-control" placeholder='<%$ Resources:Resource, EmailAddress %>' runat="server" id="tbxLogin"/>
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group has-feedback has-feedback-left">
                                <input type="password" class="form-control" placeholder='<%$ Resources:Resource, Password %>' runat="server" id="tbxPassword"/>
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<div class="form-group login-options">
								<div class="row">
									<div class="col-sm-12">
										<label class="checkbox-inline">
                                            <asp:CheckBox runat="server" ID="cbxRemember" CssClass="styled" />
											<%= Resources.Resource.Remember %>
										</label>
									</div>

									
								</div>
                                <div class="row">
									

									<div class="col-sm-12 text-right">
										<a href="PasswordRecover.aspx" class="color-gesmobile"><%= Resources.Resource.ForgotPassword %></a>
									</div>
								</div>
							</div>

							<div class="form-group">
								<asp:Button runat="server" id="btnLogin" type="submit" CssClass="btn bg-gesmobile btn-block"  OnClick="btnLogin_Click" Text='<%$ Resources:Resource, Login %>' />
							</div>


							<div class="content-divider text-muted form-group"><span><%= Resources.Resource.DontHaveAccount %></span></div>
							<a href="Subscribe.aspx" class="btn btn-default btn-block content-group"><%= Resources.Resource.Subscribe %></a>
							<span class="help-block text-center no-margin"> <a href="TermsAndConditions.aspx" target="_blank" class="color-gesmobile"><%= Resources.Resource.TermsAndConditions %></a></span>
						</div>

                        <br />
                        <div class="alert alert-info alert-styled-right alert-bordered" style="width:500px; margin:0 auto 20px auto">
							<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only"><%= Resources.Resource.Close %></span></button>

                            

                            <%= Resources.Resource.CookiesBanner %>


							
						</div>
					</div>
					<!-- /advanced login -->


                    <!-- Footer -->
                    <div class="row  text-muted text-center">
                        &copy; <asp:Label runat="server" ID="LabelCopyYear" />. 
                        <asp:HyperLink runat="server" ID="HyperLinkCopy" />
                    </div>
                    <!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->
        

	</div>
	<!-- /page container -->
    </form>

</body>
</html>
