﻿<%@ Page Title="DeliveryNote" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Projects_DeliveryNotes_Default" Codebehind="Default.aspx.cs" %>
<%@ Register src="../../assets/Controls/RatingViewer.ascx" tagname="RatingViewer" tagprefix="rv" %>
<%@ Register src="../../assets/Controls/ProjectStatusLabel.ascx" tagname="ProjectStatusLabel" tagprefix="psl" %>
<%@ Register src="../../assets/Controls/ImportedInLink.ascx" tagname="ImportedInLink" tagprefix="iil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
    <div class="btn-group" runat="server" id="quickmenu">
		<button type="button" class="btn bg-gesmobile btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
		</button>

		<ul class="dropdown-menu dropdown-menu-right">
			<li><a href="Job.aspx"><i class="icon-plus2"></i> <%= Resources.Resource.Create %> <%= LabelQuoteOrDeliveryNote %></a></li>
		</ul>
	</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">




        <div class="content" id="app">

                    <div class="row">

                       
						<div class="col-sm-9 col-md-6">
								

							<!-- User card with thumb and social icons at the bottom -->
							<div class="panel">
									<div class="panel-body">
										<div class="row text-center">
											
											<div class="col-xs-2" style="width:20%" v-bind:class="{ 'bordered alpha-primary': statusFilter=='DeliveryNoteDraft' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.Draft_Tooltip %>">
                                                <a href="" v-on:click="setStatusFilter($event, 'DeliveryNoteDraft')" class="text-grey">
                                                    <p><i class="icon-floppy-disk icon-2x display-inline-block text-grey tada infinite" id="icoCountDraft" runat="server"></i></p>
                                                    <h5 class="text-semibold no-margin" id="lblCountDraft" runat="server">.</h5>
                                                    <p><strong class="text-muted text-size-small"><%= Resources.Resource.Draft_many %></strong></p>

                                                </a>

											</div>

											<div class="col-xs-2" style="width:20%" v-bind:class="{ 'bordered alpha-primary': statusFilter=='DeliveryNoteSent' }"  data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.Sent_Tooltip %>">
												
                                                <a href="" v-on:click="setStatusFilter($event, 'DeliveryNoteSent')">
                                                    <p><i class="icon-envelop icon-2x display-inline-block text-primary tada infinite" id="icoCountSent" runat="server"></i></p>
                                                    <h5 class="text-semibold no-margin" id="lblCountSent" runat="server">.</h5>
                                                    <p><strong class="text-muted text-size-small"><%= Resources.Resource.Sent_many %></strong></p>
                                                    
                                                </a>

												
											</div>

                                            <div class="col-xs-2" style="width:20%" v-bind:class="{ 'bordered alpha-primary': statusFilter=='DeliveryNoteRefused' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.Declined_Tooltip %>">
                                                <a href="" v-on:click="setStatusFilter($event, 'DeliveryNoteRefused')" class="text-danger" >
                                                    <p><i class="icon-trash icon-2x display-inline-block text-danger tada infinite"  id="icoCountRefused" runat="server"></i></p>
												<h5 class="text-semibold no-margin" id="lblCountRefused" runat="server">.</h5>
                                                    <p><strong class="text-muted text-size-small"><%= Resources.Resource.Declined_many %></strong></p>
                                                   
                                                </a>
											</div>

                                            <div class="col-xs-2" style="width:20%" v-bind:class="{ 'bordered alpha-primary': statusFilter=='DeliveryNoteAccepted' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.Accepted_Tooltip %>">
                                                <a href="" v-on:click="setStatusFilter($event, 'DeliveryNoteAccepted')" class="text-green" >
                                                    <p><i class="icon-checkmark icon-2x display-inline-block tada infinite text-green" id="icoCountAccepted" runat="server"></i></p>
												<h5 class="text-semibold no-margin" id="lblCountAccepted" runat="server">.</h5>
													
                                                    <p><strong class="text-muted text-size-small"><%= Resources.Resource.Accepted_many %></strong></p>

                                                </a>
											</div>
                                            <div class="col-xs-2" style="width:20%" v-bind:class="{ 'bordered alpha-primary': statusFilter=='DeliveryNoteMissingChild' }"  data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.AvailableForImportOfInvoice %>">
                                                <a href="" v-on:click="setStatusFilter($event, 'DeliveryNoteMissingChild')" class="text-grey">
                                                <p><i class="icon-notification2 icon-2x display-inline-block tada infinite text-grey" id="icoCountMissingChild" runat="server"></i></p>
                                                <h5 class="text-semibold no-margin" id="lblCountMissingChild" runat="server">.</h5>
                                                
                                                    <p><strong class="text-muted text-size-small"><%= Resources.Resource.Warning %></strong></p>
                                                
                                                </a>
                                            </div>

										</div>									
									</div>

							    	<div class="panel-footer text-center no-padding">
								    	<div class="row">
								    		<a  href=""  v-on:click="setStatusFilter($event, null)" class="display-block p-10 text-default" 
								    			data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.ListAllDeliveryNotes %>">
								    				<i class="fa fa-ellipsis-h"></i> <%= Resources.Resource.Show_all %>
								    				
								    			</a>
								    	</div>
									</div>
								</div>
								<!-- /user card with thumb and social icons at the bottom -->

							</div>


							<div class="col-sm-6 col-md-3">
								
							</div>

							<div class="col-sm-7 col-md-3">
								<div class="panel panel-body panel-body-accent">
									<div class="media no-margin">
										
										<div>
											<i class="fa fa-calendar-check-o" aria-hidden="true"></i> <%= Resources.Resource.Select_Period %>
										</div>
										<div>
											<select id="ddlPeriod" class="select" onchange="periodChanged()">
												<%= OptionsPeriod %>
											</select>
										</div>

										<div style="margin-top:20px">
											<i class="fa fa-user-circle-o" aria-hidden="true"></i> <%= Resources.Resource.Customers %>
										</div>
										<div>
											<select id="ddlClient" data-placeholder="<%= Resources.Resource.Select___ %>" multiple="multiple" class="select" onchange="setClientFilter()">
                                                <%= OptionsClients %>
											</select>
										</div>
									</div>
								</div>
							</div>
					</div>

					<div class="row">
                        <div class="col-sm-12 col-md-12">
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="icon-upload display-inline-block text-green"></i>
										<strong><%= ph.CMP.Name %></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

							<div class="panel-body">
                                
								<table id="delivery-notes-datatable" class="table table-hover dataTable no-footer">
									<thead>
										<tr>
											<th><%= Resources.Resource.Code %></th>
											<th><%= Resources.Resource.Customer %></th>
											<th><%= Resources.Resource.Description %></th>
											<th><%= Resources.Resource.Amount %></th>
											<th><%= Resources.Resource.Status %></th>
											<th><%= Resources.Resource.Date %></th>
											<th><%= Resources.Resource.LastUpdate %></th>
										</tr>
									</thead>
								</table>
                                
                            </div><!-- panel-body -->

                            <div class="panel-footer text-right no-padding"  runat="server" id="panelAdd">
						    	<div class="row" style="margin:10px">
						    		<a href="Job.aspx" class="btn btn-default"><i class="icon-plus2 position-left"></i> 
                                        <%= Resources.Resource.CreateNew %>
						    		</a>
						    	</div>
							</div><!-- panel-footer -->

                        </div>
                        </div>

                    </div>
        </div><!-- /content -->

    </form>
    <script type="text/javascript">
        var currency = '<%= ph.Currency %>';
		var DeliveryNote = <%= DeliveryNote.ToString().ToLower() %>;
        var _assigneeId = <%= AssigneeId %>;
		var _creatorId = <%= CreatorId %>;
    </script>
    <script type="text/javascript" src="Default.js?<%= Guid.NewGuid().ToString("n") %>"></script>
    <%--<script type="text/javascript" src="Default.js"></script>--%>
    
</asp:Content>

