﻿var app = new Vue({
    el: '#app',
    data: {
        commentId: commentId,
        items: tableDataSource,
        ressources: ressources
    },
    computed: {
        
        isAccept: function () {
            return this.items && this.items.length > 0 && this.items.length == $.grep(this.items, function (v, i) {
                return v.isChecked == true;
            }).length;
        },

        isReject: function () {
            return true;
        }
    },
    methods: {
        getComment: function () {
            return document.getElementById(commentId).value;
        },

        onRejectClick: function (e) {
            $('#rejectquote').click();
        },

        onAcceptClick: function (e) {
            var checked =  this.items && this.items.length > 0 && this.items.length == $.grep(this.items, function (v, i) {
                return v.isChecked == true;
            }).length;

            if (checked)
                $('#acceptquote').click();
            else {
                swal({
                    title: ressources.Validation_Error,
                    text: ressources.ERR_NotAllItemsChecked,
                    type: "error",
                    showCancelButton: false,
                    confirmButtonText: ressources.Close
                });
            }

        }
        
    }

});