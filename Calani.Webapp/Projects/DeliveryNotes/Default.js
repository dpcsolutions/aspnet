﻿var _periodFilterFrom = null;
var _periodFilterTo = null;
var _clientFilter = null;
var _statusFilter = 'DeliveryNoteSent';

var _dataTable = null;

function checkField(val, str){
    return val?.toString().toLowerCase().includes(str.toLowerCase());
}
function containsString (obj, str) {    
    if (checkField(obj.Ids.InternalId, str)) return true;
    if (checkField(obj.DateStr?.split(' ')[0], str)) return true;
    if (checkField(obj.Description, str)) return true;
    if (checkField(getProjectStatusNamable(obj.Status), str)) return true;
    if (checkField(obj.LastUpdateDateStr?.split(' ')[0], str)) return true;
    if (checkField(obj.Total, str)) return true;
    if (checkField(obj.ClientName, str)) return true;
    
    return false;
}

function setClientFilter() {
    _clientFilter = $('#ddlClient').val();
    refreshLines();
}

function setStatusFilter(newStatus) {
    _statusFilter = newStatus;
    refreshLines();
}

function refreshLines() {
    _dataTable.draw();
}

function refreshLiveStats() {

    let countDeliveryNoteDraft = 0;
    let countDeliveryNoteSent = 0;
    let countDeliveryNoteRefused = 0;
    let countDeliveryNoteAccepted = 0;
    let countMissingChild = 0;

    let rowsId = _dataTable.rows().eq(0);
    let searchText = $('.dataTables_filter input').val();
    
    for (let i = 0; i < rowsId.length; i++) {

        let row = _dataTable.row(rowsId[i]);
        let data = row.data();
        
        let matchesSearch = true;
        if (searchText) matchesSearch = containsString(data, searchText);
        let $node = $(row.node());
        
        if (row.node().attributes["data-anystatusfilter"] != null && row.node().attributes["data-anystatusfilter"].value == "true") {

            if (data["Status"] != null && data["Total"] != null) {

                let status = data["Status"];
                
                switch (status) {
                    case "DeliveryNoteDraft":
                        if (matchesSearch){
                            $node.removeClass("no-rating");
                            countDeliveryNoteDraft++;
                        }
                        break;
                    case "DeliveryNoteSent":
                        if (matchesSearch){
                            $node.removeClass("no-rating");
                            countDeliveryNoteSent++;
                        }
                        break;
                    case "DeliveryNoteAccepted":
                        if (matchesSearch){
                            $node.removeClass("no-rating");
                            countDeliveryNoteAccepted++;
                            if (data["MissingChild"] === true) countMissingChild++;
                        }
                        break;
                    // case "DeliveryNoteOutDated":
                    //     if (matchesSearch){
                    //         $node.removeClass("no-rating");
                    //         countDeliveryNoteOutDated++;
                    //     }
                    //     break;
                    case "DeliveryNoteRefused":
                        if (matchesSearch){
                            $node.addClass("no-rating");
                            countDeliveryNoteRefused++;
                        }
                        break;
                    default:
                        if (matchesSearch){
                            $node.addClass("no-rating");
                        }
                        break;
                }
            }
        }
    }

    $('#ContentPlaceHolder1_lblCountDraft').html(countDeliveryNoteDraft);
    $('#ContentPlaceHolder1_lblCountSent').html(countDeliveryNoteSent);
    $('#ContentPlaceHolder1_lblCountRefused').html(countDeliveryNoteRefused);
    $('#ContentPlaceHolder1_lblCountAccepted').html(countDeliveryNoteAccepted);
    $('#ContentPlaceHolder1_lblCountMissingChild').html(countMissingChild);
    
    if (countDeliveryNoteAccepted > 0)
        $('#ContentPlaceHolder1_icoCountAccepted').addClass("animated")
    else
        $('#ContentPlaceHolder1_icoCountAccepted').removeClass("animated")
}
function filterItem (settings, data, dataIndex) {

    var visible = true;

    if (_clientFilter != null) {
        var clientMatch = false;
        for (var i = 0; i < _clientFilter.length; i++) {
            if (_clientFilter[i] == settings.aoData[dataIndex]._aData["ClientId"])
                clientMatch = true;
        }
        if (clientMatch == false)
            visible = false;
    }

    var lastUpdate = dateTimeToTicks(parseDate(settings.aoData[dataIndex]._aData["LastUpdateDate"]));
    if (_periodFilterFrom != null) {
        if (lastUpdate < _periodFilterFrom)
            visible = false;
    }
    if (_periodFilterTo != null) {
        if (lastUpdate >= _periodFilterTo)
            visible = false;
    }

    settings.aoData[dataIndex].nTr.setAttribute('data-anystatusFilter', visible);

    if (_statusFilter != null) {

        if (_statusFilter == "DeliveryNoteMissingChild") {
            visible = settings.aoData[dataIndex]._aData["MissingChild"] == true;
        }
        else if (settings.aoData[dataIndex]._aData["Status"] != _statusFilter)
            visible = false;
    }

    return visible;
}

$(function () {

    // client side filters
    $.fn.dataTable.ext.search.push(filterItem);

    _dataTable = initDeliveryNotesDataTable($('#delivery-notes-datatable'), getDataUrl());
    _dataTable.search('');
    _dataTable.on('draw', refreshLiveStats);

    refreshLines();
});

new Vue({

    el: '#app',

    data: {
        statusFilter: 'DeliveryNoteSent'
    },
    methods: {
        setStatusFilter: function (e, f) {
            this.statusFilter = f;
            _statusFilter = f;
            _dataTable.draw();
            e && e.preventDefault();
        }
    }
});

function periodChanged() {
    _dataTable.destroy();
    _dataTable = initDeliveryNotesDataTable($('#delivery-notes-datatable'), getDataUrl());
}

function initDeliveryNotesDataTable(dt, dataUrl) {

    var options = getDataTableOptions(dt, true);
    options.ajax = dataUrl;
    options.columns = [
        {
            "data": "Ids",
            "render": function (ids) { return "<a href=\"Job.aspx?id=" + ids.Id + "\">" + ids.InternalId + "</a>"; }
        },
        { "data": "ClientName" },
        {
            "data": "ImportedDescription",
            "render": function (data) {
                if (data.ImportedId && data.ImportedId > 0) {
                    var t = "<a css='text-white' target='_blank' href='Job.aspx?id=" + data.ImportedId + "'><i class='icon-chevron-right'></i><label>" + data.ImportedInternalId + "</label></a>";
                    return t;
                }

                return data.Description;
            }
        },
        { "data": "Total", "render": displayMoney },
        { "data": "Status", "render": getProjectStatusHtml },
        { "data": "DateStr", "type": "date", "render": function (value) { return value ? moment(value, CALANI.DEFAULTS.dateTimeFormat).format('DD.MM.YYYY') : ''; } },
        { "data": "LastUpdateDateStr", "type": "date", "render": function (value) { return value ? moment(value, CALANI.DEFAULTS.dateTimeFormat).format('DD.MM.YYYY') : ''; } },
    ];

    return dt.DataTable(options);
}

function getDataUrl() {
    var dates = getPeriodFilterDates();
    var assigneeId = _assigneeId != undefined && _assigneeId > 0 ? _assigneeId : 0;
    var creatorId = _creatorId != undefined && _creatorId > 0 ? _creatorId : 0;
    var url = "../../API/Service.svc/ListDeliveryNotes?startTicks=" + dates[0] + "&endTicks=" + dates[1]
        + "&assigneeId=" + assigneeId + "&creatorId=" + creatorId;
    return url;
}
