﻿using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.FrontEndModels;
using System;
using System.Collections.Generic;

public partial class Projects_DeliveryNotes_Job : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }



    public string CreateAnOtherQuote
    {
        get
        {
            return Resources.Resource.CreateAnOtherDeliveryNote;
        }
    }

    public string YesCreateNewQuote
    {
        get
        {
            return Resources.Resource.YesCreateNewDeliveryNote;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        JobEditor1.EnableQuoteFields = false;
        JobEditor1.IsAdmin = ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin;
        var mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(ph.CurrentOrganizationId);

        if (!IsPostBack)
        {
            mgr.Load();

            ddlDeliveryNotePdfType.DataSource = new List<UnitModel>()
            {
                new UnitModel { Id = (long)TypeOfDeliveryNotePdfEnum.Unpriced, Name = ResourceHelper.Get(TypeOfDeliveryNotePdfEnum.Unpriced.GetDescription()) },
                new UnitModel { Id = (long)TypeOfDeliveryNotePdfEnum.Priced, Name = ResourceHelper.Get(TypeOfDeliveryNotePdfEnum.Priced.GetDescription()) },
            };

            ddlDeliveryNotePdfType.DataBind();
        }

        ddlDeliveryNotePdfType.SelectedValue = ((sbyte)mgr.DeliveryNotePdfType).ToString();
    }




    public string LabelQuoteOrDeliveryNote
    {
        get
        {
            string title = Resources.Resource.DeliveryNote;
            return title;
        }
    }

    public void Load()
    {
        

        Calani.BusinessObjects.Projects.jobsExtended info = JobEditor1.Info;

        if (info.lastUpdate != null)
        {
            lblLastChange.InnerHtml = Resources.Resource.LastChange + "<br />" + Convert2.DateToStr(info.lastUpdate.Value);
        }
        else
        {
            lblLastChange.InnerHtml = Resources.Resource.NeverSaved;
        }


        if (info.dateQuoteSent != null)
        {
            lblLastSent.InnerHtml = "<i class=\"icon-checkmark\"></i> " + Resources.Resource.LastSend + "<br />" + Convert2.DateToStr(info.dateQuoteSent.Value);
        }
        else
        {
            lblLastSent.InnerHtml = Resources.Resource.NeverSent;
            lblLastSent.Attributes["class"] += " text-muted";
        }

        if (info.dateQuoteRefused != null)
        {
            lblRejectDate.InnerHtml = "<i class=\"icon-cross3\"></i> " + Resources.Resource.MarkAsRejected + "<br />" + Convert2.DateToStr(info.dateQuoteRefused.Value);
        }
        else
        {
            if (info.dateQuoteAccepted == null) lblRejectDate.InnerText = Resources.Resource.NoDecisionYet;
            lblRejectDate.Attributes["class"] += " text-muted";
        }

        if (info.dateQuoteAccepted != null)
        {
            lblAcceptDate.InnerHtml = "<i class=\"icon-checkmark\"></i> " + Resources.Resource.MarkAsAccepted + "<br />" + Convert2.DateToStr(info.dateQuoteAccepted.Value);
        }
        else
        {
            if (info.dateQuoteRefused == null)  lblAcceptDate.InnerText = Resources.Resource.NoDecisionYet;
            lblAcceptDate.Attributes["class"] += " text-muted";
        }
    }
}