﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Projects;

public partial class Projects_DeliveryNotes_Default : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    public string OptionsClients { get; set; }
    public string OptionsPeriod { get; set; }
    public bool RoundAmount { get; set; }

    public bool DeliveryNote
    {
        get
        {
            return true;
        }
    }

    public string LabelQuoteOrDeliveryNote
    {
        get
        {
           return Resources.Resource.DeliveryNote;
        }
    }

    public string PotentialToolTip
    {
        get
        {
            if (!DeliveryNote) 
                return Resources.Resource.Pipe_Potential_Tooltip;
            
            return String.Empty;
        }
    }

    public string PotentialLabel
    {
        get
        {
            if (!DeliveryNote) 
                return Resources.Resource.Pipe_Potential;

            return Resources.Resource.Total;
        }
    }

    public long AssigneeId { get; private set; }
    public long CreatorId { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        var orgMgr = new OrganizationManager(ph.CurrentOrganizationId);
        orgMgr.Load();

        OptionsClients = ph.RenderCustomerListOption();
        OptionsPeriod = ph.RenderPeriodsListOption(preselect: orgMgr.DefaultPeriodFilter);

        RoundAmount = orgMgr.RoundAmount;

        AssigneeId = 0;
        CreatorId = 0;
        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            AssigneeId = ph.CurrentUserId;
            CreatorId = ph.CurrentUserId;
        }
    }

    protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (RoundAmount)
            e.Row.Cells[4].Visible = false;
        else
            e.Row.Cells[3].Visible = false;
    }
}