﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Projects_JobEditor" Codebehind="JobEditor.ascx.cs" %>
<%@ Register Src="~/assets/Controls/RatingStars.ascx" TagPrefix="uc1" TagName="RatingStars" %>
<%@ Register Src="~/assets/Controls/ExpenseApprovedAmountLabel.ascx" TagPrefix="uc1" TagName="ExpenseApprovedAmountLabel" %>
<%@ Import Namespace="Calani.BusinessObjects.Enums" %>
<%@ Import Namespace="Calani.BusinessObjects.Model" %>


<!-- Iconified modal test -->
<div id="modal_workflow" class="modal fade">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h5 class="modal-title"><i class="icon-forward3"></i> &nbsp;<%= Resources.Resource.GoToNextStep %></h5>
		</div>

		<div class="modal-body">



			<h6 class="text-semibold"><i class="icon-check position-left"></i> <%= Resources.Resource.YouHaveJustFinishedStep %></h6>
			<p>
                <%= Resources.Resource.YouHaveJustFinishedStep_d1 %>
                <br />
                <%= Resources.Resource.YouHaveJustFinishedStep_d2 %>
                <a href="javascript:workflowDialogReadMore()" class="modal_workflow_readmore"><%= Resources.Resource.ReadMore %></a>
                
            </p>
            <div class="modal_workflow_readmore" style="display:none;">
                <p>
                    <%= Resources.Resource.YouHaveJustFinishedStep_d3 %>
                </p>
                <p>
                    <%= Resources.Resource.YouHaveJustFinishedStep_d4 %>
			    </p>
            </div>
            <div class="modal_workflow_readmore" style="display:none">
                <a href="javascript:workflowDialogReadMore()"><%= Resources.Resource.ReadLess %></a>
            </div>

			<hr>


            <div>

                <div class="row no-form-group" id="rowWorkflowNextDeliveryNote">
                    <div class="col-xs-6">
                        <div class="checkbox">
							<label>
								<input type="checkbox" class="control-success" id="cbxWorkflowNextDeliveryNote">
								<strong><%= Resources.Resource.DeliveryNote %></strong>
							</label>
						</div>
                    </div>
					<div class="col-xs-6">
                        <div class="input-group" id="ddlWorkflowNextDeliveryNoteContainer" style="display:none">
						    <select class="form-control" id="ddlWorkflowNextDeliveryNote">
                                <option></option>
                            </select>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default btn-icon" onclick="previewNextDeliveryNote()"><i class="icon-eye"></i></button>
                            </span>
                        </div>
					</div>
				</div>

                <div class="row no-form-group" id="rowWorkflowNextInvoice">
                    <div class="col-xs-6">
                        <div class="checkbox">
							<label>
								<input type="checkbox" class="control-success" id="cbxWorkflowNextInvoice">
								<strong><%= Resources.Resource.Invoice %></strong>
							</label>
						</div>
                    </div>
					<div class="col-xs-6">
                        <div class="input-group" id="ddlWorkflowNextInvoiceContainer" style="display:none">
                            <select class="form-control" id="ddlWorkflowNextInvoice">
                                <option></option>
                            </select>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default btn-icon" onclick="previewNextInvoice()"><i class="icon-eye"></i></button>
                            </span>
                        </div>
					</div>
				</div>

                

                <div class="row no-form-group" id="rowWorkflowNextProject">
                    <div class="col-xs-6">
                        <div class="checkbox">
							<label>
								<input type="checkbox" class="control-success" id="cbxWorkflowNextProject">
								<strong><%= Resources.Resource.Project %></strong>
							</label>
						</div>
                    </div>
					<div class="col-xs-6">
                        <div class="input-group" id="ddlWorkflowNextProjectContainer" style="display:none">
						    <select class="form-control" id="ddlWorkflowNextProject" onchange="onNextProjectChanged()">
                                <option></option>
                            </select>
                            
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default btn-icon" onclick="previewNextProject()"><i class="icon-eye"></i></button>
                            </span>
                        </div>
					</div>
				</div>

                <% if (EnableRefNumInheritance)
                    { %>
                <div class="row form-group" id="refNumberContainer" style="display:none">
                    <div class="col-xs-6"></div>
                    <div class="col-xs-6 pt-5">
                        <label><%= Resources.Resource.RefNr %>:</label>
                        <div class="input-group" title="Specify reference number for a new project">
                            <input id="ref-num-prefix" class="form-control" type="number" value="" placeholder="<%= Resources.Resource.EnterNumber %>" />
                            <span id="ref-num-postfix" class="input-group-addon">-<%= tbxInternalId.Text %></span>
                        </div>
                    </div>
                </div>
                <% } %>

                <div class="row no-form-group" id="rowWorkflowNextNone">
                    <div class="col-xs-6">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="control-success" id="cbxWorkflowNextNone">
                                <strong><%= Resources.Resource.None %></strong>
                            </label>
                        </div>
                    </div>
                </div>
                

            </div>

            <% if (IsInvoice == false) {%>

            <div id="rowWorkflowAttachmentsPanel" style="display: none">
                <hr>
                <div class="row no-form-group">                    
                    <div class="col-xs-6">
                        <h6 class="text-semibold"><%= Resources.Resource.Attachments %></h6>
                        
                            <ul class="checkbox" style="list-style-type: none;" id="ulCloneAttachments">
							    <li>
								    <input type="checkbox" class="control-success" id="cbxWorkflowNextIncludeAllAttachments" onclick="checkAllAttachmentCheckboxes()" value="0">
								    <%= Resources.Resource.IncludeAllAttachments %>
							    </li>
                                <% foreach (var a in Attachments)
                                    { %>
                                    <li id="li-attachment-<%= a.Key %>">
								        <input type="checkbox" class="control-success" value="<%= a.Key %>">
								        <%= a.Value %>
							        </li>
                                <% } %>
						    </ul>
                        
                    </div>
                </div>

            </div>

            <% } %>

		</div>

		<div class="modal-footer">
			<button class="btn btn-link" data-dismiss="modal"><i class="icon-cross"></i> <%= Resources.Resource.Close %></button>
			<button class="btn btn-primary" id="btnWorkflowNextCreate" onclick="workflowDialogAction()" style="display:none" data-dismiss="modal"><i class="icon-forward3"></i> <%= Resources.Resource.Create %></button>
		</div>
	</div>
</div>
</div>
<!-- /iconified modal -->

<script type="text/javascript">
    function flagChanges(changes) {
        if (changes) {
            window.onbeforeunload = function () {
                return "<%= Resources.Resource.LeaveWithoutSaveChanges %>";
            };
        }
        else {
            window.onbeforeunload = null;
        }
    }

        function CopyToClipboard(e) {
            var elem = $("#" + "<%= tbxShareLink.ClientID %>");

            elem.select();

            document.execCommand("copy");

            e && e.preventDefault();
        }

    var projectId = <%= ProjetIdForJs %>;
    var currencies = <%= CurrenciesForJs %>;
    var isReadOnlyMode = <%= IsReadOnly %>;
    var dateCreation = '<%= DateCreation %>';
    var delayInvoice = <%= DelayInvoice %>;
    var roundAmount =  '<%= RoundAmount %>'==='True';

    
     function setSendDate(action, lblDate) {


            swal({
                title: "<%= Resources.Resource.SendDate %>",
                html: '<div class="datepickerinswal"><input id="sentAsDatePicker" class="swal2-input" /></div>',
                showConfirmButton: true,
                customClass: 'swal2-datepicker',
                onOpen: function ()
                {
                    $('#sentAsDatePicker').pickadate({
                            singleDatePicker: true,
                            selectYears: true,
        
                            selectMonths: true,
                            format: 'dd/mm/yyyy',
                            zIndex: 999999
                    });
                    $('#sentAsDatePicker').focus();
                }

            }).then(function (result) {
                var date = $('#sentAsDatePicker').val();
                
                if (result.value == true && date != null && date != "") {
                    saveProject(action + ':' + date, null, null);

                    $("#ContentPlaceHolder1_JobEditor1_tbxInvoiceValidityDate").val(date);
                    if (lblDate != null)
                    {

                        $(lblDate).removeClass("shake");
                        $(lblDate).removeClass("animated");
           
                        $(lblDate).html('<i class="icon-checkmark"></i> ' + ressources['LastSend'] + ':<br>' + date);
                        $(lblDate).addClass("shake");
                        $(lblDate).addClass("animated");

                        $(lblDate).removeClass('text-muted');
                        $(lblDate).addClass('text-bold');
                    }
                }
            });


    }

    function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    function setRecColor(color) {
        color = color || getRandomColor();
        $("#colorpad").css("background-color", color);
        $('#<%= hfColor.ClientID %>').val(color);
    }

</script>

<!-- clone project dialog -->
<div id="modal_form_cloneproject" class="modal fade">
	<div class="modal-dialog content">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title"><%= Resources.Resource.Clone %></h5>
			</div>

			<div class="modal-body">

				<p>
                    <%= Resources.Resource.CloneToolIntro %>
				</p>

				<hr>


				<div class="form-group">
					<div class="row">
						<div class="col-sm-6">
							<label><strong><%= Resources.Resource.CreateANew %></strong></label>
							<select class="form-control select" id="ddlCloneTargetType">
                                <option value="quote"><%= Resources.Resource.Quote %></option>
                                <option value="deliverynote"><%= Resources.Resource.DeliveryNote %></option>
                                <option value="project"><%= Resources.Resource.Opened_Job %></option>
                                <option value="invoice"><%= Resources.Resource.Invoice %></option>
                            </select>
						</div>

						
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-sm-12">
							<label><strong><%= Resources.Resource.IncludingDataFrom %></strong></label>
							<input type="text" class="form-control" disabled="disabled" id="tbxCloneSource">
                           
						</div>
					</div>
				</div>
                <div class="form-group">
                    <label class="display-block text-semibold"><%= Resources.Resource.ImportData %></label>
                    
                    <label class="checkbox-inline">
                        <input type="checkbox" id="tbxCloneAttachments" >
                        <%= Resources.Resource.Attachments %>
                    </label>

                    <label class="checkbox-inline">
                        <input type="checkbox" id="tbxCloneLinks">
                        <%= Resources.Resource.LinkedProject %>
                    </label>

                    <label class="checkbox-inline">
                        <input type="checkbox" id="tbxCloneNotes">
                        <%= Resources.Resource.Notes %>
                    </label>
                </div>
				
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.Close %></button>
				<button type="button" class="btn btn-primary btn-save" data-action="clone"><%= Resources.Resource.Clone %></button>
			</div>
		</div>
	</div>
</div>
<!-- /clone project dialog -->  

<!-- delete project dialog -->
<div id="modal_form_deleteproject" class="modal fade">
	<div class="modal-dialog content">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title"><%= Resources.Resource.Delete %></h5>
			</div>

			<div class="modal-body">

				<p>
                    <%= Resources.Resource.DeleteToolIntro %>
				</p>

			
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
				<button type="button" class="btn btn-primary btn-save" data-action="delete"><%= Resources.Resource.Delete %></button>
			</div>
		</div>
	</div>
</div>
<!-- /delete project dialog -->  

<div class="row">
	<div class="col-sm-12 col-md-6">

			<!-- Input group addons -->
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h5 class="panel-title" runat="server" id="h5Title"></h5>
				</div>

				<div class="panel-body">

					<div class="form-horizontal">
						<fieldset class="content-group">

                            <div class="form-group" runat="server" id="divRefNr" data-popup="tooltip" title='<%$ Resources:Resource, RefNr_Tooltip %>' data-placement="right">
								<label class="control-label col-lg-2"><%= Resources.Resource.RefNr %></label>
								<div class="col-lg-10">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa-regular fa-hashtag"></i></span>
										<asp:TextBox Id="tbxInternalId" runat="server" CssClass="form-control" />
									</div>
								</div>
							</div>

                            <div class="form-group">
								<label class="control-label col-lg-2"><%= Resources.Resource.CustomerReference %></label>
								<div class="col-lg-10">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa-solid fa-user-tag"></i></span>
										<asp:TextBox Id="tbxCustomerReference" runat="server" CssClass="form-control" MaxLength="100" />
									</div>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-lg-2"><%= Resources.Resource.Customer %></label>
								<div class="col-lg-10">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa-regular fa-user-circle"></i></span>
										<asp:DropDownList ID="ddlCustomer" runat="server" CssClass="select-search"
                                            DataTextField="companyName" DataValueField="id"
                                            />
									</div>
								</div>
							</div>

                            <script type="text/javascript">
                                var selectedAddressId = <%= SelectedAddressId %>;
                            </script>
							<div class="form-group">
								<label class="control-label col-lg-2"><%= Resources.Resource.Address %></label>
								<div class="col-lg-10">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-industry"></i></span>
										<asp:DropDownList ID="ddlCustomerAddress" runat="server" CssClass="select"
                                            DataTextField="Description" DataValueField="id"
                                            />
									</div>
								</div>
							</div>

                            <script type="text/javascript">
                                var selectedContactId = <%= SelectedContactId %>;
                            </script>
							<div class="form-group">
								<label class="control-label col-lg-2"><%= Resources.Resource.Contact %></label>
								<div class="col-lg-10">
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-user-check"></i></span>
										<asp:DropDownList ID="ddlCustomerContact" runat="server" CssClass="select" 
                                            DataTextField="Description" DataValueField="id"
                                            />
									</div>
								</div>
							</div>
                            <div class="delivery-notes-elements form-group hidden">
                                <script type="text/javascript">
                                    var selectedLinkedJobId = <%= SelectedLinkedJobId %>;
                                </script>
                                <label class="control-label col-lg-2"><%= Resources.Resource.LinkProject %></label>
                            <div class="col-lg-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-cog5"></i></span>
                                        <asp:DropDownList ID="ddlJob" runat="server" CssClass="select" DataTextField="name" DataValueField="value"/>
                            </div>
                            </div>
                            </div>
                            <% if (IsDeliveryNote == false)
                               { %>

                            <div class="form-group">
								<label class="control-label col-lg-2"><%= Resources.Resource.LocationOfIntervention %></label>
								<div class="col-lg-10">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa-sharp fa-solid fa-location-dot"></i></span>
										<asp:TextBox Id="tbxLocationOfIntervention" TextMode="MultiLine" runat="server" CssClass="form-control threelines" />
									</div>
								</div>
							</div>

                            <div class="form-group">
								<label class="control-label col-lg-2"><%= Resources.Resource.TypeOfIntervention %></label>
								<div class="col-lg-10">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa-solid fa-briefcase"></i></span>
										<asp:TextBox Id="tbxTypeOfIntervention" runat="server" CssClass="form-control" MaxLength="100" />
									</div>
								</div>
							</div>
                            <% } %>
							<% if (IsProject){ %>
								<div class="form-group">
                            		<label class="control-label col-lg-2"><%= Resources.Resource.Employees %></label>
                            		<div class="col-lg-10">
                            			<div class="input-group">
											<span class="input-group-addon" ><i class="fa-solid fa-user-group"></i></span>
				                            <select id="msAdditionalEmployees" class="select select-resources2" multiple="multiple" <%= IsAdmin ? "" : "disabled='disabled'" %>></select>
                            			</div>
                            		</div>
	                            </div>
	                        <% } %>
                            <div class="delivery-notes-elements form-group hidden">
								<label class="control-label col-lg-2"><%= Resources.Resource.SendCopyEmailTo %></label>
								<div class="col-lg-10">
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-mail5"></i></span>
										<asp:TextBox Id="tbxSendCopyEmailTo" runat="server" CssClass="form-control" TextMode="Email" /> 
									</div>
								</div>
							</div>
                            
							<div class="form-group">
								<label class="control-label col-lg-2"><%= Resources.Resource.Description %></label>
								<div class="col-lg-10">
									<textarea runat="server" id="tbxDescription" rows="5" class="form-control" placeholder='<%$ Resources:Resource, DescriptionVisibleOnDocument %>'></textarea>
								</div>
							</div>
                            
							<div class="form-group" style="display: none;">
								<label class="control-label col-lg-2"><%= Resources.Resource.MeetingDescription %></label>
								<div class="col-lg-10">
									<textarea runat="server" id="tbxMeetingDescription" rows="3" class="form-control" placeholder='<%$ Resources:Resource, DescriptionVisibleOnMeeting %>'></textarea>
								</div>
							</div>

                            <div class="form-group" runat="server" id="divQuoteField_dateQuoteValidity" data-popup="tooltip" title='<%$ Resources:Resource, ValidityDate_Tooltip %>'  data-placement="right">
								<label class="control-label col-lg-2"><%= Resources.Resource.ValidityDate %></label>
								<div class="col-lg-10">
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-calendar"></i></span>
                                        <input type="text" id="tbxValidityDate" runat="server" 
                                               class="form-control daterange-single" />
									</div>
								</div>
							</div>

                            <div class="form-group" runat="server" id="divQuoteField_dateInvoiceValidity" data-popup="tooltip" title='<%$ Resources:Resource, InvoiceValidityDate_Tooltip %>'  data-placement="right">
								<label class="control-label col-lg-2"><%= Resources.Resource.InvoiceValidityDate %></label>
								<div class="col-lg-10">
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-calendar"></i></span>
                                        <input type="text" id="tbxInvoiceValidityDate" runat="server" 
                                               class="form-control daterange-single" />
									</div>
								</div>
							</div>


                            <div class="form-group" runat="server" id="divQuoteField_rating" data-popup="tooltip" title='<%$ Resources:Resource, QuoteRating_Tooltip %>'  data-placement="right">
								<label class="control-label col-lg-2"><%= Resources.Resource.QuoteRating %></label>
								<div class="col-lg-10">
									<div class="input-group">
										<span class="input-group-addon"><i class="fa fa-star-half-empty"></i></span>
                                        <div class="form-control">
                                            <uc1:RatingStars runat="server" ID="ratingStarsQuote" />
                                        </div>
									</div>
								</div>
							</div>




                            <div class="form-group" runat="server" id="divProject_Deadline" data-popup="tooltip" title='<%$ Resources:Resource, ProjectDeadline_Tooltip %>'  data-placement="right">
								<label class="control-label col-lg-2"><%= Resources.Resource.ProjectDeadline %></label>
								<div class="col-lg-10">
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-calendar"></i></span>
                                        <input type="text" id="tbxProjectDeadLine" runat="server" 
                                               class="form-control daterange-single" />
									</div>
								</div>
							</div>
                           
                            <div class="form-group" runat="server" id="divProject_Visits" data-popup="tooltip" title='<%$ Resources:Resource, ProjectVisitsForfait_Tooltip %>'  data-placement="right">
                                <label class="control-label col-lg-2"><%= Resources.Resource.ProjectVisitsForfait %></label>
								<div class="col-lg-10">
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-pie-chart5"></i></span>
                                        <input type="number" id="tbxProjectVisitsForfait" runat="server" 
                                               class="form-control" />
									</div>
								</div>                                
							</div>

                            <div runat="server" id="divProject_VisitsStatus">
                                    <div class="progress">
                                        <div class="progress-bar bg-success-400" 
                                            data-popup="tooltip" runat="server" id="divProject_VisitsStatus_cursor">
                                        </div>
                                    </div>
                                    <div> 
                                        <asp:Label runat="server" ID="lblVisitsStatusLeft" />
                                    </div>

                            </div>
                            
                            <div class="form-group" runat="server" id="divProject_Currency" data-popup="tooltip" title='<%$ Resources:Resource, ForeignCurrencies %>'  data-placement="right">
								<label class="control-label col-lg-2"><%= Resources.Resource.Currencies %></label>
								<div class="col-lg-10">
									<div class="input-group">
										<span class="input-group-addon"><i class="icon-coin-dollar"></i></span>
                                        
                                            <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="select" 
                                            DataTextField="description" DataValueField="id"
                                            />
                                        
									</div>
								</div>                               
							</div>

                            <script type="text/javascript">
                                var selectedAssigneeId = <%= SelectedAssigneeId %>;
                            </script>
                            <div class="form-group" runat="server" id="divDeliveryNotesField_assignee">
                                
                                <label class="control-label col-md-2"><%= Resources.Resource.AssignedTo %></label>
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-user-plus"></i></span>
                                        <asp:DropDownList ID="ddlAssignee" runat="server" CssClass="select" AppendDataBoundItems="True" DataTextField="description" DataValueField="id">
                                            <asp:ListItem Value ="" Text="<%$ Resources:Resource, Select___ %>"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" runat="server" id="divDeliveryNotesField_deliveryDate">
                                <label class="control-label col-md-2"><%= Resources.Resource.DeliveryAt %></label>
                                <div class="col-md-10">
                                    
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-calendar3"></i></span>
                                        <input type="text" class="form-control datetimepicker-h" runat="server" id="tbxDeliveryDate"  value="" readonly=""/>
                                    </div>
                                </div>
                            </div>

                            <div class="delivery-notes-elements form-group hidden" >
                                    <label class="control-label col-lg-2"><%= Resources.Resource.Comment %></label>
								<div class="col-lg-10">
									<textarea runat="server" id="tbxComment" rows="3" class="form-control" placeholder='<%$ Resources:Resource, EnterYourMessage %>' ></textarea>
								</div>
							</div>

                             <div class="form-group" runat="server" id="divShareLink" data-popup="tooltip" title='<%$ Resources:Resource, ProjectShareLink_Tooltip %>'  data-placement="right">
								<label class="control-label col-lg-2"><%= Resources.Resource.ViewLink %></label>
								<div class="col-lg-10">

                                    <div class="input-group" runat="server">
                                                    <span class="input-group-addon"><i class="icon-share3"></i></span>
                                                   <input type="text" id="tbxShareLink" runat="server" class="form-control " />
                                                    <span class="input-group-addon">
                                                        <a href="" onclick="return CopyToClipboard(event)"><%= Resources.Resource.Copy %></a>
                                                    </span>
                                                </div>

								</div>
							</div>



						</fieldset>




					</div>
				</div>
			</div>
			<!-- /input group addons -->


<% if((IsProject || IsReadOnly.ToLowerInvariant() == "true") && ExpectedHours > 0) { %>
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title"><%= Resources.Resource.HoursSummary %></h6>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
					</ul>
				</div>
			</div>
			<div class="panel-body">
                <ul>
                    <li><label><%= Resources.Resource.ExpectedHours %>:&nbsp;</label><span><%= ExpectedHours %></span></li>
                    <li><label><%= Resources.Resource.InvoicedHours %>:&nbsp;</label><span><%= InvoicedHours %></span></li>
                    <li><label><%= Resources.Resource.AwaitingImportHours %>:&nbsp;</label><span><%= AwaitingImportHours %></span></li>
                    <li><label><%= Resources.Resource.RemainingHours %>:&nbsp;</label><span><%= (ExpectedHours - InvoicedHours - AwaitingImportHours > 0) ? (ExpectedHours - InvoicedHours - AwaitingImportHours) : 0 %></span></li>
                </ul>
            </div>
		</div>
<% } %>

	</div>


    <div class="col-sm-12 col-md-6" runat="server" id="panelNotes">
        <div class="panel panel-flat" runat="server" id="Div1">
			<div class="panel-heading">
				<h6 class="panel-title"><%= Resources.Resource.Notes %></h6>
				<div class="heading-elements">
                    <span class="badge bg-primary heading-text" id="lblCountNotes" runat="server"></span>
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
					</ul>
				</div>
            </div>
            <div class="panel-body">
							<ul class="media-list chat-stacked content-group"  id="panelNotes" style="max-height:320px">
								
                                <!--
								<li class="media">
									<div class="media-left"><img src="../../assets/images/placeholder.jpg" class="img-circle img-md" alt=""></div>
									<div class="media-body">
										<div class="media-heading">
											<a href="#" class="text-semibold">Margo Baker</a>
											<span class="media-annotation pull-right">24/04/18 10:12</a></span>
										</div>
										Goldfish indisputable vexed hello on held some gosh :-)
									</div>
								</li>
                                -->
								
							</ul>

	                    	<textarea name="enter-message" class="form-control content-group" rows="3" cols="1" placeholder='<%$ Resources:Resource, EnterYourMessage %>' runat="server" id="tbxNewNote"></textarea>

	                    	<div class="row" runat="server" id="btnAddNoteContainer">
	                    		

	                    		<div class="col-xs-12 text-right">
		                            <button id="btnAddNote" type="button" class="btn btn-labeled btn-labeled-right"><b><i class="icon-circle-right2"></i></b> <%= Resources.Resource.SendNote %></button>
	                    		</div>
	                    	</div>
						</div>
        </div>
        
    </div>
    
    

    <div class="col-sm-12 col-md-6" runat="server" id="panelAttachments">


        <!-- panel parent -->
        <div class="panel panel-flat" runat="server" id="divLinkedProject">
			<div class="panel-heading">
				<h6 class="panel-title"><asp:Label runat="server" ID="lblLinkedProject" Text="Linked projects" /></h6>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
					</ul>
				</div>



			</div>

			<div class="panel-body">

                <asp:Repeater ID="repeaterImportedInList" runat="server">
                    <ItemTemplate>
                        <p>
                            <span class="label label-primary">
                                <asp:HyperLink runat="server" NavigateUrl='<%# Eval("Tag2") %>' Target="_blank" CssClass="text-white" >
                                    <i class="icon-new-tab"></i> 
                                    <asp:Label runat="server" Text='<%# Eval("internalId") %>' />
                                </asp:HyperLink>
                            </span> : 
                
                            
                            <asp:Label runat="server" Text='<%# Eval("Tag1") %>' CssClass="label label-default" />

                            <%= Resources.Resource.Amount %>

                            <asp:Label runat="server" Text='<%# ToTotal(Eval("total") as double?) %>' Font-Bold="true" />


                        </p>
                    </ItemTemplate>
                </asp:Repeater>
                

                <asp:Panel ID="panelSingleParent" runat="server" Visible="false">
                    <span class="label label-primary">
                        <asp:HyperLink runat="server" NavigateUrl="#" Target="_blank" CssClass="text-white" ID="hlParentProject" Enabled='<%# IsReadOnly== "false" %>'>
                            <i class="icon-new-tab"></i> 
                            <asp:Label runat="server" ID="lblParentName" />
                        </asp:HyperLink>
                    </span> : 
                
                
                     
                    <asp:Label runat="server" ID="lblParentType" CssClass="label label-default" />

                    <%= Resources.Resource.PreviouslySentForAValueOf %>

                    <asp:Label runat="server" ID="lblParentValue" Font-Bold="true" />
                </asp:Panel>
            </div>
        </div>


        <!-- panel child -->
        <div class="panel panel-flat" runat="server" id="divChildrenProject">
			<div class="panel-heading">
				<h6 class="panel-title"><asp:Label runat="server" ID="lblParentProject"><%= Resources.Resource.ChildrenProjects %></asp:Label></h6>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
					</ul>
				</div>



			</div>

			<div class="panel-body">



                <asp:Repeater ID="repeaterChildren" runat="server">
                    <ItemTemplate>
                        <p>
                            <span class="label label-primary">
                                <asp:HyperLink runat="server" NavigateUrl='<%# Eval("Tag2") %>' Target="_blank" CssClass="text-white" Enabled='<%# IsReadOnly== "false" %>'>
                                    <i class="icon-new-tab"></i> 
                                    <asp:Label runat="server" Text='<%# Eval("internalId") %>' />
                                </asp:HyperLink>
                            </span> : 
                
                            
                            <asp:Label runat="server" Text='<%# Eval("Tag1") %>' CssClass="label label-default" />

                            <%= Resources.Resource.Amount %>

                            <asp:Label runat="server" Text='<%# ToTotal(Eval("total") as double?) %>' Font-Bold="true" />


                        </p>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>

        <div class="panel panel-flat" runat="server" id="divPartInvoices">
			<div class="panel-heading">
				<h6 class="panel-title"><%= Resources.Resource.PartInvoices %></h6>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
					</ul>
				</div>



			</div>

			<div class="panel-body">

                <asp:GridView runat="server" ID="repeaterPartialInvoices" AutoGenerateColumns="false" ShowHeader="false" >
                    
                    <Columns>
                        
                        <asp:TemplateField>
                            <ItemTemplate>
                                <span class="label label-primary">
                                <asp:HyperLink runat="server" NavigateUrl='<%# Bind("url") %>' Target="_blank" CssClass="text-white" ID="link" Enabled='<%# IsReadOnly== "false" %>'>
                                    <i class="icon-new-tab"></i> 
                                    <asp:Label runat="server" ID="lblName" Text='<%# Bind("internalId") %>' />
                                </asp:HyperLink></span> 
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:BoundField DataField="total" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right" DataFormatString="{0:F2}" />
                        <asp:BoundField DataField="dateInvoicing"  ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right" DataFormatString="{0:dd.MM.yy}" />
                    </Columns>

                </asp:GridView>

                


            </div>
        </div>
    
        <div class="panel panel-flat" runat="server" id="panelAttachments_panel">
			<div class="panel-heading">
				<h6 class="panel-title"><%= Resources.Resource.Attachments %></h6>
				<div class="heading-elements">
                    <span class="badge bg-primary heading-text" id="lblAttachmentsCount" runat="server"></span>
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
					</ul>
				</div>

			</div>

			<div class="panel-body">

                <div class="tabbable">
					<ul class="nav nav-tabs nav-tabs-highlight">
						<li class="active" runat="server" id="liAttachments"><a href="#ContentPlaceHolder1_JobEditor1_docstabAttachments" data-toggle="tab"><%= Resources.Resource.Attachments %></a></li>
                        <li runat="server" id="liHistory"><a href="#ContentPlaceHolder1_JobEditor1_docstabHistory" data-toggle="tab"><%= Resources.Resource.History %></a></li>
                        <li runat="server" id="liNewAttachment"><a href="#ContentPlaceHolder1_JobEditor1_docstabNew" data-toggle="tab"><%= Resources.Resource.Add_new %></a></li>
					</ul>

					<div class="tab-content">
						<div class="tab-pane active" id="docstabAttachments" runat="server">
							
                            <p class="small text-justify">
                                <%= Resources.Resource.AttachmentsDocuments_Explain %>
							</p>

                            <asp:GridView runat="server" ID="gridFilesAttachments" AutoGenerateColumns="False" ShowHeader="False">
                                <Columns>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" id="lnk" NavigateUrl='<%# "../DownloadDocument.ashx?id=" + Eval("id") + "&orgId=" + Eval("organizationID") + "&t=" + Eval("FileNameNoExtension") %> ' >
                                                <asp:Label runat="server" id="ico" CssClass='<%# Bind("Icon") %>'></asp:Label>
                                                &nbsp;
                                                <asp:Label ID="lbl" runat="server" Text='<%# Bind("NameWithExtension") %>'></asp:Label>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="date" HeaderText="Date" />
                                    <asp:TemplateField ItemStyle-Wrap="false" >
                                        <ItemTemplate>
                                            <asp:LinkButton Visible='<%# IsReadOnly== "false" %>' runat="server" id="lnk" href='<%# SendPage + "?id=" + ProjectId + "&sel=" + Eval("id") %>' data-popup="tooltip" title='<%$ Resources:Resource, SendByEmail %>'>
                                                <asp:Label runat="server" id="ico" CssClass='icon-envelop' ></asp:Label>
                                            </asp:LinkButton>

                                            <asp:LinkButton Visible='<%# IsReadOnly== "false" %>' runat="server" href="javascript:void(0)" id="lnkd" class="btnDeleteAttachment" data-attachment='<%#  Eval("id") %>' data-name='<%#  Eval("NameWithExtension") %>' data-popup="tooltip" title='<%$ Resources:Resource, Delete %>'>
                                                <asp:Label runat="server" id="icod" CssClass='icon-trash text-danger'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                    
				            </asp:GridView>

						</div>

						<div class="tab-pane" id="docstabHistory" runat="server">
							<p class="small text-justify">
                                <%= Resources.Resource.HistoryDocuments_Explain %>
							</p>
                            <asp:GridView runat="server" ID="gridFilesHistory" AutoGenerateColumns="False" ShowHeader="False">
                                <Columns>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" id="lnk" NavigateUrl='<%# "../DownloadDocument.ashx?id=" + Eval("id") + "&orgId="+Eval("organizationID") + "&t=" + Eval("FileNameNoExtension") %>'>
                                                <asp:Label runat="server" id="ico" CssClass='<%# Bind("Icon") %>'></asp:Label>
                                                &nbsp;
                                                <asp:Label ID="lbl" runat="server" Text='<%# Bind("NameWithExtension") %>'></asp:Label>
                                            </asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="date" HeaderText="Date" />
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:LinkButton Visible='<%# IsReadOnly== "false" %>' runat="server" id="lnk" href='<%# SendPage + "?id=" + ProjectId + "&sel=" + Eval("id") %>' data-popup="tooltip" title='<%$ Resources:Resource, SendByEmail %>'>
                                                <asp:Label runat="server" id="ico" CssClass='icon-envelop'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                    
				            </asp:GridView>

						</div>

                        <div class="tab-pane" id="docstabNew" runat="server">
                            <asp:HiddenField runat="server" ID="hfFile" />
                            <div id="dropzone_multiple" class="fallback dropzone dz-clickable">
                              
                            </div>
                            
						</div>
					</div>
				</div>
			</div>
		</div>
    
    </div>

    <div class="col-sm-12 col-md-6">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title"><%= Resources.Resource.Color %></h6>
            </div>
            <div class="panel-body">
                <asp:HiddenField runat="server" ID="hfColor" />
                <button type="button" id="colorpad" class="btn btn-float btn-rounded" onclick="setRecColor()" style="background-color:<%= hfColor.Value %>"></button>
            </div>
        </div>
    </div>

<div class="col-sm-12 col-md-6" runat="server" id="Div2" >
     <div class="panel panel-flat" runat="server" id="divExpenses">
        <div class="panel-heading">
            <h6 class="panel-title"><%= Resources.Resource.Expenses %></h6>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                </ul>
            </div>



        </div>

        <div class="panel-body">
            <asp:Label runat="server" ID="lblNoData" Visible="false">
                <%= Resources.Resource.NoExpensesYet %>
            </asp:Label>
            <asp:GridView runat="server" ID="gridExpenses" AutoGenerateColumns="false">
                <Columns>
                    <asp:HyperLinkField DataTextField="id" HeaderText="#" DataNavigateUrlFields="id"  ItemStyle-CssClass="text-nowrap p-5" DataNavigateUrlFormatString="~/Costs/Expenses/Edit.aspx?id={0}" />
                    <asp:BoundField DataField="employeeName" HeaderText="Employee"  ItemStyle-CssClass="p-5" HtmlEncode="false"  />
                    <asp:BoundField DataField="date" HeaderText="Date" HtmlEncode="false"  ItemStyle-CssClass="p-5"/>
                    <asp:BoundField DataField="description" HeaderText="Description"   ItemStyle-CssClass="p-5"/>
                    <asp:BoundField DataField="price" HeaderText="Amount" ItemStyle-CssClass="text-right text-nowrap p-5" DataFormatString="{0:F2}" HtmlEncode="false" HeaderStyle-CssClass="text-right" />
                    <asp:BoundField DataField="currency" HeaderText="Currency"  ItemStyle-CssClass="text-nowrap p-5" />
                    <asp:TemplateField HeaderText="ApprovedAmount" ItemStyle-CssClass="text-right text-nowrap p-5" HeaderStyle-CssClass="text-right" >
                        <ItemTemplate>
                            <uc1:ExpenseApprovedAmountLabel runat="server" ItemStyle-CssClass="text-nowrap p-5" ID="ExpenseApprovedAmountLabel" Amount='<%# Bind("approvedAmount") %>' Approved='<%# Bind("approved") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div><!-- panel-body -->
         <div class="panel-footer no-padding" runat="server" id="Div3">
             <div class="row text-right" style="margin:10px" >
                        
                 <a class="btn btn-default" href="<%= this.ResolveUrl("~/Costs/Expenses/Edit.aspx?pid=" + ProjectId) %>">
                     <span class='icon-plus2'></span>&nbsp;<span><%= Resources.Resource.Add_new_Expense %></span>
                 </a>
             </div>
         </div>
    </div>
</div>
<div class="col-sm-12 col-md-6" runat="server" id="panelReports" >

        
        <div class="panel panel-flat" runat="server">
			<div class="panel-heading">
				<h6 class="panel-title"><%= Resources.Resource.Reports%></h6>
				<div class="heading-elements">
                    <span class="badge bg-primary heading-text" id="lblREportsCount" runat="server"></span>
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
					</ul>
				</div>

			</div>

			<div class="panel-body">
					<div class="tab-content">
						<div class="tab-pane active" >
							
                            <p class="small text-justify">
                                <%= Resources.Resource.ReportsDocuments_Explain %>:
							</p>

                            <asp:GridView runat="server" ID="gridReports" AutoGenerateColumns="False" ShowHeader="True">
                                <Columns>
                                    <asp:TemplateField HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server"  Visible='<%# IsReadOnly== "false" %>' id="lnk" 
                                                           NavigateUrl='<%# "../Projects/WorkReport/ReportDetails.aspx?pid=" + Eval("projectId")+"&rid="+ Eval("id") %>'>
                                                
                                                <asp:Label ID="lbl" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                                            </asp:HyperLink>
                                            
                                            <asp:HyperLink runat="server"  Visible='<%# IsReadOnly== "true" %>' id="HyperLink1" 
                                                           NavigateUrl='<%# "../DownloadDocument.ashx?rid=" + Eval("id") + "&orgId="+Eval("OrgId") + "&t=" + Eval("FileNameNoExtension") %>'>
                                                
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                                            </asp:HyperLink>
                                            
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="UpdatedAt" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}" />
                                    
                                    <asp:TemplateField HeaderText="Status" >
                                        <ItemTemplate>
                                            <span><i class='<%# (int)Eval("Status") == 1 ? "icon-checkmark-circle2 text-success" : "icon-close2 text-danger" %>'></i></span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
				            </asp:GridView>

						</div>

					</div>

			</div>
            <div class="panel-footer no-padding" runat="server" id="btnAddReportContainer">
                    <div class="row text-right" style="margin:10px" >
                        
                        <a class="btn btn-default" href="<%= this.ResolveUrl("WorkReport/ReportDetails.aspx?pid=" + ProjectId) %>">
                            <span class='icon-plus2'></span>&nbsp;<span><%= Resources.Resource.Add_Report %></span>
                        </a>
                    </div>
            </div>
		</div>
    </div>

					
    <div class="col-sm-12 col-md-6" runat="server" id="panelDeliveryNotes" >
        <div class="panel panel-flat" runat="server">
			<div class="panel-heading">
				<h6 class="panel-title"><%= Resources.Resource.DeliveryNote%></h6>
				<div class="heading-elements">
                    <span class="badge bg-primary heading-text" id="lblDeliveryNotesCounts" runat="server"></span>
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
					</ul>
				</div>
			</div>

			<div class="panel-body">
				<div class="tab-content">
					<div class="tab-pane active" >
                        <p class="small text-justify">
                            <%= Resources.Resource.DeliveryNotes %>:
						</p>
                        <asp:GridView runat="server" ID="gridDeliveryNotes" AutoGenerateColumns="False" ShowHeader="True">
                            <Columns>
                                <asp:TemplateField HeaderText="RefNr">
                                    <ItemTemplate>
                                        <asp:HyperLink runat="server" id="lnk"  
                                            NavigateUrl='<%# "DeliveryNotes/Job.aspx?id=" + Eval("id")%>'>
											<%# Eval("id") %>
                                        </asp:HyperLink>   
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="date" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}" />
                                <asp:TemplateField HeaderText="Status" >
                                    <ItemTemplate>
										<asp:Label ID="lbStatus" runat="server" Text='<%# GetStatusText(Eval("ProjectStatus")) %>'></asp:Label>
	                                </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
				        </asp:GridView>
					</div>
				</div>
			</div>
            <div class="panel-footer no-padding" runat="server" id="Div5">
                <div class="row text-right" style="margin:10px" >
                    <a class="btn btn-default" href="<%= this.ResolveUrl("DeliveryNotes/Job.aspx?pid=" + ProjectId) %>">
                        <span class='icon-plus2'></span>&nbsp;<span><%= Resources.Resource.Add_DeliveryNote %></span>
                    </a>
                </div>
            </div>
		</div>
    </div>

<% if (IsProject){ %>
	<div class="col-sm-12 col-md-6" runat="server" id="panelTaskLists" >
        <div class="panel panel-flat" runat="server">
			<div class="panel-heading">
				<h6 class="panel-title"><%= Resources.Resource.MenuList_TaskLists%></h6>
				<div class="heading-elements">
                    <span class="badge bg-primary heading-text" id="lblTaskListsCounts" runat="server"></span>
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
					</ul>
				</div>
			</div>

			<div class="panel-body">
				<div class="form-horizontal">
					<div class="row">
					    <div class="form-group" style="margin-left: 0 !important">
							<label class="control-label col-lg-2"><%= Resources.Resource.LinkTaskList %></label>
	                        <div class="col-lg-10">
	                            <div class="input-group" id="msTemplateListIdsGroup">
                    				<span class="input-group-addon" ><i class="icon-list3"></i></span>
	                                <select id="msTemplateListIds" class="select select-resources2" multiple="multiple"></select>
	                            </div>
	                        </div>
	                    </div>
					</div>
					
					<div class="tab-content">
						<div class="tab-pane active" >
							
							<asp:Label ID="tasksExistsLabel" CssClass="small text-justify" runat="server"></asp:Label>
							<asp:Label ID="noTasksLabel" CssClass="small text-justify" runat="server"></asp:Label>
	                        <asp:GridView runat="server" ID="gridTaskLists" AutoGenerateColumns="False" ShowHeader="True">
	                            <Columns>
		                            <asp:HyperLinkField DataTextField="internalId" DataNavigateUrlFields="id,jobId" DataNavigateUrlFormatString="./../TaskList/TaskList/AddEdit.aspx?id={0}&projectId={1}"/>
		                            <asp:BoundField DataField="date" DataFormatString="{0:dd/MM/yyyy HH:mm}"/>
		                            <asp:TemplateField><ItemTemplate><%# Eval("contacts.firstName") + " " + Eval("contacts.lastName") %></ItemTemplate></asp:TemplateField>
	                            </Columns>
					        </asp:GridView>
						</div>
					</div>
				</div>
			</div>
            <div class="panel-footer no-padding" runat="server" id="Div23">
                <div class="row text-right" style="margin:10px" >
                    <a class="btn btn-default" href="../../TaskList/TaskList/AddEdit.aspx?projectId=<%=ProjectId%>">
                        <span class='icon-plus2'></span>&nbsp;<span><%= Resources.Resource.Add_new %></span>
                    </a>
                </div>
            </div>
		</div>
    </div>			
<% } %>

</div>

<div class="row">
		<div class="col-sm-12 col-md-12">

			<div class="panel panel-flat">
					<div class="panel-heading">
						<h5 class="panel-title"><%= Resources.Resource.Items_Services_Products %></h5>
					</div>

					<div class="panel-body">
                        <asp:Panel runat="server" ID="PanelImportTimeSheets">
                            
                        </asp:Panel>

                        <asp:Panel runat="server" ID="PanelImportExpenses">
                            
                        </asp:Panel>

                        <div class="alert alert-warning alert-bordered" id="alertReminderCurrency" style="display:none">
							<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
							<span class="text-semibold"><%=Resources.Resource.Reminder  %>!</span> 
                            <span id="lblReminderCurrency"></span>
						</div>

						<div class="hot-container">
                            
							<div id="hot_ac_lazy"></div>
                            <style type="text/css">
                                #hot_ac_lazy .handsontable td.htInvalid {
                                    background-color: transparent !important;
                                }
                            </style>
						</div>

					</div>

                <div class="panel-footer no-padding" id="panelAddLine" runat="server">
                    <div class="col-sm-6 col-md-6">
                        <div class="row" style="margin:10px">
						        <button id="btnInvoiceSelection" type="button" class="btn btn-primary btn-save" data-action="invoice-part" style="display:none;"><i class="icon-file-check2 position-left"></i> <%= Resources.Resource.InvoiceSelection %>
                                    <span id="btnInvoiceSelectionCount" />
						        </button>
						        <button id="btnDeliveryNoteSelection" type="button" class="btn btn-info" onclick="openDeliveryNoteDialog()" style="display:none;"><i class="icon-box position-left"></i> <%= Resources.Resource.CreateDeliveryNote %>	
							        <span id="btnDeliveryNoteSelectionCount" />
							    </button>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
					        <div class="row text-right" style="margin:10px">
						        <button id="btnAddLine" type="button" class="btn btn-default"><i class="icon-plus2 position-left"></i> <%= Resources.Resource.Add_line %></button>
					        </div>
                    </div>
                </div>


                    <div class="panel-footer">
                        
                        <div class="form-horizontal">
							<fieldset class="content-group no-margin" style="padding-top:10px">


                                <div class="form-group" runat="server" id="RowStillToInvoice">
									<label class="control-label col-lg-6 text-right"><%= Resources.Resource.StillToInvoice %></label>
									<div class="col-lg-5">
										<div class="input-group" data-popup="tooltip"  data-placement="left" title='<%$ Resources:Resource, BillTotalDescription %>' runat="server">
											<span class="input-group-addon"><i class="icon-balance"></i></span>
											<input type="text" value="" id="tbxStillToInvoice" step="0.01" class="form-control" placeholder="Still to invoice" disabled="disabled" >
										</div>
									</div>
								</div>

                                <div class="form-group" id="panelMainDiscount" runat="server">
									<label class="control-label col-lg-6 text-right"><%= Resources.Resource.Discount %></label>													
									<div class="col-lg-5">
										<div class="input-group" data-popup="tooltip" data-placement="left" title='<%$ Resources:Resource, DiscountDescription %>' runat="server">
											<span class="input-group-addon"><i class="icon-percent"></i></span>
                                            <input type="number" step="0.01" runat="server" value="" class="form-control" placeholder="Discount" id="tbxDiscount"/>
										</div>
									</div>
								</div>


								<div class="form-group">
									<div id="totalQuantityGroup" runat="server">
									    <label class="control-label col-lg-3 text-right"><%= Resources.Resource.TotalQuantity %></label>
	                                    <div class="col-lg-2 text-right">
	                                        <div class="input-group" data-popup="tooltip"  data-placement="left" title='<%$ Resources:Resource, TotalQuantity %>' runat="server">
                                        		<span class="input-group-addon"><i class="icon-calculator2"></i></span>
                                        		<input type="text" step="0.01" value="" class="form-control" placeholder="Total quantity" disabled="disabled" id="tbxTotalItemsQuantity" >
	                                            <input type="hidden" id="hdnTotalItemsQuantity" />
	                                        </div>
	                                    </div>
									</div>
									<asp:Panel runat="server" ID="OnDisabledUsePriceIncludingVat1">
                                       <label id="lblTotalHt" class="control-label col-lg-6 text-right" runat="server"><%= Resources.Resource.SubTotal %> HT</label>
	                                        <div class="col-lg-5 text-right">
                                           		<div class="input-group" data-popup="tooltip"  data-placement="left" title='<%$ Resources:Resource, SubTotalDescription %>' runat="server">
                                           			<span class="input-group-addon"><i class="icon-calculator2"></i></span>
                                           			<input type="text" step="0.01" value="" class="form-control" placeholder="Total price" disabled="disabled" id="btnTotalExclTax" >
	                                                   <input type="hidden" id="hdnTotalExclTaxBeforeDiscount" />
                                           		</div>
	                                        </div>                     
                                    </asp:Panel>
								</div>

                                <%--Discounts / Rebaits--%>

								<asp:Panel runat="server" ID="OnDisabledUsePriceIncludingVat2">           
	                                <div class="form-group">
	                                    <span class="col-lg-6 text-right">
	                                        <select id="ddlDiscountType" class="form-control discount-type" style="width: auto;">
	                                            <option value="<%= (int)TypeOfDiscountEnum.Discount %>" <%= DiscountType == TypeOfDiscountEnum.Discount ? "selected" : "" %>><%= Resources.Resource.Discount_option %></option>
	                                            <option value="<%= (int)TypeOfDiscountEnum.Rebate %>" <%= DiscountType == TypeOfDiscountEnum.Rebate ? "selected" : "" %>><%= Resources.Resource.Rebate_option %></option>
	                                            <option value="<%= (int)TypeOfDiscountEnum.Reduction %>"  <%= DiscountType == TypeOfDiscountEnum.Reduction ? "selected" : "" %>><%= Resources.Resource.Reduction_option %></option>
										    </select>
	                                    </span>
	                                    
										<div class="col-lg-5 text-left">
	                                        <div class="input-group discount-feature-container" id="discount-feature-container" data-popup="tooltip" data-placement="left" title=''>
	                                            <span class="input-group-addon" style=""><i class="fa-light fa-percent"></i></span>
											    <input id="tbxDiscountPercent" value="<%= DiscountPercent %>" type="number" min="0" max="100" class="form-control discount-percent" style="width: 70px;"/>
	                                            <input id="hdnDiscountPercentValidated" type="hidden"  value="<%= DiscountPercent %>" />

	                                            <div style="float: right">                                                
	                                                <label style="display:inline-block;padding-top:7px;padding-right:5px;float:left;">soit</label>
	                                                <input id="tbxDiscountAmount" type="text" disabled="disabled" class="form-control discount-amount" style="display: inline;width:200px" />
	                                            </div>
	                                            
	                                        </div>                                        
										</div>
									</div>
								
	                                <div class="form-group additional-discount-container <%= DiscountPercent2 > 0 ? string.Empty : "collapse" %>">
	                                    <span class="col-lg-6 text-right">
	                                        <select id="ddlDiscountType2" class="form-control discount-type" style="width: auto;">
	                                            <option value="<%= (int)TypeOfDiscountEnum.Discount %>" <%= DiscountType2 == TypeOfDiscountEnum.Discount ? "selected" : "" %>><%= Resources.Resource.Discount_option %></option>
	                                            <option value="<%= (int)TypeOfDiscountEnum.Rebate %>" <%= DiscountType2 == TypeOfDiscountEnum.Rebate ? "selected" : "" %>><%= Resources.Resource.Rebate_option %></option>
	                                            <option value="<%= (int)TypeOfDiscountEnum.Reduction %>"  <%= DiscountType2 == TypeOfDiscountEnum.Reduction ? "selected" : "" %>><%= Resources.Resource.Reduction_option %></option>
										    </select>
	                                    </span>
	                                    
										<div class="col-lg-5 text-left">
	                                        <div class="input-group discount-feature-container" id="discount-feature-container2" data-popup="tooltip" data-placement="left" title=''>
	                                            <span class="input-group-addon" style=""><i class="fa-light fa-percent"></i></span>
											    <input  id="tbxDiscountPercent2" value="<%= DiscountPercent2 %>" type="number" min="0" max="100" class="form-control discount-percent" style="width: 70px;"/>
	                                            <input id="hdnDiscountPercentValidated2" type="hidden"  value="<%= DiscountPercent2 %>" />

	                                            <div style="float: right">                                                
	                                                <label style="display:inline-block;padding-top:7px;padding-right:5px;float:left;">soit</label>
	                                                <input id="tbxDiscountAmount2" type="text" disabled="disabled" class="form-control discount-amount" style="display: inline;width:200px" />
	                                            </div>
	                                            
	                                        </div>                                        
										</div>
									</div>

	                                <div class="form-group additional-discount-container <%= DiscountPercent3 > 0 ? string.Empty : "collapse" %>">
	                                    <span class="col-lg-6 text-right">
	                                        <select id="ddlDiscountType3" class="form-control discount-type" style="width: auto;">
	                                            <option value="<%= (int)TypeOfDiscountEnum.Discount %>" <%= DiscountType3 == TypeOfDiscountEnum.Discount ? "selected" : "" %>><%= Resources.Resource.Discount_option %></option>
	                                            <option value="<%= (int)TypeOfDiscountEnum.Rebate %>" <%= DiscountType3 == TypeOfDiscountEnum.Rebate ? "selected" : "" %>><%= Resources.Resource.Rebate_option %></option>
	                                            <option value="<%= (int)TypeOfDiscountEnum.Reduction %>"  <%= DiscountType3 == TypeOfDiscountEnum.Reduction ? "selected" : "" %>><%= Resources.Resource.Reduction_option %></option>
										    </select>
	                                    </span>
	                                    
										<div class="col-lg-5 text-left">
	                                        <div class="input-group discount-feature-container" id="discount-feature-container3" data-popup="tooltip" data-placement="left" title=''>
	                                            <span class="input-group-addon" style=""><i class="fa-light fa-percent"></i></span>
											    <input id="tbxDiscountPercent3" value="<%= DiscountPercent3 %>" type="number" min="0" max="100" class="form-control discount-percent" style="width: 70px;"/>
	                                            <input id="hdnDiscountPercentValidated3" type="hidden"  value="<%= DiscountPercent3 %>" />

	                                            <div style="float: right">                                                
	                                                <label style="display:inline-block;padding-top:7px;padding-right:5px;float:left;">soit</label>
	                                                <input id="tbxDiscountAmount3" type="text" disabled="disabled" class="form-control discount-amount" style="display: inline;width:200px" />
	                                            </div>
	                                            
	                                        </div>                                        
										</div>
									</div>

	                                <div class="form-group additional-discount-container <%= DiscountPercent4 > 0 ? string.Empty : "collapse" %>">
	                                    <span class="col-lg-6 text-right">
	                                        <select id="ddlDiscountType4" class="form-control discount-type" style="width: auto;">
	                                            <option value="<%= (int)TypeOfDiscountEnum.Discount %>" <%= DiscountType4 == TypeOfDiscountEnum.Discount ? "selected" : "" %>><%= Resources.Resource.Discount_option %></option>
	                                            <option value="<%= (int)TypeOfDiscountEnum.Rebate %>" <%= DiscountType4 == TypeOfDiscountEnum.Rebate ? "selected" : "" %>><%= Resources.Resource.Rebate_option %></option>
	                                            <option value="<%= (int)TypeOfDiscountEnum.Reduction %>"  <%= DiscountType4 == TypeOfDiscountEnum.Reduction ? "selected" : "" %>><%= Resources.Resource.Reduction_option %></option>
										    </select>
	                                    </span>
	                                    
										<div class="col-lg-5 text-left">
	                                        <div class="input-group discount-feature-container" id="discount-feature-container4" data-popup="tooltip" data-placement="left" title=''>
	                                            <span class="input-group-addon" style=""><i class="fa-light fa-percent"></i></span>
											    <input id="tbxDiscountPercent4" value="<%= DiscountPercent4 %>" type="number" min="0" max="100" class="form-control discount-percent" style="width: 70px;"/>
	                                            <input id="hdnDiscountPercentValidated4" type="hidden"  value="<%= DiscountPercent4 %>" />

	                                            <div style="float: right">                                                
	                                                <label style="display:inline-block;padding-top:7px;padding-right:5px;float:left;">soit</label>
	                                                <input id="tbxDiscountAmount4" type="text" disabled="disabled" class="form-control discount-amount" style="display: inline;width:200px" />
	                                            </div>
	                                            
	                                        </div>                                        
										</div>
									</div>

	                                <div class="form-group">
	                                    <span class="col-lg-6 text-right"></span>
	                                    <div class="col-lg-5 text-left">
	                                        <div style="float: right"><a id="add-discount-line-button" class="wysiwyg-color-black" onclick="showAdditionalDiscount()">+ <%= Resources.Resource.Add_line %></a></div>
	                                    </div>
	                                </div>
                                </asp:Panel>
                                <%-- End of Discounts / Rebaits--%>

								<asp:Panel runat="server" ID="OnDisabledUsePriceIncludingVat3">   
	                                <div class="form-group">
										<label class="control-label col-lg-6 text-right">Total HT</label>
										<div class="col-lg-5 text-right">
											<div class="input-group">
												<span class="input-group-addon"><i class="icon-calculator2"></i></span>
												<input type="text" step="0.01" value="" class="form-control" placeholder="Total price" disabled="disabled" id="tbxTotalHT" >
											</div>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-lg-6 text-right"><%= Resources.Resource.Taxes %></label>
										<div class="col-lg-5 text-right">
											<div class="input-group" data-popup="tooltip"  data-placement="left" title='<%$ Resources:Resource, TaxesDescription %>' runat="server">
												<span class="input-group-addon"><i class="icon-balance"></i></span>
												<input type="text" id="tbxTaxes" step="0.01" class="form-control" placeholder="Taxes" disabled="disabled" >
	                                            <span class="input-group-btn">
													<button class="btn btn-default" type="button" onclick="$('#taxDetails').toggle()"><i class="icon-question3"></i></button>
												</span>
											</div>
										</div>
									</div>

	                                <div id="taxDetails" style="display:none;"></div>
                                </asp:Panel>
							
                                <div class="form-group" runat="server" id="divRoundedAmount">
                                    <label class="control-label col-lg-6 text-right"><%= Resources.Resource.RoundedAmount %></label>
                                    <div class="col-lg-5">
                                        <div class="input-group" data-popup="tooltip"  data-placement="left" title='<%$ Resources:Resource, RoundedAmount %>' runat="server">
                                            <span class="input-group-addon"><i class="icon-cash4"></i></span>
                                            <input type="text" value="" id="tbxRoundedAmount" step="0.01" class="form-control" placeholder="rounded amount" disabled="disabled" />
                                        </div>
                                    </div>
                                </div>

								<div class="form-group">
									<label class="control-label col-lg-6 text-right"><strong><%= Resources.Resource.Total %> TTC</strong></label>
									<div class="col-lg-5">
										<div class="input-group" data-popup="tooltip"  data-placement="left" title='<%$ Resources:Resource, BillTotalDescription %>' runat="server">
											<span class="input-group-addon"><i class="icon-cash4"></i></span>
											<input type="text" value="" id="tbxGrandTotal" step="0.01" class="form-control" placeholder="Total price" disabled="disabled" >
										</div>
									</div>
								</div>

                                

							</fieldset>
						</div>

                    </div>

                  

					
			</div>

		</div>

</div>
<style>
	.select2-search__field{
		width: 100% !important;
	}
	
	.dn-create-swal {
        width: 80%;
        max-width: 1000px;
        min-height: 350px;
        max-height: 600px !important;
    }
    
    #msTemplateListIdsGroup .select2-container .select2-selection__choice {
        background-color: rgb(33, 150, 243) !important;
    }
</style>
<script type="text/javascript">
    var services = <%= JsonServices %>;
    var taxes = <%= JsonTaxes %>;
    var tableDataSource = <%= JsonTableDataSource %>;
    var notes = <%= JsonNotes %>;
    var commentId = "<%= tbxComment.ClientID %>";
    var ressources = {};
    ressources['SureToDeleteLine'] = "<%= Resources.Resource.SureToDeleteLine %>";
    ressources['ERR_client_not_allowed'] = "<%= Resources.Resource.ERR_client_not_allowed %>";
    ressources['ERR_client_contact_not_allowed'] = "<%= Resources.Resource.ERR_client_contact_not_allowed %>";
    ressources['ERR_client_address_not_allowed'] = "<%= Resources.Resource.ERR_client_address_not_allowed %>";
    ressources['ERR_due_date_mandatory'] = "<%= Resources.Resource.ERR_due_date_mandatory %>";
    ressources['Discount_P'] = "<%= Resources.Resource.Discount_P %>";
    ressources['Unit_Price'] = "<%= Resources.Resource.Unit_Price %>";
    ressources['Qty'] = "<%= Resources.Resource.Qty %>";
    ressources['Total'] = "<%= Resources.Resource.Total %>";
    ressources['Tax'] = "<%= Resources.Resource.VAT %>";
    ressources['Now'] = "<%= Resources.Resource.Now %>";
    ressources['Me'] = "<%= Resources.Resource.Me %>";
    ressources['NoStayHere'] = "<%= Resources.Resource.NoStayHere %>";
    ressources['YesOpenDeliveryNote'] = "<%= Resources.Resource.YesOpenDeliveryNote %>";
    ressources['YesGoToNew'] = "<%= Resources.Resource.YesGoToNew %>";
    ressources['CloneDone'] = "<%= Resources.Resource.CloneDone %>";
    ressources['DoYouWantToBeRedirectedTo'] = "<%= Resources.Resource.DoYouWantToBeRedirectedTo %>";
    ressources['CreateProjectNow'] = "<%= Resources.Resource.CreateProjectNow %>";
    ressources['DoYouWantToCreateANewProjectNow'] = "<%= Resources.Resource.DoYouWantToCreateANewProjectNow %>";
    ressources['CreateInvoiceNow'] = "<%= Resources.Resource.CreateInvoiceNow %>";
    ressources['DoYouWantToCreateANewInvoiceNow'] = "<%= Resources.Resource.DoYouWantToCreateANewInvoiceNow %>";
    ressources['NoLater'] = "<%= Resources.Resource.NoLater %>";
    ressources['YesCreateNow'] = "<%= Resources.Resource.YesCreateNow %>";
    ressources['DropFilesToUpload'] = "<%= Resources.Resource.DropFilesToUpload %>";
    ressources['OrClick'] = "<%= Resources.Resource.OrClick %>";
    ressources['Finished'] = "<%= Resources.Resource.Finished %>";
    ressources['On_hold'] = "<%= Resources.Resource.On_hold %>";
    ressources['Opened'] = "<%= Resources.Resource.Opened %>";
    ressources['Late'] = "<%= Resources.Resource.Late %>";
    ressources['Pending'] = "<%= Resources.Resource.Pending %>";
    ressources['Paid'] = "<%= Resources.Resource.Paid %>";
    ressources['ERR_internalId_already_used'] = "<%= Resources.Resource.ERR_internalId_already_used %>";
    ressources['OK'] = "<%= Resources.Resource.OK %>";
    ressources['Accept'] = "<%= Resources.Resource.Accept %>";
    ressources['DontUpdate'] = "<%= Resources.Resource.DontUpdate %>";
    ressources['OpenPartInvoice'] = "<%= Resources.Resource.OpenPartInvoice %>";
    ressources['OpenPartInvoiceDescription'] = "<%= Resources.Resource.OpenPartInvoiceDescription %>";
    ressources['YesOpenPartInvoice'] = "<%= Resources.Resource.YesOpenPartInvoice %>";
    ressources['CreateDeliveryNote'] = "<%= Resources.Resource.CreateDeliveryNote %>";
    ressources['RefNr'] = "<%= Resources.Resource.RefNr %>";
    ressources['SelectedQuantity'] = "<%= Resources.Resource.SelectedQuantity %>";
    ressources['Remainder'] = "<%= Resources.Resource.Remainder %>";
    ressources['Create'] = "<%= Resources.Resource.Create %>";
    ressources['Cancel'] = "<%= Resources.Resource.Cancel %>";
    ressources['NewDeliveryNote'] = "<%= Resources.Resource.NewDeliveryNote %>";
    ressources['OpenDeliveryNoteDescription'] = "<%= Resources.Resource.OpenDeliveryNoteDescription %>";
    ressources['DN'] = "<%= Resources.Resource.DN %>";
	
    ressources['Item'] = "<%= Resources.Resource.Item %>";
    ressources['PlannedQuantity'] = "<%= Resources.Resource.PlannedQuantity %>";
    ressources['SelectedQuantity'] = "<%= Resources.Resource.SelectedQuantity %>";
    ressources['RemainingQuantity'] = "<%= Resources.Resource.RemainingQuantity %>";
	
    ressources['CreateProjectChooseType'] = "<%= Resources.Resource.CreateProjectChooseType %>";
    ressources['ChooseNextStep'] = "<%= Resources.Resource.ChooseNextStep %>";
    ressources['Invoice'] = "<%= Resources.Resource.Invoice %>";
    ressources['Project'] = "<%= Resources.Resource.Opened_Job %>";
    ressources['PricesAreInCurrency'] = "<%= Resources.Resource.PricesAreInCurrency %>";
    ressources['LastSend'] = "<%= Resources.Resource.LastSend %>";
    ressources['DeleteAttachment'] = "<%= Resources.Resource.DeleteAttachment %>";
    ressources['DoYouWantToDeleteAttachment'] = "<%= Resources.Resource.DoYouWantToDeleteAttachment %>";
    ressources['CreateNew'] = "<%= Resources.Resource.CreateNew %>";
    ressources['ERR_Comment_Mandatory'] = "<%= Resources.Resource.ERR_Comment_Mandatory %>";
    ressources['ERR_NotAllItemsChecked'] = "<%= Resources.Resource.ERR_NotAllItemsChecked %>";
    ressources['Validation_Error'] = "<%= Resources.Resource.Validation_Error %>";
    ressources['Close'] = "<%= Resources.Resource.Close %>";
    ressources['UpdateInvoiceDate'] = "<%= Resources.Resource.UpdateInvoiceDate %>";
    ressources['Update'] = "<%= Resources.Resource.Update %>";

    ressources['MustSelectAddress'] = "<%= Resources.Resource.MustSelectAddress %>";
    ressources['MustSelectContact'] = "<%= Resources.Resource.MustSelectContact %>";
    ressources['MustSelectCustomer'] = "<%= Resources.Resource.MustSelectCustomer %>";
	
    ressources['AlreadyDelivered'] = "<%= Resources.Resource.AlreadyDelivered %>";
    ressources['AlreadyInvoiced'] = "<%= Resources.Resource.AlreadyInvoiced %>";

    var isReadOnly = <%= IsReadOnly %>;
    var disableInvoicedOnly = <%= DisableInvoicedOnly.ToString().ToLower() %>;
    var usePricesIncludingVat = <%= UsePricesIncludingVat.ToString().ToLower() %>;
    var enableProjectFields = <%= EnableProjectFields.ToString().ToLower() %>;
    var enableDeliveryNotesFields = <%= EnableDeliveryNotesFields.ToString().ToLower() %>;
    var hideUnitColumn = <%= HideUnitColumn.ToString().ToLower() %>;
    var hideServiceCustomNumberColumn = <%= HideServiceCustomNumber.ToString().ToLower() %>;

    var attachmentsCollection = <%= JsonAttachments %>;
	
    var additionalEmployeesDataJson = <%= AdditionalEmployeesDataJson %>;
    var selectedEmployeeIds = [<%= string.Join(",", SelectedEmployeeIds) %>];

    var templateListIdsDataJson = <%= TemplateListIdsDataJson %>;
    var selectedTemplateListIds = [<%= string.Join(",", SelectedTemplateListIds) %>];
	
    $('#modal_workflow').on('show.bs.modal', function (e) {
      var refNum = $('#<%= tbxInternalId.ClientID %>').val();
      $('#ref-num-postfix').text('-' + refNum);
    });
    
</script>
<script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Projects/JobEditor.js") %>"></script>
<script type="text/javascript" src="<%= Page.ResolveClientUrl("~/Projects/Workflow.js") %>"></script>

