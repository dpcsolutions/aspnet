﻿var _periodFilterFrom = null;
var _periodFilterTo = null;
var _clientFilter = null;
var _statusFilter = null;

var _dataTable = null;
var _initTagAdd = false;
var Tags = [];
var enterYourMessageWithThreeDot = ressources['EnterYourMessage1'];
var EditIdStatus = $("input[inlineeditfield=status]").val();
$("#ddlStatusInLine").val(EditIdStatus);
var EditIdDueDate = $("input[inlineeditfield=dueDate]").val();
$("#tbxDueDate").val(EditIdDueDate);
var EditIdddlPriority = $("input[inlineeditfield=priority]").val();
$("#ddlPriority").val(EditIdddlPriority);
var EditIdddlClients = $("input[inlineeditfield=Owner]").val();
$("#ddlClients").val(EditIdddlClients);
//-----------------------------------------------------------------------------------------------------------------------------//
var lblStatusColor = $(".lblStatusColor");
var ddlPriorityId = $(".ddlPriorityId");
var isMessage = $(".isMessage");
$.each($(isMessage), function (key, value) {

    if (value.value == 'False') {
        $($(value.previousSibling)[0].previousSibling).css('display', 'none');
    }
});

$(".ddlPriorityItem").on('change', function () {
    if (this.value == 1) {
        this.className = "form-control label label-flat border-warning text-grey-600 ddlPriorityItem";

    }
    if (this.value == 2) {
        this.className = "form-control label label-flat border-grey text-grey-600 ddlPriorityItem";
    }
    if (this.value == 3) {
        this.className = "form-control label label-flat border-info text-grey-600 ddlPriorityItem";
    }
    if (this.value == 4) {
        this.className = "form-control label label-flat border-success text-grey-600 ddlPriorityItem";
    }
});

$.each($(ddlPriorityId), function (key, value) {
    value.nextElementSibling.value = value.value;

    if (value.value == 1) {
        value.nextElementSibling.value = 1;
        value.nextElementSibling.className = "form-control label label-flat border-warning text-grey-600 ddlPriorityItem";

    }
    if (value.value == 2) {
        value.nextElementSibling.value = 2;
        value.nextElementSibling.className = "form-control label label-flat border-grey text-grey-600 ddlPriorityItem";
    }
    if (value.value == 3) {
        value.nextElementSibling.value = 3;
        value.nextElementSibling.className = "form-control label label-flat border-info text-grey-600 ddlPriorityItem";
    }
    if (value.value == 4) {
        value.nextElementSibling.value = 4;
        value.nextElementSibling.className = "form-control label label-flat border-success text-grey-600 ddlPriorityItem";
    }
});

var taskId = $("#ContentPlaceHolder1_TaskId").val();

$(".ddlStatusColorItem").on('change', function () {
    if (this.value == 1) {
        this.className = "form-control label label-flat border-warning text-warning-600 ddlStatusColorItem";
    }
    if (this.value == 2) {
       
        this.className = "form-control label label-flat border-info text-info-600 ddlStatusColorItem";
    }
    if (this.value == 3) {        
        this.className = "form-control label label-flat border-grey text-grey-600 ddlStatusColorItem";
    }
    if (this.value == 4) {        
        this.className = "form-control label label-flat border-success text-success-600 ddlStatusColorItem";
    }
    if (this.value == 5) {
        
        this.className = "form-control label label-flat border-danger text-danger-600 ddlStatusColorItem";
    }
    if (this.value == 6) {        
        this.className = "form-control label label-flat border-info text-info-600 ddlStatusColorItem";
    }
});

$.each($(lblStatusColor), function (i, obj) {
    obj.nextElementSibling.value = obj.value;
    if (obj.value == 1) {
        obj.nextElementSibling.value = 1;
        obj.previousSibling.className = 'text-warning';
        obj.nextElementSibling.className = "form-control label label-flat border-warning text-warning-600 ddlStatusColorItem";
    }
    if (obj.value == 2) {
        obj.nextElementSibling.value = 2;
        obj.nextElementSibling.className = "form-control label label-flat border-info text-info-600 ddlStatusColorItem";
        obj.previousSibling.className = 'text-info';
    }
    if (obj.value == 3) {
        obj.nextElementSibling.value = 3;
        obj.nextElementSibling.className = "form-control label label-flat border-grey text-grey-600 ddlStatusColorItem";
        obj.previousSibling.className = 'text-grey';
    }
    if (obj.value == 4) {
        obj.nextElementSibling.value = 4;
        obj.nextElementSibling.className = "form-control label label-flat border-success text-success-600 ddlStatusColorItem";
        obj.previousSibling.className = 'text-success';
    }
    if (obj.value == 5) {
        obj.nextElementSibling.value = 5;
        obj.nextElementSibling.className = "form-control label label-flat border-danger text-danger-600 ddlStatusColorItem";
        obj.previousSibling.className = 'text-danger';
    }
    if (obj.value == 6) {
        obj.nextElementSibling.value = 6;
        obj.nextElementSibling.className = "form-control label label-flat border-info text-info-600 ddlStatusColorItem";
        obj.previousSibling.className = 'text-info';
    }
});

$("#pinTask").hover(function () {
    $(this).css("color", "#fd710d");
}, function () {
    $(this).css("color", "#333");
});
$("#unpinTask").hover(function () {
    $(this).css("color", "#333");
}, function () {
    $(this).css("color", "#fd710d");
});

//$("#ddlClients").val(EditIdddlClients);
//-----------------------------------------------------------------------------------------------------------------------------//
$(function () {

    var isEditModalOpen1stTime = false;
    var isEditModalOpen1stTimeForProgress = false;
    
    getTagsForAutocomplete();

    $('#createTaskHeader').show();
    $('#editTaskHeader').hide();
    $("#taskMessageCreate").empty();
    $("#taskMessageEdit").empty();

    function empty(str) {
        if (typeof str == 'undefined' || !str || str.length === 0 || str === "" || !/[^\s]/.test(str) || /^\s*$/.test(str) || str.replace(/\s/g, "") === "") {
            return true;
        }
        else {
            return false;
        }
    }

    $(document).on('click', '.paginate_button', function () {
        getTagsForAutocomplete();
    });

    $("[id*=grid] tr").find('td:first').click(function () {

        $("#ContentPlaceHolder1_gridTaskId").val('');
        $('#openUpload').click(function () {
            $('#fileUpload').trigger('click');
        });
        $("#txtTaskDisplay").val('True');
        $("#divDisplayTaskDetails").css('display', 'block');
        $(".progressBar").css('display', 'none');
        var s = $("[id*=grid] td");
        var TaskId = $(this).find('input')[0].value;
        if (TaskId > 0) {
            $('#createTaskHeader').hide();
            $('#editTaskHeader').show();
            $('#divPin').css('display', 'block');
            $.ajax({
                type: "GET",
                contentType: "application/json; charset=utf-8",
                url: '../../API/Service.svc/GetTaskDetails?TaskId=' + TaskId,

                dataType: "json",
                success: function (res) {
                    getTagsForAutocomplete();

                    isEditModalOpen1stTime = false;
                    isEditModalOpen1stTimeForProgress = false;
                    $('#singleFieldTags2').tagit('removeAll');
                    var assignto = [];
                    for (var i = 0; i < res.AssignUsers.length; i++) {
                        assignto.push(res.AssignUsers[i].Key);
                    }

                    $('#ddlAssignTo').val(assignto).trigger('change');

                    var setTags = '';
                    _initTagAdd = true;
                    for (var i = 0; i < res.Tags.length; i++) {
                        setTags = i == 0 ? res.Tags[i].Value : setTags + "," + res.Tags[i].Value;
                        $('#singleFieldTags2').tagit('createTag', res.Tags[i].Value);
                    }

                    _initTagAdd = false;
                    if (res.isPinned) {
                        $('#pinTask').css("display", "none");
                        $('#unpinTask').css("display", "block");
                        $('#unpinTask').css("color", '#fd710d');
                    }
                    else {
                        $('#unpinTask').css("display", "none");
                        $('#pinTask').css("display", "block");
                        $('#pinTask').css("color", '#333');
                    }
                    $('#ContentPlaceHolder1_pinUnpin').val(res.isPinned);
                    //value 
                    $('#editIsPinned').val(res.isPinned);
                    $('#ContentPlaceHolder1_Text3').val(res.title);
                    $('#ContentPlaceHolder1_Textarea1').val(res.description);
                    var str = [];
                    if (res.Tags != null) {
                        $.each($(res.Tags), function (key, tag) {
                            str.push(tag.Value);
                        });


                        var allTagList = [];
                        Tags.forEach(tag => {
                            allTagList.push(tag.Key);
                        });
                        let tagHtml = '<ul style="list-style-type:none; overflow:hidden;">';
                        str.forEach(e => {

                            let ind = allTagList.indexOf(e);
                            tagHtml += ` <li class="tagcolorsection" style="background-color:` + Tags[ind].Value + `;">`
                                        + e + `</li>`;
                        });
                        tagHtml += '</ul>';
                      
                        $("#tagListReadOnly").empty();
                        $("#tagListReadOnly").append(tagHtml);

                    }

                    //display
                    $("#lblTaskDisplayTitle").html(res.title);
                    $('#lblTaskDisplayDescription').html(res.description);
                    $('#lblEditDisplayOwner').html(res.OwnerName);
                    var assignName = [];
                    if (res.Tags != null) {
                        $.each($(res.AssignUsers), function (key, assign) {
                            assignName.push(assign.Value);
                        });
                        $("#lblEditDisplayAssignTo").html(assignName.join(", "));
                    }
                    $("#lblEditDisplayTimline").html(res.timeline);


                    $('#lblEditDisplayPriority').html(ressources[res.PriorityName]);
                    $('#lblEditDisplayStatus').html(ressources[res.StatusName]);
                    $('#ContentPlaceHolder1_TaskTile').val(res.title);
                    $('#ContentPlaceHolder1_Description').val(res.description);
                    $('#ddlOwner').val(res.owner);
                    $('#ddlOwner').val(res.owner);
                    $('#ddlOwner').select2().trigger('change');

                    $('#ContentPlaceHolder1_ddlPriority').val(res.priority);
                    $('#ContentPlaceHolder1_ddlPriority').select2().trigger('change');

                    $('#ContentPlaceHolder1_ddlStatus').val(res.status);
                    $('#ContentPlaceHolder1_ddlStatus').select2().trigger('change');

                    $('#ContentPlaceHolder1_DropDownListstatus').val(res.status);
                    $('#ContentPlaceHolder1_DropDownListstatus').select2().trigger('change');

                    $('#modal_EditTask .barrating').barrating('set', res.progress);
                    $('#modal_CreateTask1 .barrating').barrating('set', res.progress);

                    $("#singleFieldTags2").val();


                    //date range
                    if (!empty(res.timeline)) {
                        var dateEdit = res.timeline.split('-');
                        $('#ContentPlaceHolder1_tbxTimelineDateRange').daterangepicker({
                            showDropdowns: true,
                            opens: "left",
                            applyClass: 'bg-slate-600',
                            cancelClass: 'btn-default',
                            locale: {
                                format: CALANI.DEFAULTS.dateFormat
                            },
                            startDate: dateEdit[0],
                            endDate: dateEdit[1]

                        });
                    }

                    //end date range
                    // $('#ContentPlaceHolder1_tbxTimelineDateRange').val(res.timeline);
                    if (!empty(res.dueDate)) {
                        let dt = moment(res.dueDate);
                        $('#ContentPlaceHolder1_tbxDueDate').val(dt.format('l'));
                        $("#lblEditDisplayDueDate").html(dt.format('l'));
                    }
                    var $example = $(".ddlAssignTo").select2();
                    $example.val("2506").trigger("change");

                    $("#taskMessageCreate").empty();
                    $("#taskMessageEdit").empty();
                    var html = '';
                    var message = res.TaskMessages;
                    if (message.length > 0) {
                        html = html + "<ul class=\"media-list chat-list content-group\">";
                        for (i = 0; i < message.length; i++) {
                            var activityMsg = message[i].Message;
                            var user = message[i].CodeName;
                            var nowDate = '';
                            if (message[i].CreatedAt != null) {
                                nowDate = new Date(parseInt(message[i].CreatedAt.substr(6)));
                                nowDate = nowDate.format("ddd, dd HH:MM");
                            }

                            if (message[i].IsOwned) {
                                html = html + "<li class=\"media date-step\"><span></span></li>";
                                html = html + `
                                   <li class="media reversed">
									<div class="media-body"> `;
                                if (message[i].IsAttachment) {
                                    switch (message[i].AttachmentMimeType) {
                                        case "image/jpeg":
                                        case "image/jpg":
                                        case "image/png":
                                            if (message[i].ImageData != null) {
                                                html = html + `<div class=\"media-content bg-white\">
                                                        <a class="example-image-link" data-lightbox="MyImages" data-title="` + message[i].NameWithExtension + `" href=\"../../DownloadDocument.ashx?id=` + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + `\">
                                                        <img id=\"ItemPreview\" data-lightbox="roadtrip" src=\"data:`+ message[i].AttachmentMimeType + `;base64,` + message[i].ImageData + `\" />
                                                        </div></a><span class=\"display-block\" style="overflow-wrap: anywhere;">` + message[i].NameWithExtension + `</span>`;
                                            } else {
                                                html = html + "<div class=\"media-content bg-white\"><a class=\"example - image - link\" data-lightbox=\"MyImages\" data-title=\"" + message[i].NameWithExtension + "\" href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"> <img data-lightbox=\"roadtrip\" src =\"../../assets/images/file_thumbnail/photo.png\" /></div></a><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";
                                            }
                                            break;
                                        case "text/csv":
                                            html = html + "<div class=\"media-content bg-white\"><a href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"><img src=\"../../assets/images/file_thumbnail/excel.png\" /></a></div><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";
                                            break;
                                        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                                            html = html + "<div class=\"media-content bg-white\"><a href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"><img src=\"../../assets/images/file_thumbnail/excel.png\" /></a></div><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";
                                            break;
                                        case "video/mp4":
                                        case "video/avi":
                                        case "video/quicktime":
                                            html = html + `<div class=\"media-content bg-white\">
                                                            <video width="100%" controls class="myvideo" style="height:100%">
                                                            <source src="../../DownloadDocument.ashx?id=` + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + `" id="video_here">
                                                            Your browser does not support HTML5 video.
                                                            </video></div>
                                                            <span class=\"display-block\">` + message[i].NameWithExtension + `</span>`;
                                            break;
                                        case "application/pdf":
                                            html = html + "<div class=\"media-content bg-white\"><a href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"><img src=\"../../assets/images/file_thumbnail/pdf.png\" /></a></div><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";
                                            break;
                                        default:
                                            html = html + "<div class=\"media-content bg-white\"><a href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"><img src=\"../../assets/images/file_thumbnail/word.png\" /></a></div><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";

                                    }

                                }
                                else if (activityMsg != null) {
                                    html = html + "<div class=\"media-content\"> " + activityMsg + "</div>";
                                }
                                else {
                                    html = html;
                                }

                                html = html + "<span class=\"media-annotation display-block mt-10 text-italic\">" + message[i].UserFirstName + " " + message[i].UserLastName + "," + nowDate + "<a href=\"#\"><i class=\"icon-pin-alt position-right text-muted\"></i></a></span>";
                                html = html + `</div>

									<div class="media-right">`;

                                html = html + "<button type=\"button\" class=\"btn border-slate btn-flat btn-icon btn-rounded\">" + user + "</button>";
                                html = html + `
									</div>
								</li>`;




                            }
                            else {
                                html = html + "<li class=\"media date-step\"><span></span></li>";

                                html = html + "<li class=\"media\">";
                                html = html + `<div class=\"media-left\">`;

                                html = html + "<button type =\"button\" class=\"btn border-slate btn-flat btn-icon btn-rounded\">" + user + "</button>";
                                html = html + `</div>`;

                                html = html + "<div class=\"media-body\">";
                                if (message[i].IsAttachment) {
                                    switch (message[i].AttachmentMimeType) {
                                        case "image/jpeg":
                                        case "image/jpg":
                                        case "image/png":
                                            if (message[i].ImageData != null) {
                                                html = html + `<div class=\"media-content bg-white\">
                                                        <a class="example-image-link" data-lightbox="OImages" data-title="` + message[i].NameWithExtension + `" href=\"../../DownloadDocument.ashx?id=` + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + `\">
                                                        <img id=\"ItemPreview\" data-lightbox="roadtrip" src=\"data:`+ message[i].AttachmentMimeType + `;base64,` + message[i].ImageData + `\" />
                                                        </div></a><span class=\"display-block\" style="overflow-wrap: anywhere;">` + message[i].NameWithExtension + `</span>`;
                                            } else {
                                                html = html + "<div class=\"media-content bg-white\"><a class=\"example - image - link\" data-lightbox=\"OImages\" data-title=\"" + message[i].NameWithExtension + "\" href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"> <img data-lightbox=\"roadtrip\" src =\"../../assets/images/file_thumbnail/photo.png\" /></div></a><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";
                                            }
                                            break;
                                        case "text/csv":
                                            html = html + "<div class=\"media-content bg-white\"><a href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"><img src=\"../../assets/images/file_thumbnail/excel.png\" /></a></div><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";
                                            break;
                                        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                                            html = html + "<div class=\"media-content bg-white\"><a href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"><img src=\"../../assets/images/file_thumbnail/excel.png\" /></a></div><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";
                                            break;
                                        case "video/mp4":
                                        case "video/avi":
                                        case "video/quicktime":
                                            html = html + `<div class=\"media-content bg-white\">
                                                            <video width="100%" controls class="myvideo" style="height:100%">
                                                            <source src="../../DownloadDocument.ashx?id=` + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + `" id="video_here">
                                                            Your browser does not support HTML5 video.
                                                            </video></div>
                                                            <span class=\"display-block\">` + message[i].NameWithExtension + `</span>`;
                                            break;
                                        case "application/pdf":
                                            html = html + "<div class=\"media-content bg-white\"><a href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"><img src=\"../../assets/images/file_thumbnail/pdf.png\" /></a></div><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";
                                            break;
                                        default:
                                            html = html + "<div class=\"media-content bg-white\"><a href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"><img src=\"../../assets/images/file_thumbnail/word.png\" /></a></div><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";

                                    }

                                }
                                else if (activityMsg != null) {
                                    html = html + "<div class=\"media-content\"> " + activityMsg + "</div>";
                                }
                                else {
                                    html = html;
                                }
                                html = html + "<span class=\"media-annotation display-block mt-10 text-italic\">" + message[i].UserFirstName + " " + message[i].UserLastName + "," + nowDate + "<a href=\"#\"><i class=\"icon-pin-alt position-right text-muted\"></i></a></span>";
                                html = html + "</div>";
                                html = html + "</li>";
                            }

                        }
                        html = html + "</ul>";
                        html = html + ` <textarea name="enter-message" id="" class="form-control content-group cls_Message" rows="3" cols="1" placeholder="` + enterYourMessageWithThreeDot + `"></textarea>

	                    	<div class="row">
	                    		<div class="col-xs-6">
		                        	<ul class="icons-list icons-list-extended mt-10">`;
                        html = html + "<li><input class=\"fileUpload\" type=\"file\" multiple /></li>";

                        html = html + ` </ul>
	                    		</div>

	                    		<div class="col-xs-6 text-right"> `;
                        html = html + "<button type=\"button\" onclick=\"sendActivityMessge()\" class=\"btn bg-teal-400 btn-labeled btn-labeled-right\"><b><i class=\"icon-circle-right2\"></i></b> " + ressources['Send'] + " </button></div></div>";


                    }

                    else {

                        html = html + ` <textarea name="enter-message" id="" class="form-control content-group cls_Message" rows="3" cols="1" placeholder="` + enterYourMessageWithThreeDot + `"></textarea>

	                    	<div class="row">
	                    		<div class="col-xs-6">
		                        	<ul class="icons-list icons-list-extended mt-10">`;
                        html = html + "<li><input class=\"fileUpload\" type=\"file\" multiple /></li>";

                        html = html + ` </ul>
	                    		</div>

	                    		<div class="col-xs-6 text-right"> `;
                        html = html + "<button type=\"button\" onclick=\"sendActivityMessge()\" class=\"btn bg-teal-400 btn-labeled btn-labeled-right\"><b><i class=\"icon-circle-right2\"></i></b> " + ressources['Send'] + "</button></div></div>";


                    }
                    var htmlCreate = html.replace(/MyImages/g, "MyImagesCreate");
                    var htmlEdit = html.replace(/MyImages/g, "MyImagesEdit");
                    htmlCreate = htmlCreate.replace(/OImages/g, "OImagesCreate");
                    htmlEdit = htmlEdit.replace(/OImages/g, "OImagesEdit");
                    $("#taskMessageCreate").append(htmlCreate);
                    $("#taskMessageEdit").append(htmlEdit);
                },
                error: function (a, b, c) {
                    alert(a.responseText);
                }
            });
        }

        $("#ContentPlaceHolder1_TaskId").val(TaskId);
        $("#ContentPlaceHolder1_pinTaskId").val(TaskId);
        $('#modal_EditTask').modal('show');
    });



    $('#modal_CreateTask').on('hidden.bs.modal', function () {
        location.reload();
    });
    $("#modal_EditTask #ContentPlaceHolder1_ratingStars2_s").change(function () {
        if (isEditModalOpen1stTimeForProgress) {
            onchangesave();
            var progressval = $("#modal_EditTask #ContentPlaceHolder1_ratingStars2_s").val();
            $('#modal_CreateTask1 .barrating').barrating('set', progressval);
        }
        else {
            isEditModalOpen1stTimeForProgress = true;
        }
    });

    $("#ContentPlaceHolder1_DropDownListstatus").change(function () {
        if (isEditModalOpen1stTime) {
            onchangesave();
        }
        else {
            isEditModalOpen1stTime = true;
        }
    });
});
function ddlToolTipsRegion(ddlRegion,tab) {
    if (ddlRegion.value == 0) {
        ddlRegion.title = "";
    } else {
        if (tab==1) {
            ddlRegion.title = "Selected Status is : " + ddlRegion.options[ddlRegion.selectedIndex].text;
        }
        else {
            ddlRegion.title = "Selected Priority is : " + ddlRegion.options[ddlRegion.selectedIndex].text;
        }
    }
}  
function onchangesave() {
    var statusval = $("#ContentPlaceHolder1_DropDownListstatus").val();
    var progressval = $("#modal_EditTask #ContentPlaceHolder1_ratingStars2_s").val();

    var taskStatusUpdate = {
        TaskId: $("#ContentPlaceHolder1_TaskId").val(),
        TaskStatusId: statusval,
        Praogressval: progressval
    };
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../API/Service.svc/UpdateTaskStatus',
        data: JSON.stringify(taskStatusUpdate),
        dataType: "json",
        success: function (res) {
            afterTaskSave(res);
        },
        error: function (a, b, c) {
            alert(a.responseText);
        }
    });
}
function pinTask() {
    var taskId = $("#ContentPlaceHolder1_TaskId").val();

    var isPinned = $('#ContentPlaceHolder1_pinUnpin').val();

    setPinUnpin(isPinned, taskId);

}

function editTask() {
    $("#txtTaskDisplay").val('False');
    var editTaskId = $("#ContentPlaceHolder1_TaskId").val();
    $("#taskMessageCreate").css('display', 'block');
    $("#taskMessageEdit").css('display', 'block');
    $(".progressBar").css('display', 'none');
    $("#modal_EditTask").modal("hide");
    $('#modal_EditTask').on('hidden.bs.modal', function () {
        // Load up a new modal...
        //$('#myModalNew').modal('show')
        $("#modal_CreateTask1").modal("show");
        setColor(Tags);
    });
}

function tagsList(e) {

    if (e !== undefined) {
        var gridTaskId = e.parent().parent().get(0).children[0].id;
        $("#ContentPlaceHolder1_gridTaskId").val(gridTaskId);
    }


    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '../../API/Service.svc/GetAllTagsForTask',
        dataType: "json",
        success: function (res) {

            $('#privatetagList').empty();
            $('#sharedtagList').empty();
            var privateHtml = '';
            var sharedHtml = '';
            for (i = 0; i < res.PrivateTags.length; i++) {
                privateHtml = privateHtml + `<span><option class="form-control mt-5 text-white text-bold tag_option" onClick="getTagOption($(event.target))" data-type=` + res.PrivateTags[i].type + ` style="background-color:` + res.PrivateTags[i].color + `;" value ="` + res.PrivateTags[i].id + `">` + res.PrivateTags[i].name + `</option><i onclick="editTag($(event.target))" class="icon-pencil tag_icon"></i></span>`;
            }
            $("#privatetagList").append(privateHtml);

            for (i = 0; i < res.SharedTags.length; i++) {
                sharedHtml = sharedHtml + `<span><option class="form-control mt-5 text-white text-bold tag_option" onClick="getTagOption($(event.target))" data-type=` + res.SharedTags[i].type + ` style="background-color:` + res.SharedTags[i].color + `;" value ="` + res.SharedTags[i].id + `">` + res.SharedTags[i].name + `</option><i onclick="editTag($(event.target))" class="icon-pencil tag_icon"></i></span>`;
            }
            $("#sharedtagList").append(sharedHtml);
        }
    });

    $('#tagList').modal("show");
}

function createTag(tabaNme) {
    $('#ContentPlaceHolder1_TagName').val('');
    $("#ContentPlaceHolder1_TagId").val('');
    $('#ContentPlaceHolder1_TagColor').spectrum();
    $("#ContentPlaceHolder1_TagColor").spectrum("set", '#000');
    $("input[type='radio'].form-check-input:checked").val('');
    $("input[type='radio'].form-check-input:checked").val('');

    if (tabaNme.toLowerCase() == "privatetab") {
        $('input:radio[name="ctl00$ContentPlaceHolder1$radio-inline-left"][value="1"]').attr('checked', 'checked');
        $('div#uniform-ContentPlaceHolder1_privateRadio span').addClass("checked");

        $('div#uniform-ContentPlaceHolder1_sharedRadio span').removeClass("checked");
    }
    else {
        $('input:radio[name="ctl00$ContentPlaceHolder1$radio-inline-left"][value="2"]').attr('checked', 'checked');
        $('div#uniform-ContentPlaceHolder1_sharedRadio span').addClass("checked");
        $('div#uniform-ContentPlaceHolder1_privateRadio span').removeClass("checked");
    }
    $('#createTagHeader').show();
    $('#editTagHeader').hide();
    $('#btnTagDelete').hide();
    $('#modal_CreateTag').modal('show');
}
function editTag(e) {
    var tagName = e.context.previousElementSibling.text;
    var tagColor = e.context.previousElementSibling.style.backgroundColor;
    var tagId = e.context.previousElementSibling.value;
    var tagType = e.context.previousElementSibling.dataset.type;
    if (tagType == 1) {
        $('div#uniform-ContentPlaceHolder1_privateRadio span').addClass("checked");
        $('div#uniform-ContentPlaceHolder1_sharedRadio span').removeClass("checked");
    }
    else {
        $('div#uniform-ContentPlaceHolder1_sharedRadio span').addClass("checked");
        $('div#uniform-ContentPlaceHolder1_privateRadio span').removeClass("checked");
    }

    $('#ContentPlaceHolder1_TagName').val(tagName);
    $("#ContentPlaceHolder1_TagId").val(tagId);
    $('#ContentPlaceHolder1_TagColor').spectrum({
        color: tagColor
    });
    $('#ContentPlaceHolder1_TagColor').val(tagColor);
    $('#createTagHeader').hide();
    $('#editTagHeader').show();
    $('#btnTagDelete').show();

    $('#modal_CreateTag').modal('show');



}

function getTagOption(e) {
    try {
        var tagName = e.context.text;
        var tagColor = e.context.style.backgroundColor;
        var tagId = e.context.value;
        var tagType = e.context.dataset.type;
        var tags = [];
        var gridTaskId = $("#ContentPlaceHolder1_gridTaskId").val();
        tags.push(tagName);

        var taskId = $("#ContentPlaceHolder1_TaskId").val();

        if (gridTaskId !== '') {
            $('#' + gridTaskId).tagit('createTag', tags);
        }
        else if (taskId > 0 || taskId === '0') {
            $('#singleFieldTags2').tagit('createTag', tags);
        }
    } catch (error) {
        console.log(error)
    } finally {
        setColor(Tags);
    }
}

function sendActivityMessge() {
    $(".progressBar").css('display', 'none');
    var message;
    var files;
    var percentVal;
    var bar = $('.bar');
    var progress = $('.progressBar ');
    var percent = $('.percent');
    var status = $('#status');
    percent.html('0%');
    var txtTaskDisplay = $("#txtTaskDisplay").val();
    if (txtTaskDisplay == 'True') {
        message = $(".cls_Message")[0].value;
        files = $(".fileUpload").get(0).files;
    }
    else {
        message = $(".cls_Message")[0].value;
        files = $(".fileUpload").get(0).files;
    }

    var taskId = $("#ContentPlaceHolder1_TaskId").val();
    if (files.length > 0) {
        $(".progressBar").css('display', 'block');
        $(".fileUpload").attr('disabled', 'disabled');
        $(".btn-labeled-right").attr('disabled', 'disabled');
        var fileData = new FormData();


        for (var i = 0; i < files.length; i++) {
            fileData.append("fileInput", files[i]);
        }



        // Make Ajax request with the contentType = false, and procesDate = false
        var ajaxRequest = $.ajax({
            type: "POST",
            url: "../../UploadTaskDocument.ashx?taskId=" + taskId,
            contentType: false,
            processData: false,
            data: fileData,
            xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentVal = Math.round(percentComplete * 100) + '%';
                        progress.width(percentVal);
                        progress.addClass('progress-bar-success');
                        percent.css('text-align', 'right');
                        percent.html(percentVal);
                    }
                }, false);
                return xhr;
            },

            success: function (res) {
                $(".progressBar").css('display', 'none');

                $.ajax({
                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    url: '../../API/Service.svc/GetTaskDetails?TaskId=' + taskId,

                    dataType: "json",
                    success: function (res) {

                        $("#taskMessageCreate").empty();
                        $("#taskMessageEdit").empty();

                        var html = '';
                        var message = res.TaskMessages;
                        html = html + "<ul class=\"media-list chat-list content-group\">";

                        html = html + getSendMessages(message);

                        var htmlCreate = html.replace(/MyImages/g, "MyImagesCreate");
                        var htmlEdit = html.replace(/MyImages/g, "MyImagesEdit");
                        htmlCreate = htmlCreate.replace(/OImages/g, "OImagesCreate");
                        htmlEdit = htmlEdit.replace(/OImages/g, "OImagesEdit");
                        $("#taskMessageCreate").append(htmlCreate);
                        $("#taskMessageEdit").append(htmlEdit);
                    }
                });
            }


        });

        ajaxRequest.done(function (xhr, textStatus) {

        });
        progress.width(percentVal);
    }




    if (message !== "") {

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            url: '../../API/Service.svc/SaveTaskActivity?taskId=' + taskId + '&Message=' + message,

            dataType: "json",
            success: function (res) {
                if (res.success == true) {
                    $.ajax({
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        url: '../../API/Service.svc/GetTaskDetails?TaskId=' + taskId,

                        dataType: "json",
                        success: function (res) {

                            $("#taskMessageCreate").empty();
                            $("#taskMessageEdit").empty();

                            var html = '';
                            var message = res.TaskMessages;
                            html = html + "<ul class=\"media-list chat-list content-group\">";
                            html = html + getSendMessages(message);
                            var htmlCreate = html.replace(/MyImages/g, "MyImagesCreate");
                            var htmlEdit = html.replace(/MyImages/g, "MyImagesEdit");
                            htmlCreate = htmlCreate.replace(/OImages/g, "OImagesCreate");
                            htmlEdit = htmlEdit.replace(/OImages/g, "OImagesEdit");
                            $("#taskMessageCreate").append(htmlCreate);
                            $("#taskMessageEdit").append(htmlEdit);
                        }
                    });
                }
            }
        });

    }

};


function getSendMessages(message) {
    var html = '';

    if (message.length > 0) {

        for (i = 0; i < message.length; i++) {
            var activityMsg = message[i].Message;
            var user = message[i].CodeName;
            var nowDate = '';

            if (message[i].CreatedAt != null) {
                nowDate = new Date(parseInt(message[i].CreatedAt.substr(6)));
                nowDate = nowDate.format("ddd, dd HH:MM");
            }

            if (message[i].IsOwned) {
                html = html + "<li class=\"media date-step\"><span></span></li>";

                html = html + `
                                   <li class="media reversed">
									<div class="media-body"> `;
                if (message[i].IsAttachment) {
                    switch (message[i].AttachmentMimeType) {
                        case "image/jpeg":
                        case "image/jpg":
                        case "image/png":
                            if (message[i].ImageData != null) {
                                html = html + `<div class=\"media-content bg-white\">
                                                        <a class="example-image-link" data-lightbox="MyImages" data-title="` + message[i].NameWithExtension + `" href=\"../../DownloadDocument.ashx?id=` + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + `\">
                                                        <img id=\"ItemPreview\" data-lightbox="roadtrip" src=\"data:`+ message[i].AttachmentMimeType + `;base64,` + message[i].ImageData + `\" />
                                                        </div></a><span class=\"display-block\" style="overflow-wrap: anywhere;">` + message[i].NameWithExtension + `</span>`;
                            } else {
                                html = html + "<div class=\"media-content bg-white\"><a class=\"example - image - link\" data-lightbox=\"MyImages\" data-title=\"" + message[i].NameWithExtension + "\" href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"> <img data-lightbox=\"roadtrip\" src =\"../../assets/images/file_thumbnail/photo.png\" /></div></a><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";
                            }
                            break;
                        case "text/csv":
                            html = html + "<div class=\"media-content bg-white\"><a href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"><img src=\"../../assets/images/file_thumbnail/excel.png\" /></a></div><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";
                            break;
                        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                            html = html + "<div class=\"media-content bg-white\"><a href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"><img src=\"../../assets/images/file_thumbnail/excel.png\" /></a></div><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";
                            break;
                        case "video/mp4":
                        case "video/avi":
                        case "video/quicktime":
                            html = html + `<div class=\"media-content bg-white\">
                                                            <video width="100%" controls class="myvideo" style="height:100%">
                                                            <source src="../../DownloadDocument.ashx?id=` + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + `" id="video_here">
                                                            Your browser does not support HTML5 video.
                                                            </video></div>
                                                            <span class=\"display-block\">` + message[i].NameWithExtension + `</span>`;
                            break;
                        case "application/pdf":
                            html = html + "<div class=\"media-content bg-white\"><a href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"><img src=\"../../assets/images/file_thumbnail/pdf.png\" /></a></div><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";
                            break;
                        default:
                            html = html + "<div class=\"media-content bg-white\"><a href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"><img src=\"../../assets/images/file_thumbnail/word.png\" /></a></div><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";

                    }

                }
                else if (activityMsg != null) {
                    html = html + "<div class=\"media-content\"> " + activityMsg + "</div>";
                }
                else {
                    html = html;
                }

                html = html + "<span class=\"media-annotation display-block mt-10 text-italic\">" + message[i].UserFirstName + " " + message[i].UserLastName + "," + nowDate + "<a href=\"#\"><i class=\"icon-pin-alt position-right text-muted\"></i></a></span>";
                html = html + `</div>

									<div class="media-right">`;

                html = html + "<button type=\"button\" class=\"btn border-slate btn-flat btn-icon btn-rounded\">" + user + "</button>";
                html = html + `
									</div>
								</li>`;

            }
            else {
                html = html + "<li class=\"media date-step\"><span></span></li>";

                html = html + "<li class=\"media\">";
                html = html + `<div class=\"media-left\">`;

                html = html + "<button type =\"button\" class=\"btn border-slate btn-flat btn-icon btn-rounded\">" + user + "</button>";
                html = html + `</div>`;

                html = html + "<div class=\"media-body\">";
                if (message[i].IsAttachment) {
                    switch (message[i].AttachmentMimeType) {
                        case "image/jpeg":
                        case "image/jpg":
                        case "image/png":
                            if (message[i].ImageData != null) {
                                html = html + `<div class=\"media-content bg-white\">
                                                        <a class="example-image-link" data-lightbox="OImages" data-title="` + message[i].NameWithExtension + `" href=\"../../DownloadDocument.ashx?id=` + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + `\">
                                                        <img id=\"ItemPreview\" data-lightbox="roadtrip" src=\"data:`+ message[i].AttachmentMimeType + `;base64,` + message[i].ImageData + `\" />
                                                        </div></a><span class=\"display-block\" style="overflow-wrap: anywhere;">` + message[i].NameWithExtension + `</span>`;
                            } else {
                                html = html + "<div class=\"media-content bg-white\"><a class=\"example - image - link\" data-lightbox=\"OImages\" data-title=\"" + message[i].NameWithExtension + "\" href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + "&t=" + message[i].FileNameNoExtension + "\"> <img data-lightbox=\"roadtrip\" src =\"../../assets/images/file_thumbnail/photo.png\" /></div></a><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";
                            }
                            break;
                        case "text/csv":
                            html = html + "<div class=\"media-content bg-white\"><a href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"><img src=\"../../assets/images/file_thumbnail/excel.png\" /></a></div><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";
                            break;
                        case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
                            html = html + "<div class=\"media-content bg-white\"><a href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"><img src=\"../../assets/images/file_thumbnail/excel.png\" /></a></div><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";
                            break;
                        case "video/mp4":
                        case "video/avi":
                        case "video/quicktime":
                            html = html + `<div class=\"media-content bg-white\">
                                                            <video width="100%" controls class="myvideo" style="height:100%">
                                                            <source src="../../DownloadDocument.ashx?id=` + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + `" id="video_here">
                                                            Your browser does not support HTML5 video.
                                                            </video></div>
                                                            <span class=\"display-block\">` + message[i].NameWithExtension + `</span>`;
                            break;
                        case "application/pdf":
                            html = html + "<div class=\"media-content bg-white\"><a href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"><img src=\"../../assets/images/file_thumbnail/pdf.png\" /></a></div><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";
                            break;
                        default:
                            html = html + "<div class=\"media-content bg-white\"><a href=\"../../DownloadDocument.ashx?id=" + message[i].AttachmentId + `&t=` + message[i].FileNameNoExtension + "\"><img src=\"../../assets/images/file_thumbnail/word.png\" /></a></div><span class=\"display-block\">" + message[i].NameWithExtension + "</span>";

                    }
                }
                else if (activityMsg != null) {
                    html = html + "<div class=\"media-content\"> " + activityMsg + "</div>";
                }
                else {
                    html = html;
                }
                html = html + "<span class=\"media-annotation display-block mt-10 text-italic\">" + message[i].UserFirstName + " " + message[i].UserLastName + "," + nowDate + "<a href=\"#\"><i class=\"icon-pin-alt position-right text-muted\"></i></a></span>";
                html = html + "</div>";
                html = html + "</li>";
            }

        }
        html = html + "</ul>";
        html = html + ` <textarea name="enter-message" id="" class="form-control content-group cls_Message" rows="3" cols="1" placeholder="` + enterYourMessageWithThreeDot + `"></textarea>

	                    	<div class="row">
	                    		<div class="col-xs-6">
		                        	<ul class="icons-list icons-list-extended mt-10">`;
        html = html + "<li><input class=\"fileUpload\" type=\"file\" multiple /></li>";

        html = html + ` </ul>
	                    		</div>

	                    		<div class="col-xs-6 text-right"> `;
        html = html + "<button type=\"button\" onclick=\"sendActivityMessge()\" class=\"btn bg-teal-400 btn-labeled btn-labeled-right\"><b><i class=\"icon-circle-right2\"></i></b> " + ressources['Send'] + "</button></div></div>";


    }

    else {

        html = html + ` <textarea name="enter-message" id="" class="form-control content-group " rows="3" cols="1" placeholder="` + enterYourMessageWithThreeDot + `"></textarea>

	                    	<div class="row">
	                    		<div class="col-xs-6">
		                        	<ul class="icons-list icons-list-extended mt-10">`;
        html = html + "<li><input class=\"fileUpload\" type=\"file\" multiple /></li>";

        html = html + ` </ul>
	                    		</div>

	                    		<div class="col-xs-6 text-right"> `;
        html = html + "<button type=\"button\" onclick=\"sendActivityMessge()\" class=\"btn bg-teal-400 btn-labeled btn-labeled-right\"><b><i class=\"icon-circle-right2\"></i></b> " + ressources['Send'] + " </button></div></div>";


    }
    return html;
}
function getTagsForAutocomplete() {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../API/Service.svc/GetAllTagNames',
        data: 1,
        processData: false,
        dataType: "json",
        success: function (tags) {
            Tags = tags;
            
            let res = [];
            tags.forEach(tag => {
                res.push(tag.Key);
            });
            $('#singleFieldTags2').tagit({
                tagSource: null,
                allowSpaces: true,
                afterTagAdded: function (event, ui) {
                    if (!ui.duringInitialization) {
                        if (jQuery.inArray(ui.tagLabel, res) !== -1) {
                            if (!_initTagAdd) {
                                __doPostUpdate(event.target, null);
                            }
                        } else {
                            $('#singleFieldTags2').tagit('removeTagByLabel', ui.tagLabel);
                            return false;
                        }
                    }
                }
            });

            $('.singleFieldTags').tagit({
                //availableTags: res
                tagSource: null,
                allowSpaces: true,
                afterTagAdded: function (event, ui) {
                    if (!ui.duringInitialization) { 
                        if (jQuery.inArray(ui.tagLabel, res) !== -1) {
                            setColor(Tags);
                            __doPostUpdate(event.target, null);
                        } else {
                            $('#' + event.target.id).tagit('removeTagByLabel', ui.tagLabel);
                            return false;
                        }
                    }
                },
                afterTagRemoved: function (event, ui) {
                    if (!ui.duringInitialization) {
                        if (jQuery.inArray(ui.tagLabel, res) !== -1) {
                            __doPostUpdate(event.target, null);
                        }
                    }
                }

            });

            setColor(Tags);
        },
        error: function (a, b, c) {
            //alert(a.responseText);
        }
    });
}


function setColor(tags) {
    let res = [];
    tags.forEach(tag => {
        res.push(tag.Key);
    });
    var x = document.getElementsByClassName("tagit-choice");
    var i;
    for (i = 0; i < x.length; i++) {
        let a = x[i].textContent.substring(0, x[i].textContent.length - 1);
        let ind = res.indexOf(a);
        if (ind >= 0) {
            x[i].style.backgroundColor = tags[ind].Value;
        }
    }
}


function DisplayDetails(row) {
    var s = row[0].innerText;
    var status = row[1].innerText;
    var dueDate = row[2].innerText;
    var priority = row[3].innerText;
    var timeline = row[4].innerText;
    var assignTo = row[5].innerText;
    var owner = row[6].innerText;
    $('#EditOwner').html(owner);
    $('#EditTimeline').html(timeline);
    $('#EditAssignedTo').html(assignTo);
    $('#EditPriority').html(priority);
    $('#EditDueDate').html(dueDate);
    $('#EditTitle').html(s);
}

function setPeriodFilter() {
    _periodFilterFrom = null;
    _periodFilterTo = null;

    var pval = $('#ddlPeriod').val();
    if (pval != null && pval.indexOf(';') > 0) {
        pval = pval.split(';');
        if (pval.length == 2) {
            _periodFilterFrom = pval[0];
            _periodFilterTo = pval[1];

            if (_periodFilterFrom != "*") _periodFilterFrom = parseInt(_periodFilterFrom);
            else _periodFilterFrom = null;

            if (_periodFilterTo != "*") _periodFilterTo = parseInt(_periodFilterTo);
            else _periodFilterTo = null;
        }
    }

    refreshLines();
}

function setClientFilter() {
    _clientFilter = $('#ddlAssignTo').val();

    refreshLines();
}

function setStatusFilter(newStatus) {
    _statusFilter = newStatus;
    refreshLines();
}

function refreshLines() {
    _dataTable.draw();



}


function refreshLiveStats() {

    var rating3sum = 0;
    var rating4sum = 0;
    var rating5sum = 0;
    var ratingsum = 0;

    var countQuoteDraft = 0;
    var countQuoteSent = 0;
    var countQuoteOutDated = 0;
    var countQuoteRefused = 0;
    var countQuoteAccepted = 0;



    var rowsId = _dataTable.rows().eq(0);
    for (var i = 0; i < rowsId.length; i++) {
        var row = _dataTable.row(rowsId[i]);

        if (row.node().attributes["data-anystatusfilter"] != null && row.node().attributes["data-anystatusfilter"].value == "true") {


            if (row.node().attributes["data-totalWithoutTaxes"] != null && row.node().attributes["data-projectstatus"] != null) {

                var rating = 5;
                if (DeliveryNote == false) {
                    rating = parseInt(row.node().attributes["data-rating"].value);
                }
                var total = parseFloat(row.node().attributes["data-totalWithoutTaxes"].value);
                var s = row.node().attributes["data-projectstatus"].value;


                if (s == "QuoteDraft" || s == "QuoteSent" || s == "QuoteAccepted" || s == "QuoteOutDated") {

                    if (DeliveryNote == true) {
                        ratingsum += total * 1;
                    }
                    else {
                        if (!isNaN(rating)) {
                            if (rating >= 1) {

                                var pcent = 0;
                                pcent = rating / 0.05;

                                var amount = total * pcent / 100;

                                ratingsum += amount;


                                if (rating == 3) rating3sum += amount;
                                if (rating == 4) rating4sum += amount;
                                if (rating == 5) rating5sum += amount;
                            }
                        }
                    }
                    $(row.node()).removeClass("no-rating");
                }
                else {
                    $(row.node()).addClass("no-rating");
                }
            }

            if (row.node().attributes["data-projectstatus"] != null) {
                var s = row.node().attributes["data-projectstatus"].value;
                if (s == "QuoteDraft") countQuoteDraft++;
                else if (s == "QuoteSent") countQuoteSent++;
                else if (s == "QuoteOutDated") countQuoteOutDated++;
                else if (s == "QuoteRefused") countQuoteRefused++;
                else if (s == "QuoteAccepted") countQuoteAccepted++;
            }


        }

    }

    $('#ContentPlaceHolder1_lblPipe3').html(formatMoney(rating3sum));
    $('#ContentPlaceHolder1_lblPipe4').html(formatMoney(rating4sum));
    $('#ContentPlaceHolder1_lblPipe5').html(formatMoney(rating5sum));
    $('#ContentPlaceHolder1_lblPipeGlobal').html(formatMoney(ratingsum));

    $('#ContentPlaceHolder1_lblCountDraft').html(countQuoteDraft);
    $('#ContentPlaceHolder1_lblCountSent').html(countQuoteSent);
    $('#ContentPlaceHolder1_lblCountOutdated').html(countQuoteOutDated);
    $('#ContentPlaceHolder1_lblCountRefused').html(countQuoteRefused);
    $('#ContentPlaceHolder1_lblCountAccepted').html(countQuoteAccepted);

    if (countQuoteOutDated > 0) $('#ContentPlaceHolder1_icoCountOutdated').addClass("animated")
    else $('#ContentPlaceHolder1_icoCountOutdated').removeClass("animated")

    if (countQuoteAccepted > 0) $('#ContentPlaceHolder1_icoCountAccepted').addClass("animated")
    else $('#ContentPlaceHolder1_icoCountAccepted').removeClass("animated")
}

function formatMoney(d) {
    d = d
        .toFixed(2)
        .replace('.', ',')
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ')
    return d + " " + currency;
}

$(function () {

    var projectId = 0;

    // client side filters
    $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {

            var visible = true;




            if (_clientFilter != null) {
                var clientMatch = false;
                for (var i = 0; i < _clientFilter.length; i++) {
                    if (_clientFilter[i] == settings.aoData[dataIndex].nTr.getAttribute('data-clientId')) clientMatch = true;
                }
                if (clientMatch == false) visible = false;
            }


            var lastUpdate = parseInt(settings.aoData[dataIndex].nTr.getAttribute('data-lastupdate'));
            if (_periodFilterFrom != null) {
                if (lastUpdate < _periodFilterFrom) visible = false;
            }
            if (_periodFilterTo != null) {
                if (lastUpdate >= _periodFilterTo) visible = false;
            }

            settings.aoData[dataIndex].nTr.setAttribute('data-anystatusFilter', visible);

            if (_statusFilter != null) {
                if (settings.aoData[dataIndex].nTr.getAttribute('data-ProjectStatus') != _statusFilter) visible = false;
            }

            return visible;
        }
    );
    // -->
    $("#ContentPlaceHolder1_grid").addClass("addgridclass");

    $('div#ContentPlaceHolder1_grid td').addClass("tdclassad");

    _dataTable = initDataTable($('#ContentPlaceHolder1_grid'), true);


    _dataTable.on('draw', function () {
        refreshLiveStats();
    });

    setPeriodFilter();



    // save button
    // ------------------------------------------------------------------------------------------------------------------------
    $('.btn-save').click(function () {

        var action = $(this).data('action');
        var buttonCall = $(this);

        saveProject(buttonCall, action);
    });


    saveProject = function (buttonCall, action) {
        $(buttonCall).attr("disabled", true);
        if (($('#ContentPlaceHolder1_TaskTile').val()) == "") {
            alert("You must enter title");
            $(buttonCall).attr("disabled", false);
            return;
        }
        var dueDate = null;
        var owner = $('#ddlOwner').val();
        var timline = $('#ContentPlaceHolder1_tbxTimelineDateRange').val();
        if ($('#ContentPlaceHolder1_tbxDueDate').length == 1)
            dueDate = $('#ContentPlaceHolder1_tbxDueDate').val();

        var rating = $('#ContentPlaceHolder1_ratingStarsQuote_s').val();


        var projectUpdate = {
            id: $("#ContentPlaceHolder1_TaskId").val(),
            action: action,
            title: $('#ContentPlaceHolder1_TaskTile').val(),
            dueDate: dueDate,
            description: $('#ContentPlaceHolder1_Description').val(),
            owner: $('#ddlOwner').val(),
            priority: $('#ContentPlaceHolder1_ddlPriority').val(),
            status: $('#ContentPlaceHolder1_ddlStatus').val(),
            progress: $('#ContentPlaceHolder1_ratingStarsQuote_s').val(),
            tags: $("#singleFieldTags2").tagit("assignedTags"),
            assignTo: $('#ddlAssignTo').val(),
            timeline: $('#ContentPlaceHolder1_tbxTimelineDateRange').val(),
            isPinned: $('#editIsPinned').val() == 'true'
        };

        var dta = JSON.stringify(projectUpdate);
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '../../API/Service.svc/UpdateTask',   //
            data: JSON.stringify(projectUpdate),
            processData: false,
            dataType: "json",
            success: function (response) {
                afterTaskSave(response);
                $(buttonCall).attr("disabled", false);

                location.reload();
            },
            error: function (a, b, c) {
            }
        });


    }
    // ------------------------------------------------------------------------------------------------------------------------>

    //save tag

    $('#btnTagSave').click(function () {
        var buttonCall = $(this);
        //$(buttonCall).attr("disabled", true);
        if (($('#ContentPlaceHolder1_TagName').val()) == "") {
            alert("You must enter name");
            $(buttonCall).attr("disabled", false);
            return;
        }
        var tagName = $('#ContentPlaceHolder1_TagName').val();
        var color = $('#ContentPlaceHolder1_TagColor').val();
        var tagType = null;
        if ($("#uniform-ContentPlaceHolder1_privateRadio span").hasClass("checked")) {
            tagType = 1;
        }
        else if ($("#uniform-ContentPlaceHolder1_sharedRadio span").hasClass("checked")) {
            tagType = 2;
        }

        var test = $("input[name='ctl00$ContentPlaceHolder1$radio-inline-left']:checked").val();

        var id = null;
        if ($("#ContentPlaceHolder1_TagId").val() === "") {
            id = 0;
        }
        else {
            id = $("#ContentPlaceHolder1_TagId").val();
        }

        var tagUpdate = {
            id: id,
            name: tagName,
            type: tagType,
            color: color,
            module: 1

        };

        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: '../../API/Service.svc/UpdateTag',   //
            data: JSON.stringify(tagUpdate),
            processData: false,
            dataType: "json",
            success: function (response) {
                afterTaskSave(response);
                $(buttonCall).attr("disabled", false);
                tagsList();
                getTagsForAutocomplete();
            },
            error: function (a, b, c) {
            }
        });
    });

    //------------------------------------------------------------------------------------------------------------------------- 

    //Delete tag

    $('#btnTagDelete').click(function () {

        var tagId = $("#ContentPlaceHolder1_TagId").val();
        if (tagId > 0) {

            deleteTag(tagId);

        }

    });
    //------------------------------------------------------------------------------------------------------------------------->


    function onChange(changes, source) {

        flagChanges(true);

        if (!changes) {
            return;
        }
        var instance = this;

        changes.forEach(function (change) {
            var row = change[0];
            var col = change[1];
            var newValue = change[3];
            var applyChange = false;
            var datarow = tableDataSource[row];
            if (col == "item") {

                var service = findService(newValue);
                if (service != null) {
                    datarow.price = service.unitPrice;
                    if (datarow.qty == null) datarow.qty = 1;
                    var tax = findTaxById(service.taxId);
                    if (tax != null) {
                        datarow.tax = tax.name;
                    }
                    applyChange = true;
                }
            }

            if (col == "qty" || col == "price" || col == "item" || col == "discount") {

                if (isNaN(datarow.qty) || isNaN(datarow.price)) {
                    datarow.totalprice = null;
                }
                else {
                    datarow.totalprice = toDouble(datarow.price) * toDouble(datarow.qty);
                    if (datarow.discount == null || isNaN(datarow.discount)) datarow.discount = 0;
                    datarow.totalprice = precisionRound(toDouble(datarow.totalprice) * (100 - toDouble(datarow.discount)) / 100, 2);
                }
                applyChange = true;
            }

            if (enableProjectFields && col == "selected") {
                // activer les controles de facturation partiels
                toggleSelection();
            }

            if (applyChange && instance.render != null) {
                instance.render();
            }
            recalcTotals();

        });
    }

});

function removeExistingSetTag() {
    $('#ContentPlaceHolder1_tbxDueDate').val(moment(new Date()).format('l')); 
    $('#modal_CreateTask1 .barrating').barrating('set', 1);
    $('#createTaskHeader').show();
    $('#editTaskHeader').hide();
    $('#singleFieldTags2').tagit('removeAll');
    $("#ContentPlaceHolder1_TaskId").val(0);
    $('#editIsPinned').val("False");
    var userId = 0;
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '../../API/Service.svc/GetSessionUserId',
        dataType: "json",
        success: function (res) {
            userId = res;
            $("#ddlOwner").val(userId).trigger('change');

            $('#ddlAssignTo').val(userId).trigger('change');
        },
        error: function (a, b, c) {
            //alert(a.responseText);
        }
    });
    $("#ContentPlaceHolder1_gridTaskId").val('');
    $('#ContentPlaceHolder1_TaskTile').val(null);
    $('#ContentPlaceHolder1_Description').val(null);
    // $('#ddlOwner').val();
    //$('#ddlOwner').select2().trigger('change');

    $('#ContentPlaceHolder1_ddlPriority').val(1);
    $('#ContentPlaceHolder1_ddlPriority').select2().trigger('change');

    $('#ContentPlaceHolder1_ddlStatus').val(2);
    $('#ContentPlaceHolder1_ddlStatus').select2().trigger('change');

    $('#ContentPlaceHolder1_ratingStarsQuote_s').val(1);
    $('#ContentPlaceHolder1_ratingStarsQuote_s').select().trigger('change');
    $("#singleFieldTags2").val();


    $(".progressBar").css('display', 'none');
    //date range..
    var dateRange = $('#ContentPlaceHolder1_tbxTimelineDateRange');
    var date = new Date();
    var dateTodayDate = moment(date.getTime()).format('l');
    var dateAddSeven = moment(date.setTime(date.getTime() + (7 * 24 * 60 * 60 * 1000))).format('l');
    $('#ContentPlaceHolder1_tbxTimelineDateRange').daterangepicker({
        showDropdowns: true,
        opens: "left",
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default',
        locale: {
            format: CALANI.DEFAULTS.dateFormat
        },
        startDate: dateTodayDate,
        endDate: dateAddSeven
    });
    // date End
    $("#taskMessageCreate").empty();
    $("#taskMessageEdit").empty();
}

//Update
function __doPostBack(eventTarget, eventArgument) {

    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;

        theForm.submit();
    }

}
function __doPostCancel(eventTarget, eventArgument) {

    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;

        theForm.submit();
    }


}
function __doPostUpdate(eventTarget, eventArgument) {
    var trObject = $(eventTarget).parent().closest('tr');
    var dataObject = {};
    var dataObject1 = {};
    $.each($(trObject).find("input,select,textarea"), function (i, obj) {
        dataObject1[$(obj).attr('inLineField')] = $(obj).val();
    });

    for (var key in dataObject1) {
        try {
            if (dataObject1.hasOwnProperty(key) && key != "undefined") {
                if (key == "assignTo") {
                    dataObject[key] = (dataObject1[key]).split(",");
                } else {
                    dataObject[key] = dataObject1[key];
                }
            }
        } catch (e) { }
    }



    for (var key in dataObject1) {

        if (dataObject1.hasOwnProperty(key) && key != "undefined") {
            if (key == "assignTo") {
                dataObject[key] = (dataObject1[key]).split(",");
            } else {
                dataObject[key] = dataObject1[key];
            }
            if (key == "isPinned") {
                dataObject[key] = (dataObject1[key] == 'True');
            }
        }
    }
    var tagList = [];

    if (dataObject.tagValue.indexOf(',') > -1) {
        tagList = dataObject.tagValue.split(',');
    }
    else {
        tagList.push(dataObject.tagValue);
    }

    dataObject.tags = tagList;

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: '../../API/Service.svc/UpdateTask',   //
        data: JSON.stringify(dataObject),
        processData: false,
        dataType: "json",
        success: function (response) {
            afterTaskSave(response);
        },
        error: function (a, b, c) {
            //alert(a.responseText);
        }
    });
}



function selected() {
    //Do your task here

    //Call the second function here
    EnableSaveButton();
}

function EnableSaveButton() {
    alert('Enable Save Button');
}
