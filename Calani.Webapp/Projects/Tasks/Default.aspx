﻿<%@ Page Title="<%$ Resources:Resource, Task %>" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Projects_Tasks_Default" EnableEventValidation="false" Codebehind="Default.aspx.cs" %>

<%@ Register Src="../../assets/Controls/RatingViewer.ascx" TagName="RatingViewer" TagPrefix="rv" %>
<%@ Register Src="../../assets/Controls/ProjectStatusLabel.ascx" TagName="ProjectStatusLabel" TagPrefix="psl" %>
<%@ Register Src="../../assets/Controls/ImportedInLink.ascx" TagName="ImportedInLink" TagPrefix="iil" %>
<%@ Register Src="~/assets/Controls/RatingStars.ascx" TagPrefix="uc1" TagName="RatingStars" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
</asp:Content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" runat="Server">

    <div class="btn-group" runat="server" id="quickmenu">
        <button type="button" class="btn bg-gesmobile btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="icon-menu7"></i>&nbsp;<span class="caret"></span>
        </button>

        <ul class="dropdown-menu dropdown-menu-right">
            <li><a href="javascript:void(0)" data-toggle="modal" onclick="removeExistingSetTag();" data-target="#modal_CreateTask1"><i class="icon-plus2"></i><%= Resources.Resource.Create %>  <%= LabelQuoteOrDeliveryNote %></a></li>
        </ul>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form runat="server">
        <div class="content">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="panel panel-white">

                        <div class="panel-heading">
                            <h6 class="panel-title">
                                <a name="quotes">
                                    <i class="icon-upload display-inline-block text-green"></i>
                                    <strong><%= Resources.Resource.Task %></strong>
                                </a>
                            </h6>
                        </div>
                        <!-- panel-heading -->

                        <div class="panel-body">
                            <asp:HiddenField ID="HiddenFieldTodo" runat="server" />
                            <asp:HiddenField ID="HiddenFieldArchive" runat="server" />
                            <asp:HiddenField ID="HiddenFieldPinned" runat="server" />
                            <ul class="nav nav-tabs nav-tabs-highlight">
                                <li id="liTodo" class="active" runat="server"><a href="#highlight-tab1" data-toggle="tab" onclick="fireTabEvent(0);"><%= Resources.Resource.Todo +(HiddenFieldTodo.Value!=""?" ("+HiddenFieldTodo.Value+")":"") %></a></li>
                                <li id="liArchive" class="" runat="server"><a href="#highlighted-tab2" data-toggle="tab" onclick="fireTabEvent(1);"><%= Resources.Resource.Archive +(HiddenFieldArchive.Value!=""?" ("+HiddenFieldArchive.Value+")":"") %></a></li>
                                <li id="liPinned" class="" runat="server"><a href="#highlighted-tab3" data-toggle="tab" onclick="fireTabEvent(2);"><%= Resources.Resource.Pinned+(HiddenFieldPinned.Value!=""?" ("+HiddenFieldPinned.Value+")":"") %></a></li>

                                <li>
                                    <asp:Button ID="btnToDoSubmit" runat="server" CommandArgument="0" Text="<%$ Resources:Resource, Todo %>" OnCommand="hdnTabBtn_Command" class="hide_details" /></li>
                                <li>
                                    <asp:Button ID="btnArchiveSubmit" runat="server" CommandArgument="1" Text="<%$ Resources:Resource, Archive %>" OnCommand="hdnTabBtn_Command" class="hide_details" /></li>
                                <li>
                                    <asp:Button ID="btnPinnedSubmit" runat="server" CommandArgument="2" Text="<%$ Resources:Resource, Pinned %>" OnCommand="hdnTabBtn_Command" class="hide_details" /></li>
                            </ul>
                            <asp:GridView runat="server" ID="grid" CssClass="TaskGrid-Class" AutoGenerateColumns="False" EnableViewState="False" OnRowCancelingEdit="grid_RowCancelingEdit" OnRowEditing="grid_RowEditing" OnRowUpdating="grid_RowUpdating">
                                <Columns>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Title %>">
                                        <ItemTemplate>
                                            <input inlinefield="id" type="text" runat="server" value='<%# Bind("id") %>' id="eitId" hidden="" />
                                            <input inlinefield="progress" type="text" runat="server" value='<%# Bind("progress") %>' id="editProgress" hidden="" />
                                            <input inlinefield="dueDate" type="text" runat="server" value='<%# Bind("dueDate") %>' id="editdueDate" hidden="" />
                                            <input inlinefield="isPinned" type="text" runat="server" value='<%# Bind("isPinned") %>' id="isPinned" hidden="" />
                                            <input type="text" runat="server" value='<%# Bind("id") %>' id="tId" hidden="" />
                                            <input inlinefield="title" type="text" runat="server" value='<%# Bind("title") %>' id="Text1" hidden="" />
                                            <a href="#">
                                                <asp:Label runat="server" HeaderText="Title" ID="lbltitle" Text='<%# Bind("title") %>'></asp:Label></a>
                                            <i class="icon-comment-discussion"></i>
                                            <input type="text" runat="server" value='<%# Bind("isMessage") %>' id="isMessageId" class="isMessage" hidden="" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Status %>">
                                        <ItemTemplate>
                                            <input inlinefield="description" type="text" runat="server" value='<%# Bind("description") %>' id="Text2" hidden="" />
                                            <a href="#" class="text-warning">
                                                <%# (!String.IsNullOrEmpty(Convert.ToString(Eval("isPinned"))) && Convert.ToString(Eval("isPinned")) == "True" ? "<i class='icon-pushpin'></i>" : "") %>
                                            </a>
                                            <div class="form-group mb-5">
                                                <input runat="server" id="lblStatus" inlinefield="status" class="lblStatusColor label label-flat border-success text-success-600" value='<%# Bind("status") %>' hidden="" />

                                                <select name="ddlStatusInLine" id="ddlStatusInLine" onchange="__doPostUpdate(this,'')" inlinefield="status" onmouseover="ddlToolTipsRegion(this,1)" class="select label label-flat border-danger text-danger-600 text-grey-600 ddlStatusColorItem">
                                                    <option value="1"><%= Resources.Resource.Draft %></option>
                                                    <option value="2"><%= Resources.Resource.Started %></option>
                                                    <option value="3"><%= Resources.Resource.Paused %></option>
                                                    <option value="4"><%= Resources.Resource.Done %></option>
                                                    <option value="5"><%= Resources.Resource.Cancelled  %></option>
                                                    <option value="6"><%= Resources.Resource.Archived %></option>
                                                </select>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="<%$ Resources:Resource, Date %>" DataField="dueDate" ReadOnly="True" DataFormatString="{0:d}"></asp:BoundField>


                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Priority %>">
                                        <ItemTemplate>

                                            <div class="form-group mb-5">
                                                <input type="text" runat="server" value='<%# Bind("prority") %>' id="ddlprorityInLineId" class="ddlPriorityId" hidden="" />
                                                <select name="ddlPriority" inlinefield="priority" id="ddlPriorityItem" onmouseover="ddlToolTipsRegion(this,2)" onchange="__doPostUpdate(this,'')" class="select label label-flat border-danger text-danger-600 ddlPriorityItem">
                                                    <option value="1"><%= Resources.Resource.Urgent %></option>
                                                    <option value="2"><%= Resources.Resource.Low %></option>
                                                    <option value="3"><%= Resources.Resource.Medium %></option>
                                                    <option value="4"><%= Resources.Resource.High %></option>
                                                    </optgroup>
                                                </select>
                                            </div>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Timeline %>">
                                        <ItemTemplate>
                                            <input type="text" inlinefield="timeline" runat="server" value='<%# Bind("timeline") %>' id="timelineInLineId" hidden="" />
                                            <asp:Label runat="server" ID="lblTimeline" Text='<%# Bind("timeline") %>'></asp:Label>
                                            <div>
                                                <progress id="myProgress" <%# ((int)Eval("progressPercentage") == 100) ? "class=\"progreesbargreen\"" : "" %> value="<%# Eval("progressPercentage") %>" max="100">
                                            </progress>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="150px" HeaderText="<%$ Resources:Resource, AssignedTo %>">
                                        <ItemTemplate>
                                            <input type="text" inlinefield="assignTo" runat="server" value='<%# Bind("assignedToValueList") %>' id="assignToInLineId" hidden="" />
                                           <%# Eval("assignedToValue") %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Owner %>">
                                        <ItemTemplate>
                                            <%# Eval("ownerName") %>
                                            <input type="text" inlinefield="owner" runat="server" value='<%# Bind("ownerId") %>' id="ddlOwnerInLineId" hidden="" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="<%$ Resources:Resource, Tag %>">
                                        <ItemTemplate>

                                            <div class="input-group tag-ul modaltacolorsection">
                                                <input id="tags<%# Eval("id") %>" name="tags" inlinefield="tagValue" class="singleFieldTags" width="800px" value='<%# Eval("tagValue") %>' readonly="readonly" />
                                                <span class="input-group-addon"><i onclick="tagsList($(event.target))" class="icon-plus3 cursor-pointer"></i></span>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>

                            </asp:GridView>



                        </div>
                        <!-- panel-body -->

                        <div class="panel-footer text-right no-padding">
                            <div class="row margin-footer">
                                <a href="javascript:void(0)" data-toggle="modal" onclick="removeExistingSetTag();" data-target="#modal_CreateTask1"><i class="icon-plus2 position-left"></i><%= Resources.Resource.Create %> <%= LabelQuoteOrDeliveryNote %></a>

                            </div>
                        </div>
                        <!-- panel-footer -->

                    </div>
                </div>

            </div>
        </div>
        <!-- /content -->

        <!-- basic modal: create task -->
        <div id="modal_CreateTask1" data-backdrop="static" data-keyboard="false" class="modal fade modal-scroll">
            <div class="modal-dialog modal-l">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title text-center" id="createTaskHeader"><%= Resources.Resource.CreateTask %></h5>
                        <h5 class="modal-title text-center" id="editTaskHeader"><%= Resources.Resource.EditTask %></h5>

                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-md-12 form-group">
                                <h6><%= Resources.Resource.Title %></h6>
                                <div class="form-group">
                                    <input type="text" id="TaskTile" name="TaskTile" runat="server" maxlength="45" class="form-control" />
                                </div>
                                <label id="TaskTile-error" class="validation-error-label hide_details" for="TaskTile"><%= Resources.Resource.FieldIsRequired %></label>
                                <div id="divPin" class="heading-elements change-pin">
                                    <input id="editIsPinned" value="" hidden="" />

                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <h6><%= Resources.Resource.Description %></h6>
                                <div class="form-group">
                                    <textarea type="text" id="Description" name="Description" cols="10" rows="3" runat="server" maxlength="45" class="form-control"></textarea>
                                </div>

                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div>
                                    <h6><%= Resources.Resource.Owner %></h6>
                                </div>
                                <div>
                                    <select id="ddlOwner" class="select">
                                        <%= OptionsContacts %>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div>
                                    <h6><%= Resources.Resource.Priority %></h6>
                                </div>
                                <div>
                                    <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlPriority">
                                        <asp:ListItem Value="1" Text='<%$ Resources:Resource, Urgent %>' />
                                        <asp:ListItem Value="2" Text='<%$ Resources:Resource, Low %>' />
                                        <asp:ListItem Value="3" Text='<%$ Resources:Resource, Medium %>' />
                                        <asp:ListItem Value="4" Text='<%$ Resources:Resource, High %>' />
                                    </asp:DropDownList>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div>
                                    <h6><%= Resources.Resource.AssignedTo %></h6>
                                </div>
                                <div class="assign_to">
                                    <select id="ddlAssignTo" data-placeholder="<%= Resources.Resource.Select___ %>" multiple="multiple" class="select">
                                        <%= OptionsContacts %>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div>
                                    <h6><%= Resources.Resource.DueDate %></h6>
                                </div>
                                <div class="input-group" runat="server">
                                    <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                    <input type="text" step="1" runat="server" value="" class="form-control daterange-single" id="tbxDueDate" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div>
                                    <h6><%= Resources.Resource.Timeline %></h6>
                                </div>
                                <div class="input-group" runat="server">
                                    <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                    <input type="text" runat="server" value="01/01/2020 - 07/01/2020" class="form-control daterange-basic" id="tbxTimelineDateRange" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div>
                                    <h6><%= Resources.Resource.Status %></h6>
                                </div>
                                <div>
                                    <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlStatus">
                                        <asp:ListItem Value="1" Text='<%$ Resources:Resource, Draft %>' />
                                        <asp:ListItem Value="2" Text='<%$ Resources:Resource, Started %>' />
                                        <asp:ListItem Value="3" Text='<%$ Resources:Resource, Paused %>' />
                                        <asp:ListItem Value="4" Text='<%$ Resources:Resource, Done %>' />
                                        <asp:ListItem Value="5" Text='<%$ Resources:Resource, Cancelled %>' />
                                        <asp:ListItem Value="6" Text='<%$ Resources:Resource, Archived %>' />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" runat="server" id="divQuoteField_rating" data-popup="tooltip" title='<%$ Resources:Resource, QuoteRating_Tooltip %>' data-placement="right">
                                    <div>
                                        <h6><%= Resources.Resource.Progress %></h6>
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-star-half-empty"></i></span>
                                        <div class="form-control">
                                            <uc1:RatingStars runat="server" ID="ratingStarsQuote" />
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div>
                                    <h6><%= Resources.Resource.Tags %></h6>
                                </div>
                                <div class="input-group tag-ul modaltacolorsection ">
                                    <%--<select id="ddlTags" data-placeholder="<%= Resources.Resource.Select___ %>" multiple="multiple" class="select">
                                        <%= OptionsTags %>
                                    </select>--%>
                                    <input name="tags" id="singleFieldTags2" value="" readonly="readonly" />
                                    <span class="input-group-addon cursor-pointer" onclick="tagsList()" ><i class="icon-plus3"></i></span>
                                </div>


                            </div>

                        </div>
                        <%-- <div id="taskMessageCreate" class="panel-body"></div>--%>
                        <div class="progressBar progress-xs">
                            <div class="bar"></div>
                            <div class="percent progress-percent-width">0%</div>
                        </div>
                        <input type="text" id="TaskId" value="" runat="server" hidden="" />
                        <input type="text" id="pinUnpin" value="" runat="server" hidden="" />
                        <div class="modal-footer pt-20 pr-5 pl-5">
                            <button type="button" id="btnTaskSave" class="btn btn-primary btn-save" data-action="save"><%= Resources.Resource.Save %></button>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><%= Resources.Resource.Close %></button>
                        </div>
                    </div>


                </div>



            </div>
        </div>
        <!-- /basic modal -->

        <!--Start Tag List Modal -->
        <div id="tagList" data-backdrop="static" data-keyboard="false" class="modal fade modal-scroll" role="dialog">
            <div class="modal-dialog modal-sm">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" id="tagListClose"class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title text-center"><%= Resources.Resource.Tags %></h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="text" id="searchTag" placeholder="<%$ Resources:Resource ,BrowseTheTag %>" runat="server" onkeyup="searchTagFilter('id','div')" class="form-control border-primary" />
                            </div>

                        </div>
                        <div class="row">
                            <div id="tabs" class="col-md-12 mt-10">
                                <ul class="nav nav-tabs nav-tabs-highlight tabs">
                                    <li id="private" class="active text-bold" runat="server"><a href="#highlight-tab1" data-id="1"  data-toggle="tab" onclick="tagTabEvent(event, 'PrivateTab');"><%= Resources.Resource.Private %></a></li>
                                    <li id="shared" class="text-bold" runat="server"><a href="#highlight-tab2" data-id="2"  data-toggle="tab" onclick="tagTabEvent(event, 'SharedTab');"><%= Resources.Resource.Shared %></a></li>

                                </ul>
                                <div id="PrivateTab" class="tabcontent active">
                                    <div id="privateTagDropdown">
                                        <button onclick="createTag('PrivateTab')" type="button" class="form-control mt-10"><i class="icon-plus3 mr-10"></i><%= Resources.Resource.Create_A_Private_Tag %></button>
                                        <h6><%= Resources.Resource.Tags %></h6>
                                        <div id="privatetagList">
                                        </div>
                                    </div>
                                </div>
                                <input type="text" id="gridTaskId" runat="server" value="" hidden="" />
                                <div id="SharedTab" class="tabcontent hide_details">
                                    <div id="sharedTagDropdown" >
                                        <button onclick="createTag('SharedTab')" type="button" class="form-control mt-10"><i class="icon-plus3 mr-10"></i><%= Resources.Resource.Create_A_Shared_Tag %></button>
                                        <h6><%= Resources.Resource.Tags %></h6>
                                        <div id="sharedtagList">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <%-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--%>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--Start Tag List Modal -->

        <!-- basic modal: edit task -->
        <div id="modal_EditTask" data-backdrop="static" data-keyboard="false" class="modal fade">

            <div class="modal-dialog modal-small" id="divDisplayTaskDetails">
                <div class="modal-content">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h6 class="panel-title text-center"><%=Resources.Resource.TaskDetails %><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <button type="button" id="editClose" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body form-horizontal">
                            <form class="form-horizontal" action="#">
                                <input type="text" id="txtTaskDisplay" value="True" hidden=""/>
                                <div class="row">
                                    <div class="col-md-7 col-sm-7 col-xs-7">
                                        <div class="form-group mb-5">
                                            <label class="col-lg-4"><%= Resources.Resource.Title %>:</label>
                                            <div class="col-lg-8">
                                                <div class="" id="lblTaskDisplayTitle"></div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <div class="text-right">
                                            <ul class="icons-list">
                                                <li><a onclick="pinTask()"><i id="pinTask" class="icon-pushpin"></i><i id="unpinTask" class="fa fa-thumb-tack"></i></a></li>
                                                <li></li>
                                                <%--<li><a onclick="editTask()">
                                                <label class="mt-5"><%= Resources.Resource.EditTask %></label>
                                                <i class="icon-pencil7"></i></a></li>--%>
                                                <li>
                                                    <button onclick="editTask()" type="button" class="btn bg-teal-400"><%= Resources.Resource.EditTask %> <i class="icon-pencil7"></i></button>
                                                </li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label><%= Resources.Resource.Description %>:</label>
                                        <textarea class="form-control-static col-md-12 desc-width" readonly id="lblTaskDisplayDescription"></textarea>

                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-8">
                                        <div class="form-group mb-5">
                                            <label class="col-lg-4 "><%= Resources.Resource.Owner %>:</label>
                                            <div class="col-lg-8">
                                                <label class="" id="lblEditDisplayOwner"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group mb-5">
                                            <label class="col-lg-4"><%= Resources.Resource.Priority %>:</label>
                                            <div class="col-lg-8">
                                                <label class="" id="lblEditDisplayPriority"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group mb-5">
                                            <label class="col-lg-4"><%= Resources.Resource.AssignedTo %>:</label>
                                            <div class="col-lg-8">
                                                <label class="" id="lblEditDisplayAssignTo"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group mb-5">
                                            <label class="col-lg-4"><%= Resources.Resource.Timeline %>:</label>
                                            <div class="col-lg-8">
                                                <label class="" id="lblEditDisplayTimline"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group mb-5">
                                            <label class="col-lg-5"><%= Resources.Resource.DueDate %>:</label>
                                            <div class="col-lg-7">
                                                <label class="" id="lblEditDisplayDueDate"></label>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-md-8">
                                    <div class="content-group">
                                            <label><%= Resources.Resource.Status %>:</label>
                                            <asp:DropDownList runat="server" CssClass="form-control select" ID="DropDownListstatus">
                                                <asp:ListItem Value="1" Text='<%$ Resources:Resource, Draft %>' />
                                                <asp:ListItem Value="2" Text='<%$ Resources:Resource, Started %>' />
                                                <asp:ListItem Value="3" Text='<%$ Resources:Resource, Paused %>' />
                                                <asp:ListItem Value="4" Text='<%$ Resources:Resource, Done %>' />
                                                <asp:ListItem Value="5" Text='<%$ Resources:Resource, Cancelled %>' />
                                                <asp:ListItem Value="6" Text='<%$ Resources:Resource, Archived %>' />
                                            </asp:DropDownList>                                        </div>
                                </div>
                                   
                                    <div class="col-md-4">
                                        <div class="form-group" runat="server" id="div2" data-popup="tooltip" title='<%$ Resources:Resource, QuoteRating_Tooltip %>' data-placement="right">

                                            <label class="col-lg-4"><%= Resources.Resource.Progress %></label>
                                                <div class="input-group col-lg-8">
                                                <span class="input-group-addon"><i class="fa fa-star-half-empty"></i></span>
                                                <div class="form-control">
                                                    <uc1:RatingStars runat="server" ID="ratingStars2" />
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                <div class="col-md-12">
                                    <label class="col-md-1"><%= Resources.Resource.Tags %>:</label>
                                    <div class="col-md-11" id="tagListReadOnly"></div>
                                </div>
                                
                                <div class="">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <hr class="mt-5 mb-5"/>
                                            <div class="panel panel-flat timeline-content">
                                                <div class="panel-heading pt-5 pb-5">
										            <h6 class="panel-title text-semibold">Activity</h6>
									            </div>
                                                <div id="taskMessageEdit" class="panel-body"></div>


                                                <div class="progressBar progress-xs show_details">
                                                    <div class="bar"></div>
                                                    <div class="percent progress-percent-width" >0%</div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                    </div>
                            </form>
                            <%-- <div id="taskMessage" class="panel-body"></div>--%>
                        </div>



                    </div>
                </div>

            </div>
        </div>
        <!-- /basic modal -->


        

        <!--Start Create Tag Modal -->
        <div id="modal_CreateTag" class="modal fade">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title text-center" id="createTagHeader"><%= Resources.Resource.CreateTag %></h5>
                        <h5 class="modal-title text-center" id="editTagHeader"><%= Resources.Resource.EditTag %></h5>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-md-12 form-group">
                                <h6><%= Resources.Resource.Name %></h6>
                                <div>
                                    <input type="text" id="TagName" name="TagName" runat="server" maxlength="45" class="form-control border-primary" />
                                </div>
                                <label id="TagName-error" class="validation-error-label hide_details" for="TagName"><%= Resources.Resource.FieldIsRequired %></label>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="display-block text-semibold"><%= Resources.Resource.Type_Of_Tag %></label>
                            <label class="radio-inline">
                                <input type="radio" name="radio-inline-left" class="styled" runat="server" id="privateRadio" use="privateSharedradio" value="1" />
                                <%= Resources.Resource.Private %>
                            </label>

                            <label class="radio-inline">
                                <input type="radio" name="radio-inline-left" class="styled" runat="server" id="sharedRadio" use="privateSharedradio" value="2" />
                                <%= Resources.Resource.Shared %>
                            </label>                           
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <h6><%= Resources.Resource.Color %></h6>
                                <input type="text" id="TagColor" name="TagColor" data-preferred-format="hex" class="form-control colorpicker-basic" value="" data-fouc="" runat="server" />
                            </div>
                        </div>

                        <input type="text" id="TagId" runat="server" hidden="" />

                    </div>
                    <div class="modal-footer">
                        <div class="col-md-3  text-left row">
                            <button type="button" id="btnTagDelete" class="btn border-slate text-slate-800 bg-white" data-action="delete"><%= Resources.Resource.Delete %></button>
                        </div>
                         <div class="col-md-9 ml-20 pr-5">
                             <button type="button" id="btnTagSave" class="btn btn-primary" data-action="save"><%= Resources.Resource.Create %></button>
                         </div>
                    </div>
                </div>
            </div>
        </div>

        <!--End Create Tag Modal -->


    </form>
    <script type="text/javascript">

        $('#tabUL a[href="#private"]').trigger('click');
        var currency = '<%= ph.Currency %>';
        var DeliveryNote = <%= DeliveryNote.ToString().ToLower() %>;
        //var taskId = $("#ContentPlaceHolder1_TaskId").val();
        //if (taskId ==="") {
        //    $('#divPin').css('display', 'none');
        //}
        var tagId = $("#ContentPlaceHolder1_TagId").val();
        if (tagId > 0) {
            $('#createTagHeader').hide();
            $('#editTagHeader').show();
            $('#btnTagDelete').show();
        } else {
            $('#createTagHeader').show();
            $('#editTagHeader').hide();
            $('#btnTagDelete').hide();
        }

        $("#editClose").click(function () {
            $("#<%=btnToDoSubmit.ClientID%>").trigger("click");
        });
        
        $("#tagListClose").click(function () {
            var gridTaskId = $("#ContentPlaceHolder1_gridTaskId").val();
            if (gridTaskId !== '') {
                $("#<%=btnToDoSubmit.ClientID%>").trigger("click");
            }
            
        });

        var ressources = [];
        ressources['Success'] = "<%= Resources.Resource.Success %>";
        ressources['Error'] = "<%= Resources.Resource.Error %>";
        ressources['OK'] = "<%= Resources.Resource.OK %>"
        function validateCreateTaskRec() {
            res = true;
            var title = $('#<%= TaskTile.ClientID %>').val();
            if (!title) {
                $('#TaskTile-error').show();
                res = false;
            }

            else {
                $('#TaskTile-error').hide();
                res = true;
            }

            return res;

        }
        function afterTaskSave(response) {

            if (!response.success) {
                var msg = response.message;
                if (ressources[msg] != null) msg = ressources[msg];
                //alert(msg); // todo une alerte plus sexy serait cool
                swal({
                    title: ressources['Error'],
                    text: msg,
                    type: "error",
                    confirmButtonColor: "#4CAF50",
                    confirmButtonText: ressources['OK']
                });


            }
            else {
                var smsg = response.message;
                swal({
                    title: ressources['Success'],
                    text: ressources[smsg],
                    type: "success",
                    confirmButtonColor: "#4CAF50",
                    confirmButtonText: ressources['OK']
                });
            }

            $('#modal_CreateTask').modal('hide');
            $('#modal_CreateTag').modal('hide');
        }


        function deleteTag(tagId) {
            swal({
                title: "<%= Resources.Resource.Are_You_Sure %>",
                text: "<%= Resources.Resource.The_Tag_Will_Be_Removed_From_All_Tags %>",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "<%= Resources.Resource.Cancel %>",
                confirmButtonColor: "#4CAF50",
                confirmButtonText: ressources['OK']
            })

                .then((value) => {

                    if (value.value !== null && value.value === true) {

                        $.ajax({
                            type: "GET",
                            contentType: "application/json; charset=utf-8",
                            url: '../../API/Service.svc/DeleteTag?tagId=' + tagId,
                            dataType: "json",
                            success: function (response) {

                                console.log("dd", response);
                                tagsList();
                                if (!response.success) {
                                    var msg = response.message;
                                    if (ressources[msg] != null) msg = ressources[msg];
                                    //alert(msg); // todo une alerte plus sexy serait cool
                                    swal({
                                        title: ressources['Error'],
                                        text: msg,
                                        type: "error",
                                        confirmButtonColor: "#4CAF50",
                                        confirmButtonText: ressources['OK']
                                    });
                                }
                                else {
                                    var smsg = response.message;

                                    swal({
                                        title: "<%= Resources.Resource.Success %>",
                                        text: "<%= Resources.Resource.DeletedSuccessfully %>",
                                        type: "success",
                                        confirmButtonColor: "#4CAF50",
                                        confirmButtonText: ressources['OK']
                                    });
                                    $('#modal_CreateTag').modal('hide');
                                    
                                }
                            },
                            error: function (a, b, c) {
                            }
                        });
                    }
                });
        }
        function setPinUnpin(isPinned, taskId) {
            if (isPinned == 'true') {
                swal({
                    title: "<%= Resources.Resource.Are_You_Sure %>",
                    text: "<%= Resources.Resource.Do_You_Want_To_Unpin_This_Task %>",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "<%= Resources.Resource.Cancel %>",
                    confirmButtonColor: "#4CAF50",
                    confirmButtonText: "<%= Resources.Resource.OK %>"
                })

                    .then((value) => {

                        if (value.value != null && value.value == true) {

                            $.ajax({
                                type: "GET",
                                contentType: "application/json; charset=utf-8",
                                url: '../../API/Service.svc/PinUnPinTask?TaskId=' + taskId,
                                dataType: "json",
                                success: function (res) {
                                    if (res) {
                                        $('#ContentPlaceHolder1_pinUnpin').val(res);
                                        $('#pinTask').css("display", "none");
                                        $('#unpinTask').css("display", "block");
                                        $('#unpinTask').css("color", '#fd710d');
                                    }
                                    else {
                                        $('#unpinTask').css("display", "none");
                                        $('#pinTask').css("display", "block");
                                        $('#pinTask').css("color", '#333');

                                    }
                                }
                            });
                        }

                    });



            }
            else {

                swal({
                    title: "<%= Resources.Resource.Are_You_Sure %>",
                    text: "<%= Resources.Resource.Do_You_Want_To_Pin_This_Task %>",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "<%= Resources.Resource.Cancel %>",
                    confirmButtonColor: "#4CAF50",
                    confirmButtonText: "<%= Resources.Resource.OK %>"
                })

                    .then((value) => {

                        if (value.value != null && value.value == true) {

                            $.ajax({
                                type: "GET",
                                contentType: "application/json; charset=utf-8",
                                url: '../../API/Service.svc/PinUnPinTask?TaskId=' + taskId,
                                dataType: "json",
                                success: function (res) {
                                    $('#ContentPlaceHolder1_pinUnpin').val(res);
                                    if (res) {
                                        $('#pinTask').css("display", "none");
                                        $('#unpinTask').css("display", "block");
                                        $('#unpinTask').css("color", '#fd710d');
                                    }
                                    else {
                                        $('#unpinTask').css("display", "none");
                                        $('#pinTask').css("display", "block");
                                        $('#pinTask').css("color", '#333');
                                    }
                                }
                            });
                        }

                    });


            }
        }

        function fireTabEvent(tab) {
            if (tab == 0) {
                $("#<%=btnToDoSubmit.ClientID%>").trigger("click");
            }
            else if (tab == 1) {
                $("#<%=btnArchiveSubmit.ClientID%>").trigger("click");
            }
            else if (tab == 2) {
                $("#<%=btnPinnedSubmit.ClientID%>").trigger("click");
            }
        }

        var selectedTab = "PrivateTab";

        function tagTabEvent(evt, tabName) {
            $("#ContentPlaceHolder1_searchTag").val('');
            if (tabName == "PrivateTab") {
                selectedTab = "PrivateTab";
            }
            else if (tabName == "SharedTab") {
                selectedTab = "SharedTab";
            }

            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(tabName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        function searchTagFilter() {
            var input, filter, ul, li, a, i, span;
            input = $("#ContentPlaceHolder1_searchTag").val();
            filter = input.toUpperCase();
            var div = null; 

            if (selectedTab == "PrivateTab") {
                div = document.getElementById("privateTagDropdown");
            }
            else{
                div = document.getElementById("sharedTagDropdown");
            }

            option = div.getElementsByTagName("option");
            span = div.getElementsByTagName("span");
            for (i = 0; i < option.length; i++) {
                txtValue = option[i].textContent || option[i].innerText;

                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    span[i].style.display = "";
                    option[i].style.display = "inline-block";

                } else {
                    span[i].style.display = "none";
                    option[i].style.display = "none";
                }
            }
        }

    </script>

    <script type="text/javascript">



        var ressources = {};
        ressources['Send'] = "<%= Resources.Resource.Send %>";
        ressources['EnterYourMessage1'] = "<%= Resources.Resource.EnterYourMessage %>";
        ressources['Excel'] = "<%= Resources.Resource.Excel %>";

        ressources['Draft'] = "<%= Resources.Resource.Draft %>";
        ressources['Started'] = "<%= Resources.Resource.Started %>";
        ressources['Paused'] = "<%= Resources.Resource.Paused %>";
        ressources['Done'] = "<%= Resources.Resource.Done %>";
        ressources['Cancelled'] = "<%= Resources.Resource.Cancelled %>";
        ressources['Archived'] = "<%= Resources.Resource.Archived %>";

        ressources['Urgent'] = "<%= Resources.Resource.Urgent %>";
        ressources['Low'] = "<%= Resources.Resource.Low %>";
        ressources['Medium'] = "<%= Resources.Resource.Medium %>";
        ressources['High'] = "<%= Resources.Resource.High %>";
        ressources['SavedSuccessfully'] = "<%= Resources.Resource.SavedSuccessfully %>";
        ressources['TagAlreadyExist'] = "<%= Resources.Resource.TagAlreadyExist %>";
        ressources['UpdatedSuccessfully'] = "<%= Resources.Resource.UpdatedSuccessfully %>";
        ressources['OK'] = "<%= Resources.Resource.OK %>";


    </script>
    <script type="text/javascript" src="Default.js?<%= Guid.NewGuid().ToString("n") %>">
    </script>

</asp:Content>

