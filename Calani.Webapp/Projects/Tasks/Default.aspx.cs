﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

using Calani.BusinessObjects.Projects;
using Calani.BusinessObjects.Tasks;
using Calani.BusinessObjects.CustomerAdmin;

public partial class Projects_Tasks_Default : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    public string OptionsContacts { get; set; }
    public string OptionsPeriod { get; set; }

    public string OptionsTags { get; set; }



    public bool DeliveryNote
    {
        get
        {
            return false;
        }
    }

    public string LabelQuoteOrDeliveryNote
    {
        get
        {
            return Resources.Resource.Task;
        }
    }


    public string PotentialToolTip
    {
        get
        {
            if (!DeliveryNote) return Resources.Resource.Pipe_Potential_Tooltip;
            return String.Empty;
        }
    }

    public string PotentialLabel
    {
        get
        {
            if (!DeliveryNote) return Resources.Resource.Pipe_Potential;
            return Resources.Resource.Total;
        }
    }

    private OrganizationManager OrgMgr { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        OrgMgr = new OrganizationManager(ph.CurrentOrganizationId);
        OrgMgr.Load();

        OptionsContacts = ph.RenderContactListOption();
        OptionsPeriod = ph.RenderPeriodsListOption(preselect: OrgMgr.DefaultPeriodFilter);

        OptionsTags = ph.RenderTagListOption();

        TasksManager tsk = new TasksManager(ph.CurrentOrganizationId);

        GridUpdate(tsk.ListTasks(userId: ph.CurrentUserId));

        HiddenFieldTodo.Value = grid.Rows.Count.ToString();
    }

    protected void grid_RowEditing(object sender, GridViewEditEventArgs e)
    {
        grid.EditIndex = e.NewEditIndex;
        grid.DataBind();

    }

    protected void grid_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grid.EditIndex = -1;
        int colorder = 6;
        List<string> rowData = new List<string>() { "clientId", "ProjectStatus", "lastUpdate", "rating", "totalWithoutTaxes" };
        new Html5GridView(grid, false, rowData, colorder + ";desc");
    }

    protected void grid_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }

    protected void hdnTabBtn_Command(object sender, CommandEventArgs e)
    {
        OptionsContacts = ph.RenderContactListOption();
        OptionsPeriod = ph.RenderPeriodsListOption(preselect: OrgMgr.DefaultPeriodFilter);

        OptionsTags = ph.RenderTagListOption();
        
        TasksManager tsk = new TasksManager(ph.CurrentOrganizationId);
        string value = e.CommandArgument.ToString();
        int tab = int.Parse(value);

        GridUpdate(tsk.ListTasks(ph.CurrentUserId, type: (enumTaskType)tab));
        if (tab==0)
        {
            HiddenFieldTodo.Value = grid.Rows.Count.ToString();
            HiddenFieldArchive.Value = null;
            HiddenFieldPinned.Value = null;
        }
        else if (tab == 1)
        {
            HiddenFieldArchive.Value = grid.Rows.Count.ToString();
            HiddenFieldTodo.Value = null;
            HiddenFieldPinned.Value = null;
        }
        else if (tab == 2)
        {
            HiddenFieldPinned.Value = grid.Rows.Count.ToString();
            HiddenFieldArchive.Value = null;
            HiddenFieldTodo.Value = null;
        }
        TabEnabled(tab);
    }

    private void TabEnabled(int tab)
    {
        switch (tab)
        {
            case 1:
                liTodo.Attributes.Add("class", "");
                liArchive.Attributes.Add("class", "active");
                liPinned.Attributes.Add("class", "");
                break;
            case 2:
                liTodo.Attributes.Add("class", "");
                liArchive.Attributes.Add("class", "");
                liPinned.Attributes.Add("class", "active");
                break;
            default:
                liTodo.Attributes.Add("class", "active");
                liArchive.Attributes.Add("class", "");
                liPinned.Attributes.Add("class", "");
                break;
        }
    }

    public void GridUpdate(List<TasksExtended> tasksExtendeds)
    {
        grid.DataSource = tasksExtendeds;
        List<string> rowData = new List<string>() { "clientId", "ProjectStatus", "lastUpdate", "rating", "totalWithoutTaxes" };
        int colorder = 6;


        if (DeliveryNote)
        {

            rowData.Remove("rating");
            colorder--;
            foreach (var col in grid.Columns)
            {
                TemplateField tf = col as TemplateField;
                if (tf != null && tf.ItemStyle != null && tf.ItemStyle.CssClass != null && tf.ItemStyle.CssClass.Contains("col-rating"))
                {
                    tf.Visible = false;
                }
            }
        }
        new Html5GridView(grid, false, rowData, colorder + ";desc");



        // page not available for employee
        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            Response.Redirect("~/");

        }
    }
}