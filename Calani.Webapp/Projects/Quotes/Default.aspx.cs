﻿using Calani.BusinessObjects.CustomerAdmin;
using System;
using System.Web.UI.WebControls;

public partial class Projects_Quotes_Default : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    public string OptionsClients { get; set; }
    public string OptionsPeriod { get; set; }

    public long AssigneeId = 0;

    public bool RoundAmount { get; set; }
    public bool UsePriceIncludingVat { get; set; }
    public bool DeliveryNote
    {
        get
        {
            return false;
        }
    }

    public string LabelQuoteOrDeliveryNote
    {
        get
        {
            return Resources.Resource.Quote;
        }
    }


    public string PotentialToolTip
    {
        get
        {
            if (!DeliveryNote) return Resources.Resource.Pipe_Potential_Tooltip;
            return String.Empty;
        }
    }

    public string PotentialLabel
    {
        get
        {
            return UsePriceIncludingVat ? Resources.Resource.TotalTTC : Resources.Resource.TotalHT;
        }
    }





    protected void Page_Load(object sender, EventArgs e)
    {
        var orgMgr = new OrganizationManager(ph.CurrentOrganizationId);
        orgMgr.Load();

        OptionsClients = ph.RenderCustomerListOption();
        OptionsPeriod = ph.RenderPeriodsListOption(preselect: orgMgr.DefaultPeriodFilter);
        UsePriceIncludingVat = orgMgr.UsePriceIncludingVat;

        RoundAmount = orgMgr.RoundAmount;

        // page not available for employee
        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            Response.Redirect("~/");
        }
        // -->

    }

    protected void grid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (RoundAmount)
            e.Row.Cells[4].Visible = false;
        else
            e.Row.Cells[3].Visible = false;
    }

}