﻿var _periodFilterFrom = null;
var _periodFilterTo = null;
var _clientFilter = null;
var _statusFilter = 'QuoteInProgress';

var _dataTable = null;

function setClientFilter() {
    _clientFilter = $('#ddlClient').val();
    refreshLines();
}

function refreshLines() {
    _dataTable.draw();
}

function checkField(val, str){
    return val?.toString().toLowerCase().includes(str.toLowerCase());
}
function containsString (obj, str) {
    if (checkField(obj.Ids.InternalId, str)) return true;
    if (checkField(obj.ClientName, str)) return true;
    if (checkField(obj.Description, str)) return true;
    if (checkField(obj.TotalHt, str)) return true;
    if (checkField(getProjectStatusNamable(obj.Status), str)) return true;
    if (checkField(obj.DateStr?.split(' ')[0], str)) return true;
    if (checkField(obj.LastUpdateDateStr?.split(' ')[0], str)) return true;
    if (checkField(obj.Rating, str)) return true;

    return false;
}

function refreshLiveStats() {
    
    let rating1sum = 0;
    let rating2sum = 0;
    let rating3sum = 0;
    let rating4sum = 0;
    let rating5sum = 0;
    let ratingsum = 0;

    let countQuoteDraft = 0;
    let countQuoteSent = 0;
    let countQuoteOutDated = 0;
    let countQuoteRefused = 0;
    let countQuoteAccepted = 0;
    let countMissingChild = 0;

    let rowsId = _dataTable.rows().eq(0);

    for (let i = 0; i < rowsId.length; i++) {
        let row = _dataTable.row(rowsId[i]);
        let data = row.data();

        if (data["Total"] != null && data["Status"] != null) {

            let rating = 5;
            if (DeliveryNote == false) {
                rating = data["Rating"];
            }

            let total = data["TotalHt"];
            let rowStatus = data["Status"];
            let mc = data["MissingChild"]

            let visible = row.node().getAttribute('data-anystatusFilter') == "true";

            if (visible &&
                (_statusFilter === null || rowStatus == _statusFilter || (_statusFilter == "QuoteInProgress" && (rowStatus == "QuoteSent" || rowStatus == "QuoteOutDated"))
                || _statusFilter == "QuoteMissingChild" && mc == "True"))
            {
                
                if (DeliveryNote == true)
                {
                    ratingsum += total * 1;
                }
                else
                {
                    if (!isNaN(rating))
                    {
                        if (rating >= 1) {
                            

                            var amount = total;

                            ratingsum += amount;

                            if (rating == 1)
                                rating1sum += amount;

                            if (rating == 2)
                                rating2sum += amount;

                            if (rating == 3)
                                rating3sum += amount;

                            if (rating == 4)
                                rating4sum += amount;

                            if (rating == 5)
                                rating5sum += amount;
                        }
                    }
                }
                $(row.node()).removeClass("no-rating");
            }
            else
            {
                $(row.node()).addClass("no-rating");
            }

            let searchText = $('.dataTables_filter input').val();
            if (rowStatus != null) {
                let matchesSearch = true;
                if (searchText) matchesSearch = containsString(data, searchText);
    
                switch (rowStatus) {
                    case "QuoteDraft":
                        if (matchesSearch) countQuoteDraft++;
                        break;
                    case "QuoteSent":
                        if (matchesSearch) countQuoteSent++;
                        break;
                    case "QuoteOutDated":
                        if (matchesSearch) countQuoteOutDated++;
                        break;
                    case "QuoteRefused":
                        if (matchesSearch) countQuoteRefused++;
                        break;
                    case "QuoteAccepted":
                        if (matchesSearch) countQuoteAccepted++;
                        break;
                    case "MissingChild":
                        if (matchesSearch) countMissingChild++;
                        break;
                }
            }
        }
    }

    $('#ContentPlaceHolder1_lblPipe1').html(formatMoney(rating1sum));
    $('#ContentPlaceHolder1_lblPipe2').html(formatMoney(rating2sum));
    $('#ContentPlaceHolder1_lblPipe3').html(formatMoney(rating3sum));
    $('#ContentPlaceHolder1_lblPipe4').html(formatMoney(rating4sum));
    $('#ContentPlaceHolder1_lblPipe5').html(formatMoney(rating5sum));

    $('#ContentPlaceHolder1_lblPipeGlobal').html(formatMoney(ratingsum));

    $('#ContentPlaceHolder1_lblCountDraft').html(countQuoteDraft);
    $('#ContentPlaceHolder1_lblCountSent').html(countQuoteSent);
    $('#ContentPlaceHolder1_lblCountOutdated').html(countQuoteOutDated);
    $('#ContentPlaceHolder1_lblCountRefused').html(countQuoteRefused);
    $('#ContentPlaceHolder1_lblCountAccepted').html(countQuoteAccepted);
    $('#ContentPlaceHolder1_lblCountMissingChild').html(countMissingChild);
    $('#ContentPlaceHolder1_lblCountInProgress').html(countQuoteSent + countQuoteOutDated);

    if (countQuoteOutDated > 0)
        $('#ContentPlaceHolder1_icoCountOutdated').addClass("animated");
    else
        $('#ContentPlaceHolder1_icoCountOutdated').removeClass("animated");

    if (countQuoteAccepted > 0)
        $('#ContentPlaceHolder1_icoCountAccepted').addClass("animated");
    else
        $('#ContentPlaceHolder1_icoCountAccepted').removeClass("animated");
}

function formatMoney(d) {
    d = d
        .toFixed(2)
        .replace('.', ',')
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') 
    return d + " " + currency;
}
function filterItem(settings, data, dataIndex) {

    var visible = true;

    var rowStatus = settings.aoData[dataIndex]._aData["Status"];


    if (_statusFilter != null) {


        if (_statusFilter == 'QuoteInProgress') {
            if (rowStatus == 'QuoteSent' || rowStatus == 'QuoteOutDated')
                visible = true;
            else
                visible = false;
        }
        else if (_statusFilter == "QuoteMissingChild") {
            if (settings.aoData[dataIndex]._aData["MissingChild"] == "True")
                visible = true;
            else
                visible = false;
        }
        else if (rowStatus != _statusFilter)
            visible = false;
    }

    var lastUpdate = parseInt(settings.aoData[dataIndex]._aData["LastUpdateDate"]);
    var dateJobValidity = parseInt(settings.aoData[dataIndex]._aData["DateJobValidity"]);
    var statusDate = null;

    switch (rowStatus) {
        case "QuoteSent":
        case "QuoteOutDated":
            statusDate = ((new Date().getTime() * 10000) + 621355968000000000);
            break;
        case "QuoteAccepted":
            statusDate = parseInt(settings.aoData[dataIndex]._aData["DateQuoteAccepted"]);
            break;
        case "QuoteRefused":
            statusDate = parseInt(settings.aoData[dataIndex]._aData["DateQuoteRefused"]);
            break;
    }

    var date = statusDate || dateJobValidity || lastUpdate;

    if (_periodFilterFrom != null) {
        if (date < _periodFilterFrom)
            visible = false;
    }
    if (_periodFilterTo != null) {
        if (date >= _periodFilterTo)
            visible = false;
    }

    if (_clientFilter != null) {
        var clientMatch = false;
        for (var i = 0; i < _clientFilter.length; i++) {
            if (_clientFilter[i] == settings.aoData[dataIndex]._aData["ClientId"]) clientMatch = true;
        }
        if (clientMatch == false)
            visible = false;
    }

    settings.aoData[dataIndex].nTr.setAttribute('data-anystatusFilter', visible);

    return visible;
}

$(function () {

    // client side filters
    $.fn.dataTable.ext.search.push(filterItem);
    // -->

    _dataTable = initQuotesDataTable($('#quotes-datatable'), getDataUrl());
    _dataTable.search('');
    _dataTable.on('draw', refreshLiveStats);

    refreshLines();

});

function periodChanged() {
    _dataTable.destroy();
    _dataTable = initQuotesDataTable($('#quotes-datatable'), getDataUrl());
}

function initQuotesDataTable(dt, dataUrl) {

    var options = getDataTableOptions(dt, true);
    options.ajax = dataUrl;
    options.columns = [
        {
            "data": "Ids",
            "render": function (ids) { return "<a href=\"Job.aspx?id=" + ids.Id + "\">" + ids.InternalId + "</a>"; }
        },
        { "data": "ClientName" },
        {
            "data": "ImportedDescription",
            "render": function (data) {
                if (data.ImportedId && data.ImportedId > 0) {
                    var t = "<a css='text-white' target='_blank' href='Job.aspx?id=" + data.ImportedId + "'><i class='icon-chevron-right'></i><label>" + data.ImportedInternalId + "</label></a>";
                    return t;
                }

                return data.Description;
            }
        },
        { "data": "TotalHt", "render": displayMoney },
        { "data": "Rating", "render": displayRating },
        { "data": "Status", "render": getProjectStatusHtml },
        {
            "data": "DateStr",
            "type": "date",
            "render": function (date) { return date ? moment(date, CALANI.DEFAULTS.dateTimeFormat).format('DD.MM.YYYY') : ''; }
        },
        {
            "data": "LastUpdateDateStr",
            "type": "date",
            "render": function (date) { return date ? moment(date, CALANI.DEFAULTS.dateTimeFormat).format('DD.MM.YYYY') : ''; }
        },
    ];

    return dt.DataTable(options);
}

function getDataUrl() {
    var dates = getPeriodFilterDates();
    var assigneeId = _assigneeId != undefined && _assigneeId > 0 ? _assigneeId : 0;
    var url = "../../API/Service.svc/ListQuotes?startTicks=" + dates[0] + "&endTicks=" + dates[1] + "&assigneeId=" + assigneeId;
    return url;
}

new Vue({

    el: '#app',

    data: {
        statusFilter: 'QuoteInProgress'
    },
    methods: {
        setStatusFilter: function (e, f) {
            this.statusFilter = f;
            _statusFilter = f;
            _dataTable.draw();
            e && e.preventDefault();
        }
    }
});