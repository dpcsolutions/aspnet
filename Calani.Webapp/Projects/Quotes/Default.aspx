﻿<%@ Page Title="Quotes" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Projects_Quotes_Default" Codebehind="Default.aspx.cs" %>
<%@ Register src="../../assets/Controls/RatingViewer.ascx" tagname="RatingViewer" tagprefix="rv" %>
<%@ Register src="../../assets/Controls/ProjectStatusLabel.ascx" tagname="ProjectStatusLabel" tagprefix="psl" %>
<%@ Register src="../../assets/Controls/ImportedInLink.ascx" tagname="ImportedInLink" tagprefix="iil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
    <div class="btn-group" runat="server" id="quickmenu">
		<button type="button" class="btn bg-gesmobile btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
		</button>

		<ul class="dropdown-menu dropdown-menu-right">
			<li><a href="Job.aspx"><i class="icon-plus2"></i> <%= Resources.Resource.Create %> <%= LabelQuoteOrDeliveryNote %></a></li>
		</ul>
	</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">

        <div class="content" id="app">

                    <div class="row">
                       
						<div class="ol-sm-9 col-md-6">

							<!-- User card with thumb and social icons at the bottom -->
							<div class="panel">
									<div class="panel-body">
										<div class="row text-center">
											
											<div class="col-xs-2" style="width: 14%"   id="divQuoteDraft" v-bind:class="{ 'bordered alpha-primary': statusFilter=='QuoteDraft' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.Draft_Tooltip %>">
                                                <a href="" v-on:click="setStatusFilter($event, 'QuoteDraft')" class="text-grey">
                                                    <p><i class="icon-floppy-disk icon-2x display-inline-block text-grey tada infinite" id="icoCountDraft" runat="server"></i></p>
												<h5 class="text-semibold no-margin" id="lblCountDraft" runat="server">.</h5>
                                                <p><strong class="text-muted text-size-small" ><%= Resources.Resource.Draft_many %></strong></p>
                                                </a>
											</div>

											<div class="col-xs-2" style="width: 14%"   id="divQuoteSent" v-bind:class="{ 'bordered alpha-primary': statusFilter=='QuoteSent' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.Sent_Tooltip %>">
                                                <a href="" v-on:click="setStatusFilter($event, 'QuoteSent')" >
                                                    <p><i class="icon-envelop icon-2x display-inline-block text-primary tada infinite" id="icoCountSent" runat="server"></i></p>
												<h5 class="text-semibold no-margin" id="lblCountSent" runat="server">.</h5>
                                                <p><strong class="text-muted text-size-small" ><%= Resources.Resource.Sent_many %></strong></p>

                                                </a>
											</div>

											<div class="col-xs-2" style="width: 14%"  id="divQuoteOutDated" v-bind:class="{ 'bordered alpha-primary': statusFilter=='QuoteOutDated' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.Outdated_Tooltip %>">
                                                <a href="" v-on:click="setStatusFilter($event, 'QuoteOutDated')"  class="text-muted">
                                                    <p><i class="icon-calendar52 icon-2x display-inline-block text-grey tada infinite"  id="icoCountOutdated" runat="server"></i></p>
												<h5 class="text-semibold no-margin text-muted" id="lblCountOutdated" runat="server">.</h5>
                                                <p><strong class="text-muted text-size-small" ><%= Resources.Resource.Outdated %></strong></p>
                                                </a>
											</div>

                                            <div class="col-xs-2" style="width: 14%"   id="divQuoteRefused" v-bind:class="{ 'bordered alpha-primary': statusFilter=='QuoteRefused' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.Declined_Tooltip %>">
                                                    <a href="" v-on:click="setStatusFilter($event, 'QuoteRefused')" class="text-danger" >
                                                        <p><i class="icon-trash icon-2x display-inline-block text-danger tada infinite"  id="icoCountRefused" runat="server"></i></p>
                                                        <h5 class="text-semibold no-margin" id="lblCountRefused" runat="server">.</h5>
                                                        <p><strong class="text-muted text-size-small" ><%= Resources.Resource.Declined %></strong></p>
                                                    </a>
                                                
											</div>
                                            <div class="col-xs-2" style="width: 14%"  id="divQuoteAccepted" v-bind:class="{ 'bordered alpha-primary': statusFilter=='QuoteAccepted' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.Accepted_Tooltip %>">
                                                <a href="" v-on:click="setStatusFilter($event, 'QuoteAccepted')" class="text-green" >
                                                    <p><i class="icon-checkmark icon-2x display-inline-block tada infinite text-green" id="icoCountAccepted" runat="server"></i></p>
                                                    <h5 class="text-semibold no-margin" id="lblCountAccepted" runat="server">.</h5>
                                                    <p><strong class="text-muted text-size-small" ><%= Resources.Resource.Accepted %></strong></p>
                                                </a>
											</div>

											 <div class="col-xs-2" style="width: 14%"  id="divQuoteInProgress"  v-bind:class="{ 'bordered alpha-primary': statusFilter=='QuoteInProgress' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.InProgress_Tooltip  %>">
                                                 <a href="" v-on:click="setStatusFilter($event, 'QuoteInProgress')" class="text-indigo" ><p><i class="icon-checkmark icon-2x display-inline-block tada infinite text-indigo" id="icoCountInProgress" runat="server"></i></p>
												<h5 class="text-semibold no-margin" id="lblCountInProgress" runat="server">.</h5>
                                                 <p><strong class="text-muted text-size-small" ><%= Resources.Resource.InProgress %></strong></p>

                                                 </a>
											</div>
											
                                            <div class="col-xs-2"  style="width: 14%" v-bind:class="{ 'bordered alpha-primary': statusFilter=='QuoteMissingChild' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.AvailableForImportOfInvoice %>">
                                                    <a href="" v-on:click="setStatusFilter($event, 'QuoteMissingChild')" class="text-grey" >
                                                        <p><i class="icon-notification2 icon-2x display-inline-block tada infinite text-grey" id="icoCountMissingChild" runat="server"></i></p>
                                                        <h5 class="text-semibold no-margin" id="lblCountMissingChild" runat="server">.</h5>
                                                        <p><strong class="text-muted text-size-small" ><%= Resources.Resource.Warning %></strong></p>

                                                    </a>
                                            </div>

										</div>									
									</div>

							    	<div class="panel-footer text-center no-padding">
								    	<div class="row">
								    		<a href=""  v-on:click="setStatusFilter($event, null)"  class="display-block p-10 text-default" 
								    			data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.ListAllQuotes %>">
								    				<i class="fa fa-ellipsis-h"></i> <%= Resources.Resource.Show_all %>
								    				
								    			</a>
								    	</div>
									</div>
								</div>
								<!-- /user card with thumb and social icons at the bottom -->

							</div>

							<div class="col-sm-6 col-md-3">
								<div class="panel panel-body-accent">
									<div class="panel-body">
										<div>
											<div class="media-left media-middle">
												<i class="icon-coins icon-3x text-success-400"></i>
											</div>

											<div class="media-body text-right" data-popup="tooltip" title="<%= PotentialToolTip %>">
												<h3 class="no-margin text-semibold" runat="server" id="lblPipeGlobal">.</h3>
												<span class="text-uppercase text-size-mini text-muted"><%= PotentialLabel %></span>
                                                
											</div>

										</div>
									</div>
									<div class="panel-body" runat="server" id="panelRatedStats">
										<div class="row text-center">
											
											<div class="col-xs-4 two-or-three-1365" data-popup="tooltip" title="<%= Resources.Resource.Pipe_Potential5_Tooltip %>">
													<div class="text-nowrap">
														<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
													</div>
													<div>
														<asp:Label runat="server" ID="lblPipe5" Text="." />
													</div>
											</div>
											<div class="col-xs-4 two-or-three-1365" data-popup="tooltip" title="<%= Resources.Resource.Pipe_Potential4_Tooltip %>">
													<div class="text-nowrap">
														<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
													</div>
													<div>
														<asp:Label runat="server" ID="lblPipe4" Text="." />
													</div>
											</div>
											<div class="col-xs-4 two-or-three-1365" data-popup="tooltip" title="<%= Resources.Resource.Pipe_Potential3_Tooltip %>">
													<div class="text-nowrap">
														<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
													</div>
													<div>
														<asp:Label runat="server" ID="lblPipe3" Text="." />
													</div>
											</div>
											<div class="col-xs-4 two-or-three-1365" data-popup="tooltip" title="<%= Resources.Resource.Pipe_Potential2_Tooltip %>">
													<div class="text-nowrap">
														<i class="fa fa-star"></i><i class="fa fa-star"></i>
													</div>
													<div>
														<asp:Label runat="server" ID="lblPipe2" Text="." />
													</div>
											</div>
											<div class="col-xs-4 two-or-three-1365" data-popup="tooltip" title="<%= Resources.Resource.Pipe_Potential1_Tooltip %>">
													<div class="text-nowrap">
														<i class="fa fa-star"></i>
													</div>
													<div>
														<asp:Label runat="server" ID="lblPipe1" Text="." />
													</div>
											</div>
										</div>
									</div>

								</div>
								
							</div>

							<div class="col-sm-7 col-md-3">
								<div class="panel panel-body panel-body-accent">
									<div class="media no-margin">
										
										<div>
											<i class="fa fa-calendar-check-o" aria-hidden="true"></i> <%= Resources.Resource.Select_Period %>
										</div>
										<div>
											<select id="ddlPeriod" class="select" onchange="periodChanged()">
												<%= OptionsPeriod %>
											</select>
										</div>

										<div style="margin-top:20px">
											<i class="fa fa-user-circle-o" aria-hidden="true"></i> <%= Resources.Resource.Customers %>
										</div>
										<div>
											<select id="ddlClient" data-placeholder="<%= Resources.Resource.Select___ %>" multiple="multiple" class="select" onchange="setClientFilter()">
                                                <%= OptionsClients %>
											</select>
										</div>
									</div>
								</div>
							</div>
					</div>

					<div class="row">
                        <div class="col-sm-12 col-md-12">
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="icon-upload display-inline-block text-green"></i>
										<strong><%= ph.CMP.Name %></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

							<div class="panel-body">

								<table id="quotes-datatable" class="table table-hover dataTable no-footer">
									<thead>
										<tr>
											<th><%= Resources.Resource.Code %></th>
											<th><%= Resources.Resource.Customer %></th>
											<th><%= Resources.Resource.Description %></th>
											<th><%= UsePriceIncludingVat ? Resources.Resource.TotalTTC : Resources.Resource.TotalHT %></th>
											<th><%= Resources.Resource.Confidence %></th>
											<th><%= Resources.Resource.Status %></th>
											<th><%= Resources.Resource.Date %></th>
											<th><%= Resources.Resource.LastUpdate %></th>
										</tr>
									</thead>
								</table>

                                
                            </div><!-- panel-body -->

                            <div class="panel-footer text-right no-padding">
						    	<div class="row" style="margin:10px">
						    		<a href="Job.aspx" class="btn btn-default"><i class="icon-plus2 position-left"></i> 
                                        <%= Resources.Resource.Create %>
                                        <%= LabelQuoteOrDeliveryNote %>
                                        
						    		</a>
						    	</div>
							</div><!-- panel-footer -->

                        </div>
                        </div>

                    </div>
        </div><!-- /content -->
    </form>
    <script type="text/javascript">
        var currency = '<%= ph.Currency %>';
		var DeliveryNote = <%= DeliveryNote.ToString().ToLower() %>;
        var _assigneeId = <%= AssigneeId %>;
    </script>
    <script type="text/javascript" src="Default.js?<%= Guid.NewGuid().ToString("n") %>"></script>
<%--<script type="text/javascript" src="Default.js"></script>--%>

    
</asp:Content>

