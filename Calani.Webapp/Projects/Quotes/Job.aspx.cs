﻿using Calani.BusinessObjects.CustomerAdmin;
using System;

public partial class Projects_Quotes_Job : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }



    public string CreateAnOtherQuote
    {
        get
        {
            return Resources.Resource.CreateAnOtherQuote;
        }
    }

    public string YesCreateNewQuote
    {
        get
        {
            return Resources.Resource.YesCreateNewQuote;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var mgr = new OrganizationManager(ph.CurrentOrganizationId);
        mgr.Load();

        JobEditor1.EnableQuoteFields = true;
        JobEditor1.IsAdmin = ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin;
        JobEditor1.EnableRefNumInheritance = mgr.EnableRefNumberInheritance;

        // page not available for employee
        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            Response.Redirect("~/");
        }
        // -->
    }

    /*
    public bool DeliveryNote
    {
        get
        {
            return false;
        }
    }*/

    public string LabelQuoteOrDeliveryNote
    {
        get
        {
            string title = Resources.Resource.Quote;
            return title;
        }
    }

    public void Load()
    {
        

        Calani.BusinessObjects.Projects.jobsExtended info = JobEditor1.Info;

        if (info.lastUpdate != null)
        {
            lblLastChange.InnerHtml = Resources.Resource.LastChange + "<br />" + Convert2.DateToStr(info.lastUpdate.Value);
        }
        else
        {
            lblLastChange.InnerHtml = Resources.Resource.NeverSaved;
        }


        if (info.dateQuoteSent != null)
        {
            lblLastSent.InnerHtml = "<i class=\"icon-checkmark\"></i> " + Resources.Resource.LastSend + "<br />" + Convert2.DateToStr(info.dateQuoteSent.Value);
        }
        else
        {
            lblLastSent.InnerHtml = Resources.Resource.NeverSent;
            lblLastSent.Attributes["class"] += " text-muted";
        }

        if (info.dateQuoteRefused != null)
        {
            lblRejectDate.InnerHtml = "<i class=\"icon-cross3\"></i> " + Resources.Resource.MarkAsRejected + "<br />" + Convert2.DateToStr(info.dateQuoteRefused.Value);
        }
        else
        {
            if (info.dateQuoteAccepted == null) 
                lblRejectDate.InnerText = String.Empty;//Resources.Resource.NoDecisionYet;

            lblRejectDate.Attributes["class"] += " text-muted";
        }

        if (info.dateQuoteAccepted != null)
        {
            lblAcceptDate.InnerHtml = "<i class=\"icon-checkmark\"></i> " + Resources.Resource.MarkAsAccepted + "<br />" + Convert2.DateToStr(info.dateQuoteAccepted.Value);
        }
        else
        {
            if (info.dateQuoteRefused == null) 
                lblAcceptDate.InnerText = String.Empty; //Resources.Resource.NoDecisionYet;

            lblAcceptDate.Attributes["class"] += " text-muted";
        }
    }
}