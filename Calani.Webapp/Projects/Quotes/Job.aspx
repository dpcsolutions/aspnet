﻿<%@ Page Title="Quote" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Projects_Quotes_Job" Codebehind="Job.aspx.cs" %>
<%@ Register src="../JobEditor.ascx" tagname="JobEditor" tagprefix="uc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .datepickerinswal .picker {
          top: inherit;
        }
        .swal2-datepicker .swal2-actions {
            z-index: inherit !important;
        }
    </style>
    <script type="text/javascript">

            var ressources2 = [];
            ressources2['CreateAnOtherQuote'] = "<%= CreateAnOtherQuote %>";
            ressources2['YesCreateNewQuote'] = "<%= YesCreateNewQuote %>";
            ressources2['GoToList'] = "<%= Resources.Resource.GoToList %>";
            ressources2['FeatureDisabledIfMultipleVAT'] = "<%= Resources.Resource.FeatureDisabledIfMultipleVAT %>";
			ressources2['Unit'] = "<%= Resources.Resource.Unit %>"

       
            
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
        };

        function afterJobSave(query, response) {

            if (query.action.indexOf("save") == 0)
            {
                listOrNew();
            }

            if (query.action.indexOf("acceptquote") == 0)
            {
                workflowDialog(true, true, true, true);
            }

            if (query.action == "sendquote" && query.id > 0)
            {
                window.location = "Send.aspx?id=" + query.id + "&sel=" + response.attachment;
            }
        }


        function listOrNew()
        {
            swal({
                    title: ressources2['CreateAnOtherQuote'],
                    text: '...',
                    type: "success",
                    showCancelButton: true,
                    showCloseButton: true,
                    closeButtonHtml: 'lapin',
                    cancelButtonText: ressources2['GoToList'],
                    confirmButtonColor: "#4CAF50",
                    confirmButtonText: ressources2['YesCreateNewQuote']
                })
            .then((value) => {
                        
                        

                if (value.value != null && value.value == true)
                {
                    /* create another */
                    window.location = "Job.aspx";
                }
                else
                {
                    if(value.dismiss == "cancel")
                    {
                        /* go to list */
                        window.location = "../Quotes/";
                    }
                    else if(value.dismiss=="close")
                    {
                        /* stay here */
                        if(query.id == -1)
                        {
                            window.location = "Job.aspx?id=" + response.id;
                        }
                                                           
                    }
                }

            });
        }

       /* $(function () {
            var id = getUrlParameter("id");
            if (!id || id < 0) {
                $(".action-button").prop('disabled', true);
            }
        });*/

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<form runat="server">

    
    <div class="content"> 


        

        <uc1:JobEditor ID="JobEditor1" runat="server"  />    
        

         

        <div class="row">

        <div class="col-sm-12 col-md-6">
	        <div class="panel panel-flat">
			        <div class="panel-heading">
				        <h5 class="panel-title"><%= LabelQuoteOrDeliveryNote %></h5>
			        </div>
			        <div class="panel-body">
				        <div class="row text-center">
											
					        <div class="col-xs-6">
							        <div class="panel panel-body border-top-primary text-center">
								        <p class="text-muted content-group-sm text-size-small two-lines-small-text" id="lblLastChange" runat="server">
                                            
								        </p>





                                        <div class="btn-group">
                                            <a href="javascript:void(0)" runat="server" class="btn btn-primary btn-save" data-action="save" data-lbl-date="ContentPlaceHolder1_lblLastChange">
                                                <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                            </a>
                                                
									        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore"><span class="caret"></span></button>
									        <ul class="dropdown-menu dropdown-menu-right" style="min-width:240px">
                                                <li><a href="javascript:void(0)" class="btn-save" data-action="save" data-lbl-date="ContentPlaceHolder1_lblLastChange"><i class="icon-floppy-disk"></i> <%= Resources.Resource.SaveChanges %></a></li>
                                                <li><a href="javascript:void(0)" class="btn-clone" data-type="*" data-toggle="modal" data-target="#modal_form_cloneproject"><i class="fa fa-copy"></i> <%= Resources.Resource.Clone %></a></li>
                                                <li><a href="javascript:void(0)" class="btn-delete" data-type="*" data-toggle="modal" data-target="#modal_form_deleteproject"><i class="fa fa-trash"></i> <%= Resources.Resource.Delete %></a></li>
									        </ul>
								        </div>


							        </div>	
					        </div>
                            <div class="col-xs-6">
										<div class="panel panel-body border-top-primary text-center">
											<p class="content-group-sm text-size-small two-lines-small-text" id="lblLastSent" runat="server">
                                                
											</p>

                                            <div class="btn-group">
											    <a href="javascript:void(0)" type="button" class="btn btn-save btn-primary" 
                                                data-lbl-date="ContentPlaceHolder1_lblLastSent"
                                                data-action="sendquote">
                                                    <i class="icon-envelop position-left"></i>  <%= Resources.Resource.Send %> 

											    </a>

                                                
                                                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" runat="server"><span class="caret"></span></button>
									            <ul class="dropdown-menu dropdown-menu-right" style="min-width:240px">
                                                    <li><a href="javascript:void(0)" class="btn-save" data-action="markquotesent" data-lbl-date="ContentPlaceHolder1_lblLastSent"><i class="icon-checkmark-circle"></i> <%= Resources.Resource.MarkAsSent %></a></li>
                                                    <li><a href="javascript:$('#ContentPlaceHolder1_lblLastSent').html('')" class="btn-save" data-action="markquotenotsent"><i class="icon-radio-unchecked"></i> <%= Resources.Resource.MarkAsNotSent %></a></li>
                                                    <li><a href="javascript:setSendDate('setquotesentdate', '#ContentPlaceHolder1_lblLastSent')"><i class="icon-calendar"></i> <%= Resources.Resource.ChangeSendDate %></a></li>
                                                    <li><a href="javascript:void(0)" class="btn-save" data-action="previewpdf:quote"><i class="icon-file-pdf"></i> <%= Resources.Resource.PreviewPDF %></a></li>
                                                    
									            </ul>
                                            </div>
								            
										</div>	
								</div>
				        </div>	
			        </div>
	        </div>
        </div>
        <div class="col-sm-12 col-md-6">

            <div class="panel panel-flat">
		            <div class="panel-heading">
			            <h5 class="panel-title"><%= Resources.Resource.Customer_Acceptance %></h5>
		            </div>
		            <div class="panel-body">

			            <div class="row text-center">
											
				            <div class="col-xs-6">
						            <div class="panel panel-body border-top-danger text-center">
							            <p class="text-muted content-group-sm text-size-small two-lines-small-text" id="lblRejectDate"  runat="server">

							            </p>

							            <button type="button" class="btn btn-save btn-danger action-button" 
                                            data-lbl-date="ContentPlaceHolder1_lblRejectDate"
                                            data-action="rejectquote">
                                            <i class="icon-thumbs-down2 position-left"></i> <%= Resources.Resource.Reject %>

							            </button>
						            </div>	
				            </div>


				            <div class="col-xs-6">
						            <div class="panel panel-body border-top-success text-center">
							            <p class="text-muted content-group-sm text-size-small two-lines-small-text" id="lblAcceptDate" runat="server">

                                        </p>

							            <button type="button" class="btn btn-save btn-success action-button" 
                                            data-lbl-date="ContentPlaceHolder1_lblAcceptDate"
                                            data-action="acceptquote">
                                            <i class="icon-thumbs-up2 position-left"></i>  <%= Resources.Resource.Accept %> 

							            </button>
							            
						            </div>	
				            </div>

			            </div>	

		            </div>
            </div>
							

        </div>

    </div>
    
    
    

    </div>
</form>
    
</asp:Content>
