﻿function workflowDialogAction()
{
    var task = {
        source: projectId,
        culture: lang4,
        to: [],
        includeAllAttachments: false,
        attachmentsIds: []
    };
    
    if ($('#cbxWorkflowNextDeliveryNote').is(":checked")) {
        var tid = $('#ddlWorkflowNextDeliveryNote').val() * 1;
        task.to.push({type:"deliverynote", id: tid});
    }
    if ($('#cbxWorkflowNextInvoice').is(":checked")) {
        var tid = $('#ddlWorkflowNextInvoice').val() * 1;
        var refNumber = $('#ContentPlaceHolder1_JobEditor1_tbxInternalId').length ? $('#ContentPlaceHolder1_JobEditor1_tbxInternalId').val() : null;
        task.to.push({
            type: "invoice",
            id: tid,
            referenceNumber: refNumber
        });
    }
    if ($('#cbxWorkflowNextProject').is(":checked")) {
        var tid = $('#ddlWorkflowNextProject').val() * 1;
        var refNumber = tid == -1 && $('#ref-num-prefix').length ? $('#ref-num-prefix').val() + $('#ref-num-postfix').text() : null;
        task.to.push({
            type: "open",
            id: tid,
            referenceNumber: refNumber
        });
    }

    if ($('#cbxWorkflowNextNone').is(":checked")) {
        
        task.to.push({ type: "none", id: 0 });
    }

    task.includeAllAttachments = $("#cbxWorkflowNextIncludeAllAttachments").is(":checked");
    task.attachmentsIds = getCheckedAttachmentsIds();
    
    $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '../../API/Service.svc/ExportToProjects',
            data: JSON.stringify(task),
            processData: false,
            dataType: "json",
            success: function (response) 
            {   
                window.location = "../" + response.message + "/Job.aspx?id=" + response.id;
            }
    });
}



function workflowDialog(bDeliveryNotes, bInvoices, bProjects, bNone = false) {
    $('#modal_workflow').modal('show');

    $('#cbxWorkflowNextDeliveryNote').prop("checked", false);
    $('#cbxWorkflowNextInvoice').prop("checked", false);
    $('#cbxWorkflowNextProject').prop("checked", false);
    $('#ddlWorkflowNextDeliveryNoteContainer').hide();
    $('#ddlWorkflowNextInvoiceContainer').hide();
    $('#ddlWorkflowNextProjectContainer').hide();
    $('#refNumberContainer').hide();

    if (bDeliveryNotes == true) {
        $('#rowWorkflowNextDeliveryNote').show();
        
    }
    else {
        $('#rowWorkflowNextDeliveryNote').hide();
    }

    if (bInvoices == true) {
        $('#rowWorkflowNextInvoice').show();
    }
    else {
        $('#rowWorkflowNextInvoice').hide();
    }

    if (bProjects == true) {
        $('#rowWorkflowNextProject').show();
    }
    else {
        $('#rowWorkflowNextProject').hide();
    }

    if (bNone == true)
        $('#rowWorkflowNextNone').show();

    else
        $('#rowWorkflowNextNone').hide();

}

function workflowDialogReadMore() {
    $('.modal_workflow_readmore').toggle();
}

function workflowEnableCreateButton() {

    if ($('#cbxWorkflowNextDeliveryNote').is(":checked") || $('#cbxWorkflowNextInvoice').is(":checked") || $('#cbxWorkflowNextProject').is(":checked") || $('#cbxWorkflowNextNone').is(":checked")) {
        $('#btnWorkflowNextCreate').show();
    }
    else {
        $('#btnWorkflowNextCreate').hide();
    }
}

function workflowRefreshList(ddl, type) {
    $(ddl).html('<option>...</option>');
    $(ddl).prop("disabled", false);

    var clientId = toNum($('#ContentPlaceHolder1_JobEditor1_ddlCustomer').val());

    var bDeliveryNotes = type == 'deliverynote';
    var bInvoices = type == 'invoice';
    var bProjects = type == 'project';

    


    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        url: '../../API/Service.svc/GetProjectsToExport?customerid=' + clientId + '&deliverynotes=' + bDeliveryNotes + '&invoices=' + bInvoices + '&projects=' + bProjects,
        dataType: "json",
        success: function (response) {
            
            $(ddl).html('');

            var optNew = $('<option value="-1">' + ressources['CreateNew'] + '...</option>');
            $(ddl).append(optNew);

            for (var i = 0; i < response.length; i++) {
                var opt = $('<option></option>');
                var txt = '';
                if (response[i].InternalId != null && response[i].InternalId != '') txt += response[i].InternalId + ' - ';
                if (response[i].Date != null && response[i].Date != '') txt += response[i].Date + ' - ';
                if (response[i].Amount != null && response[i].Amount != '') txt += (Math.round(response[i].Amount * 100) / 100).toFixed(2) + ' - ';
                txt = txt.trim();
                txt = txt.trim();
                if (txt.endsWith('-')) txt = txt.substring(0, txt.length - 1);
                txt = txt.trim();
                opt.text(txt);
                opt.attr('value', response[i].Id);

                $(ddl).append(opt);
            }
            

        }
    });
}


function cbxWorkflowNextDeliveryNote_onclick(o) {
    $('#ddlWorkflowNextDeliveryNoteContainer').toggle();

    if ($('#cbxWorkflowNextDeliveryNote').is(":checked")) {
        $('#ddlWorkflowNextDeliveryNote').html('<option>...</option>');
        $('#ddlWorkflowNextDeliveryNote').prop("disabled", true);
        workflowRefreshList($('#ddlWorkflowNextDeliveryNote'), 'deliverynote');

    }
    workflowEnableCreateButton();
}

function cbxWorkflowNextInvoice_onclick(o) {
    $('#ddlWorkflowNextInvoiceContainer').toggle();

    if ($('#cbxWorkflowNextInvoice').is(":checked")) {
        $('#ddlWorkflowNextInvoice').html('<option>...</option>');
        $('#ddlWorkflowNextInvoice').prop("disabled", true);
        workflowRefreshList($('#ddlWorkflowNextInvoice'), 'invoice');

    }
    workflowEnableCreateButton();

    if ($('#cbxWorkflowNextProject').is(":checked") && o == null) {
        $('#cbxWorkflowNextProject').prop("checked", false);
        cbxWorkflowNextProject_onclick('invoice');
    }
}

function cbxWorkflowNextProject_onclick(o) {
    $('#ddlWorkflowNextProjectContainer').toggle();
    $('#refNumberContainer').toggle();

    if ($('#cbxWorkflowNextProject').is(":checked")) {
        $('#ddlWorkflowNextProject').html('<option>...</option>');
        $('#ddlWorkflowNextProject').prop("disabled", true);
        workflowRefreshList($('#ddlWorkflowNextProject'), 'project');

    }
    workflowEnableCreateButton();

    if ($('#cbxWorkflowNextInvoice').is(":checked") && o == null) {
        $('#cbxWorkflowNextInvoice').prop("checked", false);
        cbxWorkflowNextInvoice_onclick('project');
    }
}

function cbxWorkflowNextNone_onclick(o) {
   
    workflowEnableCreateButton();
}

$(function () {

    
    $('#cbxWorkflowNextDeliveryNote').click(function () {
        cbxWorkflowNextDeliveryNote_onclick();
    });
    $('#cbxWorkflowNextInvoice').click(function () {
        cbxWorkflowNextInvoice_onclick();
    });
    $('#cbxWorkflowNextProject').click(function () {
        cbxWorkflowNextProject_onclick();
    });

    $('#cbxWorkflowNextNone').click(function () {
        cbxWorkflowNextNone_onclick();
    });


});

function previewNextDeliveryNote() {
    var pid = $('#ddlWorkflowNextDeliveryNote').val();
    if (pid >= 0) window.open("../PreviewPdf.ashx?q=" + pid + "&c=" + lang4, "PreviewPdfPopupWorkflow", "width=500,height=600");
    else alert('A new empty delivery note will be created');
}
function previewNextInvoice() {
    var pid = $('#ddlWorkflowNextInvoice').val();
    if (pid >= 0) window.open("../PreviewPdf.ashx?q=" + pid + "&c=" + lang4, "PreviewPdfPopupWorkflow", "width=500,height=600");
    else alert('A new empty invoice will be created');
}

function previewNextProject() {
    var pid = $('#ddlWorkflowNextProject').val();
    if (pid >= 0) window.open("../Opens/Job.aspx?id=" + pid + "&nomenu=1", "PreviewPdfPopupWorkflow", "width=920,height=600");
    else alert('A new empty project will be created');
}

function getCheckedAttachmentsIds() {
    return $("ul#ulCloneAttachments input[type=checkbox]:checked")
        .map(function () { return parseInt($(this).val()); }).get()
        .filter(function (id) { return id > 0; });
}

function onNextProjectChanged() {
    var selectedProjId = $('#ddlWorkflowNextProject option:selected').val();
    var display = 1 * selectedProjId == -1 ? "" : "none";
    $('#refNumberContainer').css("display", display);
}