﻿<%@ WebHandler Language="C#" Class="PreviewPdf" %>

using System;
using System.Web;
using System.Linq;
using Calani.BusinessObjects.DocGenerator;

public class PreviewPdf : IHttpHandler, System.Web.SessionState.IRequiresSessionState {

    public void ProcessRequest (HttpContext context) {

        string q = context.Request["q"];
        string c = context.Request["c"]??"fr-FR";
        string deliveryNoteTypeStr = context.Request["deliveryNoteType"];

        var ci = new System.Globalization.CultureInfo(c);

        long id = -1;
        if(q.ToLowerInvariant() == "scheduler") {

            DateTime? dtFrom = null;
            DateTime? dtTo = null;

            if (context.Request["from"] != null && context.Request["to"] != null)
            {
                DateTime start, end;
                bool result = DateTime.TryParseExact(context.Request["from"], "dd-MM-yyyy", ci, System.Globalization.DateTimeStyles.AssumeLocal, out start);
                result &= DateTime.TryParseExact(context.Request["to"], "dd-MM-yyyy", ci, System.Globalization.DateTimeStyles.AssumeLocal, out end);

                if (result)
                {
                    dtFrom = start;
                    dtTo = end;
                }
            }

            long userId = PageHelper.GetUserId(context.Session);
            long orgId = PageHelper.GetCurrentOrganizationId(context.Session);
            var docGenerator = new SchedulerGenerator(orgId, userId, ci);
            var data = docGenerator.GenerateDocument(dtFrom, dtTo);
            context.Response.ContentType = "application/pdf";
            context.Response.OutputStream.Write(data, 0, data.Length);
        }
        else if (long.TryParse(q, out id))
        {

            long userId = PageHelper.GetUserId(context.Session);

            // is it an invoice ?
            Calani.BusinessObjects.Model.CalaniEntities db = new Calani.BusinessObjects.Model.CalaniEntities();
            bool isInvoice = (from r in db.jobs where r.id == id && r.dateInvoicing != null select r).Count() > 0;


            byte[] data = null;

            if (isInvoice)
            {
                Calani.BusinessObjects.DocGenerator.InvoiceGenerator gen = new Calani.BusinessObjects.DocGenerator.InvoiceGenerator(ci, userId);
                gen.ProjectId = id;
                
                try
                {
                    data = gen.GenerateDocument();
                }
                catch (Exception e)
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write(e.Message);
                    return;
                }
            }
            else
            {
                Calani.BusinessObjects.DocGenerator.QuoteGenerator gen = new Calani.BusinessObjects.DocGenerator.QuoteGenerator(ci, userId);
                gen.ProjectId = id;

                sbyte deliveryNoteType = 0;
                var parsed = !string.IsNullOrWhiteSpace(deliveryNoteTypeStr) && sbyte.TryParse(deliveryNoteTypeStr, out deliveryNoteType);
                Calani.BusinessObjects.Enums.TypeOfDeliveryNotePdfEnum? type = parsed ? (Calani.BusinessObjects.Enums.TypeOfDeliveryNotePdfEnum?)deliveryNoteType : null;

                try
                {
                    data = gen.GenerateDocument(null, type);
                }
                catch (Exception e)
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write(e.Message);
                    return;
                }
            }

            context.Response.ContentType = "application/pdf";
            context.Response.OutputStream.Write(data, 0, data.Length);

        }
        else
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write("Hello World");
        }

    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}