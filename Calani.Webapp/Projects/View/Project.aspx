﻿<%@ Page Title="Project" Language="C#" MasterPageFile="~/MasterPageNoSession.master" AutoEventWireup="true" Inherits="Project_Default" Codebehind="Project.aspx.cs" %>
<%@ Register src="../JobEditor.ascx" tagname="JobEditor" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
    <div class="content"  >
        <uc1:JobEditor ID="JobEditor1" runat="server"  EnableInvoiceFields="true" IsReadOnlyMode="true" IsReadOnly="true" />
        


        <div class="row">

            <div class="col-sm-12 col-md-12">
	            <div class="panel panel-flat">
                    <div class="panel-heading">
				        <h5 class="panel-title"><%= Resources.Resource.Payments %></h5>
			        </div>
                    <div class="panel-body">
				        <div class="row text-center">
                            <div id="hot_ac_payments"></div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="form-horizontal">
							<fieldset class="content-group no-margin" style="padding-top:10px">


                                <div class="form-group" id="panelMainDiscount" runat="server">
									<label class="control-label col-lg-6 text-right"><%= Resources.Resource.TotalPaid %></label>													
									<div class="col-lg-5">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-coins"></i></span>
											<input type="text" step="0.01" value="" class="form-control" disabled="disabled" id="tbxTotalPaid" >
										</div>
									</div>
								</div>


								<div class="form-group">
									<label class="control-label col-lg-6 text-right"><%= Resources.Resource.Due %></label>
									<div class="col-lg-5 text-right">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-alarm"></i></span>
											<input type="text" step="0.01" value="" class="form-control" disabled="disabled" id="tbxTotalDue" >
										</div>
									</div>
								</div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row"  id="app">
                 <div class="col-sm-12 col-md-12">
	            <div class="panel panel-flat">
                    <div class="panel-heading">
				        <h5 class="panel-title"><%= Resources.Resource.Visits %></h5>
			        </div>
                    <div class="panel-body">
				        <div class="row text-center">
                               <div id="hot_ac_visits"></div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
     
    </div>
        <script type="text/javascript">

            var jobId = <%= jobId %>;
            var visitsDataSource = <%= JsonVisits %>;

            var paymentsDataSource = <%= JsonPayments %>;
            //for (var i = 0; i < 3; i++) paymentsDataSource.push({});

            var ressources2 = [];
            ressources2['Unit'] = "<%= Resources.Resource.Unit %>";
            ressources2['FeatureDisabledIfMultipleVAT'] = "<%= Resources.Resource.FeatureDisabledIfMultipleVAT %>";
            ressources2['Date'] = "<%= Resources.Resource.Date %>";
            ressources2['Start'] = "<%= Resources.Resource.Start %>";
            ressources2['End'] = "<%= Resources.Resource.End %>";
            ressources2['Name'] = "<%= Resources.Resource.Name %>";
            ressources2['Client'] = "<%= Resources.Resource.Client %>";
            ressources2['Employee'] = "<%= Resources.Resource.Employee %>";
            ressources2['Description'] = "<%= Resources.Resource.Description %>";
            ressources2['Amount'] = "<%= Resources.Resource.Amount %>";
            ressources2['Comment'] = "<%= Resources.Resource.Comment %>";
            ressources2['Error_PaymentSameDateTwice'] = "<%= Resources.Resource.Error_PaymentSameDateTwice %>";
            ressources2['CreateAnOtherInvoice'] = "<%= Resources.Resource.CreateAnOtherInvoice %>";
            ressources2['YesCreateNew'] = "<%= Resources.Resource.YesCreateNew %>";
            ressources2['GoToList'] = "<%= Resources.Resource.GoToList %>";
            ressources2['Import'] = "<%= Resources.Resource.Import %>";
            ressources2['OpenInNewWindow'] = "<%= Resources.Resource.OpenInNewWindow %>";
            ressources2['AlreadyImportedOnAnInvoice'] = "<%= Resources.Resource.AlreadyImportedOnAnInvoice %>";
            ressources2['NeverImportedYet'] = "<%= Resources.Resource.NeverImportedYet %>";
            ressources2['SorryNothingToImport'] = "<%= Resources.Resource.SorryNothingToImport %>";
            

            $(function () {
                var hot_ac_payments = document.getElementById('hot_ac_payments');
                 new Handsontable(hot_ac_payments, {
                    data: paymentsDataSource,
                    afterChange: onPaymentChange,
                    stretchH: 'all',
                    colHeaders: [ressources2['Date'], ressources2['Amount'], ressources2['Comment'], ''],
                    colWidths: [2, 2, 7, 1],
                    rowHeight: function (row) {
                        return 30;
                    },
                    defaultRowHeight: 30,
                    columns: [
                        {
                            data: 'datePayed',
                            type: 'date',
                            timeFormat: 'dd/MM/yyyy',
                            correctFormat: true,
                            className: 'htCenter',
                            datePickerConfig: {
                                firstDay: 1,
                                showWeekNumber: true
                            },
                            readOnly: isReadOnly
                        },
                        {
                            data: 'amountPayed',
                            type: 'numeric',
                            className: 'htRight',
                            format: '0,0.00',
                             numericFormat: {
                                pattern: '0,0.00',
                                culture: 'fr-FR' // this is the default culture, set up for USD
                            },
                            readOnly: isReadOnly
                        },
                        {
                            data: 'comment',
                            type: 'text',
                            readOnly: isReadOnly
                        },
                        {
                            renderer:
                            function (instance, td, row, col, prop, value, cellProperties) {
                                if (!isReadOnly) {
                                    var div;
                                    // Remove existing buttons to avoid duplicating them.
                                    $(td).children('.delbtn').remove();

                                    div = document.createElement('div');
                                    div.className = 'delbtn';
                                    div.style.cursor = 'hand';
                                    //div.appendChild(document.createTextNode('x'));
                                    delico = $('<i class="fa fa-trash" aria-hidden="true"></i>');
                                    $(div).html(' ');
                                    $(div).append(delico);
                                    td.appendChild(div);

                                    $(div).on('mouseup', function () {
                                        if (confirm(ressources.SureToDeleteLine)) {
                                            instance.alter("remove_row", row);

                                            instance.render();
                                            onPaymentChange();
                                            return true;
                                        }
                                        else {
                                            return false;
                                        }
                                    });
                                }
                                return td;
                            },
                            readOnly: true
                        }

                    ]
                });



                var hot_ac_visits = document.getElementById('hot_ac_visits');
                new Handsontable(hot_ac_visits, {
                    data: visitsDataSource,
                    stretchH: 'all',
                    colHeaders: [ressources2['Start'], ressources2['End'], ressources2['Client'], ressources2['Employee']],
                    colWidths: [2, 2, 3, 3],
                    rowHeight: function (row) {
                        return 30;
                    },
                    defaultRowHeight: 30,
                    columns: [
                        {
                            data: 'start',
                            type: 'text',
                            readOnly: isReadOnly
                        },
                        {
                            data: 'end',
                            type: 'text',
                            readOnly: isReadOnly
                        },
                        {
                            data: 'client',
                            type: 'text',
                            readOnly: isReadOnly
                        },
                        {
                            data: 'employee',
                            type: 'text',
                            readOnly: isReadOnly
                        },
                       

                    ]
                })


                
            });


            function onPaymentChange() {

                var totalpaid = 0;
                for (var i = 0; i < paymentsDataSource.length; i++)
                {
                    if (paymentsDataSource[i].amountPayed != null) {
                        totalpaid += toDouble(paymentsDataSource[i].amountPayed);
                    }
                }
                $('#tbxTotalPaid').val(precisionRound(totalpaid, 2));

                var due = toDouble($('#tbxGrandTotal').val()) * 1 - totalpaid;

                $('#tbxTotalDue').val(precisionRound(due, 2));
            }

        </script>
    <script type="text/javascript">
        $(function () {
            $('#ContentPlaceHolder1_panelMultipleImports').addClass('panel-collapsed');
            $('#ContentPlaceHolder1_panelMultipleImports .panel-body').hide();
        });
    </script>
</form>

</asp:Content>
