﻿using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Projects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Project_Default : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        JobEditor1.EnableDeliveryNotesFields = true;
        JobEditor1.IsReadOnly = "true";
        JobEditor1.IsAdmin = ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin;
    }
    public string JsonPayments { get; set; }
    public string JsonVisits { get; set; }
    public long jobId { get; set; }

    public String defaultDate { get; set; }
    public string localisedDateFormat { get; set; }
    public string currentLanguage { get; set; }

    public void Load()
    {
        Calani.BusinessObjects.Projects.jobsExtended info = JobEditor1.Info;
        
        JsonPayments = "[]";
        if (info != null && info.id > 0)
        {
            Calani.BusinessObjects.Projects.InvoicePaymentsManager mgr = new InvoicePaymentsManager(info.organizationId.Value, info.id);
            var payments = mgr.GetInvoicePayments();
            JsonPayments = new JavaScriptSerializer().Serialize(payments);
        }

        jobId = info.id;

        defaultDate = string.Concat(DateTime.UtcNow.ToString("s"), "Z");
        currentLanguage = "fr";
        localisedDateFormat = Tools.DefaultDateFormat;

        JobsManager jobMgr = new JobsManager(info.organizationId.Value);
        var visits = jobMgr.ListVisits(null,null, null, info.id, null);

        var visitsDTO = visits.Select(v => {

            var start = DateTime.ParseExact(v.start, Tools.ProjectVisitDateFormat, CultureInfo.InvariantCulture).ToString(Tools.DefaultDateShortTimeFormat, CultureInfo.InvariantCulture);
            var end = DateTime.ParseExact(v.end, Tools.ProjectVisitDateFormat, CultureInfo.InvariantCulture).ToString(Tools.DefaultDateShortTimeFormat, CultureInfo.InvariantCulture);

            var m = new { start, end, client = v.title, employee = v.resourcesStr };
            return m;
        }).ToList();

        JsonVisits = new JavaScriptSerializer().Serialize(visitsDTO);




    }
}