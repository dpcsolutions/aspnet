﻿using Calani.BusinessObjects.Attachments;
using Calani.BusinessObjects.Costs;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Projects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.DocGenerator;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Projects.Extensions;
using Calani.BusinessObjects.TaskList;
using Calani.BusinessObjects.Template;

public partial class Projects_JobEditor : System.Web.UI.UserControl
{
    public Projects_JobEditor()
    {
        IsReadOnly = "false";
    }
    public string IsReadOnly { get; set; }
    public long? ProjectId { get; set; }

    public bool DisableInvoicedOnly { get; set; }
    public bool EnableQuoteFields { get; set; }
    public bool EnableInvoiceFields { get; set; }
    public bool EnableProjectFields { get; set; }
    public bool IsRecurringInvoice { get; set; }

    public bool EnableDeliveryNotesFields { get; set; }
    public bool EnableMainDiscount { get; set; }
    public bool HideUnitColumn { get; set; }
    public bool HideServiceCustomNumber { get; set; }
    public bool UsePricesIncludingVat  { get; set; }

    public bool CollapseAttachmentPanel { get; set; }

    public string SelectedContactId { get; set; }

    public string SelectedLinkedJobId { get; set; }

    public string SelectedAssigneeId { get; set; }
    
    public List<long> SelectedEmployeeIds { get; set; }
    public string AdditionalEmployeesDataJson { get; set; }
    
    public List<long> SelectedTemplateListIds { get; set; }
    public string TemplateListIdsDataJson { get; set; }

    public string SelectedAddressId { get; set; }

    public string JsonServices { get; set; }
    public string JsonTaxes { get; set; }
    public string JsonTableDataSource { get; set; }
    public string JsonNotes { get; set; }

    public string DateCreation { get; set; }
    public int DelayInvoice { get; set; }
    public bool RoundAmount { get; set; }

    public string JsonAttachments { get; private set; }
    public Dictionary<long, string> Attachments = new Dictionary<long, string>();

    public List<long> ServiceJobsIds { get; set; }

    public bool IsInvoice { get; private set; }
    public bool IsProject { get; private set; }
    public bool IsDeliveryNote { get; private set; }

    Calani.BusinessObjects.CustomerAdmin.OrganizationManager orgmgr { get; set; }
    long organizationId;

    public Calani.BusinessObjects.Projects.jobsExtended Info { get; private set; }
    public recurringinfos Recurringinfo { get; private set; }

    public bool IsAdmin { get; set; }
    public bool EnableRefNumInheritance { get; set; }

    public double ExpectedHours { get; private set; }
    public double InvoicedHours { get; private set; }
    public double AwaitingImportHours { get; private set; }
    public double RemainingHours { get; private set; }

    public float DiscountPercent { get; private set; }
    public TypeOfDiscountEnum DiscountType { get; private set; }
    public float DiscountPercent2 { get; private set; }
    public TypeOfDiscountEnum DiscountType2 { get; private set; }
    public float DiscountPercent3 { get; private set; }
    public TypeOfDiscountEnum DiscountType3 { get; private set; }
    public float DiscountPercent4 { get; private set; }
    public TypeOfDiscountEnum DiscountType4 { get; private set; }

    private Projects_Opens_ImportTimeSheetRecords TimesheetsControl { get; set; }

    private string GetReadOnlyLink(long id)
    {
        string dataStr = JsonConvert.SerializeObject(new { id = id, org = organizationId });

        var data = Convert.ToBase64String(Encoding.UTF8.GetBytes(dataStr));

        return "~/Projects/View/Project.aspx?data=" + data;
    }

    public string ProjetIdForJs
    {
        get
        {
            if (ProjectId != null) 
                return ProjectId.Value.ToString();

            else return "null";
        }
    }

    public string CurrenciesForJs { get; set; }

    public string SendPage
    {
        get
        {
            string ret = "";
            for (int i = 0; i < Request.Url.Segments.Length - 1; i++)
            {
                ret += Request.Url.Segments[i];
            }
            return ret + "Send.aspx";
        }
    }

    public void SetReadOnly(bool ro)
    {
        tbxInternalId.Enabled = !ro;
        ddlCustomer.Enabled = !ro;
        ddlCustomerAddress.Enabled = !ro;
        ddlCustomerContact.Enabled = !ro;
        ddlAssignee.Enabled = !ro;
        tbxDeliveryDate.Disabled = ro;
        tbxDescription.Disabled = ro;
        tbxMeetingDescription.Disabled = ro;
        tbxValidityDate.Disabled = ro;
        tbxInvoiceValidityDate.Disabled = ro;
        ratingStarsQuote.Disabled = ro;
        tbxProjectDeadLine.Disabled = ro;
        tbxProjectVisitsForfait.Disabled = ro;
        ddlCurrency.Enabled = !ro;
        tbxShareLink.Visible = !ro;
        tbxComment.Disabled = ro;

        panelAddLine.Visible = !ro;
        tbxNewNote.Visible = !ro;
        btnAddNoteContainer.Visible = !ro;
        btnAddReportContainer.Visible = !ro;

        liNewAttachment.Visible = !ro;


    }

    protected void Page_Load(object sender, EventArgs e)
    {
        bool isReadOnly = IsReadOnly.Equals("true", StringComparison.InvariantCultureIgnoreCase);

        IsInvoice = Request.Url.ToString().ToLower().Contains("/invoices/");//RecurringInvoices
        IsProject = Request.Url.ToString().ToLower().Contains("/opens/");     

        #region Metadata

        // auto detect id from url if ProjetId property is not set
        if (IsReadOnly=="false" && ProjectId == null && Request.Params["id"] != null)
        {
            long id = -1;
            if(long.TryParse(Request.Params["id"], out id))
            {
                ProjectId = id;
            }
        
        }
        else if (IsReadOnly == "true" && ProjectId == null && Request.Params["data"] != null)
        {
            
                var str = Encoding.UTF8.GetString(Convert.FromBase64String(Request.Params["data"]));

                var obj = JsonConvert.DeserializeObject<dynamic>(str);

                ProjectId = obj.id;
                organizationId = obj.org;
            
        }
        // -->

        if (organizationId < 1)
            organizationId = PageHelper.GetCurrentOrganizationId(Session);

        orgmgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(organizationId);
        orgmgr.Load();

        #endregion Metadata

        RoundAmount = orgmgr.RoundAmount;
        divRoundedAmount.Visible = RoundAmount;
        btnAddNoteContainer.Visible = ProjectId != null;

        bool hideBillingInfo = orgmgr.HideBillingElementsOnProject && !isReadOnly && !IsAdmin;
        
        #region Currencies

        var currencies = PageHelper.GetOrgCurrencies(organizationId);

        if (currencies!=null && currencies.Any())
        {
            CurrenciesForJs = new JavaScriptSerializer().Serialize(currencies);

            divProject_Currency.Visible = true;
            ddlCurrency.DataSource = currencies;
            ddlCurrency.DataBind();
        }
        else
        {
            CurrenciesForJs = "null";
            divProject_Currency.Visible = false;
        }

        #endregion Currencies

        var notes = new List<Calani.BusinessObjects.MobileService.NotesExtended>();

        var mgr = new ProjectsManager(organizationId);
        Calani.BusinessObjects.Projects.jobsExtended info = null;

        #region Load Project

        if (ProjectId != null)
        {
            info = mgr.GetInfo(ProjectId.Value);
            if (info.recurringinfos1.Any()) info.recurringinfos = info.recurringinfos1.First().CloneRecurringInfo();
            
            info.AdditionalEmployeeIds = new List<long>();
            info.TemplateListIds = new List<long>();
            
            if(info.dateCreated.HasValue)
                DateCreation = info.dateCreated.Value.ToString("s");

            DelayInvoice = info.organizations?.delayInvoice ?? 0;
            
            var notesMgr = new Calani.BusinessObjects.MobileService.NotesManager(organizationId);
            notes = notesMgr.ListNotesForJob(ProjectId.Value);


            if (!String.IsNullOrWhiteSpace(info.currency))
            {
                var currid = (from r in currencies where r.id !=null && r.currency1 == info.currency select r.id.ToString()).FirstOrDefault();
                ddlCurrency.SelectedValue = currid;
            }

            DiscountPercent = info.discount_percent ?? 0;
            DiscountType = (TypeOfDiscountEnum)(info.discount_type ?? 1);
            DiscountPercent2 = info.discount_percent2 ?? 0;
            DiscountType2 = (TypeOfDiscountEnum)(info.discount_type2 ?? 1);
            DiscountPercent3 = info.discount_percent3 ?? 0;
            DiscountType3 = (TypeOfDiscountEnum)(info.discount_type3 ?? 1);
            DiscountPercent4 = info.discount_percent4 ?? 0;
            DiscountType4 = (TypeOfDiscountEnum)(info.discount_type4 ?? 1);
        }
        else
        {
            bool isQuote = Request.Url.ToString().ToLower().Contains("/quotes/");
            bool isJob = Request.Url.ToString().ToLower().Contains("/opens/");
            bool isDeliveryNote = Request.Url.ToString().ToLower().Contains("/deliverynotes/");
            bool isRecurringInvoice = Request.Url.ToString().ToLower().Contains("/recurringinvoices/");
            bool isInvoice = isRecurringInvoice || Request.Url.ToString().ToLower().Contains("/invoices/");

            // protect status
            bool wrongViewer = false;
            string rightViewerUrl = "~/Projects/";

            if (info != null)
            {
                if (isQuote && !info.ProjectStatus.ToString().StartsWith("Quote"))
                {
                    wrongViewer = true;
                    rightViewerUrl += "Quotes";
                }

            
                if (isJob && !info.ProjectStatus.ToString().StartsWith("Job"))
                {
                    wrongViewer = true;
                    rightViewerUrl += "Opens";
                }


                if ((isInvoice || isRecurringInvoice) && !info.ProjectStatus.ToString().StartsWith("Invoice"))
                {
                    wrongViewer = true;
                    rightViewerUrl += "Invoices";
                }

                if (isDeliveryNote && !info.ProjectStatus.ToString().StartsWith("DeliveryNote"))
                {
                    wrongViewer = true;
                    rightViewerUrl += "DeliveryNotes";
                }

                rightViewerUrl += "/Job.aspx?id=" + info.id;
                if (wrongViewer)
                {
                    Response.Redirect(rightViewerUrl);
                    return;
                }
            }
            // -->

            info = new Calani.BusinessObjects.Projects.jobsExtended(isQuote, isJob, isInvoice, isDeliveryNote, organizationId);           
            info.recurringinfos = isRecurringInvoice ? new recurringinfos{id = 0} : null; //getINFO
        }

        #endregion Load Project

        #region Customers

        var custMgr = new Calani.BusinessObjects.CustomerAdmin.ClientsManager(organizationId);
        var customers = custMgr.List();
        ddlCustomer.DataSource = customers;
        int urlCust = -10;


        if(info.clientId == 0 && Request.Params["cust"] != null)
        {
            if (Int32.TryParse(Request.Params["cust"], out urlCust))
            {
                if ((from r in customers where r.id == urlCust select r).Count() > 0)
                    info.clientId = urlCust;
            }
        }
        if ((from r in customers where r.id == info.clientId select r).Count() > 0) 
            ddlCustomer.SelectedValue = info.clientId.ToString();
        
        else
        {
            if (customers.Count > 0)
            {
                info.clientId = customers.First().id;
                ddlCustomer.SelectedValue = info.clientId.ToString();
            }
        }
        ddlCustomer.DataBind();

        if (ProjectId == null && urlCust < 0)
        {
            foreach (var item in ddlCustomer.Items)
            {
                (item as ListItem).Selected = false;
            }
            ddlCustomer.Items.Insert(0, new ListItem { Enabled = true, Value = "", Text = Resources.Resource.Choose, Selected = true });
            info.clientId = -10;
        }

        #endregion Customers
        
        #region Addresses

        var adrmgr = new Calani.BusinessObjects.Contacts.ClientAddressesManager(organizationId, info.clientId);
        List<Calani.BusinessObjects.Model.addresses> addresses = new List<Calani.BusinessObjects.Model.addresses>();  
        if (info.clientId >= 0)
            addresses = adrmgr.List();

        if (addresses.Count > 0)
        {
            ddlCustomerAddress.DataSource = addresses;
            if ((from r in addresses where r.id == info.clientAddress select r).Count() > 0)
            {
                SelectedAddressId = Convert2.ToString(info.clientAddress);
                ddlCustomerAddress.SelectedValue = SelectedAddressId;
            }
            ddlCustomerAddress.DataBind();
        }
        if (SelectedAddressId == null) 
            SelectedAddressId = "-1";

        #endregion Addresses

        #region Contacts

        var ctcmgr = new Calani.BusinessObjects.Contacts.ClientContactsManager(organizationId, info.clientId);
        var contacts = new List<Calani.BusinessObjects.Model.contacts>();
        if (info.clientId >= 0)
            contacts = ctcmgr.List();

        if (contacts.Count > 0)
        {
            ddlCustomerContact.DataSource = contacts;
            if ((from r in contacts where r.id == info.clientContact select r).Count() > 0)
            {
                SelectedContactId = Convert2.ToString(info.clientContact);
                ddlCustomerContact.SelectedValue = SelectedContactId;
            }
            ddlCustomerContact.DataBind();
        }

        if (SelectedContactId == null) 
            SelectedContactId = "-1";

        #endregion Contacts
        
        #region OpenprojectJob
        Calani.BusinessObjects.Projects.ProjectsManager projMgr = new Calani.BusinessObjects.Projects.ProjectsManager(organizationId);
        List<JsVariableForTemplate> projects = new List<JsVariableForTemplate>();
        if (info.clientId >= 0)
        {
            projects = projMgr.ListOpens(info.clientId)
                .Where(r => (r.dateJobDone == null && r.dateJobValidity > DateTime.Now
                               || r.dateJobDone != null && r.importedIn == null && !r.skipMissingInvoiceNotif))
                .Select(r => new JsVariableForTemplate
                {
                    value = r.id.ToString(),
                    name = r.internalId
                })
                .ToList();
        }
        projects.Insert(0, new JsVariableForTemplate { value = "", name = "(" + Resources.Resource.None + ")" });
        if (projects.Count > 0)
        {
            ddlJob.DataSource = projects;
            if ((from r in projects
                 where long.TryParse(r.value, out long parsedValue) && parsedValue == (info.linkedjobid.HasValue ? info.linkedjobid.Value : 0)
                 select r).Count() > 0)
            {
                SelectedLinkedJobId = Convert2.ToString(info.linkedjobid);
                ddlJob.SelectedValue = SelectedLinkedJobId;
            }
            ddlJob.DataBind();
        }

        if (SelectedLinkedJobId == null)
            SelectedLinkedJobId = "-1";

        #endregion OpenprojectJob

        #region Employees & AdditionalEmployees

        var employeesMgr = new EmployeesManager(organizationId);
        var employees = employeesMgr.ListEmployeesRealLightweight();
        if (employees.Count > 0)
        {
            ddlAssignee.DataSource = employees;
            if ((from r in employees where r.id == info.assigneeId select r).Any())
            {
                SelectedAssigneeId = Convert2.ToString(info.assigneeId);
                ddlAssignee.SelectedValue = SelectedAssigneeId;
            }
            ddlAssignee.DataBind();

            #region AdditionalEmployees
            
            AdditionalEmployeesDataJson = JsonConvert.SerializeObject(employees.Where(x => x.type != 0));
            SelectedEmployeeIds = new CalaniEntities().projectemployees
                .Where(x => x.projectId == info.id)?
                .Select(x => x.employeeId).ToList() ?? new List<long>();

            #endregion
        }

        #endregion Employees
        
        #region TemplateList
        
        var templateManager = new TemplateManager(organizationId);
        TemplateListIdsDataJson = JsonConvert.SerializeObject(templateManager.ListTemplatesLightWeight());
        
        SelectedTemplateListIds = new CalaniEntities().jobs_templates
            .Where(x => x.jobId == info.id)
            .Select(x => x.templateId).ToList() ?? new List<long>();
        
        #endregion
        
        long pid;

        long.TryParse(Request.Params["pid"], out pid);

        if (pid > 0)
        {
            #region For New Delivery Note

            var mgrprj = new ProjectsManager(organizationId);
            Calani.BusinessObjects.Projects.jobsExtended infoprj = null;

            infoprj = mgrprj.GetInfo(pid);

            #region Customers
            var custprjmgr = new Calani.BusinessObjects.CustomerAdmin.ClientsManager(organizationId);
            var customersprj = custprjmgr.List();
            ddlCustomer.DataSource = customersprj;

            if ((from r in customersprj where r.id == infoprj.clientId select r).Count() > 0)
                ddlCustomer.SelectedValue = infoprj.clientId.ToString();

            else
            {
                if (customersprj.Count > 0)
                {
                    infoprj.clientId = customersprj.First().id;
                    ddlCustomer.SelectedValue = infoprj.clientId.ToString();
                }
            }
            ddlCustomer.DataBind();
            #endregion Customers

            #region address
            var adrprjmgr = new Calani.BusinessObjects.Contacts.ClientAddressesManager(organizationId, infoprj.clientId);
            List<Calani.BusinessObjects.Model.addresses> addressesprj = new List<Calani.BusinessObjects.Model.addresses>();

            if (infoprj.clientId >= 0)
                addressesprj = adrprjmgr.List();

            if (addressesprj.Count > 0)
            {
                ddlCustomerAddress.DataSource = addressesprj;
                if ((from r in addressesprj where r.id == infoprj.clientAddress select r).Count() > 0)
                {
                    SelectedAddressId = Convert2.ToString(infoprj.clientAddress);
                    ddlCustomerAddress.SelectedValue = SelectedAddressId;
                }
                ddlCustomerAddress.DataBind();

            }
            #endregion address

            #region contact
            var ctcprjmgr = new Calani.BusinessObjects.Contacts.ClientContactsManager(organizationId, infoprj.clientId);
            var contactsprj = new List<Calani.BusinessObjects.Model.contacts>();
            if (infoprj.clientId >= 0)
                contactsprj = ctcprjmgr.List();

            if (contactsprj.Count > 0)
            {
                ddlCustomerContact.DataSource = contactsprj;
                if ((from r in contactsprj where r.id == infoprj.clientContact select r).Count() > 0)
                {
                    SelectedContactId = Convert2.ToString(infoprj.clientContact);
                    ddlCustomerContact.SelectedValue = SelectedContactId;
                }
                ddlCustomerContact.DataBind();
            }
            #endregion contact

            #region jobs
            Calani.BusinessObjects.Projects.ProjectsManager jobprojMgr = new Calani.BusinessObjects.Projects.ProjectsManager(organizationId);
            List<JsVariableForTemplate> jobprj = new List<JsVariableForTemplate>();
            if (infoprj.clientId >= 0)
            {
                jobprj = jobprojMgr.ListOpens(infoprj.clientId)
                    .Where(r => (r.dateJobDone == null && r.dateJobValidity > DateTime.Now
                                   || r.dateJobDone != null && r.importedIn == null && !r.skipMissingInvoiceNotif))
                    .Select(r => new JsVariableForTemplate
                    {
                        value = r.id.ToString(),
                        name = r.internalId
                    })
                    .ToList();
            }
            jobprj.Insert(0, new JsVariableForTemplate { value = "", name = "(" + Resources.Resource.None + ")" });
            if (jobprj.Count > 0)
            {
                ddlJob.DataSource = jobprj;
                if ((from r in jobprj where long.TryParse(r.value, out long parsedValue) && parsedValue == infoprj.id select r).Count() > 0)
                {
                    SelectedLinkedJobId = infoprj.id.ToString();
                    ddlJob.SelectedValue = SelectedLinkedJobId;
                }
                ddlJob.DataBind();
            }

            if (SelectedLinkedJobId == null)
                SelectedLinkedJobId = "-1";
            #endregion jobs

            #endregion For New Delivery Note
        }
        if (SelectedAssigneeId == null)
            SelectedAssigneeId = "-1";
        //else //VisitDetails
        //{
            var visit = info.visits.FirstOrDefault(v=>v.recordStatus == 0);
            if(visit!=null)
                tbxDeliveryDate.Value = visit.dateStart.HasValue? visit.dateStart.Value.ToString("yyyy-MM-dd HH:mm") : String.Empty;
        //}

        var srvmgr = new Calani.BusinessObjects.CustomerAdmin.ServicesManager(organizationId);
        var services = (from r in srvmgr.List2(includeTaxes: false, includeUnit: true) select new ServiceExp(r)).ToList();
        JsonServices = new JavaScriptSerializer().Serialize(services);

        var taxmgr = new Calani.BusinessObjects.CustomerAdmin.TaxesManager(organizationId);
        var defaultTaxId = taxmgr.GetDetaultTax();
        var export2 = (from r in taxmgr.List() select new TaxExp(r, defaultTaxId)).ToList();
        JsonTaxes = new JavaScriptSerializer().Serialize(export2);

        #region serviceJobsIds cookie

        List<long> serviceJobsIds = ServiceJobsIds ?? new List<long>();
        if (!serviceJobsIds.Any())
        {
            var cookie = Request.Cookies["serviceJobsIds"];
            if (cookie != null)
            {

                serviceJobsIds = JsonConvert.DeserializeObject<List<long>>(cookie.Value);

                Response.Cookies["serviceJobsIds"].Expires = DateTime.Now;
            }
        }

        #endregion serviceJobsIds cookie

        #region ServicesJobs

        if (serviceJobsIds.Any())
        {
            info.services_jobs = mgr.GetServicesJobsByIds(serviceJobsIds);

            if (orgmgr.EnableRefNumberInheritance)
            {
                InheritRefNumberFromService(ref info, mgr);
            }
        }

        #endregion ServicesJobs

        #region Title & Reports

        panelReports.Visible = false;
        panelDeliveryNotes.Visible = false;
        JsonNotes = new JavaScriptSerializer().Serialize(notes);

        if (info.ProjectStatus.ToString().StartsWith("Quote"))
            h5Title.InnerText = Resources.Resource.Quote;

        else if (info.ProjectStatus.ToString().StartsWith("DeliveryNote"))
            h5Title.InnerText = Resources.Resource.DeliveryNote;

        else if (info.ProjectStatus.ToString().StartsWith("Job"))
        {
            h5Title.InnerText = Resources.Resource.Job;
            panelReports.Visible = true;
            panelDeliveryNotes.Visible = true;
        }

        else if (info.ProjectStatus.ToString().StartsWith("Invoice"))
            h5Title.InnerText = Resources.Resource.Invoice;

        h5Title.InnerText += " " + info.internalId;

        Page.Title = h5Title.InnerText + " &bull; " + System.Configuration.ConfigurationSettings.AppSettings["title"];
        Label lblPageTitle = (this.Page.Master.FindControl("LabelPageTitle") as Label);
        if (lblPageTitle != null)
            lblPageTitle.Text = h5Title.InnerText;

        #endregion Title & Reports

        var orgMgr = new OrganizationManager(organizationId);
        orgMgr.Load();
        var items = info.ExportItems().OrderBy(i => i.position).ToList();
        
        UsePricesIncludingVat = orgMgr.UsePriceIncludingVat;
        if (UsePricesIncludingVat)
        {
            OnDisabledUsePriceIncludingVat1.Style.Add("display", "none");
            OnDisabledUsePriceIncludingVat2.Style.Add("display", "none");
            OnDisabledUsePriceIncludingVat3.Style.Add("display", "none");
           
            foreach (var item in items.Where(x => x.serviceVatIncl))
            {
                item.totalprice = item.price * item.qty;
            }
        }
        
        JsonTableDataSource = new JavaScriptSerializer().Serialize(items);

        RowStillToInvoice.Visible = serviceJobsIds.Any() || (from r in items where r.toinvoice != null select r).Any();

        LoadAttachments();

        LoadReports();
        LoadDeliveryNotes();
        LoadTaskLists();

        #region Options

        divQuoteField_dateQuoteValidity.Visible = EnableQuoteFields;
        divQuoteField_rating.Visible = EnableQuoteFields;
        divDeliveryNotesField_assignee.Visible = EnableDeliveryNotesFields;
        divDeliveryNotesField_deliveryDate.Visible = EnableDeliveryNotesFields;
        
        DisableInvoicedOnly = orgMgr.DisableInvoicedOnly;
        HideUnitColumn = orgmgr.HideUnitColumn;
        HideServiceCustomNumber = !orgmgr.DisplayServiceCustomNumber;
        
        if (EnableQuoteFields)
        {
            if (info.dateQuoteValidity != null)
            {
                tbxValidityDate.Value = Convert2.DateToStrForInput(info.dateQuoteValidity, "picker");
            }
            else
            {
                DateTime dateQuoteValidity = DateTime.Today;
                
                dateQuoteValidity = dateQuoteValidity.AddDays(orgmgr.DelayQuote);
                tbxValidityDate.Value = Convert2.DateToStrForInput(dateQuoteValidity, "picker");
            }
            ratingStarsQuote.Value = info.rating;
        }

        divQuoteField_dateInvoiceValidity.Visible = EnableInvoiceFields;

        if(EnableInvoiceFields)
        {
            if (info.dateInvoiceDue != null)
            {
                tbxInvoiceValidityDate.Value = Convert2.DateToStrForInput(info.dateInvoiceDue, "picker");
            }
            else
            {
                DateTime dateInvoiceValidity = DateTime.Today;
               
                dateInvoiceValidity = dateInvoiceValidity.AddDays(orgmgr.DelayInvoice);
                tbxInvoiceValidityDate.Value = Convert2.DateToStrForInput(dateInvoiceValidity, "picker");
            }
        }

        divProject_Deadline.Visible = EnableProjectFields;
        divProject_Visits.Visible = EnableProjectFields;
        divProject_VisitsStatus.Visible = EnableProjectFields;

        if (EnableProjectFields)
        {
            tbxProjectDeadLine.Value = Convert2.DateToStrForInput(info.dateJobValidity, "picker");

            tbxProjectVisitsForfait.Value = info.contractualUnitsCount != null ? info.contractualUnitsCount.ToString() : string.Empty;

            int max = info.contractualUnitsCount != null ? info.contractualUnitsCount.Value : 0;

            int done = info.doneVisits;

            double percent = Convert.ToDouble(done) / Convert.ToDouble(max) * 100;
            percent = Math.Round(percent);
            if (percent > 100) percent = 100;

            int left = max - done;

            divProject_VisitsStatus_cursor.Style["width"] = percent + "%";
            lblVisitsStatusLeft.Text = done + "/" + max; // + " ("++" left)";
            if(left > 0)
            {
                lblVisitsStatusLeft.Text += " (" + Resources.Resource.Remaining_Visits + ": " + left + ")";
                divProject_VisitsStatus_cursor.Attributes["class"] = "progress-bar bg-success-400";
            }
            else
            {
                lblVisitsStatusLeft.Text += " (" + Resources.Resource.Remaining_Over + ": " + Math.Abs(left) + ")";
                divProject_VisitsStatus_cursor.Attributes["class"] = "progress-bar bg-danger-400";
            }

        }

        if (IsReadOnly=="false" && ProjectId > 0)
        {
            string domainName = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);

            string dataStr = JsonConvert.SerializeObject(new { id = ProjectId, org = organizationId });

            var data = Convert.ToBase64String(Encoding.UTF8.GetBytes(dataStr));

            tbxShareLink.Value = domainName + "/calani/Projects/View/Project.aspx?data=" + data;
        }
        else
            divShareLink.Visible = false;

        panelMainDiscount.Visible = EnableMainDiscount;

        if (CollapseAttachmentPanel)
            panelAttachments_panel.Attributes["class"] += " panel-collapsed ";

        #endregion Options

        IsRecurringInvoice = info.recurringinfos != null;
        if (IsRecurringInvoice)
        {
            divRefNr.Style.Add("display", "none");
            divQuoteField_dateInvoiceValidity.Style.Add("display", "none");

            var dbInfo = info.recurringinfos;
            Recurringinfo = dbInfo != null
                ? dbInfo.CloneRecurringInfo()
                : new recurringinfos {id = 0};
        }
        
        #region Parent

        if (info.parentId != null && !hideBillingInfo)
        {
            divLinkedProject.Visible = true;
            
            var linkedProject = mgr.GetInfo(info.parentId.Value);

            lblParentName.Text = linkedProject.internalId;
            
            lblParentValue.Text = ToTotal(linkedProject.total);

            if (linkedProject.ProjectStatus.ToString().StartsWith("Quote"))
            {
                lblLinkedProject.Text = Resources.Resource.LinkedQuote;
                lblParentType.Text = Resources.Resource.Quote;
                hlParentProject.NavigateUrl = "~/Projects/Quotes/Job.aspx?id=" + linkedProject.id;
            }
            else if (linkedProject.ProjectStatus.ToString().StartsWith("Invoice"))
            {
                lblLinkedProject.Text = Resources.Resource.LinkedInvoice;
                lblParentType.Text = Resources.Resource.Invoice;
                hlParentProject.NavigateUrl = "~/Projects/Invoices/Job.aspx?id=" + linkedProject.id;
            }
            else if (linkedProject.ProjectStatus.ToString().StartsWith("Job"))
            {
                lblLinkedProject.Text = Resources.Resource.LinkedProject;
                lblParentType.Text = Resources.Resource.Job;
                hlParentProject.NavigateUrl = "~/Projects/Opens/Job.aspx?id=" + linkedProject.id;
            }

            panelSingleParent.Visible = true;
        }
        else
        {
            panelSingleParent.Visible = false;
            divLinkedProject.Visible = false;
        }

        #endregion Parent

        #region Children

        var child = mgr.GetChildren(info.id);
        
        if (child != null && child.Count > 0 && !hideBillingInfo)
        {
            foreach (var c in child)
            {
                if (c.ProjectStatus.ToString().StartsWith("Quote"))
                {
                    c.Tag1 = Resources.Resource.Quote;

                    if(IsReadOnly == "true")
                        c.Tag2 = GetReadOnlyLink(c.id);
                    else
                        c.Tag2 = "~/Projects/Quotes/Job.aspx?id=" + c.id;
                    
                }
                else if (c.ProjectStatus.ToString().StartsWith("Invoice"))
                {
                    c.Tag1 = Resources.Resource.Invoice;
                    if (IsReadOnly == "true")
                        c.Tag2 = GetReadOnlyLink(c.id);
                    else
                        c.Tag2 = "~/Projects/Invoices/Job.aspx?id=" + c.id;
                }
                else if (c.ProjectStatus.ToString().StartsWith("Job"))
                {
                    c.Tag1 = Resources.Resource.Job;
                    if (IsReadOnly == "true")
                        c.Tag2 = GetReadOnlyLink(c.id);
                    else
                        c.Tag2 = "~/Projects/Opens/Job.aspx?id=" + c.id;

                }
                else if (c.ProjectStatus.ToString().StartsWith("DeliveryNote"))
                {
                    c.Tag1 = Resources.Resource.DeliveryNote;
                    if (IsReadOnly == "true")
                        c.Tag2 = GetReadOnlyLink(c.id);
                    else
                        c.Tag2 = "~/Projects/DeliveryNotes/Job.aspx?id=" + c.id;

                }
            }

            divChildrenProject.Visible = true;
            repeaterChildren.DataSource = child;
            repeaterChildren.DataBind(); 
        }
        else
        {
            divChildrenProject.Visible = false;
        }

        #endregion Children

        #region Imports

        var imports = mgr.GetImports(info.id);

        if (imports != null && imports.Count > 0)
        {
            foreach (var c in imports)
            {
                if (c.ProjectStatus.ToString().StartsWith("Quote"))
                {
                    c.Tag1 = Resources.Resource.Quote;
                    if (IsReadOnly == "true")
                        c.Tag2 = GetReadOnlyLink(c.id);
                    else
                        c.Tag2 = "~/Projects/Quotes/Job.aspx?id=" + c.id;

                }
                else if (c.ProjectStatus.ToString().StartsWith("Invoice"))
                {
                    c.Tag1 = Resources.Resource.Invoice;
                    if (IsReadOnly == "true")
                        c.Tag2 = GetReadOnlyLink(c.id);
                    else
                        c.Tag2 = "~/Projects/Invoices/Job.aspx?id=" + c.id;
                }
                else if (c.ProjectStatus.ToString().StartsWith("Job"))
                {
                    c.Tag1 = Resources.Resource.Job;
                    if (IsReadOnly == "true")
                        c.Tag2 = GetReadOnlyLink(c.id);
                    else
                        c.Tag2 = "~/Projects/Opens/Job.aspx?id=" + c.id;

                }
                else if (c.ProjectStatus.ToString().StartsWith("DeliveryNote"))
                {
                    c.Tag1 = Resources.Resource.DeliveryNote;
                    if (IsReadOnly == "true")
                        c.Tag2 = GetReadOnlyLink(c.id);
                    else
                        c.Tag2 = "~/Projects/DeliveryNotes/Job.aspx?id=" + c.id;

                }

            }

            lblLinkedProject.Text = Resources.Resource.LinkedQuote;
            divLinkedProject.Visible = !hideBillingInfo;
            repeaterImportedInList.DataSource = imports;
            repeaterImportedInList.DataBind();
        }

        #endregion Imports

        #region Linked Invoices

        var linkedInvoices = (from r in info.services_jobs where r.toinvoice != null && r.toinvoice > -1 select r.toinvoice).Distinct().ToList();
        if (linkedInvoices.Count > 0 && !hideBillingInfo)
        {
            divPartInvoices.Visible = true;

            var db = new Calani.BusinessObjects.Model.CalaniEntities();
            var linkedInvoicesData = (from r in db.jobs where linkedInvoices.Contains(r.id) && r.organizationId == info.organizationId select new
            {
                internalId = r.internalId,
                url = "Invoices/Job.aspx?id=" + r.id,
                total = r.total,
                dateInvoicing = r.dateInvoicing
            }).ToList();

            repeaterPartialInvoices.DataSource = linkedInvoicesData;
            repeaterPartialInvoices.DataBind();
            new Html5GridView(repeaterPartialInvoices, false, null);

            divPartInvoices.Visible = true;
        }
        else
        {
            divPartInvoices.Visible = false;
        }

        #endregion Linked Invoices

        #region Expenses

        var expMgr = new ExpenseManager(organizationId);

        var linkedExpenses = expMgr.GetByProjectId(info.id).Where(exp => (ValidationEnum)exp.status == ValidationEnum.Approved);
        if (linkedExpenses.Any())
        {
            gridExpenses.DataSource = linkedExpenses;
            gridExpenses.DataBind();
            lblNoData.Visible = false;
            divExpenses.Visible = true;
            var rowData = new List<string>() { "date", "status" };
            new Html5GridView(gridExpenses, false, rowData);
        }
        else
        {
            divExpenses.Visible = false;
        }

        #endregion Expenses

        #region Imported Timesheets

        PanelImportTimeSheets.Visible = EnableProjectFields;
        PanelImportExpenses.Visible = EnableProjectFields;

        if (EnableProjectFields)
        {
            Control tsImportCtrl = this.LoadControl("~/Projects/Opens/ImportTimeSheetRecords.ascx");
            PanelImportTimeSheets.Controls.Add(tsImportCtrl);

            TimesheetsControl = tsImportCtrl as Projects_Opens_ImportTimeSheetRecords;

            Control tsImportExpenses = this.LoadControl("~/Projects/Opens/ImportExpenses.ascx");
            PanelImportTimeSheets.Controls.Add(tsImportExpenses);
        }

        #endregion Imported Timesheets

        var mi = this.Page.GetType().GetMethod("Load");
        if(mi != null)
        {
            Info = info;
            mi.Invoke(this.Page, null);
        }

        SetReadOnly(IsReadOnly == "true");

        if (IsProject || isReadOnly)
        {
            ExpectedHours = GetExpectedHours(info, services);
            InvoicedHours = GetInvoicedHoursCurrYear(info, mgr);
        }

        hfColor.Value = info.color;
        tbxDescription.Value = info.description;
        tbxMeetingDescription.Value = info.meetingDescriptionText;
        tbxComment.Value = info.comment;
        tbxSendCopyEmailTo.Text = info.send_copy_email_to;
        tbxDiscount.Value = Convert2.ToString(info.discount);
        tbxInternalId.Text = info.internalId;
        tbxCustomerReference.Text = info.customer_reference;
        tbxTypeOfIntervention.Text = info.type_intervention;
        tbxLocationOfIntervention.Text = info.location_intervention;
        tbxLocationOfIntervention.Attributes["maxLength"] = "400";
        totalQuantityGroup.Visible = false;
        if (orgmgr.DisplayTotalQuantity)
        {
            lblTotalHt.Attributes["class"] = lblTotalHt.Attributes["class"].Replace("col-lg-6", "col-lg-1");
            totalQuantityGroup.Visible = true;
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if (IsProject || IsReadOnly.ToLowerInvariant() == "true")
        {
            AwaitingImportHours = TimesheetsControl != null ? TimesheetsControl.HoursCurrYear : 0;
        }
    }

    public string ToTotal(double? total)
    {
        string ret = "N/A";
        if(total != null)
        {
            double t = Math.Round(total.Value);
            ret = string.Format("{0:0,0.00}", t) + " ";
            ret += Currency; // bug c'est toujours dens euros

        }
        return ret;
    }

    private void InheritRefNumberFromService(ref jobsExtended info, ProjectsManager projectMgr)
    {
        if (info == null || info.services_jobs == null)
            return;

        var service = info.services_jobs.FirstOrDefault(sj => sj.services != null);
        if (service == null || service.jobs == null)
            return;

        var projectInternalNum = service.jobs.internalId;
        var inc = new IncrementalNumber(projectInternalNum, addDash: true);

        while(projectMgr.IsFreeName(inc.ToString()) == false)
        {
            inc.Increment();
        }

        info.internalId = inc.ToString();
    }

    string _currency;

    public string Currency
    {
        get
        {
            if (_currency == null)
            {
                
                string ret = orgmgr.Currency;
                if (String.IsNullOrEmpty(ret)) ret = "€";
                _currency = ret;
                return ret;
            }
            else
            {
                return _currency;
            }
        }

    }

    private void LoadReports()
    {
        panelReports.Visible = panelReports.Visible && ProjectId.HasValue;
        var mngr = new WorkReportsManager(organizationId);

        var list = mngr.GeList(IsReadOnly == "true" ? WorkReportStatus.Validated: WorkReportStatus.All, orgmgr.CompanyName, ProjectId??0).OrderByDescending(r => r.UpdatedAt).ToList();
        gridReports.DataSource = list;
        gridReports.DataBind();
        new Html5GridView(gridReports, false, null, addDateOrder: false);

        lblREportsCount.InnerHtml = list.Count.ToString();
        lblAttachmentsCount.Visible = list.Count > 0;
    }
    
    private void LoadTaskLists()
    {
        panelTaskLists.Visible = panelTaskLists.Visible && ProjectId.HasValue;
        tasksExistsLabel.Text = $"{Resources.Resource.EnteredTaskLists}:";
        noTasksLabel.Text = Resources.Resource.NoData; 
        if (ProjectId == null) {
            panelTaskLists.Visible = false;
            return;
        }
        var taskListMgr = new TaskListManager(organizationId);
        var list = taskListMgr.ListTaskListsForProject(ProjectId.Value);
        gridTaskLists.DataSource = list;
        gridTaskLists.DataBind();
        
        lblTaskListsCounts.InnerHtml = list.Count.ToString();
        tasksExistsLabel.Visible = list.Any();
        noTasksLabel.Visible = !list.Any();
        
        gridTaskLists.Columns[0].HeaderText = Resources.Resource.Listname;
        gridTaskLists.Columns[1].HeaderText = Resources.Resource.Date;
        gridTaskLists.Columns[2].HeaderText = Resources.Resource.EnteredBy;
        new Html5GridView(gridTaskLists, false, addDateOrder: false);
    }
    
    #region Delivery notes binding
    private void LoadDeliveryNotes()
    {
        panelDeliveryNotes.Visible = panelDeliveryNotes.Visible && ProjectId.HasValue;
        bool chcekProjectId = ProjectId.HasValue;
        if (chcekProjectId == true)
        {
            var mngrDeliveryNote = new ProjectsManager(organizationId);
            Calani.BusinessObjects.Projects.jobsExtended info = null;
            info = mngrDeliveryNote.GetInfo(ProjectId ?? 0);

            var linkedDeliveryNotes = mngrDeliveryNote.ListLinkedDeliveryNotes(info.id);

            gridDeliveryNotes.DataSource = linkedDeliveryNotes;
            gridDeliveryNotes.DataBind();
            var rowData = new List<string>() { "date", "status" };
            new Html5GridView(gridDeliveryNotes, false, null, addDateOrder: false);

            lblDeliveryNotesCounts.InnerHtml = linkedDeliveryNotes.Count.ToString();
        }
        else
        {
            panelDeliveryNotes.Visible = false;
        }
    }
protected string GetStatusText(object status)
    {
        switch (status.ToString())
        {
            case "DeliveryNoteDraft":
                return Resources.Resource.Draft;
            case "DeliveryNoteSent":
                return Resources.Resource.Sent;
            case "DeliveryNoteAccepted":
                return Resources.Resource.Declined;
            case "QuoteDraft":
                return Resources.Resource.Draft;
            case "QuoteSent":
                return Resources.Resource.Sent;
            case "QuoteAccepted":
                return Resources.Resource.Accepted;

            case "QuoteRefused":
                return Resources.Resource.Declined;
            case "QuoteOutDated":
                return Resources.Resource.OutdatedQuote;
            case "JobOpened":
                return Resources.Resource.Opened;
            case "JobFinished":
                return Resources.Resource.Finished;
            case "JobOnHold":
                return Resources.Resource.On_hold;
            case "JobOutDated":
                return Resources.Resource.OutdatedJob;
            case "InvoiceDraft":
                return Resources.Resource.Draft;
            case "InvoicePaymentDue":
                return Resources.Resource.Pending;
            case "InvoicePaid":
                return Resources.Resource.Paid;
            case "InvoicePaymentLate":
                return Resources.Resource.Late;

            default:
                return "Unknown";
        }
    }
    #endregion Delivery notes binding
    private void LoadAttachments()
    {       
        panelAttachments.Visible = true;

        Calani.BusinessObjects.Attachments.AttachmentsManager attmgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
        attmgr.OrganizationId = organizationId;
        var ds =  new List<attachmentsExtended>();

        if (this.ProjectId.HasValue)
            ds.AddRange(attmgr.ListJobAttachments(this.ProjectId.Value));

        List<int> filter = Calani.BusinessObjects.Attachments.AttachmentsManager.GetNonGeneratedTypes();
        var dsAttachments = (from r in ds where r.type != null && filter.Contains(r.type.Value) select r).ToList();
        gridFilesAttachments.DataSource = dsAttachments;
        gridFilesAttachments.DataBind();
        new Html5GridView(gridFilesAttachments, false, null);
        lblAttachmentsCount.InnerHtml = dsAttachments.Count.ToString();
        lblAttachmentsCount.Visible = dsAttachments.Any();

        Attachments = dsAttachments.ToDictionary(a => a.id, a => a.NameWithExtension);
        JsonAttachments = new JavaScriptSerializer().Serialize(dsAttachments.Select(a => new { id = a.id, title = a.NameWithExtension }));

        filter = Calani.BusinessObjects.Attachments.AttachmentsManager.GetGeneratedTypes();
        var dsHistory = (from r in ds where r.type != null && filter.Contains(r.type.Value) select r).ToList();
        gridFilesHistory.DataSource = dsHistory;
        gridFilesHistory.DataBind();
        new Html5GridView(gridFilesHistory, false, null);

        if (dsAttachments.Count == 0 )
        {
            liAttachments.Visible = false;
            docstabAttachments.Visible = false;
            if (IsReadOnly == "false")
            {
                liNewAttachment.Attributes["class"] = "active";
                docstabNew.Attributes["class"] = "tab-pane active";
            }
            else
            {
                liHistory.Attributes["class"] = "active";
                docstabHistory.Attributes["class"] = "tab-pane active";
            }             

        }

    }

    private double GetExpectedHours(jobsExtended job, IList<ServiceExp> services)
    {
        if (job == null || job.services_jobs == null || !job.services_jobs.Any())
            return 0;

        double hours = 0;

        var servicesMap = services.Any() ? services.ToDictionary(s => s.id) : new Dictionary<long, ServiceExp>();

        if(job.services_jobs.Any(sj => sj.timesheetDate != null && sj.serviceId.HasValue
                                                            && servicesMap.ContainsKey(sj.serviceId.Value)
                                                            && servicesMap[sj.serviceId.Value].unit == ServiceUnitEnum.Hour))
        {
            hours = job.services_jobs.First().serviceQuantity ?? 0;
        }

        return hours;
    }

    private double GetInvoicedHoursCurrYear(jobsExtended job, ProjectsManager manager)
    {
        if (job == null)
            return 0;

        var hours = manager.GetInvoicedHoursCurrYear(job.id);
        return hours;
    }

    public class TaxExp
    {
        public long id { get; set; }
        public string name { get; set; }
        public double rate { get; set; }
        public bool isdefault { get; set; }

        public TaxExp()
        {
        }

        public TaxExp(Calani.BusinessObjects.Model.taxes t, long defaultTaxId)
        {
            id = t.id;
            name = t.name;
            rate = t.rate;
            isdefault = id == defaultTaxId;
        }        
    }

    public class ServiceExp
    {
        public long id { get; set; }
        public string internalId { get; set; }
        public string name { get; set; }
        public double unitPrice { get; set; }
        public long? taxId {get;set;}
        public ServiceUnitEnum unit { get; set; }
        public string unitName { get; set; }
        public bool priceInclVat { get; set; }

        public ServiceExp()
        {

        }

        public ServiceExp(Calani.BusinessObjects.Model.services s)
        {
            id = s.id;
            name = s.name;
            internalId = s.internalId;
            unitPrice = s.unitPrice;
            taxId = s.taxId;
            priceInclVat = s.price_including_vat;
            unit = (ServiceUnitEnum)s.unit;
            unitName = s.units != null ? (!string.IsNullOrWhiteSpace(s.units.stringResourceName) ? ResourceHelper.Get(s.units.stringResourceName) ?? s.units.stringResourceName : s.units.name) : null;
        }


    }

    public class CurrencyDescription
    {
        public string description { get; set; }
        public string symbol { get; set; }
        public string id { get; set; }
        public double rate { get; set; }
    }
}