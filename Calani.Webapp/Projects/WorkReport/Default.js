﻿var _periodFilterFrom = null;
var _periodFilterTo = null;
var _clientFilter = null;
var _statusFilter = 'Draft';

var _dataTable = null;

function setPeriodFilter() {
    _periodFilterFrom = null;
    _periodFilterTo = null;

    var pval = $('#ddlPeriod').val();
    if (pval != null && pval.indexOf(';') > 0) {
        pval = pval.split(';');
        if (pval.length == 2) {
            _periodFilterFrom = pval[0];
            _periodFilterTo = pval[1];

            if (_periodFilterFrom != "*")

                _periodFilterFrom = parseInt(_periodFilterFrom);

            else
                _periodFilterFrom = null;

            if (_periodFilterTo != "*")
                _periodFilterTo = parseInt(_periodFilterTo);

            else
                _periodFilterTo = null;
        }
    }

    refreshLines();
}
function refreshLines() {
    _dataTable.draw();
}
function refreshLiveStats() {



    var countDraft = 0;
    var countValidated = 0;
    


    var rowsId = _dataTable.rows().eq(0);
    for (var i = 0; i < rowsId.length; i++) {
        var row = _dataTable.row(rowsId[i]);
        var ci = row.node().attributes["data-clientid"].value;
        var imp = row.node().attributes["data-status"].value;

        if (imp == "Validated") {
            countValidated++;
        }
        else {
            countDraft++;
        }
    }

    var searchText = $('.dataTables_filter input').val();
    if (searchText) {
        countDraft = countItems('Draft');
        countValidated = countItems('Validated');
    }

    $('#ContentPlaceHolder1_lblCountReportDraft').html(countDraft);
    $('#ContentPlaceHolder1_lblCountReportValidated').html(countValidated);

}


function setClientFilter() {
    _clientFilter = $('#ddlClient').val();
    refreshLines();
}

function countItems(status) {
    var prevStatusFilter = _statusFilter;
    _statusFilter = status;
    var count = _dataTable.page.info().recordsDisplay;
    _statusFilter = prevStatusFilter;
    return count;
}

function filterItem(settings, data, dataIndex) {

    var visible = true;

    if (_clientFilter != null && _clientFilter != 0) {
        var clientMatch = false;
        if (_clientFilter == -1 && settings.aoData[dataIndex].nTr.getAttribute('data-clientid') == '')
            clientMatch = true;
        else if (_clientFilter == settings.aoData[dataIndex].nTr.getAttribute('data-clientid'))
            clientMatch = true;

        if (clientMatch == false)
            visible = false;
    }


    var lastUpdate = parseInt(settings.aoData[dataIndex].nTr.getAttribute('data-UpdatedAt'));

    if (_periodFilterFrom != null) {
        if (lastUpdate < _periodFilterFrom) visible = false;
    }
    if (_periodFilterTo != null) {
        if (lastUpdate >= _periodFilterTo) visible = false;
    }




    if (visible && _statusFilter != null) {

        var ci = settings.aoData[dataIndex].nTr.getAttribute("data-clientid");
        var status = settings.aoData[dataIndex].nTr.getAttribute("data-status");

        visible = false;

        if (_statusFilter == status) {
            visible = true;
        }

    }


    return visible;
}

$(function () {

    // client side filters
    $.fn.dataTable.ext.search.push(filterItem);
    // -->

    _dataTable = initDataTable($('#ContentPlaceHolder1_grid'), false, false, false, 100);
    _dataTable.search('');

    _dataTable.on('draw', function () {
        refreshLiveStats();
        if (_clientFilter && _clientFilter != 0) {
            $(":checkbox").prop("disabled", false);
        }
        else {
            $(":checkbox").prop("disabled", true);
        }

    });
    setPeriodFilter();
});


new Vue({

    el: '#app',

    data: {
        statusFilter: 'Draft'
    },
    methods: {
        setStatusFilter: function (e, f) {
            this.statusFilter = f;
            _statusFilter = f;
            _dataTable.draw();

           
            e && e.preventDefault();
        }
    }
});