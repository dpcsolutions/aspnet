﻿Vue.use(VueCkeditor.plugin, {
    editors: {
        'classic': CustomEditors.ClassicEditor
    }
})

var app = new Vue({
    el: '#app',
   
    vuetify: new Vuetify(),

    data: {
        id: pid,
        title: '',
        description: '',
        projectId: pid,
        projectInternalId: '',
        type: 0,
        status: 0,
       // editor: ClassicEditor,
        isEmployee: isEmployee,
        projects: projects,
        isSaved: false,
        savedText: changesSaved,
        timeout: 2000,
        dialog: false,
        rules: {
            required: v => !!v || fillAllRequiredFields,
            maxLength: v => (v && v.length <= 500) ||
                maxLengthTitle,
    
    
        },
        editorConfig: {
            removePlugins: ['MediaEmbed'],
            mediaEmbed: {},
            placeholder: '',
            toolbar: {
                shouldNotGroupWhenFull: true,
                items: [
                    'heading',
                    '|',
                    'bold',
                    'italic',
                    '|',
                    'bulletedList',
                    'numberedList',
                    '|',
                    'insertTable',
                    '|',
                    'undo',
                    'redo'
                ]
            },
            table: {
                contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells']
            },
        }

    },
    computed: {
    },
    mounted() {
    },
    created() {
        this.getModel();
    },
    methods: {
        validate:function() {
            this.$data.status = 1;
            this.save();
        },

        save: function () {
            var self = this;
            var obj = {
                Id: this.$data.id,
                Title: this.$data.title,
                Description: encodeURIComponent(this.$data.description),
                Type: this.$data.type,
                Status: this.$data.status,
                ProjectId: this.$data.projectId
            }
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '../../API/Service.svc/SaveWorkReport',
                data: JSON.stringify(obj),
                processData: false,
                dataType: "json",
                success: function (e) {
                    self.isSaved = true;
                    //window.location.href = './ReportDetails.aspx?pid=' + obj.ProjectId+'&rid=' + e;
                    window.history.replaceState({}, "", './ReportDetails.aspx?pid=' + obj.ProjectId + '&rid=' + e); 
                    self.id = e;
                },
                error: function (result) {
                    console.log(result);
                }
            });
        },
        del: function () {
            var self = this;
          
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '../../API/Service.svc/DeleteWorkReport',
                data: JSON.stringify(self.$data.id),
                //processData: false,
                dataType: "json",
                success: function (e) {
                    window.location.href = './Default.aspx';
                    self.modal = false;
                },
                error: function (result) {
                    self.modal = false;
                    console.log(result);
                }
            });

            
        },
        getModel: function() {
            var vm = this;
            $.ajax({
                url: '../../API/Service.svc/GetWorkReport',
                method: 'GET',
                data: {
                    pid: pid || 0,
                    rid: rid || 0
                },
                success: function (data) {
                    vm.id = data.Id;
                    vm.title = data.Title;
                    vm.description = decodeURIComponent(data.Description||'');
                    vm.projectInternalId = data.ProjectInternalId;
                    vm.type = data.Type;
                    vm.projectId = data.ProjectId;
                    vm.status = data.Status;
                },
                error: function (result) {
                    console.log(result);
                }
            });
        }
    }
});
