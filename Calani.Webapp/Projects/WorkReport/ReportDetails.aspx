﻿<%@ Page Title="TimeSheet" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="ReportDetails_Default" Codebehind="ReportDetails.aspx.cs" %>

<asp:content contentplaceholderid="head" runat="server">
 <link rel="stylesheet" href="<%= Page.ResolveClientUrl("~//assets/css/vuetify.min.css") %>" type="text/css" />
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/MaterialDesign-Webfont/5.1.45/css/materialdesignicons.min.css" type="text/css" />
</asp:content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
   
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
    <form runat="server">
        <div class="content"  id="app" >
            <v-app id="inspire">
                 <v-container >
                     <v-subheader>
                         <v-row>
                             <v-col>
                                 <a name="title"><i class="icon-grid5 display-inline-block"></i>
                                     <strong><%= Resources.Resource.Title %></strong></a>
                             </v-col>
                             <v-col class="pull-right text-right">
                                 <v-icon large class="pull-right text-right" v-bind:class="{  'icon-checkmark-circle2 text-success': status == 1, 'icon-close2 text-danger': status != 1 }" ></v-icon>
                             </v-col>
                         </v-row>
                         
                     </v-subheader>
                     
                     <v-row>
                         <v-col cols="12" lg="9" sm="9" md="9">
                             <v-text-field v-model="title" dense label="<%= Resources.Resource.Title %>" validate-on-blur="true"  :rules="[rules.required, rules.maxLength]" :disabled="status == 1"></v-text-field>
                         </v-col>
                         <v-col cols="12" lg="3" sm="3" md="3">
                             <v-autocomplete
                                 v-model="projectId"
                                 :items="projects"
                                 item-text="text"
                                 item-value="value"
                                 :rules="[rules.required]"
                                 :disabled="status == 1"
                                 dense
                                 label="<%= Resources.Resource.Project %>"
                             ></v-autocomplete>
                         </v-col>
                     </v-row>
                     
                
                     <v-subheader>
                         <a name="quotes">
                             <i class="icon-stack-picture display-inline-block "></i>
                             <strong><%= Resources.Resource.Details %></strong>
                         </a>
                     </v-subheader>
                    
                     <v-sheet elevation="3" v-show="status != 1">
                        <%--<ckeditor class="p-10" :editor="editor" v-model="description" :config="editorConfig"  ></ckeditor>--%>
                         <vue-ckeditor type="classic" v-model="description" :config="editorConfig"></vue-ckeditor>
                    </v-sheet>

                     <v-sheet elevation="3" v-show="status == 1" :visible="status == 1">
                         <div class="p-10" style="min-height: 300px" v-html="description" > </div>
                     </v-sheet>

                     <v-divider></v-divider>
                     <v-row align="center" justify="space-around">
                         <v-btn depressed v-show="!isEmployee" outlined color="brown" href='./Default.aspx'>
                             <%= Resources.Resource.BackToList %>
                         </v-btn>

                         <v-btn depressed outlined color="blue-grey"  :href="'../Opens/Job.aspx?id=' + projectId" :disabled="!projectId">
                             <%= Resources.Resource.BackToJob %>
                         </v-btn>

                         <v-btn depressed outlined @click="validate" color="success" :disabled="isEmployee || !title || status == 1 || !projectId">
                             <%= Resources.Resource.Validate %>
                         </v-btn>
                         
                         <v-btn depressed outlined @click="save" color="primary" :disabled="!title || status == 1 || !projectId">
                             <%= Resources.Resource.Save %>
                         </v-btn>
                         <v-btn  outlined dark color="red" @click="dialog = true" :disabled="isEmployee || !projectId">
                             <%= Resources.Resource.Delete %>
                         </v-btn>
                     </v-row>
                     <v-row justify="center">
                         
                         <v-dialog v-model="dialog" max-width="500">
                             <v-card>
                                 <v-card-title
                                     class="headline grey lighten-2"
                                     primary-title
                                 >
                                     <%= Resources.Resource.Confirmation %>
                                 </v-card-title>

                                 <v-card-text>
                                     <br/>
                                     <%= Resources.Resource.Delete_Is_Irreversible %>
                                 </v-card-text>

                                 <v-divider></v-divider>

                                 <v-card-actions>
                                     <v-spacer></v-spacer>
                                     <v-btn color="red"  v-on:click="del" >
                                         <%= Resources.Resource.Delete %>
                                     </v-btn>
                                     <v-btn outlined color="  grey darken-1" text @click="dialog = false">
                                         <%= Resources.Resource.Cancel %>
                                 </v-btn>
                                 </v-card-actions>
                                 
                             </v-card>
                         </v-dialog>

                          
                     </v-row>
                     
                     <v-snackbar v-model="isSaved" :timeout="timeout">
                         {{ savedText }}
                     </v-snackbar>
                </v-container> 
               
            </v-app>
        </div>
        <!-- /content -->
    </form> 
    <script type="text/javascript">
        var fillAllRequiredFields = "<%= Resources.Resource.FillAllRequiredFields %>";
        var maxLengthTitle = "<%= Resources.Resource.MaxLengthTitle500 %>";
        var changesSaved = "<%= Resources.Resource.ChangesSaved %>";
        var rid = <%= CurrentId %>;
        var pid = <%= ProjectId %>;
        var isEmployee = <%= IsEmployee.ToString().ToLower() %>;
        var projects =  <%= ProjecectsDataSource %>;

    </script>
    <script type="text/javascript" src="ReportDetails.js?<%= Guid.NewGuid().ToString("n") %>"></script>
    <%--<script type="text/javascript" src="ReportDetails.js"></script>--%>
</asp:Content>
