﻿using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Projects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

public partial class Projects_Reports_Default : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    public string OptionsClients { get; set; }
    public string OptionsPeriod { get; set; }

    ProjectsManager mgr;

    protected void Page_Load(object sender, EventArgs e)
    {
        var orgMgr = new OrganizationManager(ph.CurrentOrganizationId);
        orgMgr.Load();

        DateTime yearBegin = new DateTime(DateTime.Now.Year, 1, 1);
        DateTime yearEnd = new DateTime(DateTime.Now.Year, 12, 31);
        
        OptionsPeriod = ph.RenderPeriodsListOption(yearBegin.Ticks + ";*", preselect: orgMgr.DefaultPeriodFilter);


        
        var wMgr = new WorkReportsManager(ph.CurrentOrganizationId);

        var list = wMgr.GeList(WorkReportStatus.All, orgMgr.CompanyName).OrderByDescending(r => r.CreatedAt).ToList();

        List<long> clients = list.Where(i => i.ClientId.HasValue).Select(i => i.ClientId.Value).Distinct().ToList();

        OptionsClients = ph.RenderCustomerListOption(true, clients);

        grid.DataSource = list;

       List<string> rowData = new List<string>() { "ClientId", "UpdatedAt", "Status"};
        new Html5GridView(grid, false, rowData, null, addDateOrder: false);

       
        // -->

    }

}
