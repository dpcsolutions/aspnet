﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Newtonsoft.Json;
using Calani.BusinessObjects.Admin;
using Calani.BusinessObjects.CustomerAdmin;
using Newtonsoft.Json.Serialization;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Projects;

public partial class ReportDetails_Default : System.Web.UI.Page
{
    public PageHelper ph;
    public long CurrentId { get; set; }
    public long ProjectId { get; set; }
    public bool IsEmployee { get; set; }
    public jobsExtended Project { get; set; }

    public string ProjecectsDataSource { get; set; }

    private static ProjectsManager pMngr;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    
protected void Page_Load(object sender, EventArgs e)
    {
        long rid = -1, pid = -1;
        long.TryParse(Request.Params["rid"], out rid);
        if (rid == -1)
        {
            Response.Redirect(".");
            return;
        }
        long.TryParse(Request.Params["pid"], out pid);
        if (pid == -1)
        {
            Response.Redirect(".");
            return;
        }

        pMngr = new ProjectsManager(ph.CurrentOrganizationId);


        var orgMgr = new OrganizationManager(ph.CurrentOrganizationId);
        orgMgr.Load();

        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee && !orgMgr.EmployeeCanViewAllOpenProjects)
        {
            pMngr.EmployeeFilter = ph.CurrentUserId;
        }

        IsEmployee = ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee;
        CurrentId = rid;
        ProjectId = pid;

        var list = pMngr.ListOpens().Select(o=> new {value= o.id, text= o.internalId}).ToList();

        ProjecectsDataSource = JsonConvert.SerializeObject(list);


    }

}