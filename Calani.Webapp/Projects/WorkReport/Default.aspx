﻿<%@ Page Title="Invoices" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Projects_Reports_Default" Codebehind="Default.aspx.cs" %>
<%@ Register src="../../assets/Controls/ProjectStatusLabel.ascx" tagname="ProjectStatusLabel" tagprefix="psl" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">


        <div class="content" id="app">

                <div class="row">

					<div class="col-sm-6 col-md-6">

							<!-- User card with thumb and social icons at the bottom -->
							<div class="panel">
									<div class="panel-body">
										<div class="row text-center" >
											
											<div class="col-lg-6"  id="divReportDraft" v-bind:class="{ 'bordered alpha-primary': statusFilter=='Draft' }">
												<p><i class="icon-close2 icon-2x display-inline-block text-danger" id="icoReportDraft" runat="server"></i></p>
												<h5 class="text-semibold no-margin" id="lblCountReportDraft" runat="server">.</h5>
												<strong class="text-muted text-size-small" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.Draft %>">
													<a href="" v-on:click="setStatusFilter($event, 'Draft')" class="text-grey"><%= Resources.Resource.Draft %></a></strong>
											</div>

											<div class="col-lg-6"  id="divReportValidated" v-bind:class="{ 'bordered alpha-primary': statusFilter=='Validated' }">
												<p><i class="icon-checkmark-circle2 icon-2x display-inline-block text-green" id="icoReportValidated" runat="server"></i></p>
												<h5 class="text-semibold no-margin" id="lblCountReportValidated" runat="server">.</h5>
												<strong class="text-muted text-size-small" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.WorkReportsValidated_Tooltip %>">
                                                    <a href="" v-on:click="setStatusFilter($event, 'Validated')" class="text-green"  ><%= Resources.Resource.Validated %></a>
                                                </strong>
											</div>
											
										</div>									
									</div>

							    	<div class="panel-footer text-center no-padding">
								    	<div class="row" >
								    		<a href=""  v-on:click="setStatusFilter($event, null)"  class="display-block p-10 text-default" 
								    			data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.ListAllRecords %>">
								    				<i class="fa fa-ellipsis-h"></i> <%= Resources.Resource.Show_all %>
								    				
								    			</a>
								    	</div>
									</div>
								</div>
								<!-- /user card with thumb and social icons at the bottom -->

					</div>

                    <div class="col-sm-6 col-md-6">
								<div class="panel panel-body panel-body-accent">
									<div class="media no-margin">
										
										<div>
											<i class="fa fa-calendar-check-o" aria-hidden="true"></i> <%= Resources.Resource.Select_Period %>
										</div>
										<div>
											<select id="ddlPeriod" class="select" onchange="setPeriodFilter()">
												<%= OptionsPeriod %>
											</select>
										</div>

										<div style="margin-top:20px">
											<i class="fa fa-user-circle-o" aria-hidden="true"></i> <%= Resources.Resource.Customers %>
										</div>
										<div>
											<select id="ddlClient" data-placeholder="<%= Resources.Resource.Select___ %>" class="select" onchange="setClientFilter()">
                                                <%= OptionsClients %>
											</select>
										</div>
									</div>
								</div>
							</div>

                </div>

					<div class="row">
                        <div class="col-sm-12 col-md-12">
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="icon-upload display-inline-block text-green"></i>
										<strong><%= ph.CMP.Name %></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

							<div class="panel-body">
                                <asp:GridView runat="server" ID="grid" AutoGenerateColumns="False" EnableViewState="false">
                                    <Columns>
										<asp:TemplateField ShowHeader="False" >

											<ItemTemplate>
                                                <ItemTemplate>
                                                    <span><i class='<%# (int)Eval("Status") == 1 ? "icon-checkmark-circle2 text-success" : "icon-close2 text-danger" %>'></i></span>
                                                </ItemTemplate>

											</ItemTemplate>
										</asp:TemplateField>
                                        <asp:BoundField DataField="Id" HeaderText="Id" ItemStyle-CssClass="text-right hidden" HeaderStyle-CssClass="text-right hidden"  />
                                        <asp:BoundField DataField="ProjectId" HeaderText="ProjectId" ItemStyle-CssClass="text-right hidden" HeaderStyle-CssClass="text-right hidden"  />
                                        <asp:HyperLinkField DataTextField="Title" HeaderText="Title" ItemStyle-CssClass="text-nowrap" DataNavigateUrlFields="ProjectId,Id" DataNavigateUrlFormatString="~/Projects/WorkReport/ReportDetails.aspx?pid={0}&rid={1}"/>
										<asp:HyperLinkField DataTextField="ProjectInternalId" HeaderText="Project" ItemStyle-CssClass="text-nowrap" DataNavigateUrlFields="ProjectId" DataNavigateUrlFormatString="~/Projects/Opens/Job.aspx?id={0}"/>

                                        <asp:BoundField DataField="CreatedAt" HeaderText="CreatedAt" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right"  />
                                        <asp:BoundField DataField="CreatedBy.FullName" HeaderText="CreatedBy" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right"  />

                                        <asp:BoundField DataField="ValidatedAt" HeaderText="ValidatedAt" DataFormatString="{0:dd/MM/yyyy}" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right"  />
                                        <asp:BoundField DataField="ValidatedBy.FullName" HeaderText="ValidatedBy" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right"  />
										
                                        
                                        
										
										
                                       
                                    </Columns>
                                </asp:GridView>

                                
                            </div><!-- panel-body -->
							
							
                            <div class="panel-footer text-right no-padding" runat="server" id="panelAdd">
                                <div class="row" style="margin:10px">
                                    <a href="ReportDetails.aspx" class="btn btn-default"><i class="icon-plus2 position-left"></i> 
                                        <%= Resources.Resource.Add_new %>

                                    </a>
                                </div>
                            </div><!-- panel-footer -->
                        </div>


                    </div>
               </div>
               
              
        </div><!-- /content -->

    </form>
    <script type="text/javascript">
        var ressources = [];
        ressources['Late'] = "<%= Resources.Resource.Late %>";
        ressources['Due'] = "<%= Resources.Resource.Due %>";
        ressources['Paid'] = "<%= Resources.Resource.Paid %>";
        ressources['Invoiced'] = "<%= Resources.Resource.Invoiced %>";

        ressources['LateExcTax'] = "<%= Resources.Resource.LateExcTax %>";
        ressources['DueExcTax'] = "<%= Resources.Resource.DueExcTax %>";
		ressources['PaidExcTax'] = "<%= Resources.Resource.PaidExcTax %>";
        ressources['Draft'] = "<%= Resources.Resource.Draft %>";
        ressources['InvoicedExcTax'] = "<%= Resources.Resource.InvoicedExcTax %>";
        var currency = '<%= ph.Currency %>';

    </script>
    <script type="text/javascript" src="Default.js?<%= Guid.NewGuid().ToString("n") %>"></script>
	<%--<script type="text/javascript" src="Default.js"></script>--%>
</asp:Content>

