﻿<%@ WebHandler Language="C#" Class="GetExcelProject" %>

using System;
using System.Globalization;
using System.Web;
using Calani.BusinessObjects.Projects;

public class GetExcelProject : IHttpHandler, System.Web.SessionState.IRequiresSessionState {

    public void ProcessRequest (HttpContext context) { 
        var projectId = context.Request.Params["projectId"] != null ? long.Parse(context.Request.Params["projectId"]) : 0;
        var culture = context.Request.Params["c"];
        var localizer = Calani.BusinessObjects.SharedResource.Resource;
        
        var org = PageHelper.GetCurrentOrganizationId(context.Session);
        var extractor = new ProjectsExtractor(org);
        var data = extractor.Extract(projectId, culture);

        context.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        context.Response.AddHeader("content-disposition", "Attachment; filename=GesMobile-" 
            + localizer.GetString("Job", CultureInfo.GetCultureInfo(culture)) + "-"
            + DateTime.Now.ToString("dd-MM-yyyy_HH-mm", CultureInfo.InvariantCulture) + ".xlsx");
       
        context.Response.OutputStream.Write(data, 0, data.Length);
    }

    public bool IsReusable {
        get {
            return false;
        }
    }

}