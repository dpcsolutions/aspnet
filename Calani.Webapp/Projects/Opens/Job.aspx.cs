﻿using Calani.BusinessObjects.Projects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Projects_Opens_Job : System.Web.UI.Page
{
    public PageHelper ph;

    public string ClassVisitListTab { get; set; }
    public string ClassVisitCalendarTab { get; set; }
    public bool MissingChild { get; set; }


    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    #region calendar
    public string locale { get; set; }
    public Array standardBusinessHours { get; set; }
    public String defaultDate { get; set; }
    public string resources { get; set; }
    public string resourcesForJob { get; set; }
    public string clients { get; set; }
    public string localisedDateFormat { get; set; }
    public string internalServices { get; set; }
    public string currentLanguage { get; set; }
    public string preselectedJobId { get; set; }
    public string preselectedClientId { get; set; }
    public string officialHolidays { get; set; }
    public string EmployeeCanViewAllEvents { get; set; }

    #endregion 

    protected void Page_Load(object sender, EventArgs e)
    {
        JobEditor1.IsAdmin = ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin;
    }

    private void loadCalendar(long jobId, long clientId)
    {
        var colorFromTheme = new String[] { "#AD1457", "#6A1B9A", "#4527A0", "#283593", "#0277BD", "#00695C", "#558B2F", "#EF6C00", "#4E342E", "#444444", "#37474F" };

        var orgMgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(ph.CurrentOrganizationId);
        orgMgr.Load();

        EmployeeCanViewAllEvents = orgMgr.EmployeeCanViewAllEvents.ToString().ToLowerInvariant();

        var jobMgr = new Calani.BusinessObjects.Projects.JobsManager(ph.CurrentOrganizationId);
        var employeeMgr = new Calani.BusinessObjects.Contacts.EmployeesManager(ph.CurrentOrganizationId);

        defaultDate = string.Concat(DateTime.UtcNow.ToString("s"), "Z");
        currentLanguage = "fr";
        localisedDateFormat = ph.localisedDateFormat;
        
        //retrieve all resources for organisation
        var tmpList = employeeMgr.ListActiveUsers().Select(r => new resourcesExtended(r)).ToList<resourcesExtended>();
        for (var i = 0; i < tmpList.Count; i++)
            tmpList[i].color = colorFromTheme[i % 10];

        resources = new JavaScriptSerializer().Serialize(tmpList);

        //retrieve all resources for given job
     
        var tmpListRes = jobMgr.getResourcesForJob(jobId);
        List<resourcesExtended> resForJob = new List<resourcesExtended>();

        foreach (var r in tmpListRes)
        {
            var strId = r.ToString();
            var res = (from a in tmpList
                       where a.id == strId
                       select a).FirstOrDefault();

            if (res != null)
                resForJob.Add(res);
        }

        resourcesForJob = new JavaScriptSerializer().Serialize(resForJob);

        //handling public holidays
        var publicHolidays = jobMgr.ListVisits(null, null, null, null, (long?)VisitTypeEnum.PublicHolidays);
        foreach (var p in publicHolidays)
        {
            p.isHolidays = false;
            p.isOfficialHolidays = true;
        }

        officialHolidays = new JavaScriptSerializer().Serialize(publicHolidays);

        preselectedClientId = clientId.ToString();
        preselectedJobId = jobId.ToString();

        //list all internal jobs
        Calani.BusinessObjects.CustomerAdmin.InternalJobsManager internalJobsMgr = new Calani.BusinessObjects.CustomerAdmin.InternalJobsManager(ph.CurrentOrganizationId);
        internalServices = new JavaScriptSerializer().Serialize(internalJobsMgr.List().Select(r => new Calani.BusinessObjects.AjaxService.SimpleItem() { id = r.id.ToString(), text = r.name }).ToList<Calani.BusinessObjects.AjaxService.SimpleItem>());



        // toggle the better tab
        var visits = jobMgr.ListVisits(null, null, null, jobId, null).Count;
        int visitCounts = visits;
        if(visitCounts == 0)
        {
            ClassVisitListTab = "";
            ClassVisitCalendarTab = "active";
        }
        else
        {

            ClassVisitCalendarTab = "";
            ClassVisitListTab = "active";
        }
    }

    public void Load()
    {
        Calani.BusinessObjects.Projects.jobsExtended info = JobEditor1.Info;

        if(info.ProjectStatus == ProjectStatus.JobFinished)
        {
            lblStatusChange.InnerHtml = "<i class='icon-finish'></i> " + Resources.Resource.Finished;
        }
        else if (info.ProjectStatus == ProjectStatus.JobOnHold)
        {
            lblStatusChange.InnerHtml = "<i class='icon-pause2'></i> " + Resources.Resource.On_hold;
        }
        else if (info.ProjectStatus == ProjectStatus.JobOutDated)
        {
            lblStatusChange.InnerHtml = "<i class='icon-calendar2'></i> " + Resources.Resource.OutdatedJob;
        }
        else
        {
            lblStatusChange.InnerHtml = "<i class='icon-play4'></i> " + Resources.Resource.Opened;
        }

        loadCalendar(info.id, info.clientId);

        if(info.id > 0)
        {
            PanelCalendarAvailable.Visible = true;
            PanelCalendarNotAvailable.Visible = false;
        }
        else
        {
            PanelCalendarAvailable.Visible = false;
            PanelCalendarNotAvailable.Visible = true;
        }

        MissingChild = info.dateJobDone != null && !info.importedIn.HasValue;
        cbxDontNotifyOnMissingInvoice.Checked = info.skipMissingInvoiceNotif;
    }

}