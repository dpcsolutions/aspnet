﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.Costs;

public partial class Projects_Opens_ImportExpenses : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        long id = -1;
        if (Request.Params["id"] != null)
        {
            if (!long.TryParse(Request.Params["id"], out id))
            {
                this.Visible = false;
                return;
            }
        }

        ExpenseManager mgr = new ExpenseManager(PageHelper.GetCurrentOrganizationId(Session));
        var records = mgr.GetProjectRecords(id);

        
        double sum = (from r in records
                      where r.imported == false
                      && r.approvedmount != null
                      select r.approvedmount.Value).Sum();
        sum += (from r in records
                where r.imported == false
                && r.approvedmount == null
                && r.amount != null
                select r.amount.Value).Sum();

        if (sum == 0)
        {
            this.Visible = false;
            return;
        }

        lblMissingExpSum.InnerText = string.Format("{0:0,0.00}", sum) ;
        lblMissingExpSum.InnerText += " ";
        PageHelper ph = new PageHelper(this.Page);
        lblMissingExpSum.InnerText += ph.Currency;

        var items = (from r in records
                     
                     select new ExpenseExtendedToImport(r)).ToList();

        ExpImportGrid.DataSource = items;
        ExpImportGrid.DataBind();
        new Html5GridView(ExpImportGrid, false, new List<string> { "Imported" });

    }


    public class ExpenseExtendedToImport
    {
        public long Id { get; set; }
        public bool Imported { get; set; }
        public bool CanBeImported { get; set; }

        public bool ExpenseInProgress { get; set; }
        public bool ExpenseValidated { get; set; }

        public string Link { get; set; }

        public double Price { get; set; }

        public string Description { get; set; }

        public DateTime Date { get; set; }

        public ExpenseExtendedToImport(Calani.BusinessObjects.Model.expenses e)
        {
            Id = e.id;
            Link = "../../Costs/Expenses/Edit.aspx?id=" + e.id;

            Imported = e.imported;
            CanBeImported = !Imported;

            ExpenseInProgress = e.approvedmount == null;
            ExpenseValidated = e.approvedmount != null;

            Price = e.amount.Value;
            Description = e.description;

            if(e.date != null) Date = e.date.Value;
        }
    }
}