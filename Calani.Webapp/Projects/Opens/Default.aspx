﻿<%@ Page Title="Open jobs" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Projects_Opens_Default" Codebehind="Default.aspx.cs" %>
<%@ Register src="../../assets/Controls/ProjectStatusLabel.ascx" tagname="ProjectStatusLabel" tagprefix="psl" %>
<%@ Register src="../../assets/Controls/ProjectVisitsLabel.ascx" tagname="ProjectVisitsLabel" tagprefix="pvl" %>
<%@ Register src="../../assets/Controls/ProjectNextVisitLabel.ascx" tagname="ProjectNextVisitLabel" tagprefix="pnvl" %>
<%@ Register src="../../assets/Controls/RemainingVisitsLabel.ascx" tagname="RemainingVisitsLabel" tagprefix="rvl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
    <div class="btn-group" runat="server" id="quickmenu">
		<button type="button" class="btn bg-gesmobile btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
		</button>

		<ul class="dropdown-menu dropdown-menu-right">
			<li><a href="Job.aspx"><i class="icon-plus2"></i> <%= Resources.Resource.Create %> <%= Resources.Resource.Job %></a></li>
		</ul>
	</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">


        <div class="content" id="app">

                    <div class="row">


						<div class="col-sm-4 col-md-4">
                            <!-- User card with thumb and social icons at the bottom -->
							<div class="panel">
									<div class="panel-body">
										<div class="row text-center">
											
											<div class="col-xs-2" style="width: 20%" v-bind:class="{ 'bordered alpha-primary': statusFilter=='JobOpened' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.Open_Jobs_Tooltip %>">
                                                <a href="" v-on:click="setStatusFilter($event, 'JobOpened')">
                                                <p><i class="icon-play4 icon-2x display-inline-block text-primary tada infinite" id="icoCountJobOpened" runat="server"></i></p>
												<h5 class="text-semibold no-margin" id="lblCountJobOpened" runat="server">.</h5>
                                                
                                                <p><strong class="text-muted text-size-small" ><%= Resources.Resource.Open_Jobs_many %></strong></p>
                                                </a>

											</div>

											<div class="col-xs-2" style="width:20%" v-bind:class="{ 'bordered alpha-primary': statusFilter=='JobOnHold' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.On_hold_Tooltip %>">
                                                <a href="" v-on:click="setStatusFilter($event, 'JobOnHold')" class="text-orange" >
                                                    <p><i class="icon-pause2 icon-2x display-inline-block text-orange tada infinite" id="icoCountJobOnHold" runat="server"></i></p>
												<h5 class="text-semibold no-margin" id="lblCountJobOnHold" runat="server">.</h5>
                                                
                                                <p><strong class="text-muted text-size-small" ><%= Resources.Resource.On_hold_many %></strong></p>
                                                </a>
											</div>

											<div class="col-xs-2" style="width:20%"  v-bind:class="{ 'bordered alpha-primary': statusFilter=='JobOutDated' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.OutdatedJob_Tooltip %>">
                                                <a href="" v-on:click="setStatusFilter($event, 'JobOutDated')"  class="text-muted">
                                                    <p><i class="icon-calendar2 icon-2x display-inline-block text-grey tada infinite"  id="icoCountJobOutDated" runat="server"></i></p>
												<h5 class="text-semibold no-margin text-muted" id="lblCountJobOutDated" runat="server">.</h5>
												
                                                <p><strong class="text-muted text-size-small" ><%= Resources.Resource.Outdated %></strong></p>
                                                </a>
											</div>

                                            <div class="col-xs-2"  style="width:20%" v-bind:class="{ 'bordered alpha-primary': statusFilter=='JobFinished' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.Finished_Tooltip %>">
                                                <a href="" v-on:click="setStatusFilter($event, 'JobFinished')" class="text-success" >
                                                    <p><i class="icon-finish icon-2x display-inline-block text-success tada infinite"  id="icoCountJobFinished" runat="server"></i></p>
												<h5 class="text-semibold no-margin" id="lblCountJobFinished" runat="server">.</h5>
                                                <p><strong class="text-muted text-size-small" ><%= Resources.Resource.Finished_many %></strong></p>
                                                </a>
											</div>
                                            <div class="col-xs-2"  style="width:20%" v-bind:class="{ 'bordered alpha-primary': statusFilter=='JobMissingChild' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.AvailableForImportOfInvoice %>">
                                                <a href="" v-on:click="setStatusFilter($event, 'JobMissingChild')" class="text-grey" >
                                                    <p><i class="icon-notification2 icon-2x display-inline-block tada infinite text-grey" id="icoCountMissingChild" runat="server"></i></p>
                                                <h5 class="text-semibold no-margin" id="lblCountMissingChild" runat="server">.</h5>
                                                <p><strong class="text-muted text-size-small" ><%= Resources.Resource.Warning %></strong></p>
                                                    </a>
                                            </div>

                                        
										</div>									
									</div>

							    	<div class="panel-footer text-center no-padding">
								    	<div class="row">
								    		<a href="" v-on:click="setStatusFilter($event, null)" class="display-block p-10 text-default" 
								    			data-popup="tooltip"  data-container="body" title="List all quotes">
								    				<i class="fa fa-ellipsis-h"></i> <%= Resources.Resource.Show_all %>
								    				
								    			</a>
								    	</div>
									</div>
								</div>
								<!-- /user card with thumb and social icons at the bottom -->

						</div>
                        
                        
                        <div class="col-sm-3 col-md-3" runat="server" ID="panelTurnover">
                            <div class="panel panel-body">
                                    <div>
                                        <div class="media-left media-middle">
                                            <i class="icon-coins icon-3x text-primary"></i>
                                        </div>

                                        <div class="media-body text-right" data-popup="tooltip" title="<%= Resources.Resource.OpenedAndOutdatedHorsTaxes %>">
                                            <h3 class="no-margin text-semibold" runat="server" id="lblOened">0</h3>
                                            <span class="text-uppercase text-size-mini text-muted"><%= Resources.Resource.OpenedAndOutdatedHorsTaxes %></span>
                                        </div>

                                    </div>
                            </div>
                        </div>
							

						<div class="col-sm-2 col-md-2">
                            <div class="panel panel-white" runat="server" id="panelNextVisit">
                                <div class="panel-heading text-center">
                                    <i class="icon-calendar display-inline-block text-green"></i>
                                    <strong><%= Resources.Resource.Next_Visit %></strong>
                                </div>
                                <div class="panel-body text-center">
                                    <div><strong><%= Resources.Resource.Customer %>:</strong> <asp:HyperLink runat="server" ID="lnkNextVisitCustomer" /></div>
                                    <div><strong><%= Resources.Resource.Opened_Job %>:</strong> <asp:HyperLink runat="server" ID="lnkNextVisitProject" /></div>
                                    <div><strong><%= Resources.Resource.Date %>:</strong> <asp:Label runat="server" ID="lblNextVisitDate" /></div>
                                    <div><strong><%= Resources.Resource.Employees %>:</strong> <span runat="server" ID="spanNextVisitResources" /></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-3 col-md-3">
                            <div class="panel panel-body panel-body-accent">
                                <div class="media no-margin">
										
                                    <div>
                                        <i class="fa fa-calendar-check-o" aria-hidden="true"></i> <%= Resources.Resource.Select_Period %>
                                    </div>
                                    <div>
                                        <select id="ddlPeriod" class="select" onchange="setPeriodFilter()">
                                            <%= OptionsPeriod %>
                                        </select>
                                    </div>

                                    <div style="margin-top:20px">
                                        <i class="fa fa-user-circle-o" aria-hidden="true"></i> <%= Resources.Resource.Customers %>
                                    </div>
                                    <div>
                                        <select id="ddlClient" data-placeholder="Select..." multiple="multiple" class="select" onchange="setClientFilter()">
                                            <%= OptionsClients %>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
							
					</div>

					<div class="row">
                        <div class="col-sm-12 col-md-12">
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="icon-upload display-inline-block text-green"></i>
										<strong><%= ph.CMP.Name %></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

							<div class="panel-body">
                                <asp:GridView runat="server" ID="grid" AutoGenerateColumns="False" EnableViewState="false">
                                    <Columns>
                                        <asp:HyperLinkField DataTextField="internalId" HeaderText="Number" ItemStyle-CssClass="text-nowrap p-5" DataNavigateUrlFields="id" DataNavigateUrlFormatString="Job.aspx?id={0}"/>
                                        <asp:BoundField DataField="clientName" HeaderText="Customer" ItemStyle-CssClass=" p-5" />
                                        <asp:BoundField DataField="description" HeaderText="Description" ItemStyle-CssClass=" p-5" />
                                        
                                        <asp:TemplateField HeaderText="Visits" HeaderStyle-CssClass="text-center " ItemStyle-CssClass="text-center full-td p-5">
                                            <ItemTemplate>
                                                <pvl:ProjectVisitsLabel ID="pvl" runat="server" DoneVisits='<%# Bind("DoneVisits") %>' PlannedVisits='<%# Bind("plannedVisits") %>' ContractualUnitsCount='<%# Bind("contractualUnitsCount") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Next_Visit"  HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center full-td  p-5">
                                            <ItemTemplate>
                                                <pnvl:ProjectNextVisitLabel ID="pnvl" runat="server" NextVisitDate='<%# Bind("nextVisitDate") %>' NextVisitContacts='<%# Bind("nextVisitContacts") %>' NextVisitMissingRessource='<%# Bind("NextVisitMissingRessource") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remaining_Visits"  HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center  p-5">
                                            <ItemTemplate>
                                                <rvl:RemainingVisitsLabel ID="pnvl" runat="server" RemainingVisits='<%# Bind("remainingVisits") %>'  />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Totals.TotalHT" HeaderText="AmountTotalHT" ItemStyle-CssClass="text-right text-nowrap  p-5"  DataFormatString="{0:0,0.00}" />
                                        <asp:BoundField DataField="Totals.StillToInvoice" HeaderText="StillToInvoice" ItemStyle-CssClass="text-right text-nowrap  p-5"  DataFormatString="{0:0,0.00}" />
                                        <asp:TemplateField HeaderText="Status" ItemStyle-CssClass="p-5">
                                            <ItemTemplate>
                                                <psl:ProjectStatusLabel ID="psl" runat="server" ProjectStatus='<%# Bind("ProjectStatus") %>' MissingChild='<%# Bind("MissingChild") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="DateJobValidity" HeaderText="ProjectDeadline"  ItemStyle-CssClass="p-5" DataFormatString="{0:dd.MM.yyyy}"/>

                                    </Columns>
                                </asp:GridView>

                                
                            </div><!-- panel-body -->

                            <div class="panel-footer text-right no-padding" runat="server" id="panelAdd">
						    	<div class="row" style="margin:10px">
						    		<a href="Job.aspx" class="btn btn-default"><i class="icon-plus2 position-left"></i> 
                                        <%= Resources.Resource.Create_Job %>

						    		</a>
						    	</div>
							</div><!-- panel-footer -->

                        </div>

                        </div>
                    </div>
        </div><!-- /content -->

    </form>
	
    <script type="text/javascript">
        var ressources = [];
        ressources['Opened'] = "<%= Resources.Resource.Opened %>";

        ressources['OpenedExcTax'] = "<%= Resources.Resource.OpenedExcTax %>";
        var currency = '<%= ph.Currency %>';

    </script>
    <script type="text/javascript" src="Default.js?<%= Guid.NewGuid().ToString("n") %>"></script>
    <%--<script type="text/javascript" src="Default.js"></script>--%>
</asp:Content>

