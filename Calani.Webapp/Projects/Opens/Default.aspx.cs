﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Projects;

public partial class Projects_Opens_Default : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    public string OptionsClients { get; set; }
    public string OptionsPeriod { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        ph = new PageHelper(this);

        var mgr = new ProjectsManager(ph.CurrentOrganizationId);
        
        var orgMgr = new OrganizationManager(ph.CurrentOrganizationId);
        orgMgr.Load();

        OptionsClients = ph.RenderCustomerListOption();
        OptionsPeriod = ph.RenderPeriodsListOption(preselect: orgMgr.DefaultPeriodFilter);

        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            panelTurnover.Visible = false;

            if (!orgMgr.EmployeeCanViewAllOpenProjects)
                mgr.EmployeeFilter = ph.CurrentUserId;
        }
        var projects = mgr.ListOpens();
        foreach (var item in projects)
        {
            if (String.IsNullOrWhiteSpace(item.internalId))
                item.internalId = Resources.Resource.Open;

            if (!item.dateJobValidity.HasValue)
            {
                item.dateJobValidity = DateTime.UtcNow;
            }

            if (!String.IsNullOrWhiteSpace(item.description))
            {
                item.description = item.description.Replace("\r", " ");
                item.description = item.description.Replace("\n", " ");
                item.description = item.description.Replace("  ", " ");
                if (item.description.Length > 144) item.description = item.description.Substring(0, 144) + "...";
            } 
        }
        grid.DataSource = projects;
        
        List<string> rowData = new List<string>() { "totalToInvoice", "clientId", "ProjectStatus", "lastUpdate", "rating", "total", "totalWithoutTaxes", "MissingChild", "dateJobValidity", "dateCreated", "id" };
        new Html5GridView(grid, false, rowData, "7;desc");


        var nextVisitProject = (from r in projects where r.nextVisitDate != null && r.nextVisitDate>= DateTime.UtcNow.Date orderby r.nextVisitDate ascending select r).FirstOrDefault();
        BindNextVisitProject(nextVisitProject);

        // remove rights for employee
        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            quickmenu.Visible = false;
            panelAdd.Visible = orgMgr.EmployeeCanCreateProjects;
        }
        // -->

    }

    private void BindNextVisitProject(jobsExtended nextVisitProject)
    {
        if(nextVisitProject != null)
        {
            panelNextVisit.Visible = true;
            lnkNextVisitCustomer.Text = nextVisitProject.clientName;
            lnkNextVisitCustomer.NavigateUrl = "~/Customers/List/Details.aspx?id=" + nextVisitProject.clientId;
            lnkNextVisitProject.Text = nextVisitProject.internalId;
            lnkNextVisitProject.NavigateUrl = "Job.aspx?id=" + nextVisitProject.id;
            lblNextVisitDate.Text = nextVisitProject.nextVisitDate.Value.ToString("dd/MM/yyyy - HH:mm");
            spanNextVisitResources.InnerHtml = "";
            foreach (var c in nextVisitProject.nextVisitContacts)
            {
                spanNextVisitResources.InnerHtml += "<span class=\"label label-flat border-grey text-grey-600\" data-popup=\"tooltip\"  data-placement=\"left\" title=\"" + c.Value + "\">" + c.Key + "</span> ";
            }
        }
        else
        {
            panelNextVisit.Visible = false;
        }
    }
}