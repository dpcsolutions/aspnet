﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Projects_Opens_ImportTimeSheetRecords" Codebehind="ImportTimeSheetRecords.ascx.cs" %>

<div class="row" style="margin: 0px; margin-bottom:20px">

    <div class="alert alert-primary no-border" style="line-height: 34px; margin-bottom:10px" id="alertMissing">
        <button type="button" id="btnImportHours" style="float: right;" class="btn btn-default btn-xs">
            <i class="icon-arrow-down16 position-left"></i>
            <%=Resources.Resource.Import %>
        </button>
        <span class="text-semibold">
            <span id="lblMissingCount" runat="server"></span>
            <%=Resources.Resource.HoursCanBeImportedProject %>
            .
        </span>

    </div>


    <div id="wizard_import_hours" style="display: none; margin-bottom: 10px;" class="panel panel-body">
        <asp:GridView ID="TsImportGrid" runat="server" AutoGenerateColumns="false">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <input type="checkbox" checked="checked" class="itsr-cbx" runat="server" ID="itsr_cbx" visible='<%# Bind("CanBeImported")  %>' />
                        <i class="icon-checkbox-checked2 text-grey-300" runat="server" id="itsr_cbx_done" visible='<%# Bind("Imported") %>'></i>

                        <input type="hidden" runat="server" id="hfservice" value='<%# Eval("ServiceName")%>' class="hfservice" />
                        <input type="hidden" runat="server" id="hfserviceid" value='<%# Eval("ServiceId")%>' class="hfserviceid" />
                        <input type="hidden" runat="server" id="hfid" value='<%# Eval("Id") %>' class="hfid" />
                        <input type="hidden" runat="server" id="hfduration" value='<%# (Eval("Duration") as double?).Value.ToString(System.Globalization.CultureInfo.InvariantCulture) %>' class="hfduration" />
                        <input type="hidden" runat="server" id="hfdate" value='<%# Eval("Date")%>' class="hfdate" />
                        <input type="hidden" runat="server" id="hfemployee" value='<%# Eval("EmployeeName")%>' class="hfemployee" />
                        
 
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Timesheet">
                    <ItemTemplate>
                        <asp:HyperLink runat="server" ID="lnk" NavigateUrl='<%# Bind("Link") %>' Target="_blank">

                            <i class="icon-question6 text-blue" runat="server" id="icoNotVal" visible='<%# Bind("TimeSheetInProgress")  %>'></i>
                            <i class="icon-checkmark text-green" runat="server" id="icoVal" visible='<%# Bind("TimeSheetValidated") %>'></i>
                            &nbsp;
                            <asp:Label runat="server" ID="lblEmployee" Text='<%# Bind("EmployeeName") %>' />, 
                            <asp:Label runat="server" ID="Label1" Text='<%# Bind("Week") %>' />




                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Service" DataField="ServiceName" />

                <asp:BoundField HeaderText="Date" DataField="Date" />
                <asp:BoundField HeaderText="Start" DataField="StartTime" ItemStyle-CssClass="text-right"  HeaderStyle-CssClass="text-right" />
                <asp:BoundField HeaderText="End" DataField="EndTime" ItemStyle-CssClass="text-right"  HeaderStyle-CssClass="text-right" />
                <asp:BoundField HeaderText="Rate" DataField="Rate" ItemStyle-CssClass="text-right"  HeaderStyle-CssClass="text-right" />
                <asp:BoundField HeaderText="Duration" DataField="Duration" ItemStyle-CssClass="text-right"  HeaderStyle-CssClass="text-right" DataFormatString="{0:0,0.00}" />



            </Columns>
        </asp:GridView>
        <div class="panel-footer">
			<div class="heading-elements">
				<span class="heading-text text-semibold">
                    
                    <input type="checkbox" class="itsr-selectall" checked="checked" /> <%= Resources.Resource.SelectAll %>
				</span>
				<span class="heading-text pull-right">

                    <div class="btn-group heading-btn pull-right">
				        <button class="btn btn-success btn-add-group" type="button"><i class="icon-add"></i> <%= Resources.Resource.Add %></button>
				        <button class="btn btn-success dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
						<ul class="dropdown-menu dropdown-menu-right" style="min-width:280px">
							<li><a href="javascript:void(0)" class="btn-add-group"><i class="icon-add"></i> <%= Resources.Resource.AddGroupingService %></a></li>
							<li><a href="javascript:void(0)" class="btn-add-sep"><i class="icon-add-to-list"></i> <%= Resources.Resource.AddEachLineSepar %></a></li>
							<li class="divider"></li>
							<li><a href="javascript:void(0)" class="btn-toggle-imprtd"><i class="icon-eye-blocked"></i>  <%= Resources.Resource.ShowImportedLines %></a></li>
						</ul>
				    </div>

				</span>
			</div>
		</div>
    </div>

    <script type="text/javascript">

        

        $(function () {

            $("#wizard_import_hours tr[data-Imported='True']").hide();

            $('#btnImportHours').click(function () {
                $('#wizard_import_hours').toggle();
            })

            $('.btn-add-group').click(function () {
                var records = getRecordsToImport();
                importRecords(records, true);
                markImportedSelection();
            });
            $('.btn-add-sep').click(function () {
                var records = getRecordsToImport();
                importRecords(records, false);
                markImportedSelection();
            });
            $('.btn-toggle-imprtd').click(function () {
                $("#wizard_import_hours tr[data-Imported='True']").toggle();
            });


            function markImportedSelection() {
                var checks = $('.itsr-cbx:checked');

                for (var i = 0; i < checks.length; i++) {
                    
                    var par = $(checks[i]).parent();
                    par.append($('<i class="icon-checkbox-checked2 text-grey-300"></i>'));
                    $(checks[i]).remove();

                    
                    var line = $(par).parent();
                    $(line).data("Imported", "True");
                    $(line).attr("data-Imported", "True");
                    $(line).hide();

                    timesheetrecordsimported.push(parseInt(par.find('.hfid').val()));
                }

                $('.itsr-selectall').prop('checked', false);

                var count = $("#wizard_import_hours tr[data-Imported='False']").length;
                if (count == 0) {
                    $('#ContentPlaceHolder1_JobEditor1_PanelImportTimeSheets').hide();
                }
            }

            function getRecordsToImport() {

                var ret = [];

                var checks = $('.itsr-cbx:checked');

                for (var i = 0; i < checks.length; i++) {
                    var par = $(checks[i]).parent();
                    

                    ret.push({
                        service: par.find('.hfservice').val(),
                        serviceId: par.find('.hfserviceid').val(),
                        duration: parseFloat(par.find('.hfduration').val()),
                        user: par.find('.hfemployee').val(),
                        date: par.find('.hfdate').val().replace(/\./g, '/')
                    })
                }

                return ret;
            }


            function importRecords(records, groupit) {

                var data = records;
                if (groupit == true)
                {
                    data = [];
                    for (var i = 0; i < records.length; i++) {
                        var found = false;
                        for (var j = 0; j < data.length; j++) {

                            if (data[j].service == records[i].service) {
                                found = true;
                                data[j].duration += records[i].duration;
                                break;
                            }
                        }

                        if (found == false) {
                            data.push(records[i]);
                        }
                    }
                }

                var cleanDataSource = [];
                for (var i = 0; i < tableDataSource.length; i++) {
                    if (tableDataSource[i].item != null) {
                        cleanDataSource.push(tableDataSource[i]);
                    }
                }

                for (var i = 0; i < data.length; i++) {

                    var service = findService(data[i].service);
                    var tax = null;

                    if (service != null && service.taxId != null)
                        tax = findTaxById(service.taxId);

                    var taxname = null;
                    if (tax != null)
                        taxname = tax.name;

                    var unitprice = 0;

                    if (service != null)
                        unitprice = service.unitPrice;

                    
                    var n = new Date(0, 0);
                    n.setMinutes(+data[i].duration * 60);
                    var duration = n.toTimeString().slice(0, 5);

                    cleanDataSource.push({
                        item: data[i].date + ', ' + data[i].service + ', ' + data[i].user + ' - ' + duration ,
                        qty: data[i].duration,
                        price: unitprice,
                        tax: taxname,
                        discount: 0,
                        totalprice: precisionRound(unitprice * data[i].duration, 2),
                        serviceId: service.id,
                    });
                    
                    
                }

                while(tableDataSource.length > 0) {
                    tableDataSource.pop();
                }
                for (var i = 0; i < cleanDataSource.length; i++) {
                    tableDataSource.push(cleanDataSource[i])
                }
                for (var i = 0; i < 5; i++) tableDataSource.push({});

                hot_ac_lazy_init.updateData(tableDataSource);
                recalcTotals();
            }


            // checkboxes sync
            $('.itsr-selectall').click(function () {


                var checked = $('.itsr-selectall').is(':checked');
                if (checked) {
                    $('.itsr-cbx').prop('checked', true);
                }
                else {
                    $('.itsr-cbx').prop('checked', false);
                }
                

            });


            $('.itsr-cbx').click(function () {


                var countchecked = $(".itsr-cbx:checked").length;
                var countall = $(".itsr-cbx").length;

                if (countchecked == countall) {
                    $('.itsr-selectall').prop('checked', true);
                }
                else {
                    $('.itsr-selectall').prop('checked', false);
                }



            });
            
        });



    </script>

</div>
