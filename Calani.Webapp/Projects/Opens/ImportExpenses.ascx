﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Projects_Opens_ImportExpenses" Codebehind="ImportExpenses.ascx.cs" %>

<div class="row" style="margin: 0px; margin-bottom:20px">

    <div class="alert alert-primary no-border" style="line-height: 34px; margin-bottom:10px" id="alertMissingExpenses">
        <button type="button" id="btnImportExpenses" style="float: right;" class="btn btn-default btn-xs">
            <i class="icon-arrow-down16 position-left"></i>
            <%=Resources.Resource.Import %>
        </button>
        <span class="text-semibold">
            <span id="lblMissingExpSum" runat="server"></span>
            <%=Resources.Resource.ExpensesCanBeImported %>
            .
        </span>

    </div>

    <div id="wizard_import_expenses" style="display: none; margin-bottom: 10px;" class="panel panel-body">
        <asp:GridView ID="ExpImportGrid" runat="server" AutoGenerateColumns="false">
            <Columns>
                
                <asp:TemplateField>
                    <ItemTemplate>
                        <input type="checkbox" checked="checked" class="itsrexp-cbx" runat="server" ID="itsr_cbx" visible='<%# Bind("CanBeImported")  %>' />
                        <i class="icon-checkbox-checked2 text-grey-300" runat="server" id="itsr_cbx_done" visible='<%# Bind("Imported") %>'></i>

                        <input type="hidden" runat="server" id="hfid" value='<%# Eval("Id") %>' class="hfid" />
                        <input type="hidden" runat="server" id="hfdescription" value='<%# Eval("Description") %>' class="hfdescription" />
                        <input type="hidden" runat="server" id="hfprice" value='<%# (Eval("Price") as double?).Value.ToString(System.Globalization.CultureInfo.InvariantCulture) %>' class="hfprice" />
 
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Expense">
                    <ItemTemplate>
                        <asp:HyperLink runat="server" ID="lnk" NavigateUrl='<%# Bind("Link") %>' Target="_blank">

                            <i class="icon-question6 text-blue" runat="server" id="icoNotVal" visible='<%# Bind("ExpenseInProgress")  %>'></i>
                            <i class="icon-checkmark text-green" runat="server" id="icoVal" visible='<%# Bind("ExpenseValidated") %>'></i>
                            &nbsp;
                            <asp:Label runat="server" ID="lblDescription" Text='<%# Bind("Description") %>' />, 
                           



                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField HeaderText="Date" DataField="Date" />
                <asp:BoundField HeaderText="Price" DataField="Price" ItemStyle-CssClass="text-right"  HeaderStyle-CssClass="text-right" DataFormatString="{0:0,0.00}" />



            </Columns>
        </asp:GridView>
        <div class="panel-footer">
			<div class="heading-elements">
				<span class="heading-text text-semibold">
                    
                    <input type="checkbox" class="itsrext-selectall" checked="checked" /> <%= Resources.Resource.SelectAll %>
				</span>
				<span class="heading-text pull-right">

                    <div class="btn-group heading-btn pull-right">
				        <button class="btn btn-success btn-add-exp" type="button"><i class="icon-add"></i> <%= Resources.Resource.Add %></button>
				    </div>

				</span>
			</div>
		</div>
    </div>

    <script type="text/javascript">

        

        $(function () {

            $("#wizard_import_hours tr[data-Imported='True']").hide();

            $('#btnImportExpenses').click(function () {
                $('#wizard_import_expenses').toggle();
            })

            $('.btn-add-exp').click(function () {
                var records = getExpRecordsToImport();
                importExpRecords(records, true);
                markExpImportedSelection();
            });



            function markExpImportedSelection() {
                var checks = $('.itsrexp-cbx:checked');

                for (var i = 0; i < checks.length; i++) {
                    
                    var par = $(checks[i]).parent();
                    par.append($('<i class="icon-checkbox-checked2 text-grey-300"></i>'));
                    $(checks[i]).remove();

                    
                    var line = $(par).parent();
                    $(line).data("Imported", "True");
                    $(line).attr("data-Imported", "True");
                    $(line).hide();

                    expensesrecordsimported.push(parseInt(par.find('.hfid').val()));
                }

                $('.itsrext-selectall').prop('checked', false);

                var count = $("#wizard_import_hours tr[data-Imported='False']").length;
                if (count == 0) {
                    $('#ContentPlaceHolder1_JobEditor1_PanelImportTimeSheets').hide();
                }
            }

            function getExpRecordsToImport() {
                
                var ret = [];

                var checks = $('.itsrexp-cbx:checked');

                for (var i = 0; i < checks.length; i++) {
                    var par = $(checks[i]).parent();
                    

                    ret.push({
                        service: par.find('.hfdescription').val(),
                        price: parseFloat(par.find('.hfprice').val())
                    })
                }

                return ret;
            }


            function importExpRecords(records, groupit) {

                let cleanDataSource = [];
                for (let i = 0; i < tableDataSource.length; i++) {
                    if (tableDataSource[i].item != null) {
                        cleanDataSource.push(tableDataSource[i]);
                    }
                }

                for (let i = 0; i < records.length; i++) {
                    
                    cleanDataSource.push({
                        id: null,
                        position: null,
                        isChecked: false,
                        item: records[i].service,
                        qty: 1,
                        price: +precisionRoundNumber(records[i].price, 2),
                        tax: null,
                        discount: 0,
                        totalprice: +precisionRoundNumber(records[i].price, 2)
                    });               
                }
                
                while(tableDataSource.length > 0) {
                    tableDataSource.pop();
                }
                
                for (let i = 0; i < cleanDataSource.length; i++) {
                    tableDataSource.push(cleanDataSource[i])
                }

                hot_ac_lazy_init.loadData(tableDataSource);
                recalcTotals();
                
            }


            // checkboxes sync
            $('.itsrext-selectall').click(function () {


                var checked = $('.itsrext-selectall').is(':checked');
                if (checked) {
                    $('.itsrexp-cbx').prop('checked', true);
                }
                else {
                    $('.itsrexp-cbx').prop('checked', false);
                }
                

            });


            $('.itsrexp-cbx').click(function () {


                var countchecked = $(".itsrexp-cbx:checked").length;
                var countall = $(".itsrexp-cbx").length;

                if (countchecked == countall) {
                    $('.itsrext-selectall').prop('checked', true);
                }
                else {
                    $('.itsrext-selectall').prop('checked', false);
                }



            });
            
        });



    </script>

</div>