﻿using Calani.BusinessObjects.Projects;
using System;
using System.Collections.Generic;
using System.Linq;

public partial class Projects_Opens_ImportTimeSheetRecords : System.Web.UI.UserControl
{
    public double HoursCurrYear { get; private set; }

    //public string JsonTsimports { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        long id = -1;
        if (Request.Params["id"] != null)
        {
            if (!long.TryParse(Request.Params["id"], out id))
            {
                this.Visible = false;
                return;
            }
        }

        var orgId = PageHelper.GetCurrentOrganizationId(Session);
        var mgr = new ProjectsManager(orgId);
        double sum;

        List<TimeSheetRecordImport> items = mgr.PrepareTimeSheetRecordsForImport( id, out sum);


        if (sum == 0)
        {
            this.Visible = false;
            return;
        }
        else
        {
            lblMissingCount.InnerText = string.Format("{0:0,0.00}", sum);
        }
        
        
        TsImportGrid.DataSource = items;
        TsImportGrid.DataBind();
        new Html5GridView(TsImportGrid, false, new List<string> { "Imported" });

        HoursCurrYear = items.Where(item => !item.Imported && item.StartDate.HasValue && item.StartDate.Value.Year == DateTime.Now.Year)
            .Sum(item => item.Duration);
    }

    
}