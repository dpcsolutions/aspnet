﻿<%@ Page Title="Job" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Projects_Opens_Job" Codebehind="Job.aspx.cs" %>
<%@ Import Namespace="System.Globalization" %>
<%@ Register src="../JobEditor.ascx" tagname="JobEditor" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="dialogId"></div>
<form runat="server">
    <div class="content">

        
        <uc1:JobEditor ID="JobEditor1" runat="server" EnableProjectFields="true" />        



        <asp:Panel runat="server" ID="PanelCalendarAvailable">
        <div class="row" id="boxCalendar">

            <div class="col-sm-12 col-md-12">
	            <div class="panel panel-flat">
			        <div class="panel-heading">
				        <h5 class="panel-title"><%= Resources.Resource.Visits %></h5>
			        </div>
                    <div class="panel-body">
                        <div class="tabbable">
					        <ul class="nav nav-tabs nav-tabs-top nav-justified" id="visitTabs">
						        <li class="<%=ClassVisitListTab %>" id="visitListTabId"><a href="#visitListPane" class="legitRipple" aria-expanded="false"><%= Resources.Resource.MeetingList %></a></li>
						        <li class="<%=ClassVisitCalendarTab %>" id="visitCalendarTabId"><a href="#visitCalendarPane" class="legitRipple" aria-expanded="true"><%= Resources.Resource.ProjectScheduler %></a></li>
					        </ul>
					        <div class="tab-content">
						        <div class="tab-pane <%=ClassVisitListTab %>" id="visitListPane">
                                    <div class="visitList"></div>
						        </div>
                                <div class="tab-pane <%=ClassVisitCalendarTab %>" id="visitCalendarPane">
                                    <div class="content-group-lg select-resources-section">
					                    <h6 class="text-semibold"><%= Resources.Resource.EmployeesSelection %> </h6>
			                            <p class="content-group-sm"><%= Resources.Resource.EmployessVisitsAreVisibleOnTheCalendar %></p>
                                        <div class="input-group">
                                            <select class="select select-resources" multiple="multiple">
					                        </select>
                                            <span class="input-group-btn resources-select-button-pane">
                                                <span class="input-group-btn"><button class="btn select-all-resources"><%= Resources.Resource.All %></button></span>
                                                <span class="input-group-btn"><button style="margin-left:1px;" class="btn clear-all-resources"><%= Resources.Resource.Clear %></button></span>
                                            </span>
		                                </div>
                                    </div>
                                    <div class="visitCalendar"></div>
						        </div>
					        </div>
				        </div>
                    </div>
                </div>
            </div>

        </div>
        </asp:Panel>

        <asp:Panel runat="server" ID="PanelCalendarNotAvailable" Visible="false">
            <div class="row">
                <div class="col-sm-12 col-md-12">
	                <div class="panel panel-flat">
			            <div class="panel-heading">
				            <h5 class="panel-title"><%= Resources.Resource.Visits %></h5>
			            </div>
                        <div class="panel-body">

                            <div class="alert alert-warning no-border">
								<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only"><%= Resources.Resource.Close %></span></button>
								<span class="text-semibold"><%= Resources.Resource.Warning %> !</span> 
                                <%= Resources.Resource.PlaningAfterFirstSave %> 
                                <%= Resources.Resource.PleaseClickSaveToCreate %> 
                                
							</div>

                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>

        <div class="row" runat="server" id="panelActions">

            <div class="col-sm-12 col-md-9">
	            <div class="panel panel-flat">
			        <div class="panel-heading">
				        <h5 class="panel-title"><%= Resources.Resource.ProjectManagement %></h5>
			        </div>
                    <div class="panel-body">
                        <% if (MissingChild)
                            { %>
                        <div>
                            <div class="checkbox">
							    <label>
								    <asp:CheckBox runat="server" class="control-success" id="cbxDontNotifyOnMissingInvoice" />
								    <%= Resources.Resource.DontNotifyOnMissingInvoice %>
							    </label>
						    </div>
                        </div>
                        <% } %>

				            <div class="row text-center">
											
					            <div class="col-xs-6">
							            <div class="panel panel-body border-top-primary text-center">

								            <p class="text-muted content-group-sm text-size-small two-lines-small-text" id="lblLastChange" runat="server">
                                            
								            </p>

								            <div class="btn-group">
                                                <a href runat="server" class="btn btn-primary btn-save" data-action="save" data-lbl-date="ContentPlaceHolder1_lblLastChange">
                                                    <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                                </a>
                                                
									            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore"><span class="caret"></span></button>
									            <ul class="dropdown-menu dropdown-menu-right" style="min-width:240px">
                                                    
                                                    
                                                   <%-- <li>
                                                        <a href="javascript:void(0)" type="button" class="btn-save" 
                                                           data-lbl-date="ContentPlaceHolder1_lblLastSent"
                                                           data-action="sendproject">
                                                            <i class="icon-envelop position-left"></i>  <%= Resources.Resource.SendByEmail %> 

                                                        </a>
                                                    </li>--%>
                                                    <li><a href="javascript:void(0)" class="btn-save" data-action="save" data-lbl-date="ContentPlaceHolder1_lblLastChange"><i class="icon-floppy-disk"></i> <%= Resources.Resource.SaveChanges %></a></li>
                                                    <li><a href="javascript:void(0)" class="btn-clone" data-type="*" data-toggle="modal" data-target="#modal_form_cloneproject"><i class="fa fa-copy"></i> <%= Resources.Resource.Clone %></a></li>
                                                    <li><a id="btnDownloadExcel" href="javascript:void(0)" class="btn-default" data-type="*" data-toggle="modal" data-target="#modal_form_exportproject"><i class="icon-file-download position-left"></i> <%= Resources.Resource.Excel %></a></li>
                                                    <li><a href="javascript:void(0)" class="btn-delete" data-type="*" data-toggle="modal" data-target="#modal_form_deleteproject"><i class="fa fa-trash"></i> <%= Resources.Resource.Delete %></a></li>
                                            
									            </ul>
								            </div>
							            </div>	
					            </div>
                                <div class="col-xs-6">
										    <div class="panel panel-body border-top-primary text-center">
								                <p class="text-muted content-group-sm text-size-small two-lines-small-text" id="lblStatusChange" runat="server">
                                                    
											    </p>

                                                <div class="btn-group">
                                                    <a href runat="server" class="btn btn-success btn-save" data-action="projectfinish" data-lbl-date="ContentPlaceHolder1_lblStatusChange">
                                                        <i class="icon-finish position-left"></i> <%= Resources.Resource.FinishProject %>
                                                    </a>
                                                
									                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore2"><span class="caret"></span></button>
									                <ul class="dropdown-menu dropdown-menu-right" style="min-width:240px">
                                                        <li><a href="javascript:void(0)" class="btn-save" data-action="projectfinish" data-lbl-date="ContentPlaceHolder1_lblStatusChange"><i class="icon-finish"></i> <%= Resources.Resource.MarkAsFinished %></a></li>
                                                        <li><a href="javascript:void(0)" class="btn-save" data-action="projectsuspended" data-lbl-date="ContentPlaceHolder1_lblStatusChange"><i class="icon-pause2"></i> <%= Resources.Resource.MarkAsSuspended %></a></li>
                                                        <li><a href="javascript:void(0)" class="btn-save" data-action="projectinprogress" data-lbl-date="ContentPlaceHolder1_lblStatusChange"><i class="icon-play4"></i> <%= Resources.Resource.MarkAsInProgress %></a></li>
                                            
									                </ul>
								                </div>

										    </div>	
							    </div>
				            </div>	
			            </div>
                </div>
            </div>

        </div>

    </div>
    <script type="text/javascript">
        Handsontable.editors.TextEditor.prototype.hideEditableElement = function() {
            this.textareaParentStyle.visibility = 'hidden';
            this.textareaParentStyle.position = 'fixed';
            this.textareaParentStyle.top = '0px';
            this.textareaParentStyle.left = '0px';
            this.textareaParentStyle.zIndex = '-1';
        };
        
        Handsontable.editors.TextEditor.prototype.showEditableElement = function() {
            this.textareaParentStyle.position = 'absolute';
            this.textareaParentStyle.visibility = 'visible';
        
            this.textareaParentStyle.zIndex = '10000';
        };
        
        var publicHolidayBackgroundColor = "#fdb5b5";

        var locale = "<%= locale %>";
        var defaultDate = "<%= defaultDate%>";
        var resources = <%= resources %>;
        var currentLanguage = '<%= currentLanguage%>';
        var localisedDateFormat = '<%= localisedDateFormat%>';
        var resourcesForJob = <%= resourcesForJob %>;
        var ainternalServices = <%= internalServices%>;
        var preselectedJobId = <%= preselectedJobId %>;
        var preselectedClientId = <%= preselectedClientId %>;
        var officialHolidays = <%= officialHolidays %>;
        var employeeCanViewAllEvents = <%= EmployeeCanViewAllEvents %>;
        officialHolidays.forEach(h => {h.rendering = "background"; h.backgroundColor = publicHolidayBackgroundColor; h.allDay = true});

        var standardBusinessHours = [ // specify an array instead
            {
                dow: [ 1, 2, 3, 4, 5 ], // Monday, Tuesday, Wednesday
                start: '08:00', // 8am
                end: '18:00' // 6pm
            },
        ];

        var calendarEvents = [];

        var ressources2 = {};
        ressources2['Save'] = "<%= Resources.Resource.Save %>";
        ressources2['Delete'] = "<%= Resources.Resource.Delete %>";
        ressources2['Cancel'] = "<%= Resources.Resource.Cancel %>";
        ressources2['DeleteOnlyThisOrAll'] = "<%= Resources.Resource.DeleteOnlyThisOrAll %>";
        ressources2['DeleteAppointment'] = "<%= Resources.Resource.DeleteAppointment %>";
        ressources2['DeleteAllAppointments'] = "<%= Resources.Resource.DeleteAllAppointments %>";
        ressources2['DeleteOnlyThisAppointment'] = "<%= Resources.Resource.DeleteOnlyThisAppointment %>";
        ressources2['Unit'] = "<%= Resources.Resource.Unit %>";

        $('#btnDownloadExcel').click(function () {
            let url = "GetExcelProject.ashx";
            url += `?projectId=${preselectedJobId}`;
            url += "&c=<%=CultureInfo.CurrentCulture.TwoLetterISOLanguageName%>";
            
            window.location = url;
        });
        
        function afterJobSave(query, response) {
            if (query.action.indexOf("save") == 0 && query.id == -1) {
                window.location = "Job.aspx?id=" + response.id;
            }

            if (query.action == "projectfinish") {
                workflowDialog(false, true, false, false);
            }

            if (query.action == "sendproject" && query.id > 0) {
                window.location = "Send.aspx?id=" + query.id + "&sel=" + response.attachment;
            }
        }

        $(function () {
            calendarInit();
        });

        var calendarInit = function(){
            //$("#boxCalendar .tab-content").height("1000"); //to define

            var sp2 = new schedulerPage();
            var allResources = [];
            resources.forEach(r => allResources.push(r.id));

            var options = {
                opacifyAllEventsButId: undefined,
                screen: 1, //0:planning, 1:job, 2:holidays
                resources: resources,
                calendarId: 'visitList',
                canChangeResourceSelection: false,
                preselectedJobId: preselectedJobId,
                preselectedClientId: preselectedClientId,
                renderer: sp2.eventRenderer,
                viewEventsForAllJobs:false,
                internalServices:ainternalServices,
                buttons:  {
                    list: [{ id: 'listYear', name: 'Année' },
                            { id: 'listMonth', name: 'Mois' },
                            { id: 'listWeek', name: 'Semaine' },
                            { id: 'listDay', name: 'Jour' }]
                },
                resourcesListId: undefined, //will not fill up sresources select list
                preSelectedResources: allResources,
                defaultView: 'listYear',
                excludeAbsenceRequests: true
            };
            sp2.init(options);

            var allResourcesForJob = [];
            resourcesForJob.forEach(r => allResourcesForJob.push(r.id));


            var sp3 = new schedulerPage();

            options = {
                officialHollidays: officialHolidays,
                opacifyAllEventsButForJobId: preselectedJobId,
                screen: 1, //0:planning, 1:job, 2:holidays
                resources: resources,
                calendarId: 'visitCalendar',
                preselectedJobId: preselectedJobId,
                preselectedClientId: preselectedClientId,
                internalServices:ainternalServices,
                canChangeResourceSelection: true,
                viewEventsForAllJobs:false,
                renderer: sp3.eventRenderer,
                buttons: {
                    agenda: [{ id: 'month', name: 'Mois' },
                       { id: 'agendaWeek', name: 'Semaine' },
                       { id: 'agendaDay', name: 'Jour' }]
                },
                resourcesListId: '.select-resources', //will not fill up sresources select list
                preSelectedResources: allResourcesForJob,
                defaultView: 'month',
                excludeAbsenceRequests: true
            };

            sp3.init(options);

            $('.select-resources-section').show();

            $('#visitTabs a').click(function (e) {
                e.preventDefault();

                switch ($(e.target).attr('href'))
                {
                    case "#visitListPane":
                        sp2.refresh();
                        break;
                    case "#visitCalendarPane":
              
                        sp3.refresh();
                        break;
                }
                $(this).tab('show');
            })

            sp2.refresh();
        }
       
    </script>
</form>
</asp:Content>

