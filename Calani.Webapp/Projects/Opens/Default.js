﻿var _periodFilterFrom = null;
var _periodFilterTo = null;
var _clientFilter = null;
var _statusFilter = 'JobOpened';

var _dataTable = null;

function setPeriodFilter() {
    _periodFilterFrom = null;
    _periodFilterTo = null;

    var pval = $('#ddlPeriod').val();
    if (pval != null && pval.indexOf(';') > 0) {
        pval = pval.split(';');
        if (pval.length == 2) {
            _periodFilterFrom = pval[0];
            _periodFilterTo = pval[1];

            if (_periodFilterFrom != "*") _periodFilterFrom = parseInt(_periodFilterFrom);
            else _periodFilterFrom = null;

            if (_periodFilterTo != "*") _periodFilterTo = parseInt(_periodFilterTo);
            else _periodFilterTo = null;
        }
    }

    refreshLines();
}

function setClientFilter() {
    _clientFilter = $('#ddlClient').val();
    refreshLines();
}

function setStatusFilter(newStatus) {
    _statusFilter = newStatus;
    refreshLines();
}

function refreshLines() {
    _dataTable.draw();
}

function checkField(val, str){
    return val?.toString().toLowerCase().includes(str.toLowerCase());
}

function containsString (obj, str) {
    if (checkField(obj[0], str)) return true;
    if (checkField(obj[1], str)) return true;
    if (checkField(obj[2], str)) return true;
    if (checkField(obj[3], str)) return true;
    if (checkField(obj[4], str)) return true;
    if (checkField(obj[5], str)) return true;
    if (checkField(obj[6], str)) return true;
    if (checkField(obj[7], str)) return true;
    if (checkField(obj[8], str)) return true;
    if (checkField(obj[9].display, str)) return true;

    return false;
}

function refreshLiveStats() {

    var countJobOpened = 0;
    var countJobOnHold = 0;
    var countJobOutDated = 0;
    var countJobFinished = 0;
    var countMissingChild = 0;
    var sumOpened = 0;


    var rowsId = _dataTable.rows().eq(0);
    for (var i = 0; i < rowsId.length; i++) {
        let row = _dataTable.row(rowsId[i]);
        let data = row.data();

        if (row.node().attributes["data-anystatusfilter"] != null && row.node().attributes["data-anystatusfilter"].value == "true") {

            if (row.node().attributes["data-projectstatus"] != null) {

                let rowStatus = row.node().attributes["data-projectstatus"].value;
                if (rowStatus != null) {
                    let matchesSearch = true;
                    let searchText = $('.dataTables_filter input').val();
                    if (searchText) matchesSearch = containsString(data, searchText);

                    switch (rowStatus) {
                        case "JobOpened":
                            if (matchesSearch) {
                                countJobOpened++;                   
                                if (row.node().attributes["data-totalWithoutTaxes"] != null) {
                                    sumOpened += parseFloat(row.node().attributes["data-totalToInvoice"].value);
                                }
                            }
                            break;
                        case "JobOnHold":
                            if (matchesSearch) {
                                countJobOnHold++;
                            }
                            break;
                        case "JobOutDated":
                            if (matchesSearch) {
                                countJobOutDated++;
                                if (row.node().attributes["data-totalWithoutTaxes"] != null) {
                                    sumOpened += parseFloat(row.node().attributes["data-totalToInvoice"].value);
                                }
                            }
                            break;
                        case "JobFinished":
                            if (matchesSearch) {
                                countJobFinished++;
                                let mc = row.node().attributes["data-missingchild"].value;
                                if (mc === "True")
                                    countMissingChild++;
                            }
                            break;
                    }
                }
            }
        }
    }

    $('#ContentPlaceHolder1_lblCountJobOpened').html(countJobOpened);
    $('#ContentPlaceHolder1_lblCountJobOnHold').html(countJobOnHold);
    $('#ContentPlaceHolder1_lblCountJobOutDated').html(countJobOutDated);
    $('#ContentPlaceHolder1_lblCountJobFinished').html(countJobFinished);
    $('#ContentPlaceHolder1_lblOened').html(formatMoney(sumOpened));
    $('#ContentPlaceHolder1_lblCountMissingChild').html(countMissingChild);

    if (countJobFinished > 0)
        $('#ContentPlaceHolder1_lblCountJobFinished').addClass("animated");
    else
        $('#ContentPlaceHolder1_lblCountJobFinished').removeClass("animated");

}
function formatMoney(d) {
    d = d
        .toFixed(2)
        .replace('.', ',')
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ');
    return d + " " + currency;
}

function filterItem(settings, data, dataIndex) {
    let visible = true;

    let searchText = $('.dataTables_filter input').val();
    if (searchText) {
        visible = containsString(data, searchText);
    }
    
    if (_clientFilter != null) {
        var clientMatch = false;
        for (var i = 0; i < _clientFilter.length; i++) {
            if (_clientFilter[i] == settings.aoData[dataIndex].nTr.getAttribute('data-clientId')) clientMatch = true;
        }
        if (clientMatch == false)
            visible = false;
    }

    /*  var lastUpdate = parseInt(settings.aoData[dataIndex].nTr.getAttribute('data-lastupdate'));
      if (_periodFilterFrom != null) {
          if (lastUpdate < _periodFilterFrom) visible = false;
      }
      if (_periodFilterTo != null) {
          if (lastUpdate >= _periodFilterTo) visible = false;
      }*/
    var attr = settings.aoData[dataIndex].nTr.getAttribute('data-datejobvalidity');
    var dateJobValidity = parseInt(attr);

    attr = settings.aoData[dataIndex].nTr.getAttribute('data-datecreated');
    var dateCreated = parseInt(attr);

    if (_periodFilterFrom != null && _periodFilterTo != null) {
        visible = dateCreated <= _periodFilterTo && dateCreated >= _periodFilterFrom ||
            dateJobValidity <= _periodFilterTo && dateJobValidity >= _periodFilterFrom;
    }
    else if (_periodFilterFrom != null) {
        visible = dateJobValidity >= _periodFilterFrom;
    }
    else if (_periodFilterTo != null) {
        visible = dateJobValidity <= _periodFilterTo;
    }


    settings.aoData[dataIndex].nTr.setAttribute('data-anystatusFilter', visible);

    if (_statusFilter != null) {

        if (_statusFilter == "JobMissingChild") {
            if (settings.aoData[dataIndex].nTr.getAttribute('data-missingchild') == "True")
                visible = true;
            else
                visible = false;
        }

        else if (settings.aoData[dataIndex].nTr.getAttribute('data-ProjectStatus') != _statusFilter)
            visible = false;
    }

    return visible;
}

$(function () {

    var f = getUrlParameter('f');
    if (f != null) _statusFilter = f;

    // client side filters
    $.fn.dataTable.ext.search.push(filterItem);
    // -->

    _dataTable = initDataTable($('#ContentPlaceHolder1_grid'), true);
    _dataTable.search('');

    _dataTable.on('draw', function () {
        refreshLiveStats();
    });

    setPeriodFilter();

});

new Vue({

    el: '#app',

    data: {
        statusFilter: 'JobOpened'
    },
    methods: {
        setStatusFilter: function (e, f) {
            this.statusFilter = f;
            _statusFilter = f;
            _dataTable.draw();
            e && e.preventDefault();
        }
    }
});