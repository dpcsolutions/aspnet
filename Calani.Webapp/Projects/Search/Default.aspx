﻿<%@ Page Title="Search" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Projects_Search" Codebehind="Default.aspx.cs" %>
<%@ Register src="../../assets/Controls/ProjectStatusLabel.ascx" tagname="ProjectStatusLabel" tagprefix="psl" %>
<%@ Register src="../../assets/Controls/ProjectTypeLabel.ascx" tagname="ProjectTypeLabel" tagprefix="ptl" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
    <div class="btn-group" runat="server" id="quickmenu">
		<button type="button" class="btn bg-gesmobile btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
		</button>

		<ul class="dropdown-menu dropdown-menu-right">
			<li><a href="Job.aspx"><i class="icon-plus2"></i> <%= Resources.Resource.Create %> <%= Resources.Resource.Invoice %></a></li>
		</ul>
	</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">

        


                <div class="content">

                    <div class="row">
                        <div class="col-sm-12 col-md-12">
            
                            <div class="panel panel-body bg-grey-800">
                                <p>
                                    <strong>A la recherche d'une facture, ou d'un devis?</strong>
                                    <br />
                                    Cette page vous permet une recherche intuitive et naturelle à partir du nom du client, du nom d'un article ou service, 
                                    un email client, un numéro de téléphone,
                                    ou même une annotation que vous auriez faite sur les notes de votre projet GesMobile
                                    (un N° de série, ou une instruction particulière par exemple).
                                </p>
                                <p>
                                    <asp:TextBox runat="server" ID="tbxSearch" CssClass="form-control input-rounded" />
                                </p>
                                <p style="text-align:right;">
                                    <asp:Button runat="server" ID="btnSearch" Text="Recherche..."  CssClass="btn btn-success" OnClick="btnSearch_Click"  />
                                </p>
                            </div>

                        </div>

                    </div>


                    <asp:Panel runat="server" ID="PanelResults" CssClass="row" Visible="false">
                        <div class="col-sm-12 col-md-12">
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="icon-folder-search display-inline-block text-green"></i>
										<strong>Résultats</strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

							<div class="panel-body">
                                <asp:GridView runat="server" ID="grid" AutoGenerateColumns="False" EnableViewState="false">
                                    <Columns>
                                        <asp:HyperLinkField DataTextField="internalId" HeaderText="Number" ItemStyle-CssClass="text-nowrap" DataNavigateUrlFields="Tag1" DataNavigateUrlFormatString="{0}"/>
                                        <asp:BoundField DataField="clientName" HeaderText="Customer"  />
                                        <asp:BoundField DataField="description" HeaderText="Description"  />
                                        <asp:TemplateField HeaderText="Type">
                                            <ItemTemplate>
                                                <ptl:ProjectTypeLabel ID="pt1" runat="server" ProjectStatus='<%# Bind("ProjectStatus") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <psl:ProjectStatusLabel ID="psl" runat="server" ProjectStatus='<%# Bind("ProjectStatus") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="total" HeaderText="Total" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right" DataFormatString="{0:0,0.00}" />
                                        
                                       
                                    </Columns>
                                </asp:GridView>

                                <asp:Panel runat="server" ID="notFound" Visible="true">
                                    <p>Désolé, aucune correspondance trouvée.</p>
                                </asp:Panel>

                                
                            </div><!-- panel-body -->


                        </div>


                    </div>
                    </asp:Panel>
                </div>
    </form>
</asp:Content>

