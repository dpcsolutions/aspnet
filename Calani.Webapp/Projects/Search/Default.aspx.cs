﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Projects_Search : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
        
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {


        // avant la colonne etat, ajouter une colonne type : 'Devis' / 'Projet' / 'Facture'

        Calani.BusinessObjects.Projects.ProjectsManager mgr = new Calani.BusinessObjects.Projects.ProjectsManager(ph.CurrentOrganizationId);
        var list = mgr.NaturalSearch(tbxSearch.Text);
        foreach (var item in list)
        {
            string type = "Opens";
            if(item.ProjectStatus.ToString().StartsWith("Invoice")) type = "Invoices";
            else if(item.ProjectStatus.ToString().StartsWith("Quote")) type = "Quotes";
            item.Tag1 = "../"+ type + "/Job.aspx?id=" + item.id;

            var status = item.ProjectStatus;
        }
        grid.DataSource = list;
        grid.DataBind();

        new Html5GridView(grid, false);
        PanelResults.Visible = true;

        grid.Visible = (list.Count > 0);
        notFound.Visible = !grid.Visible;




    }
}