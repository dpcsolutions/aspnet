﻿var hot_ac_lazy_init = null;

var timesheetrecordsimported = [];
var expensesrecordsimported = [];

var saveProject = null;

function recalcTotals() {

    var totalQuantity = 0;
    var totExclTax = 0;
    var totTaxes = 0;
    var totTaxesDiscounted = 0;
    var totToInvoice = 0;
    var discountPercentEscompte = getDiscountPercent();

    var discount = $("#ContentPlaceHolder1_JobEditor1_tbxDiscount").val();

    if (discount == null || discount == "" || isNaN(discount)) discount = 0;
    discount = 100 - discount;
    discount /= 100;

    var taxesDetails = {};

    var vatRates = [];
    for (var i = 0; i < tableDataSource.length; i++) {
        var rowIdx = hot_ac_lazy_init.toPhysicalRow(i);

        if (tableDataSource[rowIdx]?.totalprice != null) {
            var ht = toDouble(tableDataSource[rowIdx].totalprice) * discount;

            totExclTax += ht;

            if (tableDataSource[rowIdx].toinvoice == null) {
                totToInvoice += ht;
                totalQuantity += +tableDataSource[rowIdx]?.qty || 0;
            }

            if (tableDataSource[rowIdx].tax != null) {
                var t = findTax(tableDataSource[rowIdx].tax);
                if (t != null) {

                    var tc = ht * t.rate / 100;
                    var tn = tableDataSource[rowIdx].tax + " (" + t.rate + "%)";

                    if (vatRates.indexOf(t.rate) == -1) {
                        vatRates.push(t.rate);
                    }

                    if (taxesDetails[tableDataSource[rowIdx].tax] == null) taxesDetails[tn] = 0;
                    taxesDetails[tn] += tc;


                    totTaxes += tc;
                    totTaxesDiscounted += (ht * (100 - discountPercentEscompte) / 100) * t.rate / 100;
                }
            }
        }
    }

    // Discount feature is disabled if there are several VAT rates applied
    switchDiscountFeature(vatRates.length < 2);

    // draw tax details
    $('#taxDetails').html('');
    var taxDetailsHtml = "";
    for (var propertyName in taxesDetails) {

        var taxDetailItem = "<div class=\"form-group\">";
        taxDetailItem += "	<label class=\"control-label col-lg-6 text-right\">$taxName</label>";
        taxDetailItem += "		<div class=\"col-lg-5 text-right\">";
        taxDetailItem += "			<div class=\"input-group\">";
        taxDetailItem += "				<span class=\"input-group-addon\"><i class=\"icon-balance\"></i></span>";
        taxDetailItem += "				<input type=\"text\" value=\"$taxValue\" step=\"0.01\" class=\"form-control\" placeholder=\"Taxes\" disabled=\"disabled\" >";
        taxDetailItem += "			</div>";
        taxDetailItem += "		</div>";
        taxDetailItem += "</div>";

        taxDetailItem = taxDetailItem.replace('$taxName', propertyName);
        taxDetailItem = taxDetailItem.replace('$taxValue', precisionRound(taxesDetails[propertyName], 2));
        taxDetailsHtml += taxDetailItem;

    }
    $('#taxDetails').html(taxDetailsHtml);
    // -->

    $('#hdnTotalExclTaxBeforeDiscount').val(totExclTax);
    $('#btnTotalExclTax').val(precisionRound(totExclTax, 2));

    refreshDiscountPercent();
    var totalExclTaxDiscounted = totExclTax - calculateAllDiscounts(totExclTax);
    
    if (vatRates.length == 1) {
        totTaxes = totTaxesDiscounted;
    }

    $('#tbxTaxes').val(precisionRound(totTaxes, 2));
    $('#tbxTotalHT').val(precisionRound(totalExclTaxDiscounted, 2));

    var originalAmount = usePricesIncludingVat ? totalExclTaxDiscounted : totalExclTaxDiscounted + totTaxes;

    var totalRounded = precisionRound(originalAmount, 2, roundAmount);

    var roundedAmount = precisionRound(precisionRoundNumber(originalAmount, 2, roundAmount) - originalAmount, 2);

    
    $('#tbxRoundedAmount').val(roundedAmount);
    $('#tbxGrandTotal').val(totalRounded);
    $("#tbxTotalItemsQuantity").val(precisionRound(totalQuantity, 2));
    
    if ($('#tbxStillToInvoice').length > 0) {
        $('#tbxStillToInvoice').val(precisionRound(totToInvoice, 2));
    }


    if (typeof onPaymentChange == 'function') {
        onPaymentChange();
    }
}

function getCurrency()
{
    var currid = $('#ContentPlaceHolder1_JobEditor1_ddlCurrency').val();
    if(currid == "" || currid == null)
    {
        $('#alertReminderCurrency').hide();
        return null;
    }
    
    if(currencies != null && currencies.length > 0)
    {
        for(var i=0; i<currencies.length; i++)
        {
            if(currencies[i].id == currid)
            {
                var cmsg = ressources['PricesAreInCurrency'];
                cmsg = cmsg.replace('$currency', currencies[i].symbol);
                $('#lblReminderCurrency').text(cmsg);
                $('#alertReminderCurrency').show();
                return currencies[i];
            }
        }
    }
    
}

getCurrency();

function applyCurrencyConversion(price)
{
    var c = getCurrency();
    if(c != null && c.rate != null)
    {
        return Math.round(price * c.rate * 100) / 100;
    }
    return price;
}

function findService(v) {

    var code = v.split('-')[0].trim();

    for (var i = 0; i < services.length; i++) {
        if ((services[i].internalId || "").toLowerCase().trim() == code.toLowerCase().trim()) {

            var ret = JSON.parse(JSON.stringify(services[i]));
            ret.unitPrice = applyCurrencyConversion(ret.unitPrice);

            return ret;
        }
        else if (services[i].name.toLowerCase().trim() == code.toLowerCase().trim()) {
            
            var ret = JSON.parse(JSON.stringify(services[i]));
            ret.unitPrice = applyCurrencyConversion(ret.unitPrice);

            return ret;
        }
    }


    return null;
}


function getRecurringInfo(recurringId){
    return {
        id: recurringId,
        isActive: $('#ContentPlaceHolder1_cbxIsActiveCheckBox').is(':checked'),
        frequencyType: +$('#ContentPlaceHolder1_ddlFrequencyType').val(),
        frequencyCount: +$('#ContentPlaceHolder1_ddlFrequencyCount').val(),
        specificDayCount: +$('#ContentPlaceHolder1_ddlSpecificDayCount').val(),
        specificMonthCount: +$('#ContentPlaceHolder1_ddlSpecificMonthCount').val(),
        daysInAdvance: +$('#ContentPlaceHolder1_ddlDaysInAdvance').val(),
        isLastDay: $('#ContentPlaceHolder1_cbxLastDayExecution').is(':checked'),
        isSpecificDay: $('#ContentPlaceHolder1_cbxSpecificDayExecution').is(':checked'),
        nextDueDate: $('#ContentPlaceHolder1_dtNextDueDate').val(),
        nextExecutionDate: $('#ContentPlaceHolder1_dtNextExecutionDate').val(),
        isUntilNotice: $('#ContentPlaceHolder1_cbxUntilNoticeDeadLine').is(':checked'),
        isSpecificMonth: $('#ContentPlaceHolder1_cbxUntilSpecificMonthDeadLine').is(':checked'),
        lastDueDate: $('#ContentPlaceHolder1_dtSpecificMonth').val(),
        invoiceSendingType: +$('#ContentPlaceHolder1_ddlSendingType').val(),
    }
}

function findTax(v) {
    var code = v.trim();
    for (var i = 0; i < taxes.length; i++) {
        if (taxes[i].name.toLowerCase() == code.toLowerCase()) {
            return taxes[i];
        }
    }
    return null;
}

function findTaxById(v) {


    for (var i = 0; i < taxes.length; i++) {

        if (taxes[i].id * 1 == v * 1) {
            return taxes[i];
        }
    }
    return null;
}

function precisionRound(number, precision, round = false) {
    /*var factor = Math.pow(10, precision);
    var ret = Math.round(number * factor) / factor;
    return ret.toFixed(precision) + " lapin";*/
    if (round)
        number = (Math.round(number * 20) / 20);
    var n = number.toFixed(precision);
    if (n == 0) return '0,00';
    return (
        n.replace('.', ',')
         .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ') 
    )
}

function precisionRoundNumber(number, precision, round = false) {
    /*var factor = Math.pow(10, precision);
    var ret = Math.round(number * factor) / factor;
    return ret.toFixed(precision) + " lapin";*/
    if (round)
        number = (Math.round(number * 20) / 20);

    return number.toFixed(precision);
}

function toDouble(txt) {

    if (typeof txt == "string") {


        txt = txt.replace(new RegExp(' ', 'g'), '');
        txt = txt.replace(new RegExp(',', 'g'), '.');
        txt = parseFloat(txt);
    }

    if (isNaN(txt)) txt = 0;
    if (txt == null) txt = 0;
    return txt;
}

String.prototype.format = function () {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};


$(function () {

    var noDefaultSort = true;
    var columns = [
        null,
        { "type": "date" },
        null,
    ];

    $.fn.dataTable.ext.type.order['date-pre'] = function (data) {
        return moment(data, CALANI.DEFAULTS.dateFormat).toDate();
    };

    initDataTable($('#ContentPlaceHolder1_JobEditor1_gridReports'), false, false, null, null, null, noDefaultSort, columns);
    initDataTable($('#ContentPlaceHolder1_JobEditor1_gridDeliveryNotes'), false, false, null, null, null, noDefaultSort, columns);

    // save button
    // ------------------------------------------------------------------------------------------------------------------------
    $('.btn-save').click(function () {
        var action = $(this).data('action');
        var lblDate = $(this).data('lblDate');
        lblDate = $('#' + lblDate);
        var buttonCall = $(this);

        if (dateCreation && (action == "sendinvoice" || action == "markinvoicesent") && delayInvoice) {

            var oldDateInvoice = moment();
            if ($('#ContentPlaceHolder1_JobEditor1_tbxInvoiceValidityDate').length == 1)
                oldDateInvoice = moment($('#ContentPlaceHolder1_JobEditor1_tbxInvoiceValidityDate').val(), momentDateFormat);

            var newDateInvouice = moment().add(delayInvoice, 'days');

            var message = ressources['UpdateInvoiceDate'].format(oldDateInvoice.format(momentDateFormat), newDateInvouice.format(momentDateFormat));



            if (newDateInvouice.isAfter(oldDateInvoice, 'day')) {
                swal({
                    title: ressources['Error'],
                    text: message,//ressources['UpdateInvoiceDate'],
                    type: "info",
                    showCancelButton: true,
                    cancelButtonText: ressources['DontUpdate'],
                    confirmButtonColor: "#4CAF50",
                    confirmButtonText: ressources['Update']

                })
                    .then((value) => {


                        if (value.value != null && value.value == true) {
                            saveProject(action, lblDate, buttonCall, true);
                        }
                        else {

                            saveProject(action, lblDate, buttonCall);
                        }
                    });
            }
            else
                saveProject(action, lblDate, buttonCall);
            

        }
        else
            saveProject(action, lblDate, buttonCall);
    });


    saveProject = function(action, lblDate, buttonCall, updateInvoiceDate = false) 
    {
        flagChanges(false);

        $(buttonCall).attr("disabled", true);

            var isQuote = false;
            if (window.location.toString().toLowerCase().indexOf("/quotes/") > 1) {
                isQuote = true;
            }

            if (action == "save") {
                if (window.location.toString().toLowerCase().indexOf("/deliverynotes/") > 1) {
                    action += ":deliverynote";
                }
                else if (window.location.toString().toLowerCase().indexOf("/quotes/") > 1) {
                    action += ":quote";
                }
                else if (window.location.toString().toLowerCase().indexOf("/opens/") > 1) {
                    action += ":open";
                }
                else if (window.location.toString().toLowerCase().indexOf("/invoices") > 1) {
                    action += ":invoice";
                }
                else if (window.location.toString().toLowerCase().indexOf("/recurringinvoices") > 1) {
                    action += ":recurringinvoice";
                }
            }

            var discount = $("#ContentPlaceHolder1_JobEditor1_tbxDiscount").val();
            if (discount == null || discount == "" || isNaN(discount)) discount = 0;

        if (projectId == null)
            projectId = -1;


            if (toNum($('#ContentPlaceHolder1_JobEditor1_ddlCustomer').val()) == null) {
                alert(ressources['MustSelectCustomer']);
                $(buttonCall).attr("disabled", false);
                return;
            }
            if (toNum($('#ContentPlaceHolder1_JobEditor1_ddlCustomerAddress').val()) == null) {
                alert(ressources['MustSelectAddress']);
                $(buttonCall).attr("disabled", false);
                return;
            }
            if (toNum($('#ContentPlaceHolder1_JobEditor1_ddlCustomerContact').val()) == null) {
                alert(ressources['MustSelectContact']);
                $(buttonCall).attr("disabled", false);
                return;
        }


        var cloneItems = { attachments: true, links: true, notes:true};

            if (action == "clone") 
            {
                $('#modal_form_cloneproject').modal('hide');
                
                var ttype = "quote";
                try {
                    ttype = $('#ddlCloneTargetType').val();
                }
                catch (e) { }

                if (ttype == null)
                    ttype = "quote";

                if (ttype == null) {
                    $(buttonCall).attr("disabled", false);
                    return;
                }

                if (ttype == null)
                    ttype = "quote";
                
                action += ":" + ttype;

                var cloneAttachments = $('#tbxCloneAttachments').is(':checked');
                cloneItems.attachments = cloneAttachments;

                var cloneLinks = $('#tbxCloneLinks').is(':checked');
                cloneItems.links = cloneLinks;

                var cloneNotes = $('#tbxCloneNotes').is(':checked');
                cloneItems.notes = cloneNotes;
                
            }

            var dataValidity = null;

        if ($('#ContentPlaceHolder1_JobEditor1_tbxValidityDate').length == 1)
            dataValidity = $('#ContentPlaceHolder1_JobEditor1_tbxValidityDate').val();

        if ($('#ContentPlaceHolder1_JobEditor1_tbxInvoiceValidityDate').length == 1)
            dataValidity = $('#ContentPlaceHolder1_JobEditor1_tbxInvoiceValidityDate').val();

        if ($('#ContentPlaceHolder1_JobEditor1_tbxProjectDeadLine').length == 1)
            dataValidity = $('#ContentPlaceHolder1_JobEditor1_tbxProjectDeadLine').val();

            var dataBatch = toNum($('#ContentPlaceHolder1_JobEditor1_tbxProjectVisitsForfait').val());
        if (dataBatch == "" || dataBatch < 1)
            dataBatch = null;

            var firstnote = null;
            if ($('#ContentPlaceHolder1_JobEditor1_tbxNewNote').length > 0) {
                firstnote = $('#ContentPlaceHolder1_JobEditor1_tbxNewNote').val();
            }

            var jobImports = [];
            var cbxJobImports = $('input.cbx-jobimport:checked');


        for (var i = 0; i < cbxJobImports.length; i++) {
                jobImports.push($(cbxJobImports[i]).data('id') * 1);
        }

        if (jobImports.length == 0)
            jobImports = null;

        var deliveryDate = $('#ContentPlaceHolder1_JobEditor1_tbxDeliveryDate').val();
        if (deliveryDate != '') {
            deliveryDate += ":00";
        }

        // Update positions
        var numRows = hot_ac_lazy_init.getSourceData().length;
        for (var rowIdx = 0; rowIdx < numRows; rowIdx++) {
            hot_ac_lazy_init.setSourceDataAtCell(hot_ac_lazy_init.toPhysicalRow(rowIdx), 'position', rowIdx + 1);
        }

            var projectUpdate = {
                culture: lang,
                id: projectId,
                action: action,
                validity: dataValidity,
                updateInvoiceDate: updateInvoiceDate || false,
                internalId: $('#ContentPlaceHolder1_JobEditor1_tbxInternalId').val(),
                clientId: toNum($('#ContentPlaceHolder1_JobEditor1_ddlCustomer').val()),
                clientContact: toNum($('#ContentPlaceHolder1_JobEditor1_ddlCustomerContact').val()),
                clientAddress: toNum($('#ContentPlaceHolder1_JobEditor1_ddlCustomerAddress').val()),
				linkedjobid: toNum($('#ContentPlaceHolder1_JobEditor1_ddlJob').val()) || 0,                
                items: tableDataSource,
                discount: discount,
                additionalEmployeeIds: $('#msAdditionalEmployees').val() ?? [],
                templateListIds: $('#msTemplateListIds').val() ?? [],
                rating: $('#ContentPlaceHolder1_JobEditor1_ratingStarsQuote_s').val(),
                description: $('#ContentPlaceHolder1_JobEditor1_tbxDescription').val(),
                meetingDescriptionText: $('#ContentPlaceHolder1_JobEditor1_tbxMeetingDescription').val(),
                batch: dataBatch,
                tsImports: timesheetrecordsimported,
                expImports: expensesrecordsimported,
                firstNote: firstnote,
                jobImports: jobImports,
                sendCopyEmailTo: $('#ContentPlaceHolder1_JobEditor1_tbxSendCopyEmailTo').val(),
                comment: $('#ContentPlaceHolder1_JobEditor1_tbxComment').val(),
                cloneItems: cloneItems,
                tmpFiles: $('#ContentPlaceHolder1_JobEditor1_hfFile').val(),
                assigneeId: toNum($('#ContentPlaceHolder1_JobEditor1_ddlAssignee').val()),
                deliveryDate: deliveryDate,
                color: $('#ContentPlaceHolder1_JobEditor1_hfColor').val(),
                allowInsertRow: true,
                invoicePaidDate: $('#invoice-payment-date-hdn').val(),
                customerReference: $('#ContentPlaceHolder1_JobEditor1_tbxCustomerReference').val(),
                typeIntervention: $('#ContentPlaceHolder1_JobEditor1_tbxTypeOfIntervention').val(),
                locationIntervention: $('#ContentPlaceHolder1_JobEditor1_tbxLocationOfIntervention').val(),
                discountType: parseInt($('#ddlDiscountType').val()),
                discountPercent: parseFloat($('#hdnDiscountPercentValidated').val() || '0'),
                discountType2: parseInt($('#ddlDiscountType2').val()),
                discountPercent2: parseFloat($('#hdnDiscountPercentValidated2').val() || '0'),
                discountType3: parseInt($('#ddlDiscountType3').val()),
                discountPercent3: parseFloat($('#hdnDiscountPercentValidated3').val() || '0'),
                discountType4: parseInt($('#ddlDiscountType4').val()),
                discountPercent4: parseFloat($('#hdnDiscountPercentValidated4').val() || '0'),
                skipMissingInvoiceNotif: (document.getElementById('ContentPlaceHolder1_cbxDontNotifyOnMissingInvoice') || { checked: false }).checked,
            };


            var paymentDates = [];
            if (typeof paymentsDataSource != 'undefined') {
                projectUpdate.payments = [];
                for (var i = 0; i < paymentsDataSource.length; i++) {
                    if (paymentsDataSource[i].datePayed != null && paymentsDataSource[i].amountPayed != null) {
                        if (paymentDates.indexOf(paymentsDataSource[i].datePayed) > -1) {

                            swal({
                                text: ressources2['Error_PaymentSameDateTwice'],
                                type: "warning",
                                showCancelButton: false,
                                confirmButtonColor: "#FF5722",
                                confirmButtonText: 'OK'
                            })

                        }
                        paymentDates.push(paymentsDataSource[i].datePayed);
                        projectUpdate.payments.push(paymentsDataSource[i]);

                    }

                }

            }

        var cleanItems = [];
        
            for (var i = 0; i < projectUpdate.items.length; i++) {
                if (projectUpdate.items[i].item) {


                    projectUpdate.items[i].qty = toDouble(projectUpdate.items[i].qty);
                    projectUpdate.items[i].price = toDouble(projectUpdate.items[i].price);
                    projectUpdate.items[i].discount = toDouble(projectUpdate.items[i].discount);
                    projectUpdate.items[i].totalprice = toDouble(projectUpdate.items[i].totalprice);
                    projectUpdate.items[i].serviceVatIncl = projectUpdate.items[i].serviceVatIncl ?? false;

                    cleanItems.push(projectUpdate.items[i]);
                }
            }
            projectUpdate.items = cleanItems;


            if (lblDate != null) {
                $(lblDate).removeClass("shake");
                $(lblDate).removeClass("animated");
            }


            projectUpdate.currency = null;
            var currentCurrency = getCurrency();
            if (currentCurrency != null && currentCurrency.id != null) {
                projectUpdate.currency = currentCurrency.id;
            }

            projectUpdate.linesToRemove = linesToRemove;

            let recurringId = $('#DateAndSendingSettingsContainer').data('recurring-id');
            if (recurringId != undefined) {
                projectUpdate.recurringUpdate = getRecurringInfo(recurringId);
            }

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '../../API/Service.svc/UpdateProject',
                data: JSON.stringify(projectUpdate),
                processData: false,
                dataType: "json",
                success: function (response) {


                    if (action == "importjobs") {
                        window.location = "../Invoices/Job.aspx?id=" + response.id;
                    }

                    if (!response.success) {
                        var msg = response.message;
                        if (ressources[msg] != null) msg = ressources[msg];

                        swal({
                            title: ressources['Error'],
                            text: msg,
                            type: "error",
                            confirmButtonColor: "#4CAF50",
                            confirmButtonText: ressources['OK']
                        })


                    }
                    else {

                        if (projectId == -1 && response.id != projectId) {
                            projectId = response.id;
                            projectUpdate.id = response.id;
                        }

                        if (lblDate != null) {
                            lblDate.html(ressources['Now'] + " (" + moment(new Date()).format('HH:mm:ss') + ")");
                            $(lblDate).addClass("shake");
                            $(lblDate).addClass("animated");

                            lblDate.removeClass('text-muted');
                            lblDate.addClass('text-bold');
                        }

                        // manage action result
                        if (afterJobSave != null)
                            afterJobSave(projectUpdate, response);

                        if (action == "previewpdf" || action.startsWith("previewpdf:")) {
                            var pdfOption = $('#ContentPlaceHolder1_ddlDeliveryNotePdfType').find(":selected").val();
                            var pdfOptionParam = pdfOption !== undefined ? '&deliveryNoteType=' + pdfOption : '';
                            let url = action === 'previewpdf:recurringinvoice' ? "../../Projects/PreviewPdf.ashx?q=" : "../PreviewPdf.ashx?q=";
                            window.open(url + projectId + "&c=" + lang4 + pdfOptionParam, "PreviewPdfPopup", "width=500,height=600");
                        }

                        if (action == "invoice-part") {

                            swal({
                                title: ressources['OpenPartInvoice'],
                                text: ressources['OpenPartInvoiceDescription'],
                                type: "success",
                                showCancelButton: true,
                                cancelButtonText: ressources['NoStayHere'],
                                confirmButtonColor: "#4CAF50",
                                confirmButtonText: ressources['YesOpenPartInvoice']
                            })

                                .then((value) => {


                                    if (value.value != null && value.value == true) {
                                        window.location = "../Invoices/Job.aspx?id=" + response.cloneId;
                                    }
                                    else {
                                        location.reload();
                                    }

                                });
                        }
                        else {


                            if (response.cloneId != null && response.cloneId > 1) {


                                swal({
                                    title: ressources['CloneDone'],
                                    text: ressources['DoYouWantToBeRedirectedTo'] + ' ' + response.cloneInternalId + ' ?',
                                    type: "success",
                                    showCancelButton: true,
                                    cancelButtonText: ressources['NoStayHere'],
                                    confirmButtonColor: "#4CAF50",
                                    confirmButtonText: ressources['YesGoToNew']
                                })

                                    .then((value) => {
                                        if (value.value != null && value.value == true) {
                                            if (action.indexOf(':quote') > 0) {
                                                window.location = "../../Projects/Quotes/Job.aspx?id=" + response.cloneId;
                                            }
                                            else if (action.indexOf(':project') > 0) {
                                                window.location = "../../Projects/Opens/Job.aspx?id=" + response.cloneId;
                                            }
                                            else if (action.indexOf(':invoice') > 0) {
                                                window.location = "../../Projects/Invoices/Job.aspx?id=" + response.cloneId;
                                            }
                                            else if (action.indexOf('save:recurringinvoice') > 0) {
                                                window.location = "../../Projects/RecurringInvoices/AddEdit.aspx?id=" + response.cloneId;
                                            }
                                            else if (action.indexOf(':deliverynote') > 0) {
                                                window.location = "../../Projects/DeliveryNotes/Job.aspx?id=" + response.cloneId;
                                            }
                                        }

                                    });



                            }
                        }




                        



                        if (action == "delete") {
                            window.location = "./";
                        }

                        if (action == "projectfinish") {
                            lblDate.html("<i class='icon-finish'></i> " + ressources['Finished']);
                            lblDate.removeClass('text-muted');
                            lblDate.addClass('text-bold');

                            
                        }
                        if (action == "projectsuspended") {
                            lblDate.html("<i class='icon-pause2'></i> " + ressources['On_hold']);
                            lblDate.removeClass('text-muted');
                            lblDate.addClass('text-bold');
                        }
                        if (action == "projectinprogress") {
                            lblDate.html("<i class='icon-play4'></i> " + ressources['Opened']);
                            lblDate.removeClass('text-muted');
                            lblDate.addClass('text-bold');
                        }
                        if (action == "invoicepaid") {
                            lblDate.html("<i class='icon-cash2'></i> " + ressources['Paid'] + "<br />" + projectUpdate.invoicePaidDate);
                            lblDate.removeClass('text-muted');
                            lblDate.addClass('text-bold');
                        }
                        if (action == "invoicepending") {
                            lblDate.html("<i class='icon-bell-check'></i> " + ressources['Pending']);
                            lblDate.removeClass('text-muted');
                            lblDate.addClass('text-bold');
                        }
                    }



                    $(buttonCall).attr("disabled", false);

                    if (response.data) {
                        // Read freshly uploaded attachments
                        var dataObj = $.parseJSON(response.data);

                        if (dataObj && dataObj.attachments) {
                            processNewAttachments(dataObj.attachments);
                        }

                        if (dataObj && dataObj.items) {
                            processNewItems(dataObj.items);
                        }
                        
                        
                    }

                },
                error: function (a, b, c) {
                    //alert(a.responseText);
                }
            });

        
    }

    function processNewAttachments(attachments) {
        if (attachments && attachments.length && attachments.length > 0) {

            for (var i in attachments) {
                var attachment = attachments[i];
                if ($("li#li-attachment-" + attachment.id).length == 0) {
                    attachmentsCollection.push(attachment);
                    $("ul#ulCloneAttachments").append('<li id="li-attachment-' + attachment.id + '"><input type="checkbox" class="control-success" value="' + attachment.id + '">' + attachment.title + '</li>');
                }
            }

            showHideAttachmentsSettingsPanel();
        }
    }

    function processNewItems(itemsUpdated) {
        
        if (itemsUpdated && itemsUpdated.length && itemsUpdated.length > 0) {
            tableDataSource = itemsUpdated.sort(function (a, b) { return a.position - b.position; });
            
            hot_ac_lazy_init.loadData(tableDataSource);
        }
    }
    // ------------------------------------------------------------------------------------------------------------------------>



    // clone button
    // ------------------------------------------------------------------------------------------------------------------------

    var clonerTargetType = "*";

    $('.btn-clone').on('click', function () {

        

        // option du buton type="*" ou type="quote/project/invoice"
        var type = $(this).data('type');
        if (type != null && type != '') clonerTargetType = type;

       
        $('#ddlCloneTargetType').val(clonerTargetType);
        $('#ddlCloneTargetType').prop('disabled', (clonerTargetType != "*"));

       
        var source = $('#ContentPlaceHolder1_JobEditor1_h5Title').html();
        $('#tbxCloneSource').val(source);

        
    });
    $('#modal_form_cloneproject').on('show.bs.modal', function () {
        //alert('onShow callback fired.')
    });


    // Discount
    // ------------------------------------------------------------------------------------------------------------------------>

    $('input.discount-percent').on('input', recalcTotals);

    // dropzone
    // ------------------------------------------------------------------------------------------------------------------------
    
    Dropzone.autoDiscover = false;
    $("#dropzone_multiple").dropzone({
        url: "../../UploadDocument.ashx?project=" + projectId,
        dictDefaultMessage: ressources['DropFilesToUpload'] + ' <span>' + ressources['OrClick'] + '</span>',
        maxFilesize: 30,
        init: function () {
            this.on("complete", function (file) {

                var val = $('#ContentPlaceHolder1_JobEditor1_hfFile').val();
                if (!val)
                    val = '[]';
                var parsedArr = JSON.parse(val);

                parsedArr.push(file.xhr.responseText);

                var sval = JSON.stringify(parsedArr);
                $('#ContentPlaceHolder1_JobEditor1_hfFile').val(sval);

            });
        }
    });
    // ------------------------------------------------------------------------------------------------------------------------>

    // ajax for dropdown lists dependances : ddlCustomer > customerAddress + customerContact
    // ------------------------------------------------------------------------------------------------------------------------
    $('#ContentPlaceHolder1_JobEditor1_ddlCustomer').on("change", function (e) {

        flagChanges(true);

        // clean lists
        var custId = $('#ContentPlaceHolder1_JobEditor1_ddlCustomer').val();
        $('#ContentPlaceHolder1_JobEditor1_ddlCustomerAddress').html('');
        $('#ContentPlaceHolder1_JobEditor1_ddlCustomerContact').html('');
        $('#ContentPlaceHolder1_JobEditor1_ddlJob').html('');
        // -->


        // clean imports  
        $('#gridImports').hide();
        $('#gridImpotsOnResults').hide();
        $('#gridImportsActions').hide();
        // ..>

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: '../../API/Service.svc/GetClientProperties',
            data: custId,
            processData: false,
            dataType: "json",
            success: function (response) {

                // restore selection:
                for (var i = 0; i < response.Addresses.length; i++) {
                    if (response.Addresses[i].id * 1 == selectedAddressId * 1) response.Addresses[i].selected = true;
                }

                for (var i = 0; i < response.Contacts.length; i++) {
                    if (response.Contacts[i].id * 1 == selectedContactId * 1) response.Contacts[i].selected = true;
                }
                for (var i = 0; i < response.Jobs.length; i++) {
                    if (response.Jobs[i].id * 1 == selectedLinkedJobId * 1) response.Jobs[i].selected = true;
                }
                // -->


                // rebind lists
                $('#ContentPlaceHolder1_JobEditor1_ddlCustomerAddress').select2({
                    minimumResultsForSearch: Infinity,
                    data: response.Addresses
                });

                $('#ContentPlaceHolder1_JobEditor1_ddlCustomerContact').select2({
                    minimumResultsForSearch: Infinity,
                    data: response.Contacts
                });
                $('#ContentPlaceHolder1_JobEditor1_ddlJob').select2({
                    minimumResultsForSearch: Infinity,
                    data: response.Jobs
                });
                // -->

            },
            error: function (a, b, c) {
                //alert(a.responseText);
            }
        });
    });
    // // ------------------------------------------------------------------------------------------------------------------------>


    //
    // Handsome table
    // ------------------------------------------------------------------------------------------------------------------------




    var servicesList = [];
    for (var i = 0; i < services.length; i++) {

        if (services[i].internalId != null && services[i].internalId != "") {
            servicesList.push(services[i].internalId + " - " + services[i].name);
        }
        else {
            servicesList.push(services[i].name);
        }
    }

    function getServicesList(query, process) {
        return servicesList;
    }

    
    function getTaxesList(query, process) {
        var currid = $('#ContentPlaceHolder1_JobEditor1_ddlCurrency').val();
        var taxesList;
        if (currid == "" || currid == null) {
            taxesList = [];
        }
        else
            taxesList = [""];

        for (var i = 0; i < taxes.length; i++) {
            taxesList.push(taxes[i].name);
        }

        return taxesList;
    }

    function getDefaultTaxName(query, process) {
        var defaultTax = taxes.filter(function (tax) {
            return tax.isdefault;
        });

        return defaultTax.length > 0 ? defaultTax[0].name : '';
    }

    if (tableDataSource.length < 1)
        for (var i = 0; i < 5; i++)
            tableDataSource.push({});

    // Define element
    var hot_ac_lazy = document.getElementById('hot_ac_lazy');

    $('#btnAddLine').click(function () {
        hot_ac_lazy_init.alter('insert_row_below', tableDataSource.length + 1);
    });

    var linesToRemove = [];

    var dtColHeaders = [
        ressources['Item'],
        ressources['Qty'],
        ressources['Unit_Price'],
        ressources['Discount_P'],
        ressources['Total'],
        ressources['Tax'],
        ''];
    
    let colCoef = 0;
    
    if (enableProjectFields || enableDeliveryNotesFields) colCoef += 1;
    if (!hideServiceCustomNumberColumn) colCoef += 1;
    if (!hideUnitColumn) colCoef += 2;
    
    let diffSize = colCoef > 0;
    
    var dtColWidths = [ 12 - colCoef, 5, 4, 4, 5, 5, diffSize ? 3 : 1.5, diffSize ? 3 : 1.5];
    var dtColumns = [
        {
            data: 'item',
            editor: htMobileDropdown(),
            selectOptions: getServicesList,
            source: function (query, process) {
                const options = getServicesList(query);
                process(Array.isArray(options) ? options : []);
            },
            type: 'dropdown',
            strict: false,
            allowInvalid: true,
            readOnly: isReadOnly
        },
        {
            data: 'qty',
            type: 'numeric',
            className: 'htRight',
            readOnly: isReadOnly,
            format: '0,0.00',
            numericFormat: {
                pattern: '0,0.00',
                culture: 'fr-FR' // this is the default culture, set up for USD
            }
        },
        {
            data: 'price',
            type: 'numeric',
            className: 'htRight',
            format: '0,0.00',
            numericFormat: {
                pattern: '0,0.00',
                culture: 'fr-FR' // this is the default culture, set up for USD
            },
            readOnly: isReadOnly

        },
        {
            data: 'discount',
            type: 'numeric',
            className: 'htRight',
            format: '0,0.00',
            numericFormat: {
                pattern: '0,0.00',
                culture: 'fr-FR' // this is the default culture, set up for USD
            },
            readOnly: isReadOnly
        },
        {
            data: 'totalprice',
            type: 'numeric',
            className: 'htRight',
            format: '0,0.00',
            numericFormat: {
                pattern: '0,0.00',
                culture: 'fr-FR' // this is the default culture, set up for USD
            },
            readOnly: true
        },
        {
            data: 'tax',
            //type: 'dropdown',
            //source: taxesList,
            type: 'dropdown',
            editor: htMobileDropdown(),
            selectOptions: getTaxesList,
            source: function (query, process) {

                process(getTaxesList(query, process));
            },
            strict: true,
            readOnly: isReadOnly
        },
        {
            renderer:
                function (instance, td, row, col, prop, value, cellProperties) { 
                    if (!isReadOnly) {
                        var div;
                        // Remove existing buttons to avoid duplicating them.
                        $(td).children('.delbtn').remove();

                        div = document.createElement('div');
                        div.className = 'delbtn';
                        div.style.cursor = 'hand';
                        div.style.textAlign = 'center';
                        div.style.verticalAlign = 'middle';
                        //div.appendChild(document.createTextNode('x'));
                        delico = $('<i class="fa fa-trash" aria-hidden="true"></i>');
                        $(div).html(' ');
                        $(div).append(delico);
                        td.appendChild(div);

                        $(div).on('mouseup', function () {
                            if (confirm(ressources.SureToDeleteLine)) {
                                // Save deleted row's id before it gets removed from table data source
                                var rowIdx = hot_ac_lazy_init.toPhysicalRow(row);
                                if (tableDataSource[rowIdx].id != null) {
                                    linesToRemove.push(tableDataSource[rowIdx].id);
                                }

                                instance.alter("remove_row", row);                                
                                instance.render();
                                recalcTotals();
                                return true;
                            }
                            else {
                                return false;
                            }
                        });
                    }
                    return td;
                },
            readOnly: true
        },
        {
            data: 'position',
            type: 'numeric',
        },

    ];

    let columnIndex = 0;
    if (enableProjectFields || enableDeliveryNotesFields) {
        dtColHeaders.splice(columnIndex, 0, ' ');
        dtColWidths.splice(columnIndex, 0, diffSize ? 3 : 2);
        dtColumns.splice(columnIndex++, 0, {
            data: 'selected',
            type: 'checkbox',
            className: 'media-middle',
            renderer: function (instance, td, row, col, prop, value, cellProperties) {
                $(td).empty();
                let rowData = tableDataSource[row];
                let hideCbx = rowData && (rowData.isDelivered || enableDeliveryNotesFields)
                    && rowData.toinvoice != null && rowData.toinvoice >= 1;
                
                if (!hideCbx){
                    let cbx = document.createElement('input');
                    cbx.type = 'checkbox';
                    cbx.className = 'htCheckboxRendererInput';
                    cbx.style.margin = '2px';
                    cbx.disabled = cellProperties.readOnly;
                    cbx.tabIndex = -1;
                    cbx.checked = value;
                    td.appendChild(cbx);

                    cbx.addEventListener('change', function () {
                        if (rowData) {
                            rowData.selected = cbx.checked;
                            instance.setDataAtRowProp(row, 'selected', cbx.checked);
                        }
                    });
                }

                if (rowData && rowData.isDelivered) {
                    let icon = document.createElement('i');
                    icon.className = 'icon-box delivered-icon';
                    icon.title = ressources['AlreadyDelivered'];
                    icon.style.color = 'rgb(76, 175, 80)';
                    icon.style.margin = '2px';
                    icon.style.cursor = 'pointer';
                    td.appendChild(icon);
                }
                if (rowData && rowData.toinvoice != null && rowData.toinvoice >= 1) {
                    let icon = document.createElement('i');
                    icon.className = 'icon-file-check2 toinvoce-icon';
                    icon.title = ressources['AlreadyInvoiced'];
                    icon.style.color = 'rgb(76, 175, 80)';
                    icon.style.margin = '2px';
                    icon.style.cursor = 'pointer';
                    td.appendChild(icon);
                }
                td.style.textAlign = 'center';
                td.style.verticalAlign = 'middle';
                return td;
            }
        });
    }
    if (!hideServiceCustomNumberColumn) {
        // Add service custom number column
        dtColHeaders.splice(columnIndex, 0, '#');
        dtColWidths.splice(columnIndex, 0, 3);
        dtColumns.splice(columnIndex++, 0, {
            data: 'customNumber',
            type: 'text',
            validator: function (value, callback) {
                // Allow max 10 symbols
                setTimeout(function () {
                    if (/^[\w\W]{0,10}$/.test(value)) {
                        callback(true);
                    } else {
                        callback(false);
                    }
                }, 100);
            },
            allowInvalid: false
        });
    }

    if (!hideUnitColumn) {
        columnIndex+=1;
        dtColHeaders.splice(columnIndex, 0, ressources2['Unit']);
        dtColWidths.splice(columnIndex, 0, 4);
        dtColumns.splice(columnIndex, 0, { data: 'unitName', className: 'htRight', });
    }
    
    var hotOptions = {
        licenseKey: 'non-commercial-and-evaluation',
        data: tableDataSource,
        afterChange: onChange,
        stretchH: 'all',
        renderAllColumns: true,
        colHeaders: dtColHeaders,
        rowHeight: function (row) {
            return 30;
        },
        defaultRowHeight: 30,
        manualRowMove: true,
        rowHeaders: true,
        columns: dtColumns,
        colWidths: dtColWidths,
        hiddenColumns: {
            // Hide Position column (the last one)
            columns: [dtColumns.length - 1],
        },
        cells: function (row, col, prop) {
            var cellProperties = {};

            var todisable = false;
 
            if (enableProjectFields) {
                var record = tableDataSource[row];
                
                if (isReadOnlyMode || record != null
                    && record.toinvoice != null 
                    && record.toinvoice >= 1 
                    && (record.isDelivered || disableInvoicedOnly)) 
                {
                    todisable = true;
                }
                
                cellProperties.readOnly = todisable;
            }
            if (enableDeliveryNotesFields) {
                var record = tableDataSource[row];

                if (isReadOnlyMode || record != null
                    && record.toinvoice != null
                    && record.toinvoice >= 1)
                    todisable = true;

                cellProperties.readOnly = todisable;
            }

            return cellProperties;
        },
    };

    // Initialize with options
    hot_ac_lazy_init = new Handsontable(hot_ac_lazy, hotOptions);
    
    hot_ac_lazy_init.addHook('afterChange', function (values) {
        if (!values || !values.length) return;
        
        var rowIdx = values[0][0];
        var colName = values[0][1];
        var prevValue = values[0][2];
        var newValue = values[0][3];
                
        // If price was set from empty to non-empty and tax is empty - set tax to default
        if (colName == 'price' && (prevValue == null || prevValue == '') && newValue != null && newValue != '') {
            var tax = hot_ac_lazy_init.getDataAtRowProp(rowIdx, "tax");
            if (tax == null || tax == '') {
                hot_ac_lazy_init.setDataAtRowProp(rowIdx, "tax", getDefaultTaxName());
            }
        }
    });
    
    function isChecked() {
        var res = true;
        for (var i = 0, ilen = tableDataSource.length; i < ilen; i++) {
            if (!tableDataSource[hot_ac_lazy_init.toPhysicalRow(i)].isChecked) {
                res = false;
                return;
            }
        }

        $('input.checker').prop("checked", res);
    }

    $(hot_ac_lazy).on('mouseup', 'input.checker', function (event) {
        var current = !$('input.checker').is(':checked'); //returns boolean
        for (var i = 0, ilen = tableDataSource.length; i < ilen; i++) {
            tableDataSource[hot_ac_lazy_init.toPhysicalRow(i)].isChecked = current;
        }
        hot_ac_lazy_init.render();
        isChecked();
    });

   

    isChecked();

    function onChange(changes, source) {
        flagChanges(true);
        if (!changes) {
            return;
        }
        var instance = this;

        var currid = $('#ContentPlaceHolder1_JobEditor1_ddlCurrency').val();
        
        changes.forEach(function (change) {
            var row = change[0];
            var col = change[1];
            var newValue = change[3];
            var applyChange = false;
            var datarow = tableDataSource[hot_ac_lazy_init.toPhysicalRow(row)];

            if (datarow.isDelivered == null)
                datarow.isDelivered = false;

            if (datarow.isChecked == null)
                datarow.isChecked = false;
            
            if (col == "item") {
                var service = findService(newValue);
                if (service != null)
                {
                    datarow.serviceId = service.id;
                    datarow.priceInclVat = service.priceInclVat;
                    datarow.price = service.unitPrice;
                    datarow.unitName = service.unitName;
                    
                    if (currid == "" || currid == null) {
                        var tax = findTaxById(service.taxId);

                        if (tax != null) {
                            datarow.tax = tax.name;
                        }

                        if (usePricesIncludingVat){
                            datarow.serviceVatIncl = service.priceInclVat;
                            datarow.price = service.priceInclVat
                                ? service.unitPrice
                                : service.unitPrice + (service.unitPrice * tax.rate / 100);
                        }
                    }

                    applyChange = true;
                }
            }

            if (col == "qty" || col == "price" || col == "item" || col == "discount")
            {
                
                if (isNaN(datarow.qty) || isNaN(datarow.price)) {
                    datarow.totalprice = null;
                }
                else
                {
                    datarow.totalprice = toDouble(datarow.price) * toDouble(datarow.qty);
                    if (datarow.discount == null || isNaN(datarow.discount)) datarow.discount = 0;
                    datarow.totalprice = precisionRound(toDouble(datarow.totalprice) * (100 - toDouble(datarow.discount)) / 100, 2);
                }
                
                applyChange = true;
            }

            if ((enableProjectFields || enableDeliveryNotesFields) && col == "selected") {
                // activer les controles de facturation partiels
                toggleSelection();
            }

            if (applyChange && instance.render != null) {
                instance.render();
            }

            if (datarow.qty == null)
                datarow.qty = 1;

            if (datarow.position == null)
                datarow.position = 0;

            if (datarow.isDelivered == null)
                datarow.isDelivered = false;

            if (datarow.rate == null)
                datarow.rate = 0;

            if (datarow.serviceVatIncl == null)
                datarow.serviceVatIncl = false;

            if (datarow.isDelivered == null)
                datarow.isDelivered = false;

            recalcTotals();
            
        });
    }

$('#ContentPlaceHolder1_JobEditor1_ddlCurrency').change(function(){

    /* auto refresh currencies */
    
    
    var recalcs = 0;
   
    for (var i = 0; i < tableDataSource.length; i++) 
    {
        var rowIdx = hot_ac_lazy_init.toPhysicalRow(i);
        if (tableDataSource[rowIdx].item != null && tableDataSource[rowIdx].item != '')
        {
            var changes = [];
            changes.push([rowIdx, "item", undefined, tableDataSource[rowIdx].item]);
            onChange(changes, "edit");
            recalcs++;
        }
    }

    if(recalcs > 0)
    {
        hot_ac_lazy_init.render();
    }

});

    
    function toggleSelection() {
        let seletedForInvoice = 0;
        let seletedForDN = 0;
        for (var i = 0; i < tableDataSource.length; i++) {
            let item = tableDataSource[hot_ac_lazy_init.toPhysicalRow(i)];
            
            if (item.selected != null && item.selected === true) {
                if (item.toinvoice === null) seletedForInvoice++;
                if (item.isDelivered === false && !enableDeliveryNotesFields) seletedForDN++;
            }
        }
        
        $('#btnInvoiceSelectionCount').html('(' + seletedForInvoice + ')');
        $('#btnDeliveryNoteSelectionCount').html('(' + seletedForDN + ')');
        
        if (seletedForInvoice > 0) $('#btnInvoiceSelection').show();
        else $('#btnInvoiceSelection').hide();
        
        if (seletedForDN > 0) $('#btnDeliveryNoteSelection').show();
        else $('#btnDeliveryNoteSelection').hide();
        
    }

    recalcTotals();
    $('#ContentPlaceHolder1_JobEditor1_tbxDiscount').change(function () {
        recalcTotals();
    });
    $('#ContentPlaceHolder1_JobEditor1_tbxDiscount').keyup(function () {
        recalcTotals();
    });
    // ------------------------------------------------------------------------------------------------------------------------>



    function appendNote(autor, date, message) {

        var letter = '?';
        letter = autor.substring(0, 1);

        // <img src="../../assets/images/placeholder.jpg" class="img-circle img-md" alt="">

        var divNote = $('<li class="media"></div>');
        divNote.append($('<div class="media-left"><span class="bigletter">' + letter + '</span></div>'));
        var divNote2 = $('<div class="media-body"></div>');
        divNote.append(divNote2);
        var divMH = $('<div class="media-heading"></div>');
        divNote2.append(divMH);
        var nA = $('<span class="text-semibold"></span>');
        divMH.append(nA);
        nA.append(document.createTextNode(autor));
        var nD = $('<span class="media-annotation pull-right"></span>');
        nD.append(document.createTextNode(date));
        divMH.append(nD);
        divNote2.append(document.createTextNode(message));


        $('#panelNotes').append(divNote);
    }

    function renderNotes() {
        if (notes != null && notes.length > 0) {

            for (var i = 0; i < notes.length; i++) {
                appendNote(notes[i].authorName, notes[i].dateWritten, notes[i].content);
            }

            $('#ContentPlaceHolder1_JobEditor1_lblCountNotes').html(notes.length);
            $('#ContentPlaceHolder1_JobEditor1_lblCountNotes').show();
        } else {
            $('#ContentPlaceHolder1_JobEditor1_lblCountNotes').hide();

            $('#ContentPlaceHolder1_JobEditor1_Div1').addClass('panel-collapsed');
            $('#ContentPlaceHolder1_JobEditor1_Div1 .panel-body').hide();
            
        }
    }


    $('#btnAddNote').click(function (e) {
        var msg = $('#ContentPlaceHolder1_JobEditor1_tbxNewNote').val();

        if (projectId < 1) {
            alert("Your need to save the project first");
            return;
        }

        if (msg != '' ) {

            var newnote = {
                "authorName": ressources['Me'],
                "dateWritten": ressources['Now'] + ' !',
                content: msg,
                jobId: projectId
            }
            notes.push(newnote);
            appendNote(newnote.authorName, newnote.dateWritten, newnote.content);


            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '../../API/Service.svc/SendProjectNote',
                data: JSON.stringify(newnote),
                processData: false,
                dataType: "json",
                success: function (response) {


                    if (!response.success) {
                        alert("retry later");
                    }

                },
                error: function (a, b, c) {
                    alert(a.responseText);
                }
            });

            var objDiv = document.getElementById("panelNotes");

            objDiv.scrollTop = objDiv.scrollHeight;

            $('#ContentPlaceHolder1_JobEditor1_lblCountNotes').html(notes.length);
            $('#ContentPlaceHolder1_JobEditor1_tbxNewNote').val('');
        }
    });

    renderNotes();

});



function toNum(v) {
    if (v == null || v == '') return null;
    v = v * 1;
    if (isNaN(v)) v = null;
    return v;
}

function refreshDiscountPercent() {
    var discountsSuffices = ['', '2', '3', '4'];
    var totalExclTax = parseFloat($("#hdnTotalExclTaxBeforeDiscount").val());
    for (var i in discountsSuffices) {
        var suffix = discountsSuffices[i];

        var value = $('input#tbxDiscountPercent' + suffix).val();
        var percent = parseFloat(value);
        var isValid = percent >= 0 && percent <= 100;

        if (isValid) {
            $('input#tbxDiscountPercent' + suffix).removeClass('border-danger');
            var discount = calculateDiscount(percent, totalExclTax);
            $('#tbxDiscountAmount' + suffix).val(discount);
            $('#hdnDiscountPercentValidated' + suffix).val(percent);

            totalExclTax -= discount;
        }
        else {
            $('input#tbxDiscountPercent' + suffix).addClass('border-danger');
            $('#tbxDiscountAmount' + suffix).val('');
            $('#hdnDiscountPercentValidated' + suffix).val(0);
        }
    }
}

function getDiscountPercent(ordinal) {
    var percent = 0;
    var isValid = true;
    ordinal = (1 * ordinal) || 1;
    var suffices = ['', '2', '3', '4'];

    var value = $('input#tbxDiscountPercent' + suffices[ordinal - 1]).val();
    percent = parseFloat(value);
    isValid = percent >= 0 && percent <= 100;

    return isValid ? percent : 0;
}

function calculateDiscount(percent, totalExclTax) {
    if (percent == 0) return 0;

    var discount = Math.round(totalExclTax * percent) / 100;
    return discount;
}

function calculateAllDiscounts(totalExclTax) {
    var totalDiscount = 0;
    var percent = 0;
    var discount = 0;

    // We have 4 discounts applied subsequently one after another
    for (var i = 1; i <= 4; i++) {
        percent = getDiscountPercent(i);
        discount = calculateDiscount(percent, totalExclTax);

        totalDiscount += discount;
        totalExclTax -= discount;
    }

    return totalDiscount;
}

function switchDiscountFeature(enable) {
    enable = enable ? true : false;
    $('select.discount-type').attr('disabled', !enable);
    if (!enable) $('input.discount-percent,input.discount-amount').val(0);
    $('input.discount-percent').attr('disabled', !enable);
    $('.discount-feature-container').attr('title', enable ? '' : ressources2['FeatureDisabledIfMultipleVAT']);
    refreshDiscountPercent();
}

function showAdditionalDiscount() {
    var hiddenDiscounts = $(".additional-discount-container.collapse");

    if (hiddenDiscounts.length == 0) return;

    if (hiddenDiscounts.length == 1) {
        $("#add-discount-line-button").addClass("collapse");
    }
    
    $(hiddenDiscounts[0]).removeClass("collapse");
}
    //
    // Attachment
    // ------------------------------------------------------------------------------------------------------------------------

$('.btnDeleteAttachment').click(function(){


    var btn = $(this);
    var tr = btn.closest('tr');
    var id = btn.data('attachment') * 1;
    var attname = btn.data('name');



    swal({
        title: ressources['DeleteAttachment'],
        text: ressources['DoYouWantToDeleteAttachment'] + ': ' + attname + ' ?',
        type: "success",
        showCancelButton: true,
        cancelButtonText: 'No',
        confirmButtonColor: "#4CAF50",
        confirmButtonText: 'Yes'
        })
        .then((value) => {
            if (value.value != null && value.value == true) 
            {
                var pa = {ProjectId: projectId, AttachmentId: id};          

                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: '../../API/Service.svc/DeleteAttachment',
                    data: JSON.stringify(pa),
                    processData: false,
                    dataType: "json",
                    success: function (response) {

                        tr.hide();                        
                        onAttachmentDeleted(id);
                    },
                    error: function (a, b, c) {
                    
                    }
                });

            }
        });

    

});

function onAttachmentDeleted(id) {
    attachmentsCollection = $.grep(attachmentsCollection, function (a) { return a.id != id; });
    $("li#li-attachment-" + id + " input[type=checkbox]").prop('checked', false);
    $("li#li-attachment-" + id).css("display", "none");
    showHideAttachmentsSettingsPanel();
}

function countAttachments() {
    if (typeof (attachmentsCollection) != 'object' || !attachmentsCollection.length) {
        return 0;
    }
    return attachmentsCollection.length;
}

function checkAllAttachmentCheckboxes() {
    if ($("#cbxWorkflowNextIncludeAllAttachments").is(":checked")) {
        $("ul#ulCloneAttachments input[type=checkbox]").prop("checked", "true");
    }
}

function showHideAttachmentsSettingsPanel() {
    var display = countAttachments() > 0 ? "block" : "none";
    $("#rowWorkflowAttachmentsPanel").css("display", display);
}


function init () {
    if (isReadOnlyMode == "true") {

        $("#ctl00 :input ").prop("disabled", true);
        $("#ctl00 :button ").hide();
        
        $("#ctl00 :textarea").prop("disabled", true);    
       
    }

    showHideAttachmentsSettingsPanel();
}

init();

$(function () {
    $('#msAdditionalEmployees').select2({
        placeholder: "",
        minimumResultsForSearch: Infinity,
        closeOnSelect: false,
        data: additionalEmployeesDataJson.map(item => ({ id: item.id, text: item.Description })),
    });
    
    $('#msAdditionalEmployees').val(selectedEmployeeIds).trigger('change');
    
    $('#msTemplateListIds').select2({
        placeholder: "", minimumResultsForSearch: Infinity, closeOnSelect: false,
        data: templateListIdsDataJson.map(item => ({ id: item.templateId, text: item.listName })),
    });
    $('#msTemplateListIds').val(selectedTemplateListIds).trigger('change');
})

function onNoteQtyChange(ie){
    let val = ie.value;
    let $nr = $('#noteRemainder');
    let max = $nr.data('max');

    if (val <= 0) val = max;
    
    let res = max - val;

    $(ie).val(res >= 0 ? val : max);
    $nr.val(res >= 0 ? res : 0);
}

var hotDN;
function openDeliveryNoteDialog(){
    let selectedItems = tableDataSource.filter(item => item.selected === true && !item.isDelivered);
    let reference = $('#ContentPlaceHolder1_JobEditor1_tbxInternalId').val();
    
    swal({
        title: ressources['CreateDeliveryNote'],
        html: `
            <div class="row" style="display: flex; justify-content: center; align-items: center; margin: 1em 0 1em 0">
                <h5 style="margin-right: 15px"> ${ressources['RefNr']}: </h5> 
                <input style="width: unset; margin: 0" class="swal2-input" id="reference" value="${reference}_${ressources['DN']}"></input> 
            </div>
            <div id="createDNContainer" style="width:100%; height:300px;"></div>
        `,
        onOpen: function() {
            const data = selectedItems.map(item => [item.item, item.qty, item.qty, 0, item.id]);
            const container = document.getElementById('createDNContainer');
            hotDN = new Handsontable(container, {
                data: data,
                colHeaders: [ressources['Item'], ressources['PlannedQuantity'], ressources['SelectedQuantity'], ressources['RemainingQuantity']],
                colWidths: [200, 100, 100, 100],
                columns: [
                    { data: 0, type: 'text', className: 'htLeft', readOnly: true},
                    { data: 1, type: 'numeric', readOnly: true}, 
                    { data: 2, type: 'numeric', readOnly: false},
                    { data: 3, type: 'numeric', readOnly: true}, 
                    { data: 4, type: 'numeric', readOnly: true}  
                ],
                hiddenColumns: {columns: [4]},
                afterChange: (changes, source) => {
                    if (source === 'loadData') return;

                    changes.forEach(([row, prop, oldValue, newValue]) => {
                        if (oldValue !== newValue && prop === 2) {
                            let planned = hotDN.getSourceDataAtRow(row)[1];
                            if (newValue > planned) newValue = planned;

                            let remained = planned - newValue;
                            hotDN.setDataAtCell(row, 2, newValue);
                            hotDN.setDataAtCell(row, 3, remained);
                        }
                    });
                },
                rowHeaders: true, width: '100%', height: 300, stretchH: 'all', licenseKey: 'non-commercial-and-evaluation'
            });
        },
        type: "info",
        showCancelButton: true,
        cancelButtonText: ressources['Cancel'],
        confirmButtonText: ressources['Create'],
        customClass: 'dn-create-swal'
    })
    .then((res) => {
        if (res.value === true){
            reference = $('#reference').val() === "" ? `${reference}_DN` :  $('#reference').val();
            const tableData = hotDN.getData();
            const formattedData = tableData.map(row => ({
                jobId: projectId,
                id: row[4],
                qty: row[2],
                remainderQty: row[3],
                reference: reference
            }));

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '../../API/Service.svc/CreateDeliveryNoteFromService',
                data: JSON.stringify(formattedData),
                dataType: "json",
                success: function (response) {
                    if (response.success) {

                        swal({
                            title: ressources['NewDeliveryNote'],
                            text: ressources['OpenDeliveryNoteDescription'],
                            type: "success",
                            showCancelButton: true,
                            cancelButtonText: ressources['NoStayHere'],
                            confirmButtonColor: "#4CAF50",
                            confirmButtonText: ressources['YesOpenDeliveryNote']
                        })
                        .then((value) => {
                            window.onbeforeunload = null;
                            if (value.value != null && value.value == true) {
                                window.location.href = "../../Projects/DeliveryNotes/Job.aspx?id=" + response.id;
                            }
                            else {
                                location.reload();
                            }
                        });
                    } else {
                        alert(ressources['Validation_Error']);
                    }
                },
                error: function (e) {
                    alert(e.responseText);
                }
            });
        }
    });
}