﻿using Calani.BusinessObjects.Attachments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Calani.BusinessObjects.DocGenerator;

public partial class Projects_SendScreen : System.Web.UI.UserControl
{
    public long? ProjectId { get; set; }
    public long? Selection { get; set; }

    public string ContactsJson { get; set; }
    public string DefaultTextJson { get; set; }

    public string SelectedEmail { get; set; }

    public string AttachmentName { get; set; }
    PageHelper ph;




    protected void Page_Load(object sender, EventArgs e)
    {
        ph = new PageHelper(this.Page);
        ph.DefineLanguage();

        Calani.BusinessObjects.Membership.LoginManager llmgr = new Calani.BusinessObjects.Membership.LoginManager();
        if(llmgr.IsEmailValidated(ph.CurrentUserId) == false)
        {
            Response.Redirect("~/");
            return;

        }

        // auto detect id from url if ProjetId property is not set
        if (ProjectId == null && Request.Params["id"] != null)
        {
            long id = -1;
            if (long.TryParse(Request.Params["id"], out id))
            {
                ProjectId = id;
            }

            
        }
        if (ProjectId == null)
        {
            Response.Redirect(".");
            return;
        }

        if(Request.Params["sel"] != null)
        {
            long id = -1;
            if (long.TryParse(Request.Params["sel"], out id))
            {
                Selection = id;
            }
        }
        // -->

        Calani.BusinessObjects.Attachments.AttachmentsManager mgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
        mgr.OrganizationId = ph.CurrentOrganizationId;
        var ds = mgr.ListJobAttachments(this.ProjectId.Value);
        attachmentsExtended pdfDoc = Selection.HasValue ? ds.FirstOrDefault(a => a.id == Selection.Value) : null;

        List<int> attachmentsTypes = Calani.BusinessObjects.Attachments.AttachmentsManager.GetNonGeneratedTypes();
        var gridDs = (from r in ds where r.type != null && attachmentsTypes.Contains(r.type.Value) orderby r.date descending select r).ToList();

        var ddlAttachmnetsDs = new List<attachmentsExtended>();
        if (pdfDoc != null) ddlAttachmnetsDs.Add(pdfDoc);
        ddlAttachmnetsDs.AddRange(gridDs);
        ddlAttachments.DataSource = ddlAttachmnetsDs;
        ddlAttachments.DataTextField = "NameWithExtension";
        ddlAttachments.DataValueField = "id";
        ddlAttachments.DataBind();
        var selection = (from r in ds where r.id == Selection select r).FirstOrDefault();
        
        panelOtherDocs.Visible = gridDs.Count > 0;
        gridFiles.DataSource = gridDs;
        gridFiles.DataBind();
        new Html5GridView(gridFiles, false, null);

        // bind selection panel
        if(selection == null)
        {
            panelSelection.Visible = false;
            AttachmentName = "";
        }
        else
        {
            panelSelection.Visible = true;
            lblSelFileName.InnerText = selection.name;
            if (selection.date != null) lblSelFileDate.InnerText = Convert2.DateToStr(selection.date.Value);
            icon.Attributes["class"] += " " + selection.Icon;
            linkDownload.HRef = "../DownloadDocument.ashx" + "?id=" + selection.id + "&t=" + selection.FileNameNoExtension;
            linkOpen.HRef = "../DownloadDocument.ashx" + "?id=" + selection.id + "&a=0" + "&t=" + selection.FileNameNoExtension;
            AttachmentName = selection.NameWithExtension;
        }


        // info for email
        Calani.BusinessObjects.Projects.ProjectsManager prjectmgr = new Calani.BusinessObjects.Projects.ProjectsManager(ph.CurrentOrganizationId);
        Calani.BusinessObjects.Projects.jobsExtended info = prjectmgr.GetInfo(ProjectId.Value);


        string templateSubject = "";
        string templateBody = "";

        Calani.BusinessObjects.Membership.LoginManager lmgr = new Calani.BusinessObjects.Membership.LoginManager();
        var user = lmgr.GetUser(HttpContext.Current.User.Identity.Name);
        Calani.BusinessObjects.Settings.DocumentTemplateManager tplmgr = new Calani.BusinessObjects.Settings.DocumentTemplateManager(user.id, System.Threading.Thread.CurrentThread.CurrentUICulture);

        var vars = ph.GetVariablesForTemplates();

        string civTitle = "";
        try
        {
            civTitle = info.contacts.civtitle;
            if (String.IsNullOrWhiteSpace(civTitle)) civTitle = "Mr";
            civTitle = "Title_" + civTitle;
            civTitle = ResourceHelper.Get(civTitle);
        }
        catch
        {
            civTitle = Resources.Resource.Title_Mr;
        }


        vars.Add(new JsVariableForTemplate { name = "docnumber", value = info.internalId });
        //vars.Add(new JsVariableForTemplate { name = "contactfn", value = info.contacts.firstName });
        //vars.Add(new JsVariableForTemplate { name = "contactln", value = info.contacts.lastName });
        //vars.Add(new JsVariableForTemplate { name = "contacttitle", value = civTitle });

        Calani.BusinessObjects.Settings.DocumentTemplateInfo tpl = null;

        long org = PageHelper.GetCurrentOrganizationId(Page.Session);
        Calani.BusinessObjects.CustomerAdmin.OrganizationManager orgmgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(org);
        orgmgr.Load();


        if (info.ProjectStatus.ToString().StartsWith("Quote") || info.ProjectStatus.ToString().StartsWith("DeliveryNote"))
        {
            if (orgmgr.BeforeInvoice == "DeliveryNote" || info.ProjectStatus.ToString().StartsWith("DeliveryNote"))
            {
                vars.Add(new JsVariableForTemplate { name = "doctype", value = Resources.Resource.Quote });
                vars.Add(new JsVariableForTemplate { name = "docnr", value = info.internalId });
                //vars.Add(new JsVariableForTemplate { name = "expdate", value = Convert2.DateToShortStr(info.dateQuoteValidity) });

                tbxSubject.Text = Resources.Resource.DeliveryNote;
                tpl = tplmgr.Get("SendDeliveryNote");
            }
            else
            {
                vars.Add(new JsVariableForTemplate { name = "doctype", value = Resources.Resource.Quote });
                vars.Add(new JsVariableForTemplate { name = "docnr", value = info.internalId });
                vars.Add(new JsVariableForTemplate { name = "expdate", value = Convert2.DateToShortStr(info.dateQuoteValidity) });

                tbxSubject.Text = Resources.Resource.Quote;
                tpl = tplmgr.Get("SendQuote");
            }
        
            
        }
        else if (info.ProjectStatus.ToString().StartsWith("Job"))
        {
            tbxSubject.Text = Resources.Resource.Job;
            tpl = tplmgr.Get("SendProject");

            vars.Add(new JsVariableForTemplate { name = "doctype", value = Resources.Resource.Project });
            vars.Add(new JsVariableForTemplate { name = "docnr", value = info.internalId });

        }
        else if (info.ProjectStatus.ToString().StartsWith("Invoice"))
        {
            vars.Add(new JsVariableForTemplate { name = "doctype", value = Resources.Resource.Invoice });
            vars.Add(new JsVariableForTemplate { name = "docnr", value = info.internalId });
            vars.Add(new JsVariableForTemplate { name = "expdate", value = Convert2.DateToShortStr(info.dateInvoiceDue) });


            if (Request.Params["mode"] != null && Request.Params["mode"] == "reminder")
            {
                Calani.BusinessObjects.Projects.ReminderManager rmd = new Calani.BusinessObjects.Projects.ReminderManager(ph.CurrentOrganizationId);
                string reminder = (rmd.GetReminders(info.id).Count).ToString();

                vars.Add(new JsVariableForTemplate { name = "reminder", value = reminder });


                tbxSubject.Text = Resources.Resource.Reminder;
                tpl = tplmgr.Get("SendReminder");
            }
            else
            {
                tpl = tplmgr.Get("SendInvoice");
            }
        }

        if (tpl != null)
        {
            templateSubject = tpl.DefaultTitle;
            templateBody = tpl.DefaultContent;
            



            

            if (tpl.UserTemplate != null && tpl.UserTemplate.title != null) 
                templateSubject = tpl.UserTemplate.title;
            if (tpl.UserTemplate != null && tpl.UserTemplate.content != null) 
                templateBody = tpl.UserTemplate.content;
        }

        tpl = tplmgr.Get("Signature");
        if (tpl != null)
        {
            string signature = tpl.DefaultContent;
            if (tpl.UserTemplate != null && tpl.UserTemplate.title != null) signature = tpl.UserTemplate.content;
            templateBody += "\n\n\n";
            templateBody += signature;
        }


        foreach (var item in vars)
        {
            templateSubject = templateSubject.Replace("$" + item.name, item.value);
            templateBody = templateBody.Replace("$" + item.name, item.value);
        }

        tbxSubject.Text = templateSubject;

        templateBody = templateBody.Replace("  ", " ");
        DefaultTextJson = new JavaScriptSerializer().Serialize(new DefText { t = templateBody });
        templateBody = templateBody.Replace("$contactfn", info.contacts.firstName);
        templateBody = templateBody.Replace("$contactln", info.contacts.lastName);
        templateBody = templateBody.Replace("$contacttitle", civTitle);
        templateBody = templateBody.Replace("  ", " ");
        tbxBody.Value = templateBody;

        Calani.BusinessObjects.Contacts.ClientContactsManager ctcmgr = new Calani.BusinessObjects.Contacts.ClientContactsManager(ph.CurrentOrganizationId, info.clientId);
        var contacts = ctcmgr.List();
        contacts.Add(new Calani.BusinessObjects.Model.contacts { CustomText = Resources.Resource.OtherEmailUseCC, firstName = Resources.Resource.Firstname, lastName = Resources.Resource.Lastname, civtitle = "Mr"  });
        ddlToContact.DataSource = contacts;
        string selectedContactId = Convert2.ToString(info.clientContact);
        ddlToContact.SelectedValue = selectedContactId;
        SelectedEmail = (from r in contacts where r.id == info.clientContact select r.primaryEmail).FirstOrDefault();
        ddlToContact.DataBind();
        tbxCC.Text = info.send_copy_email_to;

        List<ContactInfo> infos = new List<ContactInfo>();
        foreach (var item in contacts)
        {
            infos.Add(new ContactInfo
            {
                id = item.id,
                email = item.primaryEmail,
                contactfn = item.firstName,
                contactln = item.lastName,
                contacttitle = item.title
            });
        }
        ContactsJson = new JavaScriptSerializer().Serialize(infos);
        repContacts.DataSource = infos;
        repContacts.DataBind();


        if (info.ProjectStatus.ToString().StartsWith("Quote"))
        {
            lblBack.Text = Resources.Resource.BackToQuote;

            //if (orgmgr.BeforeInvoice == "DeliveryNote")
            //{
            //    lblBack.Text = Resources.Resource.BackToDeliveryNote;
            //}
            //else
            //{
            //    lblBack.Text = Resources.Resource.BackToQuote;
            //}
        }
        else if (info.ProjectStatus.ToString().StartsWith("DeliveryNote"))
        {
            lblBack.Text = Resources.Resource.BackToDeliveryNote;
        }
        else if (info.ProjectStatus.ToString().StartsWith("Job"))
        {
            lblBack.Text = Resources.Resource.BackToJob;
        }
        else if (info.ProjectStatus.ToString().StartsWith("Invoice"))
        {
            lblBack.Text = Resources.Resource.BackToInvoice;
        }
        



    }

    public class ContactInfo
    {
        public string email { get; set; }
        public string contactfn { get; set; }
        public string contactln { get; set; }
        public string contacttitle { get; set; }
        public long id { get; set; }
    }

    public class DefText
    {
        public string t { get; set; }
    }
}