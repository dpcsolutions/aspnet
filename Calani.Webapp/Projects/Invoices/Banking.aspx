﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Projects_Invoices_Banking" Codebehind="Banking.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
        <div class="content">
            


            <div class="row" runat="server" id="panelIntro">
                    
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="fa fa-bank text-green"></i>
										<strong><%= Resources.Resource.BankingImport %></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

							<div class="panel-body">
                                <p>
                                    <%=Resources.Resource.BankingImportDescription %>
                                </p>
                                    
                            </div>

                                
                        </div>
                    
                    <asp:HiddenField runat="server" ID="hfFile" />
                </div>

                

                <div class="row" runat="server" id="panelUpload">

                <div class="panel panel-white">
                    
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                
                                <div id="dropzone_file" class="fallback dropzone dz-clickable">
                              
                                </div>

                            </div>
                            <div class="col-sm-6 col-md-6">


                                <div>
                                    Demandez conseil à votre banque pour obtenir vos exports
                                    pour les inscriptions au crédit BVR au format camt.054.
                                </div>
                                

                            </div>
                        </div>
                        
                        <style type="text/css">
                            .dropzone {
                                min-height: 400px !important;
                            }
                        </style>
                        <script type="text/javascript">

                            var ressources = {};
                            ressources['DropFilesToUpload'] = "<%= Resources.Resource.DropCAMTFileToUpload %>";
                            ressources['OrClick'] = "<%= Resources.Resource.OrClick %>";

                        

                            $(function () {
                                Dropzone.autoDiscover = false;
                                $("#dropzone_file").dropzone({
                                    uploadMultiple: false,
                                    url: "UploadBanking.ashx",
                                    acceptedFiles: 'text/xml,application/octet-stream',
                                    dictDefaultMessage: ressources['DropFilesToUpload'] + ' <span>' + ressources['OrClick'] + '</span>',
                                    maxFilesize: 30,
                                    init: function () {
                                        this.on("complete", function (file) {

                                            if (file.xhr?.status === 200) {
                                                $('#ContentPlaceHolder1_hfFile').val(file.xhr.responseText);
                                            
                                            }
                        
                                        });
                                    }
                                });
                            });


                            function checkFile() {
                                var f = $('#ContentPlaceHolder1_hfFile').val();
                                if (f != null && f != '' && f.length > 3) return true;

                                alert("<%= Resources.Resource.UploadFileFirst %>");
                                return false
                            }
                        </script>
                    </div>
                    <div class="panel-footer text-right no-padding">
                        <div class="row" style="margin:10px">
                            <asp:LinkButton CssClass="btn btn-success" runat="server" ID="btnUploadNext" OnClientClick="return checkFile()" OnClick="btnUploadNext_Click" >
                                <i class="icon-upload7 position-left"></i> 
                                <%= Resources.Resource.ImportFile %>
						    </asp:LinkButton>
                        </div>
                    </div>

                </div>
            </div>



                <div class="row" runat="server" id="panelResult" visible="false">
                    <div class="panel panel-white">
                        <div class="panel-heading">
						    <h6 class="panel-title">
							    <a name="quotes">
								    <i class="icon-file-upload display-inline-block text-<%= ph.CMP.Color %>"></i>
								    <strong><%= Resources.Resource.ImportClients %></strong>
							    </a>
						    </h6>
					    </div><!-- panel-heading -->

                        <div class="panel-body">
                            
                            <asp:GridView runat="server" ID="grid" AutoGenerateColumns="False" >
                                    <Columns>
                                        <asp:TemplateField HeaderText="Match">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cbxMatch" runat="server" Checked='<%# Bind("Matched") %>' 
                                                    Visible='<%# Bind("Matched") %>' 
                                                    CssClass="checked control-primary" />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="From">
                                            <ItemTemplate>
                                                <asp:Label ID="lblFrom" runat="server" Text='<%# Bind("From") %>' />
                                                <br /> 
                                                <span class="label bg-grey"><asp:Label ID="lblTransaction" runat="server" Text='<%# Bind("Transaction") %>' /></span>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:dd/MM/yyyy}"  />
                                        <asp:BoundField DataField="Amount" HeaderText="Amount"  />
                                        
                                        <asp:TemplateField HeaderText="Invoice">
                                            <ItemTemplate>
                                                <asp:Panel runat="server" ID="panelMatching" Visible='<%# Bind("Matched") %>'>
                                                    <strong><asp:Label ID="lblClientName" runat="server" Text='<%# Bind("ClientName") %>' /></strong>
                                                    <br /> 
                                                    <span class="label bg-success">
                                                    <asp:HyperLink runat="server" ID="lnkLink" Target="_blank" style="color:white"  NavigateUrl='<%# "Job.aspx?id=" +  Eval("MatchingInvoiceId") %>'>
                                                        <asp:Label ID="lblInvoiceNum" runat="server" Text='<%# Bind("InternalId") %>' />
                                                    </asp:HyperLink></span><br /><%= Resources.Resource.Due %>: <asp:Label ID="lblDue" runat="server" Text='<%# Bind("Due") %>' />
                                                </asp:Panel>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                            </asp:GridView>

                        </div>

                        <div class="panel-footer text-right no-padding">
                        <div class="row" style="margin:10px">
                            <asp:LinkButton CssClass="btn btn-success" runat="server" ID="btnSave" OnClick="btnSave_Click" >
                                <i class="icon-file-check2 position-left"></i> 
                                <%= Resources.Resource.SavePayments %>
						    </asp:LinkButton>

                            <asp:HyperLink CssClass="btn btn-danger" Visible="false"  runat="server" ID="btnCancel" NavigateUrl="Banking.aspx">
                               <i class="icon-backward position-left"></i> 
                                <%= Resources.Resource.NoBvrMatch %>
                            </asp:HyperLink>
                        </div>
                    </div>
                    </div>
                </div>
        </div>
    </form>
</asp:Content>

