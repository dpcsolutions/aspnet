﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.BankingImport;
using Calani.BusinessObjects.CustomerAdmin;

public partial class Projects_Invoices_Banking : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnUploadNext_Click(object sender, EventArgs e)
    {
        panelIntro.Visible = false;
        panelUpload.Visible = false;

        string path = System.IO.Path.Combine(Calani.BusinessObjects.Imports.FileImportManager.DIR,
            ph.CurrentOrganizationId.ToString(),
            hfFile.Value
        );
        
        panelResult.Visible = true;

        var parser = new BankingParser();
        parser.ReadFile(path);
        var orgMgr = new OrganizationManager(ph.CurrentOrganizationId);
        orgMgr.Load();
        
        var matching = new PaymentMatching(parser.Records);
        matching.MatchInvoices(ph.CurrentOrganizationId);
        grid.DataSource = matching.Records;
        grid.DataBind();
        new Html5GridView(grid, false);

        int matchable = (from r in matching.Records where r.Matched select r).Count();
        if (matchable == 0)
        {
            btnSave.Visible = false;
            btnCancel.Visible = true;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string path = System.IO.Path.Combine(Calani.BusinessObjects.Imports.FileImportManager.DIR, ph.CurrentOrganizationId.ToString(), hfFile.Value);
        var parser = new Calani.BusinessObjects.BankingImport.BankingParser();
        parser.ReadFile(path);

        var matching = new PaymentMatching(parser.Records);
        var orgMgr = new OrganizationManager(ph.CurrentOrganizationId);
        orgMgr.Load();
        matching.MatchInvoices(ph.CurrentOrganizationId);

        for (int i = 0; i < grid.Rows.Count; i++)
        {
            CheckBox cbx = grid.Rows[i].FindControl("cbxMatch") as CheckBox;
            if(cbx != null && cbx.Checked == true && parser.Records[i].Matched)
            {
                SavePayment(parser.Records[i]);
            }
        }

        Response.Redirect("../Invoices/");
    }

    private void SavePayment(PaymentBankingRecord payment)
    {
        Calani.BusinessObjects.Projects.InvoicePaymentsManager paymgr = new Calani.BusinessObjects.Projects.InvoicePaymentsManager(ph.CurrentOrganizationId, payment.MatchingInvoiceId);
        string note = "BVR " + payment.Transaction;

        var prevpayments = paymgr.List();
        if ((from r in prevpayments where r.comment == note select r).Count() == 0)
        {
            paymgr.Add(new Calani.BusinessObjects.Model.payments
            {
                amountPayed = payment.Amount,
                datePayed = payment.Date,
                comment = note,
                jobId = payment.MatchingInvoiceId,
                recordStatus = 0
            });
        }

        Calani.BusinessObjects.Projects.ProjectsManager mgr = new Calani.BusinessObjects.Projects.ProjectsManager(ph.CurrentOrganizationId);
        var invoice = mgr.Get(payment.MatchingInvoiceId);
        var orgMgr = new OrganizationManager(ph.CurrentOrganizationId);
        
        Calani.BusinessObjects.Projects.InvoiceDueCalculator calc = new Calani.BusinessObjects.Projects.InvoiceDueCalculator(invoice);
        calc.Recalc(orgMgr.UsePriceIncludingVat);

        if (invoice.invoiceDue < 1)
        {
            invoice.dateFullyPaid = payment.Date;
        }

        mgr.Update(invoice.id, invoice);
    }
}