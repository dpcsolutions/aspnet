﻿$(function () {

    $('#gridImpotsOnResults').hide();


    $('.btn-imports-cancel').click(function () {
        $('#gridImports').html(' ');
        $('#gridImportsActions').hide();
    });


    function loadImportsList(type) {
        var clientId = toNum($('#ContentPlaceHolder1_JobEditor1_ddlCustomer').val());
        
        var bDeliveryNotes = type == "deliverynotes";
        var bQuotes = type == "quotes";
        var bProjects = type == "opens";



         $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
             url: '../../API/Service.svc/GetProjectsToImport?customerid=' + clientId + '&deliverynotes=' + bDeliveryNotes + '&quotes=' + bQuotes + '&projects=' + bProjects,
            dataType: "json",
            success: function (response) {
                

                $('#gridImports').html(' ');
                $('#gridImportsActions').hide();
                $('#gridImports').show();

                if(response.length == 0)
                {
                    $('#gridImpotsOnResults').hide();
                    $('#gridImportsActions').hide();
                    

                    var msg = $('<p></p>');
                    msg.text(ressources2['SorryNothingToImport']);
                    $('#gridImports').append(msg);
                    
                }
                else
                {
                    
                    $('#gridImpotsOnResults').show();
                    $('#gridImportsActions').show();

                    var table = $('<table class="table"></table>');
                    $('#gridImports').append(table);
                    var tr = null;
                    var td = null;
                    var cbx = null;

                    tr = $('<tr></td>');
                    table.append(tr);

                    td = $('<td></td>');
                    td.text(' ');
                    tr.append(td);

                    td = $('<td></td>');
                    td.text(' ');
                    tr.append(td);

                    td = $('<td></td>');
                    td.text(ressources2['Import']);
                    tr.append(td);

                    td = $('<td></td>');
                    td.text(ressources2['Date']);
                    tr.append(td);

                    td = $('<td></td>');
                    td.text(ressources2['Description']);
                    tr.append(td);

                    td = $('<td></td>');
                    td.text(ressources2['Amount']);
                    tr.append(td);

                   

                    for(var i=0; i<response.length; i++)
                    {
                        tr = $('<tr></td>');
                        table.append(tr);

                        td = $('<td></td>');
                        cbx = $('<input type="checkbox" class="control-primary cbx-jobimport" />');
                        cbx.data('id', response[i].Id);
                        if(response[i].ImportedInInternalId == null) cbx.attr("checked", "checked");
                        td.append(cbx);
                        tr.append(td);

                        td = $('<td></td>');
                        var folder = "Opens";
                        if (bQuotes) folder = "Quotes";
                        var viewlink = $('<a href="../' + folder + '/Job.aspx?id=' + response[i].Id + '" target="_blank" title="' + ressources2['OpenInNewWindow'] + '"></a>');
                        viewlink.text(response[i].InternalId);
                        if (response[i].ImportedInId > 0) {
                            viewlink.addClass('text-danger');
                        }
                        td.append(viewlink);
                        tr.append(td);

                        td = $('<td></td>');
                        if (response[i].ImportedInId > 0) {
                            var impsp = $('<span class="label label-danger"></span>');
                            var implink = $('<a class="text-white" href="../Invoices/Job.aspx?id=' + response[i].ImportedInId + '" target="_blank" title="' + ressources2['AlreadyImportedOnAnInvoice'] + '"></a>');
                            impsp.append(implink);
                            implink.append($('<i class="icon-chevron-right"></i>'));
                            implink.append(response[i].ImportedInInternalId);
                            td.append(impsp);
                        }
                        else
                        {
                            var impsp = $('<i class="icon-new text-primary"></i>');
                            var impsp2 = $('<span class="text-primary" title="' + ressources2['NeverImportedYet'] + '"></span>');
                            impsp2.text(" ...");
                            td.append(impsp);
                            td.append(impsp2);
                        }
                        tr.append(td);


                        td = $('<td></td>');
                        td.text(response[i].Date);
                        tr.append(td);

                        td = $('<td></td>');    
                        var text = $('<span></span>');
                        text.text(response[i].Description);
                        td.append(text);
                        tr.append(td);

                        td = $('<td></td>');
                        td.text(toDouble(response[i].Amount));
                        tr.append(td);
                    }

                    
                }
                
                   

            },
            error: function (a, b, c) {
                //alert(a.responseText);
            }
        });
    }


    $('.btn-import').click(function () {
        var btn = $(this);
        var type = btn.data('type');
        loadImportsList(type);
    });
});