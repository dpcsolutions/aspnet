﻿var _periodFilterFrom = null;
var _periodFilterTo = null;
var _clientFilter = null;
var _statusFilter = 'InvoicePaymentDue';

var _dataTable = null;

$(document).ready(function() {
    //<%-- VAT banner --%> 
    // let c = $.cookie('hide-alert');
    // if (c !== 'true'){
    //     $("#vatUpdateBanner").show();
    // } else {
    //     $("#vatUpdateButton").show();
    // }
});

function checkField(val, str){
    return val?.toString().toLowerCase().includes(str.toLowerCase());
}

function containsString (obj, str) {

    if (obj.Ids === undefined){
        if (checkField(obj[0], str)) return true;
        if (checkField(obj[1], str)) return true;
        if (checkField(obj[2], str)) return true;
        if (checkField(obj[3], str)) return true;
        if (checkField(obj[4], str)) return true;
        if (checkField(obj[5], str)) return true;
        if (checkField(obj[6], str)) return true;
        if (checkField(obj[7], str)) return true;
        if (checkField(obj[8], str)) return true;
        if (checkField(obj[9], str)) return true;
    }
    else {
        if (checkField(obj.Ids.InternalId, str)) return true;
        if (checkField(obj.ClientName, str)) return true;
        if (checkField(obj.Description, str)) return true;
        if (checkField(obj.Total, str)) return true;
        if (checkField(obj.TotalHt, str)) return true;
        if (checkField(obj.DueDateStr?.split(' ')[0], str)) return true;
        if (checkField(obj.Due, str)) return true;
        if (checkField(getProjectStatusNamable(obj.Status), str)) return true;
        if (checkField(obj.RemindersCount, str)) return true;
        if (checkField(obj.DateStr, str)) return true;
    }

    return false;
}

function closeVATBanner(){
    $.cookie('hide-alert', true);
    $("#vatUpdateBanner").hide();
    $("#vatUpdateButton").show();
}

function showVATBanner(){
    $.removeCookie('hide-alert');
    $("#vatUpdateBanner").show();
    $("#vatUpdateButton").hide();
}

function periodChanged() {
    _dataTable.destroy();
    _dataTable = initInvoicesDataTable($('#invoices-datatable'), true, false, false, undefined, 0, getDataUrl());
}

function initInvoicesDataTable(dt, firstColDesc, isGrouping, footerTotal, pageLength, aggregatedColumnIdx, dataUrl) {

    var options = getDataTableOptions(dt, firstColDesc, isGrouping, footerTotal, pageLength, aggregatedColumnIdx);
    options.ajax = dataUrl;
    options.columns = [
        {
            "data": "Ids",
            "render": function (ids) { return "<a href=\"Job.aspx?id=" + ids.Id + "\">" + ids.InternalId + "</a>"; }
        },
        { "data": "ClientName" },
        { "data": "Description" }];

    if (usePriceIncludingVat === "False"){
        options.columns.push({ "data": "Total", "render": displayMoney });
    }
    options.columns.push( { "data": "TotalExTax", "render": displayMoney },
        { "data": "DueDateStr", "type": "date" });

    if (usePriceIncludingVat === "False"){
        options.columns.push({ "data": "Due", "render": displayMoney });
    }

    options.columns.push({ "data": "Status", "render": getProjectStatusHtml },
        { "data": "RemindersCount" },
        { "data": "DateStr", "type": "date", "render": function (date) { return date ? moment(date, CALANI.DEFAULTS.dateTimeFormat).format('DD.MM.YYYY HH:mm:ss') : ''; } });

    return dt.DataTable(options);
}

function getDataUrl() {
    var dates = getPeriodFilterDates();
    var url = "../../API/Service.svc/ListInvoices?startTicks=" + dates[0] + "&endTicks=" + dates[1];
    return url;
}

function setClientFilter() {
    _clientFilter = $('#ddlClient').val();
    refreshLines();
}

function setStatusFilter(newStatus) {
    _statusFilter = newStatus;
    refreshLines();
}

function refreshLines() {
    _dataTable.draw();
}

function refreshLiveStats() {
    let countInvoiceDraft = 0;
    let countInvoicePaymentDue = 0;
    let countInvoicePaymentLate = 0;
    let countInvoicePaid = 0;

    let sumDraft = 0;
    let sumPaid = 0;
    let sumDue = 0;
    let sumDueLate = 0;

    let rowsId = _dataTable.rows().eq(0);
    for (let i = 0; i < rowsId.length; i++) {
        let row = _dataTable.row(rowsId[i]);
        let data = row.data();
        
        if (row.node().attributes["data-anystatusfilter"] != null && row.node().attributes["data-anystatusfilter"].value == "true")
        {
            let total = parseFloat(data["TotalExTax"]);
            let invoiceDue = usePriceIncludingVat ? parseFloat(data["Due"]) : parseFloat(data["DueExTax"]);

            if (isNaN(total))
                total = 0;

            if (isNaN(invoiceDue))
                invoiceDue = 0;

            let rowStatus = data["Status"];
            if (rowStatus != null) {
                let matchesSearch = true;
                let searchText = $('.dataTables_filter input').val();
                if (searchText) matchesSearch = containsString(data, searchText);

                switch (rowStatus) {
                    case "InvoiceDraft":
                        if (matchesSearch) {
                            countInvoiceDraft++;
                            sumDraft += total;
                        }
                        break;
                    case "InvoicePaymentDue":
                        if (matchesSearch) {
                            countInvoicePaymentDue++;
                            sumDue += invoiceDue;
                            sumPaid += (total - invoiceDue);
                        }
                        break;
                    case "InvoicePaymentLate":
                        if (matchesSearch) {
                            countInvoicePaymentLate++;
                            sumDueLate += invoiceDue;
                            sumPaid += (total - invoiceDue);
                        }
                        break;
                    case "InvoicePaid":
                        if (matchesSearch) {
                            countInvoicePaid++;
                            sumPaid += total;
                        }
                        break;
                }
            }
        }
    }

    $('#ContentPlaceHolder1_lblCountInvoicePaymentDue').html(countInvoicePaymentDue);
    $('#ContentPlaceHolder1_lblCountInvoiceDraft').html(countInvoiceDraft);
    $('#ContentPlaceHolder1_lblCountInvoicePaymentLate').html(countInvoicePaymentLate);
    $('#ContentPlaceHolder1_lblCountInvoicePaid').html(countInvoicePaid);

    if (typeof d3 !== 'undefined') {
        // Pie arc with legend
        // ------------------------------
        if (typeof d3 !== 'undefined') {
            // Initialize chart
            $('#pie_arc_legend').html('');

            pieArcWithLegend("#pie_arc_legend", 170);

            // Chart setup
            function pieArcWithLegend(element, size) {

                // Basic setup
                // ------------------------------

                // Add data set
                var data = [
                    {
                        "status": ressources['Draft'],
                        "icon": "<i class='status-mark border-danger-300 position-left'></i>",
                        "value": sumDraft,
                        "color": "#777",
                        "hidden": true
                    },
                    {
                        "status": ressources['LateExcTax'],
                        "icon": "<i class='status-mark border-danger-300 position-left'></i>",
                        "value": sumDueLate,
                        "color": "#EF5350",
                        "hidden": false
                    },
                    {
                        "status": ressources['DueExcTax'],
                        "icon": "<i class='status-mark border-danger-300 position-left'></i>",
                        "value": sumDue,
                        "color": "#2196F3",
                        "hidden": false
                    }
                    ,
                    {
                        "status": ressources['PaidExcTax'],
                        "icon": "<i class='status-mark border-success-300 position-left'></i>",
                        "value": sumPaid,
                        "color": "#66BB6A",
                        "hidden": false
                    }
                ];

                // Main variables
                var d3Container = d3.select(element),
                    distance = 2, // reserve 2px space for mouseover arc moving
                    radius = (size / 2) - distance,
                    sum = d3.sum(data, function (d) { return d.hidden ? 0 : d.value; });

                // Create chart
                // ------------------------------

                // Add svg element
                var container = d3Container.append("svg");

                // Add SVG group
                var svg = container
                    .attr("width", size)
                    .attr("height", size / 2)
                    .append("g")
                    .attr("transform", "translate(" + (size / 2) + "," + (size / 2) + ")");

                // Construct chart layout
                // ------------------------------

                // Pie
                var pie = d3.layout.pie()
                    .sort(null)
                    .startAngle(-Math.PI / 2)
                    .endAngle(Math.PI / 2)
                    .value(function (d) {
                        return d.hidden ? 0 : d.value;
                    });

                // Arc
                var arc = d3.svg.arc()
                    .outerRadius(radius)
                    .innerRadius(radius / 1.3);

                //
                // Append chart elements
                //

                // Group chart elements
                var arcGroup = svg.selectAll(".d3-arc")
                    .data(pie(data))
                    .enter()
                    .append("g")
                    .attr("class", "d3-arc")
                    .style({
                        'stroke': '#fff',
                        'stroke-width': 2,
                        'cursor': 'pointer'
                    });

                // Append path
                var arcPath = arcGroup
                    .append("path")
                    .style("fill", function (d) {
                        return d.data.color;
                    });


                //
                // Interactions
                //

                // Mouse
                arcPath
                    .on('mouseover', function (d, i) {

                        // Transition on mouseover
                        d3.select(this)
                            .transition()
                            .duration(500)
                            .ease('elastic')
                            .attr('transform', function (d) {
                                d.midAngle = ((d.endAngle - d.startAngle) / 2) + d.startAngle;
                                var x = Math.sin(d.midAngle) * distance;
                                var y = -Math.cos(d.midAngle) * distance;
                                return 'translate(' + x + ',' + y + ')';
                            });

                        $(element + ' [data-slice]').css({
                            'opacity': 0.3,
                            'transition': 'all ease-in-out 0.15s'
                        });
                        $(element + ' [data-slice=' + i + ']').css({ 'opacity': 1 });
                    })
                    .on('mouseout', function (d, i) {

                        // Mouseout transition
                        d3.select(this)
                            .transition()
                            .duration(500)
                            .ease('bounce')
                            .attr('transform', 'translate(0,0)');

                        $(element + ' [data-slice]').css('opacity', 1);
                    });

                // Animate chart on load
                arcPath
                    .transition()
                    .delay(function (d, i) {
                        return i * 500;
                    })
                    .duration(500)
                    .attrTween("d", function (d) {
                        var interpolate = d3.interpolate(d.startAngle, d.endAngle);
                        return function (t) {
                            d.endAngle = interpolate(t);
                            return arc(d);
                        };
                    });


                //
                // Append total text
                //

                svg.append('text')
                    .attr('class', 'text-muted')
                    .attr({
                        'class': 'half-donut-total',
                        'text-anchor': 'middle',
                        'dy': -25
                    })
                    .style({
                        'font-size': '12px',
                        'fill': '#999'
                    })
                    .text(ressources['InvoicedExcTax']);


                //
                // Append count
                //

                // Text
                svg
                    .append('text')
                    .attr('class', 'half-conut-count')
                    .attr('text-anchor', 'middle')
                    .attr('dy', -8)
                    .style({
                        'font-size': '15px',
                        'font-weight': 500
                    });

                // Animation
                svg.select('.half-conut-count')
                    .transition()
                    .duration(1500)
                    .ease('linear')
                    .tween("text", function (d) {
                        var i = d3.interpolate(this.textContent, sum);

                        return function (t) {


                            var num = d3.format("d")(Math.round(i(t)));
                            num = num * 1;
                            num = num.toFixed(2)
                                .replace('.', ',')
                                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ');

                            this.textContent = num + " " + currency;
                        };
                    });


                //
                // Legend
                //

                // Add legend list
                var legend = d3.select(element)
                    .append('ul')
                    .attr('class', 'chart-widget-legend')
                    .selectAll('li')
                    .data(pie(data))
                    .enter()
                    .append('li')
                    .attr('data-slice', function (d, i) {
                        return i;
                    })
                    .attr('style', function (d, i) {
                        return 'font-size: 13px; border-bottom: solid 2px ' + d.data.color;
                    })
                    .text(function (d, i) {
                        return d.data.status + ': ';
                    });

                // Legend text
                legend.append('span')
                    .text(function (d, i) {

                        var num = d.data.value;
                        num = num * 1;
                        num = num.toFixed(2)
                            .replace('.', ',')
                            .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ');

                        return num + " " + currency;
                    });
            }
        }
        // --> Pie arc with legend end
    }
}

function filterItem(settings, data, dataIndex) {
    var visible = true;
    var projectStatus = settings.aoData[dataIndex]._aData["Status"];

    let searchText = $('.dataTables_filter input').val();
    if (searchText) {
        visible = containsString(data, searchText);
    }

    if (_clientFilter != null) {
        var clientMatch = false;
        for (var i = 0; i < _clientFilter.length; i++) {
            if (_clientFilter[i] == settings.aoData[dataIndex]._aData["ClientId"])
                clientMatch = true;
        }
        if (clientMatch == false)
            visible = false;
    }

    var statusDate = projectStatus == 'InvoiceDraft' ? dateTimeToTicks(new Date()) : null;
    var filterByDate = dateTimeToTicks(parseDate(settings.aoData[dataIndex]._aData["FilterByDate"]));
    var lastUpdate = dateTimeToTicks(parseDate(settings.aoData[dataIndex]._aData["LastUpdateDate"]));
    var dateFullyPaid = dateTimeToTicks(parseDate(settings.aoData[dataIndex]._aData["FullyPaidDate"]));
    var date = filterByDate || statusDate || dateFullyPaid || lastUpdate;

    if (_periodFilterFrom != null) {
        if (date < _periodFilterFrom)
            visible = false;
    }

    if (_periodFilterTo != null) {
        if (date >= _periodFilterTo)
            visible = false;
    }

    settings.aoData[dataIndex].nTr.setAttribute('data-anystatusFilter', visible);

    if (_statusFilter != null) {
        if (projectStatus != _statusFilter)
            visible = false;
    }

    return visible;
}

$(function () {

    var f = getUrlParameter('f');
    if (f != null) _statusFilter = f;

    // client side filters
    $.fn.dataTable.ext.search.push(filterItem);

    _dataTable = initInvoicesDataTable($('#invoices-datatable'), true, false, false, undefined, 0, getDataUrl());
    _dataTable.search('');
    _dataTable.on('draw', refreshLiveStats);

});

new Vue({
    el: '#app',
    data: {
        statusFilter: 'InvoicePaymentDue'
    },
    methods: {
        setStatusFilter: function (e, f) {
            this.statusFilter = f;
            _statusFilter = f;
            _dataTable.draw();
            e && e.preventDefault();
        }
    }
});