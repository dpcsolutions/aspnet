﻿using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Projects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

public partial class Projects_Invoices_Job : System.Web.UI.Page
{
    public PageHelper ph;

    public List<long> ServiceJobsIds { get; set; }

    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // page not available for employee
        if (ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
        {
            Response.Redirect("~/");
        }
        // -->

        var serviceJobsIdsStr = Request.Params.Get("serviceJobsIds");
        if (!string.IsNullOrEmpty(serviceJobsIdsStr))
        {
            ServiceJobsIds = serviceJobsIdsStr.Split(',').Select(s => Int64.Parse(s)).ToList();
            JobEditor1.ServiceJobsIds = ServiceJobsIds;
        }

        JobEditor1.IsAdmin = ph.CurrentUserType == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin;
    }

    public string JsonPayments { get; set; }
    
    public DateTime? InvoicePaymentDate { get; private set; }

    public void Load()
    {
        Calani.BusinessObjects.Projects.jobsExtended info = JobEditor1.Info;

        if (info.lastUpdate != null)
        {
            lblLastChange.InnerHtml = Resources.Resource.LastChange + "<br />" + Convert2.DateToStr(info.lastUpdate.Value);
        }
        else
        {
            lblLastChange.InnerHtml = Resources.Resource.NeverSaved;
        }


        if (info.dateInvoicingOpen != null)
        {
            lblLastSent.InnerHtml = "<i class=\"icon-checkmark\"></i> " + Resources.Resource.LastSend + "<br />" + Convert2.DateToStr(info.dateInvoicingOpen.Value);
        }
        else
        {
            lblLastSent.InnerHtml = Resources.Resource.NeverSent;
            lblLastSent.Attributes["class"] += " text-muted";
        }

        InvoicePaymentDate = info.dateFullyPaid;

        lblRemindersCount.Text = JobEditor1.Info.RemindersCount.ToString();
        if(JobEditor1.Info.RemindersCount > 0)
        {
            repeaterReminders.Visible = true;
            Calani.BusinessObjects.Projects.ReminderManager remmgr = new ReminderManager(ph.CurrentOrganizationId);
            repeaterReminders.DataSource = remmgr.GetReminders(JobEditor1.Info.id);
            repeaterReminders.DataBind();
        }
        else
        {
            repeaterReminders.Visible = false;
        }

        if (info.ProjectStatus == ProjectStatus.InvoicePaymentLate)
        {
            lblStatusChange.InnerHtml = "<i class='icon-bell2'></i> " + Resources.Resource.Late;
        }
        else if (info.ProjectStatus == ProjectStatus.InvoicePaymentDue)
        {
            lblStatusChange.InnerHtml = "<i class='icon-bell-check'></i> " + Resources.Resource.Pending;
        }
        else if (info.ProjectStatus == ProjectStatus.InvoicePaid)
        {
            lblStatusChange.InnerHtml = "<i class='icon-cash2'></i> " + Resources.Resource.Paid 
                + (info.dateFullyPaid.HasValue ? "<br />" + info.dateFullyPaid.Value.ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture) : string.Empty);
        }

        JsonPayments = "[]";
        if (info != null && info.id > 0)
        {
            Calani.BusinessObjects.Projects.InvoicePaymentsManager mgr = new InvoicePaymentsManager(info.organizationId.Value, info.id);
            var payments = mgr.GetInvoicePayments();
            JsonPayments = new JavaScriptSerializer().Serialize(payments);
        }


        if(info.dateInvoiceDue != null && info.dateInvoiceDue < DateTime.Now)
        {
            panelReminder.Visible = true;
        }
        else
        {
            panelReminder.Visible = false;
        }

    }
}