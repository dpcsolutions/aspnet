﻿<%@ WebHandler Language="C#" Class="UploadBanking" %>

using System;
using System.Web;
using Calani.BusinessObjects.Imports;

public class UploadBanking : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";


        long orgId = PageHelper.GetCurrentOrganizationId(context.Session);

        string dir = System.IO.Path.Combine(FileImportManager.DIR, orgId.ToString());

        if (!System.IO.Directory.Exists(dir)) 
            System.IO.Directory.CreateDirectory(dir);

        if (context.Request.Files.Count == 1)
        {

            HttpPostedFile file = context.Request.Files[0];
            string fileName = file.FileName;
            string fileType = file.ContentType;



            if (fileType == "text/xml" || fileType == "application/octet-stream" || fileType == "text/plain")
            {
                byte[] fileData = null;
                using (var binaryReader = new System.IO.BinaryReader(file.InputStream))
                {
                    fileData = binaryReader.ReadBytes(file.ContentLength);
                }

                try
                {
                    var parser = new Calani.BusinessObjects.BankingImport.BankingParser();
                    parser.Parse(System.Text.Encoding.Default.GetString(fileData));
                    if (parser.Records.Count > 0)
                    {
                        string name = "camt.xml";
                        string fout = System.IO.Path.Combine(dir, name);
                        System.IO.File.WriteAllBytes(fout, fileData);
                        context.Response.Write(name);
                    }
                    else
                    {
                        throw new Exception("No transaction found in file");
                    }
                }
                catch
                {
                    throw new Exception("Impossible to parse file");
                }

            }
            else
            {
                throw new Exception("File type is wrong");
            }
        }
        else
        {
            throw new Exception("Can not upload multiple files");
        }



    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}