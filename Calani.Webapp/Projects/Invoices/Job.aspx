﻿<%@ Page Title="Invoince" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Projects_Invoices_Job" Codebehind="Job.aspx.cs" %>
<%@ Register src="../JobEditor.ascx" tagname="JobEditor" tagprefix="uc1" %>
<%@ Import Namespace="Calani.BusinessObjects.Generic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <%-- <script src="/calani/assets/js/shared_business_logic/working_time_calculator.js"></script> --%>
    <%-- <script src="/calani/assets/js/plugins/ui/mobiscroll.javascript.min.js"></script> --%>
    <%-- <link href="/calani/assets/css/mobiscroll.javascript.min.css" rel="stylesheet" type="text/css"> --%>

    <script src="../../assets/js/shared_business_logic/working_time_calculator.js"></script>
	<!-- Include Mobiscroll -->
    <script src="../../assets/js/plugins/ui/mobiscroll.javascript.min.js"></script>
    <link href="../../assets/css/mobiscroll.javascript.min.css" rel="stylesheet" type="text/css">
    
	<style type="text/css">
		.invoice-payment-datepicker {
			border: 1px solid #ddd;
			border-left: none;
			font-family: "Roboto", Helvetica Neue, Helvetica, Arial, sans-serif;
			font-size: 13px;
		}

        .popover {
            width: 300px;
        }

	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
    <div class="content">
        <uc1:JobEditor ID="JobEditor1" runat="server"  EnableInvoiceFields="true" />
        
        <div class="row">

            <div class="col-sm-12 col-md-12">
	            <div class="panel panel-flat">
                    <div class="panel-heading">
				        <h5 class="panel-title"><%= Resources.Resource.Payments %></h5>
			        </div>
                    <div class="panel-body">
				        <div class="row text-center">
                            <div id="hot_ac_payments"></div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="form-horizontal">
							<fieldset class="content-group no-margin" style="padding-top:10px">


                                <div class="form-group" id="panelMainDiscount" runat="server">
									<label class="control-label col-lg-6 text-right"><%= Resources.Resource.TotalPaid %></label>													
									<div class="col-lg-5">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-coins"></i></span>
											<input type="text" step="0.01" value="" class="form-control" disabled="disabled" id="tbxTotalPaid" >
										</div>
									</div>
								</div>


								<div class="form-group">
									<label class="control-label col-lg-6 text-right"><%= Resources.Resource.Due %></label>
									<div class="col-lg-5 text-right">
										<div class="input-group">
											<span class="input-group-addon"><i class="icon-alarm"></i></span>
											<input type="text" step="0.01" value="" class="form-control" disabled="disabled" id="tbxTotalDue" >
										</div>
									</div>
								</div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">

            <div class="col-sm-12 col-md-12">
	            <div class="panel panel-flat" runat="server" id="panelMultipleImports">
			        <div class="panel-heading">
				        <h5 class="panel-title"><%= Resources.Resource.MultipleImports %></h5>
                        <div class="heading-elements">
					        <ul class="icons-list">
						        <li><a data-action="collapse"></a></li>
					        </ul>
				        </div>
			        </div>
                    <div class="panel-body">



                        <p><%= Resources.Resource.MultipleImportsDescription %></p>

                        <p>
                            <a href="javascript:void(0)" runat="server" class="btn btn-default btn-import" data-type="deliverynotes"><%= Resources.Resource.DeliveryNotes %></a>
                            <a href="javascript:void(0)" runat="server" class="btn btn-default btn-import" data-type="quotes"><%= Resources.Resource.Quotes %></a>
                            <a href="javascript:void(0)" runat="server" class="btn btn-default btn-import" data-type="opens"><%= Resources.Resource.Open_Jobs %></a>
                        </p>

                        <div id="gridImports">
                            


                        </div>

                        <div id="gridImportsActions" style="display:none; margin:10px;">
                            <a href="javascript:void(0)" class="btn btn-primary btn-save" data-action="importjobs" data-lbl-date="ContentPlaceHolder1_lblLastChange"><i class="icon-import position-left"></i><%= Resources.Resource.Import %></a> 
                            <a href="javascript:void(0)" class="btn btn-default btn-xs btn-imports-cancel"><%= Resources.Resource.Cancel %></a></div>
                        </div>

                        

                    </div>
                    <div class="panel-footer" style="display:none; margin:10px;">
                        <div id="gridImpotsOnResults">
                           <p><%= Resources.Resource.MultipleImportsHowTo %></p>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        

        
        <div class="row">

            <div class="col-sm-12 col-md-12">
	            <div class="panel panel-flat">
			        <div class="panel-heading">
				        <h5 class="panel-title"><%= Resources.Resource.InvoiceManagement %></h5>
			        </div>
                    <div class="panel-body">
				            <div class="row text-center">
											
					            <div class="col-xs-4">
							            <div class="panel panel-body border-top-primary text-center">
								            <p class="text-muted content-group-sm text-size-small two-lines-small-text" id="lblLastChange" runat="server">
                                            
								            </p>

								            <div class="btn-group">
                                                <a href="javascript:void(0)" runat="server" class="btn btn-primary btn-save" data-action="save" data-lbl-date="ContentPlaceHolder1_lblLastChange">
                                                    <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                                </a>
									            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore"><span class="caret"></span></button>
									            <ul class="dropdown-menu dropdown-menu-right" style="min-width:240px">
                                                    <li><a href="javascript:void(0)" class="btn-save" data-action="save" data-lbl-date="ContentPlaceHolder1_lblLastChange"><i class="icon-floppy-disk"></i> <%= Resources.Resource.SaveChanges %></a></li>
                                                    <li><a href="javascript:void(0)" class="btn-clone" data-type="*" data-toggle="modal" data-target="#modal_form_cloneproject"><i class="fa fa-copy"></i> <%= Resources.Resource.Clone %></a></li>
                                                    <li><a href="javascript:void(0)" class="btn-delete" data-type="*" data-toggle="modal" data-target="#modal_form_deleteproject"><i class="fa fa-trash"></i> <%= Resources.Resource.Delete %></a></li>
                                            
									            </ul>
								            </div>
                                            <input type="hidden" id="invoice-payment-date-hdn" />
							            </div>	
					            </div>
                                <div class="col-xs-4">
										    <div class="panel panel-body border-top-primary text-center">
								                <p class="content-group-sm text-size-small two-lines-small-text" id="lblLastSent" runat="server">
                                                
											    </p>

                                                <div class="btn-group">
											        <a href="javascript:void(0)" type="button" class="btn btn-save btn-primary" 
                                                    data-lbl-date="ContentPlaceHolder1_lblLastSent"
                                                    data-action="sendinvoice">
                                                        <i class="icon-envelop position-left"></i>  <%= Resources.Resource.Send_Invoice %> 

											        </a>

                                                    
                                                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" runat="server"><span class="caret"></span></button>
									                <ul class="dropdown-menu dropdown-menu-right" style="min-width:240px">
                                                        <li><a href="javascript:void(0)" class="btn-save" data-action="markinvoicesent" data-lbl-date="ContentPlaceHolder1_lblLastChange"><i class="icon-checkmark-circle"></i> <%= Resources.Resource.MarkInvoiceAsOpened %></a></li>
                                                        <li><a href="javascript:void(0)" class="btn-save" data-action="markinvoicenotsent"><i class="icon-radio-unchecked"></i> <%= Resources.Resource.MarkInvoiceAsDraft %></a></li>
                                                        <li><a href="javascript:setSendDate('setinvoicesentdate', '#ContentPlaceHolder1_lblLastSent')"><i class="icon-calendar"></i> <%= Resources.Resource.ChangeSendDate %></a></li>
                                                        <li><a href="javascript:void(0)" class="btn-save" data-action="previewpdf:invoice"><i class="icon-file-pdf"></i> <%= Resources.Resource.PreviewPDF %></a></li>
									                </ul>
                                                </div>

										    </div>	


                                            <asp:Panel runat="server" ID="panelReminder" class="panel panel-body border-top-primary text-center">
                                                <p>
                                                    <%= Resources.Resource.Reminders %> : 
                                                    <asp:Label runat="server" ID="lblRemindersCount" />
                                                </p>

                                                

                                                <asp:Repeater runat="server" ID="repeaterReminders">
                                                    <ItemTemplate>
                                                        <div>
                                                            
        
                                                            

                                                            <div class="btn-group" style="margin-bottom:10px" id="divReminder<%# Eval("Id") %>">
												                <a href="javascript:void(0)" class="label bg-brown dropdown-toggle" 
                                                                    
                                                                    data-toggle="dropdown" aria-expanded="false"><%# Eval("Date", "{0:dd/MM/yyyy}") %> <span class="caret"></span></a>

												                <ul class="dropdown-menu dropdown-menu-right">
													                <li><a href="javascript:hideReminder(<%# Eval("Id") %>)" class="btn-save" data-action="deletereminder-<%# Eval("Id") %>"><span class="status-mark position-left border-danger"></span> <%= Resources.Resource.Delete %> </a></li>
												                </ul>
											                </div>

                                                       </div>
                                                    </ItemTemplate>
                                                </asp:Repeater>

                                                

                                                <button type="button" class="btn btn-save btn-danger" 
                                                    data-lbl-date="ContentPlaceHolder1_lblLastSent"
                                                    data-action="sendreminder">
                                                    <i class="icon-envelop position-left"></i> <i class="icon-alarm position-left"></i>  <%= Resources.Resource.Send_InvoiceReminder %> 

											    </button>
                                            </asp:Panel>
							    </div>
                                <div class="col-xs-4">
										    <div class="panel panel-body border-top-primary text-center">
								                <p class="text-muted content-group-sm text-size-small two-lines-small-text" id="lblStatusChange" runat="server">
                                                    
											    </p>

                                                <div class="btn-group">
                                                    <a href="javascript:void(0)" class="btn btn-success" data-toggle="popover" id="close-invoice-button"><i class="icon-finish position-left"></i> <%= Resources.Resource.CloseInvoice %></a>
                                                
									                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore2"><span class="caret"></span></button>
									                <ul class="dropdown-menu dropdown-menu-right" style="min-width:240px">
                                                        <li>
                                                            <a href="#" data-toggle="popover" data-target="#modal_invoice_payment_date"><i class="fa fa-cash2"></i> <%= Resources.Resource.MarkAsPaid %></a>
                                                        </li>
                                                        <li><a href="#" class="btn-save" data-action="invoicepending" data-lbl-date="ContentPlaceHolder1_lblStatusChange"><i class="icon-bell-check"></i> <%= Resources.Resource.MarkAsPending %></a></li>
									                </ul>
								                </div>

										    </div>	
							    </div>
				            </div>	
			            </div>
                </div>
            </div>

        </div>

        <div id="form_invoice_payment_date" class="hide">
            <div class="form-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="icon-calendar"></i></span>
					<label for="invoice-payment-date" style="margin: 0">
						<input id="invoice-payment-date" class="invoice-payment-datepicker mbsc-ios mbsc-ltr mbsc-textfield mbsc-textfield-underline" />
					</label>
				</div>
                <br />
                <div class="input-group">
                    <button type="button" class="btn btn-link" onclick="hidePopover()"><%= Resources.Resource.Cancel %></button>
                    <a href="javascript:void(0)" id="ok-close-invoice-btn" class="btn btn-primary btn-save" onclick="markInvoicePaid()"><%= Resources.Resource.OkContinue %></a>
                </div>
			</div>
        </div>

        <script type="text/javascript">

            var paymentsDataSource = <%= JsonPayments %>;
            for (var i = 0; i < 3; i++) paymentsDataSource.push({});

            var ressources2 = [];
            ressources2['Date'] = "<%= Resources.Resource.Date %>";
            ressources2['Description'] = "<%= Resources.Resource.Description %>";
            ressources2['Amount'] = "<%= Resources.Resource.Amount %>";
            ressources2['Comment'] = "<%= Resources.Resource.Comment %>";
            ressources2['Error_PaymentSameDateTwice'] = "<%= Resources.Resource.Error_PaymentSameDateTwice %>";
            ressources2['CreateAnOtherInvoice'] = "<%= Resources.Resource.CreateAnOtherInvoice %>";
            ressources2['YesCreateNew'] = "<%= Resources.Resource.YesCreateNew %>";
            ressources2['GoToList'] = "<%= Resources.Resource.GoToList %>";
            ressources2['Import'] = "<%= Resources.Resource.Import %>";
            ressources2['OpenInNewWindow'] = "<%= Resources.Resource.OpenInNewWindow %>";
            ressources2['AlreadyImportedOnAnInvoice'] = "<%= Resources.Resource.AlreadyImportedOnAnInvoice %>";
            ressources2['NeverImportedYet'] = "<%= Resources.Resource.NeverImportedYet %>";
            ressources2['SorryNothingToImport'] = "<%= Resources.Resource.SorryNothingToImport %>";
            ressources2['FeatureDisabledIfMultipleVAT'] = "<%= Resources.Resource.FeatureDisabledIfMultipleVAT %>";
            ressources2['Unit'] = "<%= Resources.Resource.Unit %>";
            


            function hideReminder(id) {
                $('#divReminder' + id).hide();

                var count = parseInt($('#ContentPlaceHolder1_lblRemindersCount').html());
                count--;
                if (count < 0) count = 0;
                $('#ContentPlaceHolder1_lblRemindersCount').html(count);
            }

            function afterJobSave(query, response)
            {
                
                if (query.action.indexOf("save") == 0)
                {

                    swal({
                            title: ressources2['CreateAnOtherInvoice'],
                            text: '...',
                            type: "success",
                            showCancelButton: true,
                            showCloseButton: true,
                            closeButtonHtml: 'lapin',
                            cancelButtonText: ressources2['GoToList'],
                            confirmButtonColor: "#4CAF50",
                            confirmButtonText: ressources2['YesCreateNew']
                        })
                    .then((value) => {
                        
                        

                        if (value.value != null && value.value == true)
                        {
                            /* create another */
                            window.location = "Job.aspx";
                        }
                        else
                        {
                            if(value.dismiss == "cancel")
                            {
                                /* go to list */
                                window.location = "../Invoices/";
                            }
                            else if(value.dismiss=="close")
                            {
                                /* stay here */
                                if(query.id == -1)
                                {
                                    window.location = "Job.aspx?id=" + response.id;
                                }
                                                           
                            }
                        }

                    });

                    
                }

                if (query.action == "sendinvoice" && query.id > 0)
                {
                    window.location = "Send.aspx?id=" + query.id + "&sel=" + response.attachment;
                }

                if (query.action == "sendreminder" && query.id > 0)
                {
                    window.location = "Send.aspx?id=" + query.id + "&sel=" + response.attachment + "&mode=reminder";
                    
                }
            }

            // Is initialized later
            var invPaymentDatePicker = null;

            function hidePopover() {
                $("#close-invoice-button").popover('hide');
            }

            function markInvoicePaid() {
                // Put the date into a hidden input, it will be read later from saveProject()
                var date = moment(invPaymentDatePicker['invoice-payment-date'].value);
                var dateStr = date.format("DD/MM/YYYY");
                $("#invoice-payment-date-hdn").val(dateStr);

                saveProject("invoicepaid", $("#ContentPlaceHolder1_lblStatusChange"), $("#ok-close-invoice-btn"));
                hidePopover();
            }

            $(function () {
                var hot_ac_payments = document.getElementById('hot_ac_payments');
                var hhot_ac_payments_init = new Handsontable(hot_ac_payments, {
                    licenseKey: 'non-commercial-and-evaluation',
                    data: paymentsDataSource,
                    afterChange: onPaymentChange,
                    stretchH: 'all',
                    colHeaders: [ressources2['Date'], ressources2['Amount'], ressources2['Comment'], ''],
                    colWidths: [2, 2, 7, 1],
                    rowHeight: function (row) {
                        return 30;
                    },
                    defaultRowHeight: 30,
                    columns: [
                        {
                            data: 'datePayed',
                            type: 'date',
                            timeFormat: 'dd/MM/yyyy',
                            correctFormat: true,
                            className: 'htCenter',
                            datePickerConfig: {
                                firstDay: 1,
                                showWeekNumber: true
                            }
                        },
                        {
                            data: 'amountPayed',
                            type: 'numeric',
                            className: 'htRight',
                            format: '0,0.00',
                             numericFormat: {
                                pattern: '0,0.00',
                                culture: 'fr-FR' // this is the default culture, set up for USD
                            }
                        },
                        {
                            data: 'comment',
                            type: 'text'
                        },
                        {
                            renderer:
                            function (instance, td, row, col, prop, value, cellProperties) {
                                var div;
                                // Remove existing buttons to avoid duplicating them.
                                $(td).children('.delbtn').remove();

                                div = document.createElement('div');
                                div.className = 'delbtn';
                                div.style.cursor = 'hand';
                                //div.appendChild(document.createTextNode('x'));
                                delico = $('<i class="fa fa-trash" aria-hidden="true"></i>');
                                $(div).html(' ');
                                $(div).append(delico);
                                td.appendChild(div);

                                $(div).on('mouseup', function () {
                                    if (confirm(ressources.SureToDeleteLine)) {
                                        instance.alter("remove_row", row);

                                        instance.render();
                                        onPaymentChange();
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                });
                                return td;
                            },
                            readOnly: true
                        }

                    ]
                });

                var locale = lang4 == 'en-US' ? mobiscroll.localeEn : mobiscroll.localeFr;

                mobiscroll.setOptions({
                    locale: locale,
                    theme: 'ios',
                    themeVariant: 'light'
                });

                <% var defaultDate = InvoicePaymentDate ?? DateTime.Now; %>

                $("#close-invoice-button").popover({
                    title: '<button type="button" class="close" onclick="hidePopover()">&times;</button>' +
                           '<span"><%= Resources.Resource.DateOfPayment %></span>',
                    container: 'body',
                    placement: 'top',
                    html: true,
                    trigger: 'manual',
                    content: function () {
                        return $('#form_invoice_payment_date').html();
                    }
                })
                .click(function (e) {
                    $(this).popover('show');
                    e.preventDefault();
                });

                $('#close-invoice-button').on('shown.bs.popover', function () {

                    invPaymentDatePicker = mobiscroll.datepicker('#invoice-payment-date', {
                        controls: ['date'],
                        select: 'date',
                        touchUi: true,
                        display: 'bubble',
                        dateFormat: '<%= Tools.DefaultDateFormat.ToUpperInvariant() %>',
                        onInit: function (event, inst) {
                            var date = $("input#invoice-payment-date-hdn").val() || new Date(<%= defaultDate.Year %>, <%= defaultDate.Month - 1 %>, <%= defaultDate.Day %>);
                            inst.setVal(date);
                        }
                    });
                });

                $('#ContentPlaceHolder1_panelMultipleImports').addClass('panel-collapsed');
                $('#ContentPlaceHolder1_panelMultipleImports .panel-body').hide();

              });


            function onPaymentChange() {

                var totalpaid = 0;
                for (var i = 0; i < paymentsDataSource.length; i++)
                {
                    if (paymentsDataSource[i].amountPayed != null) {
                        totalpaid += toDouble(paymentsDataSource[i].amountPayed);
                    }
                }
                $('#tbxTotalPaid').val(precisionRound(totalpaid, 2));

                var due = toDouble($('#tbxGrandTotal').val()) * 1 - totalpaid;

                $('#tbxTotalDue').val(precisionRound(due, 2));
            }

        </script>
    <script type="text/javascript" src="Imports.js"></script>

</form>
</asp:Content>
