﻿<%@ Page Title="Invoices" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Projects_Invoices_Default" Codebehind="Default.aspx.cs" %>
<%@ Register src="../../assets/Controls/ProjectStatusLabel.ascx" tagname="ProjectStatusLabel" tagprefix="psl" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
    <div class="btn-group" runat="server" id="quickmenu">
		<button id="vatUpdateButton" type="button" class="btn btn-icon mr-20" onclick="showVATBanner()" style="display: none">
			<i class="icon-exclamation"></i> <%= Resources.Resource.VatUpdate %>
		</button>
		<button type="button" class="btn bg-gesmobile btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
		</button>
		<ul class="dropdown-menu dropdown-menu-right">
			<li><a href="Job.aspx"><i class="icon-plus2"></i> <%= Resources.Resource.Create %> <%= Resources.Resource.Invoice %></a></li>
			<li><a href="Banking.aspx"><i class="fa fa-bank"></i> <%= Resources.Resource.BankingImport %> </a></li>
		</ul>
	</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
		<div id="vatUpdateBanner" class="content content content-no-padding mt-20" style="display: none">
			<div class="panel panel-body" style="background-color: #f5e9ce">
				<div>
					<div class="media-left media-middle">
						<i class="icon-exclamation icon-3x text-muted no-edge-top"></i>
					</div>

					<div class="media-body">
					<%-- VAT banner --%> <h6 style="display: none" class="media-heading text-semibold"><%= Resources.Resource.VatUpdate %> <asp:Label runat="server" ID="lblVatUpdate" /></h6>
					
					<div>
					    <span class="text-muted row" style="display: none"> <%-- VAT banner --%> 
                        	<%= Resources.Resource.VatUpdateMessage %>  
                        	<a target="_blank" href="../../Settings/Taxes"><%= Resources.Resource.VatUpdateMessageLink %></a>
                        </span>
                        
                        <span class="text-muted row">
                            <%= Resources.Resource.ContactIfQuestions %>
                        	<a target="_blank" href="mailto:support@gesmobile.ch">support@gesmobile.ch</a>
                        </span>
					</div>
				</div>
					<div class="media-right media-middle">
						<button type="button" class="btn btn-icon" style="background-color: transparent" onclick="closeVATBanner()">
                    		<i class="icon-close2" style="color: black"></i>
	                    </button>
					</div>
				</div>
			</div>
		</div>

        <div class="content" id="app">

                <div class="row">


						<div class="col-sm-6 col-md-6">

							<!-- User card with thumb and social icons at the bottom -->
							<div class="panel">
									<div class="panel-body">
										<div class="row text-center">

                                            <div class="col-xs-3" v-bind:class="{ 'bordered alpha-primary': statusFilter=='InvoiceDraft' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.Draft_Tooltip %>">
                                                <a href="" v-on:click="setStatusFilter($event,'InvoiceDraft')" class="text-grey" >
												<p><i class="icon-floppy-disk icon-2x display-inline-block text-grey tada infinite" id="icoCountDraft" runat="server"></i></p>
												<h5 class="text-semibold no-margin" id="lblCountInvoiceDraft" runat="server">.</h5>
                                                    <p><strong class="text-muted text-size-small"><%= Resources.Resource.Draft_many %></strong></p>
                                                </a>
											</div>
											
											<div class="col-xs-3" v-bind:class="{ 'bordered alpha-primary': statusFilter=='InvoicePaymentDue' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.Pending_Tooltip %>">
                                                <a href="" v-on:click="setStatusFilter($event,'InvoicePaymentDue')" >
												<p><i class="icon-bell-check icon-2x display-inline-block text-primary tada infinite" id="icoCountInvoicePaymentDue" runat="server"></i></p>
												<h5 class="text-semibold no-margin" id="lblCountInvoicePaymentDue" runat="server">.</h5>
												
                                                <p><strong class="text-muted text-size-small"><%= Resources.Resource.Pending_many %></strong></p>
                                                </a>
											</div>

                                            <div class="col-xs-3" v-bind:class="{ 'bordered alpha-primary': statusFilter=='InvoicePaymentLate' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.Late_Tooltip %>">
                                                <a href="" v-on:click="setStatusFilter($event,'InvoicePaymentLate')" class="text-danger" >
                                                <p><i class="icon-bell2 icon-2x display-inline-block text-danger tada infinite"  id="icoCountInvoicePaymentLate" runat="server"></i></p>
												<h5 class="text-semibold no-margin" id="lblCountInvoicePaymentLate" runat="server">.</h5>
												
                                                <p><strong class="text-muted text-size-small"><%= Resources.Resource.Late_many %></strong></p>
                                                </a>
											</div>

                                            <div class="col-xs-3" v-bind:class="{ 'bordered alpha-primary': statusFilter=='InvoicePaid' }" data-popup="tooltip"  data-container="body" title="<%= Resources.Resource.Paid_Tooltip %>">
                                                <a href="" v-on:click="setStatusFilter($event,'InvoicePaid')" class="text-green" >

                                                    <p><i class="icon-cash2 icon-2x display-inline-block text-green tada infinite"   id="icoCountInvoicePaid" runat="server"></i></p>
												    <h5 class="text-semibold no-margin" id="lblCountInvoicePaid" runat="server">.</h5>
                                                    <p><strong class="text-muted text-size-small"><%= Resources.Resource.Paid_many %></strong></p>
                                                </a>
                                                
											</div>

										</div>									
									</div>

							    	<div class="panel-footer text-center no-padding">
								    	<div class="row">
								    		<a href="" v-on:click="setStatusFilter($event, null)" class="display-block p-10 text-default" 
								    			data-popup="tooltip"  data-container="body" title="List all quotes">
								    				<i class="fa fa-ellipsis-h"></i> <%= Resources.Resource.Show_all %>
								    				
								    			</a>
								    	</div>
									</div>
								</div>
								<!-- /user card with thumb and social icons at the bottom -->

							</div>




							

							<div class="col-sm-3 col-md-3">
								<div class="panel panel-body text-center">
									<div class="svg-center" id="pie_arc_legend"></div>
								</div>
							</div>




							<div class="col-sm-3 col-md-3">
								<div class="panel panel-body panel-body-accent">
									<div class="media no-margin">
										
										<div>
											<i class="fa fa-calendar-check-o" aria-hidden="true"></i> <%= Resources.Resource.Select_Period %>
										</div>
										<div>
											<select id="ddlPeriod" class="select" onchange="periodChanged()">
												<%= OptionsPeriod %>
											</select>
										</div>

										<div style="margin-top:20px">
											<i class="fa fa-user-circle-o" aria-hidden="true"></i> <%= Resources.Resource.Customers %>
										</div>
										<div>
											<select id="ddlClient" data-placeholder="<%= Resources.Resource.Select___ %>" multiple="multiple" class="select" onchange="setClientFilter()">
                                                <%= OptionsClients %>
											</select>
										</div>
									</div>
								</div>
							</div>
					</div>

					<div class="row">
                        <div class="col-sm-12 col-md-12">
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="icon-upload display-inline-block text-green"></i>
										<strong><%= ph.CMP.Name %></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

							<div class="panel-body">

								<table id="invoices-datatable" class="table table-hover dataTable no-footer">
									<thead>
										<tr>
											<th><%= Resources.Resource.Number %></th>
											<th><%= Resources.Resource.Customer %></th>
											<th><%= Resources.Resource.Description %></th>
											<%= UsePriceIncludingVat ? "" : "<th>" + Resources.Resource.TotalTTC + "</th>" %>
											<th><%= UsePriceIncludingVat ? Resources.Resource.TotalTTC : Resources.Resource.TotalExcTax %></th>
											<th><%= Resources.Resource.To_pay_before %></th>
											<%= UsePriceIncludingVat ? "" : "<th>" + Resources.Resource.Due + "</th>" %>
											<th><%= Resources.Resource.Status %></th>
											<th><%= Resources.Resource.Reminders %></th>
											<th><%= Resources.Resource.Date %></th>
										</tr>
									</thead>
								</table>
                                
                            </div><!-- panel-body -->

                            <div class="panel-footer text-right no-padding">
						    	<div class="row" style="margin:10px">
						    		<a href="Job.aspx" class="btn btn-default"><i class="icon-plus2 position-left"></i> 
                                        <%=Resources.Resource.Create_Invoice %>
                                    </a>
						    	</div>
							</div><!-- panel-footer -->

                        </div>


                    </div>
               </div>
               
                <div class="row">
                        <div class="col-sm-12 col-md-12">
						    <div class="panel panel-white">
                            
							    <div class="panel-heading">
								    <h6 class="panel-title">
									    <a name="quotes">
										    <i class="fa fa-bank text-green"></i>
										    <strong><%= Resources.Resource.BankingImport %></strong>
									    </a>
								    </h6>
							    </div><!-- panel-heading -->

							    <div class="panel-body">
                                    <p>
                                        <%=Resources.Resource.BankingImportDescription %>
                                    </p>
                                </div>

                                <div class="panel-footer text-right no-padding">
						    	<div class="row" style="margin:10px">
						    		<a href="Banking.aspx" class="btn btn-default"><i class="icon-file-upload position-left"></i> 
                                        <%=Resources.Resource.ImportFile %>
                                    </a>
						    	</div>

							</div><!-- panel-footer -->
                            </div>
                        </div>
                </div>



        </div><!-- /content -->

    </form>
    <script type="text/javascript">
        var ressources = [];
        ressources['Late'] = "<%= Resources.Resource.Late %>";
        ressources['Due'] = "<%= Resources.Resource.Due %>";
        ressources['Paid'] = "<%= Resources.Resource.Paid %>";
        ressources['Invoiced'] = "<%= Resources.Resource.Invoiced %>";

        ressources['LateExcTax'] = "<%= Resources.Resource.LateExcTax %>";
        ressources['DueExcTax'] = "<%= Resources.Resource.DueExcTax %>";
		ressources['PaidExcTax'] = "<%= Resources.Resource.PaidExcTax %>";
        ressources['Draft'] = "<%= Resources.Resource.Draft %>";
        ressources['InvoicedExcTax'] = "<%= UsePriceIncludingVat ? Resources.Resource.InvoicedIncTax : Resources.Resource.InvoicedExcTax %>"
        var currency = '<%= ph.Currency %>';
		var usePriceIncludingVat = "<%=UsePriceIncludingVat%>";
    </script>
    <%--<script type="text/javascript" src="Default.js"> </script>--%>
	<script type="text/javascript" src="Default.js?<%= Guid.NewGuid().ToString("n") %>"> </script>
</asp:Content>

