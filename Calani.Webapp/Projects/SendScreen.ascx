﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Projects_SendScreen" Codebehind="SendScreen.ascx.cs" %>

<div class="row">
    <p class="text-center">


        
    </p>
</div>

<div class="row">
    <div class="col-sm-12 col-md-2"> </div>

    <div class="col-sm-12 col-md-8">
        <div class="panel bg-success" runat="server" id="panelSelection">
	        <div class="panel-heading">
                <div class="heading-elements" style="right:inherit; left:20px; z-index:99;">
					
                    <a href="Job.aspx?id=<%= ProjectId %>" class="btn btn-primary btn-labeled"> 
                        <b><i class="icon-square-left"></i></b>
                        <asp:Label runat="server" ID="lblBack" />
                    </a>

				</div>
		        <h6 class="panel-title text-center"><%= Resources.Resource.NewDocumentGenerated %></h6>
                
	        </div>



	        <div class="panel-body">

 

                <p class="text-center" style="margin-bottom:5px">
                    <i class="icon-3x display-inline-block animated tada infinite" runat="server" id="icon"></i>
                </p>
                <p class="text-center" style="margin-bottom:15px">
                    <strong id="lblSelFileName" runat="server"></strong> <%= Resources.Resource.GeneratedAt %> <em id="lblSelFileDate" runat="server"></em>

                </p>

                <p class="text-center">
                    <a class="btn btn-default" href="#" runat="server" id="linkDownload">
                        <i class="icon-file-download2 position-left"></i>
                        <%= Resources.Resource.Download %>
                    </a>

                    <a class="btn btn-default" href="#" runat="server" id="linkOpen" target="_blank">
                        <i class="icon-folder-open2 position-left"></i>
                        <%= Resources.Resource.Open %>
                    </a>
                </p>

                
	        </div>
        </div>

        <div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title"><%= Resources.Resource.SendByEmail %></h5>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
					</ul>
				</div>
			</div>

			<div class="panel-body">

                <div class="form-horizontal">
	                <fieldset class="content-group">

		                <div class="form-group">
			                <label class="control-label col-lg-2"><%= Resources.Resource.Subject %> :</label>
			                <div class="col-lg-10">
				                <div class="input-group">
					                <span class="input-group-addon"><i class="icon-bubble9"></i></span>
					                <asp:TextBox Id="tbxSubject" runat="server" CssClass="form-control" Text="" /> 

				                </div>
			                </div>
		                </div>

                        <div class="form-group">
			                <label class="control-label col-lg-2"><%= Resources.Resource.ToContact %> :</label>
			                <div class="col-lg-10">
				                <div class="input-group">
					                <span class="input-group-addon"><i class="icon-users"></i></span>
					                <asp:DropDownList ID="ddlToContact" runat="server" CssClass="select" 
                                            DataTextField="DescriptionAndMail" DataValueField="id"
                                            />

				                </div>
			                </div>
		                </div>

                        <div class="form-group">
			                <label class="control-label col-lg-2"><%= Resources.Resource.ToCcEmail %> :</label>
			                <div class="col-lg-10">
                                
				                <div class="input-group">
					                <span class="input-group-addon"><i class="icon-users"></i></span>
					                <asp:TextBox Id="tbxCC" runat="server" CssClass="form-control" Text="" placeholder='<%$ Resources:Resource, EmailAddress %>' /> 

				                </div>
                                <div style="margin-left:50px; margin-top:10px">

                                    <asp:Repeater runat="server" ID="repContacts" >
                                        <ItemTemplate>
                                            <div>
                                                <asp:CheckBox runat="server"  CssClass="cc_cbx_sel" Text='<%# Eval("contactln") + " " +  Eval("contactfn") %>'  ToolTip='<%# Eval("email") %>' />
                                                &lt;<asp:Label runat="server" Text='<%# Eval("email") %>' />&gt;
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>

                                </div>
			                </div>
		                </div>

                        <div style="margin-bottom:20px">
                            <textarea id="tbxBody" runat="server" class="form-control" rows="10"></textarea>

                        </div>

                        <div class="form-group">
			                <label class="control-label col-lg-4">
                                <asp:CheckBox runat="server" ID="cbxIncludeAttachment" Checked="true" />
                                <%= Resources.Resource.IncludeDocument %> :</label>
			                <div class="col-lg-8">
				                <div class="input-group">
					                <span class="input-group-addon"><i class="icon-attachment"></i></span>
                                    <asp:DropDownList ID="ddlAttachments" runat="server" CssClass="form-control" Visible="true" />
				                </div>
                                <div class="input-group">
                                    <br />
					                <label><%= Resources.Resource.Attachments %>:&nbsp;</label>
                                    <label id="tbxAttachments"></label> 
				                </div>
			                </div>
		                </div>

                         <div class="panel panel-flat panel-collapsed" runat="server" id="panelOtherDocs">

			                <div class="panel-heading">
				                <h5 class="panel-title"><%= Resources.Resource.OtherRelatedDocuments %></h5>
				                <div class="heading-elements">
					                <ul class="icons-list">
						                <li><a data-action="collapse"></a></li>
					                </ul>
				                </div>
			                </div>

			                <div class="panel-body">

                                <p>
                                    <%= Resources.Resource.OtherRelatedDocuments_ExplainCheck %>
                                </p>

				                <asp:GridView runat="server" ID="gridFiles" AutoGenerateColumns="False" ShowHeader="false">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>
                                                <asp:CheckBox  runat="server" id="cbx" CssClass="cbxAttachment" data-id='<%# Eval("id") %>' data-name='<%# Eval("NameWithExtension") %>' />
                                                <asp:HyperLink runat="server" id="lnk" NavigateUrl='<%# "../DownloadDocument.ashx?id=" + Eval("id") + "&t=" + Eval("FileNameNoExtension") %>'>
                                                    <asp:Label runat="server" id="ico" CssClass='<%# Bind("Icon") %>'></asp:Label>
                                                    &nbsp;
                                                    <asp:Label ID="lbl" runat="server" Text='<%# Bind("NameWithExtension") %>'></asp:Label>
                                                </asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="date" HeaderText="Date" />
                                    </Columns>
                    
				                </asp:GridView>

			                </div>
		                </div>

                        <div class="alert alert-success no-border" id="alertSuccess" style="display: none">
							<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
							<span class="text-semibold"><%= Resources.Resource.WellDone %>!</span> <%= Resources.Resource.EmailSentMessage %>
                            <%= Resources.Resource.WarningEmailArriveInSpam %>, <a href="https://www.gesmobile.ch/envoi-et-reception-de-vos-documents-avec-gesmobile/" target="_blank"><%= Resources.Resource.ClickHereToLearnMore %></a>.
						</div>
                        <div class="alert alert-danger no-border" id="alertError" style="display: none">
							<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
							<span class="text-semibold"><%= Resources.Resource.Error %>.</span> <%= Resources.Resource.EmailIssueMessage %>
						</div>
                        <div class="alert alert-danger no-border" id="alertLimitError" style="display: none">
							<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
							<span class="text-semibold"><%= Resources.Resource.Error %>.</span> <%= Resources.Resource.DailyEmailLimitExceeded %> <a target="_blank" href="mailto:support@gesmobile.ch">support@gesmobile.ch</a>
						</div>

                        <div class="buttonsaction text-right">
							<label class="control-label col-lg-2"></label>
							<div class="col-lg-10">
                                    <button type="button" ID="btnSend" class="btn btn-primary" >
                                        <i class="icon-envelop position-left"></i> <%= Resources.Resource.Send %>
                                    </button>                                                
							</div>
						</div>

                    </fieldset>
                </div>
                

			</div>
		</div>

    </div>
    <div class="col-sm-12 col-md-2"> </div>
</div>
<script type="text/javascript">

    var attachments = [];
    var contacts = <%= ContactsJson %>;
    var deftext = <%= DefaultTextJson %>;
    var textManuallyChanged = false;

    $(function () {

        function protectTwiceCc(email) {

            var sels = $('.cc_cbx_sel input');
            for (var i = 0; i < sels.length; i++) {
                var mail = $($(sels[i]).closest('span')).attr('title');
                if (mail == email) {
                    $($(sels[i]).closest('div')).hide();
                    $(sels[i]).prop('checked', false);
                }
                else {
                    $($(sels[i]).closest('div')).show();
                }
            }

        }
        
        $('#<%= tbxBody.ClientID %>').change(function () {
            textManuallyChanged = true;
        });

        $('#<%= ddlToContact.ClientID %>').change(function () {

            
            var newtext = deftext.t;
            var daid = $('#<%= ddlToContact.ClientID %>').val();
            for (var i = 0; i < contacts.length; i++) {
                if (contacts[i].id == daid) {
                    
                    newtext = newtext.replace(/\$contactfn/gi, contacts[i].contactfn);
                    newtext = newtext.replace(/\$contactln/gi, contacts[i].contactln);
                    newtext = newtext.replace(/\$contacttitle/gi, contacts[i].contacttitle);
                    protectTwiceCc(contacts[i].email);
                }
            }
            if (textManuallyChanged)
            {
                swal({
                    title: '<%= Resources.Resource.ChangeText %>',
                    text: '<%= Resources.Resource.ChangeTextEmailBody %>',
                    showCancelButton: true,
                    cancelButtonText: '<%= Resources.Resource.NoKeep %>',
                    confirmButtonText: '<%= Resources.Resource.YesReset %>'
                })
                .then((value) => {

                    if (value.value != null && value.value == true) {
                        $('#<%= tbxBody.ClientID %>').val(newtext);
                        textManuallyChanged = false;
                    }

                });
            }
            else
            {
                $('#<%= tbxBody.ClientID %>').val(newtext);
                textManuallyChanged = false;
            }

        });

        $('.cbxAttachment').change(displaySelectedAttachments);

        function displaySelectedAttachments() {
            attachments = [];

            var attachmentsText = '[' + $('#<%= ddlAttachments.ClientID %> option:selected').text() +']';

            var selection = $('.cbxAttachment input:checked');
            for (var i = 0; i < selection.length; i++) {
                if ($(selection[i]).is(':checked')) {
                    attachments.push($(selection[i]).parent().data('id'));
                    attachmentsText += " + [" + $(selection[i]).parent().data('name') + "]";
                }
            }
            $('#tbxAttachments').text(attachmentsText);
        }


        protectTwiceCc('<%= SelectedEmail %>');


        $('#btnSend').click(function (e) {
            
            var include = $('#ContentPlaceHolder1_SendScreen1_cbxIncludeAttachment').is(':checked');
            
            var sels_mails = [];
            var sels = $('.cc_cbx_sel input:checked');
            for (var i = 0; i < sels.length; i++) {
                var mail = $($(sels[i]).closest('span')).attr('title');
                sels_mails.push(mail);
            }
          

            $('#btnSend').attr("disabled", true);

            var mail = {};
            mail.subject = $('#ContentPlaceHolder1_SendScreen1_tbxSubject').val();

            var tocontact = $('#ContentPlaceHolder1_SendScreen1_ddlToContact').val();

            if (tocontact == "") tocontact = -1;

            mail.toContact = tocontact;

            mail.toEmail = $('#ContentPlaceHolder1_SendScreen1_tbxCC').val();
            for (var i = 0; i < sels_mails.length; i++) {
                mail.toEmail += "; " + sels_mails[i];
            }
            mail.body = $('#ContentPlaceHolder1_SendScreen1_tbxBody').val();

            var attachments2 = JSON.parse(JSON.stringify(attachments));
            var attachmentSelection = $('#ContentPlaceHolder1_SendScreen1_ddlAttachments :selected').val();
            
            if (include == true) attachments2.push(attachmentSelection);

            mail.attachments = attachments2;           

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: '../../API/Service.svc/SendMail',
                data: JSON.stringify(mail),
                processData: false,
                dataType: "json",
                success: function (response) {
                    $('#alertLimitError').hide();
                    $('#alertError').hide();
                    
                    if (response.success) {
                        $('#alertSuccess').show();
                    }
                    else {
                        $('#alertSuccess').hide();
                        
                        if (response.message === 'Email limit exceeded'){
                            $('#alertLimitError').show();
                        } else{
                            $('#alertError').show();
                        }
                    }

                    $('#btnSend').attr("disabled", false);
                },
                error: function (a, b, c)
                {
                    $('#alertError').show();
                    $('#btnSend').attr("disabled", false);
                }
            });
        });

        $('#<%= ddlAttachments.ClientID %>').change(hideSelectedAttachmentFromGrid);

        function hideSelectedAttachmentFromGrid () {
            var selection = $('#<%= ddlAttachments.ClientID %>').val();
            $('.cbxAttachment').each(function (id, el) {
                var id = $(el).attr("data-id");
                var isselection = id == selection;
                if (isselection) {
                    $(el).closest("tr").hide();
                    $("input:checkbox:checked", el).attr("checked", false);
                }
                else {
                    $(el).closest("tr").show();
                }
            });

            displaySelectedAttachments();
        }

        hideSelectedAttachmentFromGrid();
        displaySelectedAttachments();
    });
</script>