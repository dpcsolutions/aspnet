﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="FirstLogin" Codebehind="FirstLogin.aspx.cs" %>
<!DOCTYPE html>
<html lang="en">
<head runat="server">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="refresh" content="5;url=./" />
	<title>GES</title>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="assets/css/colors.css" rel="stylesheet" type="text/css">
	<link href="assets/css/extras/animate.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/icons/fontawesome/css/all.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>

	<script type="text/javascript" src="assets/js/core/app.js"></script>
	<script type="text/javascript" src="assets/js/pages/login.js"></script>
	<!-- /theme JS files -->

    <style type="text/css">
        .bg-gesmobile
        {
            background: #934553;
            color:white;
        }

        .color-gesmobile
        {
            color: #934553;
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-121845992-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-121845992-1');
    </script>
    <!-- Event snippet for Création du Compte conversion page -->
    <script>
      gtag('event', 'conversion', {'send_to': 'AW-760133643/yEoRCN7vgZYBEIvwuuoC'});
    </script>

    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '857090827960747');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=857090827960747&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->

</head>

<body class="login-container">

        <ul class="nav navbar-nav navbar-right">
			<li class="dropdown language-switch">
				<%= LanguageSwitch %>
			</li>
        </ul>

    <form runat="server">
	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Advanced login -->
					<div>

 

						<div class="panel panel-body login-form ">
							<div class="text-center">
								<!--div class="icon-object border-slate-300 text-slate-300"><i class="icon-user"></i></!--div-->
                                <div><img src="assets/images/logo.png" style="width:140px;height:140px;" class="animated tada" /></div>
								<h5 class="content-group">Bienvenue chez GesMobile<small class="display-block">C'est prêt !</small></h5>
							</div>


                            <div class="alert alert-success alert-bordered" id="lblError" >
								<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only"><%= Resources.Resource.Close %></span></button>
								<span class="text-semibold">

                                    Merci pour votre inscription! <i class="fa fa-smile-o"></i>

								</span>
							</div>


                            <div style="text-align:justify">
                                Votre compte GesMobile est prêt, vous pouvez dès à présent vous connecter.
                            </div>

							
                            <br />

							<div class="form-group">
                                <a class="btn bg-gesmobile btn-block" href="./">
                                    C'est parti <i class="fa fa-thumbs-up"></i>
                                </a>
							</div>

                            <div>
                                <img id="imglinkedin" height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=906642&conversionId=856234&fmt=gif" />
                                <script>
                                  fbq('track', 'StartTrial');
                                </script>
                            </div>


				
						</div>


					</div>
					<!-- /advanced login -->
                    <!-- Footer -->
                    <div class="row  text-muted text-center">
                        &copy; <asp:Label runat="server" ID="LabelCopyYear" />. 
                        <asp:HyperLink runat="server" ID="HyperLinkCopy" />
                    </div>
                    <!-- /footer -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->
        

	</div>
	<!-- /page container -->
    </form>

</body>
</html>
