﻿<%@ Page Title="Employees" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Ressources_OtherResources_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">


        <div class="content">
					<div>
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
										<strong><%= ph.CMP.Name %></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

                            

							<div class="panel-body">
                                <asp:GridView runat="server" ID="grid" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:HyperLinkField DataTextField="code" HeaderText="Code" DataNavigateUrlFields="id" DataNavigateUrlFormatString="Edit.aspx?id={0}"/>
                                        <asp:BoundField DataField="name" HeaderText="Name"   />
                                        <asp:BoundField DataField="description" HeaderText="Description"   />
                                        <asp:BoundField DataField="weeklyHours" HeaderText="WeeklyHours"   />
                                    </Columns>
                                </asp:GridView>
                            </div><!-- panel-body -->

                            <div class="panel-footer text-right no-padding">
						    	<div class="row" style="margin:10px">
						    		<a href="Edit.aspx" class="btn btn-default" runat="server" ID="addResourcesButton"><i class="icon-plus2 position-left"></i> 
                                        <%= Resources.Resource.AddNewOtherResource %>                                  
						    		</a>
						    	</div>
							</div><!-- panel-footer -->

                        </div>
                    </div>
        </div><!-- /content -->

    </form>
</asp:Content>

