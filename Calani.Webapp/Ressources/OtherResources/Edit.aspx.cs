﻿using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.OtherResources;
using System;
using System.Web.UI.WebControls;

public partial class Ressources_OtherResources_Edit : System.Web.UI.Page
{
    public PageHelper ph;

    private OrganizationManager _organizationManager;
    private OtherResourcesManager _otherResourcesManager;

    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);

        ph.ItemLabel = Resources.Resource.Resource_;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        _organizationManager = new OrganizationManager(ph.CurrentOrganizationId);
        _otherResourcesManager = new OtherResourcesManager(ph.CurrentOrganizationId);

        if (!IsPostBack)
        {
            if (ph.CurrentId == null)
            {
                ph.SetStateNew(Resources.Resource.AddNewOtherResource);

                tbxWeeklyHours.Value = _organizationManager.WeekWorkHours.ToString();
            }
            else
            {
                Load();
            }
        }

        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
                btnDelete, btnMore, btnMoreDdl, lblTitle, linkBackList, linkNew);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId == null)
        {
            Create();
        }
        else
        {
            Update();
        }
            
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) Delete();
    }

    protected void wh_customValidator_ServerValidate(object sender, ServerValidateEventArgs e)
    {
        if (string.IsNullOrEmpty(e.Value))
        {
            e.IsValid = false;
            return;
        }

        int weeklyHours;
        e.IsValid = int.TryParse(e.Value, out weeklyHours) && weeklyHours >= 0 && weeklyHours <= 24*7;
    }

    protected void code_customValidator_ServerValidate(object sender, ServerValidateEventArgs e)
    {
        if (string.IsNullOrEmpty(e.Value))
        {
            e.IsValid = false;
            return;
        }

        var isDuplicate = _otherResourcesManager.ResourceWithSuchCodeExists(e.Value, ph.CurrentOrganizationId);

        e.IsValid = !isDuplicate;
    }

    private void Load()
    {
        var item = _otherResourcesManager.Get(ph.CurrentId.Value);

        if(item == null)
        {
            return;
        }

        tbxCode.Value = item.code;
        tbxName.Value = item.name;
        tbxDescription.Value = item.description;
        tbxWeeklyHours.Value = item.weeklyHours.ToString();
    }

    private long Create()
    {
        double weeklyHours = _organizationManager.WeekWorkHours;
        double.TryParse(tbxWeeklyHours.Value, out weeklyHours);

        var model = new other_resources()
        {
            organizationId = ph.CurrentOrganizationId,
            code = tbxCode.Value,
            name = tbxName.Value,
            description = tbxDescription.Value,
            weeklyHours = weeklyHours,

            created_at = DateTime.UtcNow,
            created_by = ph.CurrentUserId
        };

        var ret = _otherResourcesManager.SaveOrUpdate(-1, model);

        long id = 0;

        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Created_var);
            btnSave.Enabled = false;
            id = ret.Record.id;
        }
        else
        {
            ph.DisplayError(ret.ErrorMessage);
        }

        return id;
    }

    private void Update()
    {
        if (ph.CurrentId == null)
        {
            throw new InvalidOperationException("Update Other Resource failed: CurrentId is null.");
        }

        var id = ph.CurrentId.Value;

        var existing = _otherResourcesManager.Get(id);
        if(existing == null)
        {
            throw new InvalidOperationException(string.Format("Other Resource with {0} was not found.", id));
        }

        double weeklyHours = _organizationManager.WeekWorkHours;
        double.TryParse(tbxWeeklyHours.Value, out weeklyHours);

        var entity = new other_resources()
        {
            id = id,
            code = tbxCode.Value,
            name = tbxName.Value,
            description = tbxDescription.Value,
            weeklyHours = weeklyHours,
            organizationId = existing.organizationId,
            created_at = existing.created_at,
            created_by = existing.created_by,
            updated_at = DateTime.UtcNow,
            updated_by = ph.CurrentUserId,
        };

        var ret = _otherResourcesManager.SaveOrUpdate(entity.id, entity);

        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Updated_var);
            Load();
        }
        else
        {
            ph.DisplayError(ret.ErrorMessage);
        }
    }

    protected void Delete()
    {
        if (ph.CurrentId == null)
        {
            throw new InvalidOperationException("Delete Other Resource failed: CurrentId is null.");
        }

        var ret = _otherResourcesManager.Delete(ph.CurrentId.Value);

        if (ret.Success)
        {
            panelEdit.Visible = false;
            ph.DisplaySuccess(Resources.Resource.Deleted_var);
        }
        else
        {
            ph.DisplayError(ret.ErrorMessage);
        }
    }
}