﻿using Calani.BusinessObjects.OtherResources;
using System;
using System.Linq;

public partial class Ressources_OtherResources_Default : System.Web.UI.Page
{
    public PageHelper ph;

    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        EnableViewState = false;

        if (!IsPostBack)
        {
            OtherResourcesManager mgr = new OtherResourcesManager(ph.CurrentOrganizationId);

            var list = mgr.GetOtherResources(ph.CurrentOrganizationId);
            grid.DataSource = list;

            new Html5GridView(grid);
        }
    }
}