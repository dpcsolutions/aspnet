﻿<%@ Page Title="Edit resource" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Ressources_OtherResources_Edit" Codebehind="Edit.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    <%--Canceled Seubi css for page to apply theme style--%>
    <style type="text/css">
        .table > thead > tr > th {
            border-bottom: none;  
            background: none; 
            color: black;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <form runat="server">

        <div class="content">

		    <div class="row">

                <!-- left column -->
			    <div class="col-sm-12 col-md-8">

                        <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Error %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblErrorMessage" />
						    </div>
					    </asp:Panel><!-- panelError -->

                        <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Done %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblSuccess" />
                                <br />
                                <strong><a href="." id="linkBackList" runat="server"></a></strong>
                                    <%= Resources.Resource.or %>
                                <a href="Edit.aspx" id="linkNew" runat="server"></a>.
						    </div>
					    </asp:Panel><!-- panelSuccess -->
					    
					    <asp:Panel runat="server" ID="panelEdit" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><asp:Label runat="server" ID="lblTitle" /></h6>
						    </div>

						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">

                                        <!-- code -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Code %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                                    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Code %>' runat="server" id="tbxCode" maxlength="50" />
											    </div>
                                                <div class="validation_message">
                                                    <asp:RequiredFieldValidator ControlToValidate="tbxCode" ErrorMessage="Code is required" runat="server" />
													<asp:CustomValidator runat="server" id="CustomValidator2" controltovalidate="tbxCode" onservervalidate="code_customValidator_ServerValidate" errormessage="Invalid code" />
                                                </div>
										    </div>
									    </div>
                                        <!-- /code -->

                                        <!-- name -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Name %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-font"></i></span>
												    <input type="text" required class="form-control" placeholder='<%$ Resources:Resource, Name %>' runat="server" id="tbxName" maxlength="200" />
											    </div>
                                                <div class="validation_message">
                                                    <asp:RequiredFieldValidator ControlToValidate="tbxName" ErrorMessage="Name is required" runat="server" />
                                                </div>
										    </div>
									    </div>
                                        <!-- /name -->

                                        <!-- name -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Description %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-font"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Description %>' runat="server" id="tbxDescription" maxlength="500" />
											    </div>
										    </div>
									    </div>
                                        <!-- /name -->

								    </fieldset>

                                
                                <fieldset class="content-group">
                                    <legend class="text-bold"><%= Resources.Resource.Hours %></legend>

                                    <!-- WeeklyHours -->
                                    <div class="form-group">
                                        <label class="control-label col-lg-2"><%= Resources.Resource.WeeklyHours %></label>
                                        <div class="col-lg-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                <input type="number" class="form-control" step="any" runat="server" id="tbxWeeklyHours" min="0" max="168" />
                                                <span class="input-group-addon"><%= Resources.Resource.WeeklyHours_Tooltip %></span>
                                            </div>
                                            <div class="validation_message">
                                                <asp:RequiredFieldValidator ControlToValidate="tbxWeeklyHours" ErrorMessage="Weekly hours are required" runat="server" />
                                                <asp:CustomValidator runat="server" id="wh_customValidator" controltovalidate="tbxWeeklyHours" onservervalidate="wh_customValidator_ServerValidate" errormessage="Invalid weekly hours" />
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /WeeklyHours -->
                                </fieldset>

                                    <!-- buttonsaction -->
								    <div class="buttonsaction text-right">
										<label class="control-label col-lg-2"></label>
										<div class="col-lg-10">
											<div class="btn-group">
                                                <asp:LinkButton runat="server" ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click">
                                                    <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                                </asp:LinkButton>
                                                
									            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore"><span class="caret"></span></button>
									            <ul class="dropdown-menu dropdown-menu-right" runat="server" id="btnMoreDdl">
                                                    <li><a href="javascript:void(0)" data-toggle="modal" data-target="#modal_default"><i class="fa fa-trash"></i> <%= Resources.Resource.Delete %></a></li>
												</ul>
											</div>
										</div>
									</div>
                                    <!-- /buttonsaction -->


							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->
					    
			    </div>
			    <!-- /left column -->
                					

		    </div> <!-- /row -->


					
            <!-- basic modal: confirm delete -->
		    <div id="modal_default" class="modal fade">
			    <div class="modal-dialog">
				    <div class="modal-content">
					    <div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal">&times;</button>
						    <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
					    </div>

					    <div class="modal-body">
						    <h6 class="text-semibold"><%= Resources.Resource.Are_You_Sure_To_Delete %></h6>
						    <p><%= Resources.Resource.Delete_Is_Irreversible %></p>

					    </div>

					    <div class="modal-footer">
						    <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
                            <asp:Button runat="server" ID="btnDelete" OnClick="btnDelete_Click" CssClass="btn btn-primary" Text="<%$ Resources:Resource, Yes_Delete %>" />

					    </div>
				    </div>
			    </div>
		    </div>
		    <!-- /basic modal -->

             <div id="modal_contract" class="modal fade">
        </div>
       

        </div><!-- /content -->

    </form>
</asp:Content>