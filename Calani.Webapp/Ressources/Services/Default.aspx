﻿<%@ Page Title="Services" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Ressources_Services_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="ContentQuickMenu" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
    <div class="btn-group" runat="server" id="quickmenu">
		<button type="button" class="btn bg-gesmobile btn-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<i class="icon-menu7"></i> &nbsp;<span class="caret"></span>
		</button>

		<ul class="dropdown-menu dropdown-menu-right">
			<li><a href="Edit.aspx"><i class="icon-plus2"></i> <%= Resources.Resource.Add_new %> <%= Resources.Resource.Service %></a></li>
            <li class="divider"></li>
            <li><a href="./Import/"><i class="icon-file-upload"></i> <%= Resources.Resource.ImportFile %></a></li>
		</ul>

	</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">


        <div class="content">
					<div>
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
										<strong><%= ph.CMP.Name %></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

							<div class="panel-body">
                                <asp:GridView runat="server" ID="grid" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:HyperLinkField DataTextField="name" HeaderText="Name" DataNavigateUrlFields="id" DataNavigateUrlFormatString="Edit.aspx?id={0}"/>
                                        <asp:BoundField DataField="internalId" HeaderText="RefNr"   />
                                        <asp:BoundField DataField="unitPrice" HeaderText="Unit_Price" ItemStyle-CssClass="text-right" DataFormatString="{0:F2}" HtmlEncode="false" HeaderStyle-CssClass="text-right" />
                                        <asp:BoundField DataField="unitName" HeaderText="Unit" ItemStyle-CssClass="text-right" HeaderStyle-CssClass="text-right" />
                                    </Columns>
                                </asp:GridView>
                            </div><!-- panel-body -->

                            <div class="panel-footer text-right no-padding">

                                

						    	<div class="row" style="margin:10px">

                                    <a href="./Import/" class="btn btn-default"><i class="icon-file-upload position-left"></i> 
                                        <%= Resources.Resource.ImportFile %>
						    	    </a>

						    		<a href="Edit.aspx" class="btn btn-default"><i class="icon-plus2 position-left"></i> 
                                        <%=Resources.Resource.Add_new_Service %>
                                      
						    		</a>
						    	</div>
							</div><!-- panel-footer -->

                        </div>
                    </div>
        </div><!-- /content -->

    </form>
</asp:Content>

