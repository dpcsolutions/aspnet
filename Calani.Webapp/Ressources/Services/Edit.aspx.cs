﻿using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.FrontEndModels;
using System;
using System.Collections.Generic;
using System.Linq;

public partial class Ressources_Services_Edit : System.Web.UI.Page
{
    public PageHelper ph;

    public List<UnitModel> Units { get; set; }
    public string SavedUnitName { get; set; }

    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        var orgMgr = new OrganizationManager(ph.CurrentOrganizationId);
        orgMgr.Load();
        
        Units = GetUnits(ph.CurrentOrganizationId);

        tbxInternalId.Value = tbxInternalId.Value.Replace("-", "").Replace(" ", "").Trim();

        ph.ItemLabel = Resources.Resource.Service;
        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
            btnDelete, btnMore, btnMoreDdl, lblTitle, linkBackList, linkNew);


        string s = ddlTax.SelectedValue;

        if (!IsPostBack)
        {
            var taxesMgr = new Calani.BusinessObjects.CustomerAdmin.TaxesManager(ph.CurrentOrganizationId);
            var taxes = taxesMgr.List();
            taxes.Insert(0, new Calani.BusinessObjects.Model.taxes
            {
                id = -1,
                name = "---"
            });

            ddlTax.DataSource = taxes;
            var id = (from r in taxes where r.isDefault == true select r.id).FirstOrDefault();
            if(id > 0) ddlTax.SelectedValue = id.ToString();
            ddlTax.DataBind();
        }

        if (!IsPostBack && ph.CurrentId != null)
        {
            Load();
            ph.SetStateEdit(Resources.Resource.Edit_service);
        }

        if (ph.CurrentId == null)
        {
            ph.SetStateNew(Resources.Resource.Add_new_Service);
        }

        if (!orgMgr.UsePriceIncludingVat)
        {
            panelPriceInclTax.Style.Add("display", "none");
        }
    }
     
    #region CRUD operations

    // Load from db and bind controls
    private void Load()
    {
        var mgr = new Calani.BusinessObjects.CustomerAdmin.ServicesManager(ph.CurrentOrganizationId);
        var item = mgr.Get(ph.CurrentId.Value);

        cbxPriceIncludingVat.Checked = item.price_including_vat;
        tbxName.Value = item.name;
        tbxInternalId.Value = item.internalId;
        tbxUnitPrice.Value = Convert2.ToString(item.unitPrice);
        ddlTax.SelectedValue = Convert2.ToString(item.taxId);
        SavedUnitName = GetUnitById(item.unitId ?? 0).Name;
    }

    // Create a new db record from controls values
    private void Update()
    {
        var mgr = new Calani.BusinessObjects.CustomerAdmin.ServicesManager(ph.CurrentOrganizationId);

        var selectedUnit = GetSelectedUnit();

        if (selectedUnit.Id == 0 && !string.IsNullOrWhiteSpace(selectedUnit.Name))
        {
            var unitsMgr = new UnitsManager(ph.CurrentOrganizationId);

            // TODO: handle error
            selectedUnit.Id = unitsMgr.AddCustomUnit(selectedUnit.Name);

            Units = GetUnits(ph.CurrentOrganizationId);
        }

        var ret = mgr.Update(ph.CurrentId.Value,
                        new Calani.BusinessObjects.Model.services
                        {
                            name = tbxName.Value,
                            price_including_vat = cbxPriceIncludingVat.Checked,
                            internalId = tbxInternalId.Value,
                            unitPrice = Convert2.ToDouble(tbxUnitPrice.Value, 0, 2, -99999, 99999),
                            taxId = MinusOneIsNull(Convert2.ToNullableLong(ddlTax.SelectedValue)),
                            organizationId = ph.CurrentOrganizationId,
                            unitId = selectedUnit.Id > 0 ? (long?)selectedUnit.Id : null,
                        });

        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Updated_var);
            Load();
        }
        else ph.DisplayError(ret.ErrorMessage);
    }

    private long? MinusOneIsNull(long? v)
    {
        if (v != null && v.Value == -1) return null;
        return v;
    }

    private List<UnitModel> GetUnits(long orgId)
    {
        var unitsService = new UnitsService(orgId);
        var models = unitsService.GetUnits(System.Globalization.CultureInfo.CurrentCulture);
        return models;
    }

    private UnitModel GetUnitById(long id)
    {
        if(Units != null && Units.Any())
        {
            return Units.SingleOrDefault(u => u.Id == id);
        }

        return new UnitModel();
    }

    private UnitModel GetSelectedUnit()
    {
        if(Units != null && Units.Any() && !string.IsNullOrWhiteSpace(hdnUnitName.Value))
        {
            var unit = Units.SingleOrDefault(u => u.Name == hdnUnitName.Value.Trim())
                ?? new UnitModel() 
                {
                    OrganizationId = ph.CurrentOrganizationId,
                    Name = hdnUnitName.Value.Trim()
                };

            return unit;
        }

        return new UnitModel();
    }

    // Update existing db record from new controls values
    private void Create()
    {
        var mgr = new Calani.BusinessObjects.CustomerAdmin.ServicesManager(ph.CurrentOrganizationId);

        var selectedUnit = GetSelectedUnit();

        if(selectedUnit.Id == 0 && !string.IsNullOrWhiteSpace(selectedUnit.Name))
        {
            var unitsMgr = new UnitsManager(ph.CurrentOrganizationId);

            // TODO: handle error
            selectedUnit.Id = unitsMgr.AddCustomUnit(selectedUnit.Name);

            Units = GetUnits(ph.CurrentOrganizationId);
        }

        var ret = mgr.Add(new Calani.BusinessObjects.Model.services
        {
            name = tbxName.Value,
            internalId = tbxInternalId.Value,
            price_including_vat = cbxPriceIncludingVat.Checked,
            unitPrice = Convert2.ToDouble(tbxUnitPrice.Value, 0, 2, -99999, 99999),
            taxId = MinusOneIsNull(Convert2.ToNullableLong(ddlTax.SelectedValue)),
            organizationId = ph.CurrentOrganizationId,
            type = Convert.ToInt32(Calani.BusinessObjects.CustomerAdmin.ServiceType.BillableServices),
            unitId = selectedUnit.Id == 0 ? null : (long?)selectedUnit.Id
        });

        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Created_var);
            btnSave.Enabled = false;
        }
        else ph.DisplayError(ret.ErrorMessage);
    }

    protected void Delete()
    {
        var mgr = new Calani.BusinessObjects.CustomerAdmin.ServicesManager(ph.CurrentOrganizationId);
        var ret = mgr.Delete(ph.CurrentId.Value);
        if (ret.Success)
        {
            panelEdit.Visible = false;
            ph.DisplaySuccess(Resources.Resource.Deleted_var);
        }
    }

    #endregion



    #region Buttons (triggers)
    protected void btnSave_Click(object sender, EventArgs e)
    {

        if(string.IsNullOrWhiteSpace(tbxName.Value) || string.IsNullOrWhiteSpace(tbxInternalId.Value))
        {
            ph.DisplayError(Resources.Resource.FillAllRequiredFields);
        }
        else
        {
            if (ph.CurrentId != null)
                Update();
            else
                Create();
        }

        
    }


    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) Delete();
    }
    #endregion





}