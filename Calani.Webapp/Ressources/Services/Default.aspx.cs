﻿using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.FrontEndModels;
using System;
using System.Collections.Generic;
using System.Linq;

public partial class Ressources_Services_Default : System.Web.UI.Page
{
    public PageHelper ph;

    private OrganizationManager _organizationManager;

    public bool ShowUnitColumn { get; set; }

    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        EnableViewState = false;
        if (!IsPostBack)
        {
            _organizationManager = new OrganizationManager(ph.CurrentOrganizationId);
            _organizationManager.Load();
            ShowUnitColumn = !_organizationManager.HideUnitColumn;

            var units = ShowUnitColumn ? GetUnits(ph.CurrentOrganizationId).ToDictionary(u => u.Id) : new Dictionary<long, UnitModel>();

            var mgr = new Calani.BusinessObjects.CustomerAdmin.ServicesManager(ph.CurrentOrganizationId);
            grid.DataSource = mgr.List().Select(s => new {
                id = s.id,
                name = s.name,
                internalId = s.internalId,
                unitPrice = s.unitPrice,
                unitName = ShowUnitColumn && s.unitId.HasValue
                        ? (units.ContainsKey(s.unitId.Value) ? units[s.unitId.Value].Name : s.unitId.Value.ToString())
                        : (s.unitId.ToString() ?? ""),
            });
            grid.DataBind();
            grid.Columns[3].Visible = ShowUnitColumn;
            new Html5GridView(grid);
        }
    }

    private List<UnitModel> GetUnits(long orgId)
    {
        var unitsService = new UnitsService(orgId);
        var models = unitsService.GetUnits(System.Globalization.CultureInfo.CurrentCulture);
        return models;
    }
}