﻿<%@ Page Title="Import" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Ressources_IntJobs_Import_Default" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderQuickMenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">
        <div class="content">

            <div class="row" runat="server" id="panelIntro">


                <div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="icon-file-upload display-inline-block text-<%= ph.CMP.Color %>"></i>
										<strong><%= Resources.Resource.ImportInternalJobs %></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

                            <div class="panel-body">
                               
                                <p>
                                    <%= Resources.Resource.ImportInternalJobsFileIntro %>
                                    <%= Resources.Resource.WeSuggestYouToDownload %> 
                                    <a href="demo.xlsx" type="button" class="btn btn-xs btn-info" style="min-height:inherit !important"><i class="icon-file-spreadsheet position-left"></i> <%= Resources.Resource.ExcelSampleFile %> </a>
                                    <%= Resources.Resource.AndToKeepExcelFileFormat %>
                                    

                                </p>

                            </div>


                            
                </div>
                <asp:HiddenField runat="server" ID="hfFile" />
            </div>


            <div class="row" runat="server" id="panelUpload">

                <div class="panel panel-white">
                    
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-6 col-md-6">
                                
                                <div id="dropzone_file" class="fallback dropzone dz-clickable">
                              
                                </div>

                            </div>
                            <div class="col-sm-6 col-md-6">


                                <div>
                                    <strong><%= Resources.Resource.ColumnsToInclude %></strong>
                                </div>
                                <ul>
                                    <li><strong>Ref* / Code* (Unique)</strong></li>
                                    <li><strong>Nom* / Name* </strong></li>
                                    <li><strong>Coût Unit.* / Unit Cost.*</strong></li>                
                                </ul>

                            </div>
                        </div>
                        
                        <style type="text/css">
                            .dropzone {
                                min-height: 400px !important;
                            }
                        </style>
                        <script type="text/javascript">

                            var ressources = {};
                            ressources['DropFilesToUpload'] = "<%= Resources.Resource.DropExcelFileToUpload %>";
                            ressources['OrClick'] = "<%= Resources.Resource.OrClick %>";

                        

                            $(function () {
                                Dropzone.autoDiscover = false;
                                $("#dropzone_file").dropzone({
                                    uploadMultiple: false,
                                    url: "Upload.ashx",
                                    dictDefaultMessage: ressources['DropFilesToUpload'] + ' <span>' + ressources['OrClick'] + '</span>',
                                    maxFilesize: 30,
                                    init: function () {
                                        this.on("complete", function (file) {

                                            if (file.xhr.status = 200) {
                                                $('#ContentPlaceHolder1_hfFile').val(file.xhr.responseText);
                                            
                                            }
                        
                                        });
                                    }
                                });
                            });


                            function checkFile() {
                                var f = $('#ContentPlaceHolder1_hfFile').val();
                                if (f != null && f != '' && f.length > 3) return true;

                                alert("<%= Resources.Resource.UploadFileFirst %>");
                                return false
                            }
                        </script>
                    </div>
                    <div class="panel-footer text-right no-padding">
                        <div class="row" style="margin:10px">
                            <asp:LinkButton CssClass="btn btn-success" runat="server" ID="btnUploadNext" OnClientClick="return checkFile()" OnClick="btnUploadNext_Click" >
                                <i class="icon-upload7 position-left"></i> 
                                <%= Resources.Resource.ImportFile %>
						    </asp:LinkButton>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row" runat="server" id="panelSetupColumns" visible="false">
                <div class="panel panel-white">
                    <div class="panel-heading">
						<h6 class="panel-title">
							<a name="quotes">
								<i class="icon-file-upload display-inline-block text-<%= ph.CMP.Color %>"></i>
								<strong><%= Resources.Resource.ImportInternalJobs %></strong>
							</a>
						</h6>
					</div><!-- panel-heading -->

                    <div class="panel-body">

                        <p>
                            <strong><%= Resources.Resource.SetColumnsPreview5lines %></strong>
                        </p>

                        <asp:GridView runat="server" ID="grid" CssClass="table table-hscroll" />
                        <asp:HiddenField runat="server" ID="tbxColumnsToImport" />
                        <script type="text/javascript">
                            $(function () {

                                
                                var ddlCols = $('.ddlCol');
                                ddlCols.change(function () {
                                    var cols = "";
                                    for (var i = 0; i < ddlCols.length; i++) {
                                        var col = $("#ddlCol" + i).val();
                                        cols += col;
                                        cols += ";";
                                    }

                                    $('#<%=tbxColumnsToImport.ClientID %>').val(cols);
                                })
                                

                            });
                        </script>
                    </div>
                    <div class="panel-footer text-right no-padding">
                        <div class="row" style="margin:10px">
                            <asp:LinkButton CssClass="btn btn-success" runat="server" ID="btnSetColumns" OnClick="btnSetColumns_Click" >
                                <i class="icon-magic-wand position-left"></i> 
                                <%= Resources.Resource.ImportData %>
						    </asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" runat="server" id="panelFinish" visible="false">
                <div class="panel panel-white">
                    <div class="panel-heading">
						<h6 class="panel-title">
							<a name="quotes">
								<i class="icon-file-upload display-inline-block text-<%= ph.CMP.Color %>"></i>
								<strong><%= Resources.Resource.ImportInternalJobs %></strong>
							</a>
						</h6>
					</div><!-- panel-heading -->

                    <div class="panel-body">

                        <p>
                            <strong><%= Resources.Resource.ImportDone %></strong>
                            <a href="../"><%= Resources.Resource.InternalJobs %></a>
                        </p>

                        <p>
                            <%= Resources.Resource.ItemsImported %>: 
                            <asp:Label runat="server" ID="lblImportCount" />
                        </p>

                        <p>
                            <asp:DataGrid runat="server" ID="repeatedCustomers" AutoGenerateColumns="false" CssClass="table table-hover" style="width:auto;" ShowHeader="false">
                                <Columns>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <i class='fa fa-plus'></i> <%# Eval("Name")  %> 
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <a href="../Edit.aspx?id=<%# Eval("id")  %>" class="text-slate" target="_blank">
                                               [<%= Resources.Resource.EditInNewTab %>] 
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>

            
                        </p>

                        
                    </div>
                    
                </div>
            </div>


        </div>
    </form>
</asp:Content>

