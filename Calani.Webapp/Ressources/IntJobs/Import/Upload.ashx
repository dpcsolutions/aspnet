﻿,<%@ WebHandler Language="C#" Class="Upload" %>

using System;
using System.Web;
using Calani.BusinessObjects.Imports;

public class Upload : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";


        long orgId = PageHelper.GetCurrentOrganizationId(context.Session);

        string dir = System.IO.Path.Combine(FileImportManager.DIR, orgId.ToString());
        if (!System.IO.Directory.Exists(dir)) System.IO.Directory.CreateDirectory(dir);

        if (context.Request.Files.Count == 1)
        {

            HttpPostedFile file = context.Request.Files[0];
            string fileName = file.FileName;
            string fileType = file.ContentType;



            if (true/*fileType == "application/octet-stream"*/)
            {
                byte[] fileData = null;
                using (var binaryReader = new System.IO.BinaryReader(file.InputStream))
                {
                    fileData = binaryReader.ReadBytes(file.ContentLength);
                }


                FileImportManager importer = new FileImportManager(fileData, "intjobs");
                if (importer.CanBeImported)
                {


                    string name = importer.Save(orgId);



                    context.Response.Write(name);
                }
                else
                {
                    throw new Exception("Not compatible file");
                }

            }
            else
            {
                throw new Exception("File is not an image");
            }
        }
        else
        {
            throw new Exception("Can not upload multiple files");
        }



    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}