﻿using Calani.BusinessObjects.Enums;
using System;

public partial class Ressources_IntJobs_Edit : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ph.ItemLabel = Resources.Resource.InternalJob;
        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
            btnDelete, btnMore, btnMoreDdl, lblTitle, linkBackList, linkNew);


        

        if (!IsPostBack && ph.CurrentId != null)
        {
            Load();
            ph.SetStateEdit(Resources.Resource.Edit_InternalJob);
        }

        if (ph.CurrentId == null)
        {
            ph.SetStateNew(Resources.Resource.Add_new_InternalJob);
        }
    }




    #region CRUD operations
    // Load from db and bind controls
    private void Load()
    {
        Calani.BusinessObjects.CustomerAdmin.InternalJobsManager mgr = new Calani.BusinessObjects.CustomerAdmin.InternalJobsManager(ph.CurrentOrganizationId);
        var item = mgr.Get(ph.CurrentId.Value);

        tbxName.Value = item.name;
        tbxInternalId.Value = item.internalId;
        tbxUnitPrice.Value = Convert2.ToString(item.unitPrice);
        cbxInHours.Checked = (ServiceUnitEnum)item.unit == ServiceUnitEnum.Hour;
    }

    // Create a new db record from controls values
    private void Update()
    {
        Calani.BusinessObjects.CustomerAdmin.InternalJobsManager mgr = new Calani.BusinessObjects.CustomerAdmin.InternalJobsManager(ph.CurrentOrganizationId);

        var ret = mgr.Update(ph.CurrentId.Value,
                        new Calani.BusinessObjects.Model.services
                        {
                            name = tbxName.Value,
                            internalId = tbxInternalId.Value,
                            unitPrice = Convert2.ToDouble(tbxUnitPrice.Value, 0, 2, 0, 99999),
                            organizationId = ph.CurrentOrganizationId,
                            type = Convert.ToInt32(Calani.BusinessObjects.CustomerAdmin.ServiceType.InternalJobs),
                            unit = (sbyte)(cbxInHours.Checked ? ServiceUnitEnum.Hour : ServiceUnitEnum.Unknown)
                        });
        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Updated_var);
            Load();
        }
        else ph.DisplayError(ret.ErrorMessage);
    }


    // Update existing db record from new controls values
    private void Create()
    {
        Calani.BusinessObjects.CustomerAdmin.InternalJobsManager mgr = new Calani.BusinessObjects.CustomerAdmin.InternalJobsManager(ph.CurrentOrganizationId);

        var ret = mgr.Add(new Calani.BusinessObjects.Model.services
        {
            name = tbxName.Value,
            internalId = tbxInternalId.Value,
            unitPrice = Convert2.ToDouble(tbxUnitPrice.Value, 0, 2, 0, 99999),
            organizationId = ph.CurrentOrganizationId,
            type = Convert.ToInt32(Calani.BusinessObjects.CustomerAdmin.ServiceType.InternalJobs),
            unit = (sbyte)(cbxInHours.Checked ? ServiceUnitEnum.Hour : ServiceUnitEnum.Unknown)
        });
        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Created_var);
            btnSave.Enabled = false;
        }
        else ph.DisplayError(ret.ErrorMessage);
    }

    protected void Delete()
    {
        Calani.BusinessObjects.CustomerAdmin.InternalJobsManager mgr = new Calani.BusinessObjects.CustomerAdmin.InternalJobsManager(ph.CurrentOrganizationId);
        var ret = mgr.Delete(ph.CurrentId.Value);
        if (ret.Success)
        {
            panelEdit.Visible = false;
            ph.DisplaySuccess(Resources.Resource.Deleted_var);
        }
    }
    #endregion



    #region Buttons (triggers)
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) Update();
        else Create();
    }


    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) Delete();
    }
    #endregion





}