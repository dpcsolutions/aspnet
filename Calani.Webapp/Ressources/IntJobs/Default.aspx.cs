﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Ressources_IntJobs_Default : System.Web.UI.Page
{
    public PageHelper ph;
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        EnableViewState = false;
        if (!IsPostBack)
        {
            Calani.BusinessObjects.CustomerAdmin.InternalJobsManager mgr = new Calani.BusinessObjects.CustomerAdmin.InternalJobsManager(ph.CurrentOrganizationId);
            grid.DataSource = mgr.List();
            grid.DataBind();
            new Html5GridView(grid);
        }
    }
}