﻿using Calani.BusinessObjects.AbsenceRequests;
using Calani.BusinessObjects.AbsenceRequests.Models;
using Calani.BusinessObjects.Attachments;
using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Scheduler;
using Calani.BusinessObjects.TimeManagement;
using Calani.BusinessObjects.TimeManagement.Models;
using Calani.BusinessObjects.TimeManagement.Services;
using Calani.BusinessObjects.WorkSchedulers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;

public partial class Ressources_Employees_Edit : System.Web.UI.Page
{
    private const int DefaultWeeklyHoursValue = 40;
    public PageHelper ph;
    private EmployeesManager employeesManager;
    private EmployeesManager.EmployeesContractsManager contractsManager;
    private Calani.BusinessObjects.CustomerAdmin.OrganizationManager organizationManager;
    private VacationCarriedOverDaysManager vacationCarriedOverDaysManager;
    private EmployeesOvertimeManager employeesOvertimeManager;
    private TimesheetReportService timesheetReportService;

    public double weekHours = 40;
    public double WeekHoursOrg { get; private set; }
    public int VacationsPerYear { get; private set; }
    public int VacationsPerYearOrganizationDefault { get; private set; }
    private int defaultVacationsPerYear = 20;
    public bool isOvertimeEnabled = false;
    public string activeContractRate1 = string.Empty;
    public string activeContractRate2 = string.Empty;

    public bool IsEnglish = true;

    public bool HasPreviousContracts { get; private set; }

    public string SendPage
    {
        get
        {
            string ret = "";
            for (int i = 0; i < Request.Url.Segments.Length - 1; i++)
            {
                ret += Request.Url.Segments[i];
            }
            return ret + "Send.aspx";
        }
    }

    public long ProjectId = 0;

    public string JsonAttachments { get; private set; }
    public Dictionary<long, string> Attachments = new Dictionary<long, string>();

    public string IsReadOnly = "false";

    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }

    private void BindLicenseWidget()
    {

        int freeIncludedLicenses = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NewAccountUsersLimit"]);

        bool inTrial = false;
        int licencesUsed = 0;
        int licencesAvailable = 1;

        CalaniEntities db = new Calani.BusinessObjects.Model.CalaniEntities();
        var org = (from r in db.organizations where r.id == ph.CurrentOrganizationId select r).FirstOrDefault();
        if (org != null)
        {
            // to restore 4pat
            
            if (org.userLimit != null) licencesAvailable = org.userLimit.Value;
            inTrial = (org.trialEnd != null && org.trialEnd > DateTime.Now);
        }

        if(org.subscriptionStatus == "Partnership_Active")
        {
            inTrial = true; // dans ces conditions on ne check pas 
        }

        Calani.BusinessObjects.Contacts.EmployeesManager mgr = new Calani.BusinessObjects.Contacts.EmployeesManager(ph.CurrentOrganizationId);

        licencesUsed = mgr.CountEmployees(true) - freeIncludedLicenses;
        if (licencesUsed < 0) licencesUsed = 0;


        if (!inTrial)
        {
            if (licencesAvailable - licencesUsed <= 0)
            {
                ph.DisplayError("No more license available");
                panelEdit.Visible = false;
            }




            if (licencesAvailable > 0)
            {
                lblLicencesUsed.Text = licencesUsed.ToString() + "/" + licencesAvailable.ToString();
                lblLicencesUsedProgress.ApplyStyle(new Style() { Width = new Unit((licencesUsed * 100 / licencesAvailable), UnitType.Percentage) });
            }
            else
            {
                panelLicenses.Visible = false;
            }
            panelLicenses.Visible = true;
        }
        else
        {
            panelLicenses.Visible = false;
        }
    }

    public class CarriedOverDaysPerYear
    {
        public int Year { get; set; }
        public decimal Days { get; set; }
    }

    public List<CarriedOverDaysPerYear> CarriedOverDaysPerYears { get; set; }

    public TimesheetReportModel TimesheetsReport { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        ph.ItemLabel = Resources.Resource.Employee;
        employeesManager = new EmployeesManager(ph.CurrentOrganizationId);

        contractsManager = new EmployeesManager.EmployeesContractsManager(ph.CurrentOrganizationId);
        vacationCarriedOverDaysManager = new VacationCarriedOverDaysManager(ph.CurrentOrganizationId);
        employeesOvertimeManager = new EmployeesOvertimeManager(ph.CurrentOrganizationId);
        organizationManager = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(ph.CurrentOrganizationId);
        organizationManager.Load();
        timesheetReportService = new TimesheetReportService(
                                            contractsManager,
                                            new TimeSheetsService(new TimeSheetsManager(ph.CurrentOrganizationId)),
                                            new TimeSheetRecordsManager(ph.CurrentOrganizationId),
                                            employeesOvertimeManager,
                                            new AbsenceRequestsManager(ph.CurrentOrganizationId, contractsManager, employeesManager),
                                            new OrganizationManager(ph.CurrentOrganizationId),
                                            new WorkScheduleRecManager(ph.CurrentOrganizationId));
        TimesheetsReport = new TimesheetReportModel();

        weekHours = organizationManager.WeekWorkHours > 0 ? organizationManager.WeekWorkHours : weekHours;
        WeekHoursOrg = weekHours;
        VacationsPerYearOrganizationDefault = organizationManager.Vacation;

        ph.InitCrudControls(panelSuccess, lblSuccess, panelError, lblErrorMessage,
            btnDelete, btnMore, btnMoreDdl, lblTitle, linkBackList, linkNew);

        if (!organizationManager.UseWorkSchedule)
        {
            DefaultWorkSchedule.Visible = false;
        }
        else
        {
            var scheduleMgr = new Calani.BusinessObjects.WorkSchedulers.WorkScheduleManager(ph.CurrentOrganizationId);
            var schedulersList = scheduleMgr.GetScheduleDtos();
            ddlWorkSchedule.DataSource = schedulersList;
            ddlWorkSchedule.DataBind();
        }


        if (ph.CurrentId == null)
        {
            BindLicenseWidget();
            divContracts.Visible = false;            
            tbxWeeklyHours.Value = weekHours.ToString(CultureInfo.InvariantCulture);            
            var vacantionsPerYear = organizationManager.Vacation < 0 ? defaultVacationsPerYear : organizationManager.Vacation;
            tbxVacationsPerYear.Value = vacantionsPerYear.ToString(CultureInfo.InvariantCulture);
            ph.SetStateNew(Resources.Resource.Add_new_Employee);
        }

        if (!IsPostBack)
        {
            Calani.BusinessObjects.Departments.DepartmentsManager departmentMgr = new Calani.BusinessObjects.Departments.DepartmentsManager(Convert.ToInt16(ph.CurrentOrganizationId));
            var departments = departmentMgr.List();

            departments.Insert(0, new departments
            {
                id = -1,
                name = "---"
            });

            ddlDepartment.DataSource = departments;
            var id = (from r in departments select r.id).FirstOrDefault();
            ddlDepartment.SelectedValue = id.ToString();
            ddlDepartment.DataBind();
        }

        if (ph.CurrentId != null)
        {
            divContracts.Visible = true;

            if (!IsPostBack)
            {
                var user = employeesManager.Get(ph.CurrentId.Value);
                HasPreviousContracts = user.contacts_contracts.Any();

                Calani.BusinessObjects.Departments.DepartmentsManager departmentMgr = new Calani.BusinessObjects.Departments.DepartmentsManager(Convert.ToInt16(ph.CurrentOrganizationId));
                var departments = departmentMgr.List();

                departments.Insert(0, new departments
                {
                    id = -1,
                    name = "---"
                });

                var id = (from r in departments select r.id).FirstOrDefault();
                if (user.departmentId != null) ddlDepartment.SelectedValue = user.departmentId.ToString();
                else ddlDepartment.SelectedValue = id.ToString();
                ddlDepartment.DataBind();

                if (user.work_schedule_profile_id.HasValue)
                {
                    var ddlItem = ddlWorkSchedule.Items.FindByValue(user.work_schedule_profile_id.ToString());
                    if (ddlItem != null)
                        ddlItem.Selected = true;
                }               

                Load(user);
                ph.SetStateEdit(Resources.Resource.Edit_Employee);                
            }

        }

        IsEnglish = Page.UICulture.ToLowerInvariant().Contains("english");
        JsonAttachments = "{ attachments: [] }";
    }


    #region CRUD operations

    // Load from db and bind controls
    private void Load(contacts user = null)
    {
        if (user == null)
        {
            user = employeesManager.Get(ph.CurrentId.Value);
        }

        tbxLastName.Value = user.lastName;
        tbxFirstName.Value = user.firstName;
        tbxInitials.Value = user.initials;
        tbxUsrTitle.Value = user.title;
        tbxEmail.Value = user.primaryEmail;
        tbxPassword.Value = "";
        tbxPassword2.Value = "";
        tbxPhone1.Value = user.primaryPhone;
        tbxPhone2.Value = user.secondaryPhone;
        tbxEmail2.Value = user.secondaryEmail;
        ddlAppRole.SelectedValue = user.type.ToString();
        ddlTitle.SelectedValue = user.civtitle;
        cbConsiderAsOvertime.Checked = user.overtime_allowed;
        ddlDepartment.SelectedValue = Convert2.ToString(user.departmentId);
        tbxMilitaryServicesDuration.Value = user.militaryDays.ToString("0.00", CultureInfo.InvariantCulture);

        organizationManager.Load();

        if(organizationManager.OwnerUserId != null && ph.CurrentId != null && organizationManager.OwnerUserId.Value == ph.CurrentId.Value)
        {
            ddlAppRole.Enabled = false;
        }
        else
        {
            ddlAppRole.Enabled = true;
        }

        /* if (item.work_schedule_profile_id.HasValue)
         {
             weekHours = "1";
         }
         else
         {
             weekHours = item.weeklyHours != null ? Convert2.ToString(item.weeklyHours) : organizationManager.WeekWorkHours.ToString();
             tbxWeeklyHours.Value = weekHours;
         }*/

        TimesheetsReport = timesheetReportService.CalculateTimesheetReportForPeriod(ph.CurrentOrganizationId, ph.CurrentId.Value, new DateRange(DatesRangeEnum.Since1990));
        TimesheetsReport.Summary = TimesheetsReport.Summary.Replace(Environment.NewLine, "<br />");

        var contracts = contractsManager.GetContractsDto(ph.CurrentId);

        listContracts.DataSource = contracts;
        listContracts.DataBind();

        var contract = contracts.FirstOrDefault(c => c.RecordStatus == 0) ?? new ContractDto();
        var absenceReport = GetAbsenceReportForCurrentYear();

        weekHours = user.weeklyHours.HasValue ? user.weeklyHours.Value : weekHours;
        tbxWeeklyHours.Value = weekHours.ToString("0.00", CultureInfo.InvariantCulture);

        var vacationsPerYear = contract.VacationsPerYear > 0 ? contract.VacationsPerYear : organizationManager.Vacation > 0? organizationManager.Vacation: defaultVacationsPerYear;
        tbxVacationsPerYear.Value = vacationsPerYear.ToString(CultureInfo.InvariantCulture);

        ContractVacationsPerYear.Value = vacationsPerYear.ToString(CultureInfo.InvariantCulture);
        VacationsPerYear = vacationsPerYear;

        tbxRate.Value = contract.HoursRate.ToString("0.00", CultureInfo.InvariantCulture);
        tbxVacationsRemaining.Value = absenceReport.VacationReport.VacationDaysRemaining.ToString("0.00", CultureInfo.InvariantCulture);

        LoadAttachments(ph.CurrentOrganizationId, ph.CurrentId.Value);

        CarriedOverDaysPerYears = LoadVacationCarriedOverDays(ph.CurrentId);
        var format = "f2";
        YearLabel1.Text = CarriedOverDaysPerYears[0].Year.ToString();
        YearLabel2.Text = CarriedOverDaysPerYears[1].Year.ToString();
        YearLabel3.Text = CarriedOverDaysPerYears[2].Year.ToString();
        YearLabel4.Text = CarriedOverDaysPerYears[3].Year.ToString();
        YearLabel5.Text = CarriedOverDaysPerYears[4].Year.ToString();
        DaysPerYear1.Text = CarriedOverDaysPerYears[0].Days.ToString(format, CultureInfo.InvariantCulture);
        DaysPerYear2.Text = CarriedOverDaysPerYears[1].Days.ToString(format, CultureInfo.InvariantCulture);
        DaysPerYear3.Text = CarriedOverDaysPerYears[2].Days.ToString(format, CultureInfo.InvariantCulture);
        DaysPerYear4.Text = CarriedOverDaysPerYears[3].Days.ToString(format, CultureInfo.InvariantCulture);
        DaysPerYear5.Text = CarriedOverDaysPerYears[4].Days.ToString(format, CultureInfo.InvariantCulture);

        // var overtime = employeesOvertimeManager.GetOvertimeForEmployee(user.id);

        if (TimesheetsReport != null)
        {
            tbxTotalHoursEffectivelyCompensated.Value = TimesheetsReport.TotalHoursEffectivelyCompensated.ToString("0.00", CultureInfo.InvariantCulture);
            tbxTotalHoursActuallyPaid.Value = TimesheetsReport.TotalHoursActuallyPaid.ToString("0.00", CultureInfo.InvariantCulture);
        }

        var activeContract = contractsManager.GetActiveContract(user.id);
        if(activeContract != null)
        {
            activeContractRate1 = activeContract.overtime_rate.ToString("0.00", CultureInfo.InvariantCulture);
            activeContractRate2 = activeContract.overtime_rate2.ToString("0.00", CultureInfo.InvariantCulture);
            isOvertimeEnabled = activeContract.overtime_allowed;
        }
    }

    // Create a new db record from controls values
    private void Update()
    {
        if (String.IsNullOrWhiteSpace(tbxInitials.Value))
        {
            ph.DisplayError(Resources.Resource.Error_Initials_Mandatory);
            return;
        }

        if (!String.IsNullOrWhiteSpace(tbxPassword.Value)
            && !String.IsNullOrWhiteSpace(tbxPassword2.Value)
            && tbxPassword.Value != tbxPassword2.Value)
        {
            ph.DisplayError(Resources.Resource.Error_Password_Mismatch);
            tbxPassword.Value = "";
            tbxPassword2.Value = "";
            return;
        }

        if (!String.IsNullOrWhiteSpace(tbxPassword.Value)
            && !String.IsNullOrWhiteSpace(tbxPassword2.Value)
            && !Calani.BusinessObjects.Generic.PasswordAdvisor.IsEnoughtStrength(tbxPassword.Value))
        {
            ph.DisplayError(Resources.Resource.Error_NotSecuredEnought);
            tbxPassword.Value = "";
            tbxPassword2.Value = "";
            return;
        }

        long selSchedlureId = 0;
        selSchedlureId = Convert2.StrToLong(ddlWorkSchedule.SelectedValue, selSchedlureId);
        
        string pass = employeesManager.Get(ph.CurrentId.Value).password;
        if (!String.IsNullOrWhiteSpace(tbxPassword.Value))
        {
            pass = Calani.BusinessObjects.Generic.PasswordTools.Hash(tbxPassword.Value);
        }


        if(String.IsNullOrEmpty(tbxEmail.Value))
        {
            var mgr2 = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(ph.CurrentOrganizationId);
            mgr2.Load();

            if (mgr2.OwnerUserId != null && ph.CurrentId != null && mgr2.OwnerUserId.Value == ph.CurrentId.Value)
            {
                var existing = employeesManager.Get(ph.CurrentId.Value);
                tbxEmail.Value = existing.primaryEmail;
            }
        }

        double militaryDays = 0;
        if (!string.IsNullOrEmpty(tbxMilitaryServicesDuration.Value) &&
            double.TryParse(tbxMilitaryServicesDuration.Value, out militaryDays))
        {
            if(militaryDays < 0)
            {
                militaryDays = 0;
            }
        }

        if (!String.IsNullOrWhiteSpace(hfFile.Value))
        {
            var mgr = new AttachmentsManager();
            mgr.OrganizationId = ph.CurrentOrganizationId;

            var files = JsonConvert.DeserializeObject<List<String>>(hfFile.Value);

            foreach (var file in files)
            {
                mgr.UploadFileAttachmentToDb(file, jobId: null, userId: ph.CurrentId.Value, ftype: null);
            }

            var freshAttachments = mgr.ListUserAttachments(ph.CurrentId.Value).Select(a => new { id = a.id, title = a.NameWithExtension });
            if (freshAttachments.Any())
            {
                JsonAttachments = JsonConvert.SerializeObject(new { attachments = freshAttachments.ToArray() });
            }
        }

        var contactEntity = new Calani.BusinessObjects.Model.contacts
        {
            initials = tbxInitials.Value,
            firstName = tbxFirstName.Value,
            lastName = tbxLastName.Value,
            title = tbxUsrTitle.Value,
            primaryEmail = tbxEmail.Value,
            password = pass,
            primaryPhone = tbxPhone1.Value,
            secondaryPhone = tbxPhone2.Value,
            secondaryEmail = tbxEmail2.Value,
            type = ConvertX2(ddlAppRole.SelectedValue),
            civtitle = ddlTitle.SelectedValue,
            overtime_allowed = cbConsiderAsOvertime.Checked,
            organizationId = ph.CurrentOrganizationId,
            work_schedule_profile_id = selSchedlureId > 0 ? selSchedlureId : (long?)null,
            departmentId = Convert.ToInt16(ddlDepartment.SelectedValue),
            militaryDays = militaryDays
        };

        SetWeeklyHours(contactEntity);

        UpdateVacationsCarriedOver(ph.CurrentId.Value);

        var ret = employeesManager.SaveOrUpdate(ph.CurrentId.Value, contactEntity);
        
        if (ret.Success)
        {
            var ret2 = SaveOrUpdateOvertime(ph.CurrentId.Value);

            if (ret2.Success)
            {
                ph.DisplaySuccess(Resources.Resource.Updated_var);
                Load();
            }
            else
            {
                ph.DisplayError(ret.ErrorMessage);
            }
        }
        else ph.DisplayError(ret.ErrorMessage);

        ph.UpdateSubscription();
    }

    private SmartAction<contacts_overtime> SaveOrUpdateOvertime(long userId)
    {
        /*
         * Total Hours Effectively Compensated that are displayed on UI are not always the same value that should be saved to database.
         * We want to save only the amount entyered by admin manually. Other adjustments are calculated dynamically by timesheets report.
         * Here we calculate the values entered by admin and save / update them to DB.
         * Apply same approach to Total Hours Actually Paid.
         */

        TimesheetsReport = timesheetReportService.CalculateTimesheetReportForPeriod(ph.CurrentOrganizationId, ph.CurrentId.Value, new DateRange(DatesRangeEnum.Since1990));

        var totalHoursEffCompFromUi = !string.IsNullOrWhiteSpace(tbxTotalHoursEffectivelyCompensated.Value) ? decimal.Parse(tbxTotalHoursEffectivelyCompensated.Value, CultureInfo.InvariantCulture) : 0;
        var totalHoursActPaidFromUi = !string.IsNullOrWhiteSpace(tbxTotalHoursActuallyPaid.Value) ? decimal.Parse(tbxTotalHoursActuallyPaid.Value, CultureInfo.InvariantCulture) : 0;

        var diffEffCompensated = TimesheetsReport.TotalHoursEffectivelyCompensated - totalHoursEffCompFromUi;
        var diffActuallyPaid = TimesheetsReport.TotalHoursActuallyPaid - totalHoursActPaidFromUi;

        var totalHoursEffCompByAdmin = TimesheetsReport.TotalHoursEffectivelyCompensatedEnteredByAdmin - diffEffCompensated;
        var totalHoursActPaidByAdmin = TimesheetsReport.TotalHoursActuallyPaidEnteredByAdmin - diffActuallyPaid;

        var overtime = employeesOvertimeManager.GetOvertimeForEmployee(userId);
        
        if (overtime != null && overtime.contact_id == userId)
        {
            overtime.total_hours_effectively_compensated = totalHoursEffCompByAdmin;
            overtime.total_hours_actually_paid = totalHoursActPaidByAdmin;
        }
        else
        {
            overtime = new contacts_overtime()
            {
                total_hours_effectively_compensated = totalHoursEffCompByAdmin,
                total_hours_actually_paid = totalHoursActPaidByAdmin,
                contact_id = userId                
            };
        }

        var ret = employeesOvertimeManager.SaveOrUpdate(overtime.id, overtime);

        return ret;
    }
    
    private long Create()
    {
        if (String.IsNullOrWhiteSpace(tbxInitials.Value))
        {
            ph.DisplayError(Resources.Resource.Error_Initials_Mandatory);
            return (long) 0;
        }
        

        if (!String.IsNullOrWhiteSpace(tbxPassword.Value)
            && !String.IsNullOrWhiteSpace(tbxPassword2.Value)
            && tbxPassword.Value != tbxPassword2.Value)
        {
            ph.DisplayError(Resources.Resource.Error_Password_Mismatch);
            tbxPassword.Value = "";
            tbxPassword2.Value = "";
            return (long)0;
        }

        if (!String.IsNullOrWhiteSpace(tbxPassword.Value)
            && !String.IsNullOrWhiteSpace(tbxPassword2.Value)
            && !Calani.BusinessObjects.Generic.PasswordAdvisor.IsEnoughtStrength(tbxPassword.Value))
        {
            ph.DisplayError(Resources.Resource.Error_NotSecuredEnought);
            tbxPassword.Value = "";
            tbxPassword2.Value = "";
            return (long)0;
        }

        long selSchedlureId = 0;
        selSchedlureId = Convert2.StrToLong(ddlWorkSchedule.SelectedValue, selSchedlureId);

        var mgr2 = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(ph.CurrentOrganizationId);
        mgr2.Load();

        string pass = tbxPassword.Value;

        if (String.IsNullOrWhiteSpace(tbxPassword.Value)) 
            pass = null;
        else 
            pass = Calani.BusinessObjects.Generic.PasswordTools.Hash(pass);

        var model = new contacts();

        model.initials = tbxInitials.Value;
        model.firstName = tbxFirstName.Value;
        model.lastName = tbxLastName.Value;
        model.title = tbxUsrTitle.Value;
        model.primaryEmail = tbxEmail.Value;
        model.password = pass;
        model.primaryPhone = tbxPhone1.Value;
        model.secondaryPhone = tbxPhone2.Value;
        model.secondaryEmail = tbxEmail2.Value;
        model.type = ConvertX2(ddlAppRole.SelectedValue);
        model.weeklyHours = mgr2.WeekWorkHours;
        model.civtitle = ddlTitle.SelectedValue;
        model.overtime_allowed = cbConsiderAsOvertime.Checked;
        model.departmentId = null;
        model.passwordStr = tbxPassword.Value;
        Int16 dpi;
        if (Int16.TryParse(ddlDepartment.SelectedValue, out dpi))
            model.departmentId = dpi;

        model.organizationId = ph.CurrentOrganizationId;

        if(model.weeklyHours == null)
        {
            SetWeeklyHours(model);
        }

        var ret = employeesManager.SaveOrUpdate(0,model);

        long id = 0;
        if (ret.Success)
        {
            ph.DisplaySuccess(Resources.Resource.Created_var);
            btnSave.Enabled = false;
            id = ret.Record.id;
        }
        else 
            ph.DisplayError(ret.ErrorMessage);

        ph.UpdateSubscription();

        return id;
    }

    private int? ConvertX2(string s)
    {
        int i = 0;
        if (Int32.TryParse(s, out i)) 
            return i;

        return null;
    }

    protected void Delete()
    {
        
        var ret = employeesManager.Delete(ph.CurrentId.Value);
        if (ret.Success)
        {
            panelEdit.Visible = false;
            ph.DisplaySuccess(Resources.Resource.Deleted_var);
        }
        else ph.DisplayError(ret.ErrorMessage);
    }

#endregion

    #region Buttons (triggers)

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) 
            Update();
        else Create();
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        if (ph.CurrentId != null) Delete();
    }
    
    protected void bSaveContract_OnClick(object sender, EventArgs e)
    {
        long contractId = Convert2.StrToLong(ContractId.Value, 0);
        var userId = ph.CurrentId;
        bool hasPreviousContracts = false;

        if (!ph.CurrentId.HasValue)
        {
            var nId = Create();
            if (nId > 0)
                userId = nId;
            else
                return;
        }
        else
        {
            var user = employeesManager.Get(ph.CurrentId.Value);
            hasPreviousContracts = user.contacts_contracts.Any();
        }
             

        var start = ContractStartDate.Value;
        var rate = ContractRate.Value;
        var hours = ContractHours.Value;
        var hourRateStr = ContractHourRate.Value;
        var vacPerYearStr = ContractVacationsPerYear.Value;

        if (contractId <= 0 && String.IsNullOrWhiteSpace(start) || String.IsNullOrWhiteSpace(rate) || String.IsNullOrWhiteSpace(hours))
            return;

        //var st = String.IsNullOrWhiteSpace(ContractStartTime.Value) ? "00:00:00" : ContractStartTime.Value.Trim() + ":00";

        DateTime? startDate = null;
        if (!string.IsNullOrWhiteSpace(start))
        {
            start = String.Format("{0} {1}", start, "00:00:00");
            startDate = Convert2.ToDateWithTime(start, "picker");
        }
        
        if(hasPreviousContracts && startDate.HasValue && startDate.Value.Date < DateTime.Now.Date)
        {
            return;
        }

        var drate = Convert2.ToNullableDouble(rate, null, 1, null, null);
        var dhours = Convert2.ToNullableDouble(hours, null, 1, null, null);

        var irate = 0;
        if(drate.HasValue)
            irate =(int)(drate * 100);

        var duration = 0 ;
        if(dhours.HasValue)
            duration = (int)(dhours * 60);

        var hourrate= Convert2.StrToLDecimal(hourRateStr, 0);
        var vacPerYear = Convert2.StrToInt(vacPerYearStr, 0);

        bool overtimeEnabled = cbConsiderAsOvertime.Checked;
        bool overtime2Enabled = cbxOvertime2Enabled.Checked;
        var overtimeRate = overtimeEnabled ? Convert2.StrToLDecimal(tbxOvertimeRate.Value, 0) : 0;
        var overtimeRate2 = overtime2Enabled ? Convert2.StrToLDecimal(tbxOvertimeRate2.Value, 0) : 0;
        var overtime2ThresholdHours = overtime2Enabled ? Convert2.StrToLDecimal(tbxOvertime2ThresholdHours.Text, 0) : 0;

        var contract = new ContractDto()
        {
            Id = contractId,
            UserId = userId.Value,
            Rate = irate,
            DurationMin = duration,
            StartDate = startDate,
            HoursRate = hourrate,
            VacationsPerYear = vacPerYear,
            OvertimeEnabled = overtimeEnabled,
            OvertimeRate = overtimeRate,
            Overtime2Enabled = overtime2Enabled,
            OvertimeRate2 = overtimeRate2,
            Overtime2ThresholdHours = overtime2ThresholdHours,
        };

        contractsManager.CreateContract(contract);

        if (ph.CurrentId.HasValue)
            Response.Redirect(Request.Url.PathAndQuery);
    }

    #endregion

    private void SetWeeklyHours(contacts item)
    {
        if (item == null) return;

        var contractEntity = contractsManager.GetContractsDto(ph.CurrentId).FirstOrDefault(c => c.RecordStatus == 0);

        var contract = contractEntity != null ? contractEntity : new ContractDto();

        item.weeklyHours = contract.Hours > 0 ? contract.Hours : organizationManager.WeekWorkHours > 0 ? organizationManager.WeekWorkHours : DefaultWeeklyHoursValue;
    }

    private void LoadAttachments(long organizationId, long userId)
    {
        panelAttachments.Visible = true;

        var attmgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
        attmgr.OrganizationId = organizationId;
        var ds = new List<attachmentsExtended>();

        if (userId > 0)
        {
            ds.AddRange(attmgr.ListUserAttachments(userId));
        }

        List<int> filter = Calani.BusinessObjects.Attachments.AttachmentsManager.GetNonGeneratedTypes();
        var dsAttachments = (from r in ds where r.type != null && filter.Contains(r.type.Value) select r).ToList();
        gridFilesAttachments.DataSource = dsAttachments;
        gridFilesAttachments.DataBind();
        new Html5GridView(gridFilesAttachments, false, null);
        lblAttachmentsCount.InnerHtml = dsAttachments.Count.ToString();
        lblAttachmentsCount.Visible = dsAttachments.Any();

        Attachments = dsAttachments.ToDictionary(a => a.id, a => a.NameWithExtension);
        JsonAttachments = new JavaScriptSerializer().Serialize(dsAttachments.Select(a => new { id = a.id, title = a.NameWithExtension }));

        //filter = Calani.BusinessObjects.Attachments.AttachmentsManager.GetGeneratedTypes();
        //var dsHistory = (from r in ds where r.type != null && filter.Contains(r.type.Value) select r).ToList();
        //gridFilesHistory.DataSource = dsHistory;
        //gridFilesHistory.DataBind();
        //new Html5GridView(gridFilesHistory, false, null);

        if (dsAttachments.Count == 0)
        {
            liAttachments.Visible = false;
            docstabAttachments.Visible = false;

            if (IsReadOnly == "false")
            {
                liNewAttachment.Attributes["class"] = "active";
                docstabNew.Attributes["class"] = "tab-pane active";
            }
            //else
            //{
            //    liHistory.Attributes["class"] = "active";
            //    docstabHistory.Attributes["class"] = "tab-pane active";
            //}

        }
        else
        {
            liAttachments.Visible = true;
            docstabAttachments.Visible = true;
            liAttachments.Attributes["class"] = "active";
            docstabAttachments.Attributes["class"] = "tab-pane active";

            liNewAttachment.Attributes["class"] = "";
            docstabNew.Attributes["class"] = "tab-pane";
        }

    }

    private List<CarriedOverDaysPerYear> LoadVacationCarriedOverDays(long? userId)
    {
        var last5Years = Enumerable.Range(DateTime.Now.Year - 4, 5).OrderByDescending(m => m);

        if(userId == null)
        {
            return last5Years.Select(year => new CarriedOverDaysPerYear() { Year = year }).ToList();
        }

        var entities = vacationCarriedOverDaysManager.GetVacationCarriedOverDays(userId.Value).ToDictionary(m => m.year);
        return last5Years.Select(year => 
            new CarriedOverDaysPerYear() { 
                Year = year, 
                Days = entities.ContainsKey(year) ? entities[year].days : 0 
            }).ToList();
    }

    private void UpdateVacationsCarriedOver(long employeeId)
    {
        var controls = new List<(Label, TextBox)>
        {
            (YearLabel1, DaysPerYear1),
            (YearLabel2, DaysPerYear2),
            (YearLabel3, DaysPerYear3),
            (YearLabel4, DaysPerYear4),
            (YearLabel5, DaysPerYear5),
        };

        foreach(var pair in controls)
        {
            var daysPerYear = GetCarriedOverDaysFromUI(pair.Item1, pair.Item2);
            if (daysPerYear != null)
            {
                vacationCarriedOverDaysManager.SaveOrUpdate(employeeId, daysPerYear.Year, daysPerYear.Days);
            }
        }
    }

    private CarriedOverDaysPerYear GetCarriedOverDaysFromUI(Label yearLabel, TextBox daysTextBox)
    {
        int year = 0;
        decimal days = 0;
        if (int.TryParse(yearLabel.Text.Trim(), out year) && decimal.TryParse(daysTextBox.Text.Replace(',', '.').Trim(), NumberStyles.Float, CultureInfo.InvariantCulture, out days))
        {
            return new CarriedOverDaysPerYear
            {
                Year = year,
                Days = days
            };
        }

        return null;
    }

    private AbsenceReportModel GetAbsenceReportForCurrentYear()
    {
        long organizationId = ph.CurrentOrganizationId;
        var workScheduleRecManager = new WorkScheduleRecManager(organizationId);
        var workScheduleService = new WorkScheduleService(organizationManager, workScheduleRecManager);
        var workDay = workScheduleService.GetWorkDaySettings();
        var workingTimeCalculator = new WorkingTimeCalculator(workDay);
        var absenceRequestsManager = new AbsenceRequestsManager(organizationId, contractsManager, employeesManager);
        var vacationCarriedOverDaysManager = new VacationCarriedOverDaysManager(organizationId);
        var vacationReportService = new VacationReportService(employeesManager, contractsManager, organizationManager, absenceRequestsManager,
            vacationCarriedOverDaysManager, workingTimeCalculator);
        var absenceRequestsService = new AbsenceRequestsService(absenceRequestsManager, employeesManager, contractsManager,
            vacationReportService, workingTimeCalculator);

        var currentYearRange = new DateRange(DatesRangeEnum.CurrentYear);
        var report = absenceRequestsService.GetAbsenceReports(currentYearRange.Start, currentYearRange.End, new List<long> { ph.CurrentId.Value }).Single();

        vacationReportHidden.InnerText = report.VacationReport.TextReport;

        return report;
    }
}
