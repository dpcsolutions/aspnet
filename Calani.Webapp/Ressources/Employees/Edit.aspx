﻿<%@ Page Title="Edit employee" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Ressources_Employees_Edit" Codebehind="Edit.aspx.cs" %>
<%@ Import Namespace="Calani.BusinessObjects.Enums" %>
<%@ Register Src="~/assets/Controls/PasswordAdvisor.ascx" TagPrefix="uc1" TagName="PasswordAdvisor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    <%--Canceled Seubi css for page to apply theme style--%>
    <style type="text/css">
        .table > thead > tr > th {
            border-bottom: none;  
            background: none; 
            color: black;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <form runat="server">

        <div class="content">

		    <div class="row">

                <!-- left column -->
			    <div class="col-sm-12 col-md-6">


                        <asp:Panel runat="server" ID="panelError" Visible="false" class="panel panel-danger panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Error %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblErrorMessage" />
						    </div>
					    </asp:Panel><!-- panelError -->

                        <asp:Panel runat="server" ID="panelSuccess" Visible="false" class="panel panel-success panel-bordered">
						    <div class="panel-heading">
							    <h6 class="panel-title"><%= Resources.Resource.Validation_Done %></h6>
						    </div>

						    <div class="panel-body">
							    <asp:Label runat="server" ID="lblSuccess" />
                                <br />
                                <strong><a href="." id="linkBackList" runat="server"></a></strong>
                                    <%= Resources.Resource.or %>
                                <a href="Edit.aspx" id="linkNew" runat="server"></a>.
						    </div>
					    </asp:Panel><!-- panelSuccess -->
					    
					    <asp:Panel runat="server" ID="panelEdit" class="panel panel-white">
						    <div class="panel-heading">
							    <h6 class="panel-title"><asp:Label runat="server" ID="lblTitle" /></h6>
						    </div>

						    <div class="panel-body">

							    <div class="form-horizontal">

								    <fieldset class="content-group">

                                        <!-- title -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Title %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-font"></i></span>
                                                    <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlTitle">
                                                        <asp:ListItem Value="Mr" Text='<%$ Resources:Resource, Title_Mr %>' />
                                                        <asp:ListItem Value="Mrs" Text='<%$ Resources:Resource, Title_Mrs %>' />
                                                        <asp:ListItem Value="Miss" Text='<%$ Resources:Resource, Title_Miss %>' />
                                                        <asp:ListItem Value="Professor" Text='<%$ Resources:Resource, Title_Professor %>' />
                                                        <asp:ListItem Value="Dr" Text='<%$ Resources:Resource, Title_Dr %>' />
                                                    </asp:DropDownList>
											    </div>
										    </div>
									    </div>
                                        <!-- /title -->

                                        <!-- name -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Lastname %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-font"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Lastname %>' runat="server" id="tbxLastName" />
											    </div>
										    </div>
									    </div>
                                        <!-- /name -->

                                        <!-- name -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Firstname %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="fa fa-font"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Firstname %>' runat="server" id="tbxFirstName" />
											    </div>
										    </div>
									    </div>
                                        <!-- /name -->

                                        <!-- name -->
									    <div class="form-group">
										    <label class="control-label col-lg-2"><%= Resources.Resource.Initials %></label>
										    <div class="col-lg-10">
											    <div class="input-group">
												    <span class="input-group-addon"><i class="icon-vcard"></i></span>
												    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Initials_3_letters %>' runat="server" id="tbxInitials" maxlength="5" />
											    </div>
										    </div>
									    </div>
                                        <!-- /name -->

										
                                        <!-- title -->
									    <div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.UserTitle %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-collaboration"></i></span>
                                                    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, UserTitle_Tooltip %>' runat="server" id="tbxUsrTitle"/>
												</div>
											</div>
										</div>
                                        <!-- /internalId -->
												


								    </fieldset>

									 <fieldset class="content-group">
										<legend class="text-bold"><%= Resources.Resource.Department %></legend>

										 <div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.Department %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="fa fa-balance-scale"></i></span>
                                                    <asp:DropDownList runat="server" ID="ddlDepartment" CssClass="select form-control"
                                                         DataValueField="id" DataTextField="name" />						
												</div>
											</div>
										</div>
									 </fieldset>

                                    <fieldset class="content-group">
										<legend class="text-bold"><%= Resources.Resource.Contact %></legend>

                                        <!-- email -->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.Email %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-envelope"></i></span>
                                                    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, PrimaryEmail_Tooltip %>' runat="server" id="tbxEmail"/>
												</div>
											</div>
										</div>
                                        <!-- /email -->

                                        <!-- phone1 -->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.Phone %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-phone2"></i></span>
                                                    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, Phone %>' runat="server" id="tbxPhone1"/>
												</div>
											</div>
										</div>
                                        <!-- /phone1 -->

                                        <!-- phone2 -->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.MobilePhone %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-iphone"></i></span>
                                                    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, MobilePhone %>' runat="server" id="tbxPhone2"/>
												</div>
											</div>
										</div>
                                        <!-- /phone2 -->

                                        <!-- email2 -->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.AltEmail %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-envelope"></i></span>
                                                    <input type="text" class="form-control" placeholder='<%$ Resources:Resource, AltEmail_Tooltip %>'  runat="server" id="tbxEmail2"/>
												</div>
											</div>
										</div>
                                        <!-- /email2 -->
                                    </fieldset>


                                    <fieldset class="content-group">
										<legend class="text-bold"><%= Resources.Resource.CalaniUserAndrole %></legend>

                                        

                                        <!-- password -->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.Password %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-lock5"></i></span>
                                                    <input type="password" class="form-control" placeholder='<%$ Resources:Resource, Password %>' runat="server" id="tbxPassword"/>
												</div>
											</div>
										</div>
                                        <!-- /password -->

                                         <div class="form-group">
                                            <label class="control-label col-lg-2"></label>
                                            <div class="col-lg-10">
                                                <uc1:PasswordAdvisor runat="server" ID="PasswordAdvisor" PasswordInput="ContentPlaceHolder1_tbxPassword" />
                                            </div>      
                                        </div>

                                        <!-- password conf-->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.PasswordConfirmation %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-lock5"></i></span>
                                                    <input type="password" class="form-control" placeholder='<%$ Resources:Resource, PasswordConfirmation_Tooltip %>' runat="server" id="tbxPassword2"/>
												</div>
											</div>
										</div>
                                        <!-- /password conf -->

                                        <!-- password conf-->
										<div class="form-group">
											<label class="control-label col-lg-2"><%= Resources.Resource.ApplicationRole %></label>
											<div class="col-lg-10">
												<div class="input-group">
													<span class="input-group-addon"><i class="icon-hat"></i></span>
                                                    <asp:DropDownList runat="server" ID="ddlAppRole" CssClass="form-control select" onchange="onRoleChanged(event)">
                                                        <asp:ListItem Value="2" Text='<%$ Resources:Resource, Employee %>' />
                                                        <asp:ListItem Value="1" Text='<%$ Resources:Resource, CompanyAdministrator %>' />
                                                        <asp:ListItem Value="0" Text='<%$ Resources:Resource, DisabledAccount %>' />

                                                    </asp:DropDownList>
												</div>
                                                <span id="disabled-account" style="display: none" class="form-text text-danger"><%= Resources.Resource.DisabledAccount %></span>
											</div>
										</div>
                                        <!-- /password conf -->

									</fieldset>
                                
                                <fieldset class="content-group">
                                    <legend class="text-bold"><%= Resources.Resource.Hours %></legend>

                                    <!-- WeeklyHours -->
                                    <div class="form-group">
                                        <label class="control-label col-lg-4"><%= Resources.Resource.WeeklyHours %></label>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                                                <input type="number" class="form-control" step="any" runat="server" id="tbxWeeklyHours" readonly="readonly" />
                                                <span class="input-group-addon"><%= Resources.Resource.WeeklyHours_Tooltip %></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /WeeklyHours -->

                                     <!-- Vacation -->
                                    <div class="form-group">
                                        <label class="control-label col-lg-4"><%= Resources.Resource.VacationsPerYear %></label>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-umbrella"></i></span>
                                                <input type="number" class="form-control" step="any" runat="server" id="tbxVacationsPerYear" readonly="readonly"/>
                                                <span class="input-group-addon"><%= Resources.Resource.Days %></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Vacation -->

                                     <!-- VacationsRemaining -->
                                    <div class="form-group">
                                        <label class="control-label col-lg-4"><%= Resources.Resource.VacationsRemaining %></label>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-umbrella"></i></span>
                                                <input type="number" class="form-control" step="any" runat="server" id="tbxVacationsRemaining" readonly="readonly" />
                                                <span class="input-group-addon"><%= Resources.Resource.Days %></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="display:none" runat="server" id="vacationReportHidden"></div>
                                    <!-- /VacationsRemaining -->

                                     <!-- Rate -->
                                    <div class="form-group">
                                        <label class="control-label col-lg-4"><%= Resources.Resource.HourRate %></label>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                                                <input type="number" class="form-control" step="any" runat="server" id="tbxRate" readonly="readonly" />
                                                <span class="input-group-addon"><%= Resources.Resource.PerHour %></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Rate -->

                                    <!-- Overtime fields: hide if overtimes are disabled in active contract -->
                                    <div <%= isOvertimeEnabled ? "" : "style='display:none;'" %>>

                                        <!-- Total hours worked -->
                                        <div class="form-group">
                                            <label class="control-label col-lg-4"><%= Resources.Resource.TotalHoursWorked %></label>
                                            <div class="col-lg-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-clock"></i></span>
                                                    <input type="text" class="form-control" readonly="readonly" value="<%= TimesheetsReport.TotalHoursWorked.ToString("0.00") %>" />
                                                    <span class="input-group-addon"><%= Resources.Resource.Hours %></span>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Total accumulated overtime -->
                                        <div class="form-group">
                                            <label class="control-label col-lg-4"><%= Resources.Resource.TotalAccumulatedOvertime %></label>
                                            <div class="col-lg-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-clock"></i></span>
                                                    <input type="number" class="form-control" readonly="readonly" value="<%= TimesheetsReport.TotalOvertimeHours.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture) %>" />
                                                    <span class="input-group-addon"><%= Resources.Resource.Hours %></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-lg-4"><%= string.Format(Resources.Resource.PercentHours, activeContractRate1) %></label>
                                            <div class="col-lg-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-clock"></i></span>
                                                    <input type="number" class="form-control"  readonly="readonly" value="<%= TimesheetsReport.OvertimeRate1Hours.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture) %>" />
                                                    <span class="input-group-addon"><%= Resources.Resource.Hours %></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-lg-4"><%= string.Format(Resources.Resource.PercentHours, activeContractRate2) %></label>
                                            <div class="col-lg-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-clock"></i></span>
                                                    <input type="number" class="form-control"  readonly="readonly" value="<%= TimesheetsReport.OvertimeRate2Hours.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture) %>" />
                                                    <span class="input-group-addon"><%= Resources.Resource.Hours %></span>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Total hours to be compensated -->
                                        <div class="form-group">
                                            <label class="control-label col-lg-4"><%= Resources.Resource.TotalHoursToBeCompensated %></label>
                                            <div class="col-lg-8">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-clock"></i></span>
                                                    <input type="number" id="total-hours-to-be-compensated" class="form-control"  readonly="readonly" value="<%= TimesheetsReport.TotalHoursToBeCompensated.ToString("0.00", System.Globalization.CultureInfo.InvariantCulture) %>" />
                                                    <span class="input-group-addon"><%= Resources.Resource.Hours %></span>
                                                </div>
                                            </div>
                                        </div>

                                            <!-- Total hours effectively compensated -->
                                        <div class="form-group">
                                            <label class="control-label col-lg-4"><%= Resources.Resource.TotalHoursEffectivelyCompensated %></label>
                                            <div class="col-lg-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-clock"></i></span>
                                                    <input type="number" class="form-control" step="any" runat="server" id="tbxTotalHoursEffectivelyCompensated" min="0" readonly="true" />
                                                    <span class="input-group-addon"><%= Resources.Resource.Hours %></span>
                                                </div>
                                            </div>
                                            <label class="control-label col-lg-1" style="text-align: right"><a href="javascript:void(0)" id="btnAddEffectivelyCompensatedHours"><%= Resources.Resource.Add %></a></label>
                                            <div class="col-lg-3">
                                                <div class="input-group">
                                                    <input type="number" class="form-control" step="any" id="tbxAddTotalHoursEffectivelyCompensated" min="0" />
                                                    <span class="input-group-addon"><%= Resources.Resource.Hours %></span>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Total hours actually paid -->
                                        <div class="form-group form-inline">
                                            <label class="control-label col-lg-4"><%= Resources.Resource.TotalHoursActuallyPaid %></label>
                                            <div class="col-lg-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-clock"></i></span>
                                                    <input type="number" class="form-control" step="any" runat="server" id="tbxTotalHoursActuallyPaid" min="0"  readonly="true" />
                                                    <span class="input-group-addon"><%= Resources.Resource.Hours %></span>
                                                </div>
                                            </div>
                                            <label class="control-label col-lg-1" style="text-align: right"><a href="javascript:void(0)" id="btnAddActuallyPaidHours"><%= Resources.Resource.Add %></a></label>
                                            <div class="col-lg-3">
                                                <div class="input-group">
                                                    <input type="number" class="form-control" step="any" id="tbxAddTotalHoursActuallyPaid" min="0" />
                                                    <span class="input-group-addon"><%= Resources.Resource.Hours %></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Military services -->
                                    <div class="form-group">
                                        <label class="control-label col-lg-4"><%= Resources.Resource.MilitaryServicesDuration %></label>
                                        <div class="col-lg-8">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-fighter-jet"></i></span>
                                                <input type="number" class="form-control" step="1" runat="server" id="tbxMilitaryServicesDuration" />
                                                <span class="input-group-addon"><%= Resources.Resource.Days %></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Military services -->

                                    <p id="timesheets-report-summary" style="display:none"><%= TimesheetsReport.Summary %></p>
                                </fieldset>
                                
                                
                                <fieldset class="content-group" id="DefaultWorkSchedule" runat="server">
                                    <legend class="text-bold"><%= Resources.Resource.Work_Schedule %></legend>

                                    
                                    <div class="form-group">
                                        <label class="control-label col-lg-2"><%= Resources.Resource.DefaultWorkSchedule %></label>
                                        <div class="col-lg-10">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                                <asp:DropDownList runat="server" CssClass="form-control"  ID="ddlWorkSchedule"  AppendDataBoundItems="True" 
                                                                  DataTextField="Name" DataValueField="Id" >
                                                    <asp:ListItem Text='<%$ Resources:Resource, Choose %>' Value="0" />
                                                        
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </fieldset>

                                <div class="content-group">
								    <legend class="text-bold"><%= Resources.Resource.CarriedOverDays %></legend>										
                                    <div class="table-responsive panel-body" runat="server" ID="div2">
                                        <table class="table table-hover">
                                            <thead>
                                            <tr>
                                                <th runat="server" class="col-lg-2"><%= Resources.Resource.Year %></th>
                                                <th runat="server" class="col-lg-6"><%= Resources.Resource.CarriedOverDays %></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="YearLabel1" runat="server" CssClass="text-left text-default" />
                                                    </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><%= Resources.Resource.Days.ToLower() %></span>
                                                            <asp:TextBox ID="DaysPerYear1" RUNAT="server" CssClass="form-control" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="YearLabel2" runat="server" CssClass="text-left text-default" />
                                                    </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><%= Resources.Resource.Days.ToLower() %></span>
                                                            <asp:TextBox ID="DaysPerYear2" RUNAT="server" CssClass="form-control" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="YearLabel3" runat="server" CssClass="text-left text-default" />
                                                    </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><%= Resources.Resource.Days.ToLower() %></span>
                                                            <asp:TextBox ID="DaysPerYear3" RUNAT="server" CssClass="form-control" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="YearLabel4" runat="server" CssClass="text-left text-default" />
                                                    </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><%= Resources.Resource.Days.ToLower() %></span>
                                                            <asp:TextBox ID="DaysPerYear4" RUNAT="server" CssClass="form-control" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="YearLabel5" runat="server" CssClass="text-left text-default" />
                                                    </td>
                                                    <td>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><%= Resources.Resource.Days.ToLower() %></span>
                                                            <asp:TextBox ID="DaysPerYear5" RUNAT="server" CssClass="form-control" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                               
                                    <div class="content-group">
										<legend class="text-bold"><%= Resources.Resource.WorkContract %></legend>

                                            <div class="table-responsive panel-body" runat="server" ID="divContracts">
                                                <asp:ListView runat="server" ID="listContracts" >
                                                    <LayoutTemplate>
                                                        <table class="table table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th runat="server" class="col-lg-4"><%= Resources.Resource.Start %></th>
                                                                <th runat="server" class="col-lg-4"><%= Resources.Resource.Rate %></th>
                                                                <th runat="server" class="col-lg-2"><%= Resources.Resource.Hours %></th>
                                                                <th runat="server" class="col-lg-2"><%= Resources.Resource.HourRate %></th>
                                                                <th runat="server" class="col-lg-2"><%= Resources.Resource.VacationsPerYear %></th>
                                                                <th runat="server" class="col-lg-2"><%= Resources.Resource.Action %></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr runat="server" id="itemPlaceholder" ></tr>
                                                            </tbody>
                                                        </table>
                                                    </LayoutTemplate>
                                                    <EmptyDataTemplate>
                                                        <table class="table table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th runat="server" class="col-lg-4"><%= Resources.Resource.Start %></th>
                                                                <th runat="server" class="col-lg-4"><%= Resources.Resource.Rate %></th>
                                                                <th runat="server" class="col-lg-2"><%= Resources.Resource.Hours %></th>
                                                                <th runat="server" class="col-lg-2"><%= Resources.Resource.HourRate %></th>
                                                                <th runat="server" class="col-lg-2"><%= Resources.Resource.VacationsPerYear %></th>
                                                                <th runat="server" class="col-lg-2"><%= Resources.Resource.Action %></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr runat="server" id="Tr1" ></tr>
                                                            </tbody>
                                                        </table>
                                                    </EmptyDataTemplate>
                                                   
                                                    <ItemTemplate>
                                                        <tr id='<%# Eval("Id") %>' class='<%#  (RecordStatus)Enum.Parse(typeof(RecordStatus),Eval("RecordStatus").ToString()) == RecordStatus.Active ?  "" : "bg-light-grey" %>'>
                                                            <td>
                                                                <input type="text" id="lstContractId" runat="server" hidden="" value='<%# Bind("Id") %>'/>
                                                                <asp:Label ID="StartLabel" runat="server" Text='<%#Eval("StartStr") %>' CssClass="text-left text-default"/>
                                                            </td>
                                                            <td >
                                                                <asp:Label ID="RateLabel" runat="server"  runat="server" Text='<%#Eval("Rate")+"%" %>' CssClass="text-left text-default"/>
                                                            </td>
                                                            <td >
                                                                <asp:Label ID="HoursLabel" runat="server"  runat="server" Text='<%# ((double)Eval("Hours")).ToString(System.Globalization.CultureInfo.InvariantCulture) %>' CssClass="text-left text-default"/>
                                                            </td>

                                                            <td >
                                                                <asp:Label ID="HoursRateLabel" runat="server"  runat="server" Text='<%# ((decimal)Eval("HoursRate")).ToString(System.Globalization.CultureInfo.InvariantCulture) %>' CssClass="text-left text-default"/>
                                                            </td>
                                                                <td >
                                                                <asp:Label ID="VacationsPerYearLabel" runat="server"  runat="server" Text='<%#Eval("VacationsPerYear") %>' CssClass="text-left text-default"/>
                                                            </td>

                                                            <td runat="server" Visible='<%# (RecordStatus)Enum.Parse(typeof(RecordStatus),Eval("RecordStatus").ToString()) == RecordStatus.Suspended%>'>
                                                                <span class="label label-danger"><%= Resources.Resource.Canceled %></span>
                                                            </td>
                                                            <td runat="server" Visible='<%# (RecordStatus)Enum.Parse(typeof(RecordStatus),Eval("RecordStatus").ToString()) == RecordStatus.Active%>'>
                                                                <ul class="icons-list">
                                                    
                                                                    <%-- Data-bound content. --%>
                                                        
                                                                    <li class="text-primary-600">
                                                                        <a runat="server" href="#" data-target="#modal_Contract" onclick='<%#"editContract("+ 
                                                                                Eval("Id") + ",\"" +
                                                                                Eval("StartStr") +"\"," +
                                                                                ((decimal)Eval("Rate")).ToString(System.Globalization.CultureInfo.InvariantCulture) + "," +
                                                                                ((double)Eval("Hours")).ToString(System.Globalization.CultureInfo.InvariantCulture) + "," +
                                                                                ((decimal)Eval("HoursRate")).ToString(System.Globalization.CultureInfo.InvariantCulture) + "," +
                                                                                Eval("VacationsPerYear") + "," +
                                                                                ((bool)Eval("OvertimeEnabled")).ToString(System.Globalization.CultureInfo.InvariantCulture).ToLowerInvariant() + "," +
                                                                                ((decimal)Eval("OvertimeRate")).ToString(System.Globalization.CultureInfo.InvariantCulture) + "," +
                                                                                ((decimal)Eval("OvertimeRate2")).ToString(System.Globalization.CultureInfo.InvariantCulture) + "," +
                                                                                ((decimal)Eval("Overtime2ThresholdHours")).ToString(System.Globalization.CultureInfo.InvariantCulture) + "," +
                                                                                ((bool)Eval("Overtime2Enabled")).ToString(System.Globalization.CultureInfo.InvariantCulture).ToLowerInvariant() + ");" %>'>
                                                                            <i class="icon-pencil7"></i>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:ListView>
                                                <br/>
                                                <div class="col-lg-2 pull-left">
                                                    <a onclick="editContract(0, '', '', '', 0, <%= VacationsPerYearOrganizationDefault %>, false, 0, false)"  class="btn btn-default pull-left"><i class="icon-plus2 position-left"></i><%= Resources.Resource.Add_line %></a>
                                                </div>
                                            </div>
                                        </div>

                                    <!-- buttonsaction -->
								    <div class="buttonsaction text-right">
										<label class="control-label col-lg-2"></label>
										<div class="col-lg-10">
											<div class="btn-group">
                                                <asp:LinkButton runat="server" ID="btnSave" class="btn btn-primary" OnClick="btnSave_Click">
                                                    <i class="icon-floppy-disk position-left"></i> <%= Resources.Resource.Save %>
                                                </asp:LinkButton>
                                                
									            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" runat="server" id="btnMore"><span class="caret"></span></button>
									            <ul class="dropdown-menu dropdown-menu-right" runat="server" id="btnMoreDdl">
                                                    <li><a href="javascript:void(0)" data-toggle="modal" data-target="#modal_default"><i class="fa fa-trash-o"></i> <%= Resources.Resource.Delete %></a></li>
												</ul>
											</div>
										</div>
									</div>
                                    <!-- /buttonsaction -->


							    </div>
						    </div>
					    </asp:Panel><!-- /panelEdit -->
					    
           


			    </div>
			    <!-- /left column -->
                

                <!-- right column -->
                <div class="col-sm-12 col-md-6">
                    <asp:Panel runat="server" ID="panelLicenses" class="panel panel-white" Visible="false">
                        <div class="panel-body">
                            <div class="form-group mt-5">
							    <label class="text-semibold"><%= Resources.Resource.UsedUserLicenses %></label>
                                <asp:Label CssClass="pull-right-sm" runat="server" id="lblLicencesUsed"/>
                                <div class="progress progress-xxs">
                                    <asp:Label CssClass="progress-bar progress-bar-info" runat="server" id="lblLicencesUsedProgress"/>
							    </div>
						    </div>
                            <div class="form-group mt-5">
                                <asp:HyperLink runat="server" NavigateUrl="~/Account/"  Text='<%$ Resources:Resource, ManageYourSubscription %>' />
                            </div>
                        </div>
                    </asp:Panel>
			    </div>

                <div class="col-sm-12 col-md-6" runat="server" id="panelAttachments">    
                    <div class="panel panel-flat" runat="server" id="panelAttachments_panel">
			            <div class="panel-heading">
				            <h6 class="panel-title"><%= Resources.Resource.Attachments %></h6>
				            <div class="heading-elements">
                                <span class="badge bg-primary heading-text" id="lblAttachmentsCount" runat="server"></span>
					            <ul class="icons-list">
						            <li><a data-action="collapse"></a></li>
					            </ul>
				            </div>
			            </div>

			            <div class="panel-body">

                            <div class="tabbable">
					            <ul class="nav nav-tabs nav-tabs-highlight">
						            <li class="active" runat="server" id="liAttachments"><a href="#<%= docstabAttachments.ClientID %>" data-toggle="tab"><%= Resources.Resource.Attachments %></a></li>
<%--                                    <li runat="server" id="liHistory"><a href="#<%= docstabHistory.ClientID %>" data-toggle="tab"><%= Resources.Resource.History %></a></li>--%>
                                    <li runat="server" id="liNewAttachment"><a href="#<%= docstabNew.ClientID %>" data-toggle="tab"><%= Resources.Resource.Add_new %></a></li>
					            </ul>

					            <div class="tab-content">
						            <div class="tab-pane active" id="docstabAttachments" runat="server">
							
                                        <p class="small text-justify">
                                            <%= Resources.Resource.AttachmentsDocuments_Explain %>
							            </p>

                                        <asp:GridView runat="server" ID="gridFilesAttachments" AutoGenerateColumns="False" ShowHeader="False">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" id="lnk" NavigateUrl='<%# "../../DownloadDocument.ashx?id=" + Eval("id") + "&orgId="+Eval("organizationID") + "&t=" + Eval("FileNameNoExtension") %> ' >
                                                            <asp:Label runat="server" id="ico" CssClass='<%# Bind("Icon") %>'></asp:Label>
                                                            &nbsp;
                                                            <asp:Label ID="lbl" runat="server" Text='<%# Bind("NameWithExtension") %>'></asp:Label>
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="date" HeaderText="Date" />
                                                <asp:TemplateField ItemStyle-Wrap="false" >
                                                    <ItemTemplate>
                                                        <asp:LinkButton Visible='<%# IsReadOnly== "false" %>' runat="server" href="javascript:void(0)" id="lnkd" class="btnDeleteAttachment" data-attachment='<%#  Eval("id") %>' data-name='<%#  Eval("NameWithExtension") %>' data-popup="tooltip" title='<%$ Resources:Resource, Delete %>'>
                                                            <asp:Label runat="server" id="icod" CssClass='icon-trash text-danger'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                    
				                        </asp:GridView>

						            </div>

						          <%--  <div class="tab-pane" id="docstabHistory" runat="server">
							            <p class="small text-justify">
                                            <%= Resources.Resource.HistoryDocuments_Explain %>
							            </p>
                                        <asp:GridView runat="server" ID="gridFilesHistory" AutoGenerateColumns="False" ShowHeader="False">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <asp:HyperLink runat="server" id="lnk" NavigateUrl='<%# "../DownloadDocument.ashx?id=" + Eval("id") + "&orgId="+Eval("organizationID") %>'>
                                                            <asp:Label runat="server" id="ico" CssClass='<%# Bind("Icon") %>'></asp:Label>
                                                            &nbsp;
                                                            <asp:Label ID="lbl" runat="server" Text='<%# Bind("NameWithExtension") %>'></asp:Label>
                                                        </asp:HyperLink>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="date" HeaderText="Date" />
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <asp:LinkButton Visible='<%# IsReadOnly== "false" %>' runat="server" id="lnk" href='<%# SendPage + "?id=" + ProjectId + "&sel=" + Eval("id") %>' data-popup="tooltip" title='<%$ Resources:Resource, SendByEmail %>'>
                                                            <asp:Label runat="server" id="ico" CssClass='icon-envelop'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                    
				                        </asp:GridView>

						            </div>--%>

                                    <div class="tab-pane" id="docstabNew" runat="server">
                                        <asp:HiddenField runat="server" ID="hfFile" />
                                        <div id="dropzone_multiple" class="fallback dropzone dz-clickable">
                                        </div>
                            
						            </div>
					            </div>
				            </div>
			            </div>
		            </div>    
                </div>
                <!-- /right column -->
						

		    </div> <!-- /row -->


					
            <!-- basic modal: confirm delete -->
		    <div id="modal_default" class="modal fade">
			    <div class="modal-dialog">
				    <div class="modal-content">
					    <div class="modal-header">
						    <button type="button" class="close" data-dismiss="modal">&times;</button>
						    <h5 class="modal-title"><%= Resources.Resource.Confirmation %></h5>
					    </div>

					    <div class="modal-body">
						    <h6 class="text-semibold"><%= Resources.Resource.Are_You_Sure_To_Delete %></h6>
						    <p><%= Resources.Resource.Delete_Is_Irreversible %></p>

					    </div>

					    <div class="modal-footer">
						    <button type="button" class="btn btn-link" data-dismiss="modal"><%= Resources.Resource.No_Cancel %></button>
                            <asp:Button runat="server" ID="btnDelete" OnClick="btnDelete_Click" CssClass="btn btn-primary" Text="<%$ Resources:Resource, Yes_Delete %>" />

					    </div>
				    </div>
			    </div>
		    </div>
		    <!-- /basic modal -->

             <div id="modal_contract" class="modal fade">
            <div class="modal-dialog modal-md" <%= IsEnglish ? "style='width: 600px'" : "style='width:750px'" %>>
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title text-center" id="createHeader"><%= Resources.Resource.Create %></h5>
                        <h5 class="modal-title text-center" id="editHeader"><%= Resources.Resource.Edit_Line %></h5>
                    </div>
                    <div class="modal-body">
                        <input type="text" id="ContractId" runat="server" hidden="" />
                        <div class="row">
                            <div class="col-sm-6">
                                <h6><%= Resources.Resource.Start %></h6>
                                <div class="input-group form-group">
                                    <span class="input-group-addon"><i class="icon-calendar"></i></span>
                                    <input type="text" id="ContractStartDate" name="RecStartDate" runat="server" class="form-control" />
                                </div>
                                <label id="ContractStartDate-error" class="validation-error-label" for="ContractStartDate" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                            </div>
                             <div class="col-sm-6">
                                <h6><%= Resources.Resource.VacationsPerYear %></h6>
                                <div class="input-group form-group">
                                    <span class="input-group-addon"><i class="icon-umbrella"></i></span>
                                    <input type="number" id="ContractVacationsPerYear" name="ContractVacationsPerYear"  runat="server" class="form-control" value="" placeholder=""  min="1" step="any" />
                                </div>
                                <label id="ContractVacationsPerYear-error" class="validation-error-label" for="ContractVacationsPerYear" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                            </div>

                            <div class="col-sm-6 hidden">
                                <h6><%= Resources.Resource.StartTime %></h6>
                                <div class="input-group form-group">
                                    <span class="input-group-addon"><i class="icon-watch2"></i></span>
                                    <input type="text" id="ContractStartTime" name="ContractStartTime"  runat="server" class="form-control" value="08:00" placeholder="hh:mm" onchange="onTimeChanged(event)"/>
                                </div>
                                
                                <label id="ContractStartTime-error" class="validation-error-label" for="ContractStartTime" style="display: none;"><%= Resources.Resource.Declined %></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <label id="ContractStartTime-warning" style=""><i class="fa fa-exclamation-triangle text-danger" aria-hidden="true"></i><%= Resources.Resource.ContractStartDateWarning %></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <h6><%= Resources.Resource.Rate %></h6>
                                <div class="input-group form-group">
                                    <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                    <input type="number" step="any" runat="server" id="ContractRate" name="ContractRate" class="form-control " value="100" min="0" 
                                           onchange="onRateChanged(event)"/>
                                </div>
                                <label id="ContractRate-error" class="validation-error-label" for="ContractRate" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                            </div>

                            <div class="col-sm-4">
                                <h6><%= Resources.Resource.Hours %></h6>
                                <div class="input-group form-group">
                                    <span class="input-group-addon"><i class="icon-hour-glass2"></i></span>
                                    <input type="number" step="any" id="ContractHours" name="ContractHours"  runat="server" class="form-control"  min="0"
                                            onchange="onHoursChanged(event)"/>
                                </div>
                                <label id="ContractHours-error" class="validation-error-label" for="ContractHours" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                            </div>
                            <div class="col-sm-4">
                                <h6><%= Resources.Resource.HourRate %></h6>
                                <div class="input-group form-group">
                                    <span class="input-group-addon"><i class="fa fa-usd"></i></span>
                                    <input type="number" id="ContractHourRate" name="ContractHourRate"  runat="server" class="form-control" value="" placeholder=""  min="1" step="any" />
                                </div>
                                <label id="ContractHourRate-error" class="validation-error-label" for="ContractHourRate" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <h6><%= Resources.Resource.OvertimeManagement %></h6>
                            </div>
                        </div>
                        <div class="row form-inline">
                            <div class="col-sm-12">
                                <label class="checkbox-inline">
                                    <input type="checkbox" runat="server" id="cbConsiderAsOvertime" class="checkbox-inline" />
                                    <%=string.Format(Resources.Resource.ConsiderAsOvertime, string.Format("<span id='week-hours-label'>{0}</span>", weekHours))%>
                                </label>
							</div>
						</div>

                        <!-- Overtime fields: hide if overtimes are disabled in active contract -->
                        <div id="contract_overview_section" <%= isOvertimeEnabled ? "" : "style='display:none;'" %>>
                            <div class="row form-inline">
                                <div class="col-sm-8">                                
                                    <div class="form-group">
                                        <label for="overtime-rate">
                                            <span class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                                <input type="number" runat="server" ID="tbxOvertimeRate" name="overtime-rate" class="form-control " value="100" min="0" max="1000" step="any" Width="70px" />
                                                <label id="overtimeRate-error" class="validation-error-label" for="tbxOvertimeRate" style="display: none;"><%= Resources.Resource.FieldIsRequired %></label>
                                            </span>
                                            <span><%= Resources.Resource.OvertimeRate %></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row form-inline">
                                <div <%= IsEnglish ? "class='col-sm-6'" : "class='col-sm-8'" %>>
                                    <div class="form-group">
                                        <input type="checkbox" style="margin: 0 10px 0 0" runat="server" id="cbxOvertime2Enabled" class="checkbox-inline checkbox-right" />
                                        <label for="overtime-rate2-hours-threshold">
                                            <span><%= Resources.Resource.After %></span>
                                            <asp:TextBox TextMode="Number" runat="server" ID="tbxOvertime2ThresholdHours" name="overtime-rate2-hours-threshold" CssClass="form-control" value="0" min="0" max="168" step="1"
                                                style="width:75px"/>
                                            <span><%= Resources.Resource.Hours.ToLower() %>, <%= Resources.Resource.ApplyRateOf %></span>
                                        </label>
                                    </div>
                                
                                </div>
                                <div <%= IsEnglish ? "class='col-sm-6'" : "class='col-sm-4'" %>>
                                    <div class="input-group form-group">
                                        <span class="input-group-addon"><i class="fa fa-percent"></i></span>
                                        <input type="number" runat="server" ID="tbxOvertimeRate2" name="overtime-rate2" class="form-control " value="100" min="0" max="1000" step="any" />
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="bSaveContract"  runat="server"  OnClick="bSaveContract_OnClick" OnClientClick="return  validateEditContract()" 
                                    CssClass="btn btn-primary" Text="<%$ Resources:Resource, Save %>" />  
                        <button type="button" class="btn btn-default" data-dismiss="modal"><%= Resources.Resource.Close %></button>
                    </div>
                </div>
            </div>
        </div>
       

        </div><!-- /content -->

    </form>
    <script type="text/javascript">

        var dateFormat = '<%= Resources.Resource.localisedDateFormat %>';
        var weekHours = '<%= WeekHoursOrg.ToString(System.Globalization.CultureInfo.InvariantCulture) %>';
        var hasPreviousContracts = <%= HasPreviousContracts.ToString().ToLowerInvariant() %>;

        var _tbxFirstName;
        var _tbxLastName;
        var _tbxInitials;
        
        $(document).ready(function() {
          $('input').on('keydown', function(event) {
            if (event.key === 'Enter' || event.keyCode === 13) {
              event.preventDefault();
              $(this).blur();
            }
          });
        });

        $('#<%= ContractStartDate.ClientID %>').pickadate({
            singleDatePicker: true,
            format: 'dd/mm/yyyy',
            min: true
        });

        function refreshInitials()
        {
            if (true)
            {
                var inits = _tbxFirstName.val();
                if (inits.length > 1) inits = inits.substring(0, 1);
                inits += _tbxLastName.val();
                if (inits.length > 3) inits = inits.substring(0, 3);
                inits = inits.toUpperCase();
                _tbxInitials.val(inits);
            }
        }

        $(function () {

            _tbxFirstName = $('#ContentPlaceHolder1_tbxFirstName');
            _tbxLastName = $('#ContentPlaceHolder1_tbxLastName');
            _tbxInitials = $('#ContentPlaceHolder1_tbxInitials');
            
            _tbxFirstName.keyup(refreshInitials);
            _tbxLastName.keyup(refreshInitials);

        });

        function onTimeChanged(e) {
            if (e && e.target) {
                var newVal = "";
                var value = $(e.target).val();

                var m = moment(value, moment.HTML5_FMT.TIME);
                var res = m.isValid();
                if (res)
                    newVal = m.format(moment.HTML5_FMT.TIME);

                $(e.target).val(newVal);
            }

        }

        function round(num) {
            var inc = 0.5;
            var r = Math.round((num + Number.EPSILON) * 100) / 100;
            var res = Math.ceil(r / inc) * inc;

            return res;
        }

        function onRateChanged(e) {
            if (e && e.target) {
                var newVal = "";
                var value = $(e.target).val();
                newVal = ((weekHours||40) * value) / 100;
                if (newVal > 0) {
                    $('#<%= ContractHours.ClientID %>').val(round(newVal));
                    $('#week-hours-label').text(round(newVal));
                }

                var vacationDaysPerYear = <%= VacationsPerYear %>;                
                var vacationDaysUpdated = Math.round(vacationDaysPerYear * value / 100);
                if (vacationDaysUpdated > 0) {
                    $('#<%= ContractVacationsPerYear.ClientID %>').val(vacationDaysUpdated);
                }
            }
        }

        function onRoleChanged(e) {
            if (e && e.target) {
                
                var value = $(e.target).val();
               
                if (value > 0)
                    $('#disabled-account').hide();
                else
                    $('#disabled-account').show();
            }

        }        

        function onHoursChanged(e) {
            if (e && e.target) {
                var newVal = "";
                var value = $(e.target).val();
                newVal = (100 * value) / (weekHours || 40);
                if (newVal > 0) {
                    $('#week-hours-label').text(value);
                    $('#<%= ContractRate.ClientID %>').val(round(newVal));
                }
            }

        }

        function deleteContract(id) {
            $('#<%= ContractId.ClientID %>').val(id);

            $('#modal_delete_prof').modal('show');
        }

        function validateEditContract() {

            var res = true;
            var start = $('#<%= ContractStartDate.ClientID %>').val();
            var rate = $('#<%= ContractRate.ClientID %>').val();
            var hours = $('#<%= ContractHours.ClientID %>').val();
            var overtimeRate = $('#<%= tbxOvertimeRate.ClientID %>').val();
            
            if (!hours || !start || !rate) {
                res = false;

                if (!rate)
                    $('#ContractRate-error').show();
                else
                    $('#ContractRate-error').hide();

                if (!start)
                    $('#ContractStartDate-error').show();
                else
                    $('#ContractStartDate-error').hide();

                if (!hours)
                    $('#ContractHours-error').show();
                else
                    $('#ContractHours-error').hide();
            }
            else {
                $('#ContractRate-error').hide();
                $('#ContractStartDate-error').hide();
                $('#ContractStartTime-error').hide();
                $('#ContractHours-error').hide();
                $('#ContractHourRate-error').hide();
                $('#ContractVacationsPerYear-error').hide();
            }

            if (overtimeRate) {
                var overtimeRateNumeric = 1 * overtimeRate;
                if (overtimeRateNumeric > 1000 || overtimeRateNumeric < 0) {
                    $('#overtimeRate-error').show();
                }
                else {
                    $('#overtimeRate-error').hide();
                }
            }

            var contractId = $('#<%= ContractId.ClientID %>').val();
            if (contractId == 0 && hasPreviousContracts) {
                var cleanStart = new Date(moment(start, "DD/MM/YYYY").format());
                var today = new Date();
                today.setHours(0);
                today.setMinutes(0);
                today.setSeconds(0);
                today.setMilliseconds(0);
                if (cleanStart < today) {
                    alert('Invalid start date');
                    return false;
                }
            }

            return res;
        }

        function editContract(id, start, rate, hours, hourRate, vacYear, overtimeEnabled, overtimeRate, overtimeRate2, overtime2ThresholdHours, overtime2Enabled) {
            console.log(rate, hours, hourRate, vacYear);

            $('#ContractStart-error').hide();
            $('#ContractRate-error').hide();
            $('#ContractHours-error').hide();
            $('#ContractHourRate-error').hide();
            $('#ContractVacationsPerYear-error').hide();
            $('#overtimeRate-error').hide();
            
            $('#<%= ContractId.ClientID %>').val(id);

            var mobj = start && moment(start, dateFormat.toUpperCase() + " " + moment.HTML5_FMT.TIME) || null;

            var sd = mobj && mobj.format(dateFormat.toUpperCase()) || moment().format(dateFormat.toUpperCase());
            var st = mobj && mobj.format(moment.HTML5_FMT.TIME) || "08:00";

            $('#<%= ContractStartDate.ClientID %>').val(sd);
            $('#<%= ContractStartDate.ClientID %>').prop("disabled", id > 0);
            $("#ContractStartTime-warning").css("display", id > 0 ? "none" : "");

            $('#<%= ContractStartTime.ClientID %>').val(st);
            
            $('#<%= ContractRate.ClientID %>').val(rate||100);
            $('#<%= ContractHours.ClientID %>').val(hours || weekHours||40);

            $('#<%= ContractHourRate.ClientID %>').val(hourRate);
            $('#<%= ContractVacationsPerYear.ClientID %>').val(vacYear||20);
            
            $('#<%= cbConsiderAsOvertime.ClientID %>').prop('checked', overtimeEnabled);
            $('#<%= tbxOvertimeRate.ClientID %>').val(overtimeRate);
            $('#<%= tbxOvertimeRate2.ClientID %>').val(overtimeRate2);
            $('#<%= tbxOvertime2ThresholdHours.ClientID %>').val(overtime2ThresholdHours);
            $('#<%= cbxOvertime2Enabled.ClientID %>').prop('checked', overtime2Enabled);

            toggleOvertimeSection(overtimeEnabled);
            
            if (id > 0) {
                $('#createHeader').hide();
                $('#editHeader').show();
            } else {
                $('#createHeader').show();
                $('#editHeader').hide();
            }

            $('#modal_contract').modal('show');
            
        }

        var attachmentsCollection = <%= JsonAttachments %>;

        Dropzone.autoDiscover = false;
        $("#dropzone_multiple").dropzone({
            url: "../../UploadDocument.ashx",
            dictDefaultMessage: STRING_RESOURCES.DropFilesToUpload + ' <span>' + STRING_RESOURCES.OrClick + '</span>',
            maxFilesize: 30,
            init: function () {
                this.on("complete", function (file) {

                    var val = $('#<%= hfFile.ClientID %>').val();
                    if (!val) val = '[]';
                    var parsedArr = JSON.parse(val);

                    parsedArr.push(file.xhr.responseText);

                    var sval = JSON.stringify(parsedArr);
                    $('#<%= hfFile.ClientID %>').val(sval);

                });
            }
        });

        $('.btnDeleteAttachment').click(function () {

            var btn = $(this);
            var tr = btn.closest('tr');
            var id = btn.data('attachment') * 1;
            var attname = btn.data('name');

            swal({
                title: STRING_RESOURCES['DeleteAttachment'],
                text: STRING_RESOURCES['DoYouWantToDeleteAttachment'] + ': ' + attname + ' ?',
                type: "success",
                showCancelButton: true,
                cancelButtonText: 'No',
                confirmButtonColor: "#4CAF50",
                confirmButtonText: 'Yes'
            })
                .then((value) => {
                    if (value.value != null && value.value == true) {
                        var pa = { AttachmentId: id };

                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: '../../API/Service.svc/DeleteAttachment',
                            data: JSON.stringify(pa),
                            processData: false,
                            dataType: "json",
                            success: function (response) {
                                tr.hide();
                                onAttachmentDeleted(id);
                            },
                            error: function (a, b, c) {
                            }
                        });

                    }
                });
        });

        $('#btnAddActuallyPaidHours').click(function () {
            var hoursToAdd = parseFloat($('#tbxAddTotalHoursActuallyPaid').val());
            if (isNaN(hoursToAdd)) return;

            var actuallyPaidHours = parseFloat($('#<%= tbxTotalHoursActuallyPaid.ClientID %>').val());
            actuallyPaidHours += hoursToAdd;            
            $('#<%= tbxTotalHoursActuallyPaid.ClientID %>').val(formatDecimal(actuallyPaidHours));
            $('#tbxAddTotalHoursActuallyPaid').val('');
            addToHoursToBeCompensated(formatDecimal(-1 * hoursToAdd));
        });

        $('#btnAddEffectivelyCompensatedHours').click(function () {
            var hoursToAdd = parseFloat($('#tbxAddTotalHoursEffectivelyCompensated').val());
            if (isNaN(hoursToAdd)) return;

            var compensatedHours = parseFloat($('#<%= tbxTotalHoursEffectivelyCompensated.ClientID %>').val());
            compensatedHours += hoursToAdd;
            $('#<%= tbxTotalHoursEffectivelyCompensated.ClientID %>').val(formatDecimal(compensatedHours));
            $('#tbxAddTotalHoursEffectivelyCompensated').val('');
            addToHoursToBeCompensated(formatDecimal(-1 * hoursToAdd));
        });

        function addToHoursToBeCompensated(valueToAdd) {
            if (!valueToAdd) {
                return;
            }
            var currentVal = $("#total-hours-to-be-compensated").val();
            $("#total-hours-to-be-compensated").val(formatDecimal(1 * currentVal + 1 * valueToAdd));
        }

        function formatDecimal(d) {
            d = d
                .toFixed(2)
                .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ');
            return d;
        }

        $('#<%= cbConsiderAsOvertime.ClientID %>').click(function () {
            var overtimeEnabled = $(this).prop('checked');
            toggleOvertimeSection(overtimeEnabled);
        });

        function onAttachmentDeleted(id) {
            attachmentsCollection = $.grep(attachmentsCollection, function (a) { return a.id != id; });
            $("li#li-attachment-" + id + " input[type=checkbox]").prop('checked', false);
            $("li#li-attachment-" + id).css("display", "none");
            // showHideAttachmentsSettingsPanel();
        }

        function toggleOvertimeSection(enabled) {
            $('#<%= tbxOvertimeRate.ClientID %>').prop('disabled', !enabled);
            $('#<%= tbxOvertimeRate2.ClientID %>').prop('disabled', !enabled);
            $('#<%= tbxOvertime2ThresholdHours.ClientID %>').prop('disabled', !enabled);
            $('#<%= cbxOvertime2Enabled.ClientID %>').prop('disabled', !enabled);
            $('#contract_overview_section').css("display", enabled ? "block" : "none");
        }
    </script>
</asp:Content>