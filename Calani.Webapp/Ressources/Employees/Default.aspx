﻿<%@ Page Title="Employees" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="Ressources_Employees_Default" Codebehind="Default.aspx.cs" %>

<%@ Register src="../../assets/Controls/UserRoleLabel.ascx" tagname="UserRoleLabel" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form runat="server">


        <div class="content">
					<div>
						<div class="panel panel-white">
                            
							<div class="panel-heading">
								<h6 class="panel-title">
									<a name="quotes">
										<i class="<%= ph.CMP.Icon %> display-inline-block text-<%= ph.CMP.Color %>"></i>
										<strong><%= ph.CMP.Name %></strong>
									</a>
								</h6>
							</div><!-- panel-heading -->

                            

							<div class="panel-body">
                                <div class="col-lg-12" id="alertUserLimitExceeded"   runat="server">
                                        <div class="alert alert-danger alert-styled-left">
                                            <span class="font-weight-semibold"><%= Resources.Resource.UserLimitExceeded %>:</span> <a href="mailto:info@gesmobile.ch">info@gesmobile.ch</a>
                                        </div>
                                 
                                    </div>
                                <asp:GridView runat="server" ID="grid" AutoGenerateColumns="false">
                                    <Columns>
                                        <asp:HyperLinkField DataTextField="initials" HeaderText="Initials" DataNavigateUrlFields="id" DataNavigateUrlFormatString="Edit.aspx?id={0}"/>
                                        <asp:BoundField DataField="lastName" HeaderText="Lastname"   />
                                        <asp:BoundField DataField="firstName" HeaderText="Firstname"   />
                                        <asp:BoundField DataField="primaryEmail" HeaderText="Email"   />
                                        <asp:BoundField DataField="primaryPhone" HeaderText="Phone"   />
                                        <asp:TemplateField HeaderText="ApplicationRole">
                                            <ItemTemplate>
                                                <uc1:UserRoleLabel ID="UserRoleLabel1" runat="server" UserRole='<%# Bind("type") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="RateStr" HeaderText="Rate"   />
                                    </Columns>
                                </asp:GridView>
                            </div><!-- panel-body -->

                            <div class="panel-footer text-right no-padding">
						    	<div class="row" style="margin:10px">
						    		<a href="Edit.aspx" class="btn btn-default" runat="server" ID="addUserButton"><i class="icon-plus2 position-left"></i> 
                                        <%= Resources.Resource.Add_new_Employee %>
                                   
						    		</a>
						    	</div>
							</div><!-- panel-footer -->

                        </div>
                    </div>
        </div><!-- /content -->

    </form>
</asp:Content>

