﻿using System;
using System.Linq;

public partial class Ressources_Employees_Default : System.Web.UI.Page
{
    public PageHelper ph;
    public  bool userLimitExceeded { get; set; }
    protected override void InitializeCulture()
    {
        ph = new PageHelper(this);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        EnableViewState = false;
        if (!IsPostBack)
        {
            Calani.BusinessObjects.Contacts.EmployeesManager mgr = new Calani.BusinessObjects.Contacts.EmployeesManager(ph.CurrentOrganizationId);
            var list = mgr.ListEmployees();

            grid.DataSource = list;

            var orgMgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(ph.CurrentOrganizationId);
            orgMgr.Load();
            var userLimit = orgMgr.UerLimit;
            var count = list.Where(u => u.type !=0).Count();

            userLimitExceeded = userLimit.HasValue && userLimit > 0 && userLimit <= count;
            
            alertUserLimitExceeded.Visible = userLimitExceeded;
            addUserButton.Disabled = userLimitExceeded;

            new Html5GridView(grid);
        }
    }
}