﻿using System;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySql;

namespace calani_business_obj_test
{
    [TestClass]
    public class identity_class
    {
        [TestMethod]
        //MySql.Data.MySqlClient.MySqlException
          // [ExpectedException(typeof(System.Data.Entity.Validation.DbEntityValidationException))]
      //    [ExpectedException(typeof(System.Data.Entity.Infrastructure.DbUpdateException))]
      // [ExpectedException(typeof(System.InvalidOperationException))]
        public void identity_method()
        {
           

        }


        [TestMethod]
        [ExpectedException(typeof(System.InvalidOperationException))]
        // when all the paramerts set true/ some fields updated as null
        public void Should_throw_Inavlid_Operation_Exception_when_Passing_true_to_save_method()
        {
            Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(260);
            mgr.CompanyName = "ABC Software";//.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.Email = "abcsoftware.com";
            mgr.Phone = "9898989898";
            mgr.Telecopy = "";// 12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            mgr.AdrStreet = "";
            mgr.AdrStreetNb = "";
            mgr.AdrStreet2 = "";
            mgr.AdrCity = "";
            mgr.AdrZipCode = "";
            mgr.AdrState = "";
            mgr.AdrCountry = "";
            mgr.AdrPostalBox = "";
            mgr.VatNumber = "";
            mgr.WebSite = "";


            mgr.BankAccount1_AccountName = "";
            mgr.BankAccount1_BankName = "";
            mgr.BankAccount1_Currency = "";
            mgr.BankAccount1_Iban = "";
            mgr.BankAccount1_Swift = "";

            mgr.BankAccount2_AccountName = "";
            mgr.BankAccount2_BankName = "";
            mgr.BankAccount2_Currency = "";
            mgr.BankAccount2_Iban = "";
            mgr.BankAccount2_Swift = "";

            if (!String.IsNullOrWhiteSpace(""))
            {
                mgr.OwnerUserId = Convert.ToInt64("1");
            }

            /* try
             {
                 ImageUploadExtractor upload = new ImageUploadExtractor(fileImage);
                 upload.Mime.Add("image/png");
                 upload.Mime.Add("image/jpeg");
                 upload.Mime.Add("image/gif");
                 byte[] binImage = upload.Extract();
                 if (binImage != null)
                 {
                     mgr.Logo = binImage;
                 }
             }
             catch (Exception ex)
             {
                 Calani.BusinessObjects.Generic.WarningReport.Report("Try to upload invalid image format", ex);
             }*/


            mgr.BvrMode = Convert2.StrToInt("", 0);
            mgr.BvrType = Convert2.StrToInt("", 0);
            mgr.BvrTopMargin = Convert2.StrToInt("", 0);
            mgr.BvrLeftMargin = Convert2.StrToInt("", 0);
            mgr.BvrSenderBank = "";
            mgr.BvrAccount = "";
            mgr.BvrBankLine1 = "";
            mgr.BvrBankLine2 = "";


            var ret = mgr.Save(true, true, true, true);
            //  Assert.AreEqual(true, ret.Success);
        }

        [TestMethod]
        [ExpectedException(typeof(MySql.Data.MySqlClient.MySqlException))]
        //set all fields empty must get exception
        public void Expecting_mysql_OR_DBNULL_Exception_when_sending_all_values_blank()
        {
            Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(260);
            mgr.CompanyName = "";//.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.Email = "";
            mgr.Phone = "";
            mgr.Telecopy = "";// 12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            mgr.AdrStreet = "";
            mgr.AdrStreetNb = "";
            mgr.AdrStreet2 = "";
            mgr.AdrCity = "";
            mgr.AdrZipCode = "";
            mgr.AdrState = "";
            mgr.AdrCountry = "";
            mgr.AdrPostalBox = "";
            mgr.VatNumber = "";
            mgr.WebSite = "";


            mgr.BankAccount1_AccountName = "";
            mgr.BankAccount1_BankName = "";
            mgr.BankAccount1_Currency = "";
            mgr.BankAccount1_Iban = "";
            mgr.BankAccount1_Swift = "";

            mgr.BankAccount2_AccountName = "";
            mgr.BankAccount2_BankName = "";
            mgr.BankAccount2_Currency = "";
            mgr.BankAccount2_Iban = "";
            mgr.BankAccount2_Swift = "";

            if (!String.IsNullOrWhiteSpace(""))
            {
                mgr.OwnerUserId = Convert.ToInt64("1");
            }

            /* try
             {
                 ImageUploadExtractor upload = new ImageUploadExtractor(fileImage);
                 upload.Mime.Add("image/png");
                 upload.Mime.Add("image/jpeg");
                 upload.Mime.Add("image/gif");
                 byte[] binImage = upload.Extract();
                 if (binImage != null)
                 {
                     mgr.Logo = binImage;
                 }
             }
             catch (Exception ex)
             {
                 Calani.BusinessObjects.Generic.WarningReport.Report("Try to upload invalid image format", ex);
             }*/


            mgr.BvrMode = Convert2.StrToInt("", 0);
            mgr.BvrType = Convert2.StrToInt("", 0);
            mgr.BvrTopMargin = Convert2.StrToInt("", 0);
            mgr.BvrLeftMargin = Convert2.StrToInt("", 0);
            mgr.BvrSenderBank = "";
            mgr.BvrAccount = "";
            mgr.BvrBankLine1 = "";
            mgr.BvrBankLine2 = "";


            var ret = mgr.Save(true, true, false, true);
            Assert.AreEqual(true, ret.Success);
        }

        [TestMethod]
        //set all fields empty , not throwing any exception
        public void Expecting_Any_Random_Exception_With_NULL_VALUES()
        {
            Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(260);
            mgr.CompanyName = "";//.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.Email = "";
            mgr.Phone = "";
            mgr.Telecopy = "";// 12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            mgr.AdrStreet = "";
            mgr.AdrStreetNb = "";
            mgr.AdrStreet2 = "";
            mgr.AdrCity = "";
            mgr.AdrZipCode = "";
            mgr.AdrState = "";
            mgr.AdrCountry = "";
            mgr.AdrPostalBox = "";
            mgr.VatNumber = "";
            mgr.WebSite = "";


            mgr.BankAccount1_AccountName = "";
            mgr.BankAccount1_BankName = "";
            mgr.BankAccount1_Currency = "";
            mgr.BankAccount1_Iban = "";
            mgr.BankAccount1_Swift = "";

            mgr.BankAccount2_AccountName = "";
            mgr.BankAccount2_BankName = "";
            mgr.BankAccount2_Currency = "";
            mgr.BankAccount2_Iban = "";
            mgr.BankAccount2_Swift = "";

            if (!String.IsNullOrWhiteSpace(""))
            {
                mgr.OwnerUserId = Convert.ToInt64("1");
            }

            /* try
             {
                 ImageUploadExtractor upload = new ImageUploadExtractor(fileImage);
                 upload.Mime.Add("image/png");
                 upload.Mime.Add("image/jpeg");
                 upload.Mime.Add("image/gif");
                 byte[] binImage = upload.Extract();
                 if (binImage != null)
                 {
                     mgr.Logo = binImage;
                 }
             }
             catch (Exception ex)
             {
                 Calani.BusinessObjects.Generic.WarningReport.Report("Try to upload invalid image format", ex);
             }*/


            mgr.BvrMode = Convert2.StrToInt("", 0);
            mgr.BvrType = Convert2.StrToInt("", 0);
            mgr.BvrTopMargin = Convert2.StrToInt("", 0);
            mgr.BvrLeftMargin = Convert2.StrToInt("", 0);
            mgr.BvrSenderBank = "";
            mgr.BvrAccount = "";
            mgr.BvrBankLine1 = "";
            mgr.BvrBankLine2 = "";


            var ret = mgr.Save(true, true, false, true);
            Assert.AreEqual(true, ret.Success);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Data.Entity.Validation.DbEntityValidationException))]
        // input data larger than db column size
        public void Expecting_Db_Entity_Validation_Exception_When_sending_values_large_in_size_compare_to_DB_Column_size()
        {
            Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(260);
            mgr.CompanyName = "ABC Softwareaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.Email = "abcsoftware.com";
            mgr.Phone = "9898989898";
            mgr.Telecopy = "";// 12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            mgr.AdrStreet = "";
            mgr.AdrStreetNb = "";
            mgr.AdrStreet2 = "";
            mgr.AdrCity = "";
            mgr.AdrZipCode = "";
            mgr.AdrState = "";
            mgr.AdrCountry = "";
            mgr.AdrPostalBox = "";
            mgr.VatNumber = "";
            mgr.WebSite = "";


            mgr.BankAccount1_AccountName = "";
            mgr.BankAccount1_BankName = "";
            mgr.BankAccount1_Currency = "";
            mgr.BankAccount1_Iban = "";
            mgr.BankAccount1_Swift = "";

            mgr.BankAccount2_AccountName = "";
            mgr.BankAccount2_BankName = "";
            mgr.BankAccount2_Currency = "";
            mgr.BankAccount2_Iban = "";
            mgr.BankAccount2_Swift = "";

            if (!String.IsNullOrWhiteSpace(""))
            {
                mgr.OwnerUserId = Convert.ToInt64("1");
            }

            /* try
             {
                 ImageUploadExtractor upload = new ImageUploadExtractor(fileImage);
                 upload.Mime.Add("image/png");
                 upload.Mime.Add("image/jpeg");
                 upload.Mime.Add("image/gif");
                 byte[] binImage = upload.Extract();
                 if (binImage != null)
                 {
                     mgr.Logo = binImage;
                 }
             }
             catch (Exception ex)
             {
                 Calani.BusinessObjects.Generic.WarningReport.Report("Try to upload invalid image format", ex);
             }*/


            mgr.BvrMode = Convert2.StrToInt("", 0);
            mgr.BvrType = Convert2.StrToInt("", 0);
            mgr.BvrTopMargin = Convert2.StrToInt("", 0);
            mgr.BvrLeftMargin = Convert2.StrToInt("", 0);
            mgr.BvrSenderBank = "";
            mgr.BvrAccount = "";
            mgr.BvrBankLine1 = "";
            mgr.BvrBankLine2 = "";


            var ret = mgr.Save(true, true, false, true);
            //  Assert.AreEqual(true, ret.Success);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Data.Entity.Infrastructure.DbUpdateException))]
        //alpha numeric acceptance by phone
        public void Expecting_Infrastructure_DbUpdate_Exception_When_saving_Alphanumeric_to_Phone_column()
        {
            Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(260);
            mgr.CompanyName = "ABC Software";// aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.Email = "abcsoftware.com";
            mgr.Phone = "12sdwefr2e";
            mgr.Telecopy = "12sdwefr2e";

            mgr.AdrStreet = "";
            mgr.AdrStreetNb = "";
            mgr.AdrStreet2 = "";
            mgr.AdrCity = "";
            mgr.AdrZipCode = "";
            mgr.AdrState = "";
            mgr.AdrCountry = "";
            mgr.AdrPostalBox = "";
            mgr.VatNumber = "";
            mgr.WebSite = "";


            mgr.BankAccount1_AccountName = "";
            mgr.BankAccount1_BankName = "";
            mgr.BankAccount1_Currency = "";
            mgr.BankAccount1_Iban = "";
            mgr.BankAccount1_Swift = "";

            mgr.BankAccount2_AccountName = "";
            mgr.BankAccount2_BankName = "";
            mgr.BankAccount2_Currency = "";
            mgr.BankAccount2_Iban = "";
            mgr.BankAccount2_Swift = "";

            if (!String.IsNullOrWhiteSpace(""))
            {
                mgr.OwnerUserId = Convert.ToInt64("1");
            }

            /* try
             {
                 ImageUploadExtractor upload = new ImageUploadExtractor(fileImage);
                 upload.Mime.Add("image/png");
                 upload.Mime.Add("image/jpeg");
                 upload.Mime.Add("image/gif");
                 byte[] binImage = upload.Extract();
                 if (binImage != null)
                 {
                     mgr.Logo = binImage;
                 }
             }
             catch (Exception ex)
             {
                 Calani.BusinessObjects.Generic.WarningReport.Report("Try to upload invalid image format", ex);
             }*/


            mgr.BvrMode = Convert2.StrToInt("", 0);
            mgr.BvrType = Convert2.StrToInt("", 0);
            mgr.BvrTopMargin = Convert2.StrToInt("", 0);
            mgr.BvrLeftMargin = Convert2.StrToInt("", 0);
            mgr.BvrSenderBank = "";
            mgr.BvrAccount = "";
            mgr.BvrBankLine1 = "";
            mgr.BvrBankLine2 = "";


            var ret = mgr.Save(true, true, false, true);
            //  Assert.AreEqual(true, ret.Success);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Data.Entity.Validation.DbEntityValidationException))]
        // input data larger then db column defined value for multilpe columns
        public void Expecting_DbEntityValidationException_When_saving_large_Values()
        {
            Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(260);
            mgr.CompanyName = "ABC Software";//aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.Email = "abcsoftware.comaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.Phone = "12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            mgr.Telecopy = "12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            mgr.AdrStreet = "";
            mgr.AdrStreetNb = "";
            mgr.AdrStreet2 = "";
            mgr.AdrCity = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.AdrZipCode = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.AdrState = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.AdrCountry = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.AdrPostalBox = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.VatNumber = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.WebSite = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";


            mgr.BankAccount1_AccountName = "";
            mgr.BankAccount1_BankName = "";
            mgr.BankAccount1_Currency = "";
            mgr.BankAccount1_Iban = "";
            mgr.BankAccount1_Swift = "";

            mgr.BankAccount2_AccountName = "";
            mgr.BankAccount2_BankName = "";
            mgr.BankAccount2_Currency = "";
            mgr.BankAccount2_Iban = "";
            mgr.BankAccount2_Swift = "";

            if (!String.IsNullOrWhiteSpace(""))
            {
                mgr.OwnerUserId = Convert.ToInt64("1");
            }

            /* try
             {
                 ImageUploadExtractor upload = new ImageUploadExtractor(fileImage);
                 upload.Mime.Add("image/png");
                 upload.Mime.Add("image/jpeg");
                 upload.Mime.Add("image/gif");
                 byte[] binImage = upload.Extract();
                 if (binImage != null)
                 {
                     mgr.Logo = binImage;
                 }
             }
             catch (Exception ex)
             {
                 Calani.BusinessObjects.Generic.WarningReport.Report("Try to upload invalid image format", ex);
             }*/


            mgr.BvrMode = Convert2.StrToInt("", 0);
            mgr.BvrType = Convert2.StrToInt("", 0);
            mgr.BvrTopMargin = Convert2.StrToInt("", 0);
            mgr.BvrLeftMargin = Convert2.StrToInt("", 0);
            mgr.BvrSenderBank = "";
            mgr.BvrAccount = "";
            mgr.BvrBankLine1 = "";
            mgr.BvrBankLine2 = "";


            var ret = mgr.Save(true, true, false, true);
            //  Assert.AreEqual(true, ret.Success);
        }


        [TestMethod]
        [ExpectedException(typeof(System.InvalidOperationException))]
        // sending non existing org id
        public void Expecting_Invalid_Operation_Exception_When_Passing_non_existing_organizationId()
        {
            Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(2600);
            mgr.CompanyName = "ABC Software";// aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.Email = "abcsoftware.com";
            mgr.Phone = "";// 12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            mgr.Telecopy = "";// 12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            mgr.AdrStreet = "";
            mgr.AdrStreetNb = "";
            mgr.AdrStreet2 = "";
            mgr.AdrCity = "";
            mgr.AdrZipCode = "";
            mgr.AdrState = "";
            mgr.AdrCountry = "";
            mgr.AdrPostalBox = "";
            mgr.VatNumber = "";
            mgr.WebSite = "";


            mgr.BankAccount1_AccountName = "";
            mgr.BankAccount1_BankName = "";
            mgr.BankAccount1_Currency = "";
            mgr.BankAccount1_Iban = "";
            mgr.BankAccount1_Swift = "";

            mgr.BankAccount2_AccountName = "";
            mgr.BankAccount2_BankName = "";
            mgr.BankAccount2_Currency = "";
            mgr.BankAccount2_Iban = "";
            mgr.BankAccount2_Swift = "";

            if (!String.IsNullOrWhiteSpace(""))
            {
                mgr.OwnerUserId = Convert.ToInt64("1");
            }

            /* try
             {
                 ImageUploadExtractor upload = new ImageUploadExtractor(fileImage);
                 upload.Mime.Add("image/png");
                 upload.Mime.Add("image/jpeg");
                 upload.Mime.Add("image/gif");
                 byte[] binImage = upload.Extract();
                 if (binImage != null)
                 {
                     mgr.Logo = binImage;
                 }
             }
             catch (Exception ex)
             {
                 Calani.BusinessObjects.Generic.WarningReport.Report("Try to upload invalid image format", ex);
             }*/


            mgr.BvrMode = Convert2.StrToInt("", 0);
            mgr.BvrType = Convert2.StrToInt("", 0);
            mgr.BvrTopMargin = Convert2.StrToInt("", 0);
            mgr.BvrLeftMargin = Convert2.StrToInt("", 0);
            mgr.BvrSenderBank = "";
            mgr.BvrAccount = "";
            mgr.BvrBankLine1 = "";
            mgr.BvrBankLine2 = "";


            var ret = mgr.Save(true, true, false, true);
            //  Assert.AreEqual(true, ret.Success);
        }

        [TestMethod]
        [ExpectedException(typeof(System.InvalidOperationException))]
        // change in non existing currency value for corss check with currency master
        public void Expecting_InvalidOperationException_for_checking_foreign_key_for_currency_master()
        {
            Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(260);
            mgr.CompanyName = "ABC Software";// aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.Email = "abcsoftware.com";
            mgr.Phone = "";// 12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            mgr.Telecopy = "";// 12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            mgr.AdrStreet = "";
            mgr.AdrStreetNb = "";
            mgr.AdrStreet2 = "";
            mgr.AdrCity = "";
            mgr.AdrZipCode = "";
            mgr.AdrState = "";
            mgr.AdrCountry = "";
            mgr.AdrPostalBox = "";
            mgr.VatNumber = "";
            mgr.WebSite = "";


            mgr.BankAccount1_AccountName = "";
            mgr.BankAccount1_BankName = "";
            mgr.BankAccount1_Currency = "INR";
            mgr.BankAccount1_Iban = "";
            mgr.BankAccount1_Swift = "";

            mgr.BankAccount2_AccountName = "";
            mgr.BankAccount2_BankName = "";
            mgr.BankAccount2_Currency = "INR";
            mgr.BankAccount2_Iban = "";
            mgr.BankAccount2_Swift = "";

            if (!String.IsNullOrWhiteSpace(""))
            {
                mgr.OwnerUserId = Convert.ToInt64("1");
            }

            /* try
             {
                 ImageUploadExtractor upload = new ImageUploadExtractor(fileImage);
                 upload.Mime.Add("image/png");
                 upload.Mime.Add("image/jpeg");
                 upload.Mime.Add("image/gif");
                 byte[] binImage = upload.Extract();
                 if (binImage != null)
                 {
                     mgr.Logo = binImage;
                 }
             }
             catch (Exception ex)
             {
                 Calani.BusinessObjects.Generic.WarningReport.Report("Try to upload invalid image format", ex);
             }*/


            mgr.BvrMode = Convert2.StrToInt("", 0);
            mgr.BvrType = Convert2.StrToInt("", 0);
            mgr.BvrTopMargin = Convert2.StrToInt("", 0);
            mgr.BvrLeftMargin = Convert2.StrToInt("", 0);
            mgr.BvrSenderBank = "";
            mgr.BvrAccount = "";
            mgr.BvrBankLine1 = "";
            mgr.BvrBankLine2 = "";


            var ret = mgr.Save(true, true, false, true);
            //  Assert.AreEqual(true, ret.Success);
        }

        [TestMethod]
        [ExpectedException(typeof(MySql.Data.MySqlClient.MySqlException))]
        // file upload test for different extension
        public void Expecting_mysql_exception_for_fileupload_with_non_image_extension_and_large_size()
        {
            Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(260);
            mgr.CompanyName = "ABC Software";// aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.Email = "abcsoftware.com";
            mgr.Phone = "";// 12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            mgr.Telecopy = "";// 12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            mgr.AdrStreet = "";
            mgr.AdrStreetNb = "";
            mgr.AdrStreet2 = "";
            mgr.AdrCity = "";
            mgr.AdrZipCode = "";
            mgr.AdrState = "";
            mgr.AdrCountry = "";
            mgr.AdrPostalBox = "";
            mgr.VatNumber = "";
            mgr.WebSite = "";


            mgr.BankAccount1_AccountName = "";
            mgr.BankAccount1_BankName = "";
            mgr.BankAccount1_Currency = "";
            mgr.BankAccount1_Iban = "";
            mgr.BankAccount1_Swift = "";

            mgr.BankAccount2_AccountName = "";
            mgr.BankAccount2_BankName = "";
            mgr.BankAccount2_Currency = "";
            mgr.BankAccount2_Iban = "";
            mgr.BankAccount2_Swift = "";

            if (!String.IsNullOrWhiteSpace(""))
            {
                mgr.OwnerUserId = Convert.ToInt64("1");
            }

            Image img = Image.FromFile("C:\\Users\\Shivoham\\Desktop\\Recruitment_Plus.docx");
            byte[] binImage = (byte[])(new ImageConverter()).ConvertTo(img, typeof(byte[]));
            if (binImage != null)
            {
                mgr.Logo = binImage;
            }
            /* try
             {
                 ImageUploadExtractor upload = new ImageUploadExtractor(fileImage);
                 upload.Mime.Add("image/png");
                 upload.Mime.Add("image/jpeg");
                 upload.Mime.Add("image/gif");
                 byte[] binImage = upload.Extract();
                 if (binImage != null)
                 {
                     mgr.Logo = binImage;
                 }
             }
             catch (Exception ex)
             {
                 Calani.BusinessObjects.Generic.WarningReport.Report("Try to upload invalid image format", ex);
             }*/


            mgr.BvrMode = Convert2.StrToInt("", 0);
            mgr.BvrType = Convert2.StrToInt("", 0);
            mgr.BvrTopMargin = Convert2.StrToInt("", 0);
            mgr.BvrLeftMargin = Convert2.StrToInt("", 0);
            mgr.BvrSenderBank = "";
            mgr.BvrAccount = "";
            mgr.BvrBankLine1 = "";
            mgr.BvrBankLine2 = "";


            var ret = mgr.Save(true, true, false, true);
              Assert.AreEqual(true, ret.Success);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Data.Entity.Validation.DbEntityValidationException))]
        // bvr margin case 1

        public void Expecting_DbEntityValidationException_for_String_values_in_BVR_Margin()
        {
            Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(260);
            mgr.CompanyName = "ABC Software";// aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.Email = "abcsoftware.com";
            mgr.Phone = "";// 12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            mgr.Telecopy = "";// 12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            mgr.AdrStreet = "";
            mgr.AdrStreetNb = "";
            mgr.AdrStreet2 = "";
            mgr.AdrCity = "";
            mgr.AdrZipCode = "";
            mgr.AdrState = "";
            mgr.AdrCountry = "";
            mgr.AdrPostalBox = "";
            mgr.VatNumber = "";
            mgr.WebSite = "";


            mgr.BankAccount1_AccountName = "";
            mgr.BankAccount1_BankName = "";
            mgr.BankAccount1_Currency = "";
            mgr.BankAccount1_Iban = "";
            mgr.BankAccount1_Swift = "";

            mgr.BankAccount2_AccountName = "";
            mgr.BankAccount2_BankName = "";
            mgr.BankAccount2_Currency = "";
            mgr.BankAccount2_Iban = "";
            mgr.BankAccount2_Swift = "";

            if (!String.IsNullOrWhiteSpace(""))
            {
                mgr.OwnerUserId = Convert.ToInt64("1");
            }

            /* try
             {
                 ImageUploadExtractor upload = new ImageUploadExtractor(fileImage);
                 upload.Mime.Add("image/png");
                 upload.Mime.Add("image/jpeg");
                 upload.Mime.Add("image/gif");
                 byte[] binImage = upload.Extract();
                 if (binImage != null)
                 {
                     mgr.Logo = binImage;
                 }
             }
             catch (Exception ex)
             {
                 Calani.BusinessObjects.Generic.WarningReport.Report("Try to upload invalid image format", ex);
             }*/


            mgr.BvrMode = Convert2.StrToInt("", 0);
            mgr.BvrType = Convert2.StrToInt("", 0);
            mgr.BvrTopMargin = Convert2.StrToInt("abc", 0);
            mgr.BvrLeftMargin = Convert2.StrToInt("abc", 0);
            mgr.BvrSenderBank = "";
            mgr.BvrAccount = "";
            mgr.BvrBankLine1 = "";
            mgr.BvrBankLine2 = "";


            var ret = mgr.Save(true, true, false, true);
            //  Assert.AreEqual(true, ret.Success);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Data.Entity.Infrastructure.DbUpdateException))]
        // bvr margin case 2

        public void Expecting_Entity_Infrastructure_DbUpdateException_for_String_values_for_BVR_Margin()
        {
            Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(260);
            mgr.CompanyName = "ABC Software";// aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.Email = "abcsoftware.com";
            mgr.Phone = "";// 12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            mgr.Telecopy = "";// 12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            mgr.AdrStreet = "";
            mgr.AdrStreetNb = "";
            mgr.AdrStreet2 = "";
            mgr.AdrCity = "";
            mgr.AdrZipCode = "";
            mgr.AdrState = "";
            mgr.AdrCountry = "";
            mgr.AdrPostalBox = "";
            mgr.VatNumber = "";
            mgr.WebSite = "";


            mgr.BankAccount1_AccountName = "";
            mgr.BankAccount1_BankName = "";
            mgr.BankAccount1_Currency = "";
            mgr.BankAccount1_Iban = "";
            mgr.BankAccount1_Swift = "";

            mgr.BankAccount2_AccountName = "";
            mgr.BankAccount2_BankName = "";
            mgr.BankAccount2_Currency = "";
            mgr.BankAccount2_Iban = "";
            mgr.BankAccount2_Swift = "";

            if (!String.IsNullOrWhiteSpace(""))
            {
                mgr.OwnerUserId = Convert.ToInt64("1");
            }

            /* try
             {
                 ImageUploadExtractor upload = new ImageUploadExtractor(fileImage);
                 upload.Mime.Add("image/png");
                 upload.Mime.Add("image/jpeg");
                 upload.Mime.Add("image/gif");
                 byte[] binImage = upload.Extract();
                 if (binImage != null)
                 {
                     mgr.Logo = binImage;
                 }
             }
             catch (Exception ex)
             {
                 Calani.BusinessObjects.Generic.WarningReport.Report("Try to upload invalid image format", ex);
             }*/


            mgr.BvrMode = Convert2.StrToInt("", 0);
            mgr.BvrType = Convert2.StrToInt("", 0);
            mgr.BvrTopMargin = Convert2.StrToInt("abc", 0);
            mgr.BvrLeftMargin = Convert2.StrToInt("abc", 0);
            mgr.BvrSenderBank = "";
            mgr.BvrAccount = "";
            mgr.BvrBankLine1 = "";
            mgr.BvrBankLine2 = "";


            var ret = mgr.Save(true, true, false, true);
            //  Assert.AreEqual(true, ret.Success);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Data.Entity.Validation.DbEntityValidationException))]

       public void Expecting_DbEntityValidationException_for_BVR_BANK_for_Saving_large_values_to_DB()
        {
            Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(260);
            mgr.CompanyName = "ABC Software";// aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.Email = "abcsoftware.com";
            mgr.Phone = "";// 12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            mgr.Telecopy = "";// 12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            mgr.AdrStreet = "";
            mgr.AdrStreetNb = "";
            mgr.AdrStreet2 = "";
            mgr.AdrCity = "";
            mgr.AdrZipCode = "";
            mgr.AdrState = "";
            mgr.AdrCountry = "";
            mgr.AdrPostalBox = "";
            mgr.VatNumber = "";
            mgr.WebSite = "";


            mgr.BankAccount1_AccountName = "";
            mgr.BankAccount1_BankName = "";
            mgr.BankAccount1_Currency = "";
            mgr.BankAccount1_Iban = "";
            mgr.BankAccount1_Swift = "";

            mgr.BankAccount2_AccountName = "";
            mgr.BankAccount2_BankName = "";
            mgr.BankAccount2_Currency = "";
            mgr.BankAccount2_Iban = "";
            mgr.BankAccount2_Swift = "";

            if (!String.IsNullOrWhiteSpace(""))
            {
                mgr.OwnerUserId = Convert.ToInt64("1");
            }

            /* try
             {
                 ImageUploadExtractor upload = new ImageUploadExtractor(fileImage);
                 upload.Mime.Add("image/png");
                 upload.Mime.Add("image/jpeg");
                 upload.Mime.Add("image/gif");
                 byte[] binImage = upload.Extract();
                 if (binImage != null)
                 {
                     mgr.Logo = binImage;
                 }
             }
             catch (Exception ex)
             {
                 Calani.BusinessObjects.Generic.WarningReport.Report("Try to upload invalid image format", ex);
             }*/


            mgr.BvrMode = Convert2.StrToInt("", 0);
            mgr.BvrType = Convert2.StrToInt("", 0);
            mgr.BvrTopMargin = Convert2.StrToInt("abc", 0);
            mgr.BvrLeftMargin = Convert2.StrToInt("abc", 0);
            mgr.BvrSenderBank = "";
            mgr.BvrAccount = "";
            mgr.BvrBankLine1 = "12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            mgr.BvrBankLine2 = "12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";


            var ret = mgr.Save(true, true, false, true);
            //  Assert.AreEqual(true, ret.Success);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Data.Entity.Validation.DbEntityValidationException))]

        public void Expecting_DbEntityValidationException_for_Bank_aacounts_name_for_saving_large_values_in_DB()
        {
            Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(260);
            mgr.CompanyName = "ABC Software";// aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss";
            mgr.Email = "abcsoftware.com";
            mgr.Phone = "";// 12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            mgr.Telecopy = "";// 12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

            mgr.AdrStreet = "";
            mgr.AdrStreetNb = "";
            mgr.AdrStreet2 = "";
            mgr.AdrCity = "";
            mgr.AdrZipCode = "";
            mgr.AdrState = "";
            mgr.AdrCountry = "";
            mgr.AdrPostalBox = "";
            mgr.VatNumber = "";
            mgr.WebSite = "";


            mgr.BankAccount1_AccountName = "12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            mgr.BankAccount1_BankName = "12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            mgr.BankAccount1_Currency = "";
            mgr.BankAccount1_Iban = "";
            mgr.BankAccount1_Swift = "";

            mgr.BankAccount2_AccountName = "12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            mgr.BankAccount2_BankName = "12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            mgr.BankAccount2_Currency = "";
            mgr.BankAccount2_Iban = "";
            mgr.BankAccount2_Swift = "";

            if (!String.IsNullOrWhiteSpace(""))
            {
                mgr.OwnerUserId = Convert.ToInt64("1");
            }

            /* try
             {
                 ImageUploadExtractor upload = new ImageUploadExtractor(fileImage);
                 upload.Mime.Add("image/png");
                 upload.Mime.Add("image/jpeg");
                 upload.Mime.Add("image/gif");
                 byte[] binImage = upload.Extract();
                 if (binImage != null)
                 {
                     mgr.Logo = binImage;
                 }
             }
             catch (Exception ex)
             {
                 Calani.BusinessObjects.Generic.WarningReport.Report("Try to upload invalid image format", ex);
             }*/


            mgr.BvrMode = Convert2.StrToInt("", 0);
            mgr.BvrType = Convert2.StrToInt("", 0);
            mgr.BvrTopMargin = Convert2.StrToInt("abc", 0);
            mgr.BvrLeftMargin = Convert2.StrToInt("abc", 0);
            mgr.BvrSenderBank = "";
            mgr.BvrAccount = "";
            mgr.BvrBankLine1 = "12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            mgr.BvrBankLine2 = "12sdwefr2efaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";


            var ret = mgr.Save(true, true, false, true);
            //  Assert.AreEqual(true, ret.Success);
        }
    }
}
