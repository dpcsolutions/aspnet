﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Calani.BusinessObjects.Generic;

namespace calani_business_obj_test
{
    /// <summary>
    /// Tools to convert data, mosty between db entities and html input
    /// </summary>
    public class Convert2
    {
        public static string ToString(double d)
        {
            return d.ToString(System.Globalization.CultureInfo.InvariantCulture);
        }

        public static string ToString(double? d)
        {
            if (d == null) return "";
            return ToString(d.Value);
        }

        public static double ToDouble(string str, double defaultValue, int maxDecimals, double? min = null, double? max = null)
        {
            double d = defaultValue;
            double t = 0;
            if (double.TryParse(str, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out t))
            {
                d = t;
            }
            d = Math.Round(d, maxDecimals, MidpointRounding.AwayFromZero);

            if (min != null && d < min.Value) d = min.Value;
            if (max != null && d > max.Value) d = max.Value;

            return d;
        }

        public static double? ToNullableDouble(string str, double? defaultValue = null, int maxDecimals = 0, double? min = null, double? max = null)
        {
            double? ret = defaultValue;
            if (!String.IsNullOrWhiteSpace(str))
            {
                double d = 0;
                if (defaultValue != null) d = defaultValue.Value;
                double t = 0;
                if (double.TryParse(str, System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.InvariantCulture, out t))
                {
                    d = t;

                    d = Math.Round(d, maxDecimals, MidpointRounding.AwayFromZero);

                    if (min != null && d < min.Value) d = min.Value;
                    if (max != null && d > max.Value) d = max.Value;

                    ret = d;
                }
            }
            return ret;
        }

        public static string DateToStr(DateTime value)
        {
            return value.ToShortDateString() + " " + value.ToLongTimeString();
        }

        public static string DateToShortStr(DateTime? value)
        {
            if (value == null) return "";
            return value.Value.ToShortDateString();
        }

        public static string ToString(long? l)
        {
            if (l != null) return l.ToString();
            return null;
        }

        public static long? ToNullableLong(string s)
        {
            long? ret = null;
            if (!String.IsNullOrWhiteSpace(s))
            {
                long l = 0;
                if (long.TryParse(s, out l)) ret = l;
            }
            return ret;
        }


        public static DateTime? ToDate(string s, string type)
        {
            DateTime? ret = null;
            if (type == "input")
            {
                if (!String.IsNullOrWhiteSpace(s))
                {
                    ret = Convert.ToDateTime(s);
                }
            }
            else if (type == "picker")
            {

                if (!String.IsNullOrWhiteSpace(s))
                {
                    ret = DateTime.ParseExact(s, Tools.DefaultDateFormat, System.Globalization.CultureInfo.InvariantCulture);
                }
            }
            return ret;
        }

        public static DateTime? ToDate(string s, string format, DateTime? defaultValue = null)
        {
            DateTime? ret = defaultValue;
            if (!String.IsNullOrWhiteSpace(s))
            {
                if (format.Contains("/")) s = s.Replace(".", "/");
                if (format.Contains(".")) s = s.Replace("/", ".");


                DateTime t = new DateTime();
                if (DateTime.TryParseExact(s, format, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.NoCurrentDateDefault, out t))
                {
                    ret = t;
                }
            }
            return ret;
        }

        public static string DateToStrForInput(DateTime? dateQuoteValidity, string type, int? defaultYear = null)
        {
            if (defaultYear != null)
            {
                if (dateQuoteValidity == null)
                {
                    dateQuoteValidity = new DateTime(defaultYear.Value, 1, 1);
                }
                else if (defaultYear != null && dateQuoteValidity.Value.Year < 1950)
                {
                    dateQuoteValidity = new DateTime(defaultYear.Value, 1, 1);
                }
            }


            string ret = "";
            if (type == "input")
            {
                if (dateQuoteValidity != null)
                    ret = dateQuoteValidity.Value.ToString("yyyy-MM-dd");
            }
            else if (type == "picker")
            {
                if (dateQuoteValidity != null)
                    ret = dateQuoteValidity.Value.ToString("dd/MM/yyyy");
            }
            return ret;
        }

        public static int StrToInt(string value, int v)
        {
            int x = v;
            if (value != null)
            {
                value = value.Replace(" ", "");
                if (!String.IsNullOrWhiteSpace(value))
                {
                    if (int.TryParse(value, out x))
                    {
                        return x;
                    }
                }
            }
            return v;
        }

        public static long StrToLong(string value, long v)
        {
            long x = v;
            if (value != null)
            {
                value = value.Replace(" ", "");
                if (!String.IsNullOrWhiteSpace(value))
                {
                    if (long.TryParse(value, out x))
                    {
                        return x;
                    }
                }
            }
            return v;
        }

    }
}