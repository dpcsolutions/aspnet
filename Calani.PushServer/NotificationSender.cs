﻿using System;
using System.Collections.Generic;
using log4net;
using System.Reflection;

namespace PushServer
{
    public class NotificationSender
    {
        public class notificationToken
        {
            public string token = "";
            public bool isIOS = false;
        }

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

      
        FirebaseNotificationSender _broker = null;
        List<string> DevicesToken = new List<string>();

        private void printToDebug(string str, Exception ex = null)
        {
            if (ex != null)
                _log.Error(str, ex);
            else
                _log.Debug(str);
            Console.WriteLine(str);
        }

        public NotificationSender()
        {
            //configure AAPL notifications

            _broker = new FirebaseNotificationSender();

            // Wire up events
            _broker.OnNotificationFailed += (token, message) => {
                printToDebug("Notification failed! " + message);
            };

            _broker.OnNotificationSucceeded += (token, message) => {
                printToDebug("Notification Sent!");
            };
        }

        ~NotificationSender()
        {
        }

        public void sendNotificationToToken(string deviceToken, string title, string message, int badge, Dictionary<string, string> data,
            string category = null, bool isIOS=false)
        {
            try
            {
                if(category != null)
                {
                    if (data == null)
                        data = new Dictionary<string, string>();

                    if(!data.ContainsKey("category"))
                        data.Add("category", category);
                }
                _log.Debug("before sending notif");
                _broker.sendNotificationAsync(deviceToken, message, title, data, isIOS );
            }
            catch
            {
                _log.Error("Exception while trying to send the notification for token: " + (!String.IsNullOrEmpty(deviceToken) ? deviceToken : "null"));
            }
        }

        public void sendNotificationToTokens(List<notificationToken> devicesToken, string title, string message, int badge, Dictionary<string, string> data, string category)
        {
            _log.Debug($"APSN sending {title}-{message} to {devicesToken.Count} devices");
            // Queue a notification to send
            foreach (var deviceToken in devicesToken)
                // if(deviceToken.Length < 100) //ignore tokens comming from browsers
                sendNotificationToToken(deviceToken.token, title, message, badge, data, category, deviceToken.isIOS);
        }
    }
}
