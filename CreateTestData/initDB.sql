﻿-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema calani
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema calani
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `calani` DEFAULT CHARACTER SET utf8 ;
USE `calani` ;

-- -----------------------------------------------------
-- Table `calani`.`clients`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`clients` ;

CREATE TABLE IF NOT EXISTS `calani`.`clients` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `companyName` VARCHAR(45) NULL,
  `organizationId` BIGINT NULL,
  `recordStatus` INT NOT NULL DEFAULT 0,
  `vatNumber` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_clients_organizations`
    FOREIGN KEY (`organizationId`)
    REFERENCES `calani`.`organizations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_clients_organizations_idx` ON `calani`.`clients` (`organizationId` ASC);


-- -----------------------------------------------------
-- Table `calani`.`contacts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`contacts` ;

CREATE TABLE IF NOT EXISTS `calani`.`contacts` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(45) NULL,
  `lastName` VARCHAR(45) NULL,
  `dateOfBirth` DATETIME NULL,
  `primaryPhone` VARCHAR(45) NULL,
  `secondaryPhone` VARCHAR(45) NULL,
  `primaryEmail` VARCHAR(100) NULL,
  `secondaryEmail` VARCHAR(100) NULL,
  `telecopy` VARCHAR(45) NULL,
  `clientId` BIGINT NULL,
  `organizationId` BIGINT NULL,
  `type` INT NULL,
  `password` VARCHAR(45) NULL,
  `recoverCode` VARCHAR(10) NULL,
  `status` INT NULL,
  `role` INT NULL,
  `title` VARCHAR(45) NULL,
  `civtitle` VARCHAR(45) NULL,
  `initials` VARCHAR(5) NULL,
  `account` VARCHAR(10) NULL,
  `weeklyHours` DOUBLE NULL,
  `lang` VARCHAR(10) NULL,
  `stafftoken` VARCHAR(45) NULL,
  `recordStatus` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_contacts_clients`
    FOREIGN KEY (`clientId`)
    REFERENCES `calani`.`clients` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contacts_organizations`
    FOREIGN KEY (`organizationId`)
    REFERENCES `calani`.`organizations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_contacts_clients_idx` ON `calani`.`contacts` (`clientId` ASC);

CREATE INDEX `fk_contacts_organizations_idx` ON `calani`.`contacts` (`organizationId` ASC);


-- -----------------------------------------------------
-- Table `calani`.`organizations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`organizations` ;

CREATE TABLE IF NOT EXISTS `calani`.`organizations` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `owner` BIGINT NULL,
  `email` VARCHAR(100) NULL,
  `phone` VARCHAR(45) NULL,
  `telecopy` VARCHAR(45) NULL,
  `userLimit` INT NULL,
  `dateLimit` DATETIME NULL,
  `logo` LONGBLOB NULL,
  `creationDate` DATETIME NULL,
  `activationCode` VARCHAR(10) NULL,
  `activationDate` DATETIME NULL,
  `delayQuote` INT NULL,
  `delayInvoice` INT NULL,
  `financialYearStart` DATETIME NULL,
  `currency` VARCHAR(10) NULL,
  `website` VARCHAR(80) NULL,
  `vatnumber` VARCHAR(50) NULL,
  `trialEnd` DATETIME NULL,
  `subscriptionStatus` VARCHAR(45) NULL,
  `subscriptionEnd` DATETIME NULL,
  `organizationscol` VARCHAR(45) NULL,
  `subscriptionId` VARCHAR(80) NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_organizationa_contacts`
    FOREIGN KEY (`owner`)
    REFERENCES `calani`.`contacts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `owner_UNIQUE` ON `calani`.`organizations` (`owner` ASC);


-- -----------------------------------------------------
-- Table `calani`.`taxes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`taxes` ;

CREATE TABLE IF NOT EXISTS `calani`.`taxes` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `rate` DOUBLE NOT NULL DEFAULT 0,
  `organizationId` BIGINT NULL,
  `isDefault` BIT NULL,
  `lastChange` DATETIME NULL,
  `recordStatus` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_taxes_organizations`
    FOREIGN KEY (`organizationId`)
    REFERENCES `calani`.`organizations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_taxes_organizations_idx` ON `calani`.`taxes` (`organizationId` ASC);


-- -----------------------------------------------------
-- Table `calani`.`services`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`services` ;

CREATE TABLE IF NOT EXISTS `calani`.`services` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `internalId` VARCHAR(45) NULL,
  `unitPrice` DOUBLE NOT NULL DEFAULT 0,
  `taxId` BIGINT NULL,
  `description` TINYTEXT NULL,
  `organizationId` BIGINT NULL,
  `type` INT NULL,
  `recordStatus` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_services_taxes`
    FOREIGN KEY (`taxId`)
    REFERENCES `calani`.`taxes` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_services_organizations`
    FOREIGN KEY (`organizationId`)
    REFERENCES `calani`.`organizations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_services_taxes_idx` ON `calani`.`services` (`taxId` ASC);

CREATE INDEX `fk_services_organizations_idx` ON `calani`.`services` (`organizationId` ASC);


-- -----------------------------------------------------
-- Table `calani`.`addresses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`addresses` ;

CREATE TABLE IF NOT EXISTS `calani`.`addresses` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `street` VARCHAR(45) NULL,
  `street2` VARCHAR(45) NULL,
  `nb` VARCHAR(5) NULL,
  `npa` VARCHAR(10) NULL,
  `city` VARCHAR(45) NULL,
  `postbox` VARCHAR(45) NULL,
  `state` VARCHAR(45) NULL,
  `country` VARCHAR(45) NULL,
  `clientId` BIGINT NULL,
  `organizationId` BIGINT NULL,
  `type` INT NULL,
  `recordStatus` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_addresses_clients`
    FOREIGN KEY (`clientId`)
    REFERENCES `calani`.`clients` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_addresses_organization`
    FOREIGN KEY (`organizationId`)
    REFERENCES `calani`.`organizations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_addresses_clients_idx` ON `calani`.`addresses` (`clientId` ASC);

CREATE INDEX `fk_addresses_organization_idx` ON `calani`.`addresses` (`organizationId` ASC);


-- -----------------------------------------------------
-- Table `calani`.`jobs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`jobs` ;

CREATE TABLE IF NOT EXISTS `calani`.`jobs` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `internalId` VARCHAR(45) NULL,
  `clientId` BIGINT NOT NULL,
  `clientContact` BIGINT NULL,
  `clientAddress` BIGINT NULL,
  `description` TEXT NULL,
  `rating` INT NULL,
  `discount` DOUBLE NULL,
  `total` DOUBLE NULL,
  `street` VARCHAR(45) NULL,
  `street2` VARCHAR(45) NULL,
  `nb` VARCHAR(10) NULL,
  `npa` INT NULL,
  `city` VARCHAR(45) NULL,
  `country` VARCHAR(45) NULL,
  `sentToName` VARCHAR(45) NULL,
  `sentToEmail` VARCHAR(45) NULL,
  `internalNote` VARCHAR(255) NULL,
  `dateCreated` DATETIME NULL,
  `dateQuoteSent` DATETIME NULL,
  `dateQuoteAccepted` DATETIME NULL,
  `dateQuoteRefused` DATETIME NULL,
  `dateQuoteValidity` DATETIME NULL,
  `dateOpenJob` DATETIME NULL,
  `dateJobHeld` DATETIME NULL,
  `dateJobDone` DATETIME NULL,
  `dateJobValidity` DATETIME NULL,
  `dateInvoicing` DATETIME NULL,
  `dateFullyPaid` DATETIME NULL,
  `dateInvoiceDue` DATETIME NULL,
  `type` INT NULL,
  `parentId` BIGINT NULL,
  `invoiceDue` DOUBLE NULL,
  `contractualUnitsCount` INT NULL,
  `organizationId` BIGINT NULL,
  `lastUpdate` DATETIME NULL,
  `recordStatus` INT NOT NULL DEFAULT 0,
  `currency` VARCHAR(6) NULL,
  `currencyRate` DOUBLE NULL,
  `totalInCurrency` DOUBLE NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_jobs_clients`
    FOREIGN KEY (`clientId`)
    REFERENCES `calani`.`clients` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_jobs_jobs`
    FOREIGN KEY (`parentId`)
    REFERENCES `calani`.`jobs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_jobs_organizations`
    FOREIGN KEY (`organizationId`)
    REFERENCES `calani`.`organizations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_jobs_clientcontact`
    FOREIGN KEY (`clientContact`)
    REFERENCES `calani`.`contacts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_jobs_clientaddress`
    FOREIGN KEY (`clientAddress`)
    REFERENCES `calani`.`addresses` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_jobs_clients_idx` ON `calani`.`jobs` (`clientId` ASC);

CREATE INDEX `fk_jobs_jobs` ON `calani`.`jobs` (`parentId` ASC);

CREATE INDEX `fk_jobs_organizations_idx` ON `calani`.`jobs` (`organizationId` ASC);

CREATE INDEX `fk_jobs_clientcontact_idx` ON `calani`.`jobs` (`clientContact` ASC);

CREATE INDEX `fk_jobs_clientaddress_idx` ON `calani`.`jobs` (`clientAddress` ASC);


-- -----------------------------------------------------
-- Table `calani`.`attachments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`attachments` ;

CREATE TABLE IF NOT EXISTS `calani`.`attachments` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(45) NULL,
  `file` VARCHAR(255) NULL,
  `name` VARCHAR(255) NULL,
  `type` INT NULL,
  `date` DATETIME NULL,
  `mimeType` VARCHAR(255) NULL,
  `organizationId` BIGINT NULL,
  `clientId` BIGINT NULL,
  `jobId` BIGINT NULL,
  `recordStatus` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_attachments_clients`
    FOREIGN KEY (`clientId`)
    REFERENCES `calani`.`clients` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_attachments_organizations`
    FOREIGN KEY (`organizationId`)
    REFERENCES `calani`.`organizations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_attachments_jobs`
    FOREIGN KEY (`jobId`)
    REFERENCES `calani`.`jobs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_attachments_jobs_idx` ON `calani`.`attachments` (`clientId` ASC);

CREATE INDEX `fk_attachments_organizations_idx` ON `calani`.`attachments` (`organizationId` ASC);

CREATE INDEX `fk_attachments_jobs_idx1` ON `calani`.`attachments` (`jobId` ASC);


-- -----------------------------------------------------
-- Table `calani`.`services_jobs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`services_jobs` ;

CREATE TABLE IF NOT EXISTS `calani`.`services_jobs` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `jobId` BIGINT NOT NULL,
  `taxName` VARCHAR(200) NULL,
  `taxRate` DOUBLE NULL,
  `serviceQuantity` DOUBLE NULL,
  `serviceName` VARCHAR(200) NULL,
  `servicePrice` DOUBLE NULL,
  `serviceId` BIGINT NULL,
  `discount` DOUBLE NULL,
  `totalPriceExclTax` DOUBLE NULL,
  `taxAmount` DOUBLE NULL,
  `totalPrinceIncTax` DOUBLE NULL,
  `type` INT NULL DEFAULT 1,
  `toinvoice` BIGINT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_services_jobs_jobs`
    FOREIGN KEY (`jobId`)
    REFERENCES `calani`.`jobs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_services_jobs_services`
    FOREIGN KEY (`serviceId`)
    REFERENCES `calani`.`services` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_services_jobs_jobs_idx` ON `calani`.`services_jobs` (`jobId` ASC);

CREATE INDEX `fk_services_jobs_services_idx` ON `calani`.`services_jobs` (`serviceId` ASC);


-- -----------------------------------------------------
-- Table `calani`.`notes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`notes` ;

CREATE TABLE IF NOT EXISTS `calani`.`notes` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `jobId` BIGINT NULL,
  `message` TEXT NULL,
  `author` BIGINT NULL,
  `date` DATETIME NULL,
  `recordStatus` INT NOT NULL DEFAULT 0,
  `lastModificationDate` DATETIME NULL,
  `lastModifiedById` BIGINT NULL,
  `mobileTempId` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_notes_jobs`
    FOREIGN KEY (`jobId`)
    REFERENCES `calani`.`jobs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_notes_author`
    FOREIGN KEY (`author`)
    REFERENCES `calani`.`contacts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_notes_jobs_idx` ON `calani`.`notes` (`jobId` ASC);

CREATE INDEX `fk_notes_author_idx` ON `calani`.`notes` (`author` ASC);


-- -----------------------------------------------------
-- Table `calani`.`payments`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`payments` ;

CREATE TABLE IF NOT EXISTS `calani`.`payments` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `datePayed` DATETIME NULL,
  `amountPayed` DOUBLE NULL,
  `jobId` BIGINT NULL,
  `comment` VARCHAR(255) NULL,
  `recordStatus` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_payments_jobs`
    FOREIGN KEY (`jobId`)
    REFERENCES `calani`.`jobs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_payments_jobs_idx` ON `calani`.`payments` (`jobId` ASC);


-- -----------------------------------------------------
-- Table `calani`.`visits`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`visits` ;

CREATE TABLE IF NOT EXISTS `calani`.`visits` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `jobId` BIGINT NULL,
  `dateStart` DATETIME NULL,
  `dateEnd` DATETIME NULL,
  `visitStatus` INT NULL,
  `recordStatus` INT NOT NULL DEFAULT 0,
  `type` INT NULL,
  `description` VARCHAR(1000) NULL,
  `orgId` BIGINT NULL,
  `repeatGroupId` VARCHAR(45) NULL,
  `numrepeat` INT NULL,
  `repeatdays` VARCHAR(20) NULL,
  `serviceId` BIGINT NULL,
  `lastModificationDate` DATETIME NULL,
  `lastModifiedById` BIGINT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_visits_jobs`
    FOREIGN KEY (`jobId`)
    REFERENCES `calani`.`jobs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_visits_org`
    FOREIGN KEY (`orgId`)
    REFERENCES `calani`.`organizations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_visit_services`
    FOREIGN KEY (`serviceId`)
    REFERENCES `calani`.`services` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_visits_jobs_idx` ON `calani`.`visits` (`jobId` ASC);

CREATE INDEX `fk_visits_org_idx` ON `calani`.`visits` (`orgId` ASC);

CREATE INDEX `idx_recurring_group` ON `calani`.`visits` (`repeatGroupId` ASC);

CREATE INDEX `fk_visit_services_idx` ON `calani`.`visits` (`serviceId` ASC);


-- -----------------------------------------------------
-- Table `calani`.`visits_contacts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`visits_contacts` ;

CREATE TABLE IF NOT EXISTS `calani`.`visits_contacts` (
  `visitId` BIGINT NOT NULL,
  `contactId` BIGINT NOT NULL,
  PRIMARY KEY (`visitId`, `contactId`),
  CONSTRAINT `fk_visits_contacts_contacts`
    FOREIGN KEY (`contactId`)
    REFERENCES `calani`.`contacts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_visits_contacts_visits`
    FOREIGN KEY (`visitId`)
    REFERENCES `calani`.`visits` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_visits_contacts_visits_idx` ON `calani`.`visits_contacts` (`visitId` ASC);

CREATE INDEX `fk_visits_contacts_contatcs_idx` ON `calani`.`visits_contacts` (`contactId` ASC);


-- -----------------------------------------------------
-- Table `calani`.`timesheets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`timesheets` ;

CREATE TABLE IF NOT EXISTS `calani`.`timesheets` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `employeeId` BIGINT NULL,
  `year` INT NULL,
  `week` INT NULL,
  `sentDate` DATETIME NULL,
  `acceptedDate` DATETIME NULL,
  `rejectDate` DATETIME NULL,
  `comment` LONGTEXT NULL,
  `organizationId` BIGINT NULL,
  `recordStatus` INT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_timesheets_contacts`
    FOREIGN KEY (`employeeId`)
    REFERENCES `calani`.`contacts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_timesheets_organizations`
    FOREIGN KEY (`organizationId`)
    REFERENCES `calani`.`organizations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_timesheets_contacts_idx` ON `calani`.`timesheets` (`employeeId` ASC);

CREATE INDEX `fk_timesheets_organizations_idx` ON `calani`.`timesheets` (`organizationId` ASC);


-- -----------------------------------------------------
-- Table `calani`.`timesheetRecords`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`timesheetRecords` ;

CREATE TABLE IF NOT EXISTS `calani`.`timesheetRecords` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `timesheetId` BIGINT NULL,
  `visitId` BIGINT NULL,
  `serviceId` BIGINT NULL,
  `serviceName` VARCHAR(45) NULL,
  `servicePrice` DOUBLE NULL,
  `startDate` DATETIME NULL,
  `endDate` DATETIME NULL,
  `rate` DOUBLE NULL,
  `duration` DOUBLE NULL,
  `description` VARCHAR(255) NULL,
  `recordStatus` INT NOT NULL DEFAULT 0,
  `lastModificationDate` DATETIME NULL,
  `lastModifiedBy` BIGINT NULL,
  `jobId` BIGINT NULL,
  `serviceType` INT NULL,
  `imported` BIT NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_timesheetRecords_timesheets`
    FOREIGN KEY (`timesheetId`)
    REFERENCES `calani`.`timesheets` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_timesheetRecords_lastModifiedBy`
    FOREIGN KEY (`lastModifiedBy`)
    REFERENCES `calani`.`contacts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_timesheetRecords_job`
    FOREIGN KEY (`jobId`)
    REFERENCES `calani`.`jobs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_timesheetRecords_service`
    FOREIGN KEY (`serviceId`)
    REFERENCES `calani`.`services` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_timesheetRecords_visit`
    FOREIGN KEY (`visitId`)
    REFERENCES `calani`.`visits` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_timesheetRecords_timesheets_idx` ON `calani`.`timesheetRecords` (`timesheetId` ASC);

CREATE INDEX `fk_lastModifiedBy_idx` ON `calani`.`timesheetRecords` (`lastModifiedBy` ASC);

CREATE INDEX `fk_jobItem_jobId_idx` ON `calani`.`timesheetRecords` (`jobId` ASC);

CREATE INDEX `fk_timesheetRecords_service_idx` ON `calani`.`timesheetRecords` (`serviceId` ASC);

CREATE INDEX `fk_timesheetRecords_visit_idx` ON `calani`.`timesheetRecords` (`visitId` ASC);


-- -----------------------------------------------------
-- Table `calani`.`expenses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`expenses` ;

CREATE TABLE IF NOT EXISTS `calani`.`expenses` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `amount` DOUBLE NULL,
  `attachmentId` BIGINT NULL,
  `timesheetId` BIGINT NULL,
  `date` DATETIME NULL,
  `approvedmount` DOUBLE NULL,
  `recordStatus` INT NOT NULL DEFAULT 0,
  `lastModifiedById` BIGINT NULL,
  `lastModificationDate` DATETIME NULL,
  `picture` LONGTEXT NULL DEFAULT NULL,
  `description` VARCHAR(255) NULL DEFAULT NULL,
  `employeeId` BIGINT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_expenses_timesheets`
    FOREIGN KEY (`timesheetId`)
    REFERENCES `calani`.`timesheets` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_expenses_contact`
    FOREIGN KEY (`employeeId`)
    REFERENCES `calani`.`contacts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_expenses_timesheets_idx` ON `calani`.`expenses` (`timesheetId` ASC);

CREATE INDEX `fk_expenses_contact_idx` ON `calani`.`expenses` (`employeeId` ASC);


-- -----------------------------------------------------
-- Table `calani`.`documentemplates`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`documentemplates` ;

CREATE TABLE IF NOT EXISTS `calani`.`documentemplates` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `userId` BIGINT NULL,
  `type` INT NULL,
  `name` VARCHAR(45) NULL,
  `title` LONGTEXT NULL,
  `content` LONGTEXT NULL,
  `recordStatus` INT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_documenttemplates_contacts`
    FOREIGN KEY (`userId`)
    REFERENCES `calani`.`contacts` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_documenttemplates_contacts_idx` ON `calani`.`documentemplates` (`userId` ASC);


-- -----------------------------------------------------
-- Table `calani`.`uservariables`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`uservariables` ;

CREATE TABLE IF NOT EXISTS `calani`.`uservariables` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `user` BIGINT NULL,
  `name` VARCHAR(200) NULL,
  `value` VARCHAR(200) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `calani`.`bankaccount`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `calani`.`bankaccount` ;

CREATE TABLE IF NOT EXISTS `calani`.`bankaccount` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `organizationId` BIGINT NULL,
  `priority` INT NULL,
  `currency` VARCHAR(10) NULL,
  `bankname` VARCHAR(50) NULL,
  `accountname` VARCHAR(50) NULL,
  `iban` VARCHAR(50) NULL,
  `swift` VARCHAR(50) NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_bankaccount_organization`
    FOREIGN KEY (`organizationId`)
    REFERENCES `calani`.`organizations` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_bankaccount_organization_idx` ON `calani`.`bankaccount` (`organizationId` ASC);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
