﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CreateTestData
{
    public class initialiseDB
    {
        public static void init(MySqlConnection c)
        {
            string s = System.IO.File.ReadAllText(Path.Combine(Assembly.GetExecutingAssembly().Location.Replace(Assembly.GetExecutingAssembly().GetName().Name + ".exe", "initDB.sql")));
            MySqlScript script = new MySqlScript(c, s);
            script.Execute();

            s = System.IO.File.ReadAllText(Path.Combine(Assembly.GetExecutingAssembly().Location.Replace(Assembly.GetExecutingAssembly().GetName().Name + ".exe","initialiseSP.sql")));
            script = new MySqlScript(c, s);
            script.Execute();
        }
    }
}
