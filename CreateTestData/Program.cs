﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CreateTestData
{
    class Program
    {
        public static string getDataPath(string path)
        {
            var dataPath = Path.Combine(Assembly.GetExecutingAssembly().Location.Replace(Assembly.GetExecutingAssembly().GetName().Name + ".exe", ""), "data");
            return dataPath + "/" + path;
        }

        public static string formatDate(DateTime dt)
        {
            return String.Format("{0:yyyy-MM-dd HH:mm:ss}", dt);
        }

        public static Tuple<string,string> getRandomDate(DateTime dt, int daysBefore)
        {
            dt = dt.AddDays(getRandVal(daysBefore));
            return new Tuple<string, string>( String.Format("{0:yyyy-MM-dd HH:mm:ss}", dt), String.Format("{0:yyyy-MM-dd HH:mm:ss}", dt.AddHours(getRandVal(4))));
        }

        public static int getRandVal(int max = 30)
        {
            int randval = (new Random(DateTime.Now.Millisecond)).Next();

            randval = int.Parse(randval.ToString().Substring(randval.ToString().Length - 2));
            if (randval < 1)
                randval = 1;
            else while (randval > max && randval > 0)
                randval = randval / 2;

            return randval;
        }

        public static void sendDraft(dataGenerator d, List<string> employees, List<List<string>> servicesToJob)
        {
            var draftId = d.CallStoredProcedure("sendDraft", new List<string>() { "test@mail.com", "test", formatDate(DateTime.Now) });
            var jobId = d.CallStoredProcedure("acceptDraft", new List<string>() { formatDate(DateTime.Now) });

            //add services to job
            foreach (var arg in servicesToJob)
            {
                arg[0] = jobId.ToString();
                d.CallStoredProcedure("addServiceTojob", arg);
            }

            //creating meetins for job
            for (var h = 0; h < getRandVal(200); h++)
            {
                var dt = getRandomDate(DateTime.Now, -getRandVal(20));
                var employeeId = getRandVal(employees.Count - 1);
                var rand = DateTime.Now.Ticks.ToString();

                rand = rand.Substring(rand.Length - 1);

                if (Int32.Parse(rand) <= 3)
                    rand = "1";
                else if (Int32.Parse(rand) < 5)
                    rand = "2";
                else if (Int32.Parse(rand) < 7)
                    rand = "3";
                else 
                    rand = "4";

                d.createVisit(dt.Item1, dt.Item2, rand, jobId.ToString(), employees[employeeId]);
            }
        }

        public static void jobDone(dataGenerator d, List<List<string>> servicesToJob)
        {
            var jobId = d.CallStoredProcedure("jobDone", new List<string>() { formatDate(DateTime.Now) });

            //add services to job
            foreach (var arg in servicesToJob)
            {
                arg[0] = jobId.ToString();
                d.CallStoredProcedure("addServiceTojob", arg);
            }
        }

        static void Main(string[] args)
        {


              //  eng.executeSPOnFile("createOrganisation", dataPath + "/orgs.csv");

              dataGenerator d = new dataGenerator();

            engine engOrg = new engine(d, new fileLoader(getDataPath("orgs.csv")));
            engine engAdrs = new engine(d, new fileLoader(getDataPath("orgAddress.csv")));
            engine engClientAdrs = new engine(d, new fileLoader(getDataPath("clientAddress.csv")));
            engine engOwner = new engine(d, new fileLoader(getDataPath("owner.csv")));
            engine engEmployee = new engine(d, new fileLoader(getDataPath("employee.csv")));
            engine engClient = new engine(d, new fileLoader(getDataPath("clients.csv")));
            engine engServices = new engine(d, new fileLoader(getDataPath("services.csv")));

            fileLoader f = new fileLoader(getDataPath("clientAddress.csv"));

            int maxOrgCounter = 0;
            int MAX_ORG = 3;

            d.Initialize();

            if (d.OpenConnection())
            {
                initialiseDB.init(d.connection);

                bool allOrgdone = false;
                int orgId = -1;

                do
                {
                    try
                    {
                        //for each organisation
                        //create the org
                        orgId = engOrg.executeSPOnNextLine("createOrganisation");

                        if (orgId == -1)
                            allOrgdone = true;

                        //pick up an address
                        engAdrs.executeSPOnNextLine("createOrgAddress");

                        //adds an owner
                        engOwner.executeSPOnNextLine("createOwner");

                        //adds standard owner
                        d.createOwner("Sandra","Connors", "sco@gesmobile.ch", "", "Directeur", "PHA");
                        
                        for (var j = 0; j < getRandVal(50); j++)
                            engServices.executeSPOnNextLine("createService");

                        for (var i = 0; i < getRandVal(40); i++)
                        {
                            //add client
                            engClient.executeSPOnNextLine("createClient");

                            for (var j = 0; j < getRandVal(3); j++)
                                engEmployee.executeSPOnNextLine("createClientContact");

                            d.CallStoredProcedure("setContactAsMainContact");
                            
                            //add some addresses for the client
                            for (var j = 0; j < getRandVal(4); j++)
                                engClientAdrs.executeSPOnNextLine("createClientAddress");

                            //adds employee
                            List<string> employees = new List<string>();

                            for (var j = 0; j < getRandVal(30); j++)
                                employees.Add(engEmployee.executeSPOnNextLine("createEmployee").ToString());

                            List<string> projects = new List<string>();

                            //adds projects
                            for (var j = 0; j < getRandVal(20); j++)
                            {
                                List<string> args1 = new List<string>();
                                args1.Add("Proj" + j);
                                args1.Add("proj test numéro " + j);
                                args1.Add(getRandVal(5).ToString());
                                args1.Add("0");
                                args1.Add(getRandVal(10000).ToString());

                                var adrs = f.getItems();

                                args1.AddRange(adrs);
                                args1.Add("");

                                var draftId = d.CallStoredProcedure("createDraft", args1);

                                projects.Add(draftId.ToString());

                                var services = d.getServicesForOrganisation(orgId);
                                List<List<string>> servicesToJob = new List<List<string>>();

                                for (var g = 0; g < getRandVal(10); g++)
                                {
                                    args1 = new List<string>();
                                    var row = getRandVal(services.Rows.Count-1);
                                    var price = Convert.ToDouble(services.Rows[row]["unitPrice"]);
                                    var quantity = getRandVal(10);

                                    args1.Add(draftId.ToString());
                                    args1.Add(services.Rows[row]["name"].ToString());
                                    args1.Add((price).ToString(System.Globalization.CultureInfo.InvariantCulture));
                                    args1.Add(services.Rows[row]["id"].ToString());
                                    args1.Add((price*quantity).ToString(System.Globalization.CultureInfo.InvariantCulture));
                                    args1.Add((price * quantity * 0.077).ToString(System.Globalization.CultureInfo.InvariantCulture));
                                    args1.Add((price * quantity * 1.077).ToString(System.Globalization.CultureInfo.InvariantCulture));
                                    args1.Add((quantity).ToString(System.Globalization.CultureInfo.InvariantCulture));

                                    d.CallStoredProcedure("addServiceTojob", args1);
                                    servicesToJob.Add(args1);
                                }

                                var r = getRandVal(9);

                                if(r <= 3)
                                {
                                    sendDraft(d, employees, servicesToJob);
                                }
                                else if(r >= 3 && r < 7)
                                {
                                    sendDraft(d, employees, servicesToJob);
                                    jobDone(d, servicesToJob);
                                }
                                else if(r >= 7 && r <= 8)
                                {
                                    sendDraft(d, employees, servicesToJob);
                                    jobDone(d, servicesToJob);
                                    d.payInvoiceFully(formatDate(DateTime.Now));
                                }
                            }
                        }

                        //links the owner and fills up internal services
                        d.CallStoredProcedure("fillOrganisation");
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error skipping line. Message: " + e.Message);
                    }

                    maxOrgCounter += 1;
                } while (!allOrgdone && (maxOrgCounter < MAX_ORG || MAX_ORG == -1)); 

                d.CloseConnection();
            }
            else
                Console.WriteLine("Could not open data connection ");

            Console.WriteLine("finished, press any key to exit");
            Console.ReadKey();
        }
    }
}
