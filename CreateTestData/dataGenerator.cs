﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.IO;
using static System.Net.Mime.MediaTypeNames;
using System.Reflection;
using System.Data;

namespace CreateTestData
{

    public class dataGenerator
    {
        public MySqlConnection connection;
     
        public void Initialize()
        {
            var connectionString = System.Configuration.ConfigurationManager.
                            ConnectionStrings["calaniConnectionString"].ConnectionString;
            connection = new MySqlConnection(connectionString);
        }

        public int getLastInsert()
        {
            try
            {
                DataTable data = new DataTable();
                MySqlCommand cmd = new MySqlCommand("select LAST_INSERT_ID()", connection);

                data.Load(cmd.ExecuteReader());

                object ret = null;

                if (data.Rows.Count > 0)
                    ret = data.Rows[0][0];
                else
                    ret = -1;

                return Convert.ToInt32(ret);
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR: " + e.Message);
                throw;
            }
        }

        public int executeQuery(string query)
        {
            //create command and assign the query and connection from the constructor
            MySqlCommand cmd = new MySqlCommand(query, connection);

            //Execute command
            Console.WriteLine(query);
            try
            {
               DataTable data = new DataTable();

                cmd = new MySqlCommand(query, connection);
                data.Load(cmd.ExecuteReader());
                object ret = null;

                if (data.Rows.Count > 0)
                    ret = data.Rows[0][0];
                else
                    ret = -1;

                return Convert.ToInt32(ret);
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR: " + e.Message);
                throw;
            }
        }

        public int CallStoredProcedure(string name, List<string> args = null)
        {
            string query = "call " + name + "(";
            if (args != null)
            {
                foreach (var a in args)
                    query += "'" + a.Replace("'","") + "',";

                if (query.LastIndexOf(",") > 0)
                    query = query.Substring(0, query.LastIndexOf(","));
            }
            query += ");";
       
            //create command and assign the query and connection from the constructor
            MySqlCommand cmd = new MySqlCommand(query, connection);

            //Execute command
            Console.WriteLine(query);
            try
            {
                cmd.ExecuteNonQuery();

                DataTable data = new DataTable();

                cmd = new MySqlCommand("select LAST_INSERT_ID()", connection);
                data.Load(cmd.ExecuteReader());
                object ret = null;

                if (data.Rows.Count > 0)
                    ret = data.Rows[0][0];
                else
                    ret = -1;

                return Convert.ToInt32(ret);
            }
            catch (Exception e)
            {
                Console.WriteLine("ERROR: " + e.Message + " on proc: " + name);
                throw;
             }
        }

        public DataTable getServicesForOrganisation(int org)
        {
            List<int> ids = new List<int>();

            MySqlCommand cmd = new MySqlCommand("select name, id, unitPrice from services where organizationId = " + org, connection); 
            System.Data.DataTable set = new System.Data.DataTable();
            set.Load(cmd.ExecuteReader());
            return set;
        }

        public bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        
        public bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                switch (ex.Number)
                {
                    case 0:
                        Console.WriteLine("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        Console.WriteLine("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        public void fillOrganisation()
        {
            CallStoredProcedure("fillOrganisation");
        }

        public void createOrganisation(string OrgName , string OrgEMail , string WebSite )
        {
            CallStoredProcedure("createOrganisation", new List<string>() { OrgName, OrgEMail, WebSite});
        }

        public void createOwner(string FirstName, string Name, string fiestEmail, string lastEmail, string title, string initials)
        {
            CallStoredProcedure("createOwner", new List<string>() { FirstName, Name, fiestEmail, lastEmail, title, initials });
        }

        public void createClientContact(string FirstName, string Name, string fiestEmail, string lastEmail, string title, string initials)
        {
            CallStoredProcedure("createClientContact", new List<string>() { FirstName, Name, fiestEmail, lastEmail, title, initials });
        }

        public void createEmployee(string FirstName, string Name, string fiestEmail, string lastEmail, string title, string initials)
        {
            CallStoredProcedure("createEmployee", new List<string>() { FirstName, Name, fiestEmail, lastEmail, title, initials });
        }
      

        public void createService(string servicesName, string shortName, string description, string price)
        {
            CallStoredProcedure("createService", new List<string>() { servicesName, shortName, description, price});
        }

        public void createClient(string clientName)
        {
            CallStoredProcedure("createClient", new List<string>() { clientName});
        }

        public void createDraft(string internalId, string description, string rating, string discount,
               string total, string street, string nb, string npa, string city, string country, string note)
        {
            CallStoredProcedure("createDraft", new List<string>() { internalId, description, rating, discount, total, street, city, country, note });
        }

        public void createVisit(string startDate, string endDate, string typeOfVisit, string jobid, string employeeId )
        {
            if (jobid != null && employeeId != null)
            {
                CallStoredProcedure("setJobId", new List<string>() { jobid.ToString() });
                CallStoredProcedure("setEmployeeId", new List<string>() { employeeId.ToString() });
            }
            CallStoredProcedure("createVisit", new List<string>() { startDate, endDate, typeOfVisit });
        }

        public void createClientAddress(string street, string nb, string npa, string city, string state, string country)
        {
            CallStoredProcedure("createClientAddress", new List<string>() { street, nb, npa, city, state, country });
        }

        public void createOrgAddress(string street, string nb, string npa, string city, string state, string country)
        {
            CallStoredProcedure("createClientAddress", new List<string>() { street, nb, npa, city, state, country });
        }

        public void createTimesheet(string year, string week, string comment )
        {
            CallStoredProcedure("createTimesheet", new List<string>() { year, week, comment });
        }

        public void createTimesheetItem(string start, string end, string duration)
        {
            CallStoredProcedure("createTimesheetItem", new List<string>() { start, end, duration });
        }

        public void sendDraft(string sentToName, string sentToEmail, string dt)
        {
            CallStoredProcedure("sendDraft", new List<string>() { sentToName, sentToEmail, dt });
        }

        public void acceptDraft( string dt)
        {
            CallStoredProcedure("acceptDraft", new List<string>() {  dt });
        }

        public void refuseDraft(string dt)
        {
            CallStoredProcedure("refuseDraft", new List<string>() { dt });
        }

        public void jobHeld(string dt)
        {
            CallStoredProcedure("jobHeld", new List<string>() { dt });
        }

        public void jobDone(string dt)
        {
            CallStoredProcedure("jobDone", new List<string>() { dt });
        }

        public void createPaymentForInvoice(string dt, string amount, string comment)
        {
            CallStoredProcedure("createPaymentForInvoice", new List<string>() { dt, amount, comment });
        }

        public void payInvoiceFully(string dt)
        {
            CallStoredProcedure("payInvoiceFully", new List<string>() { dt });
        }
        
    }
    
}
