﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateTestData
{
    public class engine
    {
        dataGenerator dg = null;
        fileLoader f = null;

        public engine(dataGenerator dg1, fileLoader fl)
        {
            dg = dg1;
            f = fl;
        }

        public void executeSPOnFile(string spName, string fileName, int start = 0, int end = -1)
        {
            dataGenerator d = dg != null ? dg : new dataGenerator();

            f.loadFile(fileName);

            d.Initialize();
            d.OpenConnection();

            if(end == -1)
            {
                for(var i = start + 1; i < f.lineCount(); i++)
                {
                    var args = f.getItems();
                    d.CallStoredProcedure(spName, args);
                    f.nextLine();
                }
            }

            d.CloseConnection();
        }

        public int executeSPOnNextLine(string spName)
        {
            var args = f.getItems();
            var ret = -1;

            try
            {
                Console.Write("Line: " + f.currentLine.ToString() + " ");
                ret = dg.CallStoredProcedure(spName, args);
            }
            catch (Exception e)
            {
                f.nextLine();
                throw;
            }

            if (f.nextLine())
                return ret;
            else
                return -1;
        }

        
    }
}
