﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateTestData
{
    public class fileLoader
    {
        private char LINE_ITEM_SEPARATOR = '|';
        private Dictionary<string, int> header = null;
        private string headerStr = "";
        private string[] content = null;
        public int currentLine = 1;

        public fileLoader(string path= null)
        {
            if(path != null)
            {
                loadFile(path);
            }
        }

        public void loadFile(string name)
        {
            content = System.IO.File.ReadAllLines(name);
            var sp = content[0].Split(LINE_ITEM_SEPARATOR);
            headerStr = content[0];

            header = new Dictionary<string, int>();

            for(var i = 0; i < sp.Length; i ++)
            {
                header.Add(sp[i], i);
            }
        }

        public bool nextLine()
        {
            currentLine = currentLine < content.Length ? currentLine + 1 : currentLine;
            return currentLine < content.Length;
        }

        public int lineCount()
        {
            return content.Length - 1;
        }

        public List<string> getItems()
        {
            List<string> ret = new List<string>();
            foreach(string a in content[currentLine].Split(LINE_ITEM_SEPARATOR))
            {
                ret.Add(a);
            }

            return ret;
        }

        private string getItem(string itemName)
        {
            if (!header.ContainsKey(itemName))
            {
                Console.WriteLine("Item " + itemName + " not found in header " + header);
                throw new Exception();
            }
            else
            {
                try
                {
                    string[] lineItems = content[currentLine].Split(LINE_ITEM_SEPARATOR);
                    return lineItems[header[itemName]];
                }
                catch (Exception)
                {
                    Console.WriteLine("ERROR: item " + itemName + " not found in line " + content[currentLine]);
                    throw;
                }
            }
        }
    }
}
