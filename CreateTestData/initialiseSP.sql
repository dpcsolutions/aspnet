﻿use calani;

delimiter //

DROP PROCEDURE IF EXISTS calani.fillOrganisation //

CREATE PROCEDURE fillOrganisation ()
    BEGIN
		INSERT INTO bankaccount(organizationId, priority, currency, bankname, accountname, iban, swift)
			VALUES (@OrgId, 1, 'CHF', 'UBS', 'Jean de la Noé', 'CH5604835012345678009', 'CH987654321');
		INSERT INTO taxes(name, rate, organizationId) VALUES('TVA 7.7', 7.7, @OrgId);
		INSERT INTO taxes(name, rate, organizationId) VALUES('TVA 3.7', 3.7,  @OrgId);
		INSERT INTO taxes(name, rate, organizationId) VALUES('TVA 2.5', 2.5,  @OrgId);
		INSERT INTO services(name, internalId, unitPrice, taxId, description, organizationId, type, recordStatus) VALUES	('Marketing','MKT',0,NULL,'Activités de marketing', 1,NULL,0),
										('Vente','VNT',0,NULL,'Activités de vente', @OrgId,2,0),
										('Ressources Humaines','RH',0,NULL,'Ressources humaines', @OrgId,2,0),
										('Logistique','LOG',0,NULL,'Activités liées à la logistique', @OrgId,2,0),
										('Inventaire','INV',0,NULL,'Activités liées aux inventaires', @OrgId,2,0),
										('Administration','ADM',0,NULL,'Activités administratives', @OrgId,2,0),
										('Finance','FIN',0,NULL,'Finance, comptabilité, facturation', @OrgId,2,0),
										('Autre','AUT',0,NULL,'Diverses activités non spécifiques', @OrgId,2,0);

END//

DROP PROCEDURE IF EXISTS calani.createService //

CREATE PROCEDURE createService ( serviceName varchar(80),  price double, internalId varchar(45))
    BEGIN
		INSERT INTO `services`( name, internalId, unitPrice, organizationId, type, recordStatus) 
	VALUES 
	(serviceName,internalId, price,@OrgId,1,0);
	SELECT @ServiceId := LAST_INSERT_ID();
END//

DROP PROCEDURE IF EXISTS calani.addServiceTojob //

CREATE PROCEDURE addServiceTojob ( id int, name varchar(200), price double, srvId int, totalPriceExclTax double, taxAmount double, totalPriceIncTax double, quantity double)
    BEGIN
		INSERT INTO `services_jobs`( jobId, taxName, taxRate, serviceQuantity, serviceName, servicePrice, serviceId, type, totalPriceExclTax, taxAmount, totalPrinceIncTax) 
	VALUES 
	(id, "TVA 7.7", 7.7, quantity, name, price, srvId, 1, totalPriceExclTax, taxAmount, totalPriceIncTax);
 END//

DROP PROCEDURE IF EXISTS calani.createOrganisation //

CREATE PROCEDURE createOrganisation ( OrgName varchar(80), OrgEMail varchar(80), WebSite varchar(200))
    BEGIN
		INSERT INTO `organizations`( name, owner, email, phone, telecopy, userLimit, dateLimit, logo, creationDate, activationCode, activationDate, delayQuote, delayInvoice, financialYearStart, currency, website, vatnumber, trialEnd, subscriptionStatus, subscriptionEnd, subscriptionId) 
	VALUES 
	(OrgName,null, OrgEmail,'+41 22 3333333','022 333 33 34',NULL,NULL,NULL,'2018-02-19 10:00:00',NULL,'2018-02-19 11:00:00',20,30,'0001-01-01 00:00:00',NULL,WebSite,'CHE 123.22.1123',NULL,NULL,NULL,NULL);
	SELECT @OrgId := LAST_INSERT_ID();
 END//
 
DROP PROCEDURE IF EXISTS calani.createAdmin //

 
CREATE PROCEDURE createAdmin ( ContactFName varchar(30), ContactLName varchar(40), firstemail varchar(80), initials varchar(3))
    BEGIN
	INSERT INTO contacts (firstName, lastName, dateOfbirth, primaryPhone, secondaryPhone, primaryEmail, secondaryEmail, telecopy, clientId, organizationId, type, password, recoverCode, status, role, title, civtitle, initials, account, weeklyHours, lang, stafftoken, recordStatus) 
	VALUES 
    (ContactFName,ContactLName,NULL,'','',firstemail,NULL,NULL,NULL, NULL,100,'9A8605F7C6C0C263E60F7E9F58DA56AFC9E18929',NULL,NULL,NULL,'ADMIN','Mr',initials,NULL,NULL,NULL,NULL,0);
 END//

DROP PROCEDURE IF EXISTS calani.createOwner //

CREATE PROCEDURE createOwner ( ContactFName varchar(30), ContactLName varchar(40), firstemail varchar(80), secondemail varchar(80), title varchar(10), initials varchar(3))
    BEGIN
	INSERT INTO contacts (firstName, lastName, dateOfbirth, primaryPhone, secondaryPhone, primaryEmail, secondaryEmail, telecopy, clientId, organizationId, type, password, recoverCode, status, role, title, civtitle, initials, account, weeklyHours, lang, stafftoken, recordStatus) 
	VALUES 
    (ContactFName,ContactLName,NULL,'+41 22 999 999 99','+41 78 999 99 99',firstemail,secondemail,NULL,NULL,@OrgId,1,'9A8605F7C6C0C263E60F7E9F58DA56AFC9E18929',NULL,NULL,NULL,title,'Mr',initials,NULL,NULL,NULL,NULL,0);
	SELECT @OrgOwner := LAST_INSERT_ID();
	update organizations set owner = @OrgOwner where id = @OrgId;
 END//

DROP PROCEDURE IF EXISTS calani.createClientContact //

CREATE PROCEDURE createClientContact ( ContactFName varchar(30), ContactLName varchar(40), firstemail varchar(80), secondemail varchar(80), title varchar(10), initials varchar(3))
    BEGIN
	INSERT INTO contacts (firstName, lastName, dateOfbirth, primaryPhone, secondaryPhone, primaryEmail, secondaryEmail, telecopy, clientId, organizationId, type, password, recoverCode, status, role, title, civtitle, initials, account, weeklyHours, lang, stafftoken, recordStatus) 
	VALUES 
    (ContactFName,ContactLName,NULL,'+41 22 999 999 99','+41 78 999 99 99',firstemail,secondemail,NULL,@clientId,@OrgId,11,'9A8605F7C6C0C263E60F7E9F58DA56AFC9E18929',NULL,NULL,NULL,title,'Mr',initials,NULL,NULL,NULL,NULL,0);
	SELECT @ClientContactId := LAST_INSERT_ID();
 END// 

  
DROP PROCEDURE IF EXISTS calani.createEmployee //

CREATE PROCEDURE createEmployee ( ContactFName varchar(30), ContactLName varchar(40), firstemail varchar(80), secondemail varchar(80), title varchar(10), initials varchar(3))
    BEGIN
	INSERT INTO contacts (firstName, lastName, dateOfbirth, primaryPhone, secondaryPhone, primaryEmail, secondaryEmail, telecopy, clientId, organizationId, type, password, recoverCode, status, role, title, civtitle, initials, account, weeklyHours, lang, stafftoken, recordStatus) 
	VALUES 
    (ContactFName,ContactLName,NULL,'+41 22 999 999 99','+41 78 999 99 99',firstemail,secondemail,NULL,NULL,@OrgId,2,'9A8605F7C6C0C263E60F7E9F58DA56AFC9E18929',NULL,NULL,NULL,title,'Mr',initials,NULL,NULL,NULL,NULL,0);
	SELECT @EmployeeId := LAST_INSERT_ID();

 END// 

 DROP PROCEDURE IF EXISTS calani.createInternalService //

CREATE PROCEDURE createInternalService ( serviceName varchar(50), shortName varchar(40), description varchar(250), price double)
    BEGIN
	INSERT INTO services(name, internalId, unitPrice, taxId, description, organizationId, type) 
	VALUES (serviceName, shortName, price, @OrgId, description , 2, 0);
	SELECT @serviceId := LAST_INSERT_ID();
 END// 


 DROP PROCEDURE IF EXISTS calani.createClient //

CREATE PROCEDURE createClient ( companyName varchar(50))
    BEGIN
	INSERT INTO clients(companyName, organizationId) VALUES(companyName, @OrgId);
	SELECT @clientId := LAST_INSERT_ID();
 END// 

DROP PROCEDURE IF EXISTS calani.createDraft //

CREATE PROCEDURE createDraft ( internalId varchar(45), description varchar(255), rating INT, discount INT,
	total double, street varchar(45), nb varchar(10), npa INT, city varchar(45), region varchar(45), country varchar(45),
	note varchar(255))
    BEGIN
	INSERT INTO `jobs` (`internalId`,`clientId`,`description`,`rating`,`discount`,`total`,`street`,`street2`,
	`nb`,`npa`,`city`,`country`,`internalNote`,`dateCreated`,`dateQuoteValidity`,`type`,`organizationId`,`lastUpdate`,`recordStatus`) 
	VALUES (internalId, @clientId, description, rating, discount,
	total,street,'', nb, npa, city, country,
	note,'2018-02-05 11:00:00','2019-01-01 00:00:00',1,@OrgId,'2018-02-05 11:00:00',0);
	SELECT @draftId := LAST_INSERT_ID();
END//  

DROP PROCEDURE IF EXISTS calani.createVisit //

CREATE PROCEDURE createVisit ( startDate DATETIME, endDate DATETIME, statusOfVisit int)
    BEGIN
	INSERT INTO `visits` (`jobId`,`dateStart`,`dateEnd`,`visitStatus`,`type`, `recordStatus`,orgId) 
	VALUES (@jobId,startDate,endDate, statusOfVisit, 1,0, @OrgId);
	SELECT @visitId := LAST_INSERT_ID();
	INSERT INTO visits_contacts (visitId,contactId) VALUES (@visitId, @EmployeeId);
END//  

DROP PROCEDURE IF EXISTS calani.setJobId //

CREATE PROCEDURE setJobId ( jobId int )
    BEGIN
	SET @jobId = jobId;
END//  

DROP PROCEDURE IF EXISTS calani.setEmployeeId //

CREATE PROCEDURE setEmployeeId ( employeeId int )
    BEGIN
	SET @EmployeeId = employeeId;
END// 
 
DROP PROCEDURE IF EXISTS calani.createClientAddress //

CREATE PROCEDURE createClientAddress ( street varchar(45),  nb varchar(10), npa varchar(10), city varchar(45),
	state varchar(45), country varchar(45))
    BEGIN
	INSERT INTO `addresses` (`street`, `nb`, `npa`, `city`, `state`, `country`, `clientId`, 
							`organizationId`, `type`, `recordStatus`) 
	VALUES (street, nb, npa, city, state, country, @clientId, @OrgId, '2', '0');
	SELECT @addressId := LAST_INSERT_ID();
END//  

 
DROP PROCEDURE IF EXISTS calani.createOrgAddress //

CREATE PROCEDURE createOrgAddress ( street varchar(45),  nb varchar(10), npa varchar(10), city varchar(45),
	state varchar(45), country varchar(45))
    BEGIN
	INSERT INTO `addresses` (`street`, `nb`, `npa`, `city`, `state`, `country`, `clientId`, 
							`organizationId`, `type`, `recordStatus`) 
	VALUES (street, nb, npa, city, state, country, NULL, @OrgId, '1', '0');
	SELECT @addressId := LAST_INSERT_ID();
END//  

DROP PROCEDURE IF EXISTS calani.createTimesheet //

CREATE PROCEDURE createTimesheet ( year INT,  week INT, comment varchar(255) )
    BEGIN
	INSERT INTO `timesheets` (`year`, `week`, `comment`, `organizationId`,`recordStatus`) 
	VALUES (year, week, comment, @OrgId, '0');
	SELECT @timesheetId := LAST_INSERT_ID();
END//  

 
DROP PROCEDURE IF EXISTS calani.createTimesheetItem //

CREATE PROCEDURE createTimesheetItem ( startDate DATETIME,  endDate DATETIME, duration double )
    BEGIN
	INSERT INTO `timesheetrecords` (`timesheetId`, `startDate`, `endDate`, `rate`, `duration`) 
	VALUES (@timesheetId, startDate, endDate, '1', duration);
	SELECT @timesheetRecordId := LAST_INSERT_ID();
END//  

 
DROP PROCEDURE IF EXISTS calani.sendDraft //

CREATE PROCEDURE sendDraft ( psentToName varchar(45),  psentToEmail varchar(80), dt DATETIME )
    BEGIN
	UPDATE `jobs` set sentToName = psentToName, sentToEmail = psentToEmail, dateQuoteSent = dt
	WHERE id = @draftId;
END//  

 
DROP PROCEDURE IF EXISTS calani.acceptDraft //

CREATE PROCEDURE acceptDraft ( dt DATETIME )
    BEGIN
	UPDATE `jobs` set dateQuoteAccepted = dt
	WHERE id = @draftId;
	INSERT INTO `jobs` (`internalId`,`clientId`,`description`,`rating`,`discount`,`total`,`street`,`street2`,
	`nb`,`npa`,`city`,`country`,`internalNote`,`dateCreated`,`dateQuoteValidity`,`type`,`organizationId`,`lastUpdate`,`recordStatus`) 
	select `internalId`,`clientId`,`description`,`rating`,`discount`,`total`,`street`,`street2`,
	`nb`,`npa`,`city`,`country`,`internalNote`,`dateCreated`,`dateQuoteValidity`,`type`,`organizationId`,`lastUpdate`,`recordStatus`
	from jobs where id = @draftId;
	SELECT @jobId := LAST_INSERT_ID();
	update jobs set dateQuoteSent = NULL, sentToName = NULL, sentToEmail = NULL, parentId = @draftId,
	dateOpenJob = dt, dateCreated = dt
	WHERE id = LAST_INSERT_ID();
END//  

 
DROP PROCEDURE IF EXISTS calani.refuseDraft //
  
CREATE PROCEDURE refuseDraft ( dt DATETIME )
    BEGIN
	UPDATE `jobs` set dateQuoteRefused = dt
	WHERE id = @draftId;
END//  
 
 
DROP PROCEDURE IF EXISTS calani.jobHeld //

CREATE PROCEDURE jobHeld ( dt DATETIME )
    BEGIN
	UPDATE `jobs` set dateJobHeld = dt
	WHERE id = @jobId;
END//  

 
DROP PROCEDURE IF EXISTS calani.jobDone //

CREATE PROCEDURE jobDone ( dt DATETIME )
    BEGIN
	UPDATE `jobs` set dateJobDone = dt
	WHERE id = @jobId;
	select @dueDays = delayInvoice from organizations where id = @OrgId;
	INSERT INTO `jobs` (`internalId`,`clientId`,`description`,`rating`,`discount`,`total`,`street`,`street2`,
	`nb`,`npa`,`city`,`country`,`internalNote`,`dateCreated`,`dateQuoteValidity`,`type`,`organizationId`,`lastUpdate`,`recordStatus`) 
	select `internalId`,`clientId`,`description`,`rating`,`discount`,`total`,`street`,`street2`,
	`nb`,`npa`,`city`,`country`,`internalNote`,`dateCreated`,`dateQuoteValidity`,`type`,`organizationId`,`lastUpdate`,`recordStatus`
	from jobs where id = @jobId;
	SELECT @billId := LAST_INSERT_ID();
	update jobs set dateJobDone = NULL, parentId = @jobId,
	dateInvoicing = dt, dateCreated = dt, dateInvoiceDue = DATE_ADD(dt, INTERVAL @dueDays DAY),
	parentId = @jobId
	WHERE id = @billId;
END//  

 
DROP PROCEDURE IF EXISTS calani.createPaymentForInvoice //

CREATE PROCEDURE createPaymentForInvoice ( dt DATETIME, amount DOUBLE, comment VARCHAR(255) )
    BEGIN
	INSERT INTO payments(datePayed, amountPayed, jobId, comment, recordStatus)
	VALUES (dt, amount, @billId, comment, 0);
	SELECT @paymentId := LAST_INSERT_ID();
END//  

DROP PROCEDURE IF EXISTS calani.payInvoiceFully //

CREATE PROCEDURE payInvoiceFully ( dt DATETIME )
    BEGIN
	UPDATE jobs set dateFullyPaid = dt where id = @invoiceId;
END//  

DROP PROCEDURE IF EXISTS calani.setContactAsMainContact //

CREATE PROCEDURE setContactAsMainContact()
    BEGIN
	UPDATE contacts set type = 10 where id = @ClientContactId;
END//  

delimiter ;

call createAdmin("Site", "admin", 'superadmin@gesmobile.ch', "sas");