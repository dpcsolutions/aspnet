﻿using System;
using Google.Apis.Auth.OAuth2;
using System.Threading.Tasks;
using System.IO;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Configuration;
using log4net;
using System.Reflection;

namespace PushServer
{
    public class FirebaseNotificationSender
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private string _keyPath = (String)ConfigurationManager.AppSettings["firebaseAPIKey"];
        private static string accessToken = null;

        public class NotificationEventArgs : EventArgs
        {
            public string token { get; set; }
            public string message { get; set; }
        }
     
        public event EventHandler<NotificationEventArgs> OnNotificationFailed;
        public event EventHandler<NotificationEventArgs> OnNotificationSucceeded;

      
        public FirebaseNotificationSender()
        {
        }

        private void OnNotificationFailedMethod(string token, string error)
        {
            if (OnNotificationFailed != null)
            {
                OnNotificationFailed(token, new NotificationEventArgs() { token = token, message = error});
            }
        }

        private void OnNotificationSucceededMethod(string token)
        {
            if (OnNotificationSucceeded != null)
            {
                OnNotificationSucceeded(token, new NotificationEventArgs() { token = token, message = "" });
            }
        }

        private async Task<string> GetAccessToken()
        {
            using (var stream = new FileStream(_keyPath, FileMode.Open, FileAccess.Read))
            {
                var tmp = GoogleCredential.FromStream(stream);
                tmp = tmp.CreateScoped("https://www.googleapis.com/auth/firebase.messaging");
                return await tmp.UnderlyingCredential.GetAccessTokenForRequestAsync().ConfigureAwait(false);
            }
        }
        
        public void sendNotificationAsync(string token, string message, string title, Dictionary<string, string> data, bool isIOS)
        {
            sendNotificationInternal(token, message, title, data, isIOS);
        }

        public void sendNotificationSync(string token, string message, string title, Dictionary<string, string> data, bool isIOS)
        {
            Task callTask = Task.Run(() => sendNotificationInternal(token, message, title, data, isIOS));
            callTask.Wait();
        }

        public async Task sendNotificationInternal(string token, string message, string title, Dictionary<string, string> data, bool isIOS)
        {
            try
            {
                HttpClient client = new HttpClient();

                string strData = "";

                foreach (var d in data.Keys)
                {
                    strData += "\"" + d + "\"" + ":" + "\"" + data[d] + "\"" + ",";
                }

                if (strData.Length > 0)
                    strData = strData.Substring(0, strData.Length - 1);

                var content = @"{
                ""message"":{
                    ""token"" : """ + token + @""",
                    ""notification"" : {
                        ""body"" : """ + message + @""",
                        ""title"" : """ + title + @"""
                    },
                 ""data"" : {" + strData + "}}}";

                var msg = new HttpRequestMessage()
                {
                    Version = HttpVersion.Version11,
                    Method = HttpMethod.Post,
                    RequestUri = new Uri("https://fcm.googleapis.com/v1/projects/gesmobile-8ac16/messages:send"),
                    Content = new StringContent(content, Encoding.UTF8, "application/json")
                };



                /*  ""notification"" : {
                      ""body"" : """ + message + @""",
                              ""title"" : """ + title + @"""*/
                _log.Debug("getting notif access token");

                try
                {
                   // if (String.IsNullOrEmpty(accessToken))
                        accessToken = await GetAccessToken().ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    _log.Error("Error getting notification token ", e);
                }

                try
                {
                    _log.Debug("got notif access token");
                    msg.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                }
                catch (Exception e)
                {
                    _log.Error("Error creating AuthenticationHeaderValue ", e);
                }
          
                _log.Debug("sending async");

                HttpResponseMessage ret = null;

                try
                {
                    ret = await client.SendAsync(msg).ConfigureAwait(false);
                }
                catch (Exception e)
                {
                    _log.Error("Exception calling client.sendAsync for notifications", e);
                }
               

                _log.Debug("sent");

                if (ret.StatusCode == HttpStatusCode.OK)
                {
                    OnNotificationSucceededMethod(token);
                }
                else
                {
                    _log.Error("notification failed with error " + ret.ReasonPhrase + " for " + ret.RequestMessage);

                   OnNotificationFailedMethod(token, ret.ReasonPhrase);
                }
            }
            catch (Exception e)
            {
                _log.Error("Could not send notification.", e);
            }
          
        }
    }
}
