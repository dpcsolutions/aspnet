﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySql;

namespace calani_business_obj_test
{
    [TestClass]
    public class global_setting
    {
        [TestMethod]
      // date time format exception expected
        public void Expecting_datetime_format_exception_OR_Expecting_Date_validation_for_manual_date()
        {

            Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(260);
            try
            {
                mgr.FinancialYearStart = Convert2.ToDate(DateTime.Now.ToString(), "picker").Value;



            }
            catch { }
            try
            {
                mgr.DelayInvoice = Convert.ToInt32("30");
                if (mgr.DelayInvoice < 3) mgr.DelayInvoice = 3;
                if (mgr.DelayInvoice > 730) mgr.DelayInvoice = 730;


            }
            catch { }
            try
            {
                mgr.DelayQuote = Convert.ToInt32("30");
                if (mgr.DelayQuote < 3) mgr.DelayQuote = 3;
                if (mgr.DelayQuote > 730) mgr.DelayQuote = 730;

            }
            catch { }
            mgr.Currency = "";// ddlCurrency.SelectedValue;

            int emailAlertLateInvoicesDay = 1;
            Int32.TryParse("2", out emailAlertLateInvoicesDay);
            mgr.EmailAlertLateInvoicesDay = emailAlertLateInvoicesDay;

            if (1 == 1)
            {
                mgr.BeforeInvoice = "DeliveryNote";
            }

            var ret = mgr.Save(true, true, true, false);
            Assert.AreEqual(false,ret.Success);
        }


        [TestMethod]
 [ExpectedException(typeof(System.InvalidOperationException))]
     
        public void Expecting_InvalidOperationException_For_saving_non_existing_Currency_value_foreign_key_check_required()
        {

            Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(260);
            try
            {
                mgr.FinancialYearStart = DateTime.Now;// Convert2.ToDate(DateTime.Now.ToString(), "picker").Value;



            }
            catch { }
            try
            {
                mgr.DelayInvoice = Convert.ToInt32("30");
                if (mgr.DelayInvoice < 3) mgr.DelayInvoice = 3;
                if (mgr.DelayInvoice > 730) mgr.DelayInvoice = 730;


            }
            catch { }
            try
            {
                mgr.DelayQuote = Convert.ToInt32("30");
                if (mgr.DelayQuote < 3) mgr.DelayQuote = 3;
                if (mgr.DelayQuote > 730) mgr.DelayQuote = 730;

            }
            catch { }
            mgr.Currency = "bitcoin";// ddlCurrency.SelectedValue;

            int emailAlertLateInvoicesDay = 1;
            Int32.TryParse("2", out emailAlertLateInvoicesDay);
            mgr.EmailAlertLateInvoicesDay = emailAlertLateInvoicesDay;

            if (1 == 1)
            {
                mgr.BeforeInvoice = "DeliveryNote";
            }

            var ret = mgr.Save(true, true, true, false);
            Assert.AreEqual(true, ret.Success);
        }


        [TestMethod]
        [ExpectedException(typeof(System.InvalidOperationException))]    
        public void Expecting_Validation_and_Exception_for_emailAlertLateInvoicesDay_Accepting_large_value()
        {

            Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(260);
            try
            {
                mgr.FinancialYearStart = DateTime.Now;// Convert2.ToDate(DateTime.Now.ToString(), "picker").Value;



            }
            catch { }
            try
            {
                mgr.DelayInvoice = Convert.ToInt32("30");
                if (mgr.DelayInvoice < 3) mgr.DelayInvoice = 3;
                if (mgr.DelayInvoice > 730) mgr.DelayInvoice = 730;


            }
            catch { }
            try
            {
                mgr.DelayQuote = Convert.ToInt32("30");
                if (mgr.DelayQuote < 3) mgr.DelayQuote = 3;
                if (mgr.DelayQuote > 730) mgr.DelayQuote = 730;

            }
            catch { }
            mgr.Currency = "";// ddlCurrency.SelectedValue;

            int emailAlertLateInvoicesDay = 1;
            Int32.TryParse("200", out emailAlertLateInvoicesDay);
            mgr.EmailAlertLateInvoicesDay = emailAlertLateInvoicesDay;

            if (1 == 1)
            {
                mgr.BeforeInvoice = "DeliveryNote";
            }

            var ret = mgr.Save(true, true, true, false);
           // Assert.AreEqual(true, ret.Success);
        }



    }
}
