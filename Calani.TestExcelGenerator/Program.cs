﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Resources;
using Calani.BusinessObjects.DocGenerator;
using Calani.BusinessObjects;

namespace Calani.TestExcelGenerator
{
    class Program
    {

       

        static void GenerateBvr()
        {
            Calani.BusinessObjects.DocGenerator.QrBvrPdfGenerator qr = new QrBvrPdfGenerator();
            qr.TopMargin = -544;
            qr.BvrPdf = @"\\Mac\Home\Documents\vssources\repos\calani\Calani.Webapp\App_Data\bvr\bvr-qr-up.pdf";

            qr.ToIban = "CH6431961000004421557";
            qr.ToLine1 = "Robert Schneider AG";
            qr.ToLine2 = "Rue du Lac 1268";
            qr.ToLine3 = "2501 Biel";
            qr.ToCountryCode = "CH";
            qr.RefNumberBank = "000008";
            qr.RefNumberNum = "20779122585742128669";
            qr.FromLine1 = "Sarah Beispiel";
            qr.FromLine2 = "Mustergasse 1";
            qr.FromLine3 = "3600 Thoune";
            qr.Label = "Prime mensuelle juillet 2020";
            qr.Amount = 0;
            var bin2 = qr.AppendToPdf(null);
            System.IO.File.WriteAllBytes("test-qr-up.pdf", bin2);

            return;

            Calani.BusinessObjects.DocGenerator.ClassicBvrPdfGenerator bvr = new ClassicBvrPdfGenerator();
            bvr.BvrPdf = @"\\Mac\Home\Documents\vssources\repos\calani\Calani.Webapp\App_Data\bvr\bvr-bank.pdf";
            bvr.ToBankLine1 = "Swisstorage SA / 112415";
            bvr.ToBankLine2 = "Rue de la Télérésidence 10";
            bvr.ToLine1 = "Swisstorage SA";
            bvr.ToLine2 = "Rue de la Télérésidence 10";
            bvr.ToLine3 = "3963 Crans-Montana / 112415";
            bvr.ToLine4 = "";
            bvr.FromLine1 = "RGS Immo GmbH";
            bvr.FromLine2 = "Chemin de Chantevent, 37";
            bvr.FromLine3 = "3960 Sierre";
            bvr.Account = "01-12162-7";
            bvr.RefNumberBank = "112415";
            bvr.RefNumberNum = "123494";
            bvr.Amount = 152;
            bvr.DebugRectangles = false;
            bvr.HideAccountAndBank = true;
            var bin = bvr.AppendToPdf(null);
            System.IO.File.WriteAllBytes("test-bvr.pdf", bin);

        }
        static void Main(string[] args)
        {

            GenerateBvr();


        }
    }
}
