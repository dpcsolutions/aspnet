﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.Membership
{
    public partial class NewAccountManager
    {

        public string Email { get; set; }

        public string Password { get; set; }
        //public string PasswordConfirm { get; set; }

        public string CompanyName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public bool AcceptTerms { get; set; }

        public string KnowUs { get; set; }


        public int TrialPeriod { get; set; }
        public int TrialStartWithUsers { get; set; }


        public NewAccountManager()
        {
            try
            {
                TrialPeriod = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrialPeriod"]);
            }
            catch
            {
                TrialPeriod = 15;
            }

            try
            {
                TrialStartWithUsers = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["NewAccountUsersLimit"]);

            }
            catch
            {
                TrialStartWithUsers = 3;
            }
        }

        public Generic.SmartAction<Model.organizations> Create(string pathForDefaultOrgData)
        {
            var ret = new Generic.SmartAction<Model.organizations>(Generic.SmartActionType.Add);
            ret.Success = true;


            // email
            if (Email == null)
            {
                ret.Abort("ERR_Email_invalid");
                return ret;
            }
            
            Email = Email.ToLower();
            if (!Generic.EmailValidator.IsEmailValid(Email))
            {
                ret.Abort("ERR_Email_invalid");
                return ret;
            }

            LoginManager lmgr = new LoginManager();
            var exist = lmgr.GetUser(Email);
            if(exist != null)
            {
                ret.Abort("ERR_Email_already_used");
                return ret;
            }


            // -->

            // password
            if (Password == null)
            {
                ret.Abort("ERR_Password_invalid");
                return ret;
            }
            /*if (PasswordConfirm == null)
            {
                ret.Abort("ERR_Password_invalid");
                return ret;
            }*/
            if (!Calani.BusinessObjects.Generic.PasswordAdvisor.IsEnoughtStrength(Password))
            {
                ret.Abort("Error_NotSecuredEnought");
                return ret;
            }

            /*if (Password != PasswordConfirm)
            {
                ret.Abort("Error_Password_Mismatch");
                return ret;
            }*/
            // -->


            // mandatory fields
            if (String.IsNullOrWhiteSpace(CompanyName))
            {
                ret.Abort("ERR_CompanyName_invalid");
            }
            if (CompanyName.Length < 3)
            {
                ret.Abort("ERR_CompanyName_invalid");
                return ret;
            }

            if (String.IsNullOrWhiteSpace(FirstName))
            {
                ret.Abort("ERR_Firstname_invalid");
                return ret;
            }

            if (String.IsNullOrWhiteSpace(FirstName))
            {
                ret.Abort("ERR_Lastname_invalid");
                return ret;
            }


            if (DateOfBirth != null)
            {

                TimeSpan age = DateTime.Now - DateOfBirth.Value;
                int ageYears = (new DateTime(1, 1, 1) + age).Year - 1;
                if (ageYears < 18)
                {
                    ret.Abort("ERR_DateOfBirth_invalid");
                    return ret;
                }
            }

            if(!AcceptTerms)
            {
                ret.Abort("ERR_Need_Accept_Terms");
                return ret;
            }
            // -->

            Model.CalaniEntities db = new Model.CalaniEntities();

            Model.organizations org = new Model.organizations();
            org.email = Email;
            org.name = CompanyName;
            org.creationDate = DateTime.Now;
            org.activationCode = Generic.PasswordTools.RandomCode(5);
            org.activationDate = null;
            org.trialEnd = DateTime.Now.AddDays(TrialPeriod);
            org.userLimit = TrialStartWithUsers;
            org.dailyMailsLimit = 20;
            org.knowus = KnowUs;
            org.use_work_schedule = true;
            org.phone = Phone;
            db.organizations.Add(org);

            db.SaveChanges();

          
            ret.Record = org;

            Model.contacts ctc = new Model.contacts();
            ctc.type = Convert.ToInt32(Contacts.ContactTypeEnum.User_Admin);
            ctc.firstName = FirstName;
            ctc.lastName = LastName;
            ctc.initials = "";

            if (!String.IsNullOrWhiteSpace(ctc.firstName) && ctc.firstName.Length > 1)
                ctc.initials = ctc.firstName.Substring(0, 1);

            if (!String.IsNullOrWhiteSpace(ctc.lastName) && ctc.lastName.Length > 2)
                ctc.initials += ctc.lastName.Substring(0, 2);

            if (String.IsNullOrWhiteSpace(ctc.initials))
                ctc.initials = "XXX";
            ctc.primaryEmail = Email;
            ctc.password = Generic.PasswordTools.Hash(Password);
            ctc.organizationId = org.id;
            //ctc.dateOfBirth = DateOfBirth;
            db.contacts.Add(ctc);
            db.SaveChanges();
            if(ctc != null && ret.Record != null)
                ret.Record.owner = ctc.id;

            org.owner = ctc.id;

            db.work_schedule_profiles_rec.Add(new work_schedule_profiles_rec
            {
                recordStatus = 0,
                organization_id = org.id,
                name = "Custom",
                rate = 100,
                type = (int)TimeCategoryType.Custom
            });
            db.work_schedule_profiles_rec.Add(new work_schedule_profiles_rec
            {
                recordStatus = 0,
                organization_id = org.id,
                name = "Auto",
                rate = 100,
                type = (int)TimeCategoryType.Auto
            });

            db.work_schedule_profiles_rec.Add(new work_schedule_profiles_rec
            {
                recordStatus = 0,
                organization_id = org.id,
                name = "Vacation",
                rate = 100,
                type = (int)TimeCategoryType.Vacation
            });

            db.work_schedule_profiles_rec.Add(new work_schedule_profiles_rec
            {
                recordStatus = 0,
                organization_id = org.id,
                name = "Normal",
                rate = 100,
                type = (int)TimeCategoryType.Normal
            });
            db.work_schedule_profiles_rec.Add(new work_schedule_profiles_rec
            {
                recordStatus = 0,
                organization_id = org.id,
                name = "Overtime",
                rate = 100,
                type = (int)TimeCategoryType.Overtime
            });
            db.work_schedule_profiles_rec.Add(new work_schedule_profiles_rec
            {
                recordStatus = 0,
                organization_id = org.id,
                name = "Unpaid",
                rate = 0,
                type = (int)TimeCategoryType.Unpaid
            });

            db.work_schedule_profiles_rec.Add(new work_schedule_profiles_rec
            {
                recordStatus = 0,
                organization_id = org.id,
                name = "Ill",
                rate = 100,
                type = (int)TimeCategoryType.Ill
            });

            db.work_schedule_profiles_rec.Add(new work_schedule_profiles_rec
            {
                recordStatus = 0,
                organization_id = org.id,
                name = "Lunch",
                rate = -100,
                type = (int)TimeCategoryType.Lunch
            });
            db.SaveChanges();
            

            CreateDefaultObjects(org.id, pathForDefaultOrgData);


            try
            {
                MailChimp.MailChimp mc = new MailChimp.MailChimp(System.Configuration.ConfigurationManager.AppSettings["MailChimpToken"],
                    System.Configuration.ConfigurationManager.AppSettings["MailChimpDataCenter"]);
                mc.AddSubscriber(
                    System.Configuration.ConfigurationManager.AppSettings["MailChimpListId"],
                    org.email,
                    org.name,
                    FirstName + " " + LastName);

            }
            catch
            {
            }


            if (DisableActivationEmail == false)
            {
                Generic.MailSender mailSender = new Generic.MailSender();
                mailSender.MailTo.Email = ctc.primaryEmail;
                mailSender.MailTo.UserId = ctc.id;
                //mailSender.Subject = "GesMobile.ch - Validez votre email";
                //mailSender.Variables.Add("title", ActivateYourAccount);
                //mailSender.Variables.Add("intro", ActivateYourAccount_more.Replace("$Firstname", FirstName).Replace("$Lastname", LastName));
                //mailSender.Variables.Add("buttontext", ActivateYourAccount_button);
                mailSender.Variables.Add("custemail", ctc.primaryEmail);
                mailSender.Variables.Add("buttonurl", Generic.MailSender.PublicUrl + "Verify.aspx?c=" + org.activationCode + "&m=" + Email);
                mailSender.SendSmartHtmlEmail("ges_01_activation.html", "activation");
            }


            //alerte staff
            if (true)
            {
                Generic.MailSender mailSender = new Generic.MailSender();
                mailSender.MailTo.Email = System.Configuration.ConfigurationManager.AppSettings["NewSub"];
                mailSender.MailTo.UserId = ctc.id;
                //mailSender.Subject = "GesMobile.ch - Nouvel inscrit : " + usr.primaryEmail;
                //mailSender.Variables.Add("title", AccountActivated);
                //mailSender.Variables.Add("intro", AccountActivated_more.Replace("$Firstname", usr.firstName).Replace("$Lastname", usr.lastName));
                //mailSender.Variables.Add("buttontext", AccountActivated_button);
                string orgname = "?";
                string orgknowus = "?";

                

                if (ctc.organizations != null)
                {
                    orgname = ctc.organizations.name;
                    orgknowus = ctc.organizations.knowus;
                }

                mailSender.Variables.Add("custemail", ctc.primaryEmail);
                mailSender.Variables.Add("orgname", orgname);
                mailSender.Variables.Add("lastname", ctc.lastName);
                mailSender.Variables.Add("firstname", ctc.firstName);
                mailSender.Variables.Add("knowus", orgknowus);


                mailSender.SendSmartHtmlEmail("ges_01_newsub.html", "activation");
            }

            return ret;
        }


        public bool DisableActivationEmail { get; set; }
        public string Phone { get; set; }


        public void ResendActivation(string email)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();

            var org = (from r in db.organizations where r.email == email select r).FirstOrDefault();

            if (org != null)
            {

                Generic.MailSender mailSender = new Generic.MailSender();
                mailSender.MailTo.Email = org.email;
                if(org.owner != null) mailSender.MailTo.UserId = org.owner.Value;
                //mailSender.Subject = "GesMobile.ch - Validez votre email";
                //mailSender.Variables.Add("title", ActivateYourAccount);
                //mailSender.Variables.Add("intro", ActivateYourAccount_more.Replace("$Firstname", FirstName).Replace("$Lastname", LastName));
                //mailSender.Variables.Add("buttontext", ActivateYourAccount_button);
                mailSender.Variables.Add("custemail", org.email);
                mailSender.Variables.Add("buttonurl", Generic.MailSender.PublicUrl + "Verify.aspx?c=" + org.activationCode + "&m=" + Email);
                mailSender.SendSmartHtmlEmail("ges_01_activation.html", "activation");

            }
        }



        public bool ActivateAccount(string email, string code)
        {

            if (code == null) return false;
            if (email == null) return false;

            code = code.ToUpper();
            email = email.ToLower();

            Model.CalaniEntities db = new Model.CalaniEntities();
            var act = (from r in db.organizations where r.email == email && r.activationCode == code select r).FirstOrDefault();

            if (act == null) return false;

            act.activationCode = null;
            act.activationDate = DateTime.Now;
            db.SaveChanges();


            try
            {
                var usr = (from r in db.contacts where r.id == act.owner select r).FirstOrDefault();

                Generic.MailSender mailSender = new Generic.MailSender();
                mailSender.MailTo.Email = usr.primaryEmail;
                mailSender.MailTo.UserId = usr.id;
                //mailSender.Subject = "GesMobile.ch - Bienvenue";
                //mailSender.Variables.Add("title", AccountActivated);
                //mailSender.Variables.Add("intro", AccountActivated_more.Replace("$Firstname", usr.firstName).Replace("$Lastname", usr.lastName));
                //mailSender.Variables.Add("buttontext", AccountActivated_button);
                mailSender.Variables.Add("custemail", usr.primaryEmail);
                mailSender.Variables.Add("buttonurl", Generic.MailSender.PublicUrl);
                mailSender.SendSmartHtmlEmail("ges_02_bienvenue.html", "activation");


                mailSender = new Generic.MailSender();
                if (!String.IsNullOrWhiteSpace(System.Configuration.ConfigurationManager.AppSettings["NewSub"]))
                {


                    mailSender.MailTo.Email = System.Configuration.ConfigurationManager.AppSettings["NewSub"];
                    mailSender.MailTo.UserId = usr.id;
                    //mailSender.Subject = "GesMobile.ch - Nouvel inscrit : " + usr.primaryEmail;
                    //mailSender.Variables.Add("title", AccountActivated);
                    //mailSender.Variables.Add("intro", AccountActivated_more.Replace("$Firstname", usr.firstName).Replace("$Lastname", usr.lastName));
                    //mailSender.Variables.Add("buttontext", AccountActivated_button);
                    string orgname = "?";
                    string orgknowus = "?";

                    if (usr.organizations != null)
                    {
                        orgname = usr.organizations.name;
                        orgknowus = usr.organizations.knowus;
                    }

                    mailSender.Variables.Add("custemail", usr.primaryEmail);
                    mailSender.Variables.Add("orgname", orgname);
                    mailSender.Variables.Add("lastname", usr.lastName);
                    mailSender.Variables.Add("firstname", usr.firstName);
                    mailSender.Variables.Add("knowus", orgknowus);
                    
                    
                    mailSender.SendSmartHtmlEmail("ges_01_newact.html", "activation");
                }
            }
            catch { }

            return true;
        }

    }
}
