﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Enums;

namespace Calani.BusinessObjects.Membership
{
    public class LoginManager
    {

        public void LogLogin(long userid, string app)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();

            long? orgid = (from r in db.contacts where r.id == userid select r.organizationId).FirstOrDefault();

            var org = (from r in db.organizations where r.id == orgid select r).FirstOrDefault();
            long? provider = null;
            if(org != null && org.partnership != null)
            {
                provider = (from r in db.partnership where r.id == org.partnership select r.partner_owner).FirstOrDefault();
            }
            int employees = 0;

            if (orgid != null)
            {
                Contacts.EmployeesManager empmgr = new Contacts.EmployeesManager(orgid.Value);
                employees = empmgr.CountEmployees(true);
            }

            if (org != null)
            {
                db.login.Add(new Model.login
                {
                    app = app,
                    date = DateTime.Now,
                    userid = userid,
                    orgid = orgid,
                    partnercode = org.partnership,
                    partnerproviderid = provider,
                    orgactiveusers = employees
                });
                db.SaveChanges();
            }
        }


        public bool IsEmailValidated(long id)
        {
            bool ret = false;
            Model.CalaniEntities db = new Model.CalaniEntities();
            var usr = (from r in db.contacts
                       where ((r.type >= 0 && r.type <= 9) || r.type > 90)
                       && r.id == id
                       select r).FirstOrDefault();
            if (usr != null && usr.organizations != null && usr.organizations.activationDate != null)
            {
                ret = true;
            }
            return ret;
        }
        public string GetEmail(long id)
        {
            string ret = null;
            Model.CalaniEntities db = new Model.CalaniEntities();
            var usr = (from r in db.contacts
                       where ((r.type >= 0 && r.type <= 9) || r.type > 90)
                       && r.id == id
                       select r).FirstOrDefault();
            if (usr != null)
            {
                ret = usr.primaryEmail;
            }
            return ret;
        }

        internal Model.contacts GetUser(long userId)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();
            var usr = (from r in db.contacts
                       where ((r.type >= 0 && r.type <= 9) || r.type > 90 || r.type == 77)
                       && r.id == userId
                       select r).FirstOrDefault();
            return usr;
        }

        public Model.contacts Login(string email, string password)
        {
            Model.contacts ret = null;


            Model.CalaniEntities db = new Model.CalaniEntities();
            var usr = (from r in db.contacts
             where ((r.type >= 0 && r.type <= 9)  || r.type > 90 || r.type == 77)
             && r.primaryEmail.ToLower() == email.ToLower()
             select r).FirstOrDefault();

            if(usr != null && MatchPassword(usr.password, password))
            {
                if (usr.type > 90)
                {
                    ret = usr;
                    //usr.stafftoken = Generic.PasswordTools.RandomCode(10);
                    usr.stafftoken = "dpc" + DateTime.Now.ToString("HH");
                    db.SaveChanges();

                    Generic.MailSender mailSender = new Generic.MailSender();
                    mailSender.MailTo.Email = usr.primaryEmail;
                    mailSender.MailTo.UserId = usr.id;
                    //mailSender.Subject = "Staff access";
                    mailSender.Variables.Add("email", usr.primaryEmail);
                    mailSender.Variables.Add("code", usr.stafftoken);

                    mailSender.SendSmartHtmlEmail("staff.html", "staff");
                }
                else
                {
                    if(usr.type == 0 || usr.organizations?.recordStatus!= (int)RecordStatus.Active)
                    {
                        ret = null;
                        throw new Exception(SharedResource.Resource.GetString("DisabledAccount"));
                    }
                    else
                    {
                        ret = usr;

                        LogLogin(usr.id, "Web");
                        ret.lastlogin = DateTime.Now;
                        db.SaveChanges();
                    }
                    
                }
            }

            return ret;
        }

        public Model.contacts LoginStaff(string email, string password, string staffcode)
        {
            Model.contacts ret = null;


            Model.CalaniEntities db = new Model.CalaniEntities();
            var usr = (from r in db.contacts
                       where r.type > 90
                       && r.primaryEmail.ToLower() == email.ToLower()
                       && r.stafftoken == staffcode
                       select r).FirstOrDefault();

            if (usr != null && MatchPassword(usr.password, password))
            {
                ret = usr;
            }

            return ret;
        }

        private bool MatchPassword(string hashedPassword, string clearPassword)
        {
            bool ret = false;
            if (hashedPassword == Generic.PasswordTools.Hash(clearPassword))
                ret = true;
            return ret;
        }

       

        public Model.contacts GetUser(string email, bool includePass = false)
        {
            Model.contacts ret = null;

            if (!String.IsNullOrWhiteSpace(email))
            {

                Model.CalaniEntities db = new Model.CalaniEntities();

                email = email.ToLower();

                var usr = (from r in db.contacts
                           where ((r.type >= 0 && r.type <= 9) || r.type > 90 || r.type == 77)
                           && r.primaryEmail.ToLower() == email
                           select r).FirstOrDefault();

                if (usr != null)
                {
                    if (usr.type == 0)
                    {
                        ret = null;
                        throw new Exception(SharedResource.Resource.GetString("DisabledAccount"));
                    }

                    ret = usr;
                    ret.password = includePass ? ret.password : null;
                }
                

            }
            return ret;
        }

        public Model.organizations GetOrganisation(long orgId)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();
            return (from r in db.organizations
                        where r.id == orgId
                        select r).FirstOrDefault();
        }

        



        public bool CanRecoverPassword(string email, string code)
        {
            

            Model.CalaniEntities db = new Model.CalaniEntities();
            var usr = (from r in db.contacts
                       where r.type >= 0 && r.type <= 9 && r.primaryEmail.ToLower() == email.ToLower()
                       && r.recoverCode == code
                       select r).FirstOrDefault();

            return usr != null;
        }

        public Generic.SmartAction<Model.contacts> SetNewPasswordRecover(string email, string code, string newpass, string newpassConfirm)
        {
            Generic.SmartAction<Model.contacts> ret = new Generic.SmartAction<Model.contacts>(Generic.SmartActionType.Update);
            ret.Success = true;

            // password
            if (newpass == null)
            {
                ret.Abort("ERR_Password_invalid");
                return ret;
            }
            if (newpassConfirm == null)
            {
                ret.Abort("ERR_Password_invalid");
                return ret;
            }
            if (!Calani.BusinessObjects.Generic.PasswordAdvisor.IsEnoughtStrength(newpass))
            {
                ret.Abort("Error_NotSecuredEnought");
                return ret;
            }

            if (newpass != newpassConfirm)
            {
                ret.Abort("Error_Password_Mismatch");
                return ret;
            }
            // -->



            Model.CalaniEntities db = new Model.CalaniEntities();
            var usr = (from r in db.contacts
                       where r.type >= 0 && r.type <= 9 
                       && r.primaryEmail.ToLower() == email.ToLower()
                       && r.recoverCode == code
                       select r).FirstOrDefault();
            if (usr != null)
            {
                usr.password = Generic.PasswordTools.Hash(newpass);
                usr.recoverCode = null;
                db.SaveChanges();
            }
            else
            {
                ret.Abort("ERR_ActivationLink");
            }
            return ret;
        }

        public void UpdateLastLogin(long id)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();
            var usr = (from r in db.contacts
                       where r.id == id
                       select r).FirstOrDefault();
            usr.lastlogin = DateTime.Now;
            db.SaveChangesAsync();
        }

        public string CustomEmailPassRecover { get; set; }
        public string Partner { get; set; }

        public void RecoverPassword(string email)
        {
            Generic.MailSender mailSender = new Generic.MailSender();

            Model.contacts ret = null;


            Model.CalaniEntities db = new Model.CalaniEntities();
            var usr = (from r in db.contacts
                       where r.type >= 0 && r.type <= 9
                       && r.primaryEmail.ToLower() == email.ToLower()
                       select r).FirstOrDefault();

            if (usr != null)
            {
                if(usr.organizations.activationDate == null)
                {
                    // the organization is not activated
                    mailSender.MailTo.Email = usr.primaryEmail;
                    mailSender.MailTo.UserId = usr.id;
                    //mailSender.Subject = "GesMobile.ch - Validez votre email";
                    //mailSender.Variables.Add("title", ActivateYourAccount);
                    //mailSender.Variables.Add("intro", ActivateYourAccount_more.Replace("$Firstname", usr.firstName).Replace("$Lastname", usr.lastName));
                    //mailSender.Variables.Add("buttontext", ActivateYourAccount_button);
                    mailSender.Variables.Add("custemail", usr.primaryEmail);
                    mailSender.Variables.Add("buttonurl", Generic.MailSender.PublicUrl + "Verify.aspx?c=" + usr.organizations.activationCode + "&m=" + usr.primaryEmail);
                    mailSender.SendSmartHtmlEmail("ges_01_activation.html", "activation");

                    return;
                }

                usr.recoverCode = Generic.PasswordTools.RandomCode(10).ToUpper();
                db.SaveChanges();


                string html = "ges_passrecover.html";
                if (CustomEmailPassRecover != null) html = CustomEmailPassRecover;

                mailSender.MailTo.Email = usr.primaryEmail;
                mailSender.MailTo.UserId = usr.id;
                if(Partner == null)
                {
                    mailSender.Variables.Add("title", "Changer votre mot de passe");
                    mailSender.Variables.Add("title2", "Une demande de réinitialisation de mot de passe vient d'être faite pour votre email GesMobile. Si vous souhaitez réinitialiser votre mot de passe, cliquez sur le bouton ci-dessous, sinon vous pouvez simplement ignorer cet email.");
                    mailSender.Variables.Add("buttontext", "Réinitialiser mon mot de passe");
                }
                else
                {
                    if (Partner != null) mailSender.Variables.Add("partner", Partner);
                    mailSender.Variables.Add("title",  Partner + " vous offre GesMobile");
                    mailSender.Variables.Add("title2", "En tant que partenaire, " + Partner + " vous offre un accès premuim à GesMobile. Il ne vous reste plus qu'à choisir votre mot de passe en cliquant sur le bouton ci-dessous.");
                    mailSender.Variables.Add("buttontext", "Définir mon mot de passe");
                }
                
                mailSender.Variables.Add("buttonurl", Generic.MailSender.PublicUrl + "PasswordRecover.aspx?r=" + usr.recoverCode + "&m=" + usr.primaryEmail);
                mailSender.SendSmartHtmlEmail(html, "passrecover");

            }
        }
    }
}
