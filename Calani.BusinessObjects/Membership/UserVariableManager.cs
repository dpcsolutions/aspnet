﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Membership
{
    public class UserVariableManager
    {

        long user;

        public UserVariableManager(string useremail, long org)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();
            if (!String.IsNullOrWhiteSpace(useremail))
            {
                var rec = (from r in db.contacts where r.primaryEmail == useremail && r.organizationId == org && r.type >= 0 && r.type <= 9 select r).FirstOrDefault();
                if (rec != null)
                {
                    this.user = rec.id;
                }
            }
        }

        public UserVariableManager(long user)
        {
            this.user = user;
        }

        public string Load(string varname, string defaultvalue)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();
            string ret = defaultvalue;
            if (user > 0)
            {
                ret = (from r in db.uservariables where r.user == user && r.name == varname select r.value).FirstOrDefault();
                if (String.IsNullOrEmpty(ret))
                    ret = defaultvalue;
            }
            return ret;
        }

        public void Save(string varname, string value)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();
            var rec = (from r in db.uservariables where r.user == user && r.name == varname select r).FirstOrDefault();
            if (rec == null)
            {
                rec = new Model.uservariables { user = user, name = varname };
                db.uservariables.Add(rec);
            }
            rec.value = value;
            db.SaveChanges();
        }

    }
}
