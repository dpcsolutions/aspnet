﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Membership
{
    public class DeletionManager
    {
        public long OrgId { get; set; }
        public DeletionManager(long orgid)
        {
            OrgId = orgid;
        }

        Model.CalaniEntities db;

        List<Model.addresses> delAddresses;
        List<Model.attachments> delAttachments;
        List<Model.bankaccount> delBankAccount;
        List<Model.clients> delClients;
        List<Model.contacts> delContacts;
        List<Model.documentemplates> delDocTemplates;
        List<Model.expenses> delExpenses;
        List<Model.jobs> delJobs;
        List<Model.notes> delNotes;
        List<Model.payments> delPayments;
        List<Model.services> delServices;
        List<Model.services_jobs> delServicesJobs;
        List<Model.taxes> delTaxes;
        List<Model.timesheetrecords> delTimeSheetRecords;
        List<Model.timesheetrecords> delTimeSheetRecords2;
        List<Model.timesheets> delTimeSheets;
        List<Model.uservariables> delUserVariables;
        List<Model.visits> delVisits;


        private void Init()
        {
            db = new Model.CalaniEntities();

            List<long> remUsers = (from r in db.contacts where r.organizationId == OrgId && r.type < 10 select r.id).ToList();


            delAddresses = (from r in db.addresses where r.organizationId == OrgId select r).ToList();
            delAttachments = (from r in db.attachments where r.organizationId == OrgId select r).ToList();
            delBankAccount = (from r in db.bankaccount where r.organizationId == OrgId select r).ToList();
            delClients = (from r in db.clients where r.organizationId == OrgId select r).ToList();
            delContacts = (from r in db.contacts where r.organizationId == OrgId select r).ToList();
            delDocTemplates = (from r in db.documentemplates where r.userId != null && remUsers.Contains(r.userId.Value) select r).ToList();
            delExpenses = (from r in db.expenses where r.employeeId != null && remUsers.Contains(r.employeeId.Value) select r).ToList();
            delJobs = (from r in db.jobs where r.organizationId == OrgId select r).ToList();
            delNotes = (from r in db.notes where r.jobs != null && r.jobs.organizationId == OrgId select r).ToList();
            delPayments = (from r in db.payments where r.jobId != null && r.jobs.organizationId == OrgId select r).ToList();
            delServices = (from r in db.services where r.organizationId == OrgId select r).ToList();
            delServicesJobs = (from r in db.services_jobs where r.jobs != null && r.jobs.organizationId == OrgId select r).ToList();
            delTaxes = (from r in db.taxes where r.organizationId == OrgId select r).ToList();
            delTimeSheetRecords = (from r in db.timesheetrecords where r.timesheets != null && r.timesheets.organizationId == OrgId select r).ToList();
            delTimeSheetRecords2 = (from r in db.timesheetrecords where r.lastModifiedBy != null && remUsers.Contains(r.lastModifiedBy.Value) select r).ToList();
            delTimeSheets = (from r in db.timesheets where r.organizationId == OrgId select r).ToList();
            delUserVariables = (from r in db.uservariables where r.user != null && remUsers.Contains(r.user.Value) select r).ToList();
            delVisits = (from r in db.visits where r.orgId == OrgId select r).ToList();


            foreach (var c in delContacts)
            {
                delVisits.AddRange(c.visits.ToList());
            }
            foreach (var s in delServices)
            {
                delVisits.AddRange((from r in db.visits where r.serviceId == s.id select r).ToList());
                delTimeSheetRecords2.AddRange((from r in db.timesheetrecords where r.serviceId == s.id select r).ToList());
            }


        }

        public void Delete()
        {
            Init();

            var org = (from r in db.organizations where r.id == OrgId select r).FirstOrDefault();
            org.owner = null;
            db.SaveChanges();

            for (int i = 0; i < 10; i++)
            {



                try
                {
                    foreach (var x in delAddresses)
                    {
                        if (x.organizationId == OrgId)
                        {
                            db.addresses.Remove(x);
                        }

                    }
                    db.SaveChanges();
                }
                catch
                {
                    Init();
                }

                try
                {
                    foreach (var x in delAttachments)
                    {
                        if (x.organizationId == OrgId)
                        {
                            db.attachments.Remove(x);
                        }
                    }
                    db.SaveChanges();
                }
                catch
                {
                    Init();
                }

                try
                {
                    foreach (var x in delBankAccount)
                    {
                        if (x.organizationId == OrgId)
                        {
                            db.bankaccount.Remove(x);

                        }
                    }
                    db.SaveChanges();
                }
                catch
                {
                    Init();
                }

                try
                {
                    foreach (var x in delDocTemplates)
                    {
                        if (x.contacts != null && x.contacts.organizationId == OrgId)
                        {
                            db.documentemplates.Remove(x);
                        }
                    }
                    db.SaveChanges();
                }
                catch
                {
                    Init();
                }

                try
                {
                    foreach (var x in delExpenses)
                    {
                        if (x.contacts != null && x.contacts.organizationId == OrgId)
                            db.expenses.Remove(x);
                        db.SaveChanges();
                    }
                }
                catch
                {
                    Init();
                }


                try
                {
                    foreach (var x in delNotes)
                    {
                        if ((x.jobs != null && x.jobs.organizationId == OrgId) || (x.contacts != null && x.contacts.organizationId == OrgId))
                        {
                            db.notes.Remove(x);
                            db.SaveChanges();
                        }
                    }
                }
                catch
                {
                    Init();
                }

                try
                {
                    foreach (var x in delPayments)
                    {
                        if (x.jobs != null && x.jobs.organizationId == OrgId)
                        {
                            db.payments.Remove(x);
                            db.SaveChanges();
                        }
                    }
                }
                catch
                {
                    Init();
                }

                try
                {
                    foreach (var x in delUserVariables)
                    {
                        db.uservariables.Remove(x);
                        db.SaveChanges();
                    }
                }
                catch
                {
                    Init();
                }

                try
                {
                    foreach (var x in delServicesJobs)
                    {
                        if ((x.jobs != null && x.jobs.organizationId == OrgId) || (x.services != null && x.services.organizationId == OrgId))
                        {
                            db.services_jobs.Remove(x);
                            db.SaveChanges();
                        }
                    }
                }
                catch
                {
                    Init();
                }

                try
                {
                    foreach (var x in delTaxes)
                    {
                        if (x.organizationId == OrgId)
                        {
                            db.taxes.Remove(x);
                            db.SaveChanges();
                        }
                    }
                }
                catch
                {
                    Init();
                }

                try
                {
                    foreach (var x in delTimeSheetRecords2)
                    {
                        if ((x.services != null && x.services.organizationId == OrgId) || (x.contacts != null && x.contacts.organizationId == OrgId) || (x.jobs != null && x.jobs.organizationId == OrgId))
                        {

                            db.timesheetrecords.Remove(x);
                            db.SaveChanges();
                        }
                    }
                }
                catch
                {
                    Init();
                }

                try
                {
                    foreach (var x in delTimeSheetRecords)
                    {
                        if ((x.timesheets != null && x.timesheets.organizationId == OrgId) || (x.jobs != null && x.jobs.organizationId == OrgId) || (x.contacts != null && x.contacts.organizationId == OrgId))
                            db.timesheetrecords.Remove(x);
                        db.SaveChanges();
                    }
                }
                catch
                {
                    Init();
                }

                try
                {
                    foreach (var x in delVisits)
                    {
                        var orgs = (from r in x.contacts select r.organizationId).ToList();
                        if ((x.jobs != null && x.jobs.organizationId == OrgId) || orgs.Contains(OrgId))
                        {
                            x.jobId = null;
                            db.SaveChanges();
                        }
                    }
                }
                catch
                {
                    Init();
                }

                try
                {
                    foreach (var x in delVisits)
                    {
                        if (x.orgId == OrgId)
                        {
                            db.visits.Remove(x);
                            db.SaveChanges();
                        }
                    }
                }
                catch
                {
                    Init();
                }


                try
                {
                    foreach (var x in delJobs)
                    {
                        if (x.organizationId == OrgId)
                        {
                            db.jobs.Remove(x);
                            db.SaveChanges();
                        }
                    }
                }
                catch
                {
                    Init();
                }

                try
                {
                    foreach (var x in delClients)
                    {
                        if (x.organizationId == OrgId)
                        {
                            db.clients.Remove(x);
                            db.SaveChanges();
                        }
                    }

                }
                catch
                {
                    Init();
                }



                try
                {

                    foreach (var x in delTimeSheets)
                    {
                        if (x.organizationId == OrgId)
                        {
                            db.timesheets.Remove(x);
                            db.SaveChanges();
                        }
                    }
                }
                catch
                {
                    Init();
                }



                try
                {
                    foreach (var x in delContacts)
                    {
                        if (x.organizationId == OrgId)
                        {
                            db.contacts.Remove(x);
                            db.SaveChanges();
                        }
                    }
                }
                catch
                {
                    Init();
                }





                try
                {
                    foreach (var x in delServices)
                    {
                        if (x.organizationId == OrgId)
                        {
                            db.services.Remove(x);
                            db.SaveChanges();
                        }
                    }
                }
                catch
                {
                    Init();
                }


                try
                {
                    db.organizations.Remove(org);
                    db.SaveChanges();
                    return;
                }
                catch
                {

                }


            }

        }





    }
}

