﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Membership
{
    public class UserCultureManager
    {
        public static bool SaveUserCulture(long userId, string culture)
        {
            bool ret = true;

            Model.CalaniEntities db = new Model.CalaniEntities();
            var usr = (from r in db.contacts where r.id == userId select r).FirstOrDefault();
            if(usr != null)
            {
                if(usr.culture != culture)
                {
                    usr.culture = culture;
                    db.SaveChanges();
                    ret = true;
                }
            }

            return ret;
        }

        public static string GetUserCulture(long userId)
        {
            string culture = null;

            Model.CalaniEntities db = new Model.CalaniEntities();
            var usr = (from r in db.contacts where r.id == userId && r.culture != null select r).FirstOrDefault();
            if (usr != null)
            {
                culture = usr.culture;
            }

            return culture;
        }
    }
}
