﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.TimeManagement
{
    public enum TimeTypeEnum
    {
        Visit,
        Internal
    }

    public enum TimeSheetType
    {
        Simple = 0,
        DetailedFullWeek = 1,
        DetailedWorkDays = 2
    }
    
}
