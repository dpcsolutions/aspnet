﻿using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using log4net;

namespace Calani.BusinessObjects.TimeManagement
{
    public class TimeSheetsManager : Generic.SmartCollectionManager<Model.timesheets, long>
    {
        public int DaysToFinishWeek { get; set; }
        public TimeSheetsManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;

            DaysToFinishWeek = 3; // todo : settings de l'org
        }

        public Model.timesheets GetTimeSheet(long currentUser, int year, int week)
        {
            Model.timesheets ret = null;

            var q = base.GetBaseQuery();
            q.Query = (from r in q.Context.timesheets
                       where r.year == year
                        && r.week == week
                        && r.employeeId == currentUser
                       select r);

            ret = q.Query.FirstOrDefault();

            return ret;
        }

        internal override DbSet<timesheets> GetDbSet(Model.CalaniEntities db)
        {
            return db.timesheets;
        }

        internal override SmartQuery<timesheets> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.timesheets where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(timesheets record)
        {
            if (record != null && record.organizationId != null) return record.organizationId.Value;
            return -1;
        }

        internal override SmartQuery<timesheets> GetBaseQuery()
        {
            var q = base.GetBaseQuery();
            // base
            q.Query = (from r in q.Context.timesheets select r);

            if (OrganizationID > -1) 
                q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);

            if (!IgnoreRecordStatus) 
                q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) 
                q.Query = (from r in q.Query orderby r.year descending, r.week descending select r);


            return q;
        }


        public List<timesheetsExt> List(out double? totalHours, bool? alreadyValidated, DateTime? dtFrom = null, DateTime? dtTo = null, long? employee = null)
        {
            int startYear = -1, startWeek = -1, endYear = -1, endWeek = -1;
            
            if (dtFrom != null)
            {
                startYear = dtFrom.Value.Year;
                startWeek = CalendarTools.GetNonIsoWeekOfYear(dtFrom.Value);
            }

            if (dtTo != null){
                endYear = dtTo.Value.Year;
                endWeek = CalendarTools.GetNonIsoWeekOfYear(dtTo.Value);
            }
            
            var dataBase = new CalaniEntities().Database;
            var orgMgr = new OrganizationManager(OrganizationID);
            orgMgr.Load();
            
            var ret = dataBase.SqlQuery<timesheetsExt>(
                "call ListTimesheetsExtForManager(@p0,@p1,@p2,@p3,@p4,@p5,@p6,@p7)",
                    OrganizationID, alreadyValidated, startWeek, startYear, endWeek, endYear, employee ?? -1,orgMgr.TimeSheetType
            ).ToList();
            
            totalHours = ret.Sum(x => x.TotalDuration);
            return ret;
        }
        
        internal override void BeforeAction(SmartAction<timesheets> action)
        {

        }


        public static string GetLastChangeStatus(timesheets ts, System.Globalization.CultureInfo c)
        {
            if(ts != null && ts.sentDate != null)
            {
                return SharedResource.Resource.GetString("LastChange", c) + " " + ts.sentDate.Value.ToString("dd/MM/yyyy HH:mm:ss");
            }
            else
            {
                return SharedResource.Resource.GetString("WaitingForChanges", c);
            }
        }

        public static string GetValidationStatus(timesheets ts, System.Globalization.CultureInfo c)
        {
            if (ts != null && ts.acceptedDate != null)
            {
                return SharedResource.Resource.GetString("WasValidated", c) + " : " + ts.acceptedDate.Value.ToString("dd/MM/yyyy HH:mm:ss");
            }
            else
            {
                return SharedResource.Resource.GetString("NotValidatedYet", c);
            }
        }


        public List<TimeSheetItemsList> GetOrgTimeSheets()
        {
            var bq = GetBaseQuery();
            var res = bq.Query.Select(r => new TimeSheetItemsList()
            {
                OrganizationId = r.organizationId??0, 
                TimeSheetId = r.id,
                Employee = r.employeeId??0,
                Year = r.year??0,
                Week = r.week??0,
                
            }).ToList();

            return res;
        }
    }
}
