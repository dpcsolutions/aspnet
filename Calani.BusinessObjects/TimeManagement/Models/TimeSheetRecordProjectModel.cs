﻿using System.Runtime.Serialization;

namespace Calani.BusinessObjects.TimeManagement.Models
{
    [DataContract]
    public class TimeSheetRecordProjectModel
    {
        [DataMember]
        public string InternalId { get; set; }

        [DataMember]
        public long? ProjectId { get; set; }

        public TimeSheetRecordProjectModel(timesheetrecordsExt entity)
        {
            InternalId = entity.internalId;
            ProjectId = entity.projectId;
        }
    }
}
