﻿using Calani.BusinessObjects.Generic;
using System;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;

namespace Calani.BusinessObjects.TimeManagement.Models
{
    [DataContract]
    public class TimeSheetRecordModel
    {

        [DataMember]
        public long Id { get; set; }

        [DataMember]
        public TimeSheetRecordInvoiceModel Invoice { get; set; }

        [DataMember]
        public long? TimeSheetId { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public string StartDateStr { get; set; }

        [DataMember]
        public string Duration { get; set; }

        [DataMember]
        public string EmployeeName { get; set; }

        [DataMember]
        public string ServiceName { get; set; }

        [DataMember]
        public string Comment { get; set; }

        [DataMember]
        public long[] GroupedIds { get; set; }

        [DataMember]
        public TimeSheetRecordClientModel Client { get; set; }

        [DataMember]
        public TimeSheetRecordProjectModel Project { get; set; }

        [DataMember]
        public TimeSheetRecordImportedModel Imported { get; set; }

        public TimeSheetRecordModel(timesheetrecordsExt entity)
        {
            Id = entity.id;
            TimeSheetId = entity.timesheetId;
            StartDate = entity.startDate;
            StartDateStr = entity.startDate.HasValue ? entity.startDate.Value.ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture) : "";
            Duration = entity.durationStr;
            EmployeeName = entity.employeeName;
            ServiceName = entity.serviceName;
            Comment = entity.comment;
            GroupedIds = (entity.groupedIds ?? string.Empty).Split(',').Select(s => Int64.Parse(s)).ToArray();

            Client = new TimeSheetRecordClientModel(entity);
            Imported = new TimeSheetRecordImportedModel(entity);
            Project = new TimeSheetRecordProjectModel(entity);
            Invoice = new TimeSheetRecordInvoiceModel(entity);
        }
    }
}
