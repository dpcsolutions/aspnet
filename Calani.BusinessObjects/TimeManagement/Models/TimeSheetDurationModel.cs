﻿using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace Calani.BusinessObjects.TimeManagement.Models
{
    [DataContract]
    public class TimeSheetDurationModel
    {
        [DataMember]
        public double TotalDuration { get; set; }

        [DataMember]
        public string TotalDurationStr { get; set; }

        [DataMember]
        public double? AttendedDuration { get; set; }

        [DataMember]
        public string AttendedDurationStr { get; set; }

        [DataMember]
        public bool DisplayWarning { get; set; }

        public static string FormatDuration(double? durationHours)
        {
            if (!durationHours.HasValue) return string.Empty;
            var time = TimeSpan.FromHours(durationHours.Value);

            var totalHours = (int)time.TotalHours;
            var minutes = time.Minutes;
            if (time.Seconds >= 30) minutes++;

            if (minutes == 60)
            {
                totalHours++;
                minutes = 0;
            }

            return $"{totalHours:D2}:{minutes:D2}";
        }
    }
}
