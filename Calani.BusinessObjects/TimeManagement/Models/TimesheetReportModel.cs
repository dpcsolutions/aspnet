﻿using System.Text;

namespace Calani.BusinessObjects.TimeManagement.Models
{
    public class TimesheetReportModel
    {
        public decimal TotalHoursWorked { get; set; }
        public decimal TotalOvertimeHours { get; set; }
        public decimal OvertimeRate1Hours { get; set; }
        public decimal OvertimeRate2Hours { get; set; }
        public decimal TotalHoursToBeCompensated { get; set; }
        /// <summary>
        /// What admin entered manually + dynamic adjustments
        /// </summary>
        public decimal TotalHoursEffectivelyCompensated { get; set; }
        /// <summary>
        /// What admin entered manually + dynamic adjustments
        /// </summary>
        public decimal TotalHoursActuallyPaid { get; set; }
        /// <summary>
        /// What admin entered manually; stored in DB contacts_overtime
        /// </summary>
        public decimal TotalHoursEffectivelyCompensatedEnteredByAdmin { get; set; }
        /// <summary>
        /// What admin entered manually; stored in DB contacts_overtime
        /// </summary>
        public decimal TotalHoursActuallyPaidEnteredByAdmin { get; set; }

        public string Summary { get; set; }

        public void SumUpReport(TimesheetReportModel report)
        {
            TotalHoursWorked += report.TotalHoursWorked;
            TotalOvertimeHours += report.TotalOvertimeHours;
            OvertimeRate1Hours += report.OvertimeRate1Hours;
            OvertimeRate2Hours += report.OvertimeRate2Hours;
            TotalHoursToBeCompensated += report.TotalHoursToBeCompensated;

            var sb = new StringBuilder(Summary);
            sb.AppendLine(report.Summary);
            Summary = sb.ToString();
        }
    }
}
