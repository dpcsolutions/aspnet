﻿using System;
using System.Runtime.Serialization;

namespace Calani.BusinessObjects.TimeManagement.Models
{
    [DataContract]
    public class TimeSheetStatusModel
    {
        [DataMember]
        public DateTime? AcceptedDate { get; set; }

        [DataMember]
        public DateTime? RejectedDate { get; set; }

        [DataMember]
        public string Status { get; set; }
    }
}
