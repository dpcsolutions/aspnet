﻿using System.Runtime.Serialization;

namespace Calani.BusinessObjects.TimeManagement.Models
{
    [DataContract]
    public class TimeSheetRecordImportedModel
    {
        [DataMember]
        public long? ClientId { get; set; }

        [DataMember]
        public bool Imported { get; set; }

        public TimeSheetRecordImportedModel(timesheetrecordsExt entity)
        {
            ClientId = entity.clientId;
            Imported = entity.imported;
        }
    }
}
