﻿using System.Runtime.Serialization;

namespace Calani.BusinessObjects.TimeManagement.Models
{
    [DataContract]
    public class TimeSheetRecordInvoiceModel
    {
        [DataMember]
        public string InternalId { get; set; }

        [DataMember]
        public long? Id { get; set; }

        public TimeSheetRecordInvoiceModel(timesheetrecordsExt entity)
        {
            InternalId = entity.InvoiceInternalId;
            Id = entity.InvoiceId;
        }
    }
}
