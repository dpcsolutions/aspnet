﻿using System.Runtime.Serialization;

namespace Calani.BusinessObjects.TimeManagement.Models
{
    [DataContract]
    public class TimeSheetRecordsInvoiceModel
    {
        [DataMember]
        public long ClientId { get; set; }

        [DataMember]
        public long ProjectId { get; set; }

        [DataMember]
        public long[] TimeSheetRecordsIds { get; set; }

    }
}
