﻿using Calani.BusinessObjects.AjaxService.Models;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace Calani.BusinessObjects.TimeManagement.Models
{
    [DataContract]
    public class TimeSheetModel
    {
        [DataMember] public long Id { get; set; }

        [DataMember] public bool IsWarning { get; set; }

        [DataMember] public long? EmployeeId { get; set; }

        [DataMember] public string EmployeeName { get; set; }

        [DataMember] public string EmployeeInitials { get; set; }
        
        [DataMember] public string Comments { get; set; }
        [DataMember] public int CommentNumber { get; set; }

        [DataMember] public int? Year { get; set; }

        [DataMember] public int? Week { get; set; }

        [DataMember] public TimeSheetDurationModel Duration { get; set; }

        [DataMember] public DateRangeModel Range { get; set; }

        [DataMember] public string RangeStr { get; set; }

        [DataMember] public DateTime? SheetDate { get; set; }

        [DataMember] public TimeSheetStatusModel Status { get; set; }

        public TimeSheetModel(timesheetsExt entity, bool useWorkSchedule, TimesheetTypeEnum timesheetType)
        {
            Id = entity.id;
            IsWarning = entity.is_warning;
            EmployeeId = entity.employeeId;
            EmployeeName = entity.EmployeeName;
            EmployeeInitials = entity.EmployeeInitials;
            Year = entity.year;
            Week = entity.week;
            SheetDate = entity.sheetDate;
            
            Comments = entity.comment;
            CommentNumber = string.IsNullOrEmpty(entity.comment) ? entity.CommentNumber ?? 0 : entity.CommentNumber ?? 0 + 1;
            
            Duration = new TimeSheetDurationModel
            {
                TotalDuration = entity.TotalDuration,
                AttendedDuration = entity.AttendedDuration,
                TotalDurationStr = TimeSheetDurationModel.FormatDuration(entity.TotalDuration),
                AttendedDurationStr = TimeSheetDurationModel.FormatDuration(entity.AttendedDuration),
                DisplayWarning = DoDisplayWarning(entity, useWorkSchedule, timesheetType),
            };

            Status = new TimeSheetStatusModel
            {
                AcceptedDate = entity.acceptedDate,
                RejectedDate = entity.rejectDate,
                Status = entity.sheetStatus
            };
            
            SetDates(timesheetType);
        }

        public DateRange GetStartAndEndDates()
        {
            return new DateRange(
                Generic.CalendarTools.GetIso8601MondayOfWeek(Year.Value, Week.Value),
                Generic.CalendarTools.GetIso8601SundayOfWeek(Year.Value, Week.Value));
        }

        private void SetDates(TimesheetTypeEnum timesheetType)
        {
            if(Year.HasValue && Week.HasValue)
            {
                int weekLength = DaysInWeekForTemplate(timesheetType);

                var startDate = FirstDateOfWeekISO8601(Year.Value, Week.Value);
                var endDate = startDate.AddDays(weekLength);

                Range = new DateRangeModel
                {
                    StartDate = startDate,
                    EndDate = endDate
                };

                RangeStr = $"{startDate.ToString("dd/MM/yyyy")} - {endDate.ToString("dd/MM/yyyy")}";
            }
        }

        public static int DaysInWeekForTemplate(TimesheetTypeEnum timesheetType)
        {
            int weekLength = 6;
            switch (timesheetType)
            {
                case TimesheetTypeEnum.DetailedWorkDays:
                    weekLength = 4;
                    break;
                case TimesheetTypeEnum.SimplifiedEntry:
                    weekLength = 5;
                    break;
            }

            return weekLength;
        }

        public static DateTime FirstDateOfWeekISO8601(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            // Use first Thursday in January to get first week of the year as
            // it will never be in Week 52/53
            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            // As we're adding days to a date in Week 1,
            // we need to subtract 1 in order to get the right date for week #1
            if (firstWeek == 1)
            {
                weekNum -= 1;
            }

            // Using the first Thursday as starting week ensures that we are starting in the right year
            // then we add number of weeks multiplied with days
            var result = firstThursday.AddDays(weekNum * 7);

            // Subtract 3 days from Thursday to get Monday, which is the first weekday in ISO8601
            return result.AddDays(-3);
        }

        private bool DoDisplayWarning(timesheetsExt entity, bool useWorkSchedule, TimesheetTypeEnum timesheetType)
        {
            if (!entity.AttendedDuration.HasValue || !useWorkSchedule)
            {
                return false;
            }

            bool displayWarning = false;

            switch (timesheetType)
            {
                case TimesheetTypeEnum.SimplifiedEntry:
                    var oneMin = (double)1 / 60;
                    displayWarning = !(Math.Abs(entity.AttendedDuration.Value - entity.TotalDuration) < oneMin);
                    break;
                default:
                    displayWarning = Math.Abs(entity.AttendedDuration.Value - entity.TotalDuration) > 5;
                    break;
            }

            return displayWarning;
        }
    }
}
