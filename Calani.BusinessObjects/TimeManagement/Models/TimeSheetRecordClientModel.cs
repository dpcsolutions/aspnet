﻿using System.Runtime.Serialization;

namespace Calani.BusinessObjects.TimeManagement.Models
{
    [DataContract]
    public class TimeSheetRecordClientModel
    {
        [DataMember]
        public long? Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        public TimeSheetRecordClientModel(timesheetrecordsExt entity)
        {
            Id = entity.clientId;
            Name = entity.companyName;
        }
    }
}
