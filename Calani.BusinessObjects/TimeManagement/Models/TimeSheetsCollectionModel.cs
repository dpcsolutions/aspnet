﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Calani.BusinessObjects.TimeManagement.Models
{
    [DataContract]
    public class TimeSheetsCollectionModel
    {
        [DataMember(Name = "data")]
        public List<TimeSheetModel> Data { get; set; }
    }
}
