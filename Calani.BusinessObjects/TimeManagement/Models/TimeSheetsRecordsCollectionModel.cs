﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Calani.BusinessObjects.TimeManagement.Models
{
    [DataContract]
    public class TimeSheetsRecordsCollectionModel
    {
        [DataMember(Name = "data")]
        public List<TimeSheetRecordModel> Data { get; set; }
    }
}
