﻿using System;
using System.Data;
using System.Linq;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.TimeManagement
{
    public class TimeSheetsExtractor
    {
        public long OrganizationId { get; private set; }


        public TimeSheetsExtractor(long organizationId)
        {
            OrganizationId = organizationId;
        }

        public byte[] Extract(DateTime fromDate, DateTime toDate, long? employeeId = null)
        {
            DocGenerator.ExcelTableGenerator gen = new DocGenerator.ExcelTableGenerator();
            gen.Borders = true;
            gen.FitColumns = true;

            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Columns.Add("Employé");
            dt.Columns.Add("Année");
            dt.Columns.Add("Semaine");
            dt.Columns.Add("Début");
            dt.Columns.Add("Fin");
            dt.Columns.Add("Catégorie de temps");
            dt.Columns.Add("Taux");
            dt.Columns.Add("Durée");
            dt.Columns.Add("Projet");
            dt.Columns.Add("Client");
            dt.Columns.Add("Service");
            dt.Columns.Add("Prix du produit");
            dt.Columns.Add("Numéro de référence");
            dt.Columns.Add("Сommentaire");
            
            Model.CalaniEntities db = new Model.CalaniEntities();

            var q = (from r
                     in db.timesheetrecords
                     .Include("timesheets").Include("timesheets.contacts").Include("work_schedule_profiles_rec")
                     where r.timesheets.organizationId == OrganizationId 
                     && r.timesheets.acceptedDate != null
                     && r.recordStatus == 0
                     && r.timesheets.recordStatus == 0
                     orderby r.timesheets.year ascending, r.timesheets.week ascending, r.timesheets.contacts
                     select r);

            var data = q.ToList();
            
            var defaultTimeCat = db.work_schedule_profiles_rec
                .OrderByDescending(x => x.type)
                .FirstOrDefault(x => (x.type == 11 || x.type == 2)
                     && x.organization_id == OrganizationId
                     && x.recordStatus != -1);
            
            data = data.Where(x => x.startDate >= fromDate && x.endDate <= toDate).ToList();
            if (employeeId > 0) {
                data = data.Where(x => x.timesheets.employeeId == employeeId).ToList();
            }
            
            foreach (var r in data)
            {
                var nr = dt.NewRow();
                nr[0] = GetName(r.timesheets.contacts);
                nr[1] = r.timesheets.year;
                nr[2] = r.timesheets.week;
                nr[3] = r.startDate?.ToString("dd/MM/yyyy HH:mm");
                nr[4] = r.endDate?.ToString("dd/MM/yyyy HH:mm");
                nr[5] = r.work_schedule_profiles_rec?.name ?? defaultTimeCat?.name ?? "";
                nr[6] = r.rate;
                nr[7] = r.duration;
                if(r.jobs != null) nr[8] = r.jobs.internalId;
                if (r.jobs != null && r.jobs.clients != null && r.jobs.clients.companyName != null) nr[9] = r.jobs.clients.companyName;
                nr[10] = r.serviceName;
                nr[11] = r.servicePrice;
                nr[12] = r.serviceType;
                nr[13] = r.comment;
                dt.Rows.Add(nr);
            }

            gen.Table = dt;
            return gen.GenerateDocument();

        }
        
        public byte[] ExtractDaily(DateTime fromDate, DateTime toDate, long? employeeId = null)
        {
            DocGenerator.ExcelTableGenerator tableGenerator = new DocGenerator.ExcelTableGenerator();
            tableGenerator.Borders = true;
            tableGenerator.FitColumns = true;
            
            var dataBase = new CalaniEntities();

            var query = 
            (
                from r in dataBase.timesheetrecords
                    .Include("timesheets").Include("timesheets.contacts").Include("work_schedule_profiles_rec")
                where r.timesheets.organizationId == OrganizationId 
                      && r.timesheets.acceptedDate != null
                      && r.timesheets.employeeId > 0
                      && r.recordStatus == 0
                      && r.timesheets.recordStatus == 0
                orderby r.timesheets.year ascending, r.timesheets.week ascending, r.timesheets.contacts
                select r
            );

            var data = query.ToList();
            data = data.Where(x => x.startDate >= fromDate && x.endDate <= toDate).ToList();
            if (employeeId > 0) {
                data = data.Where(x => x.timesheets.employeeId == employeeId).ToList();
            }

            data = data != null && data?.Any() == true ? data : new System.Collections.Generic.List<timesheetrecords>(); 
            var grouped = data.GroupBy(x => x.timesheets.employeeId)
                .Select(x => new { EmplId = x.Key, Days = x.ToList() })
                .ToList();
            
            var table = new System.Data.DataTable();

            var columnQnt = 1;

            foreach (var group in grouped)
            {
                var byDay = group.Days.GroupBy(x => x.startDate.Value.Date)
                    .Select(x => new {day = x.Key, reports = x.ToList()})
                    .ToList();
                var currentMax = byDay.Max(x => x.reports.Count);
                columnQnt = currentMax > columnQnt ? currentMax : columnQnt;
            }

            #region columns
            table.Columns.Add("Employé");
            table.Columns.Add("Année");
            table.Columns.Add("Semaine");
            if (columnQnt > 1) {
                for (var i = 1; i <= columnQnt; i++)
                {
                    table.Columns.Add($"Début_{i}"); 
                    table.Columns.Add($"Fin_{i}");  
                    table.Columns.Add($"Catégorie de temps_{i}");
                }
            }
            else {
                table.Columns.Add("Début"); 
                table.Columns.Add("Fin");  
                table.Columns.Add("Catégorie de temps");
            }
            table.Columns.Add("Taux");
            table.Columns.Add("Durée(heures)");
            table.Columns.Add("Pause (minutes)");
            table.Columns.Add("Total quotidien");
            table.Columns.Add("Сommentaire");
            var sortColumn = table.Columns.Add("SortColumn", typeof(DateTime));
            sortColumn.ColumnMapping = MappingType.Hidden;
            
            #endregion
            
            var mgr = new Contacts.EmployeesManager(OrganizationId);
            var employees = mgr.ListEmployees();

            var defaultTimeCat = dataBase.work_schedule_profiles_rec
                .OrderByDescending(x => x.type)
                .FirstOrDefault(x => (x.type == 11 || x.type == 2)
                     && x.organization_id == OrganizationId
                     && x.recordStatus != -1);
                
            #region rows
            columnQnt *= 3;
            foreach (var group in grouped)
            {
                var byDay = group.Days.OrderBy(x => x.startDate)
                    .GroupBy(x => x.startDate.Value.Date)
                    .Select(x => new { day = x.Key, reports = x.ToList()})
                    .ToList();

                foreach (var dayGroup in byDay)
                {
                    var reports = dayGroup.reports;
                    var first = reports.First();
                    var employee = employees.First(x => x.id == first.timesheets.employeeId);
                    
                    var newRow = table.NewRow();
                    newRow[0] = $"{employee.lastName} {employee.firstName}";
                    newRow[1] = first.timesheets.year;
                    newRow[2] = first.timesheets.week;

                    var totalDuration = 0.0;
                    var totalWeightedRate  = 0.0;
                    var totalLunchDuration  = 0;
                    
                    for (var i = 0; i < reports.Count; i++)
                    {
                        var delta = i*3 + 2;
                        var item = reports[i];
       
                        newRow[1+delta] = item.startDate?.ToString("dd/MM/yyyy HH:mm");
                        newRow[2+delta] = item.endDate?.ToString("dd/MM/yyyy HH:mm");
                        newRow[3+delta] = item.work_schedule_profiles_rec?.name ?? defaultTimeCat?.name ?? "";

                        totalLunchDuration += item.pause_duration ?? 0;
                        totalDuration += item.duration ?? 0;
                        totalWeightedRate += item.duration * item.rate ?? 0;
                    }

                    totalDuration = totalDuration <= 0 ? 1 : totalDuration;
                    newRow[3+columnQnt] = (totalWeightedRate/totalDuration).ToString("0.##");
                    newRow[4+columnQnt] = totalDuration.ToString("0.##");
                    
                    newRow[5+columnQnt] = totalLunchDuration;
                    newRow[6+columnQnt] = ((totalDuration * 60 - totalLunchDuration)/60).ToString("0.##");
                    
                    newRow[7+columnQnt] = string.Join(", ", reports
                        .Where(x => !string.IsNullOrWhiteSpace(x.comment))
                        .Select(x => x.comment));
                    
                    table.Rows.Add(newRow);
                }
            }
            #endregion
            
            table.DefaultView.Sort = "SortColumn" + " " + "ASC";
            
            var dtOut = table.DefaultView.ToTable();
            tableGenerator.Table = dtOut;
            
            return tableGenerator.GenerateDocument(8 + columnQnt);
        }
        
        private string GetName(contacts contacts)
        {
            string ret = "?";
            if(contacts != null)
            {
                if (contacts.firstName != null) ret = contacts.firstName;
                ret += " ";
                if (contacts.lastName != null) ret += contacts.lastName;
                ret = ret.Trim();
                ret = ret.Replace("  ", " ");
            }
            return ret;
        }
    }
}
