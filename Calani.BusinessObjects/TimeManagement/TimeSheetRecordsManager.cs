﻿using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Membership;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Notifications;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Calani.BusinessObjects.TimeManagement
{
    public class TimeSheetRecordsManager : Generic.SmartCollectionManager<Model.timesheetrecords, long>
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public TimeSheetRecordsManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }


        internal override DbSet<timesheetrecords> GetDbSet(Model.CalaniEntities db)
        {
            return db.timesheetrecords;
        }

        internal override SmartQuery<timesheetrecords> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.timesheetrecords where r.id == id select r);
            return q;
        }


        internal override long GetRecordOrganization(timesheetrecords record)
        {
            if (record != null && record.timesheetId != null && record.timesheets.organizationId != null) return record.timesheets.organizationId.Value;
            return -1;
        }

        internal override SmartQuery<timesheetrecords> GetBaseQuery()
        {
            var q = base.GetBaseQuery();



            // base
            q.Query = (from r in q.Context.timesheetrecords select r);
            if (OrganizationID > -1) q.Query = (from r in q.Query where r.timesheets.organizationId == OrganizationID select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.startDate ascending select r);


            return q;
        }

        private timesheetrecords SaveRecord(CalaniEntities db, TimeUpdate tu, long sheetId, long userId, System.Globalization.CultureInfo c)
        {
            
            timesheetrecords dbnewrec = new timesheetrecords();
            dbnewrec.timesheetId = sheetId;
            tu.Bind(dbnewrec, c, OrganizationID, db, userId);
            var r = db.timesheetrecords.Add(dbnewrec);
            db.SaveChanges();
            return r;
        }

        public TimeSheetUpdateAction ValidateTimesheet(long timesheetId, System.Globalization.CultureInfo culture)
        {
            TimeSheetUpdateAction ret = new TimeSheetUpdateAction();
            ret.success = false;
            ret.id = timesheetId;

            var db = new CalaniEntities();
            var sheet = (from r in db.timesheets where r.id == timesheetId select r).FirstOrDefault();

            if(sheet != null)
            {
                sheet.acceptedDate = DateTime.Now;
                var action = ItemActions.Validate;
                db.SaveChanges();

                ret.success = true;
                ret.validationStatus = TimeSheetsManager.GetValidationStatus(sheet, culture);

                if (sheet.employeeId.HasValue)
                {
                    var empMgr = new Contacts.EmployeesManager(OrganizationID);
                    var emp = empMgr.Get(sheet.employeeId.Value);
                    UpdateVacationsUsage(emp);

                    var update = new TimeSheetUpdate
                    {
                        action = action.ToString(),
                        employee = sheet.employeeId.Value,
                        sheetId = sheet.id,
                        week = sheet.week ?? 0,
                        year = sheet.year ?? 0,
                        
                    };

                    var orgManager = new OrganizationManager(this.OrganizationID);
                    orgManager.Load();
                    var notificationOption = (TimesheetEmailOptionsEnum)orgManager.TimesheetEmailOption;
                    var sendNotification = action == ItemActions.Validate &&
                                           notificationOption != TimesheetEmailOptionsEnum.NotifyOnValidationOnlyIfChanged;
                    if (sendNotification)
                    {
                        SendNotification(sheet.employeeId.Value, action, update);
                    }
                }                
            }

            return ret;
        }

        public TimeSheetUpdateAction UpdateSet(TimeSheetUpdate update, System.Globalization.CultureInfo c, long userId)
        {
            TimeSheetUpdateAction ret = new TimeSheetUpdateAction();
            ret.success = false;

            var orgManager = new OrganizationManager(this.OrganizationID);
            orgManager.Load();
            
            update.items = (from r in update.items where r.date != null  && (r.rowType == (int)TimeSheetRecordRowType.MasterRow ||  r.duration!=0) select r).ToList();

            // Ensure that pause duration is attached to only a single record per day
            foreach(var day in update.items.GroupBy(i => i.date))
            {
                if(day.Count(line => line.pauseDuration > 0) > 1)
                {
                    var pauseDuration = day.Where(line => line.pauseDuration > 0).First().pauseDuration;
                    foreach (var line in day)
                    {
                        line.pauseDuration = 0;
                    }

                    var firstLine = day.OrderBy(line => line.startTime).First();
                    firstLine.pauseDuration = pauseDuration;
                }
            }

            var items = update.items.Where(i => i.recordType == 0 ).ToList();

            var serviceItems = update.items.Where(i => i.recordType == 1).ToList();

            var isWarn = items.Any(i => i.warn);

            var db = new CalaniEntities();

            Contacts.EmployeesManager empMgr = new Contacts.EmployeesManager(OrganizationID);
            var emp = empMgr.Get(update.employee);

            // new sheet ?
            if (update.sheetId == 0)
            {
                var newSheet = new timesheets();
              
                if (emp != null) 
                    newSheet.employeeId = emp.id;
                
                if(update.week > 0 && !items.Any())
                    newSheet.week = update.week;

                if(update.year > 0 && !items.Any())
                    newSheet.year = update.year;

                newSheet.organizationId = OrganizationID;
                db.timesheets.Add(newSheet);
                db.SaveChanges();
                update.sheetId = newSheet.id;
            }
            
            var dbrecords = (from r in db.timesheetrecords where r.timesheetId == update.sheetId && r.timesheets.organizationId == OrganizationID select r).ToList();
            
            // 1:update existing items
            foreach (var item in items)
            {
                if(item.id>0)
                {
                    var dbrec = (from r in dbrecords where r.id == item.id select r).FirstOrDefault();
                    if(dbrec != null)
                    {
                        item.Bind(dbrec, c, OrganizationID, db, userId);
                    }
                }
            }
           
            // -->

            // 2:remove missing items
            List<long> keepIds = (from r in items where r.id > 0 select r.id).Distinct().ToList();

            var dbRecordsToDelete = (from r in dbrecords where !keepIds.Contains(r.id) select r).ToList();

            foreach (var item in dbRecordsToDelete)
            {
                db.timesheetrecords.Remove(item);
            }

            List<long> keepServiceIds = (from r in serviceItems where r.id > 0 select r.id).Distinct().ToList();

           
            // -->

            // 3: add new items

            List<TimeUpdate> parents = items.Where(i => i.isParent).ToList();

            foreach (var pi in parents)
            {
                var child = items.Where(i => i.parentId.HasValue && (i.parentId == pi.position || i.parentId == pi.id)).ToList();

                if (pi.id <= 0)
                {
                    if (!pi.IsEmpty() && pi.duration > 0)
                    {
                        var r = SaveRecord(db, pi, update.sheetId, userId, c);
                        pi.id = r.id;
                    }
                }
                foreach (var ci in child)
                {
                    if (ci.id <= 0)
                    {
                        if (!ci.IsEmpty() && ci.duration > 0)
                        {
                            ci.parentId = pi.id;
                            SaveRecord(db, ci, update.sheetId, userId, c);
                        }
                    }
                }
            }

            var ones = items.Where(i => !i.isParent && !i.parentId.HasValue);

            foreach (var one in ones)
            {
                if (one.id <= 0)
                {
                    if (!one.IsEmpty() && one.duration > 0)
                    {
                        SaveRecord(db, one, update.sheetId, userId, c);

                    }
                }
            }

            // -->

            var sheet = (from r in db.timesheets where r.id == update.sheetId select r).FirstOrDefault();
            if (update.sheetId == 0) 
                sheet = null;


            if (sheet != null)
            {
               
                
                if(emp != null) 
                    sheet.employeeId = emp.id;

                sheet.is_warning = isWarn;
                sheet.year = update.year;
                sheet.week = update.week;
                sheet.comment = update.comment;

                if (db.ChangeTracker.HasChanges())
                {
                    sheet.sentDate = DateTime.Now; // sentDate sert de lastupdate 
                }
                
            }
            ret.id = sheet.id;

            ItemActions action = ItemActions.Update;

            // Check this before dates are updated
            var recordHasChanges = db.ChangeTracker.HasChanges();

            if (update.action == "save")
            {
                sheet.recordStatus = 0;
                sheet.acceptedDate = null;
                action = ItemActions.Update;

            }
            else if (update.action == "validate")
            {
                sheet.acceptedDate = DateTime.Now;
                action = ItemActions.Validate;

            }
            else if (update.action == "unvalidate")
            {
                sheet.acceptedDate = null;
                action = ItemActions.Update;

            }
            else if (update.action == "delete")
            {
                sheet.recordStatus = -1;
}

            // 4: commit
            db.SaveChanges();

            TimeDuplicateManager.CheckAndDeleteTimesheetRecordsDuplicates(update.sheetId, OrganizationID);
            
            // 5: get new data
            ret.success = true;

            var records = ListSheetRecords(update.sheetId);

            

            var newdata = (from r in records select new TimeUpdate(r)).ToList();
            
          

            ret.items = newdata;
            // -->


            ret.lastUpdateStatus = TimeSheetsManager.GetLastChangeStatus(sheet, c);
            ret.validationStatus = TimeSheetsManager.GetValidationStatus(sheet, c);

            if (update.action.Contains("validate"))
                UpdateVacationsUsage( emp);

            var notificationOption = (TimesheetEmailOptionsEnum)orgManager.TimesheetEmailOption;
            var sendNotification = (action == ItemActions.Update && recordHasChanges) ||
                                   (action == ItemActions.Validate &&
                                        (notificationOption != TimesheetEmailOptionsEnum.NotifyOnValidationOnlyIfChanged || recordHasChanges));
            if (sendNotification)
            {
                SendNotification(userId, action, update);
            }

            return ret;
        }

        void UpdateVacationsUsage( contacts employee)
        {
            try
            {
                var db = new CalaniEntities();
                var contract = db.contacts_contracts.FirstOrDefault(f => f.recordStatus == 0 && f.contact_Id == employee.id);
                if (contract != null)
                {
                    var usedVacationsHours = db.timesheetrecords.Where(tsr =>
                            tsr.work_schedule_profiles_rec != null &&
                            tsr.work_schedule_profiles_rec.type == (int)TimeCategoryType.Vacation &&
                            tsr.timesheets.employeeId == employee.id && tsr.recordStatus == 0 &&
                            tsr.timesheets.recordStatus == 0 &&
                            tsr.timesheets.acceptedDate != null && tsr.timesheets.acceptedDate >= contract.start_date)
                        .Sum(i => i.duration).GetValueOrDefault();

                    var workDay = (employee.organizations.week_work_duration / 5) / 60;

                    contract.vacation_remains = Convert.ToDecimal(string.Format("{0:F2}", (contract.vacation_year * workDay - usedVacationsHours) / workDay));
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                _log.Error($"updateVacationsUsage. Error. employee:{employee.id}. ", e);
            }

        }
        public List<timesheetrecords> ListSheetRecords(long id)
        {
            var q = GetBaseQuery();
            return (from r in q.Query where r.timesheetId == id select r).ToList();
        }
        
        internal override void BeforeAction(SmartAction<timesheetrecords> action)
        {

        }


        public List<Model.timesheetrecords> GetProjectRecords(long jobid)
        {
            var q = GetBaseQuery();
            var q2 = (from r in q.Query.Include("timesheets")
             where r.jobId == jobid 
             
             orderby r.startDate ascending
             select r);

            var records = q2.ToList();

            return records;
        }


       

        public IOrderedQueryable<Model.timesheetrecords> GetRecordsForPeriod(DateTime start, DateTime end)
        {
            var q = GetBaseQuery();

            var records = (from r in q.Context.timesheetrecords
                           where r.recordStatus == 0 && r.startDate >= start && r.endDate <= end &&
                                 r.timesheets.organizationId == OrganizationID &&
                      r.timesheets.acceptedDate !=null
                           orderby r.startDate ascending
                           select r);

            return records;
        }

        public IEnumerable<timesheetrecordsExt> GetRecordsForInvoiceTime(DateTime start, DateTime end)
        {
            var q = GetBaseQuery();

            var sql = @"
SELECT	GROUPED.*,
		INV.InvoiceId,
		INV.InvoiceInternalId,
		null as visitId,
		null as serviceName,
		null as servicePrice,
		null as rate,
		null as description,
		0 as recordStatus,
		null as lastModificationDate,
		GROUPED.projectId as jobId,
		null as serviceType,
		null as work_schedule_profiles_rec_id,
		false as is_warning,
		null as parent_id,
		0 as order_index,
		null as lunch_duration,
		null as row_type,
		false as disable_import,
		null as lastModifiedBy,
		null as work_schedule_profiles_rec,
		null as contacts,
		null as jobs, null as services,
		null as timesheetrecords1,
		null as timesheetrecords2,
		null as timesheets,
		null as visits,
		duration as durationStr
FROM (SELECT
		max(TSHR_EXT.id) as id,
		max(TSHR_EXT.timesheetId) as timesheetId,
		max(TSHR_EXT.clientId) as clientId,
		group_concat(TSHR_EXT.id separator ',') as groupedIds,
		DAYOFYEAR(TSHR_EXT.startDate),
		min(TSHR_EXT.startDate) as startDate, 
		max(TSHR_EXT.endDate) as endDate,
		sum(TSHR_EXT.duration) as duration,
		group_concat(TSHR_EXT.comment separator ';') as comment,
		TSHR_EXT.employeeId as employeeId,
		max(TSHR_EXT.internalId) as internalId,
		max(TSHR_EXT.projectId) as projectId,
		max(TSHR_EXT.imported) as imported,
		max(TSHR_EXT.companyName) as companyName,
		max(TSHR_EXT.employeeName) as employeeName,
		TSHR_EXT.serviceId as serviceId,
		max(TSHR_EXT.serviceName) as serviceName
	FROM (SELECT
			TSHR.id, 
			TSHR.timesheetId,
			TSHR.serviceId,
			TSHR.startDate, 
			TSHR.endDate,
			TSHR.duration,
			TSHR.jobId,
			TSHR.imported,
			TSHR.comment,
			TSHR.serviceName,
			TSH.employeeId,
			j.clientId,
			j.internalId,
			j.id as projectId,
			cl.companyName as companyName,
			CONCAT_WS(' ', emp.firstName, emp.lastName) as employeeName
			
			FROM (select * from timesheetrecords order by startDate asc) AS TSHR INNER JOIN timesheets AS TSH ON TSHR.timesheetId = TSH.id
			LEFT JOIN jobs j on j.id = TSHR.jobId
			LEFT JOIN clients cl on cl.id = j.clientId
			LEFT JOIN contacts emp on emp.id = TSH.employeeId
			
			WHERE (TSHR.recordStatus = 0 AND TSHR.startDate >= @start AND TSHR.endDate <= @end AND TSH.organizationId = @orgId AND TSH.acceptedDate IS NOT NULL AND TSHR.disable_import = false)) AS TSHR_EXT
			
			GROUP BY TSHR_EXT.jobId, TSHR_EXT.employeeId, TSHR_EXT.timesheetId, TSHR_EXT.serviceId, DAYOFYEAR(TSHR_EXT.startDate))
            
            as GROUPED
			
			LEFT JOIN -- Invoice
				(SELECT j.id as InvoiceId, j.internalId as InvoiceInternalId, j.dateInvoicing, j.clientId, sj.contactId, sj.timesheetDate from jobs j 
				JOIN services_jobs sj on j.id = sj.jobId where j.dateInvoicing IS NOT NULL) INV on INV.clientId = GROUPED.clientId && INV.contactId = GROUPED.employeeId && INV.timesheetDate = GROUPED.startDate;

";

            var startDateParam = new MySqlParameter("start", start);
            var endDateParam = new MySqlParameter("end", end);
            var orgIdParam = new MySqlParameter("orgId", OrganizationID);

            var t = q.Context.Database.SqlQuery(typeof(timesheetrecordsExt), sql, startDateParam, endDateParam, orgIdParam).ToListAsync();
            var tr = t.Result.Cast<timesheetrecordsExt>();

            return tr;
        }

        public IEnumerable<jobs> GetInvoices(long clientId, long employeeId, DateTime date)
        {
            using (var db = new CalaniEntities())
                return
                    (from j in db.jobs
                    from sj in j.services_jobs
                    where sj.recordStatus != (int)RecordStatus.Deleted
                    where j.dateInvoicing != null // invoices only
                    where j.clientId == clientId && employeeId == sj.contactId && sj.timesheetDate == date.Date
                    select j)
                    .Distinct()
                    .ToList();
        }

        public void CancelImport(long orgId, long userId, List<long> timesheetRecordIds)
        {
            if (timesheetRecordIds == null || !timesheetRecordIds.Any()) return;

            var db = new CalaniEntities();

            var records = db.timesheetrecords.Where(r => timesheetRecordIds.Contains(r.id) && r.timesheets.organizationId == orgId);

            if (records.Any())
            {
                foreach (var record in records)
                {
                    record.imported = false;
                    record.lastModifiedBy = userId;
                    record.disable_import = true;
                }

                db.SaveChanges();
            }
        }

        private void SendNotification(long currentUserId, ItemActions action, TimeSheetUpdate tsUpdate)
        {
            NotificationManager nm = new NotificationManager();

            var cultureStr = "fr-FR";

            var culture = new System.Globalization.CultureInfo(cultureStr);

            if (tsUpdate.employee > 0)
            {
                try
                {
                    cultureStr = UserCultureManager.GetUserCulture(tsUpdate.employee);

                    culture = new System.Globalization.CultureInfo(cultureStr ?? "fr-FR");
                }
                catch (Exception e)
                {
                    _log.Error("SendNotification, getUserCulture error.", e);
                }
            }





            var msg = String.Empty;
            var title = String.Empty;
            var category = String.Empty;
            var users = new List<long>();
            var db = new CalaniEntities();

            var org = (from r in db.organizations where r.id == OrganizationID select r).FirstOrDefault();

            var employeesManager = new Calani.BusinessObjects.Contacts.EmployeesManager(OrganizationID);

            IEnumerable<long> tmpUsers;
            switch (action)
            {
                case ItemActions.Create:

                   
                    break;

                case ItemActions.Update:
                    
                    if (currentUserId != tsUpdate.employee)
                        users.Add(tsUpdate.employee);

                    var rTitleU = GetResource("TimeSheetNotificationTitleModified", culture);
                    title = String.Format(rTitleU, tsUpdate.week);

                    category = "TimeSheets";

                    var rMsgU = GetResource("TimeSheetNotificationMsgModified", culture);

                    msg = String.Format(rMsgU, tsUpdate.week);

                    break;

                case ItemActions.Validate:
                    var rTitleV = GetResource("TimeSheetNotificationTitleValidated", culture);
                    title = String.Format(rTitleV, tsUpdate.week);

                    category = "TimeSheet";

                    var rMsgV = GetResource("TimeSheetNotificationMsgValidated", culture);

                    msg = String.Format(rMsgV, tsUpdate.week);

                    
                    users.Add(tsUpdate.employee);
                    break;
                case ItemActions.Reject:
                    var rTitleR = GetResource("TimeSheetNotificationTitleRejected", culture);
                    title = String.Format(rTitleR, tsUpdate.week);

                    category = "TimeSheet";

                    var rMsgR = GetResource("TimeSheetNotificationMsgRejected", culture);

                    msg = String.Format(rMsgR, tsUpdate.week);


                    users.Add(tsUpdate.employee);
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(action), action, null);
            }

            _log.Debug($"Sending notification, title:'{title}' to:{String.Join(",", users)}");

            if (!String.IsNullOrWhiteSpace(title))
            {
                nm.sendNotifications(msg, title, users, new Dictionary<string, string>() { { "timeSheetId", tsUpdate.sheetId.ToString() } }, category);

                var mailSender = new Calani.BusinessObjects.Generic.MailSender();

                var pUrl = System.Configuration.ConfigurationManager.AppSettings["PublicUrl"];
                mailSender.Variables.Add("PublicUrl",pUrl);

                var linkTemplate = GetResource("ConsultLinkHtmlTemplate", culture);
                var link = String.Format(linkTemplate, pUrl + "Time/Sheets/Sheet.aspx?id=" + tsUpdate.sheetId);

                msg += " " + link;

                
                foreach (var userId in users)
                {
                    var user = employeesManager.Get(userId);
                    mailSender.MailTo.Email = user.primaryEmail ?? user.secondaryEmail;
                    mailSender.MailTo.UserId = userId;
                    mailSender.SendSmartHtmlEmail(title, msg, category);
                }
            }

        }

        private string GetResource(string n, CultureInfo cultureInfo)
        {
            if (SharedResource.Resource == null) return n;
            return SharedResource.Resource.GetString(n, cultureInfo);
        }
    }
}
