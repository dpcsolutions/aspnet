﻿using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.TimeManagement.Models;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using log4net;

namespace Calani.BusinessObjects.TimeManagement.Services
{
    public class TimeSheetsService
    {
        private readonly TimeSheetsManager _timeSheetsManager;

        public TimeSheetsService(TimeSheetsManager timeSheetsManager)
        {
            _timeSheetsManager = timeSheetsManager;
        }

        public List<TimeSheetModel> ListTimeSheets(long orgId, DateRange dateRange, long? employeeId = null, bool? alreadyValidated = null)
        {
            double? totalHours = 0;
            var timeSheets = _timeSheetsManager.List(out totalHours, alreadyValidated, dateRange?.Start, dateRange?.End, employeeId);
            
            var orgMgr = new CustomerAdmin.OrganizationManager(orgId);
            orgMgr.Load();
            
            var models = timeSheets.Select(x => new TimeSheetModel(x, orgMgr.UseWorkSchedule, 
                (TimesheetTypeEnum)orgMgr.TimeSheetType)).ToList();

            return models;
        }
    }
}
