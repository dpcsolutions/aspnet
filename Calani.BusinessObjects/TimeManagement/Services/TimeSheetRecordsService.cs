﻿using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.TimeManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Calani.BusinessObjects.TimeManagement.Services
{
    public class TimeSheetRecordsService
    {
        private readonly TimeSheetRecordsManager _timeSheetRecordsManager;

        private readonly ClientContactsManager _contactsManager;

        public TimeSheetRecordsService(TimeSheetRecordsManager timeSheetRecordsManager, ClientContactsManager contactsManager)
        {
            _timeSheetRecordsManager = timeSheetRecordsManager;
            _contactsManager = contactsManager;
        }

        public List<TimeSheetRecordModel> ListTimeSheetsRecordsForInvoiceTime(DateRange dateRange)
        {
            if(dateRange == null)
            {
                dateRange = new DateRange(new DateTime(DateTime.Now.Year, 1, 1).AddYears(-100), new DateTime(DateTime.Now.Year, 12, 31));
            }

            Func<DateTime?, int> dateProjector = d => d.HasValue ? d.Value.DayOfYear : 0;
            var entities = _timeSheetRecordsManager.GetRecordsForInvoiceTime(dateRange.Start, dateRange.End).ToList();

            foreach (var en in entities) en.durationStr = TimeSpan.FromHours(en.duration ?? 0).ToString("hh':'mm''");

            var list = entities.Select(r => new TimeSheetRecordModel(r)).OrderBy(r => r.StartDate).ToList();
            return list;
        }

        public void CancelTimeSheetRecordImport(List<long> timeSheetRecordIds, long userId)
        {
            if (timeSheetRecordIds == null || !timeSheetRecordIds.Any()) return;

            var user = _contactsManager.GetById(userId).Query.FirstOrDefault();

            if(user == null)
            {
                throw new ArgumentException("User not found", nameof(userId));
            }

            var userType = (ContactTypeEnum)(user.type ?? 0);

            if (userType != ContactTypeEnum.User_Admin)
            {
                throw new UnauthorizedAccessException("Only admins are allowed to cancel import");
            }

            _timeSheetRecordsManager.CancelImport(_timeSheetRecordsManager.OrganizationID, userId, timeSheetRecordIds);
        }
    }
}
