﻿using Calani.BusinessObjects.AbsenceRequests;
using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Scheduler;
using Calani.BusinessObjects.TimeManagement.Models;
using Calani.BusinessObjects.WorkSchedulers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace Calani.BusinessObjects.TimeManagement.Services
{
    public class TimesheetReportService
    {
        private readonly EmployeesManager.EmployeesContractsManager _contractsManager;
        private readonly TimeSheetsService _timesheetsService;
        private readonly TimeSheetRecordsManager _timesheetRecordsManager;
        private readonly EmployeesOvertimeManager _employeesOvertimeManager;
        private readonly AbsenceRequestsManager _absenceRequestManager;
        private readonly OrganizationManager _organizationManager;
        private readonly WorkScheduleRecManager _workScheduleRecManager;
        private readonly WorkingTimeCalculator _workingTimeCalculator;

        public TimesheetReportService(
            EmployeesManager.EmployeesContractsManager contractsManager,
            TimeSheetsService timesheetsService,
            TimeSheetRecordsManager timesheetRecordsManager,
            EmployeesOvertimeManager employeesOvertimeManager,
            AbsenceRequestsManager absenceRequestManager,
            OrganizationManager organizationManager,
            WorkScheduleRecManager workScheduleRecManager)
        {
            _contractsManager = contractsManager;
            _timesheetsService = timesheetsService;
            _timesheetRecordsManager = timesheetRecordsManager;
            _employeesOvertimeManager = employeesOvertimeManager;
            _absenceRequestManager = absenceRequestManager;
            _organizationManager = organizationManager;
            _workScheduleRecManager = workScheduleRecManager;

            var workScheduleService = new WorkScheduleService(_organizationManager, _workScheduleRecManager);
            var workDay = workScheduleService.GetWorkDaySettings();
            _workingTimeCalculator = new WorkingTimeCalculator(workDay);
        }

        public TimesheetReportModel CalculateTimesheetReportForPeriod(long orgId, long contactId, DateRange range)
        {
            var reportOverTimesheets = new TimesheetReportModel();

            var timesheets = _timesheetsService.ListTimeSheets(orgId, range, contactId, alreadyValidated: true);

            var exceptions = new List<string>();

            foreach(var timesheet in timesheets)
            {
                try
                {
                    var report = CalculateTimesheetReportForTimesheet(timesheet);
                    reportOverTimesheets.SumUpReport(report);
                }
                catch(ArgumentException exception)
                {
                    exceptions.Add(exception.Message);
                }
            }

            var summaryBuilder = new StringBuilder(reportOverTimesheets.Summary);
            
            var overtimeForUser = _employeesOvertimeManager.GetOvertimeForEmployee(contactId) ?? new contacts_overtime();
            string hoursEffCompensatedSummary;
            reportOverTimesheets.TotalHoursEffectivelyCompensated = CalculateTotalHoursEffectivelyCompensated(contactId, overtimeForUser, out hoursEffCompensatedSummary);
            reportOverTimesheets.TotalHoursActuallyPaid = overtimeForUser.total_hours_actually_paid;
            reportOverTimesheets.TotalHoursActuallyPaidEnteredByAdmin = overtimeForUser.total_hours_actually_paid;
            reportOverTimesheets.TotalHoursEffectivelyCompensatedEnteredByAdmin = overtimeForUser.total_hours_effectively_compensated;
            summaryBuilder.AppendLine($"{Environment.NewLine}Total hours actually paid from database = {reportOverTimesheets.TotalHoursActuallyPaid}h.");
            summaryBuilder.AppendLine($"{hoursEffCompensatedSummary}");

            summaryBuilder.AppendLine($"Total hours to be compensated = {reportOverTimesheets.TotalHoursToBeCompensated}h - {reportOverTimesheets.TotalHoursEffectivelyCompensated}h (eff compensated hours) - {reportOverTimesheets.TotalHoursActuallyPaid}h (actually paid hours).");

            reportOverTimesheets.TotalHoursToBeCompensated -= (reportOverTimesheets.TotalHoursEffectivelyCompensated + reportOverTimesheets.TotalHoursActuallyPaid);

            summaryBuilder.AppendLine($"{string.Join(Environment.NewLine, exceptions)}");

            reportOverTimesheets.Summary = summaryBuilder.ToString();

            return reportOverTimesheets;
        }

        #region Private Methods

        private TimesheetReportModel CalculateTimesheetReportForTimesheet(TimeSheetModel timesheet)
        {
            var report = new TimesheetReportModel();

            if (!timesheet.EmployeeId.HasValue)
            {
                throw new ArgumentException($"Timesheet #{timesheet.Id} has no EmployeeId set.");
            }

            var contract = GetRespectiveContractForTimesheet(timesheet.EmployeeId.Value, timesheet);

            if(contract == null)
            {
                throw new ArgumentException($"Contract for timesheet #{timesheet.Id} was not found.");
            }

            var durationSeconds = CalculateTotalDurationForTimesheetSeconds(timesheet);
            var holidayDurationSeconds = CalculateHolidayDurationForTimesheetSeconds(timesheet);
            
            var overtimeRate2NotScaledSeconds = CalculateOvertimeRate2NotScaledSeconds(durationSeconds, contract);
            var overtimeRate1NotScaledSeconds = CalculateOvertimeRate1NotScaledSeconds(durationSeconds, overtimeRate2NotScaledSeconds, contract);

            // Overtime can have >100% rates applied to it
            var overtimeRate2ScaledSeconds = CalculateOvertimeRate2ScaledSeconds(overtimeRate2NotScaledSeconds, contract);            
            var overtimeRate1ScaledSeconds = CalculateOvertimeRate1ScaledSeconds(overtimeRate1NotScaledSeconds, contract);

            report.OvertimeRate1Hours = FromSecondsToHours(overtimeRate1ScaledSeconds);
            report.OvertimeRate2Hours = FromSecondsToHours(overtimeRate2ScaledSeconds);
            report.TotalOvertimeHours = FromSecondsToHours(overtimeRate1NotScaledSeconds + overtimeRate2NotScaledSeconds);
            report.TotalHoursWorked = FromSecondsToHours(durationSeconds);
            report.TotalHoursToBeCompensated = report.OvertimeRate1Hours + report.OvertimeRate2Hours;

            var summary = new StringBuilder();
            var timesheetDates = timesheet.GetStartAndEndDates();
            summary.AppendLine("------------------------------------------------------------");
            summary.AppendLine($"Employee: #{timesheet.EmployeeId} {timesheet.EmployeeName};");
            summary.AppendLine($"Timesheet: #{timesheet.Id}; from {timesheetDates.Start.ToShortDateString()} to {timesheetDates.End.ToShortDateString()}; week: {timesheet.Week}; year: {timesheet.Year};");
            summary.AppendLine($"Contract: #{contract.id}; start: {contract.start_date.Value.ToShortDateString()}; active: {contract.recordStatus == (int)RecordStatus.Active};");
            summary.AppendLine($"Week duration: {(decimal)(contract.duration ?? 0) / 60} hours;");
            summary.AppendLine($"Overtime enabled: {contract.overtime_allowed}; Overtime2 enabled: {contract.overtime_rate2_enabled}; Overtime2 threshold: {contract.overtime_rate2_threshold_hours} hours;");
            summary.AppendLine($"Total hours worked: {report.TotalHoursWorked};");
            summary.AppendLine($"Overtime 1 not scaled: {overtimeRate1NotScaledSeconds / (60 * 60)} hours;");
            summary.AppendLine($"Overtime 2 not scaled: {overtimeRate2NotScaledSeconds / (60 * 60)} hours;");
            summary.AppendLine($"Overtime 1 rate: {contract.overtime_rate}%, overtime 1 scaled: {overtimeRate1ScaledSeconds / (60 * 60)} hours;");
            summary.AppendLine($"Overtime 2 rate: {contract.overtime_rate2}%, overtime 2 scaled: {overtimeRate2ScaledSeconds / (60 * 60)} hours;");
            summary.AppendLine($"To be compensated: ({report.TotalHoursWorked} - {overtimeRate1NotScaledSeconds / (60 * 60)} - {overtimeRate2NotScaledSeconds / (60 * 60)}) + {overtimeRate1ScaledSeconds / (60 * 60)} + {overtimeRate2ScaledSeconds / (60 * 60)} = " +
                $"{report.TotalHoursWorked - overtimeRate1NotScaledSeconds / (60 * 60) - overtimeRate2NotScaledSeconds / (60 * 60) + overtimeRate1ScaledSeconds / (60 * 60) + overtimeRate2ScaledSeconds / (60 * 60)}");

            report.Summary = summary.ToString();

            return report;
        }

        private contacts_contracts GetRespectiveContractForTimesheet(long contactId, TimeSheetModel timesheet)
        {
            if (!timesheet.SheetDate.HasValue)
            {
                throw new ArgumentException($"Timesheet #{timesheet.Id} has no SheetDate.");
            }

            var timesheetDates = timesheet.GetStartAndEndDates();

            var contract = _contractsManager.GetContractStartingInPeriod(timesheetDates.Start, timesheetDates.End, contactId);

            return contract;
        }

        private decimal CalculateTotalDurationForTimesheetSeconds(TimeSheetModel timesheet)
        {
            decimal timesheetDurationSeconds = 0;

            var records = _timesheetRecordsManager.ListSheetRecords(timesheet.Id);

            foreach(var record in records)
            {
                var durationSeconds = (record.endDate.Value - record.startDate.Value).TotalSeconds 
                                                - (record.lunch_duration ?? 0) * 60
                                                - (record.pause_duration ?? 0) * 60;

                timesheetDurationSeconds += (decimal)durationSeconds;
            }

            return timesheetDurationSeconds;
        }
        
        private decimal CalculateHolidayDurationForTimesheetSeconds(TimeSheetModel timesheet)
        {
            decimal timesheetDurationSeconds = 0;

            var  db = new CalaniEntities();
            var holidayCatId = (from r in db.work_schedule_profiles_rec
                where r.organization_id == _organizationManager._id
                      && r.name == "Férié"
                select r).FirstOrDefault()?.id ?? 0;
            
            if (holidayCatId > 0)
            {
                var records = _timesheetRecordsManager.ListSheetRecords(timesheet.Id)
                    .Where(x => x.work_schedule_profiles_rec_id == holidayCatId);

                foreach(var record in records)
                {
                    var durationSeconds = (record.endDate.Value - record.startDate.Value).TotalSeconds;
                    timesheetDurationSeconds += (decimal)durationSeconds;
                }

                return timesheetDurationSeconds;
            }
            return 0;
            
        }

        private decimal CalculateOvertimeRate2NotScaledSeconds(decimal totalDurationSeconds, contacts_contracts contract)
        {
            decimal overtimeSeconds = 0;

            if(!contract.overtime_allowed || !contract.overtime_rate2_enabled)
            {
                return overtimeSeconds;
            }

            var weekDurationMin = contract.duration ?? 0;
            var overtimeRate2ThresholdHours = contract.overtime_rate2_threshold_hours;

            overtimeSeconds = Math.Max(totalDurationSeconds - weekDurationMin * 60 - overtimeRate2ThresholdHours * 60 * 60, 0);

            return overtimeSeconds;
        }

        private decimal CalculateOvertimeRate1NotScaledSeconds(decimal totalDurationSeconds, decimal overtimeRate2Seconds, contacts_contracts contract)
        {
            decimal overtimeSeconds = 0;

            if (!contract.overtime_allowed)
            {
                return overtimeSeconds;
            }

            var weekDurationMin = contract.duration ?? 0;

            overtimeSeconds = Math.Max(totalDurationSeconds - weekDurationMin * 60 - overtimeRate2Seconds, 0);

            return overtimeSeconds;
        }

        private decimal CalculateOvertimeRate1ScaledSeconds(decimal overtimeRate1NotScaledSeconds, contacts_contracts contract)
        {
            var rate1Percent = contract.overtime_rate;
            var scaled = overtimeRate1NotScaledSeconds * rate1Percent / 100;

            return scaled;
        }

        private decimal CalculateOvertimeRate2ScaledSeconds(decimal overtimeRate2NotScaledSeconds, contacts_contracts contract)
        {
            var rate2Percent = contract.overtime_rate2;
            var scaled = overtimeRate2NotScaledSeconds * rate2Percent / 100;

            return scaled;
        }

        private decimal FromSecondsToHours(decimal seconds)
        {
            return Math.Round(seconds / (60 * 60), 2, MidpointRounding.AwayFromZero);
        }

        private decimal CalculateTotalHoursEffectivelyCompensated(long contactId, contacts_overtime overtimeForEmployee, out string summary)
        {//
            summary = string.Empty;

            if (overtimeForEmployee == null)
            {
                return 0;
            }

            var hours = overtimeForEmployee.total_hours_effectively_compensated;

            var approvedTimeRecoveryAbsRequests = _absenceRequestManager.ListApprovedTimeRecoveryLeaves(null, null, contactId);
            decimal totalRecoveryHours = 0;

            var summaryBuilder = new StringBuilder();
            summaryBuilder.AppendLine($"Total hours effectively compensated from database = {hours}h.");
            summaryBuilder.AppendLine($"Adding approved recovery time from {approvedTimeRecoveryAbsRequests.Count} absence requests.");
            foreach (var absRequest in approvedTimeRecoveryAbsRequests)
            {
                var db = new CalaniEntities();
                var holidayList = db.calendars_rec
                    .Where(x => x.name == "Férié" && x.calendars.organization_Id == _organizationManager._id && x.recordStatus != -1)
                    .Include(x => x.calendars)
                    .ToList();
                
                var recoveredHours = _workingTimeCalculator.CalculateAbsenceMinutes(absRequest.StartDate, absRequest.EndDate, holidayList) / 60;
                totalRecoveryHours += (decimal)recoveredHours;
                summaryBuilder.AppendLine($"Absence request #{absRequest.Id} has {recoveredHours} working hours");
            }

            summaryBuilder.AppendLine($"Total hours effectively compensated = {hours}h + {totalRecoveryHours}h = {hours + totalRecoveryHours}h.");

            hours += totalRecoveryHours;
            summary = summaryBuilder.ToString();

            return hours;
        }

        #endregion Private Methods
    }
}
