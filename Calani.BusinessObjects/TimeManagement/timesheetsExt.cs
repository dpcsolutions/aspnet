﻿using Calani.BusinessObjects.TimeManagement.Models;
using System;

namespace Calani.BusinessObjects.TimeManagement
{
    public class timesheetsExt : Model.timesheets
    {
        

        public timesheetsExt()
        {

        }

        public timesheetsExt(Model.timesheets r, Enums.TimesheetTypeEnum templateType)
        {
            Generic.Tools.CopyAllProperties(r, this);
            EmployeeName = r.contacts.lastName + " " + r.contacts.firstName;
            EmployeeName = EmployeeName.Trim();
            EmployeeInitials = r.contacts.initials;

            if (r.year != null && r.week != null)
            {
                sheetDate = Generic.CalendarTools.GetIso8601MondayOfWeek(r.year.Value, r.week.Value);
            }


            if (r.acceptedDate == null)
            {
                sheetStatus = "tovalidate";
                if(sheetDate != null && r.rejectDate == null)
                {
                    var endOfTimesheetWeek = sheetDate.Value.AddDays(TimeSheetModel.DaysInWeekForTemplate(templateType));
                    if (DateTime.Now.Date <= endOfTimesheetWeek.Date) sheetStatus = "currentweek";
                }
            }
            else
            {
                sheetStatus = "validated";
            }

        }

        public double TotalDuration { get; set; }
        public double? AttendedDuration { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeInitials { get; set; }
        public int? CommentNumber { get; set; }

        public DateTime? sheetDate { get; set; }

        public long DateTicks
        {
            get
            {
                return sheetDate.GetValueOrDefault().Ticks;
            }
        }
        public string sheetStatus { get; set; }
    }

    public class timesheetrecordsExt : Model.timesheetrecords
    {
        public timesheetrecordsExt()
        {
        }

        public timesheetrecordsExt(Model.timesheetrecords r)
        {
            Generic.Tools.CopyAllProperties(r, this);
        }

        private long? _clientId;

        public long? clientId
        {
            get
            {
                return _clientId ?? base.jobs?.clients?.id;
            }

            set
            {
                _clientId = value;
            }
        }

        private string _companyName;

        public string companyName
        {
            get
            {
                return _companyName ?? base.jobs?.clients?.companyName;
            }

            set
            {
                _companyName = value;
            }
        }

        private long? _employeeId;

        public long? employeeId
        {
            get
            {
                return _employeeId ?? timesheets.employeeId; // it has to be the the correct way. it makes more sense
                //return base.jobs?.contacts?.id;
            }

            set
            {
                _employeeId = value;
            }
        }

        private string _employeeName;

        public string employeeName
        {
            get
            {
                return _employeeName ?? base.timesheets?.contacts?.firstName + " " + base.timesheets?.contacts?.lastName;
            }

            set
            {
                _employeeName = value;
            }
        }

        private long? _projectId;

        public long? projectId
        {
            get
            {
                return _projectId ?? base.jobs?.id;
            }

            set
            {
                _projectId = value;
            }
        }

        private string _internalId;

        public string internalId
        {
            get
            {
                return _internalId ?? base.jobs?.internalId;
            }

            set
            {
                _internalId = value;
            }
        }

        public string groupedIds { get; set; }
        public string durationStr { get; set; }

        public long? InvoiceId { get; set; }
        public string InvoiceInternalId { get; set; }       
            
    }
}
