﻿using System.Collections.Generic;
using Calani.BusinessObjects.Model;
using System.Linq;

namespace Calani.BusinessObjects.TimeManagement
{
    public static class TimeDuplicateManager
    {
        public static void CheckAndDeleteTimesheetRecordsDuplicates(long id, long orgId)
        {
            var db = new CalaniEntities();
            
            var savedRecords = (from r in db.timesheetrecords where r.timesheetId == id 
                && r.timesheets.organizationId == orgId select r).ToList();
            var duplicates = new List<timesheetrecords>(); 
            
            var groupedDuplicates = savedRecords
                .GroupBy(x => new { x.startDate, x.endDate })
                .Where(x => x.Count() > 1).ToList();

            if (groupedDuplicates.Any())
            {
                foreach (var group in groupedDuplicates)
                {
                    var first = true;
                    foreach (var record in group)
                    {
                        if (first) first = false;
                        else duplicates.Add(record);
                    }
                }
            
                db.timesheetrecords.RemoveRange(duplicates);
                db.SaveChanges();
            }
        }
    }
}