﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Calendars;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.WorkSchedulers;

namespace Calani.BusinessObjects.TimeManagement
{
    public class TimeSheetItemsList
    {
        public int Year { get; set; }
        public int Week { get; set; }
        public long Employee { get; set; }

        public long OrganizationId { get; set; }
        public long TimeSheetId { get; set; }

        public TimeSheetItemsList()
        {
        }
        public TimeSheetItemsList(long organizationId, long timeSheetId)
        {
            OrganizationId = organizationId;
            TimeSheetId = timeSheetId;
            Year = -1;
            Week = -1;
            Employee = -1;
        }

        public TimeSheetItemsList(long organizationId, int year, int week, long employee)
        {
            OrganizationId = organizationId;
            Year = year;
            Week = week;
            Employee = employee;
            TimeSheetId = -1;
        }

        public List<SelectableTimeItem> List(System.Globalization.CultureInfo c, bool shortNames = false)
        {
            List<SelectableTimeItem> ret = new List<SelectableTimeItem>();

            var db = new Model.CalaniEntities();

            
            if(TimeSheetId > 0)
            {
                var ts = (from r in db.timesheets where r.organizationId == OrganizationId && r.id == TimeSheetId select r).FirstOrDefault();
                if(ts != null && ts.year != null && ts.week != null && ts.employeeId != null)
                {
                    Year = ts.year.Value;
                    Week = ts.week.Value;
                    Employee = ts.employeeId.Value;
                }
            }
            

            // 1: projets et visites dans la semaine
            
            Generic.CalendarTools week = new Generic.CalendarTools(Year, Week);

            var visits = (from r in db.visits
                where r.dateStart > week.Start && r.dateEnd < week.End
                    //TODO : && r.visitStatus != annulée
                && r.contacts.Any(ctc => ctc.id == Employee)
                orderby r.dateStart descending
                select r
                ).ToList();

            foreach (var visit in visits)
            {
                long? customerId = null;
                long? jobId = null;
                long? visitId = visit.id;

                string jobName = "";
                string customerName = "";
                string visitName = "";

                if (visit.jobId != null)
                {
                    jobId = visit.jobId;

                    customerId = visit.jobs.clientId;
                    customerName = visit.jobs.clients.companyName;
                    jobName = shortNames ? visit.jobs.internalId + (String.IsNullOrWhiteSpace(visit.jobs.description) ? String.Empty : "-" + visit.jobs.description) : visit.jobs.clients.companyName + " - " + visit.jobs.internalId;
                }


                visitName = customerName;

                string scheduledStart = null;
                string scheduledEnd = null;

                if (visit.dateStart != null) {
                    scheduledStart = visit.dateStart.Value.ToString("HH:mm");
                    visitName += " - " + visit.dateStart.Value.ToString("dd.MM.yy") + " - " + visit.dateStart.Value.ToString("HH:mm");
                }
                if (visit.dateEnd != null) 
                    scheduledEnd = visit.dateEnd.Value.ToString("HH:mm");


                    


                ret.Add(new SelectableTimeItem
                {
                    visitName = visitName,
                    visitId = visitId,
                    jobId = jobId,
                    jobName = jobName,
                    clientName = customerName,
                    recordStatus = visit.recordStatus,
                    serviceId =null,
                    serviceName=null,
                    serviceInternal=null,
                    date = visit.dateStart.Value.ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture),
                    scheduledStart = scheduledStart,
                    scheduledEnd = scheduledEnd,
                    description = visit.description
                    
                        
                });
            }

            // -->


            // 2: compléter avec les projets ouverts sans visite
            Projects.ProjectsManager pmgr = new Projects.ProjectsManager(OrganizationId);
            pmgr.IgnoreRecordStatus = true;
            var projects = pmgr.ListOpens(null);
            if (projects != null) projects = (from r in projects orderby r.id descending select r).ToList();

            foreach (var project in projects)
            {
                ret.Add(new SelectableTimeItem
                {
                    visitName = null,
                    visitId = null,
                    jobId = project.id,
                    clientName = project.clientName,
                    jobName = shortNames ? project.internalId + (String.IsNullOrWhiteSpace(project.description) ? String.Empty : "-" + project.description) : project.clientName + " - " + project.internalId,
                    recordStatus = project.recordStatus,
                    serviceId = null,
                    serviceName = null,
                    serviceInternal = null,
                    date = null,
                    scheduledStart = null,
                    scheduledEnd = null,

                });
            }
            // -->

            // 3: compléter avec le catalogue des prestations
            CustomerAdmin.ServicesManager smgr = new CustomerAdmin.ServicesManager(OrganizationId);
            smgr.IgnoreRecordStatus = true;
            var services = smgr.List();
            foreach (var service in services)
            {
                ret.Add(new SelectableTimeItem
                {
                    visitName = null,
                    visitId = null,
                    jobId = null,
                    jobName = null,
                    serviceId = service.id,
                    serviceName = service.name,
                    serviceInternal = false,
                    recordStatus = service.recordStatus,
                    date = null,
                    scheduledStart = null,
                    scheduledEnd = null,

                });
            }
            // -->

            // 4: compléter avec le catalogue des prestations
            CustomerAdmin.InternalJobsManager imgr = new CustomerAdmin.InternalJobsManager(OrganizationId);
            imgr.IgnoreRecordStatus = true;
            var internals = imgr.List();
            foreach (var intern in internals)
            {
                ret.Add(new SelectableTimeItem
                {
                    visitName = null,
                    visitId = null,
                    jobId = null,
                    jobName = null,
                    serviceId = intern.id,
                    serviceName = intern.name,
                    recordStatus = intern.recordStatus,
                    serviceInternal = true,
                    date = null,
                    scheduledStart = null,
                    scheduledEnd = null,
                });
            }
            // -->


            // 5: schedule records
            Contacts.EmployeesManager emplMgr = new Contacts.EmployeesManager(OrganizationId);
            List<long> calendarIds ;

            var recList = emplMgr.GetAssignedScheduleRecordsDto(Employee, out calendarIds);

            var calDays = new List<string[]>();
            if (calendarIds != null)
            {
                var calMngr = new CalendarsRecManager(OrganizationId);
                foreach (var id in calendarIds)
                {
                    var recs  = calMngr.GetCalendarRecordsDto(id);

                    var ha = recs.Select(r => r.Hours).ToArray();
                    calDays.Add(ha);
                }

                ret.Add(new SelectableTimeItem
                {
                    calendarDays = calDays
                });
            }

            var wsrm = new WorkScheduleRecManager(OrganizationId);

            var predefinedCat  = wsrm.GetScheduleRecordsDto(null);

            foreach (var dto in predefinedCat)
            {
                ret.Add(new SelectableTimeItem
                {
                    
                    timeId = dto.Id,
                    timeName = dto.Name,
                    timeStart = null,
                    timeEnd = null,
                    timeRate = dto.Rate,
                    timeDays = Array.Empty<int>(),
                    timeType = dto.Type,
                    

                });
            }

            foreach (var rec in recList)
            {
                List<int> days = new List<int>();
                if(rec.Mo)
                    days.Add(1);
                if (rec.Tu)
                    days.Add(2);
                if (rec.We)
                    days.Add(3);
                if (rec.Th)
                    days.Add(4);
                if (rec.Fr)
                    days.Add(5);
                if (rec.Sa)
                    days.Add(6);
                if (rec.Su)
                    days.Add(7);

                ret.Add(new SelectableTimeItem
                {
                    visitName = null,
                    visitId = null,
                    jobId = null,
                    jobName = null,
                    serviceId = null,
                    serviceName = null,
                    timeId = rec.Id,
                    timeName = rec.Name,
                    timeStart = rec.Start,
                    timeEnd = rec.End,
                    timeRate = rec.Rate,
                    timeColor = rec.Color,
                    timeDays = days.ToArray(),
                    timeType = rec.Type,
                    serviceInternal = false,
                    date = null,
                    scheduledStart = null,
                    scheduledEnd = null,

                });
            }
            // -->

            return ret;
        }
    

         
    }

    public class SelectableTimeItem
    {
        public long? visitId { get; set; }
        public string visitName { get; set; }

        public long? jobId { get; set; }
        public string jobName { get; set; }

        public long? serviceId { get; set; }
        public string serviceName { get; set; }

        public bool? serviceInternal { get; set; }

        public string date { get; set; }

        public string scheduledStart { get; set; }
        public string scheduledEnd { get; set; }
        public long? timeId { get; set; }
        public int timeRate { get; set; }
        
        public string timeName { get; set; }
        public string timeStart { get; set; }
        public string timeEnd { get; set; }
        public TimeCategoryType timeType { get; set; }

        public int[] timeDays { get; set; }
        public string timeColor { get; set; }
        public List<string[]> calendarDays { get; set; }
        public string clientName { get; set; }
        public string description { get; set; }
        public int recordStatus { get; set; }
    }

}
