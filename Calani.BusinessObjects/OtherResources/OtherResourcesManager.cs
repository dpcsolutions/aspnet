﻿using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;


namespace Calani.BusinessObjects.OtherResources
{
    public class OtherResourcesManager : Generic.SmartCollectionManager<Model.other_resources, long>
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public OtherResourcesManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<other_resources> GetDbSet(Model.CalaniEntities db)
        {
            return db.other_resources;
        }

        internal override SmartQuery<other_resources> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.other_resources where r.id == id select r);
            return q;
        }

        public List<other_resources> GetOtherResources(long organizationId)
        {
            return GetBaseQuery().Query.Where(r => r.organizationId == organizationId).ToList();
        }

        public bool ResourceWithSuchCodeExists(string code, long organizationId)
        {
            code = code.ToLowerInvariant();

            return GetBaseQuery().Query.Any(r => r.organizationId == organizationId && r.code == code);
        }

        internal override SmartQuery<other_resources> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            q.Query = (from r in q.Context.other_resources select r);

            if (!IgnoreRecordStatus)
                q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            if (OrganizationID > -1)
                q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);

            return q;
        }

        public SmartAction<other_resources> SaveOrUpdate(long id, other_resources resource)
        {
            try
            {
                var result = (id > 0) ? Update(id, resource) : Add(resource);
                return result;
            }
            catch (Exception e)
            {
                _log.Info($"SaveOrUpdate. error. ", e);
                throw;
            }
        }
    }
}
