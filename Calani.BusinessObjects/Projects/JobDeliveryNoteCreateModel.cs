﻿namespace Calani.BusinessObjects.Projects
{
    public class JobDeliveryNoteCreateModel
    {
        public long jobId { get; set; }
        public long id { get; set; }
        public double qty { get; set; }
        public double remainderQty { get; set; }
        public string reference { get; set; }
    }
}