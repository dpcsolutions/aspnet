﻿using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Notifications;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Calani.BusinessObjects.Hangfire;
using Calani.BusinessObjects.Projects.Extensions;
using Hangfire;
using jobs = Calani.BusinessObjects.Model.jobs;

namespace Calani.BusinessObjects.Projects
{
    public class ProjectItemsUpdater
    {
        public long ProjectId { get; set; }
        
        public long OrganizationId { get; set; }
        //public List<AjaxService.ProjectItem> Items { get; set; }
        
        public AjaxService.ProjectUpdate Update { get; set; } 
        
        public bool DisableDbWriting { get; set; }
        
        public Model.jobs GeneratedJob { get; private set; }

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void UpdateDb(long userId, string action)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();


            // internal id (verify antidoublon)
            if (!DisableDbWriting)
            {
                if ((from r in db.jobs where r.recordStatus == 0 && r.organizationId == OrganizationId && r.id != ProjectId && r.internalId == Update.internalId select r).Count() > 0)
                {
                    throw new Exception("ERR_internalId_already_used");
                }
            }
            // -->

            var orgmgr = new OrganizationManager(OrganizationId);
            orgmgr.Load();

            bool insert = false;
            Model.jobs job = null;

            List<Model.services_jobs> lines = null;
            if (ProjectId > -1 && !DisableDbWriting)
            {
                var orgId = (from r in db.jobs where r.id == ProjectId select r.organizationId).First();
                if (OrganizationId > -1 && orgId != OrganizationId) throw new Exception("Not allowed to access this record");

                job = (from r in db.jobs where r.id == ProjectId && r.organizationId == OrganizationId select r).FirstOrDefault();

                lines = (from r in db.services_jobs where r.jobId == ProjectId && r.recordStatus == (int)RecordStatus.Active select r).ToList();

                job.updatedBy = userId;
            }
            else //Create new
            {
                lines = new List<Model.services_jobs>();
                
                job = new Model.jobs();
                job.dateCreated = DateTime.Now;
                job.createdBy = userId;

                if (action.EndsWith(":invoice") || action.EndsWith(":recurringinvoice")) 
                    job.dateInvoicing = job.dateCreated;

                if (action.Contains("quote"))
                    job.subtype = "Quote";

                if (action.Contains("deliverynote") )
                    job.subtype = "DeliveryNote";

                job.organizationId = OrganizationId;
                job.type = Convert.ToInt32(ProjectTypeEnum.Billable);
                db.jobs.Add(job);
                
                insert = true;                
            }

            GeneratedJob = job;

            if ((from r in db.clients where r.organizationId == job.organizationId && r.id == Update.clientId select r).Count() == 1)
            {
                job.clientId = Update.clientId;
            }
            else
            {
                throw new Exception("ERR_client_not_allowed");
            }

            if ((from r in db.contacts where r.clientId == job.clientId && r.id == Update.clientContact select r).Count() == 1)
            {
                job.clientContact = Update.clientContact;
            }
            else
            {
                throw new Exception("ERR_client_contact_not_allowed");
            }

            if ((from r in db.addresses where r.clientId == job.clientId && r.id == Update.clientAddress select r).Count() == 1)
            {
                job.clientContact = Update.clientContact;
            }
            else
            {
                throw new Exception("ERR_client_address_not_allowed");
            }
            if ((from r in db.jobs where r.clientId == job.clientId && r.id == Update.linkedjobid select r).Count() == 1)
            {
                job.linkedjobid = Update.linkedjobid;
            }
            if (insert && !DisableDbWriting)
            {
                db.SaveChanges();
                ProjectId = job.id;
            }

            if (Update.assigneeId > 0 &&
                (from r in db.contacts
                    where r.organizationId == job.organizationId && r.id == Update.assigneeId
                    select r).Count() == 1)
            {
                if (job.assigneeId != Update.assigneeId)
                {
                    job.assigneeId = Update.assigneeId;
                }

            }
            else
                job.assigneeId = null;


            /*Recurring invoices*/
            if (Update.recurringUpdate != null)
            {
                var recurringInfo = Update.recurringUpdate.CloneRecurringUpdate(job.id);
                var dbInfo = (from r in db.recurringinfos where r.id == recurringInfo.id select r).FirstOrDefault();

                if (dbInfo == null || RecurringJobsManager.RecurringInfosHasChanges(dbInfo, recurringInfo))
                {
                    db.recurringinfos.AddOrUpdate(recurringInfo);
                }
            }

            /*Visit for DN*/
            if (action.Contains("deliverynote") && !String.IsNullOrWhiteSpace(Update.deliveryDate))
            {
                var vMgr = new JobsManager(OrganizationId);
                var v = new visitToUpdate();
                v.applyToAllRepeat = false;
                v.clientId = job.clientId.ToString();
                v.culture = "fr";
                v.description = Update.description;

                var assigneeId = Update.assigneeId.ToString();
                if (!string.IsNullOrWhiteSpace(assigneeId))
                    v.resources = assigneeId + ",";

                DateTime deliveryDate;
                var parsed = DateTime.TryParseExact(Update.deliveryDate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out deliveryDate);
                if (parsed)
                {
                    v.startDate = deliveryDate.ToString("yyyy-MM-ddTHH:mm");
                    v.startTime = deliveryDate.ToString("HH:mm");
                    v.endDate = deliveryDate.AddHours(1).ToString("yyyy-MM-ddTHH:mm");
                    v.endTime = deliveryDate.AddHours(1).ToString("HH:mm");

                    if (job.visits.FirstOrDefault() != null)
                        v.id = job.visits.First().id.ToString();
                    else
                        v.id = "-1";

                    v.jobId = job.id.ToString();
                    v.numrepeat = 0;
                    v.internalServiceId = -1;
                    v.repeatGroupId = null;
                    v.repeatdays = String.Empty;
                    v.status = ((int)VisitStatusEnum.Planned).ToString();
                    v.type = ((int)VisitTypeEnum.PublicVisit).ToString();
                    vMgr.UpdateOrCreate(userId, v);
                }
            }
                

            var activeRecords = new List<Model.services_jobs>();

            var currentCurrency = GetJobCurrency(Update.currency, db, OrganizationId);
            if (Update.items != null)
            {
                foreach (var item in Update.items)
                {

                    if (!String.IsNullOrWhiteSpace(item.item))
                    {

                        Model.services_jobs dbItem = null;
                        
                        if (item.id != null)
                            dbItem = (from r in lines where r.id == item.id select r).FirstOrDefault();

                        if (dbItem == null)
                        {
                            dbItem = new Model.services_jobs();
                            db.services_jobs.Add(dbItem);

                            var anotherDBItem = db.services_jobs.SingleOrDefault(sj => sj.id == item.id);
                            if (anotherDBItem != null)
                            {
                                dbItem.contactId = anotherDBItem.contactId;
                                dbItem.timesheetDate = anotherDBItem.timesheetDate;
                            }
                        }
                        activeRecords.Add(dbItem);

                        dbItem.jobId = ProjectId;

                        Model.services service = GetServiceById(db, item.serviceId);
                        string serviceName = "";
                        long? serviceId = null;
                        if (service != null)
                        {
                            
                            serviceName = item.item; //keep the filled name
                            //serviceName = service.name; // instead of the 'unchanged' item name
                            serviceId = service.id;
                        }
                        else
                        {
                            serviceName = item.item;
                            serviceId = null;
                        }

                        dbItem.serviceName = serviceName;
                        dbItem.serviceId = serviceId;
                        dbItem.custom_number = !string.IsNullOrEmpty(item.customNumber) ? item.customNumber.Substring(0, Math.Min(10, item.customNumber.Length)) : item.customNumber;
                        dbItem.servicePrice = item.price;
                        dbItem.serviceQuantity = item.qty;
                        dbItem.unitName = item.unitName;


                        dbItem.taxName = null;
                        dbItem.taxRate = 0;
                        if (!String.IsNullOrWhiteSpace(item.tax))
                        {
                            Model.taxes tax = GetTaxes(db, item.tax);
                            if (tax != null)
                            {
                                dbItem.taxName = tax.name;
                                dbItem.taxRate = tax.rate;
                            }
                        }

                        dbItem.discount = item.discount;
                        dbItem.position = item.position;

                        if (dbItem.servicePrice == null) dbItem.servicePrice = 0;
                        dbItem.totalPriceExclTax = dbItem.servicePrice * dbItem.serviceQuantity;
                        double dDiscount = 0;
                        if (dbItem.discount != null) dDiscount = dbItem.discount.Value;
                        dbItem.totalPriceExclTax = Math.Round(dbItem.totalPriceExclTax.Value * (100 - dDiscount) / 100, 2, MidpointRounding.AwayFromZero);

                        dbItem.taxAmount = dbItem.totalPriceExclTax * dbItem.taxRate / 100;
                        dbItem.totalPrinceIncTax = dbItem.totalPriceExclTax + dbItem.taxAmount;

                        //  round all
                        dbItem.servicePrice = Math.Round(dbItem.servicePrice.Value, 2, MidpointRounding.AwayFromZero);
                        dbItem.totalPriceExclTax = Math.Round(dbItem.totalPriceExclTax.Value, 2, MidpointRounding.AwayFromZero);
                        dbItem.taxAmount = Math.Round(dbItem.taxAmount.Value, 2, MidpointRounding.AwayFromZero);
                        dbItem.totalPrinceIncTax = Math.Round(dbItem.totalPrinceIncTax.Value, 2, MidpointRounding.AwayFromZero);
                        // -->
                        
                        if (orgmgr.UsePriceIncludingVat && item.serviceVatIncl)
                        {
                            item.totalprice = item.price * item.qty;
                            dbItem.totalPriceExclTax = Math.Round(dbItem.totalPriceExclTax.Value * (100 - dDiscount) / 100, 2, MidpointRounding.AwayFromZero);
                        }

                        // selection
                        if (item.selected ?? false)
                        {
                            if (item.toinvoice == null || item.toinvoice <= 0)
                                dbItem.toinvoice = -1;
                        }

                        dbItem.is_checked = item.isChecked;

                        if (currentCurrency != null && currentCurrency.rate > 0)
                        {


                            dbItem.servicePriceInCurrency = dbItem.servicePrice;
                            dbItem.servicePrice /= currentCurrency.rate;
                            dbItem.servicePrice = Math.Round(dbItem.servicePrice.Value, 2, MidpointRounding.AwayFromZero);

                            dbItem.totalPriceExclTaxInCurrency = dbItem.totalPriceExclTax;
                            dbItem.totalPriceExclTax /= currentCurrency.rate;
                            dbItem.totalPriceExclTax = Math.Round(dbItem.totalPriceExclTax.Value, 2, MidpointRounding.AwayFromZero);

                            dbItem.totalPriceIncTaxInCurrency = dbItem.totalPrinceIncTax;
                            dbItem.totalPrinceIncTax /= currentCurrency.rate;
                            dbItem.totalPrinceIncTax = Math.Round(dbItem.totalPrinceIncTax.Value, 2, MidpointRounding.AwayFromZero);

                            dbItem.currency = currentCurrency.currency1;
                        }
                        else
                        {
                            dbItem.servicePriceInCurrency = null;
                            dbItem.totalPriceExclTaxInCurrency = null;
                            dbItem.totalPriceIncTaxInCurrency = null;
                            dbItem.currency = null;
                        }
                    }
                }
            }
        

            job.clientAddress = Update.clientAddress;
            job.discount = Update.discount;
            job.description = Update.description;
            job.lastUpdate = DateTime.Now;
            job.comment = Update.comment;
            job.send_copy_email_to = Update.sendCopyEmailTo;
            job.currency = currentCurrency?.currency1??String.Empty;
            job.color = !string.IsNullOrEmpty(Update.color) ? Update.color : job.color;
            job.customer_reference = Update.customerReference;
            job.type_intervention = Update.typeIntervention;
            job.location_intervention = Update.locationIntervention;
            job.discount_type = (sbyte)Update.discountType;
            job.discount_percent = Update.discountPercent;
            job.discount_type2 = (sbyte)Update.discountType2;
            job.discount_percent2 = Update.discountPercent2;
            job.discount_type3 = (sbyte)Update.discountType3;
            job.discount_percent3 = Update.discountPercent3;
            job.discount_type4 = (sbyte)Update.discountType4;
            job.discount_percent4 = Update.discountPercent4;
            job.skipMissingInvoiceNotif = Update.skipMissingInvoiceNotif;

            if (!String.IsNullOrWhiteSpace(Update.deliveredTo))
                job.deliveredTo = Update.deliveredTo;


            if (!String.IsNullOrWhiteSpace(Update.validity))
            {
                
                DateTime dtValidity = new DateTime();
                //if (DateTime.TryParse(Update.validity, out dtValidity))
                if (DateTime.TryParseExact(Update.validity, Tools.DefaultDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dtValidity))
                {
                    if(job.dateInvoicing != null) // invoice
                    {
                        job.dateInvoiceDue = dtValidity;
                    }

                    else if(job.dateOpenJob != null ) // quote
                    {
                        job.dateQuoteValidity = dtValidity;
                       
                    }

                   // else // open
                    {
                        job.dateJobValidity = dtValidity;
                    }
                }
                else
                {
                    if (action.EndsWith(":invoice"))
                    {
                        throw new Exception($"ERR_due_date_mandatory. Update.validity = {Update.validity ?? string.Empty}");
                    }
                }
            }
            else
            {
                if (action.EndsWith(":invoice"))
                    throw new Exception($"ERR_due_date_mandatory. Update.validity = {Update.validity ?? string.Empty}");
            }

            if (job.dateOpenJob != null || 
                (Update.id == -1 
                    && Update.batch > 0 
                    && (Update.action != null && Update.action.Contains("save:open"))
                )
            ) 
            {
                job.contractualUnitsCount = Update.batch;
            }


            if (job.dateInvoicing == null)
            {
                // ce n'est pas une facture (c'est un devis ou un projet)
                if (Update.validity != null)
                {
                    DateTime dtValidity = new DateTime();
                    if (DateTime.TryParseExact(Update.validity, Tools.DefaultDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dtValidity))
                    {
                        job.dateQuoteValidity = dtValidity;
                    }

                }

                job.rating = Update.rating;
            }


            if (!DisableDbWriting)
            {
                if(String.IsNullOrWhiteSpace(Update.internalId))
                {
                    throw new Exception("ERR_internalId_is_mandatory");
                }

                // internal id (verify antidoublon)
                if ((from r in db.jobs where r.organizationId == job.organizationId && r.id != job.id && r.internalId == Update.internalId && r.recordStatus == 0 select r).Count() == 0)
                {
                    job.internalId = Update.internalId;
                }
                else
                {
                    throw new Exception("ERR_internalId_already_used");
                }
                // -->

                db.SaveChanges();


                job = (from r in db.jobs where r.id == job.id select r).FirstOrDefault();
            }

            var calc = UpdateTotal(job, activeRecords, currentCurrency, orgmgr.RoundAmount);

            if (!DisableDbWriting)
            {
                db.SaveChanges();


                // delete old lines
                List<long> activeRecordsIds = (from r in activeRecords select r.id).ToList();
                foreach (var item in lines.Where(l => (Update.linesToRemove ?? new long[0]).Contains(l.id)))
                {
                    item.recordStatus = (int)RecordStatus.Deleted;
                    item.updatedAt = DateTime.Now;
                    item.updatedBy = userId;
                }
                db.SaveChanges();
                // -->
            }


            
            // payments
            if (Update.payments != null)
            {
                Projects.InvoicePaymentsManager paymr = new InvoicePaymentsManager(OrganizationId, job.id);
                var exPaymentsQuery = paymr.GetBaseQuery();
                var exPaymentsRecords = exPaymentsQuery.Query.ToList();

                List<long> recordsToKeep = new List<long>();

                foreach (var item in Update.payments)
                {
                    //DateTime dt = item.datePayed
                    DateTime? dt = Generic.CalendarTools.ParseDate(item.datePayed);
                    if (dt != null)
                    {
                        var rec = (from r in exPaymentsRecords where r.datePayed == dt.Value select r).FirstOrDefault();
                        if (rec != null)
                        {
                            rec.comment = item.comment;
                            rec.amountPayed = item.amountPayed;
                            recordsToKeep.Add(rec.id);
                        }
                        else
                        {
                            rec = new Model.payments();
                            rec.comment = item.comment;
                            rec.amountPayed = item.amountPayed;
                            rec.datePayed = dt;
                            rec.jobId = job.id;
                            rec.recordStatus = 0;
                            exPaymentsQuery.Context.payments.Add(rec);
                        }
                    }
                }

                var recordsToDelete = (from r in exPaymentsRecords where !recordsToKeep.Contains(r.id) select r).ToList();
                foreach (var recordToDelete in recordsToDelete)
                {


                    exPaymentsQuery.Context.payments.Remove(recordToDelete);
                }

                if (!DisableDbWriting)
                {
                    exPaymentsQuery.Context.SaveChanges();
                }
            }

            if (!DisableDbWriting)
            {
                db.Dispose();
                db = new Model.CalaniEntities();

                job = (from r in db.jobs where r.id == job.id select r).FirstOrDefault();
            }

            calc = new InvoiceDueCalculator(job);
            calc.Recalc(orgmgr.UsePriceIncludingVat);

            if (!DisableDbWriting)
            {
                db.SaveChanges();
            }

            // -->


            if (!DisableDbWriting)
            {
                if (Update.tsImports != null)
                {
                    var tsrecords = (from r in db.timesheetrecords
                                     where r.timesheets.organizationId == OrganizationId
                                        && Update.tsImports.Contains(r.id)
                                     select r).ToList();
                    foreach (var tsr in tsrecords)
                    {
                        tsr.imported = true;
                    }

                }

                if (Update.expImports != null)
                {
                    var tsrecords = (from r in db.expenses
                                     where r.contacts.organizationId == OrganizationId
                                        && Update.expImports.Contains(r.id)
                                     select r).ToList();
                    foreach (var tsr in tsrecords)
                    {
                        tsr.imported = true;
                    }

                }


                db.SaveChanges();
            }

            if (Update.assigneeId.HasValue)
            {
                //notify user of change
                NotificationManager nm = new NotificationManager();

                var msg = String.Empty;
                var users = new List<long>() { job.assigneeId.Value };
                
                if (job != null && job.clients.companyName != null)
                    msg += $" pour {job.clients.companyName}";

                if (job != null && job.city != null)
                    msg += $" à {job.city}";

                if (job != null && !String.IsNullOrEmpty(job.description))
                    msg += $". Remarque: {job.description}";

                var category = "updateProject";

                var title = "Changement de project " + job.internalId;

                _log.Debug("Sending notification to:" + String.Join(",", users));
                
               //nm.sendNotifications(msg, title, users, new Dictionary<string, string>() { { "projId", job.id.ToString() }, { "client", (job != null && job.clients.companyName != null ? job.clients.companyName : "") } }, category);
            }
        }

        public static currency GetJobCurrency(string currency, Model.CalaniEntities db, long orgId)
        {
            currency currentCurrency = null;
            if (currency != null)
            {
                long currencyId = 0;
                long.TryParse(currency, out currencyId);
                var c = (from r in db.currency where r.id == currencyId && r.organizationId == orgId select r).FirstOrDefault();
                if (c != null && c.rate > 0)
                {
                    currentCurrency = c;
                }
            }
            return currentCurrency;
        }

        public static InvoiceDueCalculator UpdateTotal(jobs job, List<Model.services_jobs> activeRecords, Model.currency currentCurrency, bool roundTotal)
        {
            if (job.organizationId == null) return null;
            
            var orgMgr = new OrganizationManager(job.organizationId.Value);
            orgMgr.Load();
            
            var jobExt = new jobsExtended(job)
            {
                services_jobs = activeRecords
            };

            if (job.discount == null) job.discount = 0;

            job.total = jobExt.CalculateTotal(roundTotal, inCurrency: false);
            job.totalInCurrency = jobExt.CalculateTotal(roundTotal, inCurrency: true);
            job.totalWithoutTaxes = jobExt.CalculateTotalExclTaxDiscounted(inCurrency: false);
            
            if (currentCurrency != null && currentCurrency.rate > 0)
            {
                job.currency = currentCurrency.currency1;
                job.currencyRate = currentCurrency.rate;
            }
            /*else 
            {
                job.totalInCurrency = null;
                job.currency = null;
                job.currencyRate = null;
            }*/

            InvoiceDueCalculator calc = new InvoiceDueCalculator(job, currentCurrency);
            calc.Recalc(orgMgr.UsePriceIncludingVat);
            return calc;
        }

        private Model.services GetServiceById(Model.CalaniEntities db, long? serviceId)
        {
            if (serviceId == null) return null;

            var service =  (from r in db.services where r.organizationId == OrganizationId && (r.id == serviceId.Value) select r).FirstOrDefault();

            return service;
        }

        /// <summary>
        /// Try to parse service id or ref number from the line's text
        /// Example 1: "555 - Carton" - service Id = 555, service description = Carton
        /// Example 2 (edited by user): "1 - 555 - Carton" - ordinal number = 1, service id = 555, service description = Carton
        /// </summary>
        private Model.services GetServiceByName(Model.CalaniEntities db, string lineText)
        {
            if (String.IsNullOrWhiteSpace(lineText))
                return null;

            string code;

            var dashesCount = lineText.Count(c => c == '-');
            var parts = lineText.Split('-');

            switch (dashesCount)
            {
                // No dashes - no service code
                case 0: code = ""; break;

                // 1 dash - we have a service code at the beginning of the string
                case 1: code = parts.First(); break;

                // More than 1 dash - we assume that service code goes right after the 1st dash
                default: code = parts[1]; break;
            }

            code = code.Trim().ToLower();

            if (String.IsNullOrWhiteSpace(code))
                return null;

            var service = (from r in db.services where r.organizationId == OrganizationId && (r.name.ToLower() == code.ToLower() || r.internalId.ToLower() == code.ToLower()) select r).FirstOrDefault();
            
            return service;
        }

        private Model.taxes GetTaxes(Model.CalaniEntities db, string n)
        {
            string code = n.Trim().ToLower();
            return (from r in db.taxes where r.organizationId == OrganizationId && r.name.ToLower() == code.ToLower() select r).FirstOrDefault();
        }

        public List<long> UpdateImportedTimeRecords(List<TimeSheetRecordImport> timeRecords)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();
            var res = new List<long>();
            if (timeRecords != null)
            {

                var records = timeRecords.GroupBy(r => new {r.ServiceName, r.EmployeeName, date = r.StartDate .GetValueOrDefault().DayOfYear});
                foreach (var record in records)
                {
                    Model.services_jobs dbItem = null;
                    dbItem = new Model.services_jobs();
                    db.services_jobs.Add(dbItem);

                    var item = record.FirstOrDefault();
                    
                    if (item != null)
                    {
                        dbItem.jobId = ProjectId;
                        var service = GetServiceByName(db, record.Key.ServiceName);

                        var serviceName = String.IsNullOrEmpty(item.ServiceName) ? "--" : item.ServiceName;

                        dbItem.contactId = item.EmployeeId;
                        dbItem.timesheetDate = item.StartDate.GetValueOrDefault().Date;
                        dbItem.serviceName = item.Date + ", " + serviceName + ", " + item.EmployeeName + " - " + TimeSpan.FromHours(record.Sum(r => r.Duration)).ToString("hh':'mm''");
                        if (service != null)
                            dbItem.serviceId = service.id;

                        dbItem.servicePrice = service?.unitPrice;
                        dbItem.serviceQuantity = record.Sum(d => d.Duration); 


                        dbItem.taxName = null;
                        dbItem.taxRate = 0;
                        if (service?.taxes != null)
                        {
                            dbItem.taxName = service.taxes.name;
                            dbItem.taxRate = service.taxes.rate;
                        }

                        dbItem.discount = 0;

                        if (dbItem.servicePrice == null)
                            dbItem.servicePrice = 0;

                        dbItem.totalPriceExclTax = dbItem.servicePrice * dbItem.serviceQuantity;
                        double dDiscount = 0;

                        if (dbItem.discount != null)
                            dDiscount = dbItem.discount.Value;
                        dbItem.totalPriceExclTax = Math.Round(dbItem.totalPriceExclTax.Value * (100 - dDiscount) / 100, 2);

                        dbItem.taxAmount = dbItem.totalPriceExclTax * dbItem.taxRate / 100;
                        dbItem.totalPrinceIncTax = dbItem.totalPriceExclTax + dbItem.taxAmount;

                        //  round all
                        dbItem.servicePrice = Math.Round(dbItem.servicePrice.Value, 2, MidpointRounding.AwayFromZero);
                        dbItem.totalPriceExclTax = Math.Round(dbItem.totalPriceExclTax.Value, 2, MidpointRounding.AwayFromZero);
                        dbItem.taxAmount = Math.Round(dbItem.taxAmount.Value, 2, MidpointRounding.AwayFromZero);
                        dbItem.totalPrinceIncTax = Math.Round(dbItem.totalPrinceIncTax.Value, 2, MidpointRounding.AwayFromZero);
                        // -->

                        foreach (var r in record)
                        {
                            var tsr = db.timesheetrecords.FirstOrDefault(i => i.id == r.Id);

                            if (tsr != null)
                                tsr.imported = true;

                        }


                        db.SaveChanges();

                        res.Add(dbItem.id);
                    }
                }
            }
          return res;
        }
    }
}
