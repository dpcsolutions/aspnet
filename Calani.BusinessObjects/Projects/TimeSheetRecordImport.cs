﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.Projects
{
    public class TimeSheetRecordImport
    {

        public long Id { get; set; }
        public string Link { get; set; }
        public long TimeSheetId { get; set; }
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Week { get; set; }
        public bool TimeSheetValidated { get; set; }
        public bool TimeSheetInProgress { get; set; }
        public string Date { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public double Rate { get; set; }
        public double Duration { get; set; }

        public long ServiceId { get; set; }
        public string ServiceName { get; set; }

        public bool Imported { get; set; }
        public bool CanBeImported { get; set; }


        public DateTime? StartDate { get; set; }
        public TimeSheetRecordImport()
        {

        }

        public TimeSheetRecordImport(timesheetrecords r)
        {
            Id = r.id;
            var emp = r.timesheets.contacts;
            TimeSheetId = r.timesheetId.Value;
            Week = r.timesheets.year.Value + "-" + r.timesheets.week.Value;
            Link = "../../Time/Sheets/Sheet.aspx?id=" + TimeSheetId;
            EmployeeId = emp.id;
            EmployeeName = "";
            if (emp != null && emp.firstName != null) 
                EmployeeName += " " + emp.firstName;

            if (emp != null && emp.lastName != null) 
                EmployeeName += " " + emp.lastName;

            EmployeeName = EmployeeName.Trim();

            TimeSheetValidated = r.timesheets.acceptedDate != null;

            TimeSheetInProgress = !TimeSheetValidated;

            Date = r.startDate.Value.ToString("dd.MM.yyyy");
            StartDate = r.startDate;

            StartTime = r.startDate.Value.ToString("HH:mm");

            EndTime = r.endDate.Value.ToString("HH:mm");

            Rate = 100;

            if (r.rate != null)
                Rate = r.rate.Value;

            Duration = Math.Round(r.endDate.Value.Subtract(r.startDate.Value).TotalHours * 100 / Rate, 2, MidpointRounding.AwayFromZero);

            Imported = r.imported;
            CanBeImported = !Imported;

            if (r.services != null && !String.IsNullOrWhiteSpace(r.services.internalId))
                ServiceName = r.services.internalId + " - " + r.services.name;
            else
                ServiceName = r.serviceName;

            ServiceId = r.serviceId.HasValue? r.serviceId.Value: 0;
        }

    }
}
