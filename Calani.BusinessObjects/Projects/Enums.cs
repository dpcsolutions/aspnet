﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Projects
{
    public enum ProjectTypeEnum
    {
        Internal = 0,
        Billable = 1
    }
    


    // TODO : refresh this enum. not really implemented yet.
    public enum VisitTypeEnum
    {
        PublicVisit = 1,
        PrivateVisit = 2,
        ResourceHolidays = 3,
        PublicHolidays = 4,
        AbsenceRequest = 5,
        CustomCalendarEvent = 6,
    }
    public enum VisitStatusEnum
    {
        Planned = 4,
        Done = 1,
        InProces = 0,
        Cancelled = 3,
        Rescheduled = 2
    }

    public enum ProjectStatus
    {
        QuoteDraft = 0,
        QuoteSent = 1,
        QuoteAccepted = 2,
        QuoteRefused = 3,
        QuoteOutDated = 4,
        DeliveryNoteDraft = 5,
        DeliveryNoteSent = 6,
        DeliveryNoteAccepted = 7,
        DeliveryNoteRefused = 8,
        JobOpened = 9,
        JobOnHold = 10,
        JobFinished = 11,
        JobOutDated = 12,
        InvoiceDraft = 13,
        InvoicePaymentDue = 14,
        InvoicePaymentLate = 15,
        InvoicePaid = 16
    }
}
