﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Projects
{
    public class ReminderManager
    {
        long _organizationId = -1;
        public ReminderManager(long orgId)
        {
            _organizationId = orgId;
        }


        public List<InvoiceReminder> GetReminders(long jobid)
        {
            var db = new Model.CalaniEntities();
            var records = (from r in db.jobreminders
                           where r.jobid == jobid 
                           && r.jobs.organizationId == _organizationId
                           && r.date != null
                           orderby r.date ascending
                           select r).ToList();

            List<InvoiceReminder> ret = new List<InvoiceReminder>();
            foreach (var item in records)
            {
                ret.Add(new InvoiceReminder
                {
                    Count = ret.Count + 1,
                    Date = item.date.Value,
                    Id = item.id
                });
            }
            return ret;
        }

        public bool AddReminder(long jobid, DateTime? date = null)
        {
            if (date == null) date = DateTime.Now;

            var db = new Model.CalaniEntities();
            var verif = (from r in db.jobs where r.id == jobid && r.organizationId == _organizationId select r).Count();

            if(verif == 1)
            {
                db.jobreminders.Add(new Model.jobreminders
                {
                    date = date,
                    jobid = jobid

                });
                bool ret = db.SaveChanges() > 0;
                return ret;
            }

            return false;
        }

        public bool RemoveReminder(long jobid, long id)
        {
            var db = new Model.CalaniEntities();
            var record = (from r in db.jobreminders
                          where r.jobid == jobid
                          && r.jobs.organizationId == _organizationId
                          && r.date != null
                          && r.id == id
                          orderby r.date ascending
                          select r).FirstOrDefault();
            if (record != null)
            {
                db.jobreminders.Remove(record);
                db.SaveChanges();
                return true;
            }
            return false;
        }

    }

    public class InvoiceReminder
    {
        public int Count { get; set; }
        public DateTime Date { get; set; }

        public long Id { get; set; }
    }
}
