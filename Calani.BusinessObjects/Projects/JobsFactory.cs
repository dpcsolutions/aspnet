﻿namespace Calani.BusinessObjects.Projects
{
    public class JobsFactory
    {
        public jobsExtended ToInvoiceLightweight(Model.jobs entity, bool isVatIncluded)
        {
            var model = new jobsExtended();

            model.assigneeId = entity.assigneeId;
            model.city = entity.city;
            model.clientAddress = entity.clientAddress;
            model.clientContact = entity.clientContact;
            model.clientId = entity.clientId;
            model.clients = entity.clients;
            model.color = entity.color;
            model.comment = entity.comment;
            model.contractualUnitsCount = entity.contractualUnitsCount;
            model.country = entity.country;
            model.createdBy = entity.createdBy;
            model.currency = entity.currency;
            model.currencyRate = entity.currencyRate;
            model.dateCreated = entity.dateCreated;
            model.dateFullyPaid = entity.dateFullyPaid;
            model.dateFullyPaid = entity.dateFullyPaid;
            model.dateInvoiceDue = entity.dateInvoiceDue;
            model.dateInvoicing = entity.dateInvoicing;
            model.dateInvoicingOpen = entity.dateInvoicingOpen;
            model.dateInvoicePending = entity.dateInvoicePending;
            model.dateJobDone = entity.dateJobDone;
            model.dateJobHeld = entity.dateJobHeld;
            model.dateJobValidity = entity.dateJobValidity;
            model.dateOpenJob = entity.dateOpenJob;
            model.dateQuoteAccepted = entity.dateQuoteAccepted;
            model.dateQuoteRefused = entity.dateQuoteRefused;
            model.dateQuoteSent = entity.dateQuoteSent;
            model.dateQuoteValidity = entity.dateQuoteValidity;
            model.deliveredTo = entity.deliveredTo;
            model.description = entity.description;
            model.discount = entity.discount;
            model.discount_percent = entity.discount_percent;
            model.discount_type = entity.discount_type;
            model.discount_percent2 = entity.discount_percent2;
            model.discount_type2 = entity.discount_type2;
            model.discount_percent3 = entity.discount_percent3;
            model.discount_type3 = entity.discount_type3;
            model.discount_percent4 = entity.discount_percent4;
            model.discount_type4 = entity.discount_type4;
            model.filterByDate = entity.filterByDate;
            model.id = entity.id;
            model.importedIn = entity.importedIn;
            model.internalId = entity.internalId;
            model.internalNote = entity.internalNote;
            model.invoiceDue = entity.invoiceDue;
            model.invoiceDueInCurrency = entity.invoiceDueInCurrency;
            model.lastUpdate = entity.lastUpdate;
            model.nb = entity.nb;
            model.npa = entity.npa;
            model.organizationId = entity.organizationId;
            model.parentId = entity.parentId;
            model.rating = entity.rating;
            model.recordStatus = entity.recordStatus;
            model.send_copy_email_to = entity.send_copy_email_to;
            model.sentToEmail = entity.sentToEmail;
            model.sentToName = entity.sentToName;
            model.street = entity.street;
            model.street2 = entity.street2;
            model.subtype = entity.subtype;            
            model.total = entity.total;
            model.totalInCurrency = entity.totalInCurrency;
            model.totalWithoutTaxes = entity.totalWithoutTaxes;
            model.type = entity.type;
            model.updatedBy = entity.updatedBy;
            model.location_intervention = entity.location_intervention;
            model.type_intervention = entity.type_intervention;
            model.customer_reference = entity.customer_reference;

            model.payments = entity.payments;
            model.jobreminders = entity.jobreminders;
            model.recurringinfos = entity.recurringinfos;
            model.recurringinfos1 = entity.recurringinfos1;

            #region commentedOutCode30102022Panco
            
            //visits = entity.visits;
            //work_reports = entity.work_reports;
            //timesheetrecords = entity.timesheetrecords;
            //timesheet_records_services = entity.timesheet_records_services;
            //addresses = entity.addresses;
            //attachments = entity.attachments;
            //contacts = entity.contacts;
            //contacts1 = entity.contacts1;
            //contacts2 = entity.contacts2;
            //expenses = entity.expenses;
            //jobs1 = entity.jobs1;
            //jobs2 = entity.jobs2;
            //notes = entity.notes;
            //organizations = entity.organizations;
            //resource_allocations = entity.resource_allocations;
            //services_jobs = entity.services_jobs;

            #endregion

            model.CalculateProperties(entity, includeReminders: true, isVatIncluded);

            return model;
        }
    }
}
