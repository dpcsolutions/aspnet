﻿using System.Globalization;
using System.Linq;

namespace Calani.BusinessObjects.Projects
{
    public class ProjectsExtractor
    {
        public long OrganizationId { get; private set; }

        public ProjectsExtractor(long organizationId)
        {
            OrganizationId = organizationId;
        }

        public byte[] Extract(long projectId, string culture)
        {
            var gen = new DocGenerator.ExcelTableGenerator();
            var localizer = SharedResource.Resource;
            var cI = CultureInfo.GetCultureInfo(culture);
            gen.Borders = true;
            gen.FitColumns = true;
            
            var dt = new System.Data.DataTable();
            dt.Columns.Add("#");
            dt.Columns.Add(localizer.GetString("Item", cI));
            dt.Columns.Add(localizer.GetString("Qty", cI));
            dt.Columns.Add(localizer.GetString("Unit_Price", cI));
            dt.Columns.Add(localizer.GetString("Discount_P", cI));
            dt.Columns.Add(localizer.GetString("Total", cI));
            dt.Columns.Add(localizer.GetString("Tax", cI));

            var db = new Model.CalaniEntities();

            var q = (from r
                 in db.services_jobs
                 where r.jobId == projectId 
                 && r.recordStatus == 0
                 orderby r.position //string.IsNullOrEmpty(r.custom_number), r.custom_number  ascending
                 select r);

            var data = q.ToList();
            
            foreach (var r in data)
            {
                var nr = dt.NewRow();
                nr[0] = r.custom_number;
                nr[1] = r.serviceName;
                nr[2] = r.serviceQuantity;
                nr[3] = r.servicePrice;
                nr[4] = r.discount;
                nr[5] = r.totalPriceExclTax;
                nr[6] = r.taxRate;
                dt.Rows.Add(nr);
            }

            gen.Table = dt;
            return gen.GenerateDocument();

        }
    }
}
