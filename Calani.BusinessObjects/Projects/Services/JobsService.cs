﻿using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Projects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Calani.BusinessObjects.Projects.Extensions;

namespace Calani.BusinessObjects.Projects
{
    public class JobsService
    {
        private readonly ProjectsManager _projectsManager;

        public JobsService(ProjectsManager projectsManager)
        {
            _projectsManager = projectsManager;
        }

        public List<InvoiceModel> ListInvoices(DateRange dateRange)
        {
            var list = _projectsManager.ListInvoices(orgID: _projectsManager.OrganizationID, clientId: null, dateRange: dateRange);

            foreach (var item in list)
            {
                item.internalId = String.IsNullOrWhiteSpace(item.internalId) ?
                        SharedResource.Resource.GetString("Open") : item.internalId;

                item.description = !String.IsNullOrWhiteSpace(item.description) ?
                        TruncateWithEllipsis(item.description) : item.description;
            }

            return list.Where(x => x.recurringinfos == null || !x.recurringinfos1.Any()).ToList()
                .Select(i => new InvoiceModel(i)).ToList();
        }

        public List<InvoiceModel> ListRecurringInvoices()
        {
            var list = _projectsManager.ListInvoices(orgID: _projectsManager.OrganizationID, clientId: null);
            var recurringMgr = new RecurringManager(_projectsManager.OrganizationID);

            foreach (var item in list)
            {
                item.total = item.totalInCurrency;
                item.internalId = String.IsNullOrWhiteSpace(item.internalId) ?
                        SharedResource.Resource.GetString("Open") : item.internalId;
                item.description = !String.IsNullOrWhiteSpace(item.description) ?
                        TruncateWithEllipsis(item.description) : item.description;
                item.recurringinfos = recurringMgr.GetActualForInvoice(item.id).CloneRecurringInfo();
            }

            return list.Where(x => x.recurringinfos != null).ToList()
                .Select(i => new InvoiceModel(i)).ToList();
        }

        public List<DeliveryNoteModel> ListDeliveryNotes(DateRange dateRange, long? assigneeId, long? creatorId = null)
        {
            _projectsManager.FixQuotesOrDeliveryNoteWithoutSubType();
            var list = _projectsManager.ListDeliveryNotes(custid: null, assigneeId: assigneeId, dateRange: dateRange, creatorId: creatorId);

            foreach (var item in list)
            {
                item.internalId = String.IsNullOrWhiteSpace(item.internalId) ?
                        SharedResource.Resource.GetString("Open") : item.internalId;

                item.description = !String.IsNullOrWhiteSpace(item.description) ?
                        TruncateWithEllipsis(item.description) : item.description;
            }

            return list.Select(i => new DeliveryNoteModel(i)).ToList();
        }

        public List<QuoteModel> ListQuotes(DateRange dateRange, long? assigneeId)
        {
            _projectsManager.FixQuotesOrDeliveryNoteWithoutSubType();
            var list = _projectsManager.ListQuotes(assigneeId: assigneeId, dateRange: dateRange);

            foreach (var item in list)
            {
                item.internalId = String.IsNullOrWhiteSpace(item.internalId) ?
                        SharedResource.Resource.GetString("Open") : item.internalId;

                item.description = !String.IsNullOrWhiteSpace(item.description) ?
                        TruncateWithEllipsis(item.description) : item.description;
            }

            return list.Select(i => new QuoteModel(i)).ToList();
        }

        #region Private Methods

        private string TruncateWithEllipsis(string text)
        {
            var maxLength = 144;
            text = text.Replace("\r", " ").Replace("\n", " ").Replace("  ", " ");
            if (text.Length > maxLength)
            {
                text = text.Substring(0, maxLength) + "...";
            }

            return text;
        }

        #endregion Private Methods
    }
}
