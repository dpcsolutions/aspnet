﻿using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Projects.Models;
using HiQPdf;
using log4net;
using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Calani.BusinessObjects.Projects
{
    public class WorkReportsManager : Generic.SmartCollectionManager<Model.work_reports, long>
    {
        public static string DIR;
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public WorkReportsManager(long orgId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = orgId;
        }

        public static string PublicUrl { get; set; }
        public long EmployeeFilter { get; set; }

        public long SaveReport(long userId, long orgId, WorkReportModel model)
        {
            try
            {
                SmartAction<work_reports> res = null;

                var id = model.Id;
                work_reports entity;
                if (model.Id < 1)
                {
                    entity = new work_reports()
                    {
                        created_at = DateTime.UtcNow,
                        created_by = userId,
                        recordStatus = (int)RecordStatus.Active,
                        job_id = model.ProjectId,
                        organization_id = orgId
                    };


                    entity = this.Add(entity).Record;
                    id = entity.id;
                }
                else
                {
                    entity = GetById(id).Query.FirstOrDefault();
                    entity.updated_by = userId;
                    entity.updated_at = DateTime.UtcNow;

                }

                entity.job_id = model.ProjectId;
                entity.title = model.Title;
                entity.description = model.Description;
                entity.type = (int)model.Type;
                entity.status = (int)model.Status;




                model.Id = id;

                if (model.Status == WorkReportStatus.Validated)
                {
                    var orgMgr = new OrganizationManager(orgId);
                    orgMgr.Load();

                    entity.status = (int)model.Status;
                    entity.validated_at = DateTime.UtcNow;
                    entity.validated_by = userId;

                    this.Update(model.Id, entity);
                    
                    entity = GetById(model.Id).Query.FirstOrDefault();
                    model = entity.ToDto(orgMgr.CompanyName);
                    model.LogoImage = orgMgr.Logo;

                    var file = GeneratePdf(model);
                    entity.pdf = file;
                    model.Pdf = file;
                    var admin = entity.organizations.contacts.FirstOrDefault(c => c.type == 1)?.ToDto();
                    model.Admin = admin;
                    SendNotification(model);
                }

                this.Update(id, entity);

                return id;
            }
            catch (Exception e)
            {
                _log.Error($"SaveWorkReport. Error. rid:{model.Id}, orgId:{orgId}, userId:{userId}, status:{model.Status}",e);
                throw;
            }
   
        }

        public WorkReportModel GetLatest(WorkReportStatus status, string companyName)
        {
            var q = GetBaseQuery();

            var queue = (from r in q.Query
                where r.recordStatus == (int)RecordStatus.Active && r.status == (int)status
                         select r).FirstOrDefault();

            return queue.ToDto(companyName);
        }

        public int GetStat(WorkReportStatus status)
        {
            List<WorkReportModel> ret = null;

            var q = GetBaseQuery();

            var startDate = new DateTime(DateTime.UtcNow.Year,1,1);
            var endDate = startDate.AddYears(1).AddDays(-1);
            var queue = (from r in q.Query
                where r.recordStatus == (int) RecordStatus.Active && r.status == (int) status
                                                                  && r.created_at >= startDate &&
                                                                  r.created_at <= endDate
                select r);


            return queue.Count();
        }

        private void SendNotification(WorkReportModel model)
        {
            try
            {
                var mailSender = new MailSender();

                mailSender.Attachments.Add(new Generic.MailAttachment
                {
                    File = Path.Combine(GetDir().FullName, model.Pdf),
                    CustomName = model.Title
                });


                mailSender.MailTo.Email = model.CreatedBy.PrimaryEmail;
                mailSender.MailTo.UserId = model.CreatedBy.Id;
                mailSender.CustomSubject = "Email pour informer de la validation du rapport de travail";

                mailSender.Variables.Add("UserName", model.CreatedBy.FullName);
                mailSender.Variables.Add("ReportId", model.Id.ToString());

                mailSender.Variables.Add("ValidationDate", model.ValidatedAt.Value.ToString("g"));
                mailSender.Variables.Add("Validator", model.ValidatedBy?.FullName);
                mailSender.Variables.Add("AdminName", model.Admin?.FullName);

                //mailSender.Variables.Add("buttonlink", System.Configuration.ConfigurationManager.AppSettings["PublicUrl"] + "Projects/Invoices/");

                mailSender.SendSmartHtmlEmail("ges_send_work_report_validation_email_fr.html", "send_document");
            }
            catch (Exception e)
            {
                _log.Error($"WorkReport->SendNotification. id:{model.Id},status:{model.Status}", e);
            }
                
        }

        private string GeneratePdf(WorkReportModel model)
        {
            try
            {
                string template = Tools.GetEmbeddedResourceText("WorkReportPdfTemplate.html", Assembly.GetExecutingAssembly());
                var logo = model.LogoImage;

                var desc = HttpUtility.UrlDecode(model.Description, System.Text.Encoding.UTF8);

                var templateModel = new WorkReportTemplateModel
                {
                    Title = model.Title,
                    Description = desc,
                    Year = DateTime.UtcNow.Year,
                    Logo= logo ,
                    CreatorStr = String.Format("Créateur: {0} à {1}", model.CreatedBy.FullName, model.CreatedAt.Value.ToString("g")),
                    ValidatorStr = model.Status == WorkReportStatus.Validated? String.Format("Validateur: {0} à {1}", model.ValidatedBy?.FullName, model.ValidatedAt != null ? model.ValidatedAt.Value.ToString("g") : string.Empty) : String.Empty,
                    ProjectStr = String.Format("Projet: {0}", model.ProjectInternalId),
                    CompanyName = model.CompanyName ?? "GesMobile",
                };

                var result = Engine.Razor.RunCompile(template, Guid.NewGuid().ToString(), typeof(WorkReportTemplateModel), templateModel);

                // create the HTML to PDF converter
                var converter = new HtmlToPdf();
                
                // set PDF page size and orientation
                converter.Document.PageSize = PdfPageSize.A4;
                converter.Document.PageOrientation = PdfPageOrientation.Portrait;

                // set PDF page margins
                converter.Document.Margins = new PdfMargins(5);
                
                var file = String.Format("{0}_{1}.pdf", model.ProjectInternalId, model.Id);

                var path = Path.Combine(GetDir().FullName, file);
                // convert HTML to PDF
                converter.ConvertHtmlToFile(result, string.Empty, path);

                return file;
            }
            catch (Exception e)
            {
                _log.Error($"WorkReport->GeneratePdf. id:{model.Id}, status:{model.Status}", e);
                return null;
            }
          
        }

        public DirectoryInfo GetDir()
        {
            DirectoryInfo di = new DirectoryInfo(Path.Combine(DIR, OrganizationID.ToString()));
            if (!di.Exists)
                di.Create();
            return di;
        }
        
        public WorkReportModel GetReport(long id, long pid, string companyName)
        {
            var model = new WorkReportModel
            {
            };

            if (id > 0)
            {
                var q = GetBaseQuery().Query;
                var entity = q.FirstOrDefault(t => t.id == id && t.recordStatus == (int)RecordStatus.Active);
                model = entity.ToDto(companyName);
            }
            else
            {
                var db = new CalaniEntities();
                model.ProjectId = pid;
                model.ProjectInternalId = db.jobs.FirstOrDefault(j=>j.id ==pid )?.internalId;
                model.Status = WorkReportStatus.Draft;
                model.Type = WorkReportType.Project;
                
            }
            
            return model;
        }

        internal override DbSet<work_reports> GetDbSet(Model.CalaniEntities db)
        {
            return db.work_reports;
        }

        internal override SmartQuery<work_reports> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.work_reports where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(work_reports record)
        {
            if (record != null )
                return record.organization_id;
            return -1;
        }

        internal override SmartQuery<work_reports> GetBaseQuery()
        {
            var q = base.GetBaseQuery();
            // base
            q.Query = (from r in q.Context.work_reports where r.organization_id == OrganizationID && r.recordStatus == 0 select r);

           

            if (!IgnoreRecordStatus) 
                q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) 
                q.Query = (from r in q.Query orderby r.id  select r);


            return q;
        }

        public List<WorkReportModel> GeList(WorkReportStatus status, string companyName, long? projectId = null)
        {
           
            List<WorkReportModel> ret = null;

            var q = GetBaseQuery();

            var queue = (from r in q.Query
                where  r.recordStatus == (int) RecordStatus.Active 
                select r);
            if (projectId.HasValue)
            {
                queue = queue.Where(i => i.job_id == projectId);
            }

            if (status != WorkReportStatus.All)
            {
                queue = queue.Where(i => i.status == (int)status);
            }
         
            var entities = queue.OrderByDescending(r => r.id).ToList();

            ret = entities.Select(e => e.ToDto(companyName)).ToList();
            return ret;
        }

        internal override void BeforeAction(SmartAction<work_reports> action)
        {
            if (action.Type == SmartActionType.Update && action.Record != null && action.Record.updated_at == null)
            {
                action.Record.updated_at = DateTime.UtcNow;
            }
        }


        public void DeleteReport(long userId, long orgId, long id)
        {
            this.Delete(id);
        }

        public String GetReportPdfPath(long id)
        {
            var path = string.Empty;
             var q = GetBaseQuery().Query;
            var entity = q.FirstOrDefault(t => t.id == id );

            if (entity != null)
                path = Path.Combine(GetDir().FullName, entity.pdf);

            return path;
        }
    }

}
