﻿using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Projects.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.Projects
{
    public class ProjectActionManager
    {
        public long ProjectId { get; set; }
        public long OrganizationId { get; set; }
        public string Action { get; set; }

        public string Error { get; private set; }
        public long Attachment { get; private set; }
        public System.Globalization.CultureInfo Culture { get; set; }

        public long UserId { get; set; }

        public long ToInvoiceId { get; set; }
        public ProjectUpdate Update { get; set; }

        public bool DoAction()
        {
            Action = Action.ToLower();

            Model.CalaniEntities db = new Model.CalaniEntities();
            var dbjob = (from r in db.jobs where r.id == ProjectId select r).First();
            
            if (OrganizationId > -1 && dbjob.organizationId != OrganizationId) throw new Exception("Not allowed to access this record");


            var orgmgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(OrganizationId);
            orgmgr.Load();

            var job = new jobsExtended(dbjob,false);

            if(Action.StartsWith("invoice-part"))
            {
                // count existing invoices
                List<long> invoicesIdLinked = new List<long>();
                foreach (var item in job.services_jobs)
                {
                    if (item.toinvoice != null && item.toinvoice > -1)
                    {
                        invoicesIdLinked.Add(item.toinvoice.Value);
                    }
                }
                long counter = (invoicesIdLinked.Count + 1);

                // create invoice
                Calani.BusinessObjects.Projects.ProjectsManager mgr = new ProjectsManager(OrganizationId);
                var clone = mgr.CloneProject(job.id, "invoice", "part", "-" + counter, Update.cloneItems);
                long newInvoiceId = clone.Key;
                

                // set "to invoice" with the right invoice id
                foreach (var item in job.services_jobs)
                {
                    if(item.toinvoice != null && item.toinvoice == -1)
                    {
                        item.toinvoice = newInvoiceId;
                    }
                }

                ToInvoiceId = newInvoiceId;

            }
            
            if(Action.StartsWith("save") || Action == "importjobs")
            {
                if (Action == "save:open")
                {
                    if (dbjob.dateOpenJob == null) dbjob.dateOpenJob = DateTime.Now;
                }

                if (Action == "save:invoice" || Action == "importjobs" || Action == "save:recurringinvoice")
                {
                    if (dbjob.dateOpenJob == null) dbjob.dateOpenJob = DateTime.Now;
                    if (dbjob.dateInvoicing == null) dbjob.dateInvoicing = DateTime.Now;
                    if (dbjob.dateInvoiceDue == null) dbjob.dateInvoiceDue = DateTime.Now.AddDays(orgmgr.DelayInvoice);
                }
            }

            if (Action == "sendquote" || Action == "senddeliverynote" || Action == "sendproject")
            {
                //if(dbjob.dateQuoteSent == null)
                    dbjob.dateQuoteSent = DateTime.Now; // GM-45. Quote - last sent date does not update

                DocGenerator.QuoteGenerator gen = new DocGenerator.QuoteGenerator(Culture, UserId);
                gen.ProjectId = this.ProjectId;
                byte[] data = gen.GenerateDocument(null);

                Attachments.AttachmentsManager att = new Attachments.AttachmentsManager();
                att.OrganizationId = job.organizationId.Value;
                int version = att.CountJobVersionsDoc(Attachments.AttachmentTypeEnum.GeneratedPdf_Invoice, job.id);
                Attachment = att.AddAttachment(data, Attachments.AttachmentTypeEnum.GeneratedPdf_Invoice, job.id, null, null, job.internalId + " v" + version);
                
            }

            if (Action.StartsWith("setquotesentdate:"))
            {
                DateTime dt = new DateTime();
                string strdate = Action.Split(':').LastOrDefault();
                if(DateTime.TryParseExact(strdate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeLocal, out dt))
                {
                    dbjob.dateQuoteSent = dt;
                }
            }

            if (Action == "markquotesent")
            {
                if(dbjob.dateQuoteSent == null) dbjob.dateQuoteSent = DateTime.Now;
            }

            if (Action == "markquotenotsent")
            {
                dbjob.dateQuoteSent = null;
                dbjob.dateQuoteAccepted = null;
                dbjob.dateQuoteRefused = null;
                
            }

            if (Action == "sendinvoice")
            {
                // generate

                if (dbjob.dateInvoicing == null) 
                    dbjob.dateInvoicing = DateTime.Now;
                
                dbjob.dateInvoicingOpen = DateTime.Now;

                if (Update.updateInvoiceDate)
                {
                    var days = dbjob.organizations.delayInvoice ?? 0;
                    dbjob.dateJobValidity = DateTime.Now.AddDays(days);
                    dbjob.dateInvoiceDue = DateTime.Now.AddDays(days);
                }

                db.SaveChanges();

                DocGenerator.InvoiceGenerator gen = new DocGenerator.InvoiceGenerator(Culture, UserId);
                gen.ProjectId = this.ProjectId;
                byte[] data = gen.GenerateDocument();

                Attachments.AttachmentsManager att = new Attachments.AttachmentsManager();
                att.OrganizationId = job.organizationId.Value;
                int version = att.CountJobVersionsDoc(Attachments.AttachmentTypeEnum.GeneratedPdf_Invoice, job.id);
                Attachment = att.AddAttachment(data, Attachments.AttachmentTypeEnum.GeneratedPdf_Invoice, job.id, null, null,
                    job.internalId + " v" + version);

            }

            if (Action == "markinvoicenotsent")
            {
                dbjob.dateInvoicingOpen = null;
                dbjob.dateFullyPaid = null;
                dbjob.dateInvoiceDue = null;
                dbjob.dateQuoteValidity = null;

                
                InvoiceDueCalculator calc = new InvoiceDueCalculator(dbjob);
                calc.Recalc(orgmgr.UsePriceIncludingVat);
            }

            if (Action == "markinvoicesent")
            {
                dbjob.dateInvoicingOpen = DateTime.Now;


                // generate

                if (dbjob.dateInvoicing == null)
                    dbjob.dateInvoicing = DateTime.Now;

                if (Update.updateInvoiceDate)
                {
                    var days = dbjob.organizations.delayInvoice ?? 0;
                    dbjob.dateJobValidity = DateTime.Now.AddDays(days);
                    dbjob.dateInvoiceDue = DateTime.Now.AddDays(days);
                }

                db.SaveChanges();
                DocGenerator.InvoiceGenerator gen = new DocGenerator.InvoiceGenerator(Culture, UserId);
                gen.ProjectId = this.ProjectId;
                byte[] data = gen.GenerateDocument();

                Attachments.AttachmentsManager att = new Attachments.AttachmentsManager();
                att.OrganizationId = job.organizationId.Value;
                int version = att.CountJobVersionsDoc(Attachments.AttachmentTypeEnum.GeneratedPdf_Invoice, job.id);
                Attachment = att.AddAttachment(data, Attachments.AttachmentTypeEnum.GeneratedPdf_Invoice, job.id, null, null,
                    job.internalId + " v" + version);
            }

            if (Action.StartsWith("setinvoicesentdate:"))
            {
                DateTime dt = new DateTime();
                string strdate = Action.Split(':').LastOrDefault();
                if (DateTime.TryParseExact(strdate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeLocal, out dt))
                {
                    dbjob.dateInvoicingOpen = dt;
                }
            }

            if (Action == "sendreminder")
            {
                ReminderManager remmgr = new ReminderManager(OrganizationId);

                // FAIRE UNE VARIANTE REMINDER
                // -->

                // generate
                if(dbjob.dateQuoteSent == null) dbjob.dateQuoteSent = DateTime.Now;
                DocGenerator.InvoiceGenerator gen = new DocGenerator.InvoiceGenerator(Culture, UserId);
                gen.ProjectId = this.ProjectId;
                gen.Reminder = remmgr.GetReminders(this.ProjectId).Count + 1;
                byte[] data = gen.GenerateDocument(action: Action);

                Attachments.AttachmentsManager att = new Attachments.AttachmentsManager();
                att.OrganizationId = job.organizationId.Value;
                int version = att.CountJobVersionsDoc(Attachments.AttachmentTypeEnum.GeneratedPdf_Invoice, job.id);
                Attachment = att.AddAttachment(data, Attachments.AttachmentTypeEnum.GeneratedPdf_Invoice, job.id, null, null,
                    job.internalId + " v" + version + " R"+ gen.Reminder);
                // -->

                
                remmgr.AddReminder(this.ProjectId);

            }
            else if (Action != null && Action.StartsWith("deletereminder-"))
            {
                string strid = Action.Substring("deletereminder-".Length);
                long lid = 0;
                if(long.TryParse(strid, out lid))
                {
                    ReminderManager remmgr = new ReminderManager(OrganizationId);
                    remmgr.RemoveReminder(this.ProjectId, lid);
                }
            }

            if (Action == "acceptquote")
            {
                if (job.ProjectStatus.ToString().StartsWith("Quote")
                    || job.ProjectStatus.ToString().StartsWith("DeliveryNote"))
                {
                    // set dates
                    if (dbjob.dateQuoteSent == null) dbjob.dateQuoteSent = DateTime.Now;
                    dbjob.dateQuoteAccepted = DateTime.Now;
                    dbjob.dateQuoteRefused = null;
                    
                }
                else
                {
                    // impossible, ce n'est pas un devis
                    Error = "ERR_WrongStatus_NotQuote";
                    return false;
                }
            }

            if (Action == "rejectquote")
            {
                if (job.ProjectStatus.ToString().StartsWith("Quote")
                    || job.ProjectStatus.ToString().StartsWith("DeliveryNote"))
                {
                    // set dates
                    if (dbjob.dateQuoteSent == null) dbjob.dateQuoteSent = DateTime.Now;
                    dbjob.dateQuoteAccepted = null;
                    dbjob.dateQuoteRefused = DateTime.Now;

                }
                else
                {
                    // impossible, ce n'est pas un devis
                    Error = "ERR_WrongStatus_NotQuote";
                    return false;
                }
            }

            if (Action == "projectfinish" ||Action == "jobfinish")
            {
                if (job.ProjectStatus.ToString().StartsWith("Job"))
                {
                    // set dates
                    dbjob.dateJobDone = DateTime.Now;
                    dbjob.dateJobHeld = null;
                }
                else
                {
                    // impossible, ce n'est pas un job
                    Error = "ERR_WrongStatus_NotJob";
                    return false;
                }
            }

            if (Action == "projectsuspended" || Action == "jobsuspended")
            {
                if (job.ProjectStatus.ToString().StartsWith("Job"))
                {
                    // set dates
                    dbjob.dateJobDone = null;
                    dbjob.dateJobHeld = DateTime.Now;
                }
                else
                {
                    // impossible, ce n'est pas un job
                    Error = "ERR_WrongStatus_NotJob";
                    return false;
                }
            }

            if (Action == "projectinprogress" || Action == "jobinprogress")
            {
                if (job.ProjectStatus.ToString().StartsWith("Job"))
                {
                    // set dates
                    dbjob.dateJobDone = null;
                    dbjob.dateJobHeld = null;
                    if (dbjob.dateJobValidity != null && dbjob.dateJobValidity.Value < DateTime.Now) dbjob.dateJobValidity = null;
                }
                else
                {
                    // impossible, ce n'est pas un job
                    Error = "ERR_WrongStatus_NotJob";
                    return false;
                }
            }

            if (Action == "invoicepaid")
            {
                
                if (job.ProjectStatus.ToString().StartsWith("Invoice"))
                {
                    var invoicePaidDate = DateTime.Now;

                    if (!string.IsNullOrWhiteSpace(Update.invoicePaidDate))
                    {
                        if(DateTime.TryParseExact(Update.invoicePaidDate, Tools.DefaultDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out invoicePaidDate))
                        {
                            // set dates
                            dbjob.dateFullyPaid = invoicePaidDate;
                            //dbjob.dateInvoiceDue = null;
                            dbjob.invoiceDue = null;
                        }
                        else
                        {
                            Error = "ERR_invoicePaidDate_wrongFormat";
                            return false;
                        }
                    }
                }
                else
                {
                    // impossible, ce n'est pas un job
                    Error = "ERR_WrongStatus_NotInvoice";
                    return false;
                }
            }

            if (Action == "invoicepending")
            {

                if (job.ProjectStatus.ToString().StartsWith("Invoice"))
                {
                    dbjob.dateInvoicePending = DateTime.Now;
                    // set dates
                    dbjob.dateFullyPaid = null;
                    if (dbjob.dateInvoiceDue != null && dbjob.dateInvoiceDue.Value < DateTime.Now) dbjob.dateInvoiceDue = null;
                    // recalc invoice due //pinpin
                    InvoiceDueCalculator calc = new InvoiceDueCalculator(dbjob);
                    calc.Recalc(orgmgr.UsePriceIncludingVat);

                }
                else
                {
                    // impossible, ce n'est pas un job
                    Error = "ERR_WrongStatus_NotInvoice";
                    return false;
                }
            }

            if(Action == "importjobs")
            {
                List<long> jobsToImport = Update.jobImports;

                foreach (var item in jobsToImport)
                {
                    AppendProjectLinesOnCurrentProject(db, dbjob, item, Culture);
                }

                var records = dbjob.services_jobs.Where(sj => sj.recordStatus != (int)RecordStatus.Deleted).ToList();
                Model.currency currency = null; // can not manage currencies when importing in this version
                ProjectItemsUpdater.UpdateTotal(dbjob, records, currency, orgmgr.RoundAmount);

            }

            db.SaveChanges();

            return true;
         }


        public static void AppendProjectLinesOnCurrentProject(Model.CalaniEntities db, Model.jobs currentJob, long sourceId, CultureInfo culture)
        {
            var sourceJob = (from r in db.jobs
                             where r.organizationId == currentJob.organizationId
                                && r.clientId == currentJob.clientId
                                && r.id == sourceId
                             select r).FirstOrDefault();

            


            string datestr = "";
            
            bool isDeliveryNote = sourceJob.subtype != null && sourceJob.subtype == "DeliveryNote";
            if (isDeliveryNote && sourceJob.visits != null && sourceJob.visits.Any() && sourceJob.visits.First().dateStart.HasValue)
            {
                datestr = $"({sourceJob.visits.First().dateStart.Value.ToString("dd.MM.yyyy")})";
            }
            else if (sourceJob.dateQuoteSent != null)
            {
                datestr = $"({sourceJob.dateQuoteSent.Value.ToString("dd.MM.yyyy")})";
            }

            if (sourceJob != null)
            {
                string projectLabel = "";
                ProjectsManager pmgr = new ProjectsManager(sourceJob.organizationId.Value);
                projectLabel = pmgr.GetProjectTypeLabel(sourceJob.id, culture);


                var sourceRows = sourceJob.services_jobs.Where(sj => sj.recordStatus != (int)RecordStatus.Deleted).ToList();

                if (sourceRows.Count > 0)
                {
                    var position = currentJob.services_jobs.Any() ? currentJob.services_jobs.Max(sj => sj.position) : 0;

                    currentJob.services_jobs.Add(new Model.services_jobs
                    {
                        jobs = currentJob,
                        serviceName = "-- " + projectLabel + " #" + sourceJob.internalId + " " + datestr,
                        servicePrice = 0,
                        serviceQuantity = 0,
                        type = 2,
                        totalPriceExclTax = 0,
                        taxAmount = 0,
                        totalPrinceIncTax = 0,
                        position = ++position,
                    });

                    foreach (var sourcerow in sourceRows)
                    {
                        var copy = sourcerow.Copy();
                        copy.jobs = currentJob;
                        copy.position = position + copy.position;
                        currentJob.services_jobs.Add(copy);
                    }

                    if (sourceJob.importedIn == null || sourceJob.importedIn == 0)
                    {
                        sourceJob.importedIn = currentJob.id;
                    }

                    if(sourceJob.dateQuoteSent == null)
                    {
                        sourceJob.dateQuoteSent = DateTime.Now;
                    }

                    if (sourceJob.dateQuoteAccepted == null)
                    {
                        sourceJob.dateQuoteAccepted = DateTime.Now;
                    }
                }

            }
        }
    }
}
