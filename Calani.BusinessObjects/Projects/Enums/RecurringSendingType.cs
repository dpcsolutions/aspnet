﻿namespace Calani.BusinessObjects.Projects.Enums
{
    public enum RecurringSendingType
    {
        
        Automatic = 1,
        DraftInvoiceWithNotificationMail = 2,
        DraftQuoteWithNotificationMail = 3
    }
}