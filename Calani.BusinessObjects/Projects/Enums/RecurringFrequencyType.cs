﻿namespace Calani.BusinessObjects.Projects.Enums
{
    public enum RecurringFrequencyType
    {
        Week = 1,
        Month = 2,
        Year = 3
    }
}