﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Projects
{
    public class LateInvoicesEmailAlert
    {

        public void TriggerAll()
        {
            

            Model.CalaniEntities db = new Model.CalaniEntities();

            var q = (from r in db.jobs
                     where
                        r.organizationId != null
                        && r.dateInvoicing != null
                        && r.dateFullyPaid == null
                        && r.dateInvoiceDue < DateTime.Now
                        && r.recordStatus == 0
                     select r);

            


            List<long> orgs = (from r in q select r.organizationId.Value).Distinct().ToList();

            foreach (var item in orgs)
            {
                TriggerCustomer(item);
            }

        }

        public void TriggerCustomer(long org)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();

            int day = Convert.ToInt32(DateTime.Now.DayOfWeek);

            var alert = (from r in db.organizations where r.id == org select r.emailAlertLateInvoicesDay).FirstOrDefault();

            if(alert == null)
            {
                return;
            }

            if(alert != day)
            {
                return;
            }


            var dueInvoices = (from r in db.jobs.Include("clients").Include("jobreminders")
                               where
                                  r.organizationId == org
                                  && r.dateInvoicing != null && r.dateInvoicingOpen!=null 
                                  && r.dateFullyPaid == null
                                  && r.dateInvoiceDue < DateTime.Now
                                  && r.recordStatus == 0
                               select r).ToList();

            int admintype = Convert.ToInt32(Contacts.ContactTypeEnum.User_Admin);

            var admins = (from r in db.contacts
                          where 
                            r.organizationId == org
                            && r.type == admintype
                          select r).ToArray();

            string currency = (from r in db.organizations where r.id == org select r.currency).FirstOrDefault();


            // TODO : a traduire. manque la langue du client en dehors du UI
            string text_customers = "Client";
            string text_due = "Reste<br>à payer";
            string text_duedate = "Limite<br>paiement";
            string text_delay = "Retard";
            string text_late_invoices = "Factures à relancer";
            string text_my_invoices = "Mes factures";
            string text_reminder = "Rappels";
            string text_invoices_are_late = "$count$ de vos factures sont arrivés à échéance. Peut-être devriez vous relancer vos clients ?";
            // -->
            

            if (dueInvoices.Count > 0)
            {
                StringBuilder html = new StringBuilder();
                html.AppendLine("<table style='width:100%' border=1>");
                html.AppendLine("<tr>");
                html.AppendLine("<th style='text-align:left'>"+ text_customers +"</th>");
                html.AppendLine("<th style='text-align:right'>"+ text_due +"</th>");
                html.AppendLine("<th style='text-align:center'>" + text_duedate + "</th>");
                html.AppendLine("<th style='text-align:right'>" + text_delay + "</th>");
                html.AppendLine("<th style='text-align:right'>" + text_reminder + "</th>");
                html.AppendLine("</tr>");
                foreach (var i in dueInvoices)
                {
                    html.AppendLine("<tr>");
                    html.AppendLine("<td style='text-align:left'><strong><a style='color:blue: text-decoration:underline' href='"+System.Configuration.ConfigurationManager.AppSettings["PublicUrl"]+ "Projects/Invoices/Job.aspx?id="+i.id+"'>" + i.clients.companyName + " - " + i.internalId + "</a></strong></td>");
                    html.AppendLine("<td style='text-align:right'>" + String.Format("{0:0,0.00}", i.invoiceDue) + " "+ currency +"</td>");
                    html.AppendLine("<td style='text-align:center'>" + i.dateInvoiceDue.Value.ToString("dd.MM.yyyy") + "</td>");
                    html.AppendLine("<td style='text-align:right'>" + Math.Ceiling(DateTime.Now.Subtract(i.dateInvoiceDue.Value).TotalDays) + " jours</td>");
                    html.AppendLine("<td style='text-align:right'>" + i.jobreminders.Count()   + "</td>");
                    html.AppendLine("</tr>");
                }

                foreach (var admin in admins)
                {
                    Generic.MailSender mailSender = new Generic.MailSender();
                    mailSender.MailTo.Email = admin.primaryEmail;
                    mailSender.MailTo.UserId = admin.id;
                    //mailSender.Subject = text_late_invoices + " : " + dueInvoices.Count;
                    mailSender.Variables.Add("title1", text_late_invoices);
                    mailSender.Variables.Add("dueinvoices", dueInvoices.Count.ToString());
                    mailSender.Variables.Add("title2", text_invoices_are_late.Replace("$count$",dueInvoices.Count.ToString()));
                    mailSender.Variables.Add("myinvoices", text_my_invoices);
                    mailSender.Variables.Add("table", html.ToString());
                    mailSender.Variables.Add("buttonlink", System.Configuration.ConfigurationManager.AppSettings["PublicUrl"] + "Projects/Invoices/");
                    mailSender.SendSmartHtmlEmail("ges_dueinvoices.html", "dueinvoices");
                }
                
            }

        }

    }
}
