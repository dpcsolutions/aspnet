﻿using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.Attachments;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Projects.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using Calani.BusinessObjects.CustomerAdmin;

namespace Calani.BusinessObjects.Projects
{
    public class ProjectsManager : Generic.SmartCollectionManager<Model.jobs, long>
    {
        public long? EmployeeFilter { get; set; }
        public long? DeliveryNotesEmployeeFilter { get; set; }

        public ProjectsManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<jobs> GetDbSet(Model.CalaniEntities db)
        {
            return db.jobs;
        }

        internal override SmartQuery<jobs> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in GetDbSet(q.Context) where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(jobs record)
        {
            if (record != null && record.organizationId != null) return record.organizationId.Value;
            return -1;
        }

        internal override SmartQuery<jobs> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from r in q.Context.jobs where r.type == 1 select r);
            if (OrganizationID > -1)
                q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);
            
            if (!IgnoreRecordStatus)
                q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            if (EmployeeFilter != null)
            {
                q.Query = from r in q.Query where 
                    r.visits.Any(v => v.contacts.Any(c => c.id == EmployeeFilter.Value)) 
                    || r.projectemployees.Any(v => v.employeeId == EmployeeFilter.Value)
                select r;
            }

            // default sorting
            if (!IgnoreRecordStatus) 
                q.Query = (from r in q.Query orderby r.lastUpdate descending select r);


            return q;
        }

        public SmartQuery<jobs> GetPublicQuery() { return GetBaseQuery(); }
        
        public bool IsFreeName(string v)
        {
            var bq = GetBaseQuery();
            var xQ = (from r in bq.Query where r.internalId == v select r);
            return xQ.Count() == 0;
        }

        internal override void BeforeAction(SmartAction<jobs> action)
        {
            if (action.Type == SmartActionType.Add || action.Type == SmartActionType.Update)
            {
                action.Record.lastUpdate = DateTime.Now;
            }

            if (action.Type == SmartActionType.Delete)
            {
                try
                {
                    Model.CalaniEntities db = new CalaniEntities();
                    var visits = (from r in db.visits where r.orgId == OrganizationID && r.jobId == action.ExistingRecord.id select r).ToList();
                    foreach (var visit in visits) visit.recordStatus = -1;

                    var links = (from r in db.services_jobs where r.toinvoice != null && r.toinvoice == action.ExistingRecord.id select r).ToList();
                    foreach (var link in links) link.toinvoice = null;

                    db.SaveChanges();
                }
                catch { }

            }
        }

        public List<long> ImportTime(long clientId, long projId, List<long> records)
        {
            var mgr = new TimeManagement.TimeSheetRecordsManager(OrganizationID);

            var timeRecords = mgr.GetProjectRecords(projId).Where(r=>records.Contains(r.id))
                .Select(r=> new TimeSheetRecordImport(r)).ToList();


            var itemsUpdater = new Projects.ProjectItemsUpdater();
            itemsUpdater.OrganizationId = OrganizationID;
            itemsUpdater.ProjectId = projId;
            
            return itemsUpdater.UpdateImportedTimeRecords(timeRecords);

        }

        public List<TimeSheetRecordImport> PrepareTimeSheetRecordsForImport( long projId, out double sum)
        {
            var mgr = new TimeManagement.TimeSheetRecordsManager(OrganizationID);
            var records = mgr.GetProjectRecords(projId);
             sum = (from r in records
                where
                    r.duration != null
                    && r.startDate != null
                    && r.endDate != null
                    && r.timesheetId != null
                    && r.serviceId != null
                    && r.imported == false
                select r.duration.Value * r.rate.Value / 100).Sum();

           

            if (sum == 0)
            {
           
                return null;
            }


            List<TimeSheetRecordImport> items = (from r in records
                where
                    r.duration != null
                    && r.startDate != null
                    && r.endDate != null
                    && r.timesheetId != null
                    && r.serviceId != null
                select new TimeSheetRecordImport(r)).ToList();

            return items;
        }

        public jobsExtended GetInfo(long id)
        {
            var orgmgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(OrganizationID);
            orgmgr.Load();

            var r = Get(id);
            return new jobsExtended(r, true);
        }

        public void FixQuotesOrDeliveryNoteWithoutSubType()
        {
            var bq = GetBaseQuery();
            var xQ = (from r in bq.Query.Include("clients")
                      where r.dateOpenJob == null 
                      && r.dateInvoicing == null
                      && r.subtype == null
                      select r);
            
            var dbRecords = xQ.ToList();

            var orgmgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(OrganizationID);
            orgmgr.Load();

            foreach (var item in dbRecords)
            {
                if (orgmgr.BeforeInvoice == "DeliveryNote")
                {
                    item.subtype = "DeliveryNote";
                }
                else
                {
                    item.subtype = "Quote";
                }
            }

            bq.Context.SaveChanges();
            
        }
        #region Delivery notes
        public List<jobsExtended> ListLinkedDeliveryNotes(long? pid = null)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();

            var bq = GetBaseQuery();
            var xQ = (from r in bq.Query.Include("clients")
                      where r.dateOpenJob == null
                      && r.dateInvoicing == null
                      && r.subtype == "DeliveryNote"
                      select r);

            var tmp = xQ.ToList();
            if (pid != null)
                xQ = (from r in xQ where r.linkedjobid == pid select r);
            var dbRecords = xQ.ToList();
            var ret = GetJobExtended(dbRecords).ToList();
            return ret;
        }
        #endregion Delivery notes
        public List<jobsExtended> ListDeliveryNotes(long? custid = null, long? assigneeId = null, long? creatorId = null,
                        DateRange dateRange = null, bool includeServices = false, bool includeVisits = false)
        {
            var bq = GetBaseQuery();
            var xQ = (from r in bq.Query.Include("clients")
                      where r.dateOpenJob == null
                      && r.dateInvoicing == null
                      && r.subtype == "DeliveryNote"
                      select r);

            if (includeServices)
            {
                xQ = xQ.Include(e => e.services_jobs);
            }

            if (includeVisits)
            {
                xQ = xQ.Include(e => e.visits);
            }

            var tmp = xQ.ToList();

            if (custid != null) 
                xQ = (from r in xQ where r.clientId == custid select r);

            if (assigneeId.HasValue && creatorId.HasValue)
            {
                xQ = (from r in xQ where r.assigneeId == assigneeId || r.createdBy == creatorId select r);
            }
            else if (assigneeId.HasValue)
            {
                xQ = (from r in xQ where r.assigneeId == assigneeId select r);
            }
            else if (creatorId.HasValue)
            {
                xQ = (from r in xQ where r.createdBy == creatorId select r);
            }

            if (dateRange != null)
            {
                xQ = (from r in xQ
                      where (r.dateQuoteSent == null && r.dateQuoteAccepted == null && r.dateQuoteRefused == null) || // draft
                            (r.filterByDate != null && r.filterByDate >= dateRange.Start && r.filterByDate <= dateRange.End) ||
                            (r.lastUpdate != null && r.lastUpdate >= dateRange.Start && r.lastUpdate <= dateRange.End)
                      select r);
            }

            var dbRecords = xQ.ToList();
            FixNewProperties(dbRecords, bq.Context);

            var roundTotal = false;
            if (includeServices)
            {
                var orgmgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(OrganizationID);
                orgmgr.Load();
                roundTotal = orgmgr.RoundAmount;
            }

            var ret = GetJobExtended(dbRecords, roundTotal: roundTotal).ToList();

            // MissingChild
            List<long> idsEnds = new List<long>();
            foreach (var i in ret)
            {
                if (i.dateQuoteAccepted != null)
                {
                    idsEnds.Add(i.id);
                }
            }

            foreach (var i in ret)
            {
                if (i.dateQuoteAccepted != null && !i.importedIn.HasValue)
                {
                    i.MissingChild = true;
                }
            }
            // -->


            // Imported In
            List<long> idImportedIn = new List<long>();
            foreach (var i in ret)
            {
                if (i.importedIn != null)
                {
                    idImportedIn.Add(i.importedIn.Value);
                }
            }

            var imports = (from r in bq.Context.jobs
                           where r.organizationId == OrganizationID
                           && idImportedIn.Contains(r.id)
                           select new
                           {
                               id = r.id,
                               internalId = r.internalId

                           }).ToList();

            foreach (var i in ret)
            {
                if (i.importedIn != null)
                {
                    i.ImportedInInternalId = (from r in imports where r.id == i.importedIn.Value select r.internalId).FirstOrDefault();
                }
            }
            // -->

            return ret;
        }

        public List<jobsExtended> ListQuotes(long? custid = null, long? assigneeId = null, DateRange dateRange = null, bool includeServices = false)
        {
            var bq = GetBaseQuery();
            var xQ = (from r in bq.Query.Include("clients")
                      where r.dateOpenJob == null 
                      && r.dateInvoicing == null
                      && r.subtype == "Quote"
                      select r);
            var gg = xQ.ToList();
            if (includeServices)
            {
                xQ = xQ.Include(e => e.services_jobs);
            }

            if (custid != null)
                xQ = (from r in xQ where r.clientId == custid select r);

            if (assigneeId.HasValue)
                xQ = (from r in xQ where r.assigneeId == assigneeId select r);

            if (dateRange != null)
            {
                xQ = (from r in xQ
                      where
                           // QuoteSend, QuoteOutdated are shown in current period
                           (r.dateQuoteSent != null && r.dateQuoteAccepted == null && r.dateQuoteRefused == null && DateTime.Now >= dateRange.Start && DateTime.Now <= dateRange.End) ||
                           // QuoteAccepted are filtered by date-accepted 
                           (r.dateQuoteAccepted != null && r.dateQuoteAccepted >= dateRange.Start && r.dateQuoteAccepted <= dateRange.End) ||
                           // QuoteRefused are filtered by date-refused
                           (r.dateQuoteRefused != null && r.dateQuoteRefused >= dateRange.Start && r.dateQuoteRefused <= dateRange.End) ||
                           (r.dateJobValidity != null && r.dateJobValidity >= dateRange.Start && r.dateJobValidity <= dateRange.End) ||
                           (r.lastUpdate != null && r.lastUpdate >= dateRange.Start && r.lastUpdate <= dateRange.End)
                      select r);
            }

            var dbRecords = xQ.ToList();
            FixNewProperties(dbRecords, bq.Context);

            var orgmgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(OrganizationID);
            orgmgr.Load();
            
            var ret = GetJobExtended(dbRecords, roundTotal: orgmgr.RoundAmount, 
                usePriceIncludingVat: orgmgr.UsePriceIncludingVat).ToList(); 

            // MissingChild
            List<long> idsEnds = new List<long>();
            foreach (var i in ret)
            {
                if(i.dateQuoteAccepted != null)
                {
                    idsEnds.Add(i.id);
                }
            }
            foreach (var i in ret)
            {
                if (i.dateQuoteAccepted != null && !i.importedIn.HasValue)
                {
                    i.MissingChild = true;
                }
            }
            // -->

            // Imported In
            List<long> idImportedIn = new List<long>();
            foreach (var i in ret)
            {
                if(i.importedIn != null)
                {
                    idImportedIn.Add(i.importedIn.Value);
                }
            }

            var imports = (from r in bq.Context.jobs
                where r.organizationId == OrganizationID
                      && idImportedIn.Contains(r.id)
                select new
                {
                    id = r.id,
                    internalId = r.internalId

                }).ToList();

            foreach (var i in ret)
            {
                if (i.importedIn != null)
                {
                    i.ImportedInInternalId = (from r in imports where r.id == i.importedIn.Value select r.internalId).FirstOrDefault();
                }
            }
            // -->

            return ret;
        }

        private IEnumerable<jobsExtended> GetJobExtended(IEnumerable<jobs> dbRecords, bool includeRemindres = false,
            bool roundTotal = false, bool usePriceIncludingVat = false)
        {
            var ret = new List<jobsExtended>();
            var dbList = dbRecords.ToList();
            
            foreach(var job in dbList)
            {
                var jobExtended = new jobsExtended(job, includeRemindres, roundTotal, usePriceIncludingVat);
                
                ret.Add(jobExtended);
            }

            return ret;
        }
        
        public List<jobsExtended> ListOpens(long? custid = null, bool includeServices = false)
        {
            var bq = GetBaseQuery();
            var xQ = (from r in bq.Query.Include("clients").Include("visits").Include("visits.contacts").Include("services_jobs") where 
                      r.dateOpenJob != null && r.dateInvoicing == null select r);

            if (includeServices)
            {
                xQ = xQ.Include(e => e.services_jobs);
            }
            
            if (custid != null) 
                xQ = (from r in xQ where r.clientId == custid select r);

            var dbRecords = xQ.ToList();

            FixNewProperties(dbRecords, bq.Context);

            var roundTotal = false;
            if (includeServices)
            {
                var orgmgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(OrganizationID);
                orgmgr.Load();
                roundTotal = orgmgr.RoundAmount;
            }

            var ret = GetJobExtended(dbRecords, roundTotal: roundTotal).ToList();

            // MissingChild
            List<long> idsEnds = new List<long>();
            foreach (var i in ret)
            {
                if (i.dateJobDone != null)
                {
                    idsEnds.Add(i.id);
                }
            }

            foreach (var i in ret)
            {
                //if (i.dateJobDone != null && !child.Contains(i.id)) 
                if (!i.skipMissingInvoiceNotif && i.dateJobDone != null && !i.importedIn.HasValue )
                {
                    i.MissingChild = true;
                }
            }
            // -->

            return ret;
        }

        internal void CorrectClosedInvoinces()
        {
            var bq = GetBaseQuery();
            var list = (from r in bq.Query where r.dateFullyPaid != null && r.invoiceDue != null && r.invoiceDue > 0 select r).ToList();
            int changes = 0;
            foreach (var item in list)
            {
                changes++;
                item.invoiceDue = 0;
            }

            if (changes > 0)
            {
                bq.Context.SaveChanges();
            }
        }

        public List<jobs> ListInvoicesDbRecords(long? clientId = null, DateRange dateRange = null, bool includeServices = false)
        {
            CorrectClosedInvoinces();
            var bq = GetBaseQuery();
            var xQ = (from r in bq.Query where r.dateInvoicing != null select r);
            if (clientId != null) xQ = (from r in xQ where r.clientId == clientId select r);
            if(dateRange != null)
            {
                xQ = from r in xQ where r.dateInvoicingOpen == null || 
                    (r.filterByDate != null && r.filterByDate >= dateRange.Start && r.filterByDate <= dateRange.End) ||
                    (r.dateFullyPaid != null && r.dateFullyPaid >= dateRange.Start && r.dateFullyPaid <= dateRange.End) ||
                    (r.dateFullyPaid == null && r.lastUpdate != null && r.lastUpdate >= dateRange.Start && r.lastUpdate <= dateRange.End)
                select r;
            }
            if (includeServices) xQ = xQ.Include(r => r.services_jobs);
            
            xQ = xQ.Include(r => r.jobreminders).Include(r => r.payments).Include(r => r.clients);

            return xQ.ToList();
        }
        
        public List<jobsExtended> ListInvoices(long orgID, long? clientId = null, DateRange dateRange = null, bool includeServices = false)
        {
            List<jobsExtended> ret;
            var bq = GetBaseQuery();
            var dbRecords = ListInvoicesDbRecords(clientId, dateRange, includeServices);
            
            var orgmgr = new CustomerAdmin.OrganizationManager(OrganizationID);
            orgmgr.Load();
            
            var changes = 0;
            foreach (var job in dbRecords)
            {
                if (job.discount == null) job.discount = 0;
                if (job.totalWithoutTaxes == null)
                {
                    var activeRecords = job.services_jobs.Where(sj => sj.recordStatus != (int)RecordStatus.Deleted).ToList();
                    job.totalWithoutTaxes = (from r in activeRecords select r.totalPriceExclTax).Sum();
                    job.totalWithoutTaxes = job.totalWithoutTaxes * (100 - job.discount ?? 0) / 100;
                    if(job.totalWithoutTaxes != null) job.totalWithoutTaxes = Math.Round(job.totalWithoutTaxes.Value, 2, MidpointRounding.AwayFromZero);
                    changes++;
                }
                
                // if(job.dateInvoicing != null && (job.invoiceDue == null || job.invoiceDue == 0))
                var calc = new InvoiceDueCalculator(job);
                if(calc.Recalc(orgmgr.UsePriceIncludingVat)) 
                    changes++;
                // if(job.dateInvoicing != null && (job.invoiceDue == null || job.invoiceDue == 0))
            }
            
            if (changes > 0) bq.Context.SaveChanges();

            if (includeServices) {
                ret = dbRecords.Select(e => new jobsExtended(e, roundTotal: orgmgr.RoundAmount, isVatIncluded: orgmgr.UsePriceIncludingVat)).ToList();
            }
            else {
                var jobsFactory = new JobsFactory();
                ret = dbRecords.Select(e => jobsFactory.ToInvoiceLightweight(e, orgmgr.UsePriceIncludingVat)).ToList();
            }
            
            return ret;
        }
        
        public List<long> GetInvoicesIdsByDateInvoicing(long clientId, DateRange dateRange)
        {
            var bq = GetBaseQuery();

            var xQ = from r in bq.Query where
                     r.clientId == clientId &&
                     r.dateInvoicing != null && r.dateInvoicing >= dateRange.Start && r.dateInvoicing <= dateRange.End
                     select r.id;

            var ids = xQ.ToList();

            return ids;
        }

        public void FixNewProperties(List<jobs> jobs, CalaniEntities db)
        {
            int changes = 0;
            foreach (var job in jobs)
            {
                if (job.discount == null) job.discount = 0;

                if(job.totalWithoutTaxes == null)
                {
                    var activeRecords = job.services_jobs.Where(sj => sj.recordStatus != (int)RecordStatus.Deleted).ToList();
                    job.totalWithoutTaxes = (from r in activeRecords select r.totalPriceExclTax).Sum();
                    job.totalWithoutTaxes = job.totalWithoutTaxes * (100 - job.discount ?? 0) / 100;
                    if(job.totalWithoutTaxes != null) job.totalWithoutTaxes = Math.Round(job.totalWithoutTaxes.Value, 2, MidpointRounding.AwayFromZero);
                    changes++;
                }
            }

            if (changes > 0) db.SaveChanges();
        }

        public string GetStat(string type, string status, string method)
        {
            string ret = "";

            var startDate = new DateTime(DateTime.UtcNow.Year, 1, 1);
            var endDate = startDate.AddYears(1).AddDays(-1);


            var bq = GetBaseQuery();
            var xQ = bq.Query;

            if (type == "Quote" || type == "DeliveryNote")
            {
                xQ = (from r in xQ where r.dateOpenJob == null && r.dateInvoicing == null select r);
                if (status == "Accepted")
                {
                    xQ = (from r in xQ where r.dateQuoteAccepted != null select r);
                }
                else if (status == "Draft")
                {
                    xQ = (from r in xQ where r.dateQuoteSent == null select r);
                }
            }
            else if (type == "Job" || type == "Open")
            {
                xQ = (from r in xQ where r.dateOpenJob != null && r.dateInvoicing == null select r);
                if (status == "Opened")
                {
                    xQ = (from r in xQ where r.dateJobDone == null && r.dateJobHeld == null && (r.dateJobValidity == null || r.dateJobValidity < DateTime.Now) select r);
                }
                else if (status == "Finished")
                {
                    xQ = (from r in xQ where r.dateJobDone != null select r);
                }
            }
            else if (type == "Invoice")
            {

                xQ = (from r in xQ where r.dateInvoicing != null && r.lastUpdate >= startDate && r.lastUpdate <= endDate select r);

                // todo: nouveau invoice.draft
                if (status == "Draft")
                {
                    xQ = (from r in xQ where r.dateInvoicingOpen == null && r.dateFullyPaid == null select r);
                }
                else if (status == "Open")
                {
                    xQ = (from r in xQ where r.dateInvoicingOpen != null && r.dateFullyPaid == null && r.dateInvoiceDue < DateTime.Now select r);
                }
                else 
                if (status == "Late")
                {
                    xQ = (from r in xQ where r.dateInvoicingOpen != null && r.dateFullyPaid == null && r.dateInvoiceDue != null && r.dateInvoiceDue < DateTime.Now select r);
                }
                else if (status == "Pending")
                {
                    xQ = (from r in xQ where r.dateInvoicingOpen != null && r.dateFullyPaid == null && (r.dateInvoiceDue >= DateTime.Now || r.dateInvoiceDue == null) && r.dateInvoicing != null select r);
                }
            }

            if (method == "Count")
            {
                ret = xQ.Count().ToString();
            }
            else if (method == "Last")
            {
                ret = (from r in xQ orderby r.id descending select r.internalId).FirstOrDefault();
            }
            else if (method == "LastId")
            {
                ret = (from r in xQ orderby r.id descending select r.id).FirstOrDefault().ToString();
            }

            return ret;
        }


        public long CountCustomerQuotes(long custId)
        {
            var bq = GetBaseQuery();
            var xQ = (from r in bq.Query where r.dateOpenJob == null && r.dateInvoicing == null select r);
            xQ = (from r in xQ where r.clientId == custId select r);
            return xQ.LongCount();
        }

        public long CountCustomerOpenProjets(long custId)
        {
            var bq = GetBaseQuery();
            var xQ = (from r in bq.Query where r.dateOpenJob != null && r.dateInvoicing == null select r);
            xQ = (from r in xQ where r.clientId == custId select r);
            return xQ.LongCount();
        }
        public long CountCustomerInvoices(long custId)
        {
            var bq = GetBaseQuery();
            var xQ = (from r in bq.Query where r.dateInvoicing != null select r);
            xQ = (from r in xQ where r.clientId == custId select r);
            return xQ.LongCount();
        }

        public string GetLastQuoteInternalId()
        {
            var bq = GetBaseQuery();
            var xQ = (from r in bq.Query where r.subtype == "Quote" && r.dateOpenJob == null && r.dateInvoicing == null && r.internalId != null orderby r.id descending select r.internalId);
            string ret = "Q";
            var rec = xQ.FirstOrDefault(r => r != null && r != "");
            if (!String.IsNullOrWhiteSpace(rec)) ret = rec;
            return ret;
        }

        public string GetLastDeliveryNoteInternalId()
        {
            var bq = GetBaseQuery();
            var xQ = (from r in bq.Query where r.subtype == "DeliveryNote" && r.dateOpenJob == null && r.dateInvoicing == null && r.internalId != null orderby r.id descending select r.internalId);
            string ret = "D";
            var rec = xQ.FirstOrDefault(r => r != null && r != "");
            if (!String.IsNullOrWhiteSpace(rec)) ret = rec;
            return ret;
        }

        public string GetLastOpenInternalId()
        {
            var bq = GetBaseQuery();
            var xQ = (from r in bq.Query where r.dateOpenJob != null && r.dateInvoicing == null && r.internalId != null orderby r.id descending select r.internalId);
            string ret = "P";
            var rec = xQ.FirstOrDefault(r => r != null && r != "");
            if (!String.IsNullOrWhiteSpace(rec)) ret = rec;
            return ret;
        }

        public string GetLastInvoiceInternalId()
        {
            var bq = GetBaseQuery();
            var xQ = (from r in bq.Query where r.dateInvoicing != null && r.internalId != null orderby r.id descending select r.internalId);
            string ret = "I";
            var rec = xQ.FirstOrDefault(r => r != null && r != "");
            if (!String.IsNullOrWhiteSpace(rec)) ret = rec;
            return ret;
        }

        public KeyValuePair<long, string> CloneProject(long sourceId, string type, string allitems,
            string suffix, CloneItems cloneItems, bool invoiceToQuote = false, DateTime? invoicePending = null)
        {
            var result = new KeyValuePair<long, string>();

            if (type == null) type = "quote";
            type = type.ToLower().Trim();

            var query = GetById(sourceId);
            var source = query.Query.FirstOrDefault();
            
            if (source != null)
            {
                jobsExtended ext = new jobsExtended(type == "quote", type == "project"
                    || type == "job", type == "invoice", type == "deliverynote", OrganizationID);

                jobs newProject = new jobs();

                // status and internalId
                newProject.internalId = ext.internalId;
                if (!string.IsNullOrWhiteSpace(suffix))
                {
                    newProject.internalId = source.internalId + suffix;

                    while(IsFreeName(newProject.internalId) == false)
                    {
                        var inc = new IncrementalNumber(newProject.internalId);
                        inc.Increment();
                        newProject.internalId = inc.ToString();
                    }
                }
                
                newProject.dateInvoicing = ext.dateInvoicing;
                newProject.dateOpenJob = ext.dateOpenJob;
                newProject.subtype = ext.subtype;
                newProject.dateQuoteSent = source.dateQuoteSent;
                
                if (invoiceToQuote)
                {
                    newProject.subtype = "Quote";
                    newProject.dateInvoicing = null;
                    newProject.dateJobValidity = DateTime.Now;
                    newProject.dateQuoteValidity = DateTime.Now;
                }
               
                // copy meta and customer info
                newProject.dateInvoicePending = invoicePending;
                newProject.clientId = source.clientId;
                newProject.clientAddress = source.clientAddress;
                newProject.city = source.city;
                newProject.contractualUnitsCount = source.contractualUnitsCount;
                newProject.country = source.country;
                newProject.dateCreated = DateTime.Now;
                newProject.dateFullyPaid = null;
                newProject.dateInvoiceDue = null;
                newProject.description = source.description;
                newProject.comment = source.comment;
                newProject.send_copy_email_to = source.send_copy_email_to;
                newProject.discount = source.discount;
                newProject.discount_percent = source.discount_percent;
                newProject.discount_type = source.discount_type;
                newProject.discount_percent2 = source.discount_percent2;
                newProject.discount_type2 = source.discount_type2;
                newProject.discount_percent3 = source.discount_percent3;
                newProject.discount_type3 = source.discount_type3;
                newProject.discount_percent4 = source.discount_percent4;
                newProject.discount_type4 = source.discount_type4;
                newProject.currency = source.currency;
                newProject.currencyRate = source.currencyRate;

                newProject.internalNote = source.internalNote;

                newProject.invoiceDue = source.invoiceDue;
                newProject.nb = source.nb;
                newProject.npa = source.npa;
                newProject.organizationId = OrganizationID;

                newProject.sentToEmail = source.sentToEmail;
                newProject.sentToName = source.sentToName;
                newProject.street = source.street;
                newProject.street2 = source.street2;
                newProject.total = source.total;
                newProject.type = source.type;
                
                var orgMgr = new OrganizationManager(OrganizationID);
                orgMgr.Load();
                if (cloneItems.attachments)
                {
                    var ignoreAtt = new List<AttachmentTypeEnum>(){AttachmentTypeEnum.GeneratedPdf_Generic, AttachmentTypeEnum.GeneratedPdf_Quote,
                        AttachmentTypeEnum.GeneratedPdf_Invoice, AttachmentTypeEnum.GeneratedXlsx_Generic,
                        AttachmentTypeEnum.GeneratedXlsx_Quote, AttachmentTypeEnum.GeneratedXlsx_Invoice};


                    var mgr = new AttachmentsManager();
                    mgr.OrganizationId = OrganizationID;

                    foreach (var att in source.attachments)
                    {
                        if (att != null &&  att.type.HasValue && !ignoreAtt.Any(i => i == (AttachmentTypeEnum) att.type.Value))
                        {
                            var clonedAtt = mgr.CloneAttachment(att);
                            newProject.attachments.Add(clonedAtt);
                        }
                    }
                }

                if (cloneItems.links)
                    newProject.parentId = source.id;

                if (cloneItems.notes)
                    foreach (var note in source.notes)
                    {
                        newProject.notes.Add(new notes()
                        {
                            date = note.date,
                            recordStatus = note.recordStatus,
                            contacts = note.contacts,
                            author = note.author,
                            lastModificationDate = note.lastModificationDate,
                            message = note.message,
                            mobileTempId = note.mobileTempId,
                            lastModifiedById = note.lastModifiedById

                        });

                    }

                query.Context.jobs.Add(newProject);

                List<services_jobs> services = new List<services_jobs>();
                bool aldreayInvoiced = false;

                foreach (var extSJ in source.services_jobs.Where(sj => sj.recordStatus != (int)RecordStatus.Deleted))
                {
                    if (extSJ.toinvoice != null && extSJ.toinvoice > 0) 
                        aldreayInvoiced = true;
                }
                if(aldreayInvoiced && type == "invoice" && source.dateInvoicing == null && allitems != "part")
                {
                    allitems = "notinvoiced";
                }

                foreach (var extSJ in source.services_jobs.Where(sj => sj.recordStatus != (int)RecordStatus.Deleted))
                {
                    if (allitems == null 
                        || ( allitems == "part" && extSJ.toinvoice != null && extSJ.toinvoice == -1)
                        || allitems == "notinvoiced" && extSJ.toinvoice == null)
                    {
                        services_jobs newSJ = extSJ.Copy();
                        newSJ.jobs = newProject;
                        newSJ.toinvoice = null; // do not keep links on invoices

                        query.Context.services_jobs.Add(newSJ);
                        services.Add(newSJ);
                    }
                }            

                if (allitems != null && allitems == "part")
                {
                    // need refresh
                    newProject.total = (from r in services select r.totalPrinceIncTax).Sum();
                    newProject.total = newProject.total * (100 - newProject.discount ?? 0) / 100;
                    newProject.total = Math.Round(newProject.total.Value, 2, MidpointRounding.AwayFromZero);
                    InvoiceDueCalculator calc = new InvoiceDueCalculator(newProject);
                    calc.Recalc(orgMgr.UsePriceIncludingVat);
                }

                query.Context.SaveChanges();

                result = new KeyValuePair<long, string>(newProject.id, newProject.internalId);

            }

            return result;
        }

        public List<jobsExtended> GetChildren(long id)
        {
            var q = GetBaseQuery();
            q.Query = (from r in q.Query where r.parentId>0 && r.parentId == id select r);
            var dbRecords = q.Query.ToList();

            var ret = GetJobExtended(dbRecords).ToList();
            return ret;

        }

        public List<jobsExtended> GetImports(long id)
        {
            var q = GetBaseQuery();
            q.Query = (from r in q.Query where r.importedIn > 0 && r.importedIn == id select r);
            var dbRecords = q.Query.ToList();

            var ret = GetJobExtended(dbRecords).ToList();
            return ret;
        }


        public List<jobsExtended> NaturalSearch(string search)
        {
            Model.CalaniEntities db = new CalaniEntities();

            string[] terms = search.Split(' ');
            terms = (from r in terms where !String.IsNullOrWhiteSpace(r) select r).ToArray();

            List<long> ids = new List<long>();


            foreach (var term in terms)
            {
                
                var upper = RemoveDiacritics(term.ToUpper());
                var morelids = new List<long>();
                morelids.AddRange((from r in db.jobs where r.organizationId == OrganizationID && r.internalId.ToUpper().Replace("à", "a")
                    .Replace("â", "a")
                    .Replace("ä", "a")
                    .Replace("ç", "c")
                    .Replace("é", "e")
                    .Replace("è", "e")
                    .Replace("ê", "e")
                    .Replace("ë", "e")
                    .Replace("î", "i")
                    .Replace("ï", "i")
                    .Replace("ô", "o")
                    .Replace("ù", "u")
                    .Replace("û", "u")
                    .Replace("ü", "u").Contains(upper) select r.id).ToList());
                morelids.AddRange((from r in db.jobs where r.organizationId == OrganizationID && r.description.ToUpper().Replace("à", "a")
                    .Replace("â", "a")
                    .Replace("ä", "a")
                    .Replace("ç", "c")
                    .Replace("é", "e")
                    .Replace("è", "e")
                    .Replace("ê", "e")
                    .Replace("ë", "e")
                    .Replace("î", "i")
                    .Replace("ï", "i")
                    .Replace("ô", "o")
                    .Replace("ù", "u")
                    .Replace("û", "u")
                    .Replace("ü", "u").Contains(upper) select r.id).ToList());

                morelids.AddRange((from r in db.services_jobs where r.jobs.organizationId == OrganizationID && r.services != null && r.services.internalId.ToUpper().Replace("à", "a")
                    .Replace("â", "a")
                    .Replace("ä", "a")
                    .Replace("ç", "c")
                    .Replace("é", "e")
                    .Replace("è", "e")
                    .Replace("ê", "e")
                    .Replace("ë", "e")
                    .Replace("î", "i")
                    .Replace("ï", "i")
                    .Replace("ô", "o")
                    .Replace("ù", "u")
                    .Replace("û", "u")
                    .Replace("ü", "u").Contains(upper) select r.jobId).ToList());
                morelids.AddRange((from r in db.services_jobs where r.jobs.organizationId == OrganizationID && r.serviceName.ToUpper().Replace("à", "a")
                    .Replace("â", "a")
                    .Replace("ä", "a")
                    .Replace("ç", "c")
                    .Replace("é", "e")
                    .Replace("è", "e")
                    .Replace("ê", "e")
                    .Replace("ë", "e")
                    .Replace("î", "i")
                    .Replace("ï", "i")
                    .Replace("ô", "o")
                    .Replace("ù", "u")
                    .Replace("û", "u")
                    .Replace("ü", "u").Contains(upper) select r.jobId).ToList());

                morelids.AddRange((from r in db.jobs where r.organizationId == OrganizationID && r.clients.companyName.ToUpper().Replace("à", "a")
                    .Replace("â", "a")
                    .Replace("ä", "a")
                    .Replace("ç", "c")
                    .Replace("é", "e")
                    .Replace("è", "e")
                    .Replace("ê", "e")
                    .Replace("ë", "e")
                    .Replace("î", "i")
                    .Replace("ï", "i")
                    .Replace("ô", "o")
                    .Replace("ù", "u")
                    .Replace("û", "u")
                    .Replace("ü", "u").Contains(upper) select r.id).ToList());

                morelids.AddRange((from r in db.jobs where r.organizationId == OrganizationID && r.addresses != null && r.addresses.city.ToUpper().Replace("à", "a")
                    .Replace("â", "a")
                    .Replace("ä", "a")
                    .Replace("ç", "c")
                    .Replace("é", "e")
                    .Replace("è", "e")
                    .Replace("ê", "e")
                    .Replace("ë", "e")
                    .Replace("î", "i")
                    .Replace("ï", "i")
                    .Replace("ô", "o")
                    .Replace("ù", "u")
                    .Replace("û", "u")
                    .Replace("ü", "u").Contains(upper) select r.id).ToList());
                morelids.AddRange((from r in db.jobs where r.organizationId == OrganizationID && r.addresses != null && r.addresses.npa.ToUpper().Replace("à", "a")
                    .Replace("â", "a")
                    .Replace("ä", "a")
                    .Replace("ç", "c")
                    .Replace("é", "e")
                    .Replace("è", "e")
                    .Replace("ê", "e")
                    .Replace("ë", "e")
                    .Replace("î", "i")
                    .Replace("ï", "i")
                    .Replace("ô", "o")
                    .Replace("ù", "u")
                    .Replace("û", "u")
                    .Replace("ü", "u").Contains(upper) select r.id).ToList());
                morelids.AddRange((from r in db.jobs where r.organizationId == OrganizationID && r.addresses != null && r.addresses.street.ToUpper().Replace("à", "a")
                    .Replace("â", "a")
                    .Replace("ä", "a")
                    .Replace("ç", "c")
                    .Replace("é", "e")
                    .Replace("è", "e")
                    .Replace("ê", "e")
                    .Replace("ë", "e")
                    .Replace("î", "i")
                    .Replace("ï", "i")
                    .Replace("ô", "o")
                    .Replace("ù", "u")
                    .Replace("û", "u")
                    .Replace("ü", "u").Contains(upper) select r.id).ToList());
                morelids.AddRange((from r in db.jobs where r.organizationId == OrganizationID && r.addresses != null && r.addresses.street.ToUpper().Replace("à", "a")
                    .Replace("â", "a")
                    .Replace("ä", "a")
                    .Replace("ç", "c")
                    .Replace("é", "e")
                    .Replace("è", "e")
                    .Replace("ê", "e")
                    .Replace("ë", "e")
                    .Replace("î", "i")
                    .Replace("ï", "i")
                    .Replace("ô", "o")
                    .Replace("ù", "u")
                    .Replace("û", "u")
                    .Replace("ü", "u").Contains(upper) select r.id).ToList());
                morelids.AddRange((from r in db.jobs where r.organizationId == OrganizationID && r.contacts != null && r.contacts.lastName.ToUpper().Replace("à", "a")
                    .Replace("â", "a")
                    .Replace("ä", "a")
                    .Replace("ç", "c")
                    .Replace("é", "e")
                    .Replace("è", "e")
                    .Replace("ê", "e")
                    .Replace("ë", "e")
                    .Replace("î", "i")
                    .Replace("ï", "i")
                    .Replace("ô", "o")
                    .Replace("ù", "u")
                    .Replace("û", "u")
                    .Replace("ü", "u").Contains(upper) select r.id).ToList());
                morelids.AddRange((from r in db.jobs where r.organizationId == OrganizationID && r.contacts != null && r.contacts.firstName.ToUpper().Replace("à", "a")
                    .Replace("â", "a")
                    .Replace("ä", "a")
                    .Replace("ç", "c")
                    .Replace("é", "e")
                    .Replace("è", "e")
                    .Replace("ê", "e")
                    .Replace("ë", "e")
                    .Replace("î", "i")
                    .Replace("ï", "i")
                    .Replace("ô", "o")
                    .Replace("ù", "u")
                    .Replace("û", "u")
                    .Replace("ü", "u").Contains(upper) select r.id).ToList());
                morelids.AddRange((from r in db.jobs where r.organizationId == OrganizationID && r.contacts != null && r.contacts.primaryEmail.ToUpper().Replace("à", "a")
                    .Replace("â", "a")
                    .Replace("ä", "a")
                    .Replace("ç", "c")
                    .Replace("é", "e")
                    .Replace("è", "e")
                    .Replace("ê", "e")
                    .Replace("ë", "e")
                    .Replace("î", "i")
                    .Replace("ï", "i")
                    .Replace("ô", "o")
                    .Replace("ù", "u")
                    .Replace("û", "u")
                    .Replace("ü", "u").Contains(upper) select r.id).ToList());
                morelids.AddRange((from r in db.jobs where r.organizationId == OrganizationID && r.contacts != null && r.contacts.primaryPhone.ToUpper().Replace("à", "a")
                    .Replace("â", "a")
                    .Replace("ä", "a")
                    .Replace("ç", "c")
                    .Replace("é", "e")
                    .Replace("è", "e")
                    .Replace("ê", "e")
                    .Replace("ë", "e")
                    .Replace("î", "i")
                    .Replace("ï", "i")
                    .Replace("ô", "o")
                    .Replace("ù", "u")
                    .Replace("û", "u")
                    .Replace("ü", "u").Contains(upper) select r.id).ToList());
                morelids.AddRange((from r in db.notes where r.jobs != null && r.jobs.organizationId == OrganizationID && r.message.ToUpper().Replace("à", "a")
                    .Replace("â", "a")
                    .Replace("ä", "a")
                    .Replace("ç", "c")
                    .Replace("é", "e")
                    .Replace("è", "e")
                    .Replace("ê", "e")
                    .Replace("ë", "e")
                    .Replace("î", "i")
                    .Replace("ï", "i")
                    .Replace("ô", "o")
                    .Replace("ù", "u")
                    .Replace("û", "u")
                    .Replace("ü", "u").Contains(upper) select r.jobId.Value).ToList());
              
                var moreids = (from r in morelids select r).Distinct().ToList();


                if (ids.Count == 0)
                {
                    ids.AddRange(moreids);
                }
                else
                {
                    ids = (from r in moreids where ids.Contains(r) select r).ToList();
                }

            }


            var q = GetBaseQuery();
            var dbRecords = (from r in q.Query where ids.Contains(r.id) orderby r.lastUpdate descending select r).ToList();

            return GetJobExtended(dbRecords).ToList();
        }

        public string GetProjectTypeLabel(long projectId, System.Globalization.CultureInfo culture)
        {


            var orgmgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(OrganizationID);
            orgmgr.Load();


            var record = Get(projectId);
            var currentJobExt = new jobsExtended(record, orgmgr.RoundAmount);

            string label = "Quote";
            if (currentJobExt.ProjectStatus.ToString().StartsWith("DeliveryNote")) label = "DeliveryNote";
            if (currentJobExt.ProjectStatus.ToString().StartsWith("Job")) label = "Project";
            if (currentJobExt.ProjectStatus.ToString().StartsWith("Invoice")) label = "Invoice";

            try
            {
                label = SharedResource.Resource.GetString(label, culture);
            }
            catch { }

            return label;
        }

        static string RemoveDiacritics(string text)
        {
            string formD = text.Normalize(NormalizationForm.FormD);
            StringBuilder sb = new StringBuilder();

            foreach (char ch in formD)
            {
                UnicodeCategory uc = CharUnicodeInfo.GetUnicodeCategory(ch);
                if (uc != UnicodeCategory.NonSpacingMark)
                {
                    sb.Append(ch);
                }
            }

            return sb.ToString().Normalize(NormalizationForm.FormC);
        }

        public IList<services_jobs> GetServicesJobsByIds(List<long> list)
        {
            Model.CalaniEntities db = new CalaniEntities();

            var res = db.services_jobs.Where(s => s.recordStatus != (int)RecordStatus.Deleted && list.Contains(s.id)).ToList();
            return res;
        }

        public List<jobs> ListProjectsForScheduler()
        {
            var bq = GetBaseQuery();
            var xQ = (from r in bq.Query.Include("clients") select r);

            var dbRecords = xQ.ToList();

            return dbRecords;
        }

        public double GetInvoicedHoursCurrYear(long jobId)
        {
            if (jobId <= 0) return 0;

            var project = Get(jobId);

            if (project == null) return 0;

            var invIdsFromThisYear = GetInvoicesIdsByDateInvoicing(project.clientId, new DateRange(DatesRangeEnum.CurrentYear));

            var applicableServices = project.services_jobs.Where(sj => sj.recordStatus != (int)RecordStatus.Deleted).Where(s => 
                                (s.toinvoice != null && s.toinvoice > -1 && invIdsFromThisYear.Contains(s.toinvoice.Value)) ||
                                (s.timesheetDate.HasValue && s.timesheetDate.Value.Year == DateTime.Now.Year));

            var hours = applicableServices.Sum(s => s.serviceQuantity ?? 0);

            return hours;
        }
    }
}