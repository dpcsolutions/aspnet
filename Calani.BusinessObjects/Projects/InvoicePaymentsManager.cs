﻿using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Projects
{
    public class InvoicePaymentsManager : Generic.SmartCollectionManager<Model.payments, long>
    {
        public long InvoiceId { get; private set; }

        public InvoicePaymentsManager(long organizationId, long invoiceId)
        {
            InvoiceId = invoiceId;
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<payments> GetDbSet(Model.CalaniEntities db)
        {
            return db.payments;
        }

        internal override SmartQuery<payments> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in GetDbSet(q.Context) where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(payments record)
        {
            if (record != null && record.jobs != null && record.jobs.organizationId != null) return record.jobs.organizationId.Value;
            return -1;
        }

        internal override SmartQuery<payments> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from r in q.Context.payments select r);
            if (InvoiceId > -1) q.Query = (from r in q.Query where r.jobId == InvoiceId select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.datePayed ascending select r);


            return q;
        }


        public List<paymentExtended> GetInvoicePayments()
        {
            var c = GetBaseQuery();
            var records = c.Query.ToList();
            return (from r in records select new paymentExtended(r)).ToList();
        }


        internal override void BeforeAction(SmartAction<payments> action)
        {
            if (action.Type == SmartActionType.Add || action.Type == SmartActionType.Update)
            {
                
            }

        }
    }
}
