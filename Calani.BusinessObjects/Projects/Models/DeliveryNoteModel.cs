﻿using System.Runtime.Serialization;

namespace Calani.BusinessObjects.Projects.Models
{
    [DataContract]
    public class DeliveryNoteModel : JobBaseModel
    {
        [DataMember]
        public ImportedDescriptionModel ImportedDescription { get; set; }

        [DataMember]
        public bool MissingChild { get; set; }

        public DeliveryNoteModel(jobsExtended job) : base(job)
        {
            ImportedDescription = new ImportedDescriptionModel()
            {
                ImportedId = job.importedIn.HasValue ? job.importedIn.Value : 0,
                ImportedInternalId = job.ImportedInInternalId,
                Description = job.description,
            };

            MissingChild = job.MissingChild;
        }
    }
}
