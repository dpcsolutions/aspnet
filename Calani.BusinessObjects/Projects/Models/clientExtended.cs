﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Projects
{
    public class clientExtended
    {
        public String id { get; set; }
        public String text { get; set; }
      
        public clientExtended()
        {

        }
        public clientExtended(Model.clients r)
        {
            id = r.id.ToString();
            text = r.companyName;
        }
    }
}
