﻿using Calani.BusinessObjects.Projects.Models;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Calani.BusinessObjects.Projects
{
    [DataContract]
    public class JobsCollectionModel<T> where T:JobBaseModel
    {
        [DataMember(Name = "data")]
        public List<T> Data { get; set; }
    }
}
