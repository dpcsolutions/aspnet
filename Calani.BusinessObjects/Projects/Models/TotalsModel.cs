﻿using System.Runtime.Serialization;

namespace Calani.BusinessObjects.Projects.Models
{
    [DataContract]
    public class TotalsModel
    {
        [DataMember] public double SubTotalHT { get; set; }

        [DataMember] public double RebaitAmount { get; set; }

        [DataMember] public double TotalHT { get; set; }

        [DataMember] public double TaxAmount { get; set; }

        [DataMember] public double TotalTTC { get; set; }
        
        [DataMember] public double StillToInvoice { get; set; }
    }
}
