﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Projects
{
    public class jobExtended
    {
        public String id { get; set; }
        public String text { get; set; }

        public jobExtended()
        {

        }
        public jobExtended(Model.jobs r)
        {
            id = r.id.ToString();
            text = r.internalId;
            if(!String.IsNullOrWhiteSpace(r.description))
            {
                text += " > ";
                string desc = r.description;
                if (desc.Length > 30) desc = desc.Substring(0, 30) + "...";
                text += desc;
            }
        }
    }
}
