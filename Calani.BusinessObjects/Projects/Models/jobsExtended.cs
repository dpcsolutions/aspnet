﻿using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Projects.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Calani.BusinessObjects.CustomerAdmin;

namespace Calani.BusinessObjects.Projects
{
    public class jobsExtended : Model.jobs
    {
        public string employeeName { get; set; }
        public int RemindersCount { get; set; }

        public double? invoiceDueExclTaxes { get; set; }

        private double? tmpTotalRounded;

        public double? totalToInvoice { get; set; }

        public double? totalRounded
        {
            get { return Math.Round((Math.Round((base.total ?? 0) * 20, MidpointRounding.AwayFromZero) / 20), 2); }
            set => tmpTotalRounded = value;
        }

        private double? tmpinvoiceDueRounded;

        public double? invoiceDueRounded
        {
            get { return Math.Round((Math.Round((base.invoiceDue ?? 0) * 20, MidpointRounding.AwayFromZero) / 20), 2); }
            set => tmpinvoiceDueRounded = value;
        }

        private double? tmptotalInCurrencyRounded;

        public double? totalInCurrencyRounded
        {
            get
            {
                return Math.Round((Math.Round((base.totalInCurrency ?? 0) * 20, MidpointRounding.AwayFromZero) / 20),
                    2);
            }
            set => tmptotalInCurrencyRounded = value;
        }

        private double? tmptotalWithoutTaxesRounded;

        public double? totalWithoutTaxesRounded
        {
            get
            {
                return Math.Round((Math.Round((base.totalWithoutTaxes ?? 0) * 20, MidpointRounding.AwayFromZero) / 20),
                    2);
            }
            set => tmptotalWithoutTaxesRounded = value;
        }

        public bool HasOtherCurrency
        {
            get { return !string.IsNullOrWhiteSpace(currency); }
        }

        public TotalsModel Totals { get; set; }

        public List<long> AdditionalEmployeeIds { get; set; }
        public List<long> TemplateListIds { get; set; }

        public override ICollection<services_jobs> services_jobs
        {
            get { return base.services_jobs.Where(sj => sj.recordStatus != (int) RecordStatus.Deleted).ToList(); }
            set { base.services_jobs = value; }
        }


        public jobsExtended()
        {
        }

        public jobsExtended(Model.jobs r, bool includeReminders = false, bool roundTotal = false,
            bool isVatIncluded = false)
        {
            try {
                //Generic.Tools.CopyAllProperties not used because causes big performance problems
                #region CopyAllProperties
                id = r.id;
                internalId = r.internalId;
                clientId = r.clientId;
                clientContact = r.clientContact;
                clientAddress = r.clientAddress;
                description = r.description;
                rating = r.rating;
                discount = r.discount;
                total = r.total;
                street = r.street;
                street2 = r.street2;
                nb = r.nb;
                npa = r.npa;
                city = r.city;
                country = r.country;
                sentToName = r.sentToName;
                sentToEmail = r.sentToEmail;
                internalNote = r.internalNote;
                dateCreated = r.dateCreated;
                dateQuoteSent = r.dateQuoteSent;
                dateQuoteAccepted = r.dateQuoteAccepted;
                dateQuoteRefused = r.dateQuoteRefused;
                dateQuoteValidity = r.dateQuoteValidity;
                dateOpenJob = r.dateOpenJob;
                dateJobHeld = r.dateJobHeld;
                dateJobDone = r.dateJobDone;
                dateJobValidity = r.dateJobValidity;
                dateInvoicing = r.dateInvoicing;
                dateInvoicingOpen = r.dateInvoicingOpen;
                dateInvoicePending = r.dateInvoicePending;
                dateFullyPaid = r.dateFullyPaid;
                dateInvoiceDue = r.dateInvoiceDue;
                type = r.type;
                parentId = r.parentId;
                importedIn = r.importedIn;
                invoiceDue = r.invoiceDue;
                contractualUnitsCount = r.contractualUnitsCount;
                organizationId = r.organizationId;
                lastUpdate = r.lastUpdate;
                recordStatus = r.recordStatus;
                currency = r.currency;
                currencyRate = r.currencyRate;
                totalInCurrency = r.totalInCurrency;
                invoiceDueInCurrency = r.invoiceDueInCurrency;
                subtype = r.subtype;
                comment = r.comment;
                send_copy_email_to = r.send_copy_email_to;
                totalWithoutTaxes = r.totalWithoutTaxes;
                assigneeId = r.assigneeId;
                deliveredTo = r.deliveredTo;
                color = r.color;
                filterByDate = r.filterByDate;
                createdBy = r.createdBy;
                updatedBy = r.updatedBy;
                customer_reference = r.customer_reference;
                location_intervention = r.location_intervention;
                type_intervention = r.type_intervention;
                discount_type = r.discount_type;
                discount_percent = r.discount_percent;
                discount_type2 = r.discount_type2;
                discount_type3 = r.discount_type3;
                discount_type4 = r.discount_type4;
                discount_percent2 = r.discount_percent2;
                discount_percent3 = r.discount_percent3;
                discount_percent4 = r.discount_percent4;
                skipMissingInvoiceNotif = r.skipMissingInvoiceNotif;
                linkedjobid = r.linkedjobid;
                organizations = r.organizations;
                payments = r.payments;
                contacts = r.contacts;
                addresses = r.addresses;
                recurringinfos1 = r.recurringinfos1;
                recurringinfos = r.recurringinfos;
                services_jobs = r.services_jobs.Where(sj => sj.recordStatus != (int) RecordStatus.Deleted).ToList();
                clients = r.clients;
                #endregion
            }
            catch (Exception e) {
                Console.WriteLine(e);
                throw;
            }

            CalculateProperties(r, includeReminders, isVatIncluded);
        }

        public jobsExtended(bool isQuote, bool isJob, bool isInvoice, bool isDeliveryNote, long organizationId)
        {
            DateTime now = DateTime.Now;

            var mgr = new ProjectsManager(organizationId);
            //mgr.IgnoreRecordStatus = true;

            string tInternalId = null;

            AdditionalEmployeeIds = new List<long>();
            TemplateListIds = new List<long>();
            
            if (isInvoice)
            {
                dateInvoicing = now;
                tInternalId = mgr.GetLastInvoiceInternalId();
            }

            if (isJob)
            {
                dateOpenJob = now;
                tInternalId = mgr.GetLastOpenInternalId();
            }

            if (isQuote)
            {
                dateQuoteSent = null;
                tInternalId = mgr.GetLastQuoteInternalId();
                subtype = "Quote";
            }

            if (isDeliveryNote)
            {
                dateQuoteSent = null;
                tInternalId = mgr.GetLastDeliveryNoteInternalId();
                subtype = "DeliveryNote";
            }

            //mgr = new ProjectsManager(organizationId);

            var inc = new Calani.BusinessObjects.Generic.IncrementalNumber(tInternalId);
            do
            {
                inc.Increment();
            } while (!mgr.IsFreeName(inc.ToString()));

            this.internalId = inc.ToString();
        }

        public void CalculateProperties(Model.jobs r, bool includeReminders, bool isVatIncluded)
        {
            if (dateFullyPaid != null)
            {
                invoiceDue = 0;
            }

            if (includeReminders)
            {
                RemindersCount = r.jobreminders.Count;
            }

            if (r.invoiceDue != null && r.total != null)
            {
                double percent = r.invoiceDue.Value / r.total.Value;

                if (r.totalWithoutTaxes != null)
                {
                    invoiceDueExclTaxes = isVatIncluded
                        ? r.totalWithoutTaxes.Value * percent
                        : r.totalWithoutTaxes.Value * percent;

                    invoiceDueExclTaxes = Math.Round(invoiceDueExclTaxes.Value, 2, MidpointRounding.AwayFromZero);
                }
            }

            Totals = CalculateAllProperties(isVatIncluded, true);
            totalToInvoice = Totals.StillToInvoice;
        }

        private TotalsModel CalculateAllProperties(bool isVatIncluded, bool inCurrency)
        {
            var model = new TotalsModel();
            var services = services_jobs.ToList();

            model.RebaitAmount = CalculateTotalRebaitAmount(isVatIncluded);
            foreach (var service in services)
            {
                if (isVatIncluded)
                {
                    model.SubTotalHT +=
                        VatIncludedCalcManager.GetPriceExcludingVat(service.totalPriceExclTax, service.taxRate);
                    model.TaxAmount += VatIncludedCalcManager.GetVatSum(service.totalPriceExclTax, service.taxRate);
                }
                else
                {
                    model.SubTotalHT += CalculateServiceTotalExclTax(service, inCurrency);
                    model.TaxAmount += CalculateServiceVAT(service, inCurrency);
                }

                if (service.toinvoice == null)
                {
                    model.StillToInvoice += service.totalPriceExclTax.Value -
                                            service.totalPriceExclTax.Value * (discount_percent ?? 0) / 100;
                }
            }

            model.TotalHT = model.SubTotalHT - model.RebaitAmount;
            model.TotalTTC = isVatIncluded
                ? model.TotalHT
                : model.TotalHT + model.TotalHT;

            return model;
        }

        private string tmpClientName;

        public string clientName
        {
            get
            {
                if (this.clients == null) return null;
                return this.clients.companyName;
            }
            set { tmpClientName = value; }
        }

        private int doneVisitstmp;

        public int doneVisits
        {
            get
            {
                int f = Convert.ToInt32(VisitStatusEnum.Done);
                DateTime now = DateTime.Now;
                return (from r in visits where r.visitStatus == f select r).Count();
            }
            set { doneVisitstmp = value; }
        }

        private int plannedVisitstmp;

        public int plannedVisits
        {
            get
            {
                int f = Convert.ToInt32(VisitStatusEnum.Planned);
                DateTime now = DateTime.Now;
                return (from r in visits where r.visitStatus == f select r).Count();
            }
            set { plannedVisitstmp = value; }
        }

        private int? tmpremainingVisits;

        public int? remainingVisits
        {
            get
            {
                int? ret = null;
                if (this.contractualUnitsCount != null && this.contractualUnitsCount.Value > 0)
                {
                    ret = this.contractualUnitsCount.Value;
                    ret -= doneVisits;
                }

                return ret;
            }
            set { tmpremainingVisits = value; }
        }

        private DateTime? tmpnextVisitDate;

        public DateTime? nextVisitDate
        {
            get
            {
                int f = Convert.ToInt32(VisitStatusEnum.Planned);
                DateTime? ret =
                    (from r in visits
                        where r.visitStatus == f && r.dateStart > DateTime.Now
                        orderby r.dateStart ascending
                        select r.dateStart).FirstOrDefault();
                if (ret == null)
                    ret = (from r in visits where r.visitStatus == f orderby r.dateStart ascending select r.dateStart)
                        .FirstOrDefault();
                return ret;
            }
            set { tmpnextVisitDate = value; }
        }

        private bool tmpnextVisitMissingRessource;

        public bool nextVisitMissingRessource
        {
            get
            {
                if (nextVisitDate != null)
                {
                    var c = nextVisitContacts;
                    if (c == null)
                    {
                        return true;
                    }

                    if (c.Count == 0)
                    {
                        return true;
                    }
                }

                return false;
            }
            set { tmpnextVisitMissingRessource = value; }
        }

        Dictionary<string, string> tmpnextVisitContacts;

        public Dictionary<string, string> nextVisitContacts
        {
            get
            {
                int f = Convert.ToInt32(VisitStatusEnum.Planned);
                var nv = (from r in visits
                    where r.visitStatus == f && r.dateStart > DateTime.Now
                    orderby r.dateStart ascending
                    select r).FirstOrDefault();
                if (nv == null)
                    nv = (from r in visits where r.visitStatus == f orderby r.dateStart ascending select r)
                        .FirstOrDefault();


                if (nv != null && nv.contacts != null)
                {
                    Dictionary<string, string> ret = new Dictionary<string, string>();
                    foreach (var contact in nv.contacts)
                    {
                        ret.Add(contact.initials, contact.firstName + " " + contact.lastName);
                    }

                    return ret;
                }

                return null;
            }
            set { tmpnextVisitContacts = value; }
        }

        public List<ProjectItem> ExportItems()
        {
            List<AjaxService.ProjectItem> ret = new List<AjaxService.ProjectItem>();

            var items = this.services_jobs.ToList();

            foreach (var item in items)
            {
                AjaxService.ProjectItem i = new AjaxService.ProjectItem();
                ret.Add(i);

                i.id = item.id;
                i.isChecked = item.is_checked ?? false;
                i.customNumber = item.custom_number;
                i.item = item.serviceName;
                i.isDelivered = item.isDeliveried ?? false;
                if (item.serviceId != null)
                {
                    //i.item = item.services.internalId + " - " + item.services.name;
                    i.item = item.serviceName; // keep the changed label
                }

                i.serviceId = item.serviceId;
                i.serviceName = item.serviceName;
                i.unitName = item.unitName;
                i.position = item.position;
                if (item.services != null)
                {
                    i.serviceInternalId = item.services.internalId;
                    i.serviceVatIncl = item.services.price_including_vat;
                }


                if (item.serviceQuantity != null)
                    i.qty = item.serviceQuantity.Value;

                i.tax = item.taxName;

                if (item.taxRate != null)
                    i.rate = item.taxRate.Value;

                if (item.discount != null)
                    i.discount = item.discount.Value;

                if (item.toinvoice != null)
                    i.toinvoice = item.toinvoice;

                if (!String.IsNullOrWhiteSpace(item.currency))
                {
                    if (item.servicePrice != null)
                        i.price = item.servicePriceInCurrency.Value;

                    if (item.servicePrice != null)
                        i.totalprice = item.totalPriceExclTaxInCurrency.Value;
                }
                else
                {
                    if (item.servicePrice != null)
                        i.price = item.servicePrice.Value;
                    if (item.totalPriceExclTax != null)
                        i.totalprice = item.totalPriceExclTax.Value;
                }
            }

            return ret;
        }

        private ProjectStatus ProjectStatusTmp;

        public ProjectStatus ProjectStatus
        {
            get
            {
                ProjectStatus ret = ProjectStatus.QuoteDraft;


                if (dateOpenJob != null)
                {
                    if (dateJobHeld != null)
                    {
                        ret = ProjectStatus.JobOnHold;
                    }
                    else
                    {
                        ret = ProjectStatus.JobOpened;
                    }


                    if (dateJobDone != null)
                    {
                        ret = ProjectStatus.JobFinished;
                    }
                    else
                    {
                        if (dateJobValidity < DateTime.Now)
                        {
                            ret = ProjectStatus.JobOutDated;
                        }
                    }
                }

                if (dateInvoicing != null)
                {
                    ret = ProjectStatus.InvoiceDraft;

                    if (dateInvoicePending != null || dateInvoicingOpen != null)
                    {
                        ret = ProjectStatus.InvoicePaymentDue;

                        if (dateInvoiceDue != null && dateInvoiceDue < DateTime.Now)
                        {
                            ret = ProjectStatus.InvoicePaymentLate;
                        }
                    }

                    if (dateFullyPaid != null)
                    {
                        ret = ProjectStatus.InvoicePaid;
                    }
                }


                if (dateOpenJob == null && dateInvoicing == null) // it's a quote
                {
                    if (subtype == null || subtype == "Quote")
                    {
                        if (dateQuoteSent != null && dateQuoteAccepted == null && dateQuoteRefused == null)
                        {
                            if (dateQuoteValidity > DateTime.Now)
                            {
                                ret = ProjectStatus.QuoteSent;
                            }
                            else
                            {
                                ret = ProjectStatus.QuoteOutDated;
                            }
                        }

                        else if (dateQuoteAccepted != null && dateOpenJob == null)
                        {
                            ret = ProjectStatus.QuoteAccepted;
                        }

                        else if (dateQuoteRefused != null && dateOpenJob == null)
                        {
                            ret = ProjectStatus.QuoteRefused;
                        }
                    }
                    else if (subtype == "DeliveryNote")
                    {
                        ret = ProjectStatus.DeliveryNoteDraft;

                        if (dateQuoteSent != null && dateQuoteAccepted == null && dateQuoteRefused == null)
                        {
                            ret = ProjectStatus.DeliveryNoteSent;
                        }

                        else if (dateQuoteAccepted != null && dateOpenJob == null)
                        {
                            ret = ProjectStatus.DeliveryNoteAccepted;
                        }

                        else if (dateQuoteRefused != null && dateOpenJob == null)
                        {
                            ret = ProjectStatus.DeliveryNoteRefused;
                        }
                    }
                }


                return ret;
            }
            set { ProjectStatusTmp = value; }
        }


        public string Tag1 { get; set; }
        public string Tag2 { get; set; }

        public string ImportedInInternalId { get; set; }

        public bool MissingChild { get; set; }

        private DateTime? DateTmp;

        public DateTime? Date
        {
            get { return this.dateFullyPaid ?? this.lastUpdate ?? this.dateCreated; }
            set { DateTmp = value; }
        }

        private DateTime DisplayedDateTmp;

        public DateTime DisplayedDate
        {
            get
            {
                DateTime? ret = null;
                if (ProjectStatus.ToString().StartsWith("Quote"))
                {
                    if (this.dateQuoteSent != null)
                        ret = this.dateQuoteSent.Value;
                }

                if (ProjectStatus.ToString().StartsWith("Invoice"))
                {
                    if (this.dateInvoicing != null)
                        ret = this.dateInvoicing.Value;

                    if (this.dateInvoicingOpen != null)
                        ret = this.dateInvoicingOpen.Value;
                }

                if (ret == null)
                {
                    if (this.dateCreated != null)
                        ret = this.dateCreated;
                }

                if (ret == null)
                {
                    if (this.lastUpdate != null)
                        ret = this.lastUpdate;
                }

                if (ret == null)
                {
                    ret = new DateTime(0, 1, 1);
                }

                return ret.Value;
            }
            set { DisplayedDateTmp = value; }
        }

        private string OrderTagTmp;

        public string OrderTag
        {
            get
            {
                return (this.clients?.companyName ?? "").GetSystemFriendlyStringWithEuropeanCharacters() +
                       this.description + this.internalId;
            }
            set { OrderTagTmp = value; }
        }

        public double GetTotalItemsDiscount()
        {
            double totalDiscount = 0;

            if (services_jobs != null && services_jobs.Any())
            {
                totalDiscount = services_jobs.Sum(sj => sj.discount ?? 0);
            }

            return totalDiscount;
        }

        public double CalculateTotalExclTax(bool inCurrency = true, bool isVatIncluded = false)
        {
            if (!services_jobs.Any()) return 0;

            double totalExclTax = 0;

            var services = services_jobs.ToList();
            foreach (var service in services)
            {
                totalExclTax += isVatIncluded
                    ? VatIncludedCalcManager.GetPriceExcludingVat(service.totalPriceExclTax, service.taxRate)
                    : CalculateServiceTotalExclTax(service, inCurrency);
            }

            totalExclTax = Math.Round(totalExclTax, 2, MidpointRounding.AwayFromZero);

            return totalExclTax;
        }

        public double CalculateToInvoice()
        {
            if (!services_jobs.Any()) return 0;
            var smrg = new ServicesManager(0);
            double toInvoice = 0;

            var services = services_jobs.ToList();
            foreach (var service in services)
            {
                if (service.toinvoice == null)
                {
                    toInvoice += service.totalPriceExclTax.Value -
                                 service.totalPriceExclTax.Value * (discount_percent ?? 0) / 100;
                }
            }

            toInvoice = Math.Round(toInvoice, 2, MidpointRounding.AwayFromZero);
            //toInvoice = CalculateTotalExclTax() == toInvoice ? 0.0 : toInvoice;

            return toInvoice;
        }

        public double CalculateVAT(bool inCurrency = true, bool isVatIncluded = false)
        {
            if (!services_jobs.Any()) return 0;

            double totalVAT = 0;

            var services = services_jobs.ToList();
            foreach (var service in services)
            {
                totalVAT += isVatIncluded
                    ? VatIncludedCalcManager.GetVatSum(service.totalPriceExclTax, service.taxRate)
                    : CalculateServiceVAT(service, inCurrency);
            }

            totalVAT = Math.Round(totalVAT, 2, MidpointRounding.AwayFromZero);

            return totalVAT;
        }

        private double CalculateServiceTotalExclTax(services_jobs service, bool inCurrency)
        {
            if (service == null) return 0;

            var price = inCurrency && HasOtherCurrency ? service.servicePriceInCurrency : service.servicePrice;
            var itemAmountExclTax = (service.serviceQuantity ?? 0) * (price ?? 0);
            itemAmountExclTax = itemAmountExclTax * (100 - (service.discount ?? 0)) / 100;
            return itemAmountExclTax;
        }

        private double CalculateServiceTotalExclTaxDiscounted(services_jobs service, bool inCurrency)
        {
            var serviceTotalExclTaxDiscounted = CalculateServiceTotalExclTax(service, inCurrency) *
                (100 - (discount_percent ?? 0)) / 100;
            return serviceTotalExclTaxDiscounted;
        }

        private double CalculateServiceVAT(services_jobs service, bool inCurrency)
        {
            if (service == null) return 0;

            var vat = CalculateServiceTotalExclTaxDiscounted(service, inCurrency) * (service.taxRate ?? 0) / 100;

            return vat;
        }

        public double CalculateTotalRebaitAmount(bool inCurrency = true, bool isVatIncluded = false)
        {
            return CalculateRebaitAmount(0, inCurrency, isVatIncluded);
        }

        public double CalculateRebaitAmount(int ordinal, bool inCurrency = true, bool isVatIncluded = false)
        {
            double totalExclTax = CalculateTotalExclTax(inCurrency, isVatIncluded);
            var discounts = new double[4];

            discounts[0] = totalExclTax * (discount_percent ?? 0) / 100;
            discounts[1] = (totalExclTax - discounts.Sum()) * (discount_percent2 ?? 0) / 100;
            discounts[2] = (totalExclTax - discounts.Sum()) * (discount_percent3 ?? 0) / 100;
            discounts[3] = (totalExclTax - discounts.Sum()) * (discount_percent4 ?? 0) / 100;

            if (ordinal == 0)
            {
                double discountAmountTotal = discounts.Sum();
                discountAmountTotal = Math.Round(discountAmountTotal, 2, MidpointRounding.AwayFromZero);
                return discountAmountTotal;
            }
            else if (ordinal >= 1 && ordinal <= 4)
            {
                return discounts[ordinal - 1];
            }

            throw new ArgumentException("Ordinal should be between 0 and 4", nameof(ordinal));
        }

        public double CalculateTotalExclTaxDiscounted(bool inCurrency = true, bool vatIncluded = false)
        {
            double totalExclTaxDiscounted = CalculateTotalExclTax(inCurrency, vatIncluded) -
                                            CalculateTotalRebaitAmount(inCurrency, vatIncluded);

            totalExclTaxDiscounted = Math.Round(totalExclTaxDiscounted, 2, MidpointRounding.AwayFromZero);

            return totalExclTaxDiscounted;
        }

        public double CalculateTotal(bool rounded, bool inCurrency = true, bool vatIncluded = false)
        {
            double total = vatIncluded
                ? CalculateTotalExclTaxDiscounted(inCurrency)
                : CalculateTotalExclTaxDiscounted(inCurrency) + CalculateVAT(inCurrency, false);

            if (rounded)
            {
                total = Math.Round(Math.Round(total * 20) / 20, 2);
            }

            return total;
        }

        public double CalculatePayments()
        {
            if (payments == null || !payments.Any()) return 0;

            var paid = payments.Sum(p => p.amountPayed ?? 0);

            return paid;
        }

        public double CalculateDue(bool rounded, bool inCurrency = true, bool vatIncluded = false)
        {
            var due = CalculateTotal(rounded, inCurrency, vatIncluded) - CalculatePayments();
            return due;
        }

        public List<(double, double, double)> CalculateVATPerRate(bool inCurrency = true, bool vatIncluded = false)
        {
            if (services_jobs == null || !services_jobs.Any())
            {
                return new List<(double, double, double)>();
            }

            var vatsPerRate = services_jobs.GroupBy(s => s.taxRate).Select(g =>
                {
                    var rate = g.Key;
                    var totalExclTaxDiscountedOverServices =
                        g.Sum(s => CalculateServiceTotalExclTaxDiscounted(s, inCurrency));
                    var vat = vatIncluded
                        ? totalExclTaxDiscountedOverServices -
                          totalExclTaxDiscountedOverServices / (1 + (rate ?? 0) / 100)
                        : totalExclTaxDiscountedOverServices * (rate ?? 0) / 100;
                    return (rate ?? 0, Math.Round(totalExclTaxDiscountedOverServices, 2, MidpointRounding.AwayFromZero),
                        Math.Round(vat, 2, MidpointRounding.AwayFromZero));
                })
                .Where(t => t.Item1 > 0)
                .ToList();

            return vatsPerRate;
        }
    }
}