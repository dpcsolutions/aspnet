﻿using Calani.BusinessObjects.Generic;
using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace Calani.BusinessObjects.Projects.Models
{
    [DataContract]
    public class JobBaseModel
    {
        [DataMember]
        public IdsModel Ids { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public long ClientId { get; set; }

        [DataMember]
        public string ClientName { get; set; }

        [DataMember]
        public double TotalHt { get; set; }

        [DataMember]
        public double Total { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public DateTime? Date { get; set; }

        [DataMember]
        public string DateStr { get; set; }

        [DataMember]
        public DateTime? LastUpdateDate { get; set; }

        [DataMember]
        public string LastUpdateDateStr { get; set; }

        [DataMember]
        public DateTime? FilterByDate { get; set; }

        [DataMember]
        public string FilterByDateStr { get; set; }

        [DataContract]
        public class IdsModel
        {
            [DataMember]
            public long Id { get; set; }

            [DataMember]
            public string InternalId { get; set; }
        }

        public JobBaseModel(jobsExtended job)
        {
            Ids = new IdsModel() { Id = job.id, InternalId = job.internalId };
            Description = job.description;
            ClientId = job.clientId;
            ClientName = job.clientName;
            TotalHt = job.CalculateTotalExclTaxDiscounted(inCurrency: false);
            Total = job.total ?? 0;
            Status = job.ProjectStatus.ToString();
            LastUpdateDate = job.lastUpdate;
            Date = job.dateFullyPaid ?? job.lastUpdate ?? job.dateCreated;
            FilterByDate = job.filterByDate;

            LastUpdateDateStr = LastUpdateDate.HasValue ? LastUpdateDate.Value.ToString(Tools.DefaultDateTimeFormat, CultureInfo.InvariantCulture) : string.Empty;
            DateStr = Date.HasValue ? Date.Value.ToString(Tools.DefaultDateTimeFormat, CultureInfo.InvariantCulture) : string.Empty;
            FilterByDateStr = FilterByDate.HasValue ? FilterByDate.Value.ToString(Tools.DefaultDateTimeFormat, CultureInfo.InvariantCulture) : string.Empty;
        }
    }
}
