﻿using Calani.BusinessObjects.Generic;
using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace Calani.BusinessObjects.Projects.Models
{
    [DataContract]
    public partial class QuoteModel : DeliveryNoteModel
    {
        [DataMember]
        public int? Rating { get; set; }

        [DataMember]
        public DateTime? DateJobValidity { get; set; }

        [DataMember]
        public string DateJobValidityStr { get; set; }

        [DataMember]
        public DateTime? DateQuoteAccepted { get; set; }

        [DataMember]
        public string DateQuoteAcceptedStr { get; set; }

        [DataMember]
        public DateTime? DateQuoteRefused { get; set; }

        [DataMember]
        public string DateQuoteRefusedStr { get; set; }

        public QuoteModel(jobsExtended job) : base(job)
        {
            Rating = job.rating;
            DateJobValidity = job.dateJobValidity;
            DateQuoteAccepted = job.dateQuoteAccepted;
            DateQuoteRefused = job.dateQuoteRefused;

            DateJobValidityStr = DateJobValidity.HasValue ? DateJobValidity.Value.ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture) : string.Empty;
            DateQuoteAcceptedStr = DateQuoteAccepted.HasValue ? DateQuoteAccepted.Value.ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture) : string.Empty;
            DateQuoteRefusedStr = DateQuoteRefused.HasValue ? DateQuoteRefused.Value.ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture) : string.Empty;
        }
    }
}
