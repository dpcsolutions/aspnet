﻿using Calani.BusinessObjects.Generic;
using System;
using System.Globalization;
using System.Runtime.Serialization;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Projects.Extensions;

namespace Calani.BusinessObjects.Projects.Models
{
    [DataContract]
    public class InvoiceModel : JobBaseModel
    {
        [DataMember] public int RemindersCount { get; set; }

        [DataMember] public double TotalExTax { get; set; }

        [DataMember] public double Due { get; set; }

        [DataMember] public double DueExTax { get; set; }

        [DataMember] public bool IsDraft { get; set; }

        [DataMember] public DateTime? DueDate { get; set; }

        [DataMember] public string DueDateStr { get; set; }

        [DataMember] public DateTime? FullyPaidDate { get; set; }

        [DataMember] public string FullyPaidDateStr { get; set; }

        [DataMember] public DateTime? ValidityDate { get; set; }

        [DataMember] public string ValidityDateStr { get; set; }

        [DataMember] public recurringinfos RecurringInfo { get; set; }

        public InvoiceModel(jobsExtended job)
            : base(job)
        {
            RemindersCount = job.RemindersCount;
            RecurringInfo = job.recurringinfos?.CloneRecurringInfo();
            Total = job.total ?? 0;
            TotalExTax = job.totalWithoutTaxes ?? 0;
            Due = job.invoiceDue ?? 0;
            DueExTax = job.invoiceDueExclTaxes.HasValue && !double.IsNaN(job.invoiceDueExclTaxes.Value)
                ? job.invoiceDueExclTaxes.Value
                : 0;
            IsDraft = job.ProjectStatus == ProjectStatus.InvoiceDraft;
            DueDate = job.dateInvoiceDue;
            FullyPaidDate = job.dateFullyPaid;
            ValidityDate = job.dateJobValidity;
            DueDateStr = DueDate.HasValue
                ? DueDate.Value.ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture)
                : string.Empty;
            FullyPaidDateStr = FullyPaidDate.HasValue
                ? FullyPaidDate.Value.ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture)
                : string.Empty;
            ValidityDateStr = ValidityDate.HasValue
                ? ValidityDate.Value.ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture)
                : string.Empty;
        }
    }
}