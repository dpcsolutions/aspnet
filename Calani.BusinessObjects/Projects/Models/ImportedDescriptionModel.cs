﻿using System.Runtime.Serialization;

namespace Calani.BusinessObjects.Projects.Models
{
    [DataContract]
    public class ImportedDescriptionModel
    {
        [DataMember]
        public long ImportedId { get; set; }

        [DataMember]
        public string ImportedInternalId { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}
