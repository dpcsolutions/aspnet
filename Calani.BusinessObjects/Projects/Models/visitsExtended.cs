﻿using Calani.BusinessObjects.Calendars;
using Calani.BusinessObjects.Generic;
using System;
using System.Collections.Generic;

namespace Calani.BusinessObjects.Projects
{
    public class visitsExtended
    {
        public class resource
        {
            public resource()
            {

            }

            public resource(long sid, long effectiveTime, string sname, string sInitials, long ltimesheetid)
            {
                id = sid;
                effectiveTimeInMinutes = effectiveTime;
                timesheetid = ltimesheetid;
                name = sname;
                initials = sInitials;
            }

            public long id;
            public long effectiveTimeInMinutes = 0;
            public string name = "";
            public string initials = "";
            public long timesheetid = -1;
        }

        public class visitsExtendedEqualityComparer : IEqualityComparer<visitsExtended>
        {

            public bool Equals(visitsExtended x, visitsExtended y)
            {
                return x.id == y.id;
            }

            public int GetHashCode(visitsExtended obj)
            {
                return obj.id.GetHashCode();
            }
        }

        public String id { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public String addressLine1 { get; set;  }
        public String addressLine2 { get; set; }
        public String addressLine3 { get; set; }
        public String addressLine4 { get; set; }
        public string phone { get; set; }
        public string contactName { get; set; }
        public string backgroundColor { get; set; }
        public string borderColor { get; set; }
        public string textColor { get; set; }
        public string icon { get; set; }
        public List<resource> resourcesObj { get; set; }
        public List<long> resources { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string jobDescription { get; set; }
        public string jobInternalNumber { get; set; }
        public int visitStatus { get; set; }
        public string resourcesStr { get; set; }
        public string companyName { get; set; }
        public int visitType { get; set; }
        public long jobId { get; set; }
        public long clientId { get; set; }
        public string repeatGroupId { get; set; }
        public long numrepeat { get; set; }
        public string repeatdays { get; set; }
        public long internalServiceId { get; set; }
        public long lastModifiedById { get; set; }
        
        public bool isHolidays { get; set; }
        public bool isOfficialHolidays { get; set; }
        public bool allDay { get; set; }
        public string rendering { get; set; }

        public visitsExtended()
        {

        }

        public visitsExtended(Model.visits r, string sBackgroundColor = null, string sBorderColor = null, string sTextColor = null)
        {
            id = r.id.ToString();

            backgroundColor = (sBackgroundColor != null ? sBackgroundColor : null);
            borderColor = (sBorderColor != null ? (string)sBorderColor : backgroundColor);
            textColor = (sTextColor != null ? sTextColor : null);
            clientId = (r.jobs != null ? r.jobs.clientId : 0);
            isHolidays = r.type == 2;
            isOfficialHolidays = r.type == 3;
            start = FormatDate(r.dateStart);
            end = FormatDate(r.dateEnd);
            visitStatus = r.visitStatus != null ? (int)r.visitStatus : 0;
            resources = new List<long>();
            if (r.type != null) visitType = r.type.Value;
            else r.type = 1;
            

            description = (visitType != (int)VisitTypeEnum.PrivateVisit) ? r.description : "";
            jobInternalNumber = (r.jobs != null ? (string)r.jobs.internalId : "");
            repeatGroupId = r.repeatGroupId;
            numrepeat = r.numrepeat != null ? (int)r.numrepeat : 0;
            repeatdays = r.repeatdays;
            internalServiceId = r.serviceId != null ? (long)r.serviceId : -1;

            lastModifiedById = r.lastModifiedById??0;

            if (r.jobs != null)
            {
                if (r.jobs.type == null || r.jobs.type == (int)ProjectTypeEnum.Billable)
                {
                    addressLine1 = (r.jobs.nb != null ? r.jobs.nb + " ": "") + (r.jobs.street != null ? r.jobs.street : "");
                    addressLine2 = (r.jobs.street2 != null ?  r.jobs.street2 : null);
                    addressLine3 = (r.jobs.npa != null ? r.jobs.npa.ToString() + " " : "") + (r.jobs.city != null ? r.jobs.city : "");
                    addressLine4 = (r.jobs.country != null ? r.jobs.country : null);

                    addressLine1 = (addressLine1 == "" ? addressLine1 = null : addressLine1);
                    addressLine3 = (addressLine3 == "" ? addressLine3 = null : addressLine3);
                }

                companyName = (r.jobs.clients != null ? r.jobs.clients.companyName : null);
                title = (companyName != null ? companyName : "") + " - " + 
                   ( r.jobs.contacts != null ? 
                        (r.jobs.contacts.firstName.Length > 0 ? r.jobs.contacts.firstName.Substring(0,1):"") + ". " + r.jobs.contacts.lastName : "");

                contactName = (r.jobs.contacts != null ? r.jobs.contacts.firstName + " " + r.jobs.contacts.lastName : null);
                phone = (r.jobs.contacts != null ? r.jobs.contacts.primaryPhone : null);
                jobId = r.jobs.id;
                jobDescription = r.jobs.description;
            }

            if (r.contacts != null)
            {
                resourcesObj = new List<resource>();

                foreach (var c in r.contacts)
                {
                    resources.Add(c.id);
                    if (resourcesStr == null)
                        resourcesStr = "";

                    string shortName = (c.firstName != null && c.firstName.Length > 0 ? c.firstName.Substring(0, 1) + ". " : "") + (c.lastName != null ? c.lastName : "");
                    resourcesStr += (resourcesStr == "" ? "" : ", ") + shortName;

                    resourcesObj.Add(new resource(c.id, 0, shortName, c.initials, -1));
                }

                if (r.contacts.Count > 1)
                {
                    backgroundColor = "#fffaf0";
                    borderColor = "#000000";
                    textColor = "#000000";
                }
            }
        }

        public visitsExtended(CalendarRecordDto calendarRecord)
        {
            id = $"calev_{calendarRecord.Id}";
            start = FormatDate(calendarRecord.StartDate);
            end = FormatDate(calendarRecord.EndDate);
            allDay = true;
            title = calendarRecord.Name;
            description = calendarRecord.Name;
            rendering = "background";
            isHolidays = true;
            resources = new List<long>();
            resourcesObj = new List<resource>();
            visitType = (int)VisitTypeEnum.CustomCalendarEvent;
            backgroundColor = "#FCF876";
            borderColor = backgroundColor;
            textColor = "#000";
        }

        private string FormatDate(DateTime? date)
        {
            var formattedDate = date != null ? ((DateTime)date).ToString(Tools.ProjectVisitDateFormat, System.Globalization.CultureInfo.InvariantCulture) : "";
            return formattedDate;
        }
    }
}
