﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Projects
{
    public class resourcesExtended
    {
        public String id { get; set; }
        public String text { get; set; }
        public String title { get; set; }
        public String color { get; set; }
        public string initials { get; set; }

        public resourcesExtended()
        {

        }
        public resourcesExtended(Model.contacts r)
        {
            Random rnd = new Random();
            var c = Color.FromArgb(rnd.Next(0, 256), rnd.Next(0, 256), rnd.Next(0, 256));

            id = r.id.ToString();
            text = r.firstName + " " + r.lastName;
            title = r.firstName + " " + r.lastName;
            color = "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");

            if(!String.IsNullOrWhiteSpace(r.initials))
            {
                initials = r.initials;
            }
            else
            {
                r.initials = "";
                if (!String.IsNullOrWhiteSpace(r.firstName) && r.firstName.Length > 1) r.initials += r.firstName.Substring(0, 1);
                if (!String.IsNullOrWhiteSpace(r.lastName) && r.lastName.Length > 2) r.initials += r.lastName.Substring(0, 2);
                r.initials = r.initials.ToUpper();
            }
        }
    }
}
