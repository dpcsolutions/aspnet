﻿namespace Calani.BusinessObjects.Projects.Models
{
    public class WorkReportTemplateModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public byte[] Logo { get; set; }
        public string CreatorStr { get; set; }
        public string ValidatorStr { get; set; }
        public string ProjectStr { get; set; }
        public int Year { get; set; }
        public string CompanyName { get; set; }
    }
}
