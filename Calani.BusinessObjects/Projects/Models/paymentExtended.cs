﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Projects
{
    public class paymentExtended
    {
        public string datePayed { get; set; }
        public double amountPayed { get; set; }
        public string comment { get; set; }

        public paymentExtended()
        {

        }

        public paymentExtended(Model.payments p)
        {
            if(p.datePayed != null) datePayed = p.datePayed.Value.ToString("dd/MM/yyyy");
            if (p.datePayed != null) amountPayed = p.amountPayed.Value;
            comment = p.comment;
        }
    }
}
