﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Projects
{
    public class ProjectEmailDocumentsSender
    {
        public long OrganizationId { get; set; }
        public AjaxService.SendEmail Email { get; set; }
        public string From { get; set; }
        public long FromUserId { get; set; }

        public void Send(bool htmlEncode = true)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();

            List<Generic.MailTo> mails = new List<Generic.MailTo>();
            mails.Add(new Generic.MailTo { Email = From, UserId = FromUserId });

            var contact = (from r in db.contacts where r.id == Email.toContact && r.organizationId == OrganizationId select r).FirstOrDefault();
            if (contact != null)
            {
                if (String.IsNullOrWhiteSpace(contact.primaryEmail))
                    throw new Exception("No email for this contact");
                
                if (mails.All(x => x.Email != contact.primaryEmail))
                    mails.Add(new Generic.MailTo { Email = contact.primaryEmail, UserId = FromUserId});
                    
            }

            string orgname = (from r in db.organizations where r.id == OrganizationId select r.email).FirstOrDefault();
            string orgname2 = (from r in db.organizations where r.id == OrganizationId select r.name).FirstOrDefault();

            if (!String.IsNullOrWhiteSpace(Email.toEmail))
            {
                Email.toEmail = Email.toEmail.Replace(",", ";");
                List<string> moreMails = Email.toEmail.Split(';').ToList();
                moreMails = (from r in moreMails where !String.IsNullOrWhiteSpace(r) && Generic.EmailValidator.IsEmailValid(r.Trim()) select r).ToList();
                if (moreMails.Count > 0)
                {
                    foreach (var m in moreMails)
                    {
                        if (mails.All(x => x.Email != m))
                            mails.Add(new Generic.MailTo { Email = m, UserId = FromUserId });
                    }
                }
            }

            List<Generic.MailAttachment> attachmentsbag = new List<Generic.MailAttachment>();
            Attachments.AttachmentsManager mgr = new Attachments.AttachmentsManager();
            mgr.OrganizationId = OrganizationId;
            var attachments = (from r in db.attachments where r.organizationId == OrganizationId && Email.attachments.Contains(r.id) select r).ToList();
            List<System.Net.Mail.Attachment> atts = null;
            if(attachments != null && attachments.Count > 0)
            {
                atts = new List<System.Net.Mail.Attachment>();
                foreach (var attachmentBase in attachments)
                {

        

                    Attachments.attachmentsExtended attachment = new Attachments.attachmentsExtended(attachmentBase);
                    mgr.ResolveFileName(attachment);
                    string customName = mgr.GetCustomName(attachment);
     

                    attachmentsbag.Add(new Generic.MailAttachment
                    {
                        File = attachment.FilePath,
                        CustomName = customName
                    });
                }
            }

            if (contact != null && mails.All(x => x.Email != contact.primaryEmail))
            {
                mails.Add(new Generic.MailTo { Email = contact.primaryEmail, UserId = FromUserId });
            }

            Generic.MailSender mailSender = new Generic.MailSender();
            mailSender.Attachments.AddRange(attachmentsbag);
            mailSender.MailToMany = mails;
            mailSender.CustomSubject = Email.subject;
            mailSender.ReplyTo = From;
            mailSender.FromName = orgname2;
           

            string htmlbody = Email.body;
            htmlbody = htmlEncode ? System.Web.HttpUtility.HtmlEncode(htmlbody) : htmlbody;
            htmlbody = htmlbody.Replace("\r\n", "\n");
            htmlbody = htmlbody.Replace("\n", "<br />\n");

            mailSender.Variables.Add("message", htmlbody);
            mailSender.Variables.Add("orgid", OrganizationId.ToString());
            mailSender.Variables.Add("orgmail", orgname);
            mailSender.Variables.Add("PublicUrl", System.Configuration.ConfigurationManager.AppSettings["PublicUrl"]);
            


            mailSender.SendSmartHtmlEmail("ges_send_document.html", "send_document");


            //sender.Variables


        }
    }
}
