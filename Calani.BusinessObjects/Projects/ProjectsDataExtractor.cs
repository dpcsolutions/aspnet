﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Projects
{
    public class ProjectsDataExtractor
    {
        public long OrganizationId { get; private set; }

        
        public ProjectsDataExtractor(long organizationId)
        {
            OrganizationId = organizationId;
        }

        public byte[] Extract(bool quotes, bool jobs, bool invoices)
        {
            DocGenerator.ExcelTableGenerator gen = new DocGenerator.ExcelTableGenerator();
            gen.Borders = true;
            gen.FitColumns = true;

            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Columns.Add("Type");
            dt.Columns.Add("Projet");
            dt.Columns.Add("Num");
            dt.Columns.Add("Client");
            dt.Columns.Add("Article");
            dt.Columns.Add("PrixUnit");
            dt.Columns.Add("Qte");
            dt.Columns.Add("PrixHT");
            dt.Columns.Add("Reduc");
            dt.Columns.Add("PrixTTC");

            Model.CalaniEntities db = new Model.CalaniEntities();

            var q = (from r 
                     in db.services_jobs
                     .Include("jobs").Include("services").Include("jobs.clients")
                     where r.jobs.organizationId == OrganizationId
                     orderby r.jobId, r.id
             select r);

            var data = q.ToList();

            foreach (var r in data)
            {
                bool isQuote = (r.jobs.dateOpenJob == null && r.jobs.dateInvoicing == null);
                bool isJobs = (r.jobs.dateOpenJob != null && r.jobs.dateInvoicing == null);
                bool isInvoice = (r.jobs.dateInvoicing != null);

                bool can = (isQuote == true && quotes == true) || (isJobs == true && jobs == true) || (isInvoice == true && invoices == true);

                if(can)
                {
                    var nr = dt.NewRow();
                    if (isQuote) nr["Type"] = "Devis";
                    if (isJobs) nr["Type"] = "Projet";
                    if (isInvoice) nr["Type"] = "Facture";

                    nr["Projet"] = r.jobId.ToString();
                    nr["Num"] = r.jobs.internalId.ToString();
                    nr["Client"] = r.jobs.clients.companyName.ToString();
                    nr["Article"] = r.serviceName;
                    nr["PrixUnit"] = r.servicePrice;
                    nr["Qte"] = r.serviceQuantity;
                    nr["PrixHT"] = r.totalPriceExclTax;
                    nr["Reduc"] = r.discount;
                    nr["PrixTTC"] = r.totalPrinceIncTax;
                    dt.Rows.Add(nr);
                }

                

            }

            gen.Table = dt;
            return gen.GenerateDocument();

        }
    }
}
