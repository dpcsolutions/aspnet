﻿using Calani.BusinessObjects.Model;
using System;

namespace Calani.BusinessObjects.Projects
{
    public class WorkReportModel
    {
        
        public long Id { get; set; }
        public long ProjectId { get; set; }
        
        public String Title { get; set; }
        public String Description { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? ValidatedAt { get; set; }
        public EmployeeDto UpdatedBy { get; set; }
        public EmployeeDto CreatedBy { get; set; }
        public EmployeeDto ValidatedBy { get; set; }
        public String ProjectInternalId { get; set; }
        public WorkReportType Type { get; set; }
        public WorkReportStatus Status { get; set; }
        public long? ClientId { get; set; }
        public WorkReportAction Action { get; set; }
        public string Pdf { get; set; }
        public EmployeeDto Admin { get; set; }
        public long OrgId { get; set; }
        public string CompanyName { get; set; }
        public string FileNameNoExtension { get; set; }
        public byte[] LogoImage { get; set; }
    }

    public enum WorkReportType : int
    {
        Project = 0,
        Reserved = 1
    }

    public enum WorkReportStatus : int
    {
        All = -1,
        Draft = 0,
        Validated = 1
    }

    public enum WorkReportAction : int
    {
        
        Save = 0,
        Validate = 1,
        Delete = 2
    }

    public static class WorkReportModelExtensions
    {
        public static WorkReportModel ToDto(this work_reports entity, string companyName)
        {
            var model = new WorkReportModel();

            if (entity != null)
            {
                model.Id = entity.id;
                model.Title = entity.title;
                model.Description = entity.description;
                model.Type = (WorkReportType)entity.type;
                model.Status = (WorkReportStatus)entity.status;
                model.UpdatedAt = entity.updated_at;
                model.CreatedAt = entity.created_at;
                model.ValidatedAt = entity.validated_at;
                model.UpdatedBy = entity.contacts?.ToDto();
                model.ValidatedBy = entity.contacts1?.ToDto();
                model.CreatedBy = entity.contacts?.ToDto();
                model.ProjectId = entity.jobs?.id??0;
                model.ProjectInternalId = entity.jobs?.internalId;
                model.ClientId = entity.jobs?.clientId;
                model.Pdf = entity.pdf;
                model.OrgId = entity.organization_id;
                model.CompanyName = companyName;
            }
            return model;
        }
    }
}
