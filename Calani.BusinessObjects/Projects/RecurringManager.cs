﻿using System;
using System.Collections.Generic;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using log4net;

namespace Calani.BusinessObjects.Projects
{
    public class RecurringManager : SmartCollectionManager<recurringinfos, long>
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public RecurringManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<recurringinfos> GetDbSet(CalaniEntities db)
        {
            return db.recurringinfos;
        }

        public recurringinfos GetActualForInvoice(long invoiceId)
        {
            return (from r in base.GetBaseQuery()
                .Context.recurringinfos where r.invoiceId == invoiceId orderby r.id descending select r).FirstOrDefault();
        }

        public List<recurringinfos> ListInfos()
        {
            return (from r in base.GetBaseQuery()
                .Context.recurringinfos orderby r.id descending select r).ToList();
        }

        internal override SmartQuery<recurringinfos> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in GetDbSet(q.Context) where r.id == id select r);
            return q;
        }
    }
}