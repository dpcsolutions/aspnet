﻿using System;
using System.Linq;

namespace Calani.BusinessObjects.Projects
{
    public class InvoiceDueCalculator
    {
        Model.jobs _project;
        Model.currency _currency;
        public InvoiceDueCalculator(Model.jobs project, Model.currency currentCurrency)
        {
            _project = project;
            _currency = currentCurrency;
        }

        public InvoiceDueCalculator(Model.jobs project)
        {
            _project = project;
        }

        public bool Recalc(bool vatIncluded = false)
        {
            bool ret = false;

            double currencyRate = 0;
            if (_currency != null)
            {
                currencyRate = _currency.rate;
            }
            else if (_project.currencyRate != null)
            {
                currencyRate = _project.currencyRate.Value;
            }

            _project.total = vatIncluded ? _project.totalWithoutTaxes : _project.total;
            _project.invoiceDue = _project.total;
            if (currencyRate != 0)
            {
                _project.invoiceDue = _project.total * currencyRate;
            }

            var payments = _project.payments.ToList();
            foreach (var payment in payments)
            {
                if (payment.amountPayed != null && payment.datePayed != null)
                {
                    _project.invoiceDue -= payment.amountPayed.Value;
                }
            }

            if (_project.discount != null)
            {
                double discount = 100 - _project.discount ?? 0;
                _project.invoiceDue = _project.invoiceDue * discount / 100;
            }

            double newDew = 0;
            if (_project.invoiceDue != null) newDew = Math.Round(_project.invoiceDue.Value, 2, MidpointRounding.AwayFromZero);
            if (_project.invoiceDue != newDew)
            {
                _project.invoiceDue = newDew;
                ret = true;
            }


            if (currencyRate != 0)
            {
                _project.invoiceDueInCurrency = _project.invoiceDue;
                _project.invoiceDue /= currencyRate;
                _project.invoiceDue = Math.Round(_project.invoiceDue.Value, 2, MidpointRounding.AwayFromZero);
            }


            return ret;
        }
    }
}
