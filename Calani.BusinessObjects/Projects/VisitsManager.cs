﻿using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Notifications;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using log4net;

using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Calani.BusinessObjects.Projects
{
    public class JobsManager : Generic.SmartCollectionManager<visits, long>
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        internal class VisitIDComparer : IEqualityComparer<contacts>
        {
            public bool Equals(contacts x, contacts y)
            {
                if (string.Equals(x.id, y.id))
                {
                    return true;
                }
                return false;
            }

            public int GetHashCode(contacts obj)
            {
                return obj.id.GetHashCode();
            }
        }

        internal class reource : IEqualityComparer<contacts>
        {
            public bool Equals(contacts x, contacts y)
            {
                if (string.Equals(x.id, y.id))
                {
                    return true;
                }
                return false;
            }

            public int GetHashCode(contacts obj)
            {
                return obj.id.GetHashCode();
            }
        }

        public JobsManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<visits> GetDbSet(Model.CalaniEntities db)
        {
            return db.visits;
        }

        internal override SmartQuery<visits> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in GetDbSet(q.Context) where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(visits record)
        {
            if (record != null && record.jobs != null && record.jobs.organizationId != null) return record.jobs.organizationId.Value;
            return -1;
        }

        internal override SmartQuery<visits> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from v in q.Context.visits
                        .Include(a => a.jobs)
                        .Include(c => c.contacts)
                        .Include(cc => cc.jobs.clients)
                        .Include(b => b.jobs.contacts)
                       select v);

            if (OrganizationID > -1) q.Query = (from r in q.Query where r.orgId == OrganizationID || r.orgId == null select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.dateStart descending select r);


            return q;
        }

        private bool isSameResources(String[] fromWeb, Model.visits visit)
        {
            string contactsInDB = "";
            var tmp1 = (from d in visit.contacts orderby d.id select d.id);

            foreach (var d in tmp1)
                contactsInDB += (d.ToString() + ',');

            string contactsFromWeb = "";

            if (fromWeb.Length > 0)
            {
                var tmp2 = (from d in fromWeb orderby d select d);

                foreach (var d in tmp2)
                    contactsFromWeb += (d.ToString() + ',');
            }

            return contactsInDB == contactsFromWeb;
        }

        public bool DeleteGroup(string groupId)
        {
            var now = DateTime.Now;

            using (CalaniEntities db = new CalaniEntities())
            {
                var items = (from v in GetBaseQuery().Context.visits
                             where v.repeatGroupId == groupId &&
                             v.dateStart != null &&
                             (int)v.visitStatus == (int)VisitStatusEnum.Planned
                             select v).ToList();

                foreach (var v in items)
                    Delete(v.id);

                return true;
            }
        }

        public string[] parseResourceString(string resourceStr)
        {
            var res = new String[] { };

            if (!String.IsNullOrEmpty(resourceStr))
            {
                res = resourceStr.Split(',');
                res = res.Take(res.Count() - 1).ToArray();
            }

            return res;
        }

        private visits CreateVisit(long userId, visitToUpdate toCreate, DateTime dtStart, DateTime dtEnd, string recurringId, CalaniEntities db)
        {

            _log.Info($"CreateVisit. userId:{userId}, type:{toCreate.type}, dtStart:{dtStart:O}, dtEnd:{dtEnd:O}");

            var q = new Model.visits();

            q.description = toCreate.description;
            q.dateStart = dtStart;

            q.dateEnd = dtEnd;
            //need to change next line to handle internal activities
            q.type = (int.Parse(toCreate.type) == (int)VisitTypeEnum.PrivateVisit ? (int)VisitTypeEnum.PrivateVisit : int.Parse(toCreate.type));

            q.visitStatus = int.Parse(toCreate.status);
            q.orgId = OrganizationID;
            q.serviceId = (toCreate.internalServiceId != -1 ? toCreate.internalServiceId : (long?)null);

            if (!String.IsNullOrEmpty(toCreate.jobId) && toCreate.jobId != "-1")
            {
                var id = long.Parse(toCreate.jobId);
                var job = (from j in db.jobs where j.id == id select j).FirstOrDefault();

                if (job != null && job.clientId == long.Parse(toCreate.clientId))
                    q.jobId = job.id;
            }
            else
                q.jobId = null;


            if (q.contacts == null)
                q.contacts = new List<contacts>();
            else
                q.contacts.Clear();

            q.contacts = new List<contacts>();

            var res = parseResourceString(toCreate.resources);

            foreach (var t in res)
            {
                var id = long.Parse(t);
                var existContact = (from ct in db.contacts where ct.id == id select ct).FirstOrDefault();

                if (existContact != null)
                    q.contacts.Add(existContact);
            }

            q.numrepeat = toCreate.numrepeat;
            q.repeatdays = toCreate.repeatdays;
            q.repeatGroupId = recurringId;
            q.lastModificationDate = DateTime.UtcNow;
            q.lastModifiedById = userId;
            return q;
        }

        private string GetDateFromVisitString(string strDate, string strTime)
        {
            return !String.IsNullOrEmpty(strDate) ? strDate.Substring(0, strDate.IndexOf('T') + 1) + strTime : "";
        }

        private void UpdateVisit(long userId, visits q, visitToUpdate toCreate, DateTime dtStart, DateTime dtEnd, string recurringId, CalaniEntities db)
        {
            _log.Info($"UpdateVisit. id:{q.id}, userId:{userId}, type:{toCreate.type}, dtStart:{dtStart:O}, dtEnd:{dtEnd:O}");

            q.description = toCreate.description;
            q.dateStart = dtStart;
            q.dateEnd = dtEnd;

            //need to change next line to handle internal activities
            q.type = int.Parse(toCreate.type);
            q.visitStatus = int.Parse(toCreate.status);
            q.orgId = OrganizationID;
            q.serviceId = (toCreate.internalServiceId != -1 ? toCreate.internalServiceId : (long?)null);

            if (!String.IsNullOrEmpty(toCreate.jobId) && toCreate.jobId != "-1")
            {
                var id = long.Parse(toCreate.jobId);
                var job = (from j in db.jobs where j.id == id select j).FirstOrDefault();

                if (job != null && job.clientId == long.Parse(toCreate.clientId))
                    q.jobId = job.id;
            }
            else
                q.jobId = null;

            if (q.contacts == null)
                q.contacts = new List<contacts>();
            else
                q.contacts.Clear();

            q.contacts = new List<contacts>();

            var res = parseResourceString(toCreate.resources);

            foreach (var t in res)
            {
                var id = long.Parse(t);
                var existContact = (from ct in db.contacts where ct.id == id select ct).FirstOrDefault();

                if (existContact != null)
                    q.contacts.Add(existContact);
            }

            q.numrepeat = toCreate.numrepeat;
            q.repeatdays = toCreate.repeatdays;
            q.repeatGroupId = recurringId;
            q.lastModificationDate = DateTime.UtcNow;
            q.lastModifiedById = userId;
        }

        private void CreateRecuringVisits(long userId, visitToUpdate toUpdate, DateTime startDate, DateTime endDate,
            string groupId, CalaniEntities db)
        {
            if (!toUpdate.applyToAllRepeat && toUpdate.id != "-1") return;

            var days = toUpdate.repeatdays.Split(',');

            //day 1 is monday, day 7 in sunday.
            for (var i = 0; i < toUpdate.numrepeat; i++)
            {
                foreach (var d in days)
                {
                    //we need to check what day is the day sent from the web to find the next 
                    //day to consider from the days list

                    if (i > 0 || (i == 0 && (int)startDate.DayOfWeek < int.Parse(d)))
                    {
                        var n = new Model.visits();

                        var monday = (startDate.AddDays(-((int)startDate.DayOfWeek - 1))).AddDays(7 * i);

                        var dtStart = monday.AddDays(int.Parse(d) - 1);
                        dtStart = new DateTime(dtStart.Year, dtStart.Month, dtStart.Day, startDate.Hour, startDate.Minute, 0);

                        var dtEnd = monday.AddDays(endDate.DayOfYear - startDate.DayOfYear);
                        dtEnd = dtEnd.AddDays(int.Parse(d) - 1);
                        dtEnd = new DateTime(dtEnd.Year, dtEnd.Month, dtEnd.Day, endDate.Hour, endDate.Minute, 0);

                        var newVisit = CreateVisit(userId, toUpdate, dtStart, dtEnd, groupId, db);

                        db.visits.Add(newVisit);
                    }
                }
            }
        }

        private void UpdateRecuringVisits(long userId, visitToUpdate toUpdate, DateTime startDate, DateTime endDate, string groupId, CalaniEntities db)
        {
            if (!toUpdate.applyToAllRepeat && toUpdate.id != "-1") return;

            var visits = (from j in db.visits
                          where j.repeatGroupId == groupId &&
                          j.dateStart >= startDate
                          select j
                        ).ToList();

            //day 1 is monday, day 7 in sunday.
            foreach (var j in visits)
            {
                if (j.dateStart != null)
                {
                    var start = (DateTime)j.dateStart;

                    start = new DateTime(start.Year, start.Month, start.Day, startDate.Hour, startDate.Minute, 00);
                    var end = start.Add(endDate - startDate);
                    UpdateVisit(userId, j, toUpdate, start, end, groupId, db);
                }
            }
        }
        public long UpdateOrCreate(long userId, visitToUpdate toUpdate)
        {
            long idRet = -1;

            using (CalaniEntities db = new CalaniEntities())
            {
                Model.visits visit = null;

                if (toUpdate.startTime != null && toUpdate.startTime.Length == 4) 
                    toUpdate.startTime = "0" + toUpdate.startTime;

                if (toUpdate.endTime != null && toUpdate.endTime.Length == 4) 
                    toUpdate.endTime = "0" + toUpdate.endTime;


                DateTime start = DateTime.ParseExact(GetDateFromVisitString(toUpdate.startDate, toUpdate.startTime), "yyyy-MM-ddTHH:mm", CultureInfo.InvariantCulture);
                DateTime end = DateTime.ParseExact(GetDateFromVisitString(toUpdate.endDate ?? toUpdate.startDate, toUpdate.endTime ?? toUpdate.startTime), "yyyy-MM-ddTHH:mm", CultureInfo.InvariantCulture);

                bool visitIsNew = toUpdate.id == "-1";

                string msg = "";
                string title = "";
                string category = "";

                var jobId = long.Parse(toUpdate.jobId);
                var visitId = long.Parse(toUpdate.id);
                var job = db.jobs.Include("clients").Where(w => w.id == jobId).FirstOrDefault();

                var serviceType = toUpdate.internalServiceId != -1 ? db.services.Where(s => s.id == toUpdate.internalServiceId).FirstOrDefault() : null;

                title = (serviceType != null ? "interne" : "client");

                if (visitIsNew)
                {
                    //new visit
                    category = "newAppointment";
                    title = $"Nouveau rendez-vous " + title;

                    if (start.ToShortDateString() == end.ToShortDateString())
                        msg = $"Planifié pour le {start.ToShortDateString()} de {start.ToShortTimeString()} à {end.ToShortTimeString()}";
                    else
                        msg = $"Planifié du {start.ToShortDateString()} à {start.ToShortTimeString()} au {end.ToShortDateString()} à {end.ToShortTimeString()}";

                    string recurringGroupId = toUpdate.numrepeat > 0 ? (Guid.NewGuid()).ToString() : null;
                    
                    visit = CreateVisit(userId, toUpdate, start, end, recurringGroupId, db);

                    db.visits.Add(visit);

                    CreateRecuringVisits(userId, toUpdate, start, end, recurringGroupId, db);
                }
                else
                {
                    //existing visit
                    visit = (from v in GetDbSet(db)
                             where v.id == visitId
                             select v).FirstOrDefault();

                    category = "updateAppointment";
                    title = "Changement de rendez-vous " + title;

                    if ((visit.dateEnd != null && ((DateTime)visit.dateEnd != end)) ||
                        (visit.dateStart != null && ((DateTime)visit.dateStart != start)))
                    {
                        if (start.ToShortDateString() == end.ToShortDateString())
                            msg = $"Le rendez-vous du: {((DateTime)visit.dateStart).ToShortDateString()} de {((DateTime)visit.dateStart).ToShortTimeString()} à {((DateTime)visit.dateEnd).ToShortTimeString()} a été déplacé au " +
                                  ((start).ToShortDateString() == ((DateTime)visit.dateStart).ToShortDateString() ? "même jour ": $"{ (start).ToShortDateString()}") + $" de { (start).ToShortTimeString()} à { (end).ToShortTimeString()}";
                        else
                            msg = $"Le rendez-vous du: {((DateTime)visit.dateStart).ToShortDateString()} de {((DateTime)visit.dateStart).ToShortTimeString()} au {((DateTime)visit.dateEnd).ToShortDateString()} à {((DateTime)visit.dateEnd).ToShortTimeString()} a été déplacé au " +
                                  $"{ (start).ToShortDateString()} de { (start).ToShortTimeString()} au {((DateTime)visit.dateEnd).ToShortDateString()} à { (end).ToShortTimeString()}";
                    }

                    string recurringGroupId = toUpdate.numrepeat > 0 ? (toUpdate.repeatGroupId != "" ? toUpdate.repeatGroupId : (Guid.NewGuid()).ToString()) : null;

                    UpdateVisit(userId, visit, toUpdate, start, end, recurringGroupId, db);

                    if (toUpdate.applyToAllRepeat)
                        UpdateRecuringVisits(userId, toUpdate, start, end, recurringGroupId, db);
                }

                db.SaveChanges();

                //notify user of change
                NotificationManager nm = new NotificationManager();
                if(!String.IsNullOrEmpty(toUpdate.resources))
                {
                    List<long> users = new List<long>();
                  
                    var res = toUpdate.resources.Split(',');
             
                    for (var i = 0; i<res.Length-1; i++)
                        users.Add(long.Parse(res[i]));

                    if (job != null && job.clients.companyName != null)
                        msg += $" pour {job.clients.companyName}";
             
                    if (job != null && job.city != null)
                        msg += $" à {job.city}";
             
                    if (job != null && !String.IsNullOrEmpty(job.description))
                        msg += $". Remarque: {job.description}";
             
                    if (serviceType != null)
                        msg += $". Concerne: {serviceType.name}";
             
                    _log.Debug("Sending notification to:" + toUpdate.resources);

                    nm.sendNotifications(msg, title, users, new Dictionary<string, string>() { { "visitId", toUpdate.id}, { "newStartDate", start.ToShortDateString()}, { "newStartTime",start.ToShortTimeString() }, { "startDate", ((DateTime)visit.dateStart).ToShortDateString()}, { "startTime", ((DateTime)visit.dateStart).ToShortTimeString() }, { "client", (job != null && job.clients.companyName != null ? job.clients.companyName  : "")}, { "internalService", (serviceType != null ? serviceType.name : "" ) } }, category);
                }
                return visit.id;
            }
        }

        public List<visits> GetNextVisits(long? employeeId, DateTime? dtFrom, int count)
        {
            var q = GetBaseQuery();
            if (dtFrom == null) dtFrom = DateTime.Now;

            q.Query = (from r in q.Query
                       where r.dateStart > dtFrom.Value

                       select r);

            if(employeeId != null)
            {
                q.Query = (from r in q.Query
                           where r.contacts.Any(c => c.id == employeeId.Value)
                           orderby r.dateStart
                           select r); 
            }

            return q.Query.Take(count).ToList();
        }

        public List<visitsExtended> ListVisits(DateTime? dtFrom, DateTime? dtTo, List<calendarResource> resources = null, long? jobId = null, long? visitType = null)
        {
            var q = GetBaseQuery();

            if (dtFrom != null)
            {
                q.Query = (from r in q.Query where r.dateStart != null && r.dateStart >= dtFrom.Value select r);
            }

            if (dtTo != null)
            {
                q.Query = (from r in q.Query where r.dateEnd != null && r.dateEnd <= dtTo.Value select r);
            }

            if (jobId != null)
            {
                q.Query = (from r in q.Query where r.jobId == jobId select r);
            }

            if (visitType != null)
                q.Query = (from r in q.Query where r.type == visitType select r);

            var dbrecords = (from r in q.Query select r).ToList();
            var ret = new List<visitsExtended>();

            if (resources != null)
            {
                foreach (var r in resources)
                {
                    var c = new contacts();
                    c.id = long.Parse(r.id);

                    foreach (var v in dbrecords)
                        if (v.contacts.ToList().Contains(c, new VisitIDComparer()))
                            ret.Add(new visitsExtended(v, r.color));
                }

                ret = ret.Distinct(new visitsExtended.visitsExtendedEqualityComparer()).ToList();

                foreach (var r in ret)
                {
                    long? id = long.Parse(r.id);

                    var lstResTime = (from t in GetBaseQuery().Context.timesheetrecords.Include("contacts")
                                      where t.visitId == id && 
                                            t.recordStatus == 0
                                      select t).ToList();

                    foreach(var restmp in r.resourcesObj)
                    {
                        var lstResTimeForContact = (from t in lstResTime
                                                    where t.contacts != null && t.contacts.id == restmp.id
                                                    select t).ToList();
                        var totalMinutes = 0;

                        foreach(var resTime in lstResTimeForContact)
                            totalMinutes += (int)((DateTime)resTime.endDate - (DateTime)resTime.startDate).TotalMinutes;

                        restmp.effectiveTimeInMinutes = totalMinutes;
                        restmp.timesheetid = lstResTimeForContact != null && lstResTimeForContact.Count > 0 ? (long)lstResTimeForContact[0].timesheetId : -1;
                    }
                }
            }
            else
            {
                ret = (from r in dbrecords select new visitsExtended(r)).ToList();
            }

            return ret;
        }

        public List<long> getResourcesForJob(long jobId)
        {
            var q = GetBaseQuery();

            q.Query = (from r in q.Query where r.jobId == jobId select r);

            var dbrecords = (from r in q.Query select r).ToList();
            var ret = new List<long>();
         
            foreach (var r in dbrecords)
            {
                foreach(var c in r.contacts.Where(c => c.status > 0))
                {
                    if (!ret.Contains(c.id))
                    {
                        ret.Add(c.id);
                    }
                }
            }

            return ret;
        }
    }

}
