﻿using System;
using System.Globalization;
using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.Projects.Extensions
{
    public static class RecurringInvoiceExtension
    {
        public static recurringinfos CloneRecurringInfo(this recurringinfos obj)
        {
            return obj == null ? null : new recurringinfos {
                id = obj.id,
                invoiceId = obj.invoiceId,
                isActive = obj.isActive,
                frequencyType = obj.frequencyType,
                daysInAdvance = obj.daysInAdvance,
                frequencyCount = obj.frequencyCount,
                specificDayCount = obj.specificDayCount,
                isLastDay = obj.isLastDay,
                isSpecificDay = obj.isSpecificDay,
                nextDueDate = obj.nextDueDate,
                isUntilNotice = obj.isUntilNotice,
                isSpecificMonth = obj.isSpecificMonth,
                lastDueDate = obj.lastDueDate,
                invoiceSendingType = obj.invoiceSendingType,
                nextExecutionDate = obj.nextExecutionDate,
                specificMonthCount = obj.specificMonthCount
            };
        }
        
        public static recurringinfos CloneRecurringUpdate(this RecurringUpdate obj, long invoiceId)
        {
            return new recurringinfos {
                id = obj.id,
                invoiceId = invoiceId,
                isActive = obj.isActive,
                frequencyType = obj.frequencyType,
                frequencyCount = obj.frequencyCount,
                specificDayCount = obj.specificDayCount,
                daysInAdvance = obj.daysInAdvance,
                isLastDay = obj.isLastDay,
                isSpecificDay = obj.isSpecificDay,
                nextDueDate = DateTime.ParseExact(obj.nextDueDate, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                nextExecutionDate = DateTime.ParseExact(obj.nextExecutionDate, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                isUntilNotice = obj.isUntilNotice,
                isSpecificMonth = obj.isSpecificMonth,
                lastDueDate = DateTime.ParseExact(obj.lastDueDate, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                invoiceSendingType = obj.invoiceSendingType
            };
        }
    }
}