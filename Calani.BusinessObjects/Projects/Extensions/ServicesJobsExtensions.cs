﻿using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.Projects.Extensions
{
    public static class ServicesJobsExtensions
    {
        public static services_jobs Copy(this services_jobs sourcerow)
        {
            var copy = new services_jobs()
            {
                currency = sourcerow.currency,
                discount = sourcerow.discount,
                jobs = sourcerow.jobs,
                serviceId = sourcerow.serviceId,
                serviceName = sourcerow.serviceName,
                servicePrice = sourcerow.servicePrice,
                servicePriceInCurrency = sourcerow.servicePriceInCurrency,
                serviceQuantity = sourcerow.serviceQuantity,
                taxAmount = sourcerow.taxAmount,
                taxName = sourcerow.taxName,
                taxRate = sourcerow.taxRate,
                totalPriceExclTax = sourcerow.totalPriceExclTax,
                totalPriceExclTaxInCurrency = sourcerow.totalPriceExclTaxInCurrency,
                totalPriceIncTaxInCurrency = sourcerow.totalPriceIncTaxInCurrency,
                totalPrinceIncTax = sourcerow.totalPrinceIncTax,
                type = sourcerow.type,
                unitName = sourcerow.unitName,
                custom_number = sourcerow.custom_number,
                position = sourcerow.position,
                toinvoice = sourcerow.toinvoice,
            };

            return copy;
        }
    }
}
