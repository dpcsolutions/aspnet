﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.Settings
{
    public class DocumentTemplateManager : Generic.SmartCollectionManager<documentemplates, long>
    {
        public long UserId { get; set; }
        
        private readonly bool _getByOrgId;
        private readonly System.Globalization.CultureInfo _culture;

        public DocumentTemplateManager(long userId, System.Globalization.CultureInfo culture, bool getByOrgId = false)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            UserId = userId;
            _culture = culture;
            _getByOrgId = getByOrgId;
        }

        internal override DbSet<documentemplates> GetDbSet(CalaniEntities db)
        {
            return db.documentemplates;
        }

        internal override SmartQuery<documentemplates> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.documentemplates where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(documentemplates record)
        {
            if (record != null && record.contacts != null && record.contacts.organizationId != null)
                return record.contacts.organizationId.Value;
            return -1;
        }

        internal override SmartQuery<documentemplates> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            q.Query = from r in q.Context.documentemplates where r.userId == UserId select r;
            if (_getByOrgId) q.Query = from r in q.Query where r.contacts.organizationId == OrganizationID select r;
            if (!IgnoreRecordStatus) q.Query = from r in q.Query where r.recordStatus == 0 select r;

            return q;
        }


        internal override void BeforeAction(SmartAction<documentemplates> action)
        {
        }

        public bool Update(string name, string content, string title)
        {
            var q = GetBaseQuery();
            var record = (from r in q.Query where r.name == name select r).FirstOrDefault();
            if (record == null)
            {
                record = new documentemplates();
                record.recordStatus = 0;
                record.userId = UserId;
                record.name = name;
                q.Context.documentemplates.Add(record);
            }

            record.content = content;
            record.title = title;

            q.Context.SaveChanges();
            return true;
        }

        public DocumentTemplateInfo Get(string name)
        {
            DocumentTemplateInfo pack = new DocumentTemplateInfo();

            var q = GetBaseQuery();
            pack.UserTemplate = (from r in q.Query where r.name == name select r).FirstOrDefault();

            if (SharedResource.Resource != null)
            {
                switch (name)
                {
                    case "Signature":
                        pack.DefaultContent = SharedResource.Resource.GetString("Email_Default_Signature", _culture);
                        pack.Title = SharedResource.Resource.GetString("Email_Title_Signature", _culture);
                        pack.VariableDescriptions = SharedResource.Resource.GetString("Email_Vars_Signature", _culture);
                        break;
                    case "SendQuote":
                        pack.DefaultContent = SharedResource.Resource.GetString("Email_Default_SendQuote", _culture);
                        pack.DefaultTitle = SharedResource.Resource.GetString("Email_Title_SendQuote", _culture);
                        pack.Title = SharedResource.Resource.GetString("Email_Description_SendQuote", _culture);
                        pack.VariableDescriptions = SharedResource.Resource.GetString("Email_Vars_SendQuote", _culture);
                        break;
                    case "SendProject":
                        pack.DefaultContent = SharedResource.Resource.GetString("Email_Default_SendProject", _culture);
                        pack.DefaultTitle = SharedResource.Resource.GetString("Email_Title_SendQuote", _culture);
                        pack.Title = SharedResource.Resource.GetString("Email_Description_SendQuote", _culture);
                        pack.VariableDescriptions = SharedResource.Resource.GetString("Email_Vars_SendQuote", _culture);
                        break;
                    case "SendReminder":
                        pack.DefaultContent = SharedResource.Resource.GetString("Email_Default_SendReminder", _culture);
                        pack.DefaultTitle = SharedResource.Resource.GetString("Email_Title_SendReminder", _culture);
                        pack.Title = SharedResource.Resource.GetString("Email_Description_SendReminder", _culture);
                        pack.VariableDescriptions = SharedResource.Resource.GetString("Email_Vars_SendReminder", _culture);
                        break;
                    case "SendInvoice":
                        pack.DefaultContent = SharedResource.Resource.GetString("Email_Default_SendInvoice", _culture);
                        pack.DefaultTitle = SharedResource.Resource.GetString("Email_Title_SendInvoice", _culture);
                        pack.Title = SharedResource.Resource.GetString("Email_Description_SendInvoice", _culture);
                        pack.VariableDescriptions = SharedResource.Resource.GetString("Email_Vars_SendInvoice", _culture);
                        break;
                    case "PdfQuoteFooter":
                        pack.DefaultContent =
                            SharedResource.Resource.GetString("Email_Default_PdfQuoteFooter", _culture);
                        pack.Title = SharedResource.Resource.GetString("Email_Description_PdfQuoteFooter", _culture);
                        pack.VariableDescriptions = SharedResource.Resource.GetString("Email_Vars_PdfQuoteFooter", _culture);
                        break;
                    case "PdfInvoiceFooter":
                        pack.DefaultContent =
                            SharedResource.Resource.GetString("Email_Default_PdfInvoiceFooter", _culture);
                        pack.Title = SharedResource.Resource.GetString("Email_Description_PdfInvoiceFooter", _culture);
                        pack.VariableDescriptions = SharedResource.Resource.GetString("Email_Vars_PdfInvoiceFooter", _culture);
                        break;
                    case "PdfDeliveryNoteFooter":
                        pack.DefaultContent =
                            SharedResource.Resource.GetString("Email_Default_PdfDeliveryNoteFooter", _culture);
                        pack.Title = SharedResource.Resource.GetString("Email_Description_PdfDeliveryNoteFooter", _culture);
                        pack.VariableDescriptions = SharedResource.Resource.GetString("Email_Vars_PdfDeliveryNoteFooter", _culture);
                        break;
                    case "SendDeliveryNote":
                        pack.DefaultContent = SharedResource.Resource.GetString("Email_Default_SendDeliveryNote", _culture);
                        pack.DefaultTitle = SharedResource.Resource.GetString("Email_Title_SendDeliveryNote", _culture);
                        pack.Title = SharedResource.Resource.GetString("Email_Description_SendDeliveryNote", _culture);
                        pack.VariableDescriptions = SharedResource.Resource.GetString("Email_Vars_SendDeliveryNote", _culture);
                        break;
                }
            }

            return pack;
        }
    }

    public class DocumentTemplateInfo
    {
        public documentemplates UserTemplate { get; set; }
        public string DefaultContent { get; set; }
        public string DefaultTitle { get; set; }

        public string Title { get; set; }
        public string VariableDescriptions { get; set; }
    }
}