﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Calani.BusinessObjects.BankingImport
{
    public class V11Parser
    {
        public List<PaymentBankingRecord> Records { get; private set; }

        public List<string> Errors { get; private set; }

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void ParseText(string text)
        {
            Records = new List<PaymentBankingRecord>();
            Errors = new List<string>();

            text = text.Replace("\r", "\n");
            text = text.Replace("     ", "\n");
            List<string> lines = text.Split('\n').ToList();

            lines = (from r in lines where !String.IsNullOrWhiteSpace(r) select r.Trim()).ToList();

            foreach (string line in lines)
            {
                if (!line.StartsWith("999") && line.Contains("  "))
                {
                    string bvr = line.Substring(12, 27);
                    double amount = Convert.ToDouble(line.Substring(39, 10)) / 100;

                    string id = bvr.Substring(0, bvr.Length - 1);
                    id = id.Substring(id.Length - 10);
                    double d = 0;
                    double.TryParse(id, out d);


                    string strdate = String.Empty;
                    DateTime dt = DateTime.Now;
                    try
                    {
                        strdate = line.Substring(59, 6);
                        DateTime.TryParseExact(strdate, "yyMMdd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeLocal, out dt);
                    }
                    catch(Exception ex)
                    {
                        _log.Error($"ParseRecord. Error. Date:{strdate}", ex);
                    }
                    // -->


                    Records.Add(new PaymentBankingRecord
                    {
                        Amount = amount,
                        Transaction = bvr,
                        MatchingInvoiceId = Convert.ToInt64(d),
                        Date = dt
                    });
                }
            }

        }

        public void ReadFile(string v11File)
        {
            
            try
            {
                string text = System.IO.File.ReadAllText(v11File);
                ParseText(text);
            }
            catch (Exception ex)
            {
                Errors.Add("File Error: " + ex.Message);
            }
        }
    }
}
