﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.BankingImport
{
    public class PaymentBankingRecord
    {
        public string From { get; set; }
        public string Transaction { get; set; }
        public double Amount { get; set; }

        public long MatchingInvoiceId { get; set; }

        public DateTime Date { get; set; }

        public bool Matched { get; set; }
        public double Due { get; set; }
        public string ClientName { get; set; }
        public string InternalId { get; set; }
    }
}
