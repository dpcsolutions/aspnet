﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using log4net;

namespace Calani.BusinessObjects.BankingImport
{
    public class CamtParser
    {
        public List<PaymentBankingRecord> Records { get; private set; }

        public List<string> Errors { get; private set; }
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void ParseXml(string xml)
        {
            Records = new List<PaymentBankingRecord>();
            Errors = new List<string>();
            try
            {
                var xdoc = System.Xml.Linq.XDocument.Parse(xml);
                var txdtls = (from r in xdoc.Descendants() where r.Name.LocalName == "TxDtls" select r).ToList();

                foreach (var txdtl in txdtls)
                {
                    ParseRecord(txdtl);
                }
            }
            catch (Exception ex)
            {
                Errors.Add("File Error: " + ex.Message);
            }
        }

        public void ReadFile(string xmlFile)
        {
            Records = new List<PaymentBankingRecord>();
            Errors = new List<string>();
            try
            {
                var xdoc = System.Xml.Linq.XDocument.Load(xmlFile);
                var txdtls = (from r in xdoc.Descendants() where r.Name.LocalName == "TxDtls" select r).ToList();

                foreach (var txdtl in txdtls)
                {
                    ParseRecord(txdtl);
                }
            }
            catch(Exception ex)
            {
                Errors.Add("File Error: " + ex.Message);
            }
        }

        private void ParseRecord(XElement txdtl)
        {
            try
            {
                var nRmtInf = (from r in txdtl.Descendants() where r.Name.LocalName == "RmtInf" select r).FirstOrDefault();
                nRmtInf = (from r in nRmtInf.Descendants() where r.Name.LocalName == "Ref" select r).FirstOrDefault();


                var nRltdPties = (from r in txdtl.Descendants() where r.Name.LocalName == "RltdPties" select r).FirstOrDefault();
                nRltdPties = (from r in nRltdPties.Descendants() where r.Name.LocalName == "Nm" select r).FirstOrDefault();


                var nAmount = (from r in txdtl.Descendants() where r.Name.LocalName == "Amt" select r).FirstOrDefault();

                var nDt = (from r in txdtl.Descendants() where r.Name.LocalName == "IntrBkSttlmDt" select r).FirstOrDefault();

                string bvrnum = nRmtInf.Value;
                string amount = nAmount.Value;
                string from = nRltdPties.Value;
                string dtstr = "";
                
                if (nDt != null)
                    dtstr = nDt.Value;

                if (!String.IsNullOrWhiteSpace(amount) && !String.IsNullOrWhiteSpace(bvrnum))
                {
                    PaymentBankingRecord rec = new PaymentBankingRecord();
                    rec.Transaction = bvrnum;
                    double d = 0;

                    double.TryParse(amount, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out d);

                    rec.Amount = d;
                    rec.From = from;

                    string id = bvrnum.Substring(0, bvrnum.Length - 1);
                    id = id.Substring(id.Length - 10);

                    double.TryParse(id, out d);
                    rec.MatchingInvoiceId = Convert.ToInt64(d);

                    rec.Date = DateTime.Now;
                    try
                    {
                        //rec.Date = Convert.ToDateTime(dtstr);
                        rec.Date = DateTime.ParseExact(dtstr, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeLocal);
                    }
                    catch(Exception ex)
                    {
                        _log.Error($"ParseRecord. Error. Date:{dtstr}", ex);
                    }

                    Records.Add(rec);
                }
            }
            catch (Exception ex)
            {
                Errors.Add("Record Error: " + ex.Message);
            }
        }

        
    }
}
