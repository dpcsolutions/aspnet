﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.BankingImport
{
    public class PaymentMatching
    {
        
        public List<PaymentBankingRecord> Records { get; set; }

        public PaymentMatching(List<PaymentBankingRecord> records)
        {
            Records = records;
        }

        public void MatchInvoices(long orgId)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();

            foreach (var item in Records)
            {
                long id = item.MatchingInvoiceId;

                var job = (from r in db.jobs where r.id == id && r.organizationId == orgId select r).FirstOrDefault();

                if (job == null)
                {
                    item.MatchingInvoiceId = 0;
                    item.Matched = false;
                }
                else
                {
                    item.Matched = true;

                    if (job.invoiceDue != null) item.Due = job.invoiceDue.Value;
                    if (item.Due == 0 && job.dateFullyPaid == null && job.total != null) item.Due = job.total.Value;

                    try
                    {
                        item.ClientName = job.clients.companyName;
                    }
                    catch { }

                    item.InternalId = job.internalId;
                }
            }
        }
    }
}
