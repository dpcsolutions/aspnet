﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.BankingImport
{
    public class BankingParser
    {
        public List<PaymentBankingRecord> Records { get; private set; }

        public List<string> Errors { get; private set; }

        public void ReadFile(string path)
        {
            string text = System.IO.File.ReadAllText(path);
            Parse(text);
        }

        public void Parse(string text)
        {
            if (text.Trim().Contains("<?xml") || text.Trim().Contains("<"))
            {
                CamtParser parser = new CamtParser();
                parser.ParseXml(text);
                Records = parser.Records;
                Errors = parser.Errors;
            }
            else
            {
                V11Parser parser = new V11Parser();
                parser.ParseText(text);
                Records = parser.Records;
                Errors = parser.Errors;
            }
        }


    }
}
