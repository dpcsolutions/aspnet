﻿using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.MobileService
{
    public class NotesManager : Generic.SmartCollectionManager<notes, long>
    {

        public NotesManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<notes> GetDbSet(Model.CalaniEntities db)
        {
            return db.notes;
        }

        internal override SmartQuery<notes> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in GetDbSet(q.Context) where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(notes record)
        {
            if (record != null && record.jobs != null && record.jobs.organizationId != null) return record.jobs.organizationId.Value;
            return -1;
        }

        internal override SmartQuery<notes> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from v in q.Context.notes
                       select v);

            //  if (OrganizationID > -1) q.Query = (from r in q.Query where r.orgId == OrganizationID || r.orgId == null select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.date descending select r);


            return q;
        }


        public NotesExtended updateOrCreate(NotesExtended toUpdate, System.Globalization.CultureInfo c)
        {
            NotesExtended ret = null;

            using (CalaniEntities db = new CalaniEntities())
            {
                var parsedDate = DateTime.ParseExact(toUpdate.dateWritten, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);
                Model.notes note = null;

                try
                {
                    long id = 0;

                    if(long.TryParse(toUpdate.id, out id))
                        note = (from v in GetDbSet(db)
                                where v.id == id || (
                                    v.lastModifiedById == toUpdate.authorId &&
                                    v.lastModificationDate == parsedDate)
                                select v).FirstOrDefault();
                    else
                        note = (from v in GetDbSet(db)
                                where ( v.mobileTempId == toUpdate.mobileTempId)
                                select v).FirstOrDefault();
                }
                catch (Exception e)
                {
                    note = (from v in GetDbSet(db)
                            where (
                                v.lastModifiedById == toUpdate.authorId &&
                                v.lastModificationDate == parsedDate)
                            select v).FirstOrDefault();

                }

                if (note == null)
                {
                    //new note
                    //find job
                    var v = (from j in db.visits
                             where j.id == toUpdate.jobId
                             select j).FirstOrDefault();

                    if (v != null)
                    {
                        var n = new notes()
                        {
                            author = toUpdate.authorId,
                            date = DateTime.ParseExact(toUpdate.dateWritten, Tools.IsoLongDateFormat,
                            CultureInfo.InvariantCulture),
                            jobId = v.jobId,
                            message = toUpdate.content,
                            mobileTempId = toUpdate.mobileTempId
                        };

                        db.notes.Add(n);

                        db.SaveChanges();

                        note = (from no in GetDbSet(db).Include("contacts")
                                where no.id == n.id 
                                select no).FirstOrDefault();

                        ret = new NotesExtended(note);
                        ret.mobileTempId = toUpdate.mobileTempId;
                    }
                }
                else
                {
                    note.message = toUpdate.content;
                    note.date = DateTime.ParseExact(toUpdate.dateWritten, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);

                    ret = toUpdate;

                    db.SaveChanges();
                }

                return ret;
            }
        }



        public List<NotesExtended> ListNotesForJob(long jobId)
        {
            var q = GetBaseQuery();

            q.Query = (from r in q.Query where r.jobId == jobId select r);

            var dbrecords = (from r in q.Query.Include("contacts") orderby r.date select r).ToList();

            return (from r in dbrecords select new NotesExtended(r)).ToList(); ;
        }
    }
}
