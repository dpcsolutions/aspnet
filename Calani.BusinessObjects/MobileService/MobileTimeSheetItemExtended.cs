﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Generic;

namespace Calani.BusinessObjects.MobileService
{
    public class MobileTimeSheetItemExtended
    {
        public string id { get; set; }
        public long? visitId { get; set; }
        public long? activityRef { get; set; }
        public string activityDescription { get; set; }
        public bool isInternal { get; set; }
        public string reportedStartDateTime { get; set; }
        public string reportedEndDateTime { get; set; }
        public string lastModificationDate { get; set; }
        public string clientName { get; set; }
        public string activityName { get; set; }
        public long lastModifiedBy { get; set; }
        public double taux { get; set; }
        public string closureDate { get; set; }
        public string mobileTempId { get; set; }
        public long? jobId { get; set; }

        public MobileTimeSheetItemExtended()
        {

        }

        public MobileTimeSheetItemExtended(Model.timesheetrecords t, string jDesc, string cName, DateTime? acceptedDate)
        {
            id = t.id.ToString();
            jobId = t.jobId;
            clientName = (cName != null ? cName : "");
            activityName = (t.serviceId != null && t.serviceName  != null ? t.serviceName : (jDesc != null ? jDesc : ""));
            visitId = t.visitId; //neet to check if this is the jobid on the mobile or the visit id
            activityRef = t.serviceId;
            activityDescription = (!String.IsNullOrEmpty(t.description) ? t.description : "");
            isInternal = t.serviceType == 2;
            reportedEndDateTime = (t.endDate != null ? ((DateTime)t.endDate).ToString(Tools.IsoLongDateFormat, CultureInfo.InvariantCulture) : "");
            reportedStartDateTime = (t.startDate != null ? ((DateTime)t.startDate).ToString(Tools.IsoLongDateFormat, CultureInfo.InvariantCulture) : "");
            lastModificationDate = (t.lastModificationDate != null ? ((DateTime)t.lastModificationDate).ToString(Tools.IsoLongDateFormat, CultureInfo.InvariantCulture) : (DateTime.Now).ToString(Tools.IsoLongDateFormat));
            lastModifiedBy = (t.lastModifiedBy != null ? (long)t.lastModifiedBy : 0);
            taux = (t.rate != null ? (double)t.rate : 0);
            closureDate = (acceptedDate != null ? ((DateTime)acceptedDate).ToString(Tools.IsoLongDateFormat, CultureInfo.InvariantCulture) : null);
        }

    }
}
