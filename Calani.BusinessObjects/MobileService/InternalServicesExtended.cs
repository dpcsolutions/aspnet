﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.MobileService
{
    public class InternalServicesExtended
    {
        public string name { get; set; }
        public long id { get; set; }

        public InternalServicesExtended()
        {

        }
        
        public InternalServicesExtended(Model.services s)
        {
            name = s.name;
            id = s.id;
        }
    }
}
