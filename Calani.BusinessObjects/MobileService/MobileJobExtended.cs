﻿using Calani.BusinessObjects.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Calani.BusinessObjects.MobileService
{
    public class MobileJobsLight
    {
        public string clientName;
        public long clientId;
        public string jobNumber;
        public string jobDescription;
        public long jobId;
        public List<MobileServices> services = new List<MobileServices>();
    }
    public class MobileVisit
    {
        public bool isInternal { get; set; }
        public string startDateTime { get; set; }
        public string endDateTime { get; set; }
        //not used 3 next fields
        public string reportedStartDateTime { get; set; }
        public string reportedEndDateTime { get; set; }
        public bool chronoIsOn { get; set; }
    }
    public class MobileServices
    {
        public long id { get; set; }
        public string description { get; set; }
        public bool isInternal { get; set; }
    }
    public class MobileAddress
    {
        public string city { get; set; }
        public string npa { get; set; }
        public string street { get; set; }
        public string num { get; set; }
    }

    public class MobileRessources
    {
        public long id { get; set; }
        public string name { get; set; }
    }

    public class MobileJobExtended
    {
        const string DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
        public long id { get; set; }
        public long clientId {get; set;}
        public long jobId { get; set; }
        public String clientName { get; set; }
        public String jobTitle { get; set; }
        public String mainPhone { get; set; }
        public String mobilePhone { get; set; }
        public String email { get; set; }
        public String jobNumber { get; set; }
        public int jobStatus { get; set; }
        public MobileVisit visit { get; set; }
        public List<NotesExtended> notes { get; set; }
        public List<MobileServices> services { get; set; }
        public long hoursPlanned { get; set; }
        public long effectiveHours { get; set; }
        public MobileAddress address { get; set; }
        public string lastModificationDate { get; set; }
        public long lastModifiedBy { get; set; }
        public string closureDate { get; set; }
        public string contactName { get; set; }
        public string clientDescription { get; set; }
        public string internalVisitDescription { get; set; }
        public string internalVisitName { get; set; }
        public long internalServiceId { get; set; }
        public List<MobileRessources> resources { get; set; }

        public MobileJobExtended()
        {

        }
        public MobileJobExtended(Model.visits r, long pinternalServiceId = -1, string internalName = null, string internalDescription = null)
        {
            id = r.id;
            internalServiceId = pinternalServiceId;
            internalVisitDescription = internalDescription != null ? internalDescription : "";
            internalVisitName = internalName != null ? internalName : "";
            clientDescription = r.description != null ? r.description : "";
            clientName = r.jobId != null && r.jobs.clients != null ? r.jobs.clients.companyName : "";
            clientId = r.jobId != null && r.jobs.clients != null ? r.jobs.clients.id : -1;
            contactName = r.jobId != null && r.jobs.contacts != null ?  (r.jobs.contacts.firstName.Length > 0 ? r.jobs.contacts.firstName.Substring(0, 1) : "") + ". " + (r.jobs.contacts.lastName != null ? r.jobs.contacts.lastName :"") : "";
            jobTitle = r.jobId != null ? r.jobs.description : "";
            mainPhone = r.jobId != null && r.jobs.contacts != null ? r.jobs.contacts.primaryPhone : "";
            mobilePhone = r.jobId != null && r.jobs.contacts != null ? r.jobs.contacts.secondaryPhone : "";
            email = r.jobId != null && r.jobs.contacts != null ? r.jobs.contacts.primaryEmail : "";
            jobNumber = r.jobId != null ? r.jobs.internalId :"" ;
            jobId = r.jobId != null ? r.jobs.id : -1;
            jobStatus = (int)r.visitStatus;
            hoursPlanned = 0;
            effectiveHours = 0;
            lastModificationDate = (r.lastModificationDate == null ? DateTime.Now.ToString(DATE_FORMAT) : ((DateTime)r.lastModificationDate).ToString(DATE_FORMAT));
            lastModifiedBy = (r.lastModifiedById != null ? (long)r.lastModifiedById : 0); // to modify
            closureDate = null; //if we want to implement closing visits
            visit = new MobileVisit()
            {
                isInternal = r.jobId == null && r.serviceId != null,
                startDateTime = ((DateTime)r.dateStart).ToString(DATE_FORMAT),
                endDateTime = ((DateTime)r.dateEnd).ToString(DATE_FORMAT),
                chronoIsOn = false,
                reportedEndDateTime = "",
                reportedStartDateTime = ""
            };

            notes = new List<NotesExtended>();

            if(r.jobs != null)
                foreach(var n in r.jobs.notes)
                {
                    if(n.recordStatus == 0)
                        notes.Add(new NotesExtended(n));
                }

            services = new List<MobileServices>();
            if (r.jobs != null)
                foreach (var s in r.jobs.services_jobs.Where(sj => sj.recordStatus != (int)RecordStatus.Deleted))
                {
                    services.Add(new MobileServices() { description = s.serviceName, id = s.id });
                }

            address = new MobileAddress() { city = r.jobs != null && r.jobs.city  != null ? r.jobs.city : "", npa = r.jobs != null && r.jobs.npa  != null ? r.jobs.npa.ToString() : "", num = r.jobs != null && r.jobs.nb  != null ? r.jobs.nb : "", street = r.jobs != null  && r.jobs.street  != null ? r.jobs.street + " " + (r.jobs.street2 != null ? r.jobs.street2 : "" ) : "" };

            resources = new List<MobileRessources>();
            if(r.contacts != null)
                  
                foreach(var rs in r.contacts)
                {
                    resources.Add(new MobileRessources() { id = rs.id, name = (rs.firstName.Length > 0 ? rs.firstName.Substring(0, 1) + ". " : "") + (rs.lastName  != null ? rs.lastName :"")});
                }
        }
    }
}

