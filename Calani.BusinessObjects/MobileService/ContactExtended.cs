﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.MobileService
{
    public class ContactExtended
    {
        public long id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string initials { get; set; }
        public string language { get; set; }
        public string role { get; set; }
        public string organisation { get; set; }

        public ContactExtended()
        {

        }

        public ContactExtended(Model.contacts c, string orgName)
        {
            organisation = orgName;
            id = c.id;
            firstName = c.firstName;
            lastName = c.lastName;
            email = c.primaryEmail;
            initials = c.initials;
            language = (c.lang != null ? c.lang : "fr");
            role = (c.role != null ? c.role.ToString() : null);
        }
    }
}
