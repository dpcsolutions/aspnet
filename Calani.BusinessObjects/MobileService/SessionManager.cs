﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks; 

namespace Calani.BusinessObjects.MobileService
{
    public class session
    {
        public long userId;
        public long organisationId;
        public string token;
        public int role;
    }

    public class SessionManager
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static List<session> _sessions = new List<BusinessObjects.MobileService.session>();

        private const int _expirationMinutes = 10;

        private const string _alg = "HmacSHA256";
        private const string _salt = "rz8LuOtFBXphj9WQfvFh"; // Generated at https://www.random.org/strings

        public static session getSessionForToken(string token)
        {
            return _sessions.Find(s => s.token == token);
        }

        public static string GenerateToken(string username, string password, string ip, string userAgent, long ticks)
        {
            //  ip = "";
            //  ticks = 0;
            userAgent = "";

            string hash = string.Join(":", new string[] { username, ip, userAgent, ticks.ToString() });
            string hashLeft = "";
            string hashRight = "";

            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                hmac.Key = Encoding.UTF8.GetBytes(GetHashedPassword(password));
                _log.Debug("key " + hmac.Key);
                _log.Debug("message " + hash);
                hmac.ComputeHash(Encoding.UTF8.GetBytes(hash));
                hashLeft = Convert.ToBase64String(hmac.Hash);
                _log.Debug("hashleft " + hashLeft);
                hashRight = string.Join(":", new string[] { username, ticks.ToString() });
                _log.Debug("hashright " + hashRight);
            }
            var str =  Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Join(":", hashLeft, hashRight)));

            _log.Debug("total hash: " + str);
            return str;
        }

        public static string GetHashedPassword(string password)
        {
            string key = string.Join(":", new string[] { password, _salt });
            using (HMAC hmac = HMACSHA256.Create(_alg))
            {
                hmac.Key = Encoding.ASCII.GetBytes(_salt);
                hmac.ComputeHash(Encoding.ASCII.GetBytes(key));
                return Convert.ToBase64String(hmac.Hash);
            }
        }

        static private string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                return Convert.ToBase64String(hashmessage);
            }
        }

        public ContactExtended getUserFromToken(string token)
        {
            string key = Encoding.UTF8.GetString(Convert.FromBase64String(token));
            // Split the parts.
            string[] parts = key.Split(new char[] { ':' });

            _log.Debug("key:" + key);

            if (parts.Length == 3)
            {
                // Get the hash message, username, and timestamp.
                string username = parts[1];
                _log.Debug("username:" + username);

                Calani.BusinessObjects.Membership.LoginManager mgr = new Calani.BusinessObjects.Membership.LoginManager();
                var usr = mgr.GetUser(username, true);
                _log.Debug("usr:" + usr?.lastName);

                var org = mgr.GetOrganisation((long)usr.organizationId);
                _log.Debug("org:" + org?.name);

                return (usr != null ? new ContactExtended(usr, org.name) : null);
            }
            else
                return null;
        }

        public static session IsTokenValid(string token, string ip, string userAgent)
        {
            session result = null;

            try
            {
                // Base64 decode the string, obtaining the token:username:timeStamp.
                string key = Encoding.UTF8.GetString(Convert.FromBase64String(token));
                // Split the parts.
                string[] parts = key.Split(new char[] { ':' });

                _log.Debug("key from token:" + key);
                if (parts.Length == 3)
                {
                    // Get the hash message, username, and timestamp.
                    string hash = parts[0];
                    string username = parts[1];
                    long  ticks = long.Parse(parts[2]);

                    _log.Debug($"hash: ${hash}, user: {username}, ticks: {long.Parse(parts[2])}");

                    DateTime timeStamp = new DateTime(ticks);
                    // Ensure the timestamp is valid.
                    bool expired = Math.Abs((DateTime.UtcNow - timeStamp).TotalMinutes) > _expirationMinutes;
                    _log.Debug("session is " + (expired ? "expired" : "not expired"));

                    var findSession = getSessionForToken(hash+username);
                    _log.Debug("session " + findSession);

                    if (true) //!expired)
                    {
                        if (findSession != null)
                        {
                            result = findSession;
                        }
                        else
                        {
                            Calani.BusinessObjects.Membership.LoginManager mgr = new Calani.BusinessObjects.Membership.LoginManager();
                            var usr = mgr.GetUser(username, true);

                            _log.Debug("user " + usr?.lastName);

                            if (usr != null)
                            {
                                string password = usr.password;
                                //  var r = CreateToken(password, _salt);
                                // Hash the message with the key to generate a token.
                                _log.Debug($"user: {username}, pass: {password}, ip: {ip}, ticks: {ticks}");
                                string computedToken = GenerateToken(username, password, ip, userAgent, ticks);
                                // Compare the computed token with the one supplied and ensure they match.

                                _log.Debug("token " + computedToken);

                                if (token == computedToken)
                                {
                                    _log.Debug("token are similar");
                                    session newSession = new session()
                                    {
                                        token = hash+username,
                                        organisationId = (long)usr.organizationId,
                                        role = (int)usr.type,
                                        userId = usr.id
                                    };

                                    _sessions.Add(newSession);

                                    result = newSession;

                                    // update lastlogin token
                                    mgr.UpdateLastLogin(usr.id);
                                }
                            }
                        }
                        
                    }
                    else
                    {
                        if (findSession != null)
                        {
                            _sessions.Remove(findSession);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                _log.Debug("error in isTokenValid ", e);
            }
            return result;
        }
    }
}
