﻿using Calani.BusinessObjects.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;
using log4net;
using System.Reflection;
using Calani.BusinessObjects.Costs;

namespace Calani.BusinessObjects.MobileService
{
  
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class MobileService : IMobileService
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public string ClientIp()
        {
            return HttpContext.Current.Request.UserHostAddress;
        }
        public string ClientAgent()
        {
            return HttpContext.Current.Request.UserAgent;
        }

        public string getMyIP()
        {
            return ClientIp();
        }

        public MobileActionResult<List<ExpenseExtended>> GetExpensesForUser(string token)
        {
            
            //long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            //JobsManager mgr = new JobsManager(organizationId);
            MobileActionResult<List<ExpenseExtended>> ret = new MobileActionResult<List<ExpenseExtended>>();

            session s = SessionManager.IsTokenValid(token, ClientIp(), ClientAgent());

            if (s == null)
                return ret;
            else
                ret.notAllowed = false;

            ExpenseManager mgr = new ExpenseManager(s.organisationId);

            if (SessionManager.IsTokenValid(token, ClientIp(), ClientAgent()) == null)
                ret = new MobileActionResult<List<ExpenseExtended>>() { success = false};

            try
            {
                ret.data = mgr.ListExpenses(s.userId);//get user id from session
                ret.success = true;
            }
            catch (Exception e)
            {
                ret.success = false;
            }

            ret.rejected = ret.data != null;
            return ret;
        }

        public  MobileActionResult<List<MobileTimeSheetItemExtended>> GetTimesheetsForUser(string token)
       {
            //long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            //JobsManager mgr = new JobsManager(organizationId);
            MobileActionResult<List<MobileTimeSheetItemExtended>> ret = new MobileActionResult<List<MobileTimeSheetItemExtended>>();

            session s = SessionManager.IsTokenValid(token, ClientIp(), ClientAgent());

            if (s == null)
                return ret;
            else
                ret.notAllowed = false;

            MobileTimeSheetManager mgr = new MobileTimeSheetManager(s.organisationId);
         
            try
            {
                ret.data = mgr.ListTimesheets(s.userId);//get user id from session
                ret.success = true;
            }
            catch (Exception e)
            {
                ret.success = false;
            }

            ret.rejected = ret.data != null;
            return ret;
        }

        public MobileActionResult<List<MobileJobExtended>> GetVisitsForUser(string token)
        {
            //long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            //JobsManager mgr = new JobsManager(organizationId);
            MobileActionResult<List<MobileJobExtended>> ret = new MobileActionResult<List<MobileJobExtended>>();

            session s = SessionManager.IsTokenValid(token, ClientIp(), ClientAgent());

            if (s == null)
                return ret;
            else
                ret.notAllowed = false;

            MobileJobManager mgr = new MobileJobManager(s.organisationId);

            try
            {
                ret.data = mgr.ListVisits(s.userId);
                ret.success = true;
            }
            catch (Exception)
            {
                ret.success = false;
            }

            ret.rejected = ret.data != null;
            return ret;
        }

        public MobileActionResult<ExpenseExtended> AddOrUpdateExpense(string token, ExpenseExtended expense)
        {
            MobileActionResult<ExpenseExtended> ret = new MobileActionResult<ExpenseExtended>();

            session s = SessionManager.IsTokenValid(token, ClientIp(), ClientAgent());

            if (s == null)
                return ret;
            else
                ret.notAllowed = false;

            ExpenseManager mgr = new ExpenseManager(s.organisationId);

            try
            {
                ret.data = mgr.SaveOrUpdate(expense, s.userId);
                ret.success = true;
            }
            catch (Exception e)
            {
                ret.success = false;
            }

            ret.rejected = ret.data != null;
            return ret;
        }

        
        public MobileActionResult<ExpenseExtended> deleteExpense(string token, ExpenseExtended expense)
        {
            MobileActionResult<ExpenseExtended> ret = new MobileActionResult<ExpenseExtended>();

            session s = SessionManager.IsTokenValid(token, ClientIp(), ClientAgent());

            if (s == null)
                return ret;
            else
                ret.notAllowed = false;

            ExpenseManager mgr = new ExpenseManager(s.organisationId);

            try
            {
                var res = mgr.Delete(long.Parse(expense.id));
                ret.data = expense;
                ret.success = res.Success;
            }
            catch (Exception e)
            {
                ret.success = false;
            }

            ret.rejected = ret.data != null;
            return ret;
        }

        public MobileActionResult<MobileTimeSheetItemExtended> deleteTimesheet(string token, MobileTimeSheetItemExtended timesheet)
        {
            MobileActionResult<MobileTimeSheetItemExtended> ret = new MobileActionResult<MobileTimeSheetItemExtended>();

            session s = SessionManager.IsTokenValid(token, ClientIp(), ClientAgent());

            if (s == null)
                return ret;
            else
                ret.notAllowed = false;

            MobileTimeSheetManager mgr = new MobileTimeSheetManager(s.organisationId);

            try
            {
                var res = mgr.delete(long.Parse(timesheet.id));
                ret.data = timesheet;
                ret.success = res;
            }
            catch (Exception e)
            {
                ret.success = false;
            }

            ret.rejected = ret.data != null;
            return ret;
        }

        public MobileActionResult<NotesExtended> deleteNNote(string token, NotesExtended note)
        {
            MobileActionResult<NotesExtended> ret = new MobileActionResult<NotesExtended>();

            session s = SessionManager.IsTokenValid(token, ClientIp(), ClientAgent());

            if (s == null)
                return ret;
            else
                ret.notAllowed = false;

            NotesManager mgr = new NotesManager(s.organisationId);

            try
            {
                long id = 0;
                //if the id is not a number then it is a temp id from the mobile
                //that has not been added yet we can ignore the delete
                if (long.TryParse(note.id, out id))
                {
                    var res = mgr.Delete(long.Parse(note.id));
                    ret.success = res.Success;
                }
                else
                    ret.success = true;

                ret.data = note;
            }
            catch (Exception e)
            {
                ret.success = false;
            }

            ret.rejected = ret.data != null;
            return ret;
        }

        //post note
        public MobileActionResult<NotesExtended> AddOrUpdateNote(string token, NotesExtended note)
        {
            MobileActionResult<NotesExtended> ret = new MobileActionResult<NotesExtended>();

            session s = SessionManager.IsTokenValid(token, ClientIp(), ClientAgent());

            if (s == null)
                return ret;
            else
                ret.notAllowed = false;

            NotesManager mgr = new NotesManager(s.organisationId);

            try
            {
                ret.data = mgr.updateOrCreate(note, null);
                ret.success = true;
            }
            catch (Exception e)
            {
                ret.success = false;
            }
            ret.rejected = ret.data != null;
            return ret;
        }

        public MobileActionResult<MobileJobExtended> UpdateVisit(string token, MobileJobExtended job)
        {
            MobileActionResult<MobileJobExtended> ret = new MobileActionResult<MobileJobExtended>();

            session s = SessionManager.IsTokenValid(token, ClientIp(), ClientAgent());

            if (s == null)
                return ret;
            else
                ret.notAllowed = false;

            MobileJobManager mgr = new MobileJobManager(s.organisationId);

            if (SessionManager.IsTokenValid(token, ClientIp(), ClientAgent()) == null) return ret;

            try
            {
                ret.data = mgr.updateVisit(job, s.userId);
                ret.success = true;
            }
            catch (Exception)
            {
                ret.success = false;
            }

            ret.rejected = ret.data != null;
            return ret;
        }

        public MobileActionResult<MobileTimeSheetItemExtended> AddOrUpdateTimesheet(string token, MobileTimeSheetItemExtended timesheetrecord)
        {
            MobileActionResult<MobileTimeSheetItemExtended> ret = new MobileActionResult<MobileTimeSheetItemExtended>();

            session s = SessionManager.IsTokenValid(token, ClientIp(), ClientAgent());

            if (s == null)
                return ret;
            else
                ret.notAllowed = false;

            MobileTimeSheetManager mgr = new MobileTimeSheetManager(s.organisationId);

            if (SessionManager.IsTokenValid(token, ClientIp(), ClientAgent()) == null) return ret;

            try
            {
                ret.data = mgr.updateOrCreate(timesheetrecord, null, s.userId);
                ret.success = true;
            }
            catch (Exception e)
            {
                ret.success = false;
            }

            ret.rejected = ret.data != null;
            return ret;
        }

       public MobileActionResult<List<InternalServicesExtended>> getInternalActivities(string token)
        {
            MobileActionResult<List<InternalServicesExtended>> ret = new MobileActionResult<List<InternalServicesExtended>>();

            session s = SessionManager.IsTokenValid(token, ClientIp(), ClientAgent());

            if (s == null)
                return ret;
            else
                ret.notAllowed = false;

            Calani.BusinessObjects.CustomerAdmin.InternalJobsManager mgr = new Calani.BusinessObjects.CustomerAdmin.InternalJobsManager(s.organisationId);

            try
            {
                ret.data = (from d in mgr.List() select new InternalServicesExtended(d)).ToList();
                ret.success = true;
            }
            catch (Exception e)
            {
                ret.success = false;
            }

            ret.rejected = ret.data != null;
            return ret;
        }


        public MobileActionResult<List<MobileServices>> getServices(string token)
        {
            MobileActionResult<List<MobileServices>> ret = new MobileActionResult<List<MobileServices>>();

            session s = SessionManager.IsTokenValid(token, ClientIp(), ClientAgent());

            if (s == null)
                return ret;
            else
                ret.notAllowed = false;

            Calani.BusinessObjects.CustomerAdmin.ServicesManager mgr = new Calani.BusinessObjects.CustomerAdmin.ServicesManager(s.organisationId);
            Calani.BusinessObjects.CustomerAdmin.InternalJobsManager mgr2 = new Calani.BusinessObjects.CustomerAdmin.InternalJobsManager(s.organisationId);

            try
            {
                ret.data = (from d in mgr.List()
                            select new MobileServices() { id = d.id, description = d.name, isInternal = d.type == 2 }).ToList();
                ret.data.AddRange((from d in mgr2.List()
                            select new MobileServices() { id = d.id, description = d.name, isInternal = d.type == 2 }).ToList());
                ret.success = true;
            }
            catch (Exception e)
            {
                ret.success = false;
            }

            ret.rejected = ret.data != null;
            return ret;
        }

        public MobileActionResult<List<MobileJobsLight>> getJobsLight(string token)
        {
            MobileActionResult<List<MobileJobsLight>> ret = new MobileActionResult<List<MobileJobsLight>>();

            session s = SessionManager.IsTokenValid(token, ClientIp(), ClientAgent());

            if (s == null)
                return ret;
            else
                ret.notAllowed = false;

            MobileJobManager mgr = new MobileJobManager(s.organisationId);

            try
            {
                ret.data = mgr.listJobsLight();
                ret.success = true;
            }
            catch (Exception e)
            {
                ret.success = false;
            }

            ret.rejected = ret.data != null;
            return ret;
        }

        public MobileActionResult<ContactExtended> Login(string token, string notificationToken = null)
        {
            _log.Debug("token:" + token);
            _log.Debug("notification: " + notificationToken);

            SessionManager s = new SessionManager();
            var usr = s.getUserFromToken(token);

            if (SessionManager.IsTokenValid(token, ClientIp(), ClientAgent()) != null)
            {
                //store notification token if present
                if(notificationToken != null)
                {
                    (new NotificationManager()).storeNotification(notificationToken, usr.id);
                }

                Calani.BusinessObjects.Membership.LoginManager lmgr = new Membership.LoginManager();
                lmgr.LogLogin(usr.id, "Mobile");

                var r = new MobileActionResult<ContactExtended> { success = (usr != null), data = usr, notAllowed = false };
                return r;
            }
            else
                return new MobileActionResult<ContactExtended> { success = false };
        }
    }

    [ServiceContract(SessionMode = SessionMode.Allowed)]
    public interface IMobileService
    {


        [OperationContract]
        [WebGet(UriTemplate = "service/{token}", ResponseFormat = WebMessageFormat.Json)]
        MobileActionResult<List<MobileServices>> getServices(string token);

        [OperationContract]
        [WebGet(UriTemplate= "visit/{token}", ResponseFormat = WebMessageFormat.Json)]
        MobileActionResult<List<MobileJobExtended>> GetVisitsForUser(string token);

        [OperationContract]
        [WebGet(UriTemplate = "projectsClients/{token}", ResponseFormat = WebMessageFormat.Json)]
        MobileActionResult<List<MobileJobsLight>> getJobsLight(string token);
        
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "visit/{token}", ResponseFormat = WebMessageFormat.Json)]
        MobileActionResult<MobileJobExtended> UpdateVisit(string token, MobileJobExtended node);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "note/{token}", ResponseFormat = WebMessageFormat.Json)]
        MobileActionResult<NotesExtended> AddOrUpdateNote(string token, NotesExtended node);

        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "note/{token}", ResponseFormat = WebMessageFormat.Json)]
        MobileActionResult<NotesExtended> deleteNNote(string token, NotesExtended node);

        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "expense/{token}", ResponseFormat = WebMessageFormat.Json)]
        MobileActionResult<ExpenseExtended> deleteExpense(string token, ExpenseExtended node);

        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "timesheet/{token}", ResponseFormat = WebMessageFormat.Json)]
        MobileActionResult<MobileTimeSheetItemExtended> deleteTimesheet(string token, MobileTimeSheetItemExtended timesheet);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "expense/{token}", ResponseFormat = WebMessageFormat.Json)]
        MobileActionResult<ExpenseExtended> AddOrUpdateExpense(string token, ExpenseExtended expense);

        [OperationContract]
        [WebGet(UriTemplate = "timesheet/{token}", ResponseFormat = WebMessageFormat.Json)]
        MobileActionResult<List<MobileTimeSheetItemExtended>> GetTimesheetsForUser(string token);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "timesheet/{token}", ResponseFormat = WebMessageFormat.Json)]
        MobileActionResult<MobileTimeSheetItemExtended> AddOrUpdateTimesheet(string token, MobileTimeSheetItemExtended timesheetrecord);
        
        [OperationContract]
        [WebGet(UriTemplate = "expense/{token}", ResponseFormat = WebMessageFormat.Json)]
        MobileActionResult<List<ExpenseExtended>> GetExpensesForUser(string token);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "login/{token}", ResponseFormat = WebMessageFormat.Json)]
        MobileActionResult<ContactExtended> Login(string token, string notificationToken = null);

        [OperationContract]
        [WebGet(UriTemplate = "ip/", ResponseFormat = WebMessageFormat.Json)]
        string getMyIP();
    }
}
