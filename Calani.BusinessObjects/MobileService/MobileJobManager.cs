﻿using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;

namespace Calani.BusinessObjects.MobileService
{
    public class MobileJobManager : Generic.SmartCollectionManager<visits, long>
    {

        public MobileJobManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<visits> GetDbSet(Model.CalaniEntities db)
        {
            return db.visits;
        }

        internal override SmartQuery<visits> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in GetDbSet(q.Context) where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(visits record)
        {
            if (record != null && record.jobs != null && record.jobs.organizationId != null) return record.jobs.organizationId.Value;
            return -1;
        }

        internal override SmartQuery<visits> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from v in q.Context.visits
                        .Include(a => a.jobs)
                        .Include(c => c.contacts)
                        .Include(cc => cc.jobs.clients)
                        .Include(b => b.jobs.contacts)
                       select v);

            if (OrganizationID > -1) q.Query = (from r in q.Query where r.orgId == OrganizationID || r.orgId == null select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.dateStart descending select r);


            return q;
        }

        public void SyncContacts(long id, List<MobileRessources> contacts) {
            var db = new Model.CalaniEntities();
            contacts.ForEach(c =>
            {
                var visit = (from r in db.visits.Include(ct=>ct.contacts) where r.id == id && r.orgId == this.OrganizationID select r).FirstOrDefault();
                if (visit != null)
                {
                    var existingContacts = visit.contacts.ToList();
                    contacts.ForEach(t =>
                    {
                        if (!existingContacts.Exists(tmp => tmp.id == t.id))
                        {
                            var contact = (from ct in db.contacts where ct.id == t.id select ct).FirstOrDefault();
                            existingContacts.Add(contact);
                        }
                    });

                    var missingContacts = contacts.ToList();
                    existingContacts.ForEach(t =>
                    {
                        if (!contacts.Exists(tmp => tmp.id == t.id))
                        {
                            existingContacts.Remove(t);
                        }
                    });

                    db.SaveChanges();
                }
            });
        }

        public void RemoveContact(long id, long userId) {
            var db = new Model.CalaniEntities();
            var contact = (from r in db.contacts where r.id == userId && r.organizationId == this.OrganizationID select r).FirstOrDefault();

            if (contact != null)
            {
                var visit = (from r in db.visits where r.id == id && r.orgId == this.OrganizationID select r).FirstOrDefault();
                if (visit != null)
                {
                    if ((from r in visit.contacts where r.id == userId select r).Count() != 0)
                    {
                        visit.contacts.Remove(contact);
                        db.SaveChanges();
                    }
                }
            }
        }

        public void AddContact(long id, long userId)
        {
            var db = new Model.CalaniEntities();
            var contact = (from r in db.contacts where r.id == userId && r.organizationId == this.OrganizationID select r).FirstOrDefault();

            if(contact != null)
            {
                var visit = (from r in db.visits where r.id == id && r.orgId == this.OrganizationID select r).FirstOrDefault();
                if(visit != null)
                {
                    if((from r in visit.contacts where r.id == userId select r).Count() == 0)
                    {
                        visit.contacts.Add(contact);
                        db.SaveChanges();
                    }
                }
            }
        }

        public MobileJobExtended updateVisit(MobileJobExtended visit, long employeeId)
        {
            MobileJobExtended ret = null;

            var q = base.GetBaseQuery().Context;
            visits existingVisit = null;

            using (q)
            {
                existingVisit = (from j in q.visits
                                   where j.id == visit.id && 
                                        j.orgId == OrganizationID &&
                                        j.recordStatus == 0
                                 select j).FirstOrDefault();

                if(existingVisit != null)
                {
                    if(existingVisit.lastModificationDate == null || existingVisit.lastModificationDate <= DateTime.ParseExact(visit.lastModificationDate,Tools.IsoLongDateFormat, CultureInfo.InvariantCulture))
                    {
                        //mobile has last version let's updae the date/time
                        existingVisit.dateStart = DateTime.ParseExact(visit.visit.startDateTime, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);
                        existingVisit.dateEnd = DateTime.ParseExact(visit.visit.endDateTime, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);
                        existingVisit.lastModificationDate = DateTime.ParseExact(visit.lastModificationDate,Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);
                        existingVisit.visitStatus = visit.jobStatus;
                        existingVisit.lastModifiedById = employeeId;

                        ret = visit;

                        q.SaveChanges();
                    }
                    else
                        ret = new MobileJobExtended(existingVisit);
                }
                else
                {
                    throw new Exception("mobile job record " + visit.id + " not found in db");
                }
            }
            return ret;
        }

        public List<MobileJobsLight> listJobsLight()
        {
            var ret = new List<MobileJobsLight>();

            var context = base.GetBaseQuery().Context;
            var dtMax = (DateTime.Now).AddMonths(-12);

            var q = (from s in context.jobs.Include("clients")
                     where (s.dateJobDone == null || s.dateJobDone >= dtMax) && 
                        s.dateOpenJob != null && s.dateInvoicing == null
                     select s);

            q = (from r in q
                 where r.recordStatus == 0 &&
                 r.organizationId == OrganizationID
                 select r);

            var records = q.ToList();

            if (records != null)
            {
                foreach (var v in records)
                {
                    List<Calani.BusinessObjects.MobileService.MobileServices> lstServices = new List<Calani.BusinessObjects.MobileService.MobileServices>();
                    if (v.services_jobs != null)
                        foreach (var s in v.services_jobs.Where(sj => sj.recordStatus != (int)RecordStatus.Deleted))
                        {
                            lstServices.Add(new MobileServices() { description = s.serviceName, id = (long)s.serviceId, isInternal = s.type == 2 });
                        }

                    var job = new MobileJobsLight() {services = lstServices, clientId = v.clientId, clientName = v.clients.companyName, jobId = v.id, jobDescription = v.description, jobNumber = v.internalId};
                    ret.Add(job);
                }
            }

            return ret;
        }

        public List<MobileServices> ListServices()
        {
            var ret = new List<MobileServices>();

            var context = base.GetBaseQuery().Context;
            var q = (from s in context.services
                     where s.recordStatus == 0
                     select s);

            q = (from r in q
                where r.recordStatus == 0 &&
                 r.organizationId == OrganizationID &&
                 r.recordStatus == 0
                 select r);

            var records = q.ToList();

            if (records != null)
            {
                foreach (var v in records)
                {
                    ret.Add(new MobileServices() { id = v.id, description = v.name, isInternal = v.type == 2 });
                }
            }

            return ret;
        }

        public List<MobileJobExtended> ListVisits(long userId, long? clientId = null)
        {
            var ret = new List<MobileJobExtended>();
            base.IgnoreRecordStatus = false;

            var context = base.GetBaseQuery().Context;
            IQueryable<contacts> q = null;
                
            
            q = (from r in context.contacts.Include(a => a.visits)
                    where r.id == userId && 
                    r.recordStatus == 0
                    select r);

            if (clientId != null) q = (from r in q where r.clientId == clientId.Value select r);

            if (!IgnoreRecordStatus) q = (from r in q where r.recordStatus == 0 &&
                                          r.organizationId == OrganizationID
                                          select r);

            var records = q.FirstOrDefault();

            if (records != null)
            {
                foreach (var v in records.visits.Where(v=>v.recordStatus == 0))
                {
                    /* var servicesForJob = (from j in context.jobs.Include(a=>a.services_jobs)
                                            where j.id == v.jobId
                                            select j.services_jobs).FirstOrDefault();


                      v.services = servicesForJob;*/
                    services des = null;

                    if(v.serviceId != null)
                    {
                        des = (from s in context.services.Where(s=>s.recordStatus == 0)
                                    where s.id == v.serviceId
                                    select s).FirstOrDefault();

                        var j = new MobileJobExtended(v, (des != null ? des.id : -1), (des != null ? des.name : null), (des != null ? des.description : null));
                        ret.Add(j);
                    }
                    else
                        ret.Add(new MobileJobExtended(v));
                }
            }

            return ret;
        }
    }
}
