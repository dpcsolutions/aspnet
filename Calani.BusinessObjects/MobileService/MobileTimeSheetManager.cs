﻿using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.TimeManagement;

namespace Calani.BusinessObjects.MobileService
{

    public class MobileTimeSheetManager : Generic.SmartCollectionManager<timesheets, long>
    {
        public MobileTimeSheetManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<timesheets> GetDbSet(Model.CalaniEntities db)
        {
            return db.timesheets;
        }

        internal override SmartQuery<timesheets> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in GetDbSet(q.Context) where r.id == id select r);
            return q;
        }

        public bool delete (long id)
        {
            var res = false;

            using (Model.CalaniEntities db = new CalaniEntities())
            {
                var tsi = (from t in db.timesheetrecords
                          where t.id == id
                          select t).FirstOrDefault();

                if(tsi != null)
                {
                    var ts = (from t in db.timesheets
                              where t.id == tsi.timesheetId
                              select t).FirstOrDefault();

                    if(ts != null)
                    {
                        ts.timesheetrecords.Remove(tsi);
                    }

                    db.timesheetrecords.Remove(tsi);

                    db.SaveChanges();

                    res = true;
                }
            }

            return res;
        } 

        internal override SmartQuery<timesheets> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from v in q.Context.timesheets
                       select v);

            if (!IgnoreRecordStatus) 
                q.Query = (from r in q.Query where r.recordStatus == 0 select r);
            
            return q;
        }

       

        public MobileTimeSheetItemExtended updateOrCreate(MobileTimeSheetItemExtended toUpdate, System.Globalization.CultureInfo c, long employeeId)
        {
            MobileTimeSheetItemExtended ret = null;

            using (CalaniEntities db = new CalaniEntities())
            {
                //get timesheet for record
                DateTime dt = DateTime.ParseExact(toUpdate.reportedStartDateTime, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);

                var year = dt.Year;
                var week = Tools.GetWeekNumber(dt);

                timesheets ts = (from t in db.timesheets.Include(a=>a.timesheetrecords)
                                 where 
                                    
                                    t.employeeId == employeeId && //get from session
                                    t.organizationId == OrganizationID && //from session
                                    t.week == week &&
                                    t.year == year

                                 select t
                                 ).FirstOrDefault();

                if(ts == null)
                {
                    //need to create the ts record first
                    ts = new timesheets() {
                        organizationId = OrganizationID, //change from session
                        year = year,
                        week = week,
                        employeeId = employeeId, //from session
                        sentDate = new DateTime(),
                        timesheetrecords = new List<timesheetrecords>()
                    };

                    db.timesheets.Add(ts);
                    db.SaveChanges();
                }

                //try to find the timesheet record
                if(ts.acceptedDate == null)
                {
                    var parsedDate = DateTime.ParseExact(toUpdate.lastModificationDate, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);

                    timesheetrecords tsr = null;
                    var isNew = false;

                    try
                    {
                        long id = 0;
                        if(long.TryParse(toUpdate.id, out id))
                            tsr = (from t in ts.timesheetrecords
                                   where t.id == id ||
                                       (t.lastModifiedBy == employeeId &&
                                           t.lastModificationDate == parsedDate)
                                   select t).FirstOrDefault();
                        else
                        {
                            tsr = (from t in ts.timesheetrecords
                                   where (t.lastModifiedBy == employeeId &&
                                           t.lastModificationDate == parsedDate)
                                   select t).FirstOrDefault();
                            isNew = true;
                        }
                    }
                    catch (Exception)
                    {
                       //need to log this 
                    }

                    //checking for the date will prevent a dupplicte entry in case the mobile sends the record with -1 a second time

                    if (tsr == null)
                    {
                        //new timeshet record
                        var n = new timesheetrecords()
                        {
                            description = toUpdate.activityDescription,
                            endDate = DateTime.ParseExact(toUpdate.reportedEndDateTime, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture),
                            lastModificationDate = DateTime.ParseExact(toUpdate.lastModificationDate, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture),
                            lastModifiedBy = employeeId, //userId from sesseion
                            rate = toUpdate.taux,
                            startDate = DateTime.ParseExact(toUpdate.reportedStartDateTime, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture),
                            visitId = toUpdate.visitId,
                            serviceId = toUpdate.activityRef,
                            jobId = toUpdate.jobId,
                        };

                        if (n.endDate != null && n.startDate != null)
                        {
                            TimeSpan t = ((DateTime)n.endDate - (DateTime)n.startDate);
                            TimeSpan h1 = TimeSpan.Parse("1:00:00");

                            n.duration = (double)t.Ticks / (double)h1.Ticks;
                        }
                        else
                            n.duration = 0;

                        if (toUpdate.activityRef != null)
                        {
                            var service = (from s in db.services
                                           where s.id == toUpdate.activityRef
                                           select s).FirstOrDefault();

                            if(service != null)
                            {
                                n.serviceId = service.id;
                                n.serviceName = service.name;
                                n.servicePrice = service.unitPrice;
                                n.serviceType = service.type;
                            }
                        }

                        ts.timesheetrecords.Add(n);

                        db.SaveChanges();

                        ret = new MobileTimeSheetItemExtended(n, toUpdate.activityName, toUpdate.clientName, null);

                        ret.mobileTempId = toUpdate.mobileTempId;
                    }
                    else
                    {
                        if (tsr.lastModificationDate <= DateTime.ParseExact(toUpdate.lastModificationDate, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture))
                        {
                            tsr.description = toUpdate.activityDescription;
                            tsr.endDate = DateTime.ParseExact(toUpdate.reportedEndDateTime, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);
                            tsr.lastModificationDate = DateTime.ParseExact(toUpdate.lastModificationDate, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);
                            tsr.lastModifiedBy = employeeId;
                            tsr.rate = toUpdate.taux;
                            tsr.startDate = DateTime.ParseExact(toUpdate.reportedStartDateTime, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);
                            tsr.visitId = toUpdate.visitId;
                            tsr.serviceId = toUpdate.activityRef;
                            tsr.jobId = toUpdate.jobId;

                            if (tsr.endDate != null && tsr.startDate != null)
                            {
                                TimeSpan t = ((DateTime)tsr.endDate - (DateTime)tsr.startDate);
                                TimeSpan h1 = TimeSpan.Parse("1:00:00");

                                tsr.duration = (double)t.Ticks / (double)h1.Ticks;
                            }
                            else
                                tsr.duration = 0;

                            if (toUpdate.activityRef != null)
                            {
                                var service = (from s in db.services
                                                where s.id == toUpdate.activityRef
                                                select s).FirstOrDefault();

                                if (service != null)
                                {
                                    tsr.serviceId = service.id;
                                    tsr.serviceName = service.name;
                                    tsr.servicePrice = service.unitPrice;
                                    tsr.serviceType = service.type;
                                }
                            }

                            ret = new MobileTimeSheetItemExtended(tsr, toUpdate.activityName, toUpdate.clientName, null);
                        }
                        else
                        {
                            ret = new MobileTimeSheetItemExtended(tsr, toUpdate.activityName, toUpdate.clientName, null);
                        }

                        if (isNew)
                            ret.mobileTempId = toUpdate.mobileTempId;
                  
                        db.SaveChanges();
                        
                        TimeDuplicateManager.CheckAndDeleteTimesheetRecordsDuplicates(long.Parse(ret.mobileTempId), OrganizationID);
                    }
                }
               
                return ret;
            }
        }

        
        public List<MobileTimeSheetItemExtended> ListTimesheets(long employeeId)
        {
            List<MobileTimeSheetItemExtended> ret = new List<MobileTimeSheetItemExtended>();

            var q = GetBaseQuery();
            DateTime startDate = DateTime.Now.AddMonths(-5);
            DateTime endDate = DateTime.Now.AddMonths(+2);
            int weekNumStart = 1; //GetWeekNumber(startDate);
            int weekNumEnd = 60;// GetWeekNumber(endDate);
            q.Query = (from r in q.Query.Include("timesheetrecords")
                       where r.organizationId == OrganizationID &&
                       r.employeeId == employeeId &&
                       r.year >= startDate.Year &&
                       r.year <= endDate.Year &&
                       r.week >= weekNumStart &&
                       r.week <= weekNumEnd
                       select r);

            var dbrecords = (from r in q.Query select r);
            if(dbrecords != null)
                dbrecords.ToList();

            foreach (var item in dbrecords)
            {
                MobileJobManager jobMgr = new MobileJobManager(OrganizationID);
                var description = "";
                var clientName = "";

                foreach (var r in item.timesheetrecords)
                {
                    description = "";
                    clientName = "";

                    if (r.visitId != null)
                    {
                        using (CalaniEntities db = new CalaniEntities())
                        {
                            var visit = (from f in jobMgr.GetBaseQuery().Context.visits
                                         .Include("jobs")
                                       where f.id == (long)r.visitId
                                       select f).FirstOrDefault();

                            if (visit != null)
                            {
                                description = (!String.IsNullOrEmpty(visit.description) ? visit.description : (visit.jobs != null ? (!String.IsNullOrEmpty(visit.jobs.description) ? visit.jobs.description : "") : ""));
                                clientName = visit.jobs != null ? visit.jobs.clients.companyName : "";
                            }
                        }
                    }
                    else if (r.jobId != null)
                    {
                        using (CalaniEntities db = new CalaniEntities())
                        {
                            var job = (from f in jobMgr.GetBaseQuery().Context.jobs
                                       .Include("organizations")
                                         where r.jobId == f.id //r.visitId != null && f.id == (long)r.visitId
                                         select f).FirstOrDefault();

                            if (job != null)
                            {
                                description = (!String.IsNullOrEmpty(job.description) ? job.description : "");
                                clientName = job != null ? job.organizations.name : "";
                            }
                        }
                    }


                    var m = new MobileTimeSheetItemExtended(r, description, clientName, item.acceptedDate);
                    ret.Add(m);
                }
            }

            return ret;
        }
    }
}
