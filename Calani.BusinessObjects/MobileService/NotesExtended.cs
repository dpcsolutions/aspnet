﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Generic;

namespace Calani.BusinessObjects.MobileService
{
    public class NotesExtended
    {
        public string id { get; set; }
        public long jobId { get; set; }
        public String content { get; set; }
        public long authorId { get; set; }
        public string author { get; set; }
        public string authorName { get; set; }
        public String dateWritten { get; set; }
        public string mobileTempId { get; set; }
        public NotesExtended()
        {

        }

        public NotesExtended(Model.notes n)
        {
            id = n.id.ToString();
            jobId = (long)n.jobId;
            content = n.message;

            authorId = (long)n.contacts.id;
            author = n.contacts.initials;
            authorName = n.contacts.firstName + " " + n.contacts.lastName;

            

            dateWritten = ((DateTime)n.date).ToString(Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);
        }
    }
}
