﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.MobileService
{
    public class ExpensesReport
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public double Sum { get; set; }
        public int Count { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public long Employee { get; set; }
    }
}
