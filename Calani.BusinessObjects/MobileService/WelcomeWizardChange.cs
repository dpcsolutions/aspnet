﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.MobileService
{
    public class WelcomeWizardChange
    {
        public int step { get; set; }

        public WelcomeWizardChangeCompany company { get; set; }
        public WelcomeWizardChangeEmployee employee { get; set; }
        public WelcomeWizardChangeService service { get; set; }
        public WelcomeWizardChangeCustomer customer { get; set; }
    }

    public class WelcomeWizardChangeCompany
    {
        public string name { get; set; }
        public string street { get; set; }
        public string streetnb { get; set; }
        public string zipcode { get; set; }
        public string city { get; set; }
        public string phone { get; set; }
    }

    public class WelcomeWizardChangeEmployee
    {
        public string lastname { get; set; }
        public string firstname { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
    }

    public class WelcomeWizardChangeService
    {
        public string name { get; set; }
        public string code { get; set; }
        public double price { get; set; }

    }

    public class WelcomeWizardChangeCustomer
    {
        public string lastname { get; set; }
        public string firstname { get; set; }
        public string company { get; set; }
        public string email { get; set; }
    }
}
