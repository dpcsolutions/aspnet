﻿using Newtonsoft.Json.Linq;
using System.Configuration;

namespace Calani.BusinessObjects.Currency
{
    public class CurrencyConverter
    {
        public string SourceCurrency { get; set; }
        public double SourceValue { get; set; }


        public double Rate { get; set; }

        public string TargetCurrency { get; set; }
        public double TargetValue { get; set; }
        public CurrencyConverter()
        {
            SourceCurrency = "CHF";
            TargetCurrency = "EUR";
        }


        public void Convert()
        {
            //via exchangerateapi.io
            try
            {
                System.Net.WebClient wc = new System.Net.WebClient();
                string json = wc.DownloadString("https://api.exchangeratesapi.io/latest?symbols=" + TargetCurrency + "&base=" + SourceCurrency);

                JObject j = JObject.Parse(json);

                try
                {
                    if (((Newtonsoft.Json.Linq.JProperty)(j["rates"] as JObject).First).Name.ToString() == TargetCurrency)
                    {
                        var val = (((Newtonsoft.Json.Linq.JProperty)(j["rates"] as JObject).First).Value);
                        Rate = System.Convert.ToDouble(val);

                        TargetValue = SourceValue * Rate;
                    }
                }
                catch
                {
                }
            }
            catch { }

            if(TargetValue == 0)
            {
                var apiKey = ConfigurationManager.AppSettings["CurrConverterApiKey"];
                if (string.IsNullOrWhiteSpace(apiKey))
                {
                    TargetValue = 0;
                    return;
                }

                System.Net.WebClient wc = new System.Net.WebClient();
                string ccc = SourceCurrency + "_" + TargetCurrency;
                string url = $"https://free.currconv.com/api/v7/convert?q={ccc}&compact=y&apiKey={apiKey}";
                string json = wc.DownloadString(url);
                JObject j = JObject.Parse(json);

                try
                {
                    var val = j[ccc]["val"];
                    Rate = System.Convert.ToDouble(val);

                    TargetValue = SourceValue * Rate;
                }
                catch
                {
                }
            }
        }


        public double Convert(double sourceValue)
        {
            Convert();
            return TargetValue;
        }

        public double Convert(double sourceValue, string sourceCurrency, string targetCurrency)
        {
            SourceValue = sourceValue;
            SourceCurrency = sourceCurrency;
            TargetCurrency = targetCurrency;
            Convert();
            return TargetValue;
        }
        
    }
}
