﻿using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Currency
{
    public class CurrenciesManager : Generic.SmartCollectionManager<Model.currency, long>
    {

        public CurrenciesManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<Model.currency> GetDbSet(Model.CalaniEntities db)
        {
            return db.currency;
        }

        internal override SmartQuery<Model.currency> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.currency where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(Model.currency record)
        {
            return record.organizationId;
        }

        internal override SmartQuery<Model.currency> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from r in q.Context.currency select r);
            if (OrganizationID > -1) q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.currency1 select r);


            return q;
        }


        internal override void BeforeAction(SmartAction<currency> action)
        {
            if(action.Type == SmartActionType.Add || action.Type == SmartActionType.Update)
            {
                if(action.Record?.rate == 0)
                {
                    action.Abort("Change rate is missing (it can not be 0)");
                }
            }

            base.BeforeAction(action);
        }
    }
}
