﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Membership;
using log4net;

namespace Calani.BusinessObjects.MassMailing
{
    public class MassMailingScheduler
    {

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public List<Model.contacts> QueryEndOfTrialDays(int days, bool? everconnected)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();

            DateTime dtFrom = DateTime.Today;
            DateTime dtTo = DateTime.Today.AddDays(1);

            dtFrom = dtFrom.AddDays(-1 * days);
            dtTo = dtTo.AddDays(-1 * days);

            // si le compte a été créé il y a 11 jours

            var q = (from r in db.organizations
                     where r.trialEnd != null
                     && r.trialEnd >= dtFrom
                     && r.trialEnd <= dtTo
                     select r);

            if (everconnected != null)
            {
                if (everconnected == true)
                {
                    q = (from r in q
                         where r.contacts1 != null
                               && r.contacts1.lastlogin != null
                         select r);
                }
                else
                {
                    q = (from r in q
                         where r.contacts1 != null
                               && r.contacts1.lastlogin == null
                         select r);
                }
            }
            

            List<Model.contacts> ret = (from r in q select r.contacts1).ToList();

            return ret;
        }


        public List<Model.contacts> QueryExpirationDays(int days)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();

            DateTime dtFrom = DateTime.Today;
            DateTime dtTo = DateTime.Today.AddDays(1);

            dtFrom = dtFrom.AddDays(-1 * days);
            dtTo = dtTo.AddDays(-1 * days);

            // si le compte a été créé il y a 11 jours

            var q = (from r in db.organizations
                where r.subscriptionEnd != null
                      && r.subscriptionEnd >= dtFrom
                      && r.subscriptionEnd <= dtTo
                select r);

          
            List<Model.contacts> ret = (from r in q select r.contacts1).ToList();

            return ret;
        }

        
        public List<Model.contacts> QueryNeverConnectedSinceSubscription(int days)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();

            DateTime dtFrom = DateTime.Today;
            DateTime dtTo = DateTime.Today.AddDays(1);

            dtFrom = dtFrom.AddDays(-1 * days);
            dtTo = dtTo.AddDays(-1 * days);

            // si le compte a été créé il y a 11 jours

            List<Model.contacts> ret = (from r in db.organizations
                                        where r.trialEnd != null
                                        && r.contacts1 != null
                                        && r.contacts1.lastlogin == null
                                        && r.creationDate >= dtFrom
                                        && r.creationDate <= dtTo
                                        select r.contacts1).ToList();

            return ret;
        }

        public List<Model.contacts> QueryLastLoginFromDays(int days)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();

            DateTime dtFrom = DateTime.Today;
            DateTime dtTo = DateTime.Today.AddDays(1);

            dtFrom = dtFrom.AddDays(-1 * days);
            dtTo = dtTo.AddDays(-1 * days);

            // si le compte a été créé il y a 11 jours

            List<Model.contacts> ret = (from r in db.organizations
                                        where r.trialEnd != null
                                        && r.contacts1 != null
                                        && r.contacts1.lastlogin != null
                                        && r.contacts1.lastlogin >= dtFrom
                                        && r.contacts1.lastlogin <= dtTo
                                        select r.contacts1).ToList();

            return ret;
        }

        public void QueryAndSendNotValidatedTimeSheets(long orgId, int days)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();

            DateTime dtTo = DateTime.Today.Date.AddDays(days);

            var users = db.contacts.Where(c => c.organizationId == orgId && c.type == 1 && c.recordStatus == 0).Select(r => r).ToList();

            var anyTimeSheets = (from r in db.timesheets
                where
                    r.recordStatus == 0
                    && r.organizationId == orgId
                    && r.acceptedDate == null
                    && r.sentDate.HasValue
                    && r.sentDate <= dtTo
                select r).Any();


            var anyExpenses = (from r in db.expenses
                where
                    r.recordStatus == 0
                    && r.contacts != null && r.contacts.organizationId != null && r.contacts.organizationId == orgId
                    && r.amount<= r.approvedmount
                    && r.lastModificationDate.HasValue
                    && r.lastModificationDate <= dtTo
                select r).Any();

            Generic.MailSender mailSender = new Generic.MailSender();
            var pUrl = System.Configuration.ConfigurationManager.AppSettings["PublicUrl"];
            mailSender.Variables.Add("PublicUrl", pUrl);

            StringBuilder msg = new StringBuilder();
            var title = String.Empty;
            var category = String.Empty;

            var cultureStr = "fr-FR";

            var culture = new System.Globalization.CultureInfo(cultureStr);

            if (anyExpenses || anyExpenses)
            {
                foreach (var user in users)
                {
                    if (String.IsNullOrWhiteSpace(user.primaryEmail))
                    {

                        try
                        {
                            cultureStr = UserCultureManager.GetUserCulture(user.id);

                            culture = new System.Globalization.CultureInfo(cultureStr ?? "fr-FR");
                        }
                        catch (Exception e)
                        {
                            _log.Error("SendNotification, getUserCulture error.", e);
                        }


                        title = GetResource("EmailPendingValidationItemsTitle", culture);

                        msg.AppendLine(GetResource("EmailPendingValidationItemsMsg", culture));


                        if (anyTimeSheets)
                        {
                            var rMsg1 = GetResource("EmailPendingValidationTimeSheetsMsg", culture);
                            msg.AppendLine(String.Format(rMsg1, pUrl + "Time/Sheets/"));

                        }

                        if (anyExpenses)
                        {
                            var rMsg2 = GetResource("EmailPendingValidationExpensesMsg", culture);
                            msg.AppendLine(String.Format(rMsg2, pUrl + "Costs/Expenses/"));

                        }


                        mailSender.MailTo.Email = user.primaryEmail;
                        mailSender.MailTo.UserId = user.id;
                        mailSender.SendSmartHtmlEmail(title, msg.ToString(), category);
                    }

                }
            }

        }

        public List<long> QueryActiveOrgIds()
        {
            Model.CalaniEntities db = new Model.CalaniEntities();

            List<long> ret = (from r in db.organizations
                where r.recordStatus == 0
                select r.id).ToList();

            return ret;
        }


        public void Send(List<Model.contacts> contacts, List<string> notsendto, string type, string file)
        {
            if(notsendto == null) 
                notsendto = new List<string>();

            if(contacts != null && contacts.Count > 0)
            {
                foreach (var contact in contacts)
                {
                    if (!notsendto.Contains(contact.primaryEmail))
                    {
                        Generic.MailSender mailSender = new Generic.MailSender();
                        mailSender.MailTo.Email = contact.primaryEmail;
                        mailSender.MailTo.UserId = contact.id;
                        mailSender.Variables.Add("custemail", contact.primaryEmail);
                        mailSender.ReplyTo = "david@gesmobile.ch";
                        mailSender.FromName = "David de Gesmobile";
                        mailSender.SendSmartHtmlEmail(file, type);
                    }
                }
            }
        }

        private string GetResource(string n, CultureInfo cultureInfo)
        {
            if (SharedResource.Resource == null) return n;
            return SharedResource.Resource.GetString(n, cultureInfo);
        }

    }



}
