﻿using Calani.BusinessObjects.AjaxService;
using log4net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Calani.BusinessObjects.Attachments
{
    public class AttachmentsManager : Generic.SmartCollectionManager<Model.attachments, long>
    {
        public List<FileTypeDefinition> FileTypesDefinition { get; private set; }

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public AttachmentsManager()
        {
            FileTypesDefinition = new List<FileTypeDefinition>();
            FileTypesDefinition.Add(new FileTypeDefinition
            {
                Extension = "xlsx",
                EnumTypes = new List<AttachmentTypeEnum>
                {
                     AttachmentTypeEnum.AttachedXlsx,
                     AttachmentTypeEnum.GeneratedXlsx_Generic,
                     AttachmentTypeEnum.GeneratedXlsx_Invoice,
                     AttachmentTypeEnum.GeneratedXlsx_Quote,
                     AttachmentTypeEnum.TaskXlsx
                },
                Mime = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            });
            FileTypesDefinition.Add(new FileTypeDefinition {
                Extension = "docx",
                EnumTypes = new List<AttachmentTypeEnum>
                {
                    AttachmentTypeEnum.AttachedDocx,
                    AttachmentTypeEnum.Taskdocx,
                },
                Mime = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
            });
            FileTypesDefinition.Add(new FileTypeDefinition {
                Extension = "doc",
                EnumTypes = new List<AttachmentTypeEnum>
                {
                    AttachmentTypeEnum.AttachedDoc,
                     AttachmentTypeEnum.Taskdoc
                },
                Mime = "application/octet-stream"
            });
            /*FileTypesDefinition.Add(new FileTypeDefinition
            {
                Extension = "docx",
                EnumTypes = new List<AttachmentTypeEnum>
                {
                     AttachmentTypeEnum.Taskdocx
                },
                Mime = "application/octet-stream"
            });*/
            FileTypesDefinition.Add(new FileTypeDefinition
            {
                Extension = "ppt",
                EnumTypes = new List<AttachmentTypeEnum>
                {
                     AttachmentTypeEnum.TaskPpt
                },
                Mime = "application/octet-stream"
            });
            FileTypesDefinition.Add(new FileTypeDefinition
            {
                Extension = "pptx",
                EnumTypes = new List<AttachmentTypeEnum>
                {
                    AttachmentTypeEnum.AttachedPptx,
                    AttachmentTypeEnum.TaskPptx
                },
                Mime = "application/vnd.openxmlformats-officedocument.presentationml.presentation"
            });

            FileTypesDefinition.Add(new FileTypeDefinition
            {
                Extension = "ppt",
                EnumTypes = new List<AttachmentTypeEnum>
                {
                    AttachmentTypeEnum.AttachedPpt,
                },
                Mime = "application/octet-stream"
            });
            FileTypesDefinition.Add(new FileTypeDefinition
            {
                Extension = "pdf",
                EnumTypes = new List<AttachmentTypeEnum>
                {
                    AttachmentTypeEnum.AttachedPdf,
                    AttachmentTypeEnum.GeneratedPdf_Generic,
                    AttachmentTypeEnum.GeneratedPdf_Invoice,
                    AttachmentTypeEnum.GeneratedPdf_Quote,
                    AttachmentTypeEnum.TaskPdf
                },
                Mime = "application/pdf"
            });
            FileTypesDefinition.Add(new FileTypeDefinition
            {
                Extension = "jpg",
                EnumTypes = new List<AttachmentTypeEnum>
                {
                    AttachmentTypeEnum.AttachedJpeg,
                    AttachmentTypeEnum.TaskJpeg,
                    AttachmentTypeEnum.DeliveryNoteSign
                },
                Mime = "image/jpeg"
            });
            FileTypesDefinition.Add(new FileTypeDefinition
            {
                Extension = "png",
                EnumTypes = new List<AttachmentTypeEnum>
                {
                    AttachmentTypeEnum.AttachedPng,
                    AttachmentTypeEnum.TaskPng
                },
                Mime = "image/png"
            });
            FileTypesDefinition.Add(new FileTypeDefinition
            {
                Extension = "gif",
                EnumTypes = new List<AttachmentTypeEnum>
                {
                    AttachmentTypeEnum.AttachedGif,
                    AttachmentTypeEnum.TaskGif
                },
                Mime = "image/gif"
            });
            FileTypesDefinition.Add(new FileTypeDefinition
            {
                Extension = "csv",
                EnumTypes = new List<AttachmentTypeEnum>
                {
                    AttachmentTypeEnum.AttachedUnmanagedFile,
                    AttachmentTypeEnum.TaskUnmanagedFile
                },
                Mime = "text/csv"
            });
            FileTypesDefinition.Add(new FileTypeDefinition
            {
                Extension = "mp4",
                EnumTypes = new List<AttachmentTypeEnum>
                {
                    AttachmentTypeEnum.Taskmp4
                },
                Mime = "video/mp4"
            });
            FileTypesDefinition.Add(new FileTypeDefinition
            {
                Extension = "avi",
                EnumTypes = new List<AttachmentTypeEnum>
                {
                    AttachmentTypeEnum.Taskavi
                },
                Mime = "video/avi"
            });
            FileTypesDefinition.Add(new FileTypeDefinition
            {
                Extension = "mov",
                EnumTypes = new List<AttachmentTypeEnum>
                {
                    AttachmentTypeEnum.Taskmov
                },
                Mime = "video/quicktime"
            });
        }

        public AttachmentsManager(long organizationId) : this()
        {
            OrganizationId = organizationId;
        }



        //used in PDF generation. If not empty constructor, we have error: ERR_RESPONSE_HEADERS_MULTIPLE_CONTENT_DISPOSITION
        public AttachmentsManager(bool empty )
        {

        }
        public void Delete(long attachmentId)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();
            Model.attachments att = (from r in db.attachments
                                     where r.id == attachmentId
                                     && r.organizationId == OrganizationId
                                     select r).FirstOrDefault();
            if(att != null)
            {
                db.attachments.Remove(att);
                db.SaveChanges();
            }
            
        }

        public string GetCustomName(attachmentsExtended attachment)
        {
            string ret = attachment.name;
            string ext = (from r in FileTypesDefinition where r.Mime == attachment.mimeType select r.Extension).FirstOrDefault();
            if (ext == null) ext = ".pdf";
            return ret + "." + ext;
        }

        public static string DIR { get; set; }
        public static int MaxUploadedImagesWidth { get; set; }
        public long OrganizationId { get; set; }

        public long UserId { get; set; }
        public void ResolveFileName(attachmentsExtended attachment)
        {
            attachment.FilePath = Path.Combine(GetDir().FullName, attachment.file);
        }

        public DirectoryInfo GetDir()
        {
            DirectoryInfo di = new DirectoryInfo(Path.Combine(DIR, OrganizationId.ToString()));
            if (!di.Exists) di.Create();
            return di;
        }


        public string WritePreviewPdf(byte[] data)
        {
            string dir = Path.Combine(GetDir().FullName, "preview");
            string file = Guid.NewGuid().ToString("n") + ".pdf";
            string path = Path.Combine(dir, file);
            if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
            var di = new DirectoryInfo(dir);
            foreach (var item in di.GetFiles("*.pdf"))
            {
                try
                {
                    if (DateTime.Now.Subtract(item.CreationTime).TotalMinutes > 5)
                        item.Delete();
                }
                catch { }
            }
            System.IO.File.WriteAllBytes(path, data);
            return file;
        }

        public long AddAttachment(byte[] data, AttachmentTypeEnum type, long? jobid, long? clientid, long? userId, string name,
            AttachmentProperties prop = AttachmentProperties.None, string description = "")
        {
            long ret = -1;

            Model.CalaniEntities db = new Model.CalaniEntities();
            Model.attachments att = new Model.attachments();
            att.organizationId = OrganizationId;
            att.jobId = jobid;
            att.contactId = userId;
            att.clientId = clientid;
            
            string extension = "";

            foreach (var def in FileTypesDefinition)
            {
                foreach (var t in def.EnumTypes)
                {

                    if(type != AttachmentTypeEnum.None && Convert.ToInt32(t) == Convert.ToInt32(type))
                    {
                        att.mimeType = def.Mime;
                        att.type = Convert.ToInt32(type);
                        extension = def.Extension;
                        break;
                    }
                }
            }


            var fi = WriteFile(data, extension);
            

            att.file = fi.Name;
            att.name = name;
            att.date = DateTime.Now;
            att.properties = (int)prop;
            att.description = description;

            try
            {
                db.attachments.Add(att);
                db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

           
            ret = att.id;
            return ret;
        }


        private FileInfo WriteFile(byte[] data, string extension)
        {
            FileInfo fi = null;
            bool retry = true;
            int limit = 5;
            do
            {
                string file = "";
                file = Guid.NewGuid().ToString() + "." + extension;
                fi = new FileInfo(Path.Combine(GetDir().FullName, file));
                if (!fi.Exists)
                {
                    try
                    {
                        File.WriteAllBytes(fi.FullName, data);
                        retry = false;
                    }
                    catch
                    {
                        retry = true;
                        System.Threading.Thread.Sleep(100);
                        retry = true;

                    }
                }
                else
                {
                    retry = true;
                }

                if (retry)
                {
                    limit--;
                    if (limit == 0)
                    {
                        throw new Exception("Impossible to write attachment on disk");
                    }
                }


            } while (retry);

            return fi;
        }


        public Model.attachments CloneAttachment(Model.attachments src)
        {
            long ret = -1;

            Model.attachments att = new Model.attachments()
            {
                date = src.date,
                recordStatus = src.recordStatus,
                properties = src.properties,
                description = src.description,
                name = src.name,
                clientId = src.clientId,
                mimeType = src.mimeType,
                organizationId = src.organizationId,
                thumbnail = src.thumbnail,
                type = src.type,
                file = src.file
            };

            var path = GetPath(src.id);

            if (!String.IsNullOrWhiteSpace(path))

            {
                byte[] data = System.IO.File.ReadAllBytes(path);


                string extension = "";

                foreach (var def in FileTypesDefinition)
                {
                    foreach (var t in def.EnumTypes)
                    {
                        if (Convert.ToInt32(t) == att.type)
                        {
                            extension = def.Extension;
                            break;
                        }
                    }
                }

                var fi = WriteFile(data, extension);


                att.file = fi.Name;
                att.date = DateTime.Now;
            }


            return att;
        }

        internal int CountJobVersionsDoc(AttachmentTypeEnum type, long jobId)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();
            var fType = Convert.ToInt32(type);

            int c = (from r in db.attachments
                     where r.organizationId == OrganizationId
                        && r.jobId == jobId
                        && r.type == fType

                     select r).Count();

            return c + 1;
        }


        public List<attachmentsExtended> ListJobAttachments(long jobId, bool? onlyNotGenerated=null, bool includeSign = false)
        {
            var db = new Model.CalaniEntities();
            var records = (from r in db.attachments
               where r.organizationId == OrganizationId
                  && r.jobId == jobId
                  && r.recordStatus == 0
               orderby r.date descending
               select r).ToList();

            if (!includeSign)
                records = records.Where(r => r.type != (int) AttachmentTypeEnum.DeliveryNoteSign).ToList();
            
            var ret = records.Select(r => new attachmentsExtended(r)).ToList();
            foreach (var r in ret)
            {
                r.FilePath = Path.Combine(GetDir().FullName, r.file);
            }

            ret = ret.Where(r => File.Exists(r.FilePath)).ToList();
            foreach (var r in ret)
            {
                if(String.IsNullOrEmpty(r.thumbnail) && System.Web.MimeMapping.GetMimeMapping(r.FilePath).StartsWith("image/"))
                {
                    var a = db.attachments.FirstOrDefault(dbr => dbr.id == r.id);
                    if (a != null)
                    {
                       a.thumbnail = GetThumbnailImage(r.FilePath);
                       db.SaveChanges();
                       r.thumbnail = a.thumbnail;
                    }
                }
            }
            
            if (onlyNotGenerated == true)
            {

                List<int> filter = Calani.BusinessObjects.Attachments.AttachmentsManager.GetNonGeneratedTypes();
                ret = (from r in ret where r.type != null && filter.Contains(r.type.Value) select r).ToList();
            }
            return ret;
        }

        public List<attachmentsExtended> ListAttachmentsById(List<long> attchIds)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();

            var records = (from r in db.attachments
                where r.organizationId == OrganizationId
                      && attchIds.Any(x => x == r.id)
                      && r.recordStatus == 0
                orderby r.date descending
                select r).ToList();
            var ret = (from r in records select new attachmentsExtended(r)).ToList();
            foreach (var r in ret) r.FilePath = Path.Combine(GetDir().FullName, r.file);
            ret = (from r in ret where File.Exists(r.FilePath) select r).ToList();

            foreach (var r in ret)
            {
                if (String.IsNullOrEmpty(r.thumbnail) && System.Web.MimeMapping.GetMimeMapping(r.FilePath).StartsWith("image/"))
                {
                    var a = db.attachments.FirstOrDefault(dbr => dbr.id == r.id);
                    if (a != null)
                    {
                        a.thumbnail = GetThumbnailImage(r.FilePath);
                        db.SaveChanges();
                        r.thumbnail = a.thumbnail;
                    }
                }
            }

            return ret;
        }

        
        public List<attachmentsExtended> ListUserAttachments(long contactId)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();


            var records = (from r in db.attachments
                           where r.organizationId == OrganizationId
                              && r.contactId == contactId
                              && r.recordStatus == 0

                           orderby r.date descending
                           select r).ToList();

            var ret = (from r in records select new attachmentsExtended(r)).ToList();

            foreach (var r in ret)
            {
                r.FilePath = Path.Combine(GetDir().FullName, r.file);
            }

            ret = (from r in ret where File.Exists(r.FilePath) select r).ToList();

            foreach (var r in ret)
            {
                if (String.IsNullOrEmpty(r.thumbnail) && System.Web.MimeMapping.GetMimeMapping(r.FilePath).StartsWith("image/"))
                {
                    var a = db.attachments.FirstOrDefault(dbr => dbr.id == r.id);
                    if (a != null)
                    {
                        a.thumbnail = GetThumbnailImage(r.FilePath);

                        db.SaveChanges();

                        r.thumbnail = a.thumbnail;
                    }
                }
            }

            return ret;
        }

        private string GetThumbnailImage(string fileName)
        {
            Image image = Image.FromFile(fileName);
            Image thumb = image.GetThumbnailImage(120, 120, () => false, IntPtr.Zero);
            var res = String.Empty;
            using (MemoryStream ms = new MemoryStream())
            {
                thumb.Save(ms, image.RawFormat);
                res = Convert.ToBase64String(ms.ToArray());
            }

            return res;
        }

        public attachmentsExtended Get(long id)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();


            var dbrec = (from r in db.attachments
                     where r.organizationId == OrganizationId
                        && r.recordStatus == 0
                        && r.id == id
                     select r).FirstOrDefault();
            if (dbrec == null)
                return null;

            

            attachmentsExtended ret = new attachmentsExtended(dbrec);

            ret.FilePath = Path.Combine(GetDir().FullName, ret.file);

            if (String.IsNullOrEmpty(dbrec.thumbnail) && MimeMapping.GetMimeMapping(ret.FilePath).StartsWith("image/"))
            {
                dbrec.thumbnail = GetThumbnailImage(ret.FilePath);

                    db.SaveChanges();

                    ret.thumbnail = dbrec.thumbnail;
            }

            return ret;
        }

        public static List<int> GetGeneratedTypes()
        {
            List<int> ret = new List<int>();
            foreach (AttachmentTypeEnum t in Enum.GetValues(typeof(AttachmentTypeEnum)))
            {
                if(t.ToString().Contains("Generated"))
                {
                    ret.Add(Convert.ToInt32(t));
                }
            }
            return ret;
        }

        public static List<int> GetNonGeneratedTypes()
        {
            List<int> ret = new List<int>();
            foreach (AttachmentTypeEnum t in Enum.GetValues(typeof(AttachmentTypeEnum)))
            {
                if (!t.ToString().Contains("Generated"))
                {
                    ret.Add(Convert.ToInt32(t));
                }
            }
            return ret;
        }

        public static List<string> AllowedMimeType { get; set; }

       public long UploadAttachment(string name, string type, HttpPostedFile file, byte[] data, long? jobId, long? userId,
            AttachmentTypeEnum attachmentType = AttachmentTypeEnum.None, string properties = "0", string description = "")
        {
            long res = -1;

            FileTypeDefinition ftype = null;
            var fileTempPath = UploadAttachmentTemp(name, type, file, data, ref ftype);
            
            if (!string.IsNullOrEmpty(fileTempPath))
            {

                res = UploadFileAttachmentToDb(fileTempPath, jobId, userId, ftype, attachmentType, properties, description);

            }
            else
            {
                throw new Exception("Not allowed file type");
            }
            

            return res;
        }

        public string UploadAttachmentTemp(string name, string type, HttpPostedFile file, byte[] data, ref FileTypeDefinition ftype)
        {
            string fileTempPath = string.Empty;

            if (file != null)
            {
                name = file.FileName;
                type = file.ContentType.ToLower();
            }
            
            ftype = (from r in FileTypesDefinition
                                        where r.Mime.ToLower() == type.ToLower()
                                        select r).FirstOrDefault();

            string ext = new FileInfo(name).Extension.Trim('.');

            if (ftype == null )
            {
                ftype = (from r in FileTypesDefinition
                         where r.Extension == ext
                         select r).FirstOrDefault();
            }


            if (ftype != null)
            {

                string g = Guid.NewGuid().ToString();
                string fileTemp = name + ".temp"+g;
                fileTempPath = Path.Combine(GetDir().FullName, fileTemp);

                if (file != null)
                {
                    file.SaveAs(fileTempPath);
                }
                else if (data != null)
                {
                    System.IO.File.WriteAllBytes(fileTempPath, data);
                }

                if (ftype.Mime.Contains("image/"))
                {
                    Generic.ImageManager imgMgr = new Generic.ImageManager();
                    imgMgr.MaxSide = MaxUploadedImagesWidth;
                    imgMgr.Init(File.ReadAllBytes(fileTempPath));
                    byte[] newData = null;


                    if (ftype.Mime == "image/png")
                    {
                        newData = imgMgr.Save(System.Drawing.Imaging.ImageFormat.Png);
                    }
                    else if (ftype.Mime == "image/gif")
                    {
                        newData = imgMgr.Save(System.Drawing.Imaging.ImageFormat.Gif);
                    }
                    else
                    {
                        newData = imgMgr.Save(System.Drawing.Imaging.ImageFormat.Jpeg);
                        ftype = (from r in FileTypesDefinition where r.Mime == "image/jpeg" select r).FirstOrDefault();
                    }

                    File.Delete(fileTempPath);
                    File.WriteAllBytes(fileTempPath, newData);

                }
            }

            return fileTempPath;
        }


        public long UploadFileAttachmentToDb(string fileTempPath, long? jobId, long? userId, FileTypeDefinition ftype,
            AttachmentTypeEnum attachmentType = AttachmentTypeEnum.None, string properties = "0",
            string description = "")
        {
            try
            {
                int ip = 0;
                Int32.TryParse(properties, out ip);

                AttachmentProperties prop = (AttachmentProperties)ip;

                var data = File.ReadAllBytes(fileTempPath);
                var name = Path.GetFileNameWithoutExtension(fileTempPath);


                string ext = Path.GetExtension(name).Trim('.').ToLowerInvariant();

                name = Path.GetFileNameWithoutExtension(name);

                if (ftype == null)
                    ftype = (from r in FileTypesDefinition
                             where r.Extension == ext
                             select r).FirstOrDefault();


                if (ftype != null && attachmentType == AttachmentTypeEnum.None)
                {
                    List<AttachmentTypeEnum> enumtypes =
                        (from r in ftype.EnumTypes where r.ToString().StartsWith("Attached") select r).ToList();

                    if (enumtypes.Count > 0)
                        attachmentType = enumtypes.First();
                }


                var res = AddAttachment(data, attachmentType, jobId, clientid: null, userId, name, prop, description);
                try
                {
                    File.Delete(fileTempPath);
                }
                catch { }

                return res;
            }
            catch (IOException ex)
            {
                _log.Error(ex);   
                return 0;
            }
        }

  

    public List<TaskActivityMessage> AttachmentMessageByTask(long taskId,long userId)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();

            var records = (from r in db.attachments join 
                           ta in db.task_attachments on r.id equals ta.attachmentId
                           join c in db.contacts on ta.contactId equals c.id
                           where r.organizationId == OrganizationId
                              && ta.organizationId == OrganizationId
                              && ta.taskId == taskId
                              && r.recordStatus == 0
                           orderby r.date descending
                           select new TaskActivityMessage { 
                           AttachmentId = r.id,
                           AttachmentMimeType = r.mimeType,
                           AttachmentName = r.name,
                           AttachmentType = r.type,
                           CreatedAt = r.date,
                           IsAttachment = true,
                           AttachmentFile =  r.file,
                           OrganizationId = r.organizationId,
                           IsOwned = userId == ta.contactId,
                           UserFirstName = c.firstName,
                           UserLastName = c.lastName,
                           FileNameNoExtension = !string.IsNullOrEmpty(r.file) ? r.file.Split(new[] { '.' })[0] : string.Empty,
            CodeName = (string.IsNullOrEmpty(c.firstName) ? string.Empty : c.firstName.Substring(0, 1) )+ " " +( string.IsNullOrEmpty(c.lastName) ? string.Empty : c.lastName.Substring(0, 1))
                           })
                           .ToList();

            foreach (var r in records) {
                r.AttachmentPath = Path.Combine(GetDir().FullName, r.AttachmentFile);
                string AttachmentThumbnailPath = Path.Combine(GetDir().FullName, "Thumbnail_" + r.AttachmentFile);
                if (r.AttachmentMimeType.Contains("image/") && File.Exists(AttachmentThumbnailPath))
                {
                    r.ImageData = Convert.ToBase64String(File.ReadAllBytes(AttachmentThumbnailPath));
                }
            }

            records = (from r in records where File.Exists(r.AttachmentPath) select r).ToList();

            return records;
        }

        public bool UploadAttachmentByTask(System.Web.HttpPostedFile file, long taskId)
        {
            return UploadAttachmentForTaskActivity(null, null, file, null, taskId);
        }

        public bool UploadAttachmentForTaskActivity(string name, string type, System.Web.HttpPostedFile file, byte[] data, long TaskId)
        {
            if (file != null)
            {
                name = file.FileName;
                type = file.ContentType.ToLower();
            }
            string ext = new FileInfo(name).Extension.Trim('.');

            FileTypeDefinition ftype = (from r in FileTypesDefinition
                                        where r.Mime.ToLower() == type.ToLower()
                                        && r.Extension.ToLower() == ext
                                        select r).FirstOrDefault();

            
            if (ftype == null && type == "application/octet-stream")
            {
                ftype = (from r in FileTypesDefinition
                         where r.Extension == ext
                         select r).FirstOrDefault();
            }


            if (ftype != null)
            {

                string g = Guid.NewGuid().ToString();
                string fileTemp = g + ".temp";
                string fileTempPath = Path.Combine(GetDir().FullName, fileTemp);

                if (file != null)
                {
                    file.SaveAs(fileTempPath);
                }
                else if (data != null)
                {
                    System.IO.File.WriteAllBytes(fileTempPath, data);
                }

                if (ftype.Mime.Contains("image/"))
                {
                    Generic.ImageManager imgMgr = new Generic.ImageManager();
                    imgMgr.MaxSide = MaxUploadedImagesWidth;
                    imgMgr.Init(File.ReadAllBytes(fileTempPath));
                    byte[] newData = null;

                    if (ftype.Mime == "image/png")
                    {
                        newData = imgMgr.Save(System.Drawing.Imaging.ImageFormat.Png);
                    }
                    else if (ftype.Mime == "image/gif")
                    {
                        newData = imgMgr.Save(System.Drawing.Imaging.ImageFormat.Gif);
                    }
                    else
                    {
                        newData = imgMgr.Save(System.Drawing.Imaging.ImageFormat.Jpeg);
                        ftype = (from r in FileTypesDefinition where r.Mime == "image/jpeg" select r).FirstOrDefault();
                    }

                    File.Delete(fileTempPath);
                    File.WriteAllBytes(fileTempPath, newData);

                }


                AttachmentTypeEnum enumtype = AttachmentTypeEnum.AttachedUnmanagedFile;
                List<AttachmentTypeEnum> enumtypes = (from r in ftype.EnumTypes where r.ToString().StartsWith("Task") select r).ToList();
                if (enumtypes.Count > 0) enumtype = enumtypes.First();


                AddAttachmentByTask(File.ReadAllBytes(fileTempPath), enumtype, TaskId, null, Path.GetFileNameWithoutExtension(name));
                try
                {
                    File.Delete(fileTempPath);
                }
                catch { }
            }
            else
            {
                throw new Exception("Not allowed file type");
            }
            return true;
        }

        public long AddAttachmentByTask(byte[] data, AttachmentTypeEnum type,
           long? TaskId, long? clientid, string name)
        {
            long ret = -1;

            Model.CalaniEntities db = new Model.CalaniEntities();
            Model.attachments att = new Model.attachments();
            att.organizationId = OrganizationId;
            att.clientId = clientid;

            string extension = "";

            foreach (var def in FileTypesDefinition)
            {
                foreach (var t in def.EnumTypes)
                {
                    if (Convert.ToInt32(t) == Convert.ToInt32(type))
                    {
                        att.mimeType = def.Mime;
                        att.type = Convert.ToInt32(type);
                        extension = def.Extension;
                        break;
                    }
                }
            }

            FileInfo fi = null;
            bool retry = true;
            int limit = 5;
            do
            {
                string file = "";
                file = Guid.NewGuid().ToString() + "." + extension;
                fi = new FileInfo(Path.Combine(GetDir().FullName, file));
                if (!fi.Exists)
                {
                    try
                    {

                        if (att.mimeType.Contains("image/"))
                        {
                            FileInfo exfile = new FileInfo(Path.Combine(GetDir().FullName, "Thumbnail_" + file));
                            SaveThumbnail(exfile, data, att.mimeType);
                        }
                        File.WriteAllBytes(fi.FullName, data);
                        retry = false;
                    }
                    catch
                    {
                        retry = true;
                        System.Threading.Thread.Sleep(100);
                        retry = true;
                    }
                }
                else
                {
                    retry = true;
                }

                if (retry)
                {
                    limit--;
                    if (limit == 0)
                    {
                        throw new Exception("Impossible to write attachment on disk");
                    }
                }
            } while (retry);

            att.file = fi.Name;
            att.name = name;
            att.date = DateTime.Now;

            db.attachments.Add(att);

            db.task_attachments.Add(new Model.task_attachments
            {
                attachmentId = att.id,
                organizationId = OrganizationId,
                taskId = TaskId,
                contactId = UserId
            });
            db.SaveChanges();
            return att.id;
        }

        private void SaveThumbnail(FileInfo thumbNail, byte[] data, string mimiType)
        {
            Generic.ImageManager imgMgr = new Generic.ImageManager();
            imgMgr.MaxSide = 200;
            imgMgr.Init(data);
            byte[] newData = null;
            if (mimiType == "image/png")
            {
                newData = imgMgr.Save(System.Drawing.Imaging.ImageFormat.Png);
            }
            else if (mimiType == "image/gif")
            {
                newData = imgMgr.Save(System.Drawing.Imaging.ImageFormat.Gif);
            }
            else
            {
                newData = imgMgr.Save(System.Drawing.Imaging.ImageFormat.Jpeg);
                mimiType = "image/jpeg";
            }
            if (newData != null)
            {
                File.WriteAllBytes(thumbNail.FullName, newData);
            }
        }

        public MemoryStream GetStream(long id, bool setHeaders = true)
        {
            var rec = this.Get(id);
            if (rec == null)
                return null;

            byte[] data = null;
            if (System.IO.File.Exists(rec.FilePath))
            {
                data = System.IO.File.ReadAllBytes(rec.FilePath);
            }


            if (data != null)
            {
                MemoryStream ms = new MemoryStream();
                ms.Write(data, 0, data.Length);
                ms.Position = 0;
                if (setHeaders && System.ServiceModel.Web.WebOperationContext.Current != null)
                {
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.ContentType = rec.mimeType;
                    System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add(
                        "Content-disposition", "inline; filename=" + Uri.EscapeDataString(rec.NameWithExtension));
                }

                return ms;
            }

            return null;
        }

        public String GetPath(long id)
        {
            var rec = this.Get(id);
            if (rec == null || !System.IO.File.Exists(rec.FilePath))
                return "";

            return rec.FilePath;
        }

        public attachmentsExtended UpdateAttachmentProperties(long id, string properties)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();


            var dbrec = (from r in db.attachments
                where r.organizationId == OrganizationId
                      && r.recordStatus == 0
                      && r.id == id
                select r).FirstOrDefault();
            
            if (dbrec == null) 
                return null;

            int ip = 0;
            Int32.TryParse(properties, out ip);

            AttachmentProperties prop = (AttachmentProperties)ip;

            dbrec.properties = (int) prop;

            db.SaveChanges();

            attachmentsExtended ret = new attachmentsExtended(dbrec);
            ret.FilePath = Path.Combine(GetDir().FullName, ret.file);
            return ret;
        }
    }

    public class FileTypeDefinition
    {
        public string Extension { get; set;}
        public string Mime { get; set; }
        public List<AttachmentTypeEnum> EnumTypes { get; set; }
    }

}