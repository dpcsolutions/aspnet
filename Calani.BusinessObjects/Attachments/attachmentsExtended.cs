﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.MobileService2;

namespace Calani.BusinessObjects.Attachments
{
    public class attachmentsExtended : Model.attachments
    {
        public attachmentsExtended()
        {

        }
        public attachmentsExtended(Model.attachments r)
        {
            Generic.Tools.CopyAllProperties(r, this);
        }

        public string Icon
        {
            get
            {
                if (type != null)
                {
                    AttachmentTypeEnum t = (AttachmentTypeEnum)type;
                    if (t.ToString().Contains("Pdf"))
                    {
                        return "icon-file-pdf";
                    }
                    else if (t.ToString().Contains("Xls"))
                    {
                        return "icon-file-excel";
                    }
                    else if (t.ToString().Contains("Doc"))
                    {
                        return "icon-file-word";
                    }
                    else if (t.ToString().Contains("Ppt"))
                    {
                        return "icon-file-presentation";
                    }

                    
                }
                return "icon-image3";
            }
        }

        public string NameWithExtension
        {
            get
            {
                string ret = name;
                string extension = "bin";
                if (type != null)
                {
                    AttachmentTypeEnum t = (AttachmentTypeEnum) type;
                    switch (t)
                    {
                        case AttachmentTypeEnum.None:
                            break;
                        case AttachmentTypeEnum.GeneratedPdf_Generic:
                        case AttachmentTypeEnum.GeneratedPdf_Quote:
                        case AttachmentTypeEnum.GeneratedPdf_Invoice:
                        case AttachmentTypeEnum.AttachedPdf:
                        case AttachmentTypeEnum.TaskPdf:

                            extension = "pdf";
                            break;
                        case AttachmentTypeEnum.GeneratedXlsx_Generic:
                        case AttachmentTypeEnum.GeneratedXlsx_Quote:
                        case AttachmentTypeEnum.GeneratedXlsx_Invoice:
                        case AttachmentTypeEnum.AttachedXlsx:
                        case AttachmentTypeEnum.TaskXlsx:

                            extension = "xlsx";
                            break;
                        case AttachmentTypeEnum.AttachedXls:
                            extension = "xls";
                            break;
                        case AttachmentTypeEnum.AttachedDocx:
                        case AttachmentTypeEnum.Taskdocx:
                            extension = "docx";
                            break;
                        case AttachmentTypeEnum.AttachedDoc:
                        case AttachmentTypeEnum.Taskdoc:

                            extension = "doc";
                            break;
                        case AttachmentTypeEnum.AttachedPptx:
                        case AttachmentTypeEnum.TaskPptx:

                            extension = "pptx";
                            break;
                        case AttachmentTypeEnum.AttachedPpt:
                        case AttachmentTypeEnum.TaskPpt:

                            extension = "ppt";
                            break;
                        case AttachmentTypeEnum.AttachedUnmanagedFile:
                            break;
                        case AttachmentTypeEnum.AttachedJpeg:
                        case AttachmentTypeEnum.TaskJpeg:
                            extension = "jpg";
                            break;

                        case AttachmentTypeEnum.DeliveryNoteSign:
                            break;
                        case AttachmentTypeEnum.TaskUnmanagedFile:
                            break;
                        case AttachmentTypeEnum.TaskPng:
                        case AttachmentTypeEnum.AttachedPng:
                            extension = "png";
                            break;
                        case AttachmentTypeEnum.TaskGif:
                        case AttachmentTypeEnum.AttachedGif:

                            extension = "gif";
                            break;
                        case AttachmentTypeEnum.Taskmp4:
                            extension = "mp4";
                            break;
                        case AttachmentTypeEnum.Taskavi:
                            extension = "avi";
                            break;
                        case AttachmentTypeEnum.Taskmov:
                            extension = "mov";
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }

                }

                return ret + "." + extension;
            }
        }

        public string FilePath { get; set; }

        public string FileNameNoExtension
        {
            get
            {
                return !string.IsNullOrEmpty(file) ? file.Split(new[] { '.' })[0] : string.Empty;
            }
        }
    }

    
}
