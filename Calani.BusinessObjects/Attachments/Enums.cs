﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Attachments
{
    public enum AttachmentTypeEnum
    {
        None = 0,
        GeneratedPdf_Generic = 100,
        GeneratedPdf_Quote = 101,
        GeneratedPdf_Invoice = 102,
        AttachedPdf = 109,

        GeneratedXlsx_Generic = 200,
        GeneratedXlsx_Quote = 201,
        GeneratedXlsx_Invoice = 202,
        AttachedXlsx = 209,
        AttachedXls = 210,
        AttachedDocx = 211,
        AttachedDoc = 212,
        AttachedPptx = 213,
        AttachedPpt = 214,
        AttachedUnmanagedFile = 300,
        AttachedJpeg = 301,
        AttachedPng = 302,
        AttachedGif = 303,
        DeliveryNoteSign = 304,

        TaskUnmanagedFile = 400,
        TaskJpeg = 401,
        TaskPng = 402,
        TaskGif = 403,
        Taskdoc = 404,
        Taskdocx = 405,
        TaskPpt = 406,
        TaskPptx = 407,
        TaskXlsx = 408,
        TaskPdf = 409,
        Taskmp4 = 410,
        Taskavi = 411,
        Taskmov = 412
    }
    [Flags]
    public enum AttachmentProperties
    {
        None = 0,
        IncludeToPdf = 1

    }
}
