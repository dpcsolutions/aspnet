﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Attachments
{
    public class ProfilePictureManager
    {
        public static string DIR { get; set; }

        public long OrganizationId { get; private set; }
        public ProfilePictureManager(long organizationId)
        {
            OrganizationId = organizationId;
        }

        public DirectoryInfo GetDir()
        {
            DirectoryInfo di = new DirectoryInfo(Path.Combine(DIR, OrganizationId.ToString()));
            if (!di.Exists) di.Create();
            return di;
        }

        public FileInfo GetFilePath(long user)
        {
            string p = Path.Combine(GetDir().FullName, user.ToString() + ".png");
            return new FileInfo(p);
        }

        public bool SetPicture(long user, byte[] picture)
        {
            if (picture == null) return false;

            try
            {
                Generic.ImageManager mgr = new Generic.ImageManager();
                mgr.TargetHeight = 256;
                mgr.TargetWidth = 256;
                mgr.Crop = true;
                mgr.Init(picture);
                picture = mgr.Save(System.Drawing.Imaging.ImageFormat.Png);
            }
            catch
            {
                return false;
            }


            try
            {
                var fi = GetFilePath(user);
                System.IO.File.WriteAllBytes(fi.FullName, picture);
                return true;
            }
            catch
            {
                return false;
            }

            return false;
        }

        public byte[] GetPicture(long user)
        {
            var fi = GetFilePath(user);
            if (fi.Exists)
            {
                return System.IO.File.ReadAllBytes(fi.FullName);
            }
            return null;
        }
    }
}
