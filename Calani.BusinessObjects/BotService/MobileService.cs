﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;
using static Calani.BusinessObjects.BotService.BotService;

namespace Calani.BusinessObjects.BotService
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class BotService : IBotService
    {
        public string ClientIp()
        {
            return HttpContext.Current.Request.UserHostAddress;
        }
        public string ClientAgent()
        {
            return HttpContext.Current.Request.UserAgent;
        }

        public string getMyIP()
        {
            return ClientIp();
        }


        public MobileActionResult<List<client>> getClients()
        {
            MobileActionResult<List<client>> ret = new MobileActionResult<List<client>>();
            MobileClientManager mgr = new MobileClientManager(1);

            try
            {
                ret.data = mgr.listClients();
                ret.notAllowed = false;
                ret.rejected = false;
                ret.success = true;
            }
            catch (Exception)
            {
                ret.success = false;
            }

            ret.rejected = ret.data == null;
            return ret;
        }

        public MobileActionResult<List<resource>> getResources()
        {
            MobileActionResult<List<resource>> ret = new MobileActionResult<List<resource>>();
            MobileResourceManager mgr = new MobileResourceManager(1);

            try
            {
                ret.data = mgr.listContacts();
                ret.notAllowed = false;
                ret.rejected = false;
                ret.success = true;
            }
            catch (Exception)
            {
                ret.success = false;
            }

            ret.rejected = ret.data == null;
            return ret;
        }

        public MobileActionResult<List<availability>> GetAvailabilities(availabilityArgs args)
        {
            List<long> resourcesId = args.resourcesIds;
            long duration = args.duration == null ? 45 : (long)args.duration;
            DateTime fromDt;
            DateTime? from = null;

            if (DateTime.TryParseExact(
                                args.dateFromInUTC,
                                @"yyyy-MM-ddTHH:mm:ss.FFFZ",
                                CultureInfo.InvariantCulture,
                                DateTimeStyles.AssumeUniversal, out fromDt))
                from = fromDt;
            else
                from = DateTime.Now;

            //long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            //JobsManager mgr = new JobsManager(organizationId);
            MobileActionResult<List<availability>> ret = new MobileActionResult<List<availability>>();

            MobileAppointmentManager mgr = new MobileAppointmentManager(1);

            try
            {
                ret.data = mgr.getAvailabilities(resourcesId, duration, from);
                ret.notAllowed = false;
                ret.rejected = false;
                ret.success = true;
            }
            catch (Exception e)
            {
                ret.success = false;
            }

            ret.rejected = ret.data == null;
            return ret;
        }

        
        public MobileActionResult<long> addVisit(visitArgs args)
        {
            List<long> resourcesId = args.resourcesIds;
            DateTime fromInUTC;
            DateTime.TryParseExact(
                                args.fromDtInUTC,
                                @"yyyy-MM-ddTHH:mm:ss.FFFZ",
                                CultureInfo.InvariantCulture,
                                DateTimeStyles.AssumeUniversal, out fromInUTC);

            DateTime toInUTC;
            DateTime.TryParseExact(
                               args.toDtInUTC,
                               @"yyyy-MM-ddTHH:mm:ss.FFFZ",
                               CultureInfo.InvariantCulture,
                               DateTimeStyles.AssumeUniversal, out toInUTC);

            long projectId = args.projectId;

            MobileActionResult<long> ret = new MobileActionResult<long>();

             MobileJobManager mgr = new MobileJobManager(1);

             try
             {
                ret.data = mgr.AddVisit(resourcesId, fromInUTC, toInUTC, projectId);
                ret.notAllowed = false;
                ret.rejected = false;
                ret.success = true;
             }
             catch (Exception e)
             {
                 ret.success = false;
             }

             ret.rejected = ret.data == null;
            return ret;
        }

        public MobileActionResult<List<jobs>> getOpenJobs(string clientId)
        {
            MobileActionResult<List<jobs>> ret = new MobileActionResult<List<jobs>>();

            MobileJobManager mgr = new MobileJobManager(1);

            try
            {
                ret.data = mgr.listOpenJobs(long.Parse(clientId));
                ret.notAllowed = false;
                ret.success = true;
            }
            catch (Exception e)
            {
                ret.success = false;
            }

            ret.rejected = ret.data == null;
            return ret;
        }
    }

    public class availabilityArgs
    {
        public List<long> resourcesIds;
        public long? duration;
        public string dateFromInUTC;

        public availabilityArgs() { }
    }

    public class visitArgs
    {
        public List<long> resourcesIds;
        public string fromDtInUTC;
        public string toDtInUTC;
        public long projectId;

        public visitArgs() { }
    }

    [ServiceContract(SessionMode = SessionMode.Allowed)]
    public interface IBotService
    {

        [OperationContract]
        [WebGet(UriTemplate= "clients", ResponseFormat = WebMessageFormat.Json)]
        MobileActionResult<List<client>> getClients();

        [OperationContract]
        [WebGet(UriTemplate = "resources", ResponseFormat = WebMessageFormat.Json)]
        MobileActionResult<List<resource>> getResources();

        [OperationContract]
        [WebInvoke(UriTemplate = "availabilities", ResponseFormat = WebMessageFormat.Json)]
        MobileActionResult<List<availability>> GetAvailabilities(availabilityArgs args);

        [OperationContract]
        [WebGet(UriTemplate = "jobs/{clientId}", ResponseFormat = WebMessageFormat.Json)]
        MobileActionResult<List<jobs>> getOpenJobs(string clientId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "visit", ResponseFormat = WebMessageFormat.Json)]
        MobileActionResult<long> addVisit(visitArgs args);
    }
}
