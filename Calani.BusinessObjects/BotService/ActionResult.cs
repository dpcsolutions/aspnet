﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.BotService
{
    public class MobileActionResult<T>
    {
      
        public bool success { get; set; }
        public bool rejected { get; set; }
        public bool notAllowed { get; set; }
        public string message { get; set; }
        public T data { get; set; }

        public MobileActionResult()
        {
            this.success = false;
            this.rejected = false;
            this.notAllowed = true;
        }
    }
}
