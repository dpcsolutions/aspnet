﻿using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.BotService
{
    public class MobileClientManager : Generic.SmartCollectionManager<clients, long>
    {

        public MobileClientManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<clients> GetDbSet(Model.CalaniEntities db)
        {
            return db.clients;
        }

        internal override SmartQuery<clients> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from v in q.Context.clients
                       select v);

            if (OrganizationID > -1) q.Query = (from r in q.Query where r.organizationId == OrganizationID || r.organizationId == null select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            return q;
        }

        public List<client> listClients()
        {
            var lstClients = new List<client>();

            foreach(var c in this.List())
            {
                lstClients.Add(new client() { id = c.id, name = c.companyName });
            }

            return lstClients;
        }
    }
}
