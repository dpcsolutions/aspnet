﻿using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.BotService
{
    public class MobileResourceManager : Generic.SmartCollectionManager<contacts, long>
    {

        public MobileResourceManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<contacts> GetDbSet(Model.CalaniEntities db)
        {
            return db.contacts;
        }

        internal override SmartQuery<contacts> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from v in q.Context.contacts
                       select v);

            if (OrganizationID > -1) q.Query = (from r in q.Query where r.organizationId == OrganizationID || r.organizationId == null select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);
            q.Query = (from r in q.Query where r.type == 2 select r);

            return q;
        }

        public List<resource> listContacts()
        {
            var lstClients = new List<resource>();

            foreach(var c in this.List())
            {
                lstClients.Add(new resource() { id = c.id, fname = c.firstName, lname = c.lastName });
            }

            return lstClients;
        }
    }
}
