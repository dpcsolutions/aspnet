﻿using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.BotService
{
    public class MobileAppointmentManager : Generic.SmartCollectionManager<visits, long>
    {
        const int END_OF_DAY_HH = 18;
        const int START_OF_DAY_HH = 8;
        const int START_OF_DAY_LUNCH_HH = 12;
        const int END_OF_DAY_LUNCH_HH = 13;

        class resourceAvailabilityStatus
        {
            public long resourceId;
            public bool isAvailable;
          

            public resourceAvailabilityStatus(long id, bool isAvail)
            {
                resourceId = id;
                isAvailable = isAvail;
            }
        }

        class resourcesAvailabilityList : Dictionary<long, resourceAvailabilityStatus>
        {
            long resCount = 0;
            long totalResources = 0;

            public resourcesAvailabilityList(long totResources)
            {
               totalResources = totResources;
            }

            public bool allResourcesAreAvailable()
            {
                bool avail = resCount == totalResources;

                if(resCount == totalResources)
                {
                    foreach (var r in this)
                    {
                        avail &= r.Value.isAvailable;
                        if (avail == false) break;
                    }
                }
               
                return avail;
            }

            public void markResource(long id, bool status)
            {
                if (this.ContainsKey(id))
                    this[id].isAvailable = status;
                else
                {
                    this.Add(id, new resourceAvailabilityStatus(id, status));
                    resCount += 1;
                }
            }

            public void ResetAvailability()
            {
                foreach(var r in this)
                    this.markResource(r.Value.resourceId, true);
            }
        } 

        public MobileAppointmentManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

       internal override DbSet<visits> GetDbSet(Model.CalaniEntities db)
        {
            return db.visits;
        }

        internal override SmartQuery<visits> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from v in q.Context.visits
                       select v);

            if (OrganizationID > -1) q.Query = (from r in q.Query where r.orgId == OrganizationID || r.orgId == null select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            return q;
        }

        public List<availability> getAvailability(DateTime dtStart, DateTime dtEnd, 
            SmartQuery<visit> meetings) {
            List<availability> ret = new List<availability>();

            foreach (var c in meetings.Query.ToList())
            {
                //list of all taken time (need to filter on days only 7)
                //lstAvailabilities.Add(new client() { id = c.id, name = c.companyName });
                if (c.startDateTime >= dtStart && c.startDateTime <= dtEnd)
                {
                    //meeting starts today and might end tomorrow or later
                    // ret.Add(getAvailRec());
                }
                else if (c.startDateTime <= dtStart && c.endDateTime >= dtStart)
                {
                    //meeting started yesterday and might end tomorrow or later
                }
            }
            return ret;
        }

        private class resAvailability
        {
            public long resource;
            public DateTime dt;
            public bool available;
            public long duration;

            public resAvailability(long res, DateTime date, bool avail, long dur = 0)
            {
                resource = res;
                dt = date;
                available = avail;
                duration = dur;
            }
        }

        private class resAvailabilityComparer : Comparer<resAvailability>
        {
            public override int Compare(resAvailability x, resAvailability y)
            {
                // needs null checks
                var referenceEquals = ReferenceEquals(x, y);
                if (referenceEquals)
                {
                    return 0;
                }
                else
                    return Comparer<DateTime>.Default.Compare(x.dt, y.dt);
            }
        }

        private string toSQLString(DateTime dt)
        {
            return dt.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public List<availability> getAvailabilities(List<long> resourcesId, long duration, DateTime? fromdt)
        {
            DateTime DAY_START = DateTime.Now;
            DateTime DAY_LUNCH_START = DateTime.Now;
            DateTime DAY_LUNCH_END = DateTime.Now;
            DateTime DAY_END = DateTime.Now;

            int DAYS = 8;
             
            var lstAvailabilities = new List<availabilityInternal>();
            List<resAvailability> queuesResources = new List<resAvailability> ();

            var meetings = this.GetBaseQuery();
            var dtEndPeriod = fromdt.Value.AddDays(DAYS);

            //find all ressources involved and add them to resourcesInvolved list
            List<long> resourcesInvolved = null;

           meetings.Query = (from m in meetings.Query
                              where (m.dateEnd < dtEndPeriod && m.dateStart > fromdt) ||
                                (m.dateStart < fromdt && m.dateEnd > fromdt) ||
                                (m.dateStart > fromdt && m.dateStart < dtEndPeriod && m.dateEnd > dtEndPeriod)
                              select m);

            string sql = "SELECT calani.visits.* ";
            sql += "FROM calani.visits, calani.visits_contacts, calani.contacts ";
            sql += "where calani.visits.id = calani.visits_contacts.visitId and ";
            sql += "calani.contacts.id = calani.visits_contacts.contactId and ";
            sql += "((calani.visits.dateEnd < '{1}' && calani.visits.dateStart > '{0}') or ";
            sql += "(calani.visits.dateStart < '{0}' && calani.visits.dateEnd > '{0}') or ";
            sql += "(calani.visits.dateStart > '{0}' && calani.visits.dateStart < '{1}' &&  calani.visits.dateEnd > '{1}')) ";
            sql = String.Format(sql, new object[] { toSQLString((DateTime)fromdt), toSQLString((DateTime)dtEndPeriod) });

            if(resourcesId.Count > 0)
            {
                sql += " and (";
                for (var r = 0; r < resourcesId.Count; r ++)
                    sql += "(calani.visits_contacts.contactId = " + resourcesId[r] + (r<resourcesId.Count -1 ? ") or " : ")");
                sql += ")";
            }

            var unfilteredlist = meetings.Context.visits.SqlQuery(sql, new object[] { }).AsQueryable().Include("contacts").ToList();
        
            List<visits> filteredVisitlist = new List<visits>();

            resourcesInvolved = resourcesId.Count > 0 && unfilteredlist.Count > 0 ? new List<long>() : new List<long>(resourcesId);

            foreach (var meeting in unfilteredlist)
            {
                foreach (var resource in meeting.contacts)
                {
                    if (resourcesId.Contains(resource.id))
                    {
                        var alreadyAdded = (from v in filteredVisitlist
                                            where v.id == meeting.id
                                            select v).FirstOrDefault() != null;

                        if (!alreadyAdded)
                            filteredVisitlist.Add(meeting);

                        if (resourcesId.Count > 0 && !resourcesInvolved.Contains(resource.id))
                            resourcesInvolved.Add(resource.id);
                    }
                }
            }

            if (fromdt == null)
                fromdt = DateTime.Now;

            DAY_START = new DateTime(fromdt.Value.Year, fromdt.Value.Month, fromdt.Value.Day, START_OF_DAY_HH, 00, 00);
            DAY_LUNCH_START = new DateTime(fromdt.Value.Year, fromdt.Value.Month, fromdt.Value.Day, START_OF_DAY_LUNCH_HH, 00, 00);
            DAY_LUNCH_END = new DateTime(fromdt.Value.Year, fromdt.Value.Month, fromdt.Value.Day, END_OF_DAY_LUNCH_HH, 00, 00);
            DAY_END = new DateTime(fromdt.Value.Year, fromdt.Value.Month, fromdt.Value.Day, END_OF_DAY_HH, 00, 00);


            //handle first day (initialise days)

            if (fromdt.Value.Hour < DAY_START.Hour)
                foreach(var r in resourcesInvolved)
                    queuesResources.Add( new resAvailability(r, DAY_START, true));
       
            if (fromdt.Value.Hour < DAY_LUNCH_START.Hour)
                foreach (var r in resourcesInvolved)
                        queuesResources.Add( new resAvailability(r, DAY_LUNCH_START, false));
       
            if (fromdt.Value.Hour < DAY_LUNCH_END.Hour)
                foreach (var r in resourcesInvolved)
                        queuesResources.Add(new resAvailability(r, DAY_LUNCH_END, false));
       
            if (fromdt.Value.Hour < DAY_END.Hour)
                foreach (var r in resourcesInvolved)
                        queuesResources.Add(new resAvailability(r, DAY_END, false));

            //handle next DAYS
            for (var d = 1; d < DAYS - 1; d++)
            {
                foreach (var r in resourcesInvolved)
                {
                    queuesResources.Add(new resAvailability(r, DAY_START.AddDays(d), true));
                    queuesResources.Add(new resAvailability(r, DAY_LUNCH_START.AddDays(d), false));
                    queuesResources.Add(new resAvailability(r, DAY_LUNCH_END.AddDays(d), true));
                    queuesResources.Add(new resAvailability(r, DAY_END.AddDays(d), true));
                }
            }

            foreach (var m in filteredVisitlist)
            {
                foreach (var c in m.contacts)
                {
                    if(resourcesInvolved.Contains(c.id))
                    {
                        queuesResources.Add(new resAvailability(c.id, (DateTime)m.dateStart, false));
                        queuesResources.Add(new resAvailability(c.id, (DateTime)m.dateEnd, true));
                    }
                }
            }

            //sort list by days
            //queuesResources.sort
            DateTime? currentPointInTimeDt = null;
            resourcesAvailabilityList availability = new resourcesAvailabilityList(resourcesInvolved.Count);
            SortedSet<resAvailability> resourcesAvailableResult = new SortedSet<resAvailability>(new resAvailabilityComparer());

            var lstQueueResources = queuesResources.ToList();
            DateTime? prevPointInTimeDate = null;

            lstQueueResources.Sort(new resAvailabilityComparer());

            for (var i = 0; i < lstQueueResources.Count; i++)
            {
                var pointInTime = lstQueueResources[i];

                if (currentPointInTimeDt == null)
                {
                    currentPointInTimeDt = pointInTime.dt;
                }
                else if(pointInTime.dt != currentPointInTimeDt.Value)
                {
                    //we switch to another date slot
                    if(availability.allResourcesAreAvailable() )
                    {
                        var dur = (pointInTime.dt - currentPointInTimeDt.Value).TotalMinutes;

                        if (dur > duration && pointInTime.dt.Date == currentPointInTimeDt.Value.Date)
                            resourcesAvailableResult.Add(new resAvailability(pointInTime.resource, (DateTime)currentPointInTimeDt, true, (long)dur));
                    }

                    availability.ResetAvailability();
                    currentPointInTimeDt = pointInTime.dt;
                }
                availability.markResource(pointInTime.resource, pointInTime.available);
            }

            List<availability> ret = new List<BusinessObjects.BotService.availability>();

            foreach (var avail in resourcesAvailableResult)
            {
                var alreadyAdded = (from r in ret
                                    where r.fromDt == avail.dt.ToUniversalTime().ToString() &&
                                        r.toDt == avail.dt.AddMinutes(avail.duration).ToUniversalTime().ToString()
                                    select r).FirstOrDefault() != null;

                if(!alreadyAdded)
                    ret.Add(new BusinessObjects.BotService.availability()
                    {
                        fromDt = avail.dt.ToString("o", System.Globalization.CultureInfo.InvariantCulture).ToString(),
                        toDt = avail.dt.AddMinutes(avail.duration).ToString("O", System.Globalization.CultureInfo.InvariantCulture).ToString()
                    });
            }
             
            return ret.ToList();
        }
    }
}
