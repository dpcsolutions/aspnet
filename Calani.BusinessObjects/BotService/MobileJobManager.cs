﻿using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.BotService
{
    public class MobileJobManager : Generic.SmartCollectionManager<Model.jobs, long>
    {

        public MobileJobManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<Model.jobs> GetDbSet(Model.CalaniEntities db)
        {
            return db.jobs;
        }

        internal override SmartQuery<Model.jobs> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from v in q.Context.jobs
                       select v);

            if (OrganizationID > -1) q.Query = (from r in q.Query.Include("clients") where r.organizationId == OrganizationID || r.organizationId == null
                                                && r.dateJobDone == null && r.dateOpenJob != null
                                                select r);

            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            return q;
        }

        public List<jobs> listOpenJobs(long clientId)
        {
            var lstJobs = new List<jobs>();
            var lst = (from c in
                           this.GetBaseQuery().Query
                       where c.clientId == clientId
                       select c).ToList();

            foreach(var j in lst)
                lstJobs.Add(new jobs() { id = j.id, clientName = j.clients.companyName,
                     clientId = j.clientId, description = j.description, name = j.nb });
         
            return lstJobs;
        }

        public long AddVisit(List<long> resourcesId, DateTime from, DateTime to, long projectId)
        {
            var query = (from r in this.GetBaseQuery().Query where r.id == projectId
                         select r);

            var q = query.ToList();

            if (q.Count > 0)
            {
                //existing record 
                var job = q[0];
       
                var newVisit = new visits()
                {
                    dateStart = from,
                    dateEnd = to,
                    description = "",
                    jobId = job.id,
                    numrepeat = 0,
                    orgId = 1,
                    type = 1,
                    visitStatus = 0,
                    contacts = new List<contacts>()
                };

                this.GetBaseQuery().Context.visits.Add(newVisit);

                this.GetBaseQuery().Context.SaveChanges();

                foreach (var r in resourcesId)
                {
                    var res = (from c in this.GetBaseQuery().Context.contacts
                               where c.recordStatus == 0 &&
                                   c.organizationId == OrganizationID &&
                                   c.id == r
                               select c).FirstOrDefault();

                    newVisit.contacts.Add(res);
                }

                this.GetBaseQuery().Context.SaveChanges();

                return newVisit.id;
            }
            else
               throw new Exception("Project does not exist") ;
        }
    }
}
