﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.BotService
{
    public class client
    {
        public string name { get; set; }
        public long id { get; set; }

        public client(Model.clients client)
        {
            name = client.companyName;
            id = client.id;
        }

        public client() { }
    }

    public class jobs
    {
        public string name { get; set; }
        public string description { get; set; }
        public long id { get; set; }
        public long clientId { get; set; }
        public string clientName { get; set; }

        public jobs(Model.jobs job)
        {
            name = job.nb;
            description = job.description;
            id = job.id;
            clientId = job.clientId;
            clientName = job.clients.companyName;
        }

        public jobs() { }
    }

    public class resource
    {
        public string fname { get; set; }
        public string lname { get; set; }
        public long id { get; set; }

        public resource(Model.contacts employee)
        {
            fname = employee.firstName;
            lname = employee.lastName;
            id = employee.id;
        }

        public resource() { }
    }

    public class availabilityInternal
    {
        public String fromDt { get; set; }
        public String toDt { get; set; }
        public long resourceId { get; set; }
    }

    public class availability
    {
        public String fromDt { get; set; }
        public String toDt { get; set; }
    }

    public class visit
    {
        public string clientName { get; set; }
        public string contactName { get; set; }
        public long clientId { get; set; }
        public string jobNumber { get; set; }
        public string jobDescription { get; set; }
        public long id { get; set; }
        public DateTime startDateTime { get; set; }
        public DateTime endDateTime { get; set; }
        public List<resource> resources { get; set; }

        public visit(Model.visits v)
        {
            clientName = v.jobs.clients.companyName;
            contactName = v.jobs.contacts.lastName;
            clientId = v.jobs.clientId;
            jobNumber = v.jobs.nb;
            jobDescription = v.jobs.description;
            id = v.id;
            startDateTime = (DateTime)v.dateStart;
            endDateTime = (DateTime)v.dateEnd;

            resources = new List<resource>();

            foreach(var c in v.contacts)
            {
                resources.Add(new resource() {  fname = c.firstName, lname = c.lastName, id = c.id});
            }
        }
    }
}

