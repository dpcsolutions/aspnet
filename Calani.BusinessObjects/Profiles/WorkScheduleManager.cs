﻿using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Calani.BusinessObjects.Calendars;

namespace Calani.BusinessObjects.WorkSchedulers
{
    public class WorkScheduleManager : Generic.SmartCollectionManager<Model.work_schedule_profiles, long>
    {

        public WorkScheduleManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<Model.work_schedule_profiles> GetDbSet(Model.CalaniEntities db)
        {
            return db.work_schedule_profiles;
        }

        internal override SmartQuery<Model.work_schedule_profiles> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = from r in q.Context.work_schedule_profiles where r.id == id select r;
            return q;
        }

        internal override long GetRecordOrganization(Model.work_schedule_profiles record)
        {
            return record.organization_id;
        }

        /// <summary>
        /// GetBaseQuery
        /// </summary>
        /// <returns>query of profiles</returns>
        internal override SmartQuery<Model.work_schedule_profiles> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = from r in q.Context.work_schedule_profiles select r;
            if (OrganizationID > -1)
            {
                q.Query = from r in q.Query where r.organization_id == OrganizationID select r;
            }

            if (!IgnoreRecordStatus)
            {
                q.Query = from r in q.Query where (r.recordStatus == null || r.recordStatus == 0) select r;
            }

            return q;
        }


        internal override void BeforeAction(SmartAction<Model.work_schedule_profiles> action)
        {
            if (action.Type == SmartActionType.Update && action.Record != null && action.Record.updated_at == null)
            {
                action.Record.updated_at = DateTime.UtcNow;
            }
            base.BeforeAction(action);
        }


        public List<ScheduleDto> GetScheduleDtos()
        {
            var q = GetBaseQuery();

            var data = q.Query.ToList();

            var dtos =  (from r in data select r.ToDto()).ToList();

            foreach (var dto in dtos)
            {
                var qc = base.GetBaseQuery();

                var calendars = (from pc in qc.Context.work_schedule_profiles_calendars where pc.profile_id == dto.Id
                    join c in qc.Context.calendars on pc.calendar_id equals c.id into sr
                    from x in sr.DefaultIfEmpty()
                    select x).ToList();
                
                dto.Calendars = calendars.Select(c => c.ToDto()).ToList();
            }

            return dtos;
        }

        public void DeleteProfile(long calendarId, long userId )
        {
            base.Delete(calendarId);
        }

        public long DuplicateProfile(long id)
        {
            var q = GetById(id);
            var c = q.Query.FirstOrDefault();
           
            if (c != null)
            {
                var length = (c.name + "(Duplicated)").Length;
                var name = (c.name + "(Duplicated)").Substring(0, Math.Min(length, 45) );//name can't be longer than 45 symbols;
                var item = new work_schedule_profiles()
                {
                    name = name,
                    code = c.code,
                    //calendar_id = c.calendar_Id,
                    organization_id = c.organization_id,
                };

                var res = Add(item);

                if (res.Success)
                {
                    var mngrRec = new WorkScheduleRecManager(c.organization_id);
                    var recs = mngrRec.GetScheduleRecordsDto(id);
                    foreach (var rec in recs)
                    {
                        var r = new work_schedule_profiles_rec()
                        {
                            profile_id = res.Record.id,
                            color = rec.Color,
                            name = rec.Name,
                            start = rec.Start,
                            end = rec.End,
                            rate = rec.Rate,
                            mo = rec.Mo,
                            tu = rec.Tu,
                            we = rec.We,
                            th = rec.Th,
                            fr = rec.Fr,
                            sa = rec.Sa,
                            su = rec.Su,
                            type = (int)rec.Type,
                            recordStatus = rec.RecordStatus,
                            organization_id = rec.OrganizationId,
                        };
                        mngrRec.Add(r);
                    }

                    return res.Record.id;
                }
            }
           

            return 0;
        }

        public void UpdateProfileCalendarsSelection(long currentProfileId, IEnumerable<long> calItems)
        {
            var pcMngr = new WorkScheduleCalendarsManager(OrganizationID);
            pcMngr.DeleteSelectionForProfile(currentProfileId);


            foreach (var item in calItems)
            {
                var pc = new work_schedule_profiles_calendars();
                pc.calendar_id = item;
                pc.profile_id = currentProfileId;

                pcMngr.Add(pc);
            }
        }
        
    }


    public class WorkScheduleCalendarsManager : Generic.SmartCollectionManager<Model.work_schedule_profiles_calendars, long>
    {

        public WorkScheduleCalendarsManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<Model.work_schedule_profiles_calendars> GetDbSet(Model.CalaniEntities db)
        {
            return db.work_schedule_profiles_calendars;
        }

        internal override SmartQuery<Model.work_schedule_profiles_calendars> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = from r in q.Context.work_schedule_profiles_calendars where r.id == id select r;
            return q;
        }


        /// <summary>
        /// GetBaseQuery
        /// </summary>
        /// <returns>query of work_schedule_profiles_calendars</returns>
        internal override SmartQuery<Model.work_schedule_profiles_calendars> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = from r in q.Context.work_schedule_profiles_calendars select r;
            q.Query = from r in q.Query select r;

            return q;
        }


        internal override void BeforeAction(SmartAction<work_schedule_profiles_calendars> action)
        {
            if (action.Type == SmartActionType.Update && action.Record != null && action.Record.updated_at == null)
            {
                action.Record.updated_at = DateTime.UtcNow;
            }
            base.BeforeAction(action);
        }


        public void DeleteSelectionForProfile(long currentProfileId)
        {
            var q = GetBaseQuery();
            var currentSelection = (from pc in q.Context.work_schedule_profiles_calendars
                where pc.profile_id == currentProfileId
                select pc).ToArray();

            foreach (var item in currentSelection)
            {
                base.Delete(item.id, DeleteMethod.DeleteRecord);
            }

        }
    }
}
