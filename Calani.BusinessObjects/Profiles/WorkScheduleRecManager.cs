﻿using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Calani.BusinessObjects.Calendars;

namespace Calani.BusinessObjects.WorkSchedulers
{
    public class WorkScheduleRecManager : Generic.SmartCollectionManager<Model.work_schedule_profiles_rec, long>
    {

        public WorkScheduleRecManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<Model.work_schedule_profiles_rec> GetDbSet(Model.CalaniEntities db)
        {
            return db.work_schedule_profiles_rec;
        }

        internal override SmartQuery<Model.work_schedule_profiles_rec> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = from r in q.Context.work_schedule_profiles_rec where r.id == id select r;
            return q;
        }


        /// <summary>
        /// GetBaseQuery
        /// </summary>
        /// <returns>query of profiles</returns>
        internal override SmartQuery<Model.work_schedule_profiles_rec> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from r in q.Context.work_schedule_profiles_rec select r);

            if (OrganizationID > -1)
                q.Query = (from r in q.Query where r.organization_id == OrganizationID select r);
            // base
            q.Query = from r in q.Query where (r.recordStatus == null || r.recordStatus == 0) select r;

            return q;
        }


        internal override void BeforeAction(SmartAction<work_schedule_profiles_rec> action)
        {
            if (action.Type == SmartActionType.Update && action.Record != null && action.Record.updated_at == null)
            {
                action.Record.updated_at = DateTime.UtcNow;
            }
            base.BeforeAction(action);
        }
        
        public List<ScheduleRecordDto> GetScheduleRecordsDto(long? id)
        {
            var data = GetScheduleRecords(id);
            return (from r in data select r.ToDto()).ToList();
        }

        public List<work_schedule_profiles_rec> GetScheduleRecords(long? id)
        {
            var q = GetBaseQuery();
            if (id.HasValue)
            {
                q.Query = from r in q.Query where r.profile_id == id select r;
            }
            else/*predefined records without schedule*/
                q.Query = from r in q.Query where r.profile_id == null select r;
            var data = q.Query.ToList();

            return data.ToList();
        }

        public List<ProfileCalendarEventDescription> ConvertToCalendarEvents(List<ScheduleRecordDto> recData)
        {
            var models = new List<ProfileCalendarEventDescription>();

            if (recData != null)
            {
                foreach (var dto in recData)
                {
                    models.Add(dto.ToEventModel());
                }
            }

            return models;
        }
    }
}
