﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.TimeManagement;

namespace Calani.BusinessObjects.Calendars
{
    public class ScheduleDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        public DateTime? UpdatedAt { get; set; }
        public long? UpdatedBy { get; set; }
        public int? RecordStatus { get; set; }
        public long OrganizationId { get; set; }
        public List<CalendarDto> Calendars { get; set; }
        public bool AssignedToOrg { get; set; }
    }

    public class ScheduleRecordDto
    {
        public long OrganizationId { get; set; }
        public long Id { get; set; }
        public long? ProfileId { get; set; }
        public string Color { get; set; }
        public string Name { get; set; }
        public string Start { get; set; }
        public string End { get; set; }
        public int Rate { get; set; }
        public bool Mo { get; set; }
        public bool Tu { get; set; }
        public bool We { get; set; }
        public bool Th { get; set; }
        public bool Fr { get; set; }
        public bool Sa { get; set; }
        public bool Su { get; set; }
        public TimeCategoryType Type { get; set; }
        public bool Holidays { get { return Type == TimeCategoryType.ScheduleHoliday; } }



        public string Hours
        {
            get
            {
                var res = Start + (String.IsNullOrWhiteSpace(End) ? String.Empty : (" - " + End));
                return res;
            }
        }

        public int RecordStatus { get; set; }

      
    }


    public class ProfileCalendarEventDescription
    {
        public string start;
        public string end;
        public long id { get; set; }
        
        public string[] resourceIds { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public string color { get; set; }
        public string title { get; set; }
        
    }

    public static class ProfilesExtentions
    {
        public static ScheduleDto ToDto(this Model.work_schedule_profiles model)
        {
            if (model == null)
            {
                return null;
            }
            var dto = new ScheduleDto();

            dto.Id = model.id;
            dto.Name = model.name;
            dto.Code = model.code;
            
            dto.OrganizationId = model.organization_id;
            dto.RecordStatus = model.recordStatus;
            dto.UpdatedBy = model.updated_by;
            dto.UpdatedAt = model.updated_at;
            dto.AssignedToOrg = model.organizations.Any();

            return dto;
        }

        public static ScheduleRecordDto ToDto(this work_schedule_profiles_rec rec)
        {
            if (rec == null)
            {
                return null;
            }
            var dto = new ScheduleRecordDto();

            dto.Id = rec.id;
            dto.OrganizationId = rec.organization_id;
            dto.ProfileId = rec.profile_id;
            dto.Name = rec.name;
            dto.Start = rec.start;
            dto.End = rec.end;
            dto.Color = rec.color;
            dto.Rate = rec.rate;
            dto.Mo = rec.mo;
            dto.Tu = rec.tu;
            dto.We = rec.we;
            dto.Th = rec.th;
            dto.Fr = rec.fr;
            dto.Sa = rec.sa;
            dto.Su = rec.su;
            dto.Type = (TimeCategoryType)rec.type;

            dto.RecordStatus = rec.recordStatus??0;

            return dto;
        }

        public static ProfileCalendarEventDescription ToEventModel(this ScheduleRecordDto dto)
        {

            var model = new ProfileCalendarEventDescription();
           

            model.id = dto.Id;
            model.color = dto.Color;
            model.title = dto.Name;
            model.startTime = dto.Start;
            model.endTime = dto.End;
            model.start = DateTime.Now.Date.ToString("yyyy-MM-ddT")+ dto.Start;
            model.end = DateTime.Now.Date.ToString("yyyy-MM-ddT") + dto.End;

            var days = new List<string>();
            if (dto.Type == TimeCategoryType.ScheduleHoliday)
                days.Add("cal");
            else
            {
                if (dto.Mo)
                {
                    days.Add("mo");
                }

                if (dto.Tu)
                {

                    days.Add("tu");
                }

                if (dto.We)
                {

                    days.Add("we");
                }

                if (dto.Th)
                {
                    days.Add("th");
                }

                if (dto.Fr)
                {
                    days.Add("fr");
                }

                if (dto.Sa)
                {
                    days.Add("sa");
                }

                if (dto.Su)
                {

                    days.Add("su");
                }
            }
            

            model.resourceIds = days.ToArray();

            return model;
        }
    }
}
