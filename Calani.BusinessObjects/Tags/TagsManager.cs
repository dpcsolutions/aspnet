﻿using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Tags
{
    public class TagsManager : Generic.SmartCollectionManager<Model.tags, long>
    {
        public TagsManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        public long Count()
        {
            return GetBaseQuery().Query.LongCount();
        }

        public List<tags> GetLasts(int n)
        {
            var q = GetBaseQuery().Query;
            return (from r in q orderby r.id descending select r).Take(n).ToList();
        }

        internal override DbSet<tags> GetDbSet(Model.CalaniEntities db)
        {
            return db.tags;
        }

        internal override SmartQuery<tags> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.tags where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(tags record)
        {
            if (record != null && record.organizationId != null) return record.organizationId.Value;
            return -1;
        }
       
        internal override SmartQuery<tags> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from r in q.Context.tags select r);
            if (OrganizationID > -1) q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == (int)RecordStatus.Active select r);

            // default sorting
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.name select r);



            return q;
        }

        public List<tags> CreateTagsIfNotExist(List<string> tags, int module, string color)
        {
            if (tags == null) return new List<tags>();
            CalaniEntities db = new CalaniEntities();
            var TagNames = (from t in db.tags select t).Where(c => c.organizationId == OrganizationID).Select(e=>e.name);
            foreach (var item in tags)
            {
                if (!TagNames.Contains(item))
                {
                    this.Add(new tags
                    {
                        recordStatus = (int)RecordStatus.Active,
                        name = item,
                        organizationId = OrganizationID,
                        module = module,
                        type= (int)TagTypes.Private,
                        color=color
                    });
                }
            }

            return (from t in db.tags select t).Where(c => c.organizationId == OrganizationID && tags.Contains(c.name)).ToList();
        }

        public List<tags> GetTagsByType(int module)
        {
            CalaniEntities db = new CalaniEntities();
            return (from t in db.tags select t).Where(t => t.organizationId == OrganizationID && t.recordStatus == (int)RecordStatus.Active && (t.module == module || t.type == 2)).ToList();
        }

        public bool DeleteByTagName(long tagName)
        {
            try
            {
                CalaniEntities db = new CalaniEntities();
                var Tags = (from tag in db.tags select tag).Where(t => t.id == tagName).ToList();
                db.tags.RemoveRange(Tags);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
            
        }
    }
}
