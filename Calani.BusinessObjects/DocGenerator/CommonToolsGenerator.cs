﻿using Calani.BusinessObjects.Contacts.Models;
using Calani.BusinessObjects.Model;
using GemBox.Spreadsheet;
using System;
using System.Text;

namespace Calani.BusinessObjects.DocGenerator
{
    public class CommonToolsGenerator
    {
        public static void BindYourCustomer(ExcelWorksheet ws, clients cust, addresses custAddr, contacts custContact, 
            bool showContactInfoInvoices = false)
        {
            var rowIdx = 8;
            var range = ws.Cells.GetSubrangeAbsolute(rowIdx, 6, rowIdx, 9);
            range.Merged = true;
            range.Style.WrapText = true;
            range.Style.VerticalAlignment = VerticalAlignmentStyle.Top;
            range.Style.Font.Weight = ExcelFont.BoldWeight;
            range.Value = cust.companyName?.Trim();
            ws.Rows[rowIdx].AutoFit(true);

            StringBuilder sb = new StringBuilder();

            if (custContact != null)
            {
                custContact.firstName = custContact?.firstName?.Trim();
                custContact.lastName = custContact?.lastName?.Trim();
                if (!String.IsNullOrWhiteSpace(custContact.lastName) && custContact.lastName.StartsWith("(") &&  custContact.lastName.EndsWith(")") || !showContactInfoInvoices)
                {
                    // no name...
                }
                else
                {
                    string contact = (custContact?.firstName?.Trim() + " " + custContact?.lastName.Trim()).Trim();
                    if (contact.ToLower().Equals(cust.companyName?.Trim().ToLower()))
                    {
                        // same name...
                    }
                    else
                    {
                        sb.AppendLine(contact);
                    }
                }
            }

            if (custAddr != null)
            {
                var address = new AddressDto(custAddr);

                if (String.IsNullOrEmpty(address.Street))
                {
                    sb.Append(address.Caption);
                }
                else
                {
                    if (!String.IsNullOrEmpty(address.Caption))
                    {
                        sb.AppendLine(address.Caption);
                    }

                    if (!String.IsNullOrEmpty(address.Nb))
                    {
                        sb.AppendLine(address.Street + " " + address.Nb);
                    }
                    else
                    {
                        sb.AppendLine(address.Street);
                    }
                }

                if (!String.IsNullOrEmpty(address.Street2))
                {
                    sb.AppendLine(address.Street2);
                }

                sb.AppendLine((address.Npa + " " + address.City).Trim());

                if (!String.IsNullOrEmpty(address.Country))
                {
                    sb.AppendLine(address.Country);
                }
            }

            ws.Cells["G10"].Value = sb.ToString();
        }
    }
}
