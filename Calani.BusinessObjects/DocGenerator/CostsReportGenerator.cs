﻿using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Calani.BusinessObjects.DocGenerator
{
    public class CostsReportGenerator
    {
        public static string ExpensesDir { get; set; }

        System.Globalization.CultureInfo _culture;
        long _userId;
        long _organizationId;

        public int Year { get; set; }
        public int Month { get; set; }
        public long? Employee { get; set; }

        public bool ExcelMode { get; set; }


        List<MemoryStream> _ms = null;

        

        public CostsReportGenerator(System.Globalization.CultureInfo culture, long userId, long organizationId)
        {
            _culture = culture;
            _userId = userId;
            _organizationId = organizationId;

            ExcelMode = false;
        }
        private string GetResource(string n)
        {
            if (SharedResource.Resource == null) return n;
            return SharedResource.Resource.GetString(n, _culture);
        }

        public byte[] GenerateDocument()
        {
            var margin = 0.5;
           _ms = new List<MemoryStream>();

            SpreadsheetInfo.SetLicense(GemboxAPI.SpreadsheetLicense);

            Model.CalaniEntities db = new Model.CalaniEntities();

            DateTime dtFrom = new DateTime(Year, Month, 1);
            DateTime dtTo = dtFrom.AddMonths(1);

            var q = (from r in db.expenses.Include("contacts")
                     where r.date >= dtFrom
                     && r.date < dtTo
                     && r.contacts.organizationId == _organizationId
                     && r.approvedmount != null && r.approvedmount > 0
                     && r.recordStatus == 0
                     select r);

            if(Employee != null)
            {
                q = (from r in q where r.employeeId == Employee.Value select r);
            }

            var records = q.ToList();

            ExcelFile ef = new ExcelFile();

            var coverSheet = ef.Worksheets.Add("Cover");
            coverSheet.PrintOptions.Portrait = true;

            //coverSheet.PrintOptions.LeftMargin = margin;
            // coverSheet.PrintOptions.RightMargin = margin;

            coverSheet.Cells["A20"].Value = GetResource("Expenses_Report").ToUpper();
            coverSheet.Cells["A20"].Style.Font.Size = 300;
            coverSheet.Cells["A20"].Style.Font.Weight = ExcelFont.BoldWeight;
            coverSheet.Cells["A20"].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            coverSheet.Cells.GetSubrange("A20", "I20").Merged = true;
            coverSheet.Cells["A21"].Value = new DateTime(Year, Month, 1).ToString("MMMMM yyyy", _culture).ToUpper();
            coverSheet.Cells["A21"].Style.Font.Size = 300;
            coverSheet.Cells["A21"].Style.Font.Weight = ExcelFont.BoldWeight;
            coverSheet.Cells["A21"].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
            coverSheet.Cells.GetSubrange("A21", "I21").Merged = true;

            if (Employee != null)
            {
                Calani.BusinessObjects.Contacts.EmployeesManager mgr = new Contacts.EmployeesManager(_organizationId);
                var emp = mgr.Get(Employee.Value);
                if (String.IsNullOrWhiteSpace(emp.lastName)) emp.lastName = "";
                if (String.IsNullOrWhiteSpace(emp.firstName)) emp.firstName = "";
                coverSheet.Cells["A22"].Value = (emp.lastName + " " + emp.firstName).Trim();
                coverSheet.Cells["A22"].Style.Font.Size = 300;
                coverSheet.Cells["A22"].Style.Font.Weight = ExcelFont.BoldWeight;
                coverSheet.Cells["A22"].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
                coverSheet.Cells.GetSubrange("A22", "I22").Merged = true;

            }

            var org = (from r in db.organizations where r.id == _organizationId select r).FirstOrDefault();
            if (org.logo != null && org.logo.Length > 5)
            {
                try
                {
                    Calani.BusinessObjects.Generic.ImageManager mgr = new Generic.ImageManager();
                    mgr.MaxSide = 130;
                    mgr.Init(org.logo);
                    byte[] imgdata = mgr.Save(System.Drawing.Imaging.ImageFormat.Png);


                    MemoryStream imgms = new MemoryStream(imgdata);
                    var img = System.Drawing.Image.FromStream(imgms);

                    //MemoryStream msLogo = new MemoryStream(org.logo);
                    //_ms.Add(msLogo);
                    //coverSheet.Pictures.Add(msLogo, ExcelPictureFormat.Png, 0, 0, 130, 130, LengthUnit.Pixel);
                    coverSheet.Pictures.Add(imgms, ExcelPictureFormat.Png, 0, 0, 130, 130, LengthUnit.Pixel);
                }
                catch { }
            }

            coverSheet.HeadersFooters.FirstPage.Footer.RightSection.Append("Page ").Append(HeaderFooterFieldType.PageNumber).Append(" of ").Append(HeaderFooterFieldType.NumberOfPages);

            Calani.BusinessObjects.Currency.CurrenciesManager currMgr = new Calani.BusinessObjects.Currency.CurrenciesManager(_organizationId);
            var currencies = currMgr.List();
            int counter = 1;
            foreach (var record in records)
            {
                var sheet = ef.Worksheets.Add(counter.ToString());
                sheet.PrintOptions.Portrait = true;
                // sheet.PrintOptions.LeftMargin = margin;
                // sheet.PrintOptions.RightMargin = margin;

                SheetHeaderFooter headerFooter = sheet.HeadersFooters;
                headerFooter.FirstPage.Footer.RightSection.Append("Page ").Append(HeaderFooterFieldType.PageNumber).Append(" of ").Append(HeaderFooterFieldType.NumberOfPages);

                sheet.Cells["A1"].Value = GetResource("Date") + " : ";
                sheet.Cells["A1"].Style.Font.Weight = ExcelFont.BoldWeight;
                if (record.date != null) sheet.Cells["C1"].Value = record.date.Value.ToString("dd/MM/yyyy");
                sheet.Cells.GetSubrange("A1", "B1").Merged = true;
                sheet.Cells.GetSubrange("C1", "H1").Merged = true;
                sheet.Cells["C1"].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                //sheet.Cells["A1"].Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0,0,0), LineStyle.Thin);
                //sheet.Cells["C1"].Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0,0,0), LineStyle.Thin);

                sheet.Cells["A2"].Value = GetResource("Employee") + " : ";
                sheet.Cells["A2"].Style.Font.Weight = ExcelFont.BoldWeight;
                sheet.Cells["C2"].Value = record.contacts.lastName + " " + record.contacts.firstName;
                sheet.Cells.GetSubrange("A2", "B2").Merged = true;
                sheet.Cells.GetSubrange("C2", "H2").Merged = true;
                sheet.Cells["C2"].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                //sheet.Cells["A2"].Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
                //sheet.Cells["C2"].Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);

                sheet.Cells["A3"].Value =  GetResource("ApprovedAmount") + " :";
                sheet.Cells["A3"].Style.Font.Weight = ExcelFont.BoldWeight;
                sheet.Cells["A3"].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;

                var curr = currencies.FirstOrDefault(c => c.currency1 == record.currency);
                var symbol = curr != null ? curr.symbol : org.currency;
                sheet.Cells["C3"].Value = String.Format("{0} {1}", record.approvedmount, symbol);

                sheet.Cells.GetSubrange("A3", "B3").Merged = true;
                sheet.Cells.GetSubrange("C3", "H3").Merged = true;
                sheet.Cells["C3"].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                //sheet.Cells["A3"].Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
                //sheet.Cells["C3"].Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);

                sheet.Cells["A4"].Value = GetResource("Description") + " :";
                sheet.Cells["A4"].Style.Font.Weight = ExcelFont.BoldWeight;
                sheet.Cells["C4"].Value = record.description;
                sheet.Cells.GetSubrange("A4", "B4").Merged = true;
                sheet.Cells.GetSubrange("C4", "H4").Merged = true;
                sheet.Cells["C4"].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                //sheet.Cells["A4"].Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
                //sheet.Cells["C4"].Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);

                SetData(5, GetResource("PayType"), GetResource("PayType" + record.pay_type.type));
                SetData(6, GetResource("VAT81"), record.vat1);
                SetData(7, GetResource("VAT38"), record.vat2);
                SetData(8, GetResource("VAT26"), record.vat3);

                void SetData(int rowNo, string description, object value)
                {
                    sheet.Cells["A" + rowNo].Value = description + " :";
                    sheet.Cells["A" + rowNo].Style.Font.Weight = ExcelFont.BoldWeight;
                    sheet.Cells["C" + rowNo].Value = value;
                    sheet.Cells.GetSubrange("A" + rowNo, "B" + rowNo).Merged = true;
                    sheet.Cells.GetSubrange("C" + rowNo, "H" + rowNo).Merged = true;
                    sheet.Cells["C" + rowNo].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                }

                string file = Path.Combine(ExpensesDir, _organizationId.ToString(), record.id.ToString() + ".jpg");

                if (File.Exists(file))
                {
                    byte[] imageFile = System.IO.File.ReadAllBytes(file);

                    var mgr = new Generic.ImageManager();
                    mgr.MaxSide = 600;
                    mgr.Init(imageFile);

                    byte[] imgdata = mgr.Save(System.Drawing.Imaging.ImageFormat.Jpeg);
                    
                    MemoryStream imgms = new MemoryStream(imgdata);
                    var img = System.Drawing.Image.FromStream(imgms);

                    double w = img.Width;
                    double h = img.Height;

                    if (w > 10)
                    {
                        sheet.Pictures.Add(imgms, ExcelPictureFormat.Png, "A9", w, h, LengthUnit.Pixel);
                        _ms.Add(imgms);
                    }
                }

                counter++;
            }


            var endingSheet = ef.Worksheets.Add("Ending");
            endingSheet.PrintOptions.Portrait = false;
           // endingSheet.PrintOptions.LeftMargin = margin;
           // endingSheet.PrintOptions.RightMargin = margin;

            endingSheet.Cells["A1"].Value = GetResource("Summary") + " - " + new DateTime(Year, Month, 1).ToString("MMMMM yyyy", _culture).ToUpper();
            if (Employee != null) endingSheet.Cells["A1"].Value += " - " + coverSheet.Cells["A22"].Value;


            endingSheet.Cells.GetSubrange("A1", "J1").Merged = true;

            endingSheet.Cells["A2"].Value = GetResource("Page");
            endingSheet.Cells["C2"].Value = GetResource("Employee");
            endingSheet.Cells["E2"].Value = GetResource("Amount");
            endingSheet.Cells["E2"].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

            endingSheet.Cells["F2"].Value = GetResource("Date");
            endingSheet.Cells["G2"].Value = GetResource("Description");
            endingSheet.Cells["K2"].Value = GetResource("PayType");
            endingSheet.Cells["L2"].Value = GetResource("VAT81");
            endingSheet.Cells["M2"].Value = GetResource("VAT38");
            endingSheet.Cells["N2"].Value = GetResource("VAT26");


            endingSheet.Cells.GetSubrange("B2", "C2").Merged = true;
            endingSheet.Cells.GetSubrange("D2", "E2").Merged = true;
            //endingSheet.Cells.GetSubrange("F2", "G2").Merged = true;
            endingSheet.Cells.GetSubrange("G2", "J2").Merged = true;
            endingSheet.Cells.GetSubrange("A2").Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
            endingSheet.Cells.GetSubrange("B2", "C2").Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
            endingSheet.Cells.GetSubrange("D2", "I2").Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
            endingSheet.Cells.GetSubrange("F2").Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
            endingSheet.Cells.GetSubrange("G2", "J2").Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
            endingSheet.Cells.GetSubrange("K2", "L2").Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
            endingSheet.Cells.GetSubrange("L2", "M2").Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
            endingSheet.Cells.GetSubrange("M2", "N2").Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);

            for (int i = 0; i < records.Count; i++)
            {
                int x = i + 3;
                endingSheet.Cells["A" + x].Value = i + 2;
                endingSheet.Cells["A" + x].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                endingSheet.Cells["C" + x].Value = records[i].contacts.lastName + " " + records[i].contacts.firstName;

                var curr = currencies.FirstOrDefault(c => c.currency1 == records[i].currency);
                var symbol = curr != null ? curr.symbol : org.currency;
                endingSheet.Cells["E" + x].Value = String.Format("{0} {1}", records[i].approvedmount, symbol);
                endingSheet.Cells["E" + x].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;


                if (records[i].date != null) 
                    endingSheet.Cells["F" + x].Value = records[i].date.Value.ToString("dd/MM/yyyy");

                endingSheet.Cells["G" + x].Value = records[i].description;
                endingSheet.Cells["K" + x].Value = GetResource("PayType" + records[i].pay_type.type);
                endingSheet.Cells["L" + x].Value = records[i].vat1;
                endingSheet.Cells["M" + x].Value = records[i].vat2;
                endingSheet.Cells["N" + x].Value = records[i].vat3;

                endingSheet.Cells.GetSubrange("B" + x, "C" + x).Merged = true;
                endingSheet.Cells.GetSubrange("D" + x, "E" + x).Merged = true;
                endingSheet.Cells.GetSubrange("G" + x, "J" + x).Merged = true;
                endingSheet.Cells.GetSubrange("A" + x).Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
                endingSheet.Cells.GetSubrange("B" + x, "C" + x).Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
                endingSheet.Cells.GetSubrange("D" + x, "E" + x).Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
                endingSheet.Cells.GetSubrange("F" + x).Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
                endingSheet.Cells.GetSubrange("G" + x, "J" + x).Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
                endingSheet.Cells.GetSubrange("G" + x, "J" + x).Style.WrapText = true;

                endingSheet.Cells.GetSubrange("K" + x, "L" + x).Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
                endingSheet.Cells.GetSubrange("L" + x, "M" + x).Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
                endingSheet.Cells.GetSubrange("M" + x, "N" + x).Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);

            }

            var scaling = _culture.Equals(CultureInfo.GetCultureInfo("fr")) ? 1.0 : 1.1;
            int columnCount = endingSheet.CalculateMaxUsedColumns();
            for (int i = 0; i < columnCount; i++)
                endingSheet.Columns[i].AutoFit(scaling, endingSheet.Rows[0], endingSheet.Rows[endingSheet.Rows.Count - 1]);


            

            endingSheet.HeadersFooters.FirstPage.Footer.RightSection.Append("Page ").Append(HeaderFooterFieldType.PageNumber).Append(" of ").Append(HeaderFooterFieldType.NumberOfPages);
            endingSheet.HeadersFooters.DefaultPage.Footer.RightSection.Append("Page ").Append(HeaderFooterFieldType.PageNumber).Append(" of ").Append(HeaderFooterFieldType.NumberOfPages);

            MemoryStream ms = new MemoryStream();
            if (ExcelMode)
            {
                ef.Save(ms, SaveOptions.XlsxDefault);
            }
            else
            {
                var options = SaveOptions.PdfDefault;

                options.SelectionType = SelectionType.EntireFile;

                ef.Save(ms, options);
            }
            byte[] ret = ms.ToArray();
            foreach (var item in _ms)
            {
                item.Dispose();
            }
            ms.Dispose();

            _ms.Clear();


            return ret;
        }


        
    }
}
