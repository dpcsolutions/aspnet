﻿using log4net;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Calani.BusinessObjects.DocGenerator
{
    public class QrBvrPdfGenerator : PdfGenerator
    {
        public string BvrPdf { get; set; }

        public string ToIban { get; set; }
        public string ToLine1 { get; set; }
        public string ToLine2 { get; set; }
        public string ToLine3 { get; set; }
        public string ToLine4 { get; set; }
        public string ToCountryCode { get; set; }
        public double Amount { get; set; }
        public string Currency { get; set; }
        public string RefNumberBank { get; set; }
        public string RefNumberNum { get; set; }
        public string Label { get; set; }

        public string FromLine1 { get; set; }
        public string FromLine2 { get; set; }
        public string FromLine3 { get; set; }
        public string FromCountryCode { get; set; }

        public int TopMargin { get; set; }
        public int LeftMargin { get; set; }
        public bool DebugRectangles { get; set; }

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void Init(Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr, string directory, bool both)
        {
            if (!both)
            {
                BvrPdf = System.IO.Path.Combine(directory, "bvr-qr.pdf");
                TopMargin = 0;
            }
            else
            {
                BvrPdf = System.IO.Path.Combine(directory, "bvr-qr-up.pdf");
                TopMargin = -544;
            }

            ToLine1 = mgr.CompanyName;
            ToLine2 = mgr.AdrStreet + " " + mgr.AdrStreetNb;
            ToLine3 = mgr.AdrZipCode + " " + mgr.AdrCity;
            ToCountryCode = "CH";
            ToIban = mgr.BankAccount1_Iban;
            
            RefNumberBank = mgr.BvrSenderBank;

        }


        public byte[] MergeOnLastPage(byte[] bin1)
        {
            if (String.IsNullOrWhiteSpace(FromCountryCode)) FromCountryCode = "CH";
            if (String.IsNullOrWhiteSpace(ToCountryCode)) ToCountryCode = "CH";

            System.IO.MemoryStream ms1 = null;
            if (bin1 != null)
            {
                //byte[] bin1 = System.IO.File.ReadAllBytes(file1);
                ms1 = new System.IO.MemoryStream(bin1);
            }

            System.IO.MemoryStream ms2 = null;
            if (BvrPdf != null)
            {
                byte[] bin2 = System.IO.File.ReadAllBytes(BvrPdf);
                ms2 = new System.IO.MemoryStream(bin2);
            }

            PdfDocument doc0 = new PdfDocument();

            PdfDocument doc1 = null;
            if (bin1 != null)
            {
                doc1 = PdfReader.Open(ms1, PdfDocumentOpenMode.Import);
            }

            XPdfForm bvrForm = null;


            PdfPage page = null;

            if (bin1 != null)
            {
                for (int i = 0; i < doc1.Pages.Count; i++)
                {
                    page = doc1.Pages[i];
                    doc0.Pages.Add(page);
                }
            }

            if (BvrPdf != null)
            {
                bvrForm = XPdfForm.FromStream(ms2);
            }
            

            page = doc0.Pages[doc0.Pages.Count - 1];
            XGraphics gfx = XGraphics.FromPdfPage(page);
            gfx.MFEH = PdfFontEmbedding.Automatic;



            double width = page.Width;
            double height = page.Height;
            var box = new XRect(0, 0, width, height);
            // Draw the page identified by the page number like an image
            gfx.DrawImage(bvrForm, box);


            DrawBvr(gfx);

            System.IO.MemoryStream msout = new System.IO.MemoryStream();
            doc0.Save(msout);

            
            if (doc1 != null) doc1.Dispose();
            doc0.Dispose();
            if (ms1 != null) ms1.Dispose();
            if (ms2 != null) ms2.Dispose();

            byte[] ret = msout.ToArray();
            msout.Dispose();
            return ret;
        }

        public byte[] AppendToPdf(byte[] bin1)
        {
            if (String.IsNullOrWhiteSpace(FromCountryCode)) FromCountryCode = "CH";
            if (String.IsNullOrWhiteSpace(ToCountryCode)) ToCountryCode = "CH";

            System.IO.MemoryStream ms1 = null;
            if (bin1 != null)
            {
                //byte[] bin1 = System.IO.File.ReadAllBytes(file1);
                ms1 = new System.IO.MemoryStream(bin1);
            }

            System.IO.MemoryStream ms2 = null;
            if (BvrPdf != null)
            {
                byte[] bin2 = System.IO.File.ReadAllBytes(BvrPdf);
                ms2 = new System.IO.MemoryStream(bin2);
            }


            PdfDocument doc0 = new PdfDocument();

            PdfDocument doc1 = null;
            if (bin1 != null)
            {
                doc1 = PdfReader.Open(ms1, PdfDocumentOpenMode.Import);
            }
            PdfDocument doc2 = null;
            if (BvrPdf != null)
            {
                doc2 = PdfReader.Open(ms2, PdfDocumentOpenMode.Import);
            }


            PdfPage page = null;
            if (bin1 != null)
            {
                for (int i = 0; i < doc1.Pages.Count; i++)
                {
                    page = doc1.Pages[i];
                    doc0.Pages.Add(page);
                }
            }

            if (BvrPdf != null)
            {
                for (int i = 0; i < doc2.Pages.Count; i++)
                {
                    page = doc2.Pages[i];
                    doc0.Pages.Add(page);
                }
            }
            else
            {
                var blankpage = new PdfPage();
                if (doc0.PageCount == 0) blankpage.Size = PdfSharp.PageSize.A4;
                page = doc0.Pages.Add(blankpage);
            }

            page = doc0.Pages[doc0.Pages.Count - 1];
            XGraphics gfx = XGraphics.FromPdfPage(page);
            gfx.MFEH = PdfFontEmbedding.Automatic;


            DrawBvr(gfx);

            System.IO.MemoryStream msout = new System.IO.MemoryStream();
            doc0.Save(msout);

            if (doc2 != null) doc2.Dispose();
            if (doc1 != null) doc1.Dispose();
            doc0.Dispose();
            if (ms1 != null) ms1.Dispose();
            if (ms2 != null) ms2.Dispose();

            byte[] ret = msout.ToArray();
            msout.Dispose();
            return ret;
        }


        private string GetIbanCompact()
        {
            var defaultIban = "CH0000000000000000000";

            if (ToIban == null) return defaultIban;

            var iban = ToIban.Replace(" ", "").Trim();

            if (iban.Length != 21)
            {
                iban = defaultIban;
            }

            return iban;
        }

        public static string GetIbanFormatted(string iban)
        {
            if (string.IsNullOrEmpty(iban)) return null;
            string ret = iban.Replace(" ", string.Empty);
            ret = Regex.Replace(ret, ".{4}", "$0 ").Trim();
            return ret;
        }

        

        private string GetCurrency()
        {
            if (Currency == null) Currency = "CHF";
            return Currency;
        }

        private string ToStr(string d, int len)
        {
            while (d.Length < len)
            {
                d = "0" + d;
            }
            return d;
        }

        public int ChfLuhn(string chn)
        {
            int[] tbl = new int[] { 0, 9, 4, 6, 8, 2, 7, 1, 3, 5 };
            chn = chn.Trim().Replace(" ", "").Replace("-", "");
            int[] nums = (from r in chn where char.IsDigit(r) select Convert.ToInt32(r.ToString())).ToArray();

            int k = 0;

            for (int i = 0; i < nums.Length; i++)
            {
                k = tbl[(k + nums[i]) % 10];
            }

            int ret = (10 - k) % 10;

            return ret;
        }

        private string GetRefNrAndControlNumber(bool addspaces)
        {
            RefNumberBank = RefNumberBank.Replace(" ", "");
            RefNumberNum = RefNumberNum.Replace(" ", "");
            RefNumberBank = ToStr(RefNumberBank, 6);
            RefNumberNum = ToStr(RefNumberNum, 20);
            string control = ChfLuhn(RefNumberBank + RefNumberNum).ToString();

            string ret = RefNumberBank + RefNumberNum + control;

            if (addspaces)
            {
                int count = 0;
                string ret2 = "";
                for (int i = ret.Length - 1; i >= 0; i--)
                {
                    ret2 = ret[i] + ret2;
                    count++;
                    if (count % 5 == 0) ret2 = " " + ret2;

                }
                ret2 = ret2.Trim();
                ret = ret2;
            }


            return ret;
        }

        private string GetLabel(int len)
        {
            string ret = Label;
            if (ret == null) ret = "";
            if (ret.Length > len) ret = ret.Substring(0, len);
            return ret;
        }

        private void DrawBvr(XGraphics gfx)
        {
            var fonts = new InstalledFontCollection().Families;

            var options = new XPdfFontOptions(PdfFontEmbedding.Always);

            XFont font_txt = new XFont("Calibri", 8.8, XFontStyle.Regular, options);

            // Récépissé
            // *********
            DrawText(gfx, QrBvrPdfGenerator.GetIbanFormatted(ToIban), font_txt, 15, 52, 150, 12);
            DrawText(gfx, ToLine1, font_txt, 15, 62, 150, 12);
            DrawText(gfx, $"{ToLine2}", font_txt, 15, 72, 150, 12);
            DrawText(gfx, $"{ToLine3}", font_txt, 15, 82, 150, 12);

            DrawText(gfx, GetRefNrAndControlNumber(true), font_txt, 15, 106, 150, 12);

            DrawText(gfx, FromLine1, font_txt, 15, 130, 150, 12);
            DrawText(gfx, FromLine2, font_txt, 15, 140, 150, 12);
            DrawText(gfx, FromLine3, font_txt, 15, 150, 150, 12);

            DrawText(gfx, GetCurrency(), font_txt, 15, 220, 50, 12);
            DrawText(gfx, FormatAmount(Amount, true), font_txt, 70, 220, 100, 12);


            // Section paiement
            // ****************
            // sous le QR
            DrawText(gfx, GetCurrency(), font_txt, 190, 210, 30, 12); //215
            DrawText(gfx, FormatAmount(Amount, true), font_txt, 225, 210, 90, 12);

            // a droite du QR

            DrawText(gfx, QrBvrPdfGenerator.GetIbanFormatted(ToIban), font_txt, 332, 30, 150, 12);
            DrawText(gfx, ToLine1, font_txt, 332, 40, 150, 12);
            DrawText(gfx, ToLine2, font_txt, 332, 50, 150, 12);
            DrawText(gfx, ToLine3, font_txt, 332, 60, 150, 12);

            DrawText(gfx, GetRefNrAndControlNumber(true), font_txt, 332, 84, 150, 12);

            DrawText(gfx, GetLabel(30), font_txt, 332, 108, 150, 12);


            DrawText(gfx, FromLine1, font_txt, 332, 133, 150, 12);
            DrawText(gfx, FromLine2, font_txt, 332, 143, 150, 12);
            DrawText(gfx, FromLine3, font_txt, 332, 153, 150, 12);

            // QR content
            StringBuilder sb = new StringBuilder();
            string fldSep = "\n";
            sb.Append($"SPC{fldSep}"); // Swiss Payments Code, 3 chars
            sb.Append($"0200{fldSep}"); // Version 2.0, 4 digits
            sb.Append($"1{fldSep}"); // Encoding. 1 means UTF-8
            sb.Append($"{GetIbanCompact()}{fldSep}"); // iban (21 chars) only CH or LI country code permitted.
            sb.Append($"K{fldSep}"); // combined address elements (2 lines)
            sb.Append($"{MaxLen(ToLine1, 70)}{fldSep}"); // First name (optional) + last name or company name. Max 70 chars.
            sb.Append($"{MaxLen(ToLine2, 70)}{fldSep}"); // Address line 1.  Max 70 chars.
            sb.Append($"{MaxLen(ToLine3, 70)}{fldSep}"); // Address line 2.  Max 70 chars.
            sb.Append(fldSep); // Postal code. Max 16 chars.
            sb.Append(fldSep); // Town. Max 35 chars.
            sb.Append($"{MaxLen(ToCountryCode, 2)}{fldSep}"); // Two-digit country code according to ISO 3166-1
            sb.Append($"{fldSep}{fldSep}{fldSep}{fldSep}{fldSep}{fldSep}{fldSep}"); // Skip ultimate creditor
            sb.Append($"{FormatAmount(Amount, false)}{fldSep}"); // amount. 12 chars allowed included decimal points.
            sb.Append($"{MaxLen(GetCurrency(), 3)}{fldSep}"); // Only CHF and EUR are permitted.
            sb.Append($"K{fldSep}"); // combined address elements (2 lines)
            sb.Append($"{MaxLen(FromLine1, 70)}{fldSep}"); // First name (optional) + last name or company name. Max 70 chars.
            sb.Append($"{MaxLen(FromLine2, 70)}{fldSep}"); // Address line 1.  Max 70 chars.
            sb.Append($"{MaxLen(FromLine3, 16)}{fldSep}"); // Address line 2.  Max 16 chars.
            sb.Append(fldSep); // Postal code. Max 16 chars.
            sb.Append(fldSep); // Town. Max 35 chars.
            sb.Append($"{MaxLen(FromCountryCode, 2)}{fldSep}");// Two-digit country code according to ISO 3166-1
            sb.Append($"QRR{fldSep}"); // QRR
            sb.Append(MaxLen(GetRefNrAndControlNumber(false), 27) + fldSep); // Reference Nr. 27 chars.
            sb.Append($"{GetLabel(140)}{fldSep}");
            sb.Append($"EPD{fldSep}"); // End Payment Data
            sb.Append(fldSep);
            sb.Append(fldSep);

            var data = sb.ToString().Replace("’", "\'");

            var bmp = GenerateQr(data);
            if (bmp != null)
            {
                var qr = XImage.FromGdiPlusImage(bmp);
                gfx.DrawImage(qr, new RectangleF(192 + LeftMargin, 
                    61.3f + 535 + TopMargin, 
                    124.5f, 124.5f));
            }
            else
            {
                gfx.DrawRectangle(XBrushes.LightSalmon, new XRect(192 + LeftMargin, 
                    535 + 61 + TopMargin,
                    124, 124));
                PdfSharp.Drawing.Layout.XTextFormatter formatter = new PdfSharp.Drawing.Layout.XTextFormatter(gfx);
                formatter.DrawString(QrError, font_txt, XBrushes.Red, new XRect(192 + LeftMargin, 
                    535 + 61 + TopMargin, 
                    124, 124));
            }

        }

        public string QrError { get; set; }
        private Bitmap GenerateQr(string data)
        {

            TECIT.TBarCode.Barcode barcode = new TECIT.TBarCode.Barcode();
            barcode.BarcodeType = TECIT.TBarCode.BarcodeType.SwissQrCode;

            // are we in 32-bit mode?
            if (IntPtr.Size == 4)
                barcode.License("Digital Pen Corporation, CH-1110", TECIT.TBarCode.LicenseType.Site, 1, "D06646B35070917F5C33D774A292E4CA", TECIT.TBarCode.TBarCodeProduct.Barcode2D);
            // or in 64-bit process?
            else if (IntPtr.Size == 8)
                barcode.License("Digital Pen Corporation, CH-1110", TECIT.TBarCode.LicenseType.Site, 1, "D06646B35070917F5C33D774A292E4CA", TECIT.TBarCode.TBarCodeProduct.Barcode2D);
            //else
            //error, unknown platform!
            barcode.Data = data;
            
            // GetOptimumBitmapSize ensures the readability of the bar code
            barcode.BoundingRectangle = new Rectangle(0, 0, 287, 287);

            // save the barcode as image
            //barcode.Draw("test.png", TECIT.TBarCode.ImageType.Png);

            try
            {
                Bitmap img = barcode.DrawBitmap();
                return img;
            }
            catch(Exception ex)
            {
                _log.Error($"{ex.Message} data:{data}", ex);
                QrError = "QR Error: " + ex.Message;
                return null;
            }
        }

        private string MaxLen(string str, int l)
        {
            string ret = str;
            if (ret == null) ret = "";
            if (ret.Length > l) return ret.Substring(0, l);
            return ret;
        }

        private void DrawText(XGraphics gfx, string text, XFont font, float left, float top, float width, float height, string format = "left", bool debug = false, bool forcerect = false)
        {
            if (format != null && format.StartsWith("combs:"))
            {
                int numberofcombs = 1;
                string tmp = format.Split(':').LastOrDefault();
                Int32.TryParse(tmp, out numberofcombs);

                while (text.Length < numberofcombs)
                {
                    text = " " + text;
                }

                float combwidth = Convert.ToSingle(width) / Convert.ToSingle(numberofcombs);
                for (int i = 0; i < numberofcombs; i++)
                {
                    string dachar = text[i].ToString();
                    if (!String.IsNullOrWhiteSpace(dachar))
                    {
                        DrawText(gfx, dachar, font, left + (i * combwidth), top, combwidth, height, "center");
                    }
                }
                return;
            }

            if (true || BvrPdf == null)
            {
                top += TopMargin;
                left += LeftMargin;
            }

            top += 535;

            XStringFormat align = new XStringFormat();
            if (format == "left")
            {
                align.Alignment = XStringAlignment.Near;
            }
            else if (format == "right")
            {
                align.Alignment = XStringAlignment.Far;
            }
            else if (format == "center")
            {
                align.Alignment = XStringAlignment.Center;
            }
            align.LineAlignment = XLineAlignment.Center;




            if (DebugRectangles || forcerect)
            {
                XSolidBrush b = new XSolidBrush(XColor.FromArgb(125, XColors.Yellow));
                gfx.DrawRectangle(b, new XRect(left, top, width, height));
            }
            if (!String.IsNullOrWhiteSpace(text))
            {
                if (debug)
                {
                    gfx.DrawString(text, font, XBrushes.Blue,
                            new XRect(left, top, width, height),
                            align);
                }
                else
                {
                    gfx.DrawString(text, font, XBrushes.Black,
                                new XRect(left, top, width, height),
                                align);
                    
                }
            }
        }

    }
}
