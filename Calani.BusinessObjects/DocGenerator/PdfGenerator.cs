﻿using Calani.BusinessObjects.Projects;
using GemBox.Spreadsheet;
using System;
using System.IO;
using System.Linq;

namespace Calani.BusinessObjects.DocGenerator
{
    public class PdfGenerator
    {
        protected bool hideDiscount { get; set; }

        protected bool hideUnit { get; set; }

        protected bool hideVat { get; set; }

        protected bool hideServiceCustomNumber { get; set; }

        protected string FormatAmount(double amount, bool spaces)
        {
            if (spaces)
            {
                var nfi = (System.Globalization.NumberFormatInfo)System.Globalization.CultureInfo.InvariantCulture.NumberFormat.Clone();
                nfi.NumberGroupSeparator = " ";
                return amount.ToString("#,0.00", nfi);
            }
            else
            {

                string ret = String.Format(System.Globalization.CultureInfo.InvariantCulture, "{0:0.00}", amount);
                return ret;
            }
        }

        protected double CalculateHeight(ExcelWorksheet worksheet, int startRow, int endRow, LengthUnit lengthUnit)
        {
            if(worksheet == null || startRow < 0 || endRow < 0 || startRow > endRow)
            {
                return 0;
            }

            double totalHeight = 0;

            for(var idx = startRow; idx <= endRow; idx++)
            {
                totalHeight += worksheet.Rows[idx].Height;
            }

            return LengthUnitConverter.Convert(totalHeight, LengthUnit.Twip, lengthUnit);
        }

        protected (double newWidth, double newHeight, bool isHorizontal) GetLogoSize(MemoryStream msLogo)
        {
            var logo = new Generic.ImageManager()
            {
                TargetWidth = 400,
                TargetHeight = 400
            };

            logo.Init(msLogo.ToArray());

            double newWidth = 95;
            double newHeight = 95;
            double left = 460;
            double top = 30;

            if (logo.SourceWidth > logo.SourceHeight)
            {
                newWidth = 200;
                newHeight = newWidth * logo.SourceHeight / logo.SourceWidth;
            }
            else if (logo.SourceWidth < logo.SourceHeight)
            {
                newHeight = 95;
                newWidth = newHeight * logo.SourceWidth / logo.SourceHeight;
            }

            return (newWidth, newHeight, logo.SourceWidth > logo.SourceHeight);
        }

        protected string DoubleToString(double? value, double quantity) => value.HasValue && value.Value > 0 || quantity > 0 ? FormatAmount(value ?? 0, true) : " ";
        
        protected string DoubleToStringWithSuffix(double? value, double quantity, string suffix)
        {
            var str = DoubleToString(value, quantity);
            return String.IsNullOrWhiteSpace(str) ? str : str + suffix;
        }

        protected bool NeedHideVat(jobsExtended job)
        {
            return job.organizations.hide_vat_pdf && job.services_jobs.Any() && job.services_jobs.All(j => j.taxRate == job.services_jobs.First().taxRate);
        }

        protected bool NeedHideServiceCustomNumber(jobsExtended job)
        {
            return !job.organizations.display_service_custom_number || !job.services_jobs.Any(item => !string.IsNullOrEmpty(item.custom_number?.Trim()));
        }
    }
}
