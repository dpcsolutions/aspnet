﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PdfSharp.Pdf;
using PdfSharp.Drawing;
using PdfSharp.Pdf.IO;
using System.Drawing;
using System.Drawing.Text;

namespace Calani.BusinessObjects.DocGenerator
{
    public class ClassicBvrPdfGenerator
    {
        public string BvrPdf { get; set; }

        public bool DebugRectangles { get; set; }

        public string ToBankLine1 { get; set; }
        public string ToBankLine2 { get; set; }


        public string ToLine1 { get; set; }
        public string ToLine2 { get; set; }
        public string ToLine3 { get; set; }
        public string ToLine4 { get; set; }

        public string FromLine1 { get; set; }
        public string FromLine2 { get; set; }
        public string FromLine3 { get; set; }

        public string Account { get; set; }

        public double Amount { get; set; }

        public string RefNumberBank { get; set; }
        public string RefNumberNum { get; set; }

        public string Type { get; set; }

        public int TopMargin { get; set; }
        public int LeftMargin { get; set; }

        public bool HideAccountAndBank { get; set; }

        

        public ClassicBvrPdfGenerator()
        {
            Type = "01";
        }

        public void SetModeAndType(int mode, int type, string directory)
        {
            if(type != 1)
            {
                BvrPdf = null;
            }

            if (type == 1)
            {
                if (mode == 1)
                {
                    BvrPdf = System.IO.Path.Combine(directory, "bvr-post.pdf");
                }
                else if (mode == 2)
                {
                    BvrPdf = System.IO.Path.Combine(directory, "bvr-bank.pdf");
                }
            }
            else if (type <= 0)
            {
                if (type == -1)
                {
                    HideAccountAndBank = true;
                }
            }
        }

        public void Init(Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr, string directory)
        {
            if (mgr.BvrType == 1)
            {
                if (mgr.BvrMode == 1)
                {
                    BvrPdf = System.IO.Path.Combine(directory, "bvr-post.pdf");
                }
                else if (mgr.BvrMode == 2)
                {
                    BvrPdf = System.IO.Path.Combine(directory, "bvr-bank.pdf");
                }
            }
            else if (mgr.BvrType <= 0)
            {
                TopMargin = mgr.BvrTopMargin;
                LeftMargin = mgr.BvrLeftMargin;

                if(mgr.BvrType == -1)
                {
                    HideAccountAndBank = true;
                }
            }

            if (mgr.BvrMode == 2)
            {
                ToBankLine1 = mgr.BvrBankLine1;
                ToBankLine2 = mgr.BvrBankLine2;
            }



            ToLine1 = mgr.CompanyName;
            ToLine2 = mgr.AdrStreet + " " + mgr.AdrStreetNb;
            if (!String.IsNullOrWhiteSpace(mgr.AdrStreet2))
            {
                ToLine3 = mgr.AdrStreet2;
                ToLine4 = mgr.AdrZipCode + " " + mgr.AdrCity;
            }
            else
            {
                ToLine3 = mgr.AdrZipCode + " " + mgr.AdrCity;
            }

            Account = mgr.BvrAccount;
            RefNumberBank = mgr.BvrSenderBank;


            
        }

        public byte[] AppendToPdf(byte[] bin1)
        {
            System.IO.MemoryStream ms1 = null;
            if (bin1 != null)
            {
                //byte[] bin1 = System.IO.File.ReadAllBytes(file1);
                ms1 = new System.IO.MemoryStream(bin1);
            }

            System.IO.MemoryStream ms2 = null;
            if (BvrPdf != null)
            {
                byte[] bin2 = System.IO.File.ReadAllBytes(BvrPdf);
                ms2 = new System.IO.MemoryStream(bin2);
            }


            PdfDocument doc0 = new PdfDocument();

            PdfDocument doc1 = null;
            if (bin1 != null)
            {
                doc1 = PdfReader.Open(ms1, PdfDocumentOpenMode.Import);
            }
            PdfDocument doc2 = null;
            if (BvrPdf != null)
            {
                doc2 = PdfReader.Open(ms2, PdfDocumentOpenMode.Import);
            }
            

            PdfPage page = null;
            if (bin1 != null)
            {
                for (int i = 0; i < doc1.Pages.Count; i++)
                {
                    page = doc1.Pages[i];
                    doc0.Pages.Add(page);
                }
            }
            
            if (BvrPdf != null)
            {
                for (int i = 0; i < doc2.Pages.Count; i++)
                {
                    page = doc2.Pages[i];
                    doc0.Pages.Add(page);
                }
            }
            else
            {
                var blankpage = new PdfPage();
                if(doc0.PageCount == 0) blankpage.Size = PdfSharp.PageSize.A4;
                page = doc0.Pages.Add(blankpage);
            }

            page = doc0.Pages[doc0.Pages.Count - 1];
            XGraphics gfx = XGraphics.FromPdfPage(page);
            gfx.MFEH = PdfFontEmbedding.Automatic;


            DrawBvr(gfx);

            System.IO.MemoryStream msout = new System.IO.MemoryStream();
            doc0.Save(msout);

            if (doc2 != null) doc2.Dispose();
            if (doc1 != null) doc1.Dispose();
            doc0.Dispose();
            if (ms1 != null) ms1.Dispose();
            if (ms2 != null) ms2.Dispose();

            byte[] ret = msout.ToArray();
            msout.Dispose();
            return ret;
        }


        private void DrawBvr(XGraphics gfx)
        {
            var fonts = new InstalledFontCollection().Families;

            var options = new XPdfFontOptions(PdfFontEmbedding.Always);

            // Create a font
            //XFont font_txt = new XFont("Courier New", 9, XFontStyle.Regular);
            XFont font_txt = new XFont("Calibri", 8.6, XFontStyle.Regular, options);

            string specialfont = "OCR-B 10 BT";
            
            

            XFont font_code = new XFont(specialfont, 10, XFontStyle.Regular, options);
            XFont font_code_alt = new XFont(specialfont, 10, XFontStyle.Regular, options);
            


            XFont font_code2 = new XFont(specialfont, 11, XFontStyle.Regular, options);
            XFont font_code3 = new XFont(specialfont, 10, XFontStyle.Regular, options);
            XFont font_code4 = new XFont(specialfont, 11.8, XFontStyle.Regular, options);
            XFont font_code4_old = new XFont(specialfont, 11.8, XFontStyle.Regular, options);
            

            // Textes versement pour ...
            if (HideAccountAndBank == false)
            {
                if (ToBankLine1 != null) DrawText(gfx, ToBankLine1, font_txt, 15, 25, 150, 12);
                if (ToBankLine2 != null) DrawText(gfx, ToBankLine2, font_txt, 15, 25 + 12, 150, 12);
            }
            DrawText(gfx, ToLine1, font_txt, 15, 62, 150, 12);
            DrawText(gfx, ToLine2, font_txt, 15, 62 + 12, 150, 12);
            DrawText(gfx, ToLine3, font_txt, 15, 62 + 24, 150, 12);
            DrawText(gfx, ToLine4, font_txt, 15, 62 + 36, 150, 12);

            if (HideAccountAndBank == false)
            {
                DrawText(gfx, ToBankLine1, font_txt, 179, 25, 150, 12);
                DrawText(gfx, ToBankLine2, font_txt, 179, 25 + 12, 150, 12);
            }
            DrawText(gfx, ToLine1, font_txt, 179, 62, 150, 12);
            DrawText(gfx, ToLine2, font_txt, 179, 62 + 12, 150, 12);
            DrawText(gfx, ToLine3, font_txt, 179, 62 + 24, 150, 12);
            DrawText(gfx, ToLine4, font_txt, 179, 62 + 36, 150, 12);
            // -->


            // From
            DrawText(gfx, GetRefNrAndControlNumber(true), font_txt, 15, 176, 150, 12);
            DrawText(gfx, FromLine1, font_txt, 15, 176 + 12, 150, 12);
            DrawText(gfx, FromLine2, font_txt, 15, 176 + 24, 150, 12);
            DrawText(gfx, FromLine3, font_txt, 15, 176 + 36, 150, 12);

            // 365 ou 362 ??
            DrawText(gfx, FromLine1, font_txt, 365, 130 + 12, 150, 12);
            DrawText(gfx, FromLine2, font_txt, 365, 130 + 24, 150, 12);
            DrawText(gfx, FromLine3, font_txt, 365, 130 + 36, 150, 12);
            // -->

            // Compte
            if (HideAccountAndBank == false)
            {
                DrawText(gfx, Account, font_code, 83, 118, 75, 15);
                DrawText(gfx, Account, font_code, 256, 118, 75, 15);
            }
            // -->

            // Amount         
            double amount_int = Amount * 100;
            double amount_cent2 = amount_int -(Math.Floor(Amount) * 100);
            amount_int = Math.Ceiling((amount_int - amount_cent2) / 100);
            string amount_cents = amount_cent2.ToString();

            while (amount_cents.Length < 2) amount_cents = "0" + amount_cents;
            DrawText(gfx, amount_int.ToString(), font_code2, 3, 143, 121, 15, "combs:8");
            DrawText(gfx, amount_cents.ToString(), font_code2, 135, 143, 30, 15, "combs:2");
            DrawText(gfx, amount_int.ToString(), font_code2, 175, 143, 121, 15, "combs:8");
            DrawText(gfx, amount_cents.ToString(), font_code2, 307, 143, 30, 15, "combs:2");

            // -->


            // Ref
            DrawText(gfx, GetRefNrAndControlNumber(true), font_code, 360, 98, 220, 12, "right");

            //-->

             
            string prefix = Type;
            string code = prefix;
            string stramount = ToStr(Math.Floor(Amount * 100).ToString(), 10);
            code += stramount;
            string controleAmount = ChfLuhn(prefix + stramount).ToString();
            code += controleAmount;
            code += ">";
            code += GetRefNrAndControlNumber(false);
            code += "+ ";
            code += GetParticipantNr();
            code += ">";


            
             //DrawText(gfx, code, font_code4, 200, 238, 374, 20, "right", true);


            string code1 = prefix;
            code1 += stramount;
            code1 += controleAmount;
            code1 += ">";
            code1 += GetRefNrAndControlNumber(false);
            code1 += "+ ";



            //DrawText(gfx, code1, font_code4, 190, 238, 310, 20, "left",false,false);
            //DrawText(gfx, code1, font_code4, 190, 238, 301, 20, "combs:"+code1.Length,true, false);
            //DrawText(gfx, code1, font_code4, 190, 238, 310, 20, "combs:" + code1.Length, false, false);
            DrawText(gfx, code1, font_code4, 193, 238, 310, 20, "combs:" + code1.Length, false, false);


            string code2 = GetParticipantNr();
            code2 += ">";
            DrawText(gfx, code2, font_code4, 200, 238, 374, 20, "right");





        }

        private string GetParticipantNr()
        {
            string x = "";

            string[] parts = Account.Split('-');
            if(parts.Length == 3)
            {
                x += ToStr(parts[0], 2);
                x += ToStr(parts[1] + parts[2], 7);
            }
            else
            {
                x = ToStr(x, 9);
            }

            return x;
        }

        private string GetRefNrAndControlNumber(bool addspaces)
        {
            RefNumberBank = RefNumberBank.Replace(" ", "");
            RefNumberNum = RefNumberNum.Replace(" ", "");
            RefNumberBank = ToStr(RefNumberBank, 6);
            RefNumberNum = ToStr(RefNumberNum, 20);
            string control = ChfLuhn(RefNumberBank + RefNumberNum).ToString();

            string ret = RefNumberBank + RefNumberNum + control;
        
            if(addspaces)
            {
                int count = 0;
                string ret2 = "";
                for (int i = ret.Length - 1; i >= 0; i--)
                {
                    ret2 = ret[i] + ret2;
                    count++;
                    if (count % 5 == 0) ret2 = " " + ret2;
                    
                }
                ret2 = ret2.Trim();
                ret = ret2;
            }


            return ret;
        }

        



        public int ChfLuhn(string chn)
        {


            //int[] tbl = new int[] { 0, 9, 4, 6, 8, 2, 7, 1, 3, 5 };
            int[] tbl = new int[] { 0, 9, 4, 6, 8, 2, 7, 1, 3, 5 };
            chn = chn.Trim().Replace(" ", "").Replace("-", "");
            int[] nums = (from r in chn where char.IsDigit(r) select Convert.ToInt32(r.ToString())).ToArray();

            int k = 0;

            for (int i = 0; i < nums.Length; i++)
            {
                k = tbl[(k + nums[i]) % 10];
            }

            int ret = (10 - k) % 10;

            return ret;
        }


        

        private string ToStr(string d, int len)
        {
            while (d.Length < len)
            {
                d = "0" + d;
            }
            return d;
        }


        private void DrawText(XGraphics gfx, string text, XFont font, float left, float top, float width, float height, string format = "left", bool debug=false, bool forcerect=false)
        {
            if (format != null && format.StartsWith("combs:"))
            {
                int numberofcombs = 1;
                string tmp = format.Split(':').LastOrDefault();
                Int32.TryParse(tmp, out numberofcombs);

                while (text.Length < numberofcombs)
                {
                    text = " " + text;
                }

                float combwidth = Convert.ToSingle(width) / Convert.ToSingle(numberofcombs);
                for (int i = 0; i < numberofcombs; i++)
                {
                    string dachar = text[i].ToString();
                    if (!String.IsNullOrWhiteSpace(dachar))
                    {
                        DrawText(gfx, dachar, font, left + (i * combwidth), top, combwidth, height, "center");
                    }
                }
                return;
            }

            if (true || BvrPdf == null)
            {
                top += TopMargin;
                left += LeftMargin;
            }

            top += 535;

            XStringFormat align = new XStringFormat();
            if (format == "left")
            {
                align.Alignment = XStringAlignment.Near;
            }
            else if(format == "right")
            {
                align.Alignment = XStringAlignment.Far;
            }
            else if (format == "center")
            {
                align.Alignment = XStringAlignment.Center;
            }
            align.LineAlignment = XLineAlignment.Center;

            


            if(DebugRectangles || forcerect) gfx.DrawRectangle(XBrushes.Yellow, new XRect(left, top, width, height));
            if (!String.IsNullOrWhiteSpace(text))
            {
                if (debug)
                {
                    gfx.DrawString(text, font, XBrushes.Blue,
                            new XRect(left, top, width, height),
                            align);
                }
                else
                {
                    gfx.DrawString(text, font, XBrushes.Black,
                                new XRect(left, top, width, height),
                                align);
                }
            }
        }
    }
}
