﻿using GemBox.Spreadsheet;
using System.Drawing;
using System.IO;

namespace Calani.BusinessObjects.DocGenerator
{
    public class ExcelTableGenerator
    {
        public System.Data.DataTable Table { get; set; }

        public bool Borders { get; set; }
        public bool FitColumns { get; set; }

        public byte[] GenerateDocument(int? hiddenIndex = null)
        {
            SpreadsheetInfo.SetLicense(GemboxAPI.SpreadsheetLicense);

            ExcelFile ef = new ExcelFile();
            var sheet = ef.Worksheets.Add("Report");

            int col = 0;
            foreach (System.Data.DataColumn column in Table.Columns)
            {
                sheet.Cells[0, col].Value = column.ColumnName;
                if (col == hiddenIndex) sheet.Cells[0, col].Style.Font.Color = Color.Transparent;
                else {
                    if (Borders) sheet.Cells[0, col].Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
                }
                col++;
            }
            
            int row = 1;
            foreach (System.Data.DataRow drow in Table.Rows)
            {
                col = 0;
                foreach (System.Data.DataColumn column in Table.Columns)
                {
                    if (drow[column.ColumnName] != null)
                    {
                        sheet.Cells[row, col].Value = drow[column.ColumnName].ToString();

                        if (col == hiddenIndex) sheet.Cells[row, col].Style.Font.Color = Color.Transparent;
                        else {
                            if (Borders) sheet.Cells[row, col].Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(0, 0, 0), LineStyle.Thin);
                        }
                    }
                    col++;
                }
                row++;
            }

            if (FitColumns)
            {
                col = 0;
                foreach (System.Data.DataColumn column in Table.Columns)
                {
                    sheet.Columns[col].AutoFit();
                    col++;
                }
            }

            MemoryStream ms = new MemoryStream();
            ef.Save(ms, SaveOptions.XlsxDefault);
            byte[] ret = ms.ToArray();
            ms.Dispose();

            return ret;
        }
    }
}
