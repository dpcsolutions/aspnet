﻿using System.Globalization;

namespace Calani.BusinessObjects.DocGenerator
{
    public static class JobExtensions
    {
        public static string GetCustomerReference(this Calani.BusinessObjects.Model.jobs job, CultureInfo culture)
        {
            if (!string.IsNullOrWhiteSpace(job.customer_reference))
            {
                return $"{GetResource("CustomerReference", culture)} : {job.customer_reference}";
            }

            return string.Empty;
        }

        public static string GetLocationOfIntervention(this Calani.BusinessObjects.Model.jobs job, CultureInfo culture)
        {
            if (!string.IsNullOrWhiteSpace(job.location_intervention))
            {
                return $"{GetResource("LocationOfIntervention", culture)} : {job.location_intervention}";
            }

            return string.Empty;
        }

        public static string GetTypeOfIntervention(this Calani.BusinessObjects.Model.jobs job, CultureInfo culture)
        {
            if (!string.IsNullOrWhiteSpace(job.type_intervention))
            {
                return $"{GetResource("TypeOfIntervention", culture)} : {job.type_intervention}";
            }

            return string.Empty;
        }

        private static string GetResource(string n, CultureInfo culture)
        {
            if (SharedResource.Resource == null) return n;
            return SharedResource.Resource.GetString(n, culture);
        }
    }
}
