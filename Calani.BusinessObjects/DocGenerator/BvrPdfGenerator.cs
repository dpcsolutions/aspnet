﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.DocGenerator
{
    public class BvrPdfGenerator
    {

        public bool DebugRectangles { get; set; }

        public string ToBankLine1 { get; set; }
        public string ToBankLine2 { get; set; }


        public string ToLine1 { get; set; }
        public string ToLine2 { get; set; }
        public string ToLine3 { get; set; }
        public string ToLine4 { get; set; }

        public string FromLine1 { get; set; }
        public string FromLine2 { get; set; }
        public string FromLine3 { get; set; }

        public string Account { get; set; }

        public double Amount { get; set; }

        public string RefNumberBank { get; set; }
        public string RefNumberNum { get; set; }

        //public string Type { get; set; }

        public int TopMargin { get; set; }
        public int LeftMargin { get; set; }

        public bool HideAccountAndBank { get; set; }


        public string Label { get; set; }
        public string ToIban { get; set; }




        // mode
        // ****
        // 0: pas de BVR
        // 1: post
        // 2: bank
        // 10: qr (new)
        // 11: qr (new) + post
        // 12: qr (new) + bank
        int _mode;


        // type
        // ****
        // 1 : full print
        // 0 : no background
        // -1 : no background no account
        int _type;



        string _directory;
        Calani.BusinessObjects.CustomerAdmin.OrganizationManager _mgr;

        public void SetModeAndType(int mode, int type, string directory)
        {
            _mode = mode;
            _type = type;
            _directory = directory;
        }

        public void Init(Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr, string directory)
        {
            _directory = directory;
            _mgr = mgr;
            _mode = mgr.BvrMode;
            _type = mgr.BvrType;
        }


        public byte[] AppendToPdf(byte[] pdf)
        {
            byte[] retpdf = pdf;

            int classicMode = _mode;
            if (classicMode >= 10) classicMode -= 10;
            if (classicMode > 0)
            {
                ClassicBvrPdfGenerator classic = new ClassicBvrPdfGenerator();
                
                classic.Account = this.Account;
                classic.Amount = this.Amount;
                classic.DebugRectangles = this.DebugRectangles;
                classic.FromLine1 = this.FromLine1;
                classic.FromLine2 = this.FromLine2;
                classic.FromLine3 = this.FromLine3;
                classic.HideAccountAndBank = this.HideAccountAndBank;
                classic.LeftMargin = this.LeftMargin;
                classic.RefNumberBank = this.RefNumberBank;
                classic.RefNumberNum = this.RefNumberNum;
                classic.ToBankLine1 = this.ToBankLine1;
                classic.ToBankLine2 = this.ToBankLine2;
                classic.ToLine1 = this.ToLine1;
                classic.ToLine2 = this.ToLine2;
                classic.ToLine3 = this.ToLine3;
                classic.ToLine4 = this.ToLine4;
                classic.TopMargin = this.TopMargin;


                if (_mgr != null)
                {
                    classic.Init(_mgr, _directory);
                }
                classic.SetModeAndType(classicMode, _type, _directory);
                retpdf = classic.AppendToPdf(retpdf);
            }

            bool qrMode = _mode >= 10;
            if (qrMode)
            {
                QrBvrPdfGenerator qr = new QrBvrPdfGenerator();
                qr.Amount = this.Amount;
                qr.Currency = "CHF";
                qr.FromCountryCode = "CH";
                qr.FromLine1 = this.FromLine1;
                qr.FromLine2 = this.FromLine2;
                qr.FromLine3 = this.FromLine3;
                qr.Label = this.Label;
                qr.LeftMargin = 0;
                qr.RefNumberBank = this.RefNumberBank;
                qr.RefNumberNum = this.RefNumberNum;
                qr.ToCountryCode = "CH";
                qr.ToIban = this.ToIban;
                qr.ToLine1 = this.ToLine1;
                qr.ToLine2 = this.ToLine2;
                qr.ToLine3 = this.ToLine3;
                qr.ToLine4 = this.ToLine4;
                qr.TopMargin = this.TopMargin;

                bool both = classicMode > 0;
                if (_mgr != null)
                {
                    qr.Init(_mgr, _directory, both);
                }
                if (both)
                {
                    retpdf = qr.MergeOnLastPage(retpdf);
                }
                else
                {
                    retpdf = qr.AppendToPdf(retpdf);
                }
            }

            return retpdf;
        }
    }
}
