﻿using Calani.BusinessObjects.Attachments;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Projects;
using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Calani.BusinessObjects.CustomerAdmin;

namespace Calani.BusinessObjects.DocGenerator
{
    public class InvoiceGenerator : PdfGenerator
    {
        public long ProjectId { get; set; }

        public int Reminder { get; set; }

        public static string ExcelTemplates { get; set; }
        public static string BvrTemplates { get; set; }

        public System.Globalization.CultureInfo Culture { get; set; }

        public bool ExcelMode { get; set; }

        long _userId;

        public bool DisableBvr { get; set; }

        public InvoiceGenerator(System.Globalization.CultureInfo culture, long userId)
        {
            _userId = userId;
            Culture = culture;
        }

        List<MemoryStream> _ms = new List<MemoryStream>();

        private string GetResource(string n)
        {
            if (SharedResource.Resource == null) return n;
            return SharedResource.Resource.GetString(n, Culture);
        }

        ExcelWorksheet ws;

        int itemsLineStart = 19;
        int itemsLineEnd = 19;

        public byte[] GenerateDocument(List<long> attIds = null, string action = null)
        {
            var db = new Model.CalaniEntities();
            var dbjob = (from r in db.jobs.Include("services_jobs").Include("services_jobs.services") where r.id == ProjectId select r).FirstOrDefault();

            var job = new jobsExtended(dbjob, false);
            var orgMgr = new OrganizationManager(job.organizationId.Value);
            orgMgr.Load();
            
            // si on utilise une monnaie étrangere on désactive le BVR
            if (!String.IsNullOrWhiteSpace(job.currency))
            {
                DisableBvr = true;
            }

            SpreadsheetInfo.SetLicense(GemboxAPI.SpreadsheetLicense);

            ExcelFile ef = ExcelFile.Load(Path.Combine(ExcelTemplates, "Invoice.xlsx"), LoadOptions.XlsxDefault);

            ws = ef.Worksheets.First();
            string sheetname = GetResource("Invoice") + " " + job?.internalId?
                .Replace(":", "")
                .Replace("\0", "")
                .Replace("\x3", "")
                .Replace("\\", "")
                .Replace("/", "")
                .Replace("?", "")
                .Replace("*", "");
            if (sheetname.Length > 30)
                sheetname = sheetname.Substring(0, 30);

            ws.Name = sheetname;
            ws.PrintOptions.BottomMargin = 0.5;

            hideDiscount = job.organizations.hide_0_discount_col && job.GetTotalItemsDiscount() == 0;
            hideUnit = job.organizations.hide_unit_column;
            hideVat = NeedHideVat(job);
            hideServiceCustomNumber = NeedHideServiceCustomNumber(job);

            var isHorizontalLogo = BindYourIdentity(job.organizations);

            var cust = (from r in db.clients where r.id == job.clientId select r).FirstOrDefault();
            var custAddr = (from r in db.addresses where r.id == job.clientAddress select r).FirstOrDefault();
            var custContact = (from r in db.contacts where r.id == job.clientContact select r).FirstOrDefault();

            BindYourCustomer(cust, custAddr, custContact, job.organizations.show_contact_info_invoices);

            BindInvoiceInfo(job, orgMgr.HideCodeOnPdf);
            
            BindItems(job, job.organizations, action, out var itemsCount);

            BindAttachments(ef, job, attIds);

            BindPaymentFooter(job);

            if (!job.organizations.round_amount)
            {
                ws.Rows.Remove(itemsCount + 19);
                ws.Rows.Remove(itemsCount + 19);
            }

            ws.HeadersFooters.DefaultPage.Footer.CenterSection.Content = string.Empty;
            ws.HeadersFooters.DefaultPage.Footer.CenterSection.Append(GetResource("Page") + " ").Append(HeaderFooterFieldType.PageNumber).Append(" / ").Append(HeaderFooterFieldType.NumberOfPages);

            MemoryStream ms = new MemoryStream();
            if (ExcelMode)
            {
                ef.Save(ms, SaveOptions.XlsxDefault);
            }
            else
            {
                var options = SaveOptions.PdfDefault;
                options.SelectionType = SelectionType.EntireFile;

                ef.Save(ms, options);
            }
            byte[] ret = ms.ToArray();
            ms.Dispose();

            foreach (var ms2 in _ms)
            {
                ms2.Dispose();
            }

            if (!DisableBvr && orgMgr.BvrMode > 0)
            {
                var bvr = new BvrPdfGenerator();
                bvr.Init(orgMgr, BvrTemplates);
                bvr.Label = job.internalId;

                if (job.invoiceDue != null)
                    bvr.Amount = job.CalculateDue(orgMgr.RoundAmount, true, orgMgr.UsePriceIncludingVat);

                bvr.FromLine1 = cust.companyName;
                if (custAddr != null)
                {
                    bvr.FromLine2 = custAddr.street;
                    if (!String.IsNullOrWhiteSpace(custAddr.nb))
                        bvr.FromLine2 += ", " + custAddr.nb;

                    bvr.FromLine3 = custAddr.npa + " " + custAddr.city;

                    if (job.dateInvoicing != null)
                    {
                        bvr.RefNumberNum = "0" + job.dateInvoicing.Value.ToString("yyMM");
                    }
                    else
                    {
                        bvr.RefNumberNum = "0" + "0000";
                    }
                    string id = job.id.ToString();
                    while (id.Length < 10)
                    {
                        id = "0" + id;
                    }
                    bvr.RefNumberNum += id;
                }
                byte[] pdf = bvr.AppendToPdf(ret);
                ret = pdf;
            }


            return ret;
        }

        private void BindPaymentFooter(jobsExtended job)
        {
            organizations organizations = job.organizations;
            var cur = job.currency;
            int newFooterY = itemsLineEnd + 3;

            var bank1 = (from r in organizations.bankaccount where r.priority == 1 && !String.IsNullOrWhiteSpace(r.bankname) select r).FirstOrDefault();
            var bank2 = (from r in organizations.bankaccount where r.priority == 2 && !String.IsNullOrWhiteSpace(r.bankname) select r).FirstOrDefault();

            if (String.IsNullOrWhiteSpace(cur) && bank1 != null)
            {
                var rangeForBorder = ws.Cells.GetSubrangeAbsolute(newFooterY, 0, newFooterY, 1);
                rangeForBorder.Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(169, 169, 169), LineStyle.Thin);

                ws.Cells[newFooterY, 0].Value = GetResource("BankDetails");
                ws.Cells[newFooterY, 0].Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(169, 169, 169), LineStyle.Thin);

                var range = ws.Cells.GetSubrangeAbsolute(newFooterY + 1, 0, newFooterY + 1, 4);
                range.Merged = true;
                range.Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                range.Value = $"{GetResource("BankName")}: {bank1.bankname}";

                range = ws.Cells.GetSubrangeAbsolute(newFooterY + 2, 0, newFooterY + 2, 4);
                range.Merged = true;
                range.Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                range.Value = $"{GetResource("AccountName")}: {bank1.accountname}";

                range = ws.Cells.GetSubrangeAbsolute(newFooterY + 3, 0, newFooterY + 3, 4);
                range.Merged = true;
                range.Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                range.Value = $"{GetResource("Iban")}: {QrBvrPdfGenerator.GetIbanFormatted(bank1.iban)}";

                range = ws.Cells.GetSubrangeAbsolute(newFooterY + 4, 0, newFooterY + 4, 4);
                range.Merged = true;
                range.Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                range.Value = $"{GetResource("Swift")}: {bank1.swift}";

                range = ws.Cells.GetSubrangeAbsolute(newFooterY + 5, 0, newFooterY + 5, 4);
                range.Merged = true;
                range.Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                range.Value = $"{GetResource("Currency")}: {bank1.currency}";
            }
            else if (!String.IsNullOrWhiteSpace(cur) && bank2 != null)
            {
                var rangeForBorder = ws.Cells.GetSubrangeAbsolute(newFooterY, 4, newFooterY, 6);
                rangeForBorder.Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(169, 169, 169), LineStyle.Thin);
                ws.Cells[newFooterY, 4].Value = GetResource("BankDetails");
                ws.Cells[newFooterY, 4].Style.Borders.SetBorders(MultipleBorders.Outside, SpreadsheetColor.FromArgb(169, 169, 169), LineStyle.Thin);

                var range = ws.Cells.GetSubrangeAbsolute(newFooterY + 1, 4, newFooterY + 1, 8);
                range.Merged = true;
                range.Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                range.Value = $"{GetResource("BankName")}: {bank2.bankname}";

                range = ws.Cells.GetSubrangeAbsolute(newFooterY + 2, 4, newFooterY + 2, 8);
                range.Merged = true;
                range.Style.WrapText = true;
                range.Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                range.Value = $"{GetResource("AccountName")}: {bank2.accountname}";

                range = ws.Cells.GetSubrangeAbsolute(newFooterY + 3, 4, newFooterY + 3, 8);
                range.Merged = true;
                range.Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                range.Value = $"{GetResource("Iban")}: {QrBvrPdfGenerator.GetIbanFormatted(bank2.iban)}";

                range = ws.Cells.GetSubrangeAbsolute(newFooterY + 4, 4, newFooterY + 4, 8);
                range.Merged = true;
                range.Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                range.Value = $"{GetResource("Swift")}: {bank2.swift}";

                range = ws.Cells.GetSubrangeAbsolute(newFooterY + 5, 4, newFooterY + 5, 8);
                range.Merged = true;
                range.Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                range.Value = $"{GetResource("Currency")}: {bank2.currency}";
            }

            if (bank1 == null && bank2 == null)
            {
                ws.Cells[newFooterY, 0].Value = "";
                ws.Cells[newFooterY, 0].Style.Borders.SetBorders(MultipleBorders.All, SpreadsheetColor.FromArgb(255, 255, 255), LineStyle.None);
            }

            if (organizations.hide_gesmob_label == false)
            {
                ws.Cells[newFooterY + 10, 0].Value = GetResource("DocGeneratedWithGesMobile");
            }

            // Don't let bank details be broken between pages. Insert page break before bank details if necessary.
            // This actually doesn't work in case when for example description is long, as Excel can't calculate height correctly -
            // it returns standard row height instead of actual content's height
            var rowHeightIn = LengthUnitConverter.Convert(ws.DefaultRowHeight, LengthUnit.Twip, LengthUnit.Inch);
            var pageHeightIn = ws.PrintOptions.PageHeight - ws.PrintOptions.TopMargin - ws.PrintOptions.BottomMargin - ws.PrintOptions.HeaderMargin - 2 * rowHeightIn;
            var footerHeightIn = CalculateHeight(ws, newFooterY, newFooterY + 5, LengthUnit.Inch);
            var contentHeight = CalculateHeight(ws, 0, newFooterY - 1, LengthUnit.Inch);
            var emptySpaceTillPageEnd = pageHeightIn - (contentHeight % pageHeightIn);
            if (emptySpaceTillPageEnd < footerHeightIn || (footerHeightIn - emptySpaceTillPageEnd) > rowHeightIn)
            {
                ws.HorizontalPageBreaks.Add(newFooterY - 1);
            }
        }

        private void BindItems(jobsExtended job, organizations org, string action, out int itemsCount)
        {
            string currency = !String.IsNullOrWhiteSpace(org.currency) ? org.currency : "CHF";
            currency = !String.IsNullOrWhiteSpace(job.currency) ? job.currency.Substring(3) : currency;
            var usePriceIncludingVat = org.use_price_including_vat;
            var row = ws.Rows[itemsLineStart + 1];
            
            var items = job.services_jobs.OrderBy(s => s.position).ToList();
            for (int i = 0; i < items.Count - 3; i++)
            {
                ws.Rows.InsertCopy(itemsLineStart + 1, row);
                ws.Cells[itemsLineStart + 1, 0].Style.Borders.SetBorders(MultipleBorders.None, SpreadsheetColor.FromArgb(255, 255, 255), LineStyle.None);
            }

            itemsCount = Math.Max(3, items.Count);

            itemsLineEnd = Math.Max(21, itemsLineStart + itemsCount);

            Dictionary<double, double> vatSum = new Dictionary<double, double>();
            Dictionary<double, double> vatAmount = new Dictionary<double, double>();

            double totalWithoutTaxes = 0;

            int serviceCustomNumberColIdx = 0;
            int serviceNameColIdx = 1;
            int serviceCodeColIdx = 3;
            int serviceUnitColIdx = org.hide_code_on_pdf ? 3 : 4;
            int serviceQtyColIdx = 5;
            int servicePriceColIdx = 6;
            int discountColIdx = 7;
            int taxRateColIdx = 8;
            int totalWithTaxColIdx = 9;

            for (int i = 0; i < items.Count; i++)
            {
                var quantity = items[i].serviceQuantity ?? 0;

                // Replace internalId from service name
                var serviceName = items[i].services != null ?
                    Regex.Replace(items[i].serviceName.Trim(), $"{items[i].services.internalId} -", "").Trim()
                    : items[i].serviceName;

                if (!hideServiceCustomNumber)
                {
                    ws.Cells[itemsLineStart + i, serviceCustomNumberColIdx].Value = items[i].custom_number?.Trim();
                    ws.Cells[itemsLineStart + i, serviceCustomNumberColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                    ws.Cells[itemsLineStart + i, serviceCustomNumberColIdx].Style.VerticalAlignment = VerticalAlignmentStyle.Top;
                    ws.Cells[itemsLineStart + i, serviceCustomNumberColIdx].Style.WrapText = true;

                    ws.Cells[itemsLineStart + i, serviceNameColIdx].Value = $"{serviceName}{Environment.NewLine}";
                    ws.Cells[itemsLineStart + i, serviceNameColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                    ws.Cells[itemsLineStart + i, serviceNameColIdx].Style.VerticalAlignment = VerticalAlignmentStyle.Top;
                    ws.Cells[itemsLineStart + i, serviceNameColIdx].Style.WrapText = true;
                }
                else
                {
                    // Unmerge what is merged
                    var descriptionContainer = ws.Cells.GetSubrangeAbsolute(itemsLineStart + i, serviceNameColIdx, itemsLineStart + i, serviceNameColIdx + 1);
                    descriptionContainer.Merged = false;

                    // Merge another group of cells
                    descriptionContainer = ws.Cells.GetSubrangeAbsolute(itemsLineStart + i, serviceCustomNumberColIdx, itemsLineStart + i, serviceCustomNumberColIdx + 2);
                    descriptionContainer.Merged = true;
                    descriptionContainer.Value = $"{serviceName}{Environment.NewLine}";
                }
                

                if (org != null && org.articles_bold_pdf)
                {
                    ws.Cells[itemsLineStart + i, serviceNameColIdx].Style.Font.Weight = ExcelFont.BoldWeight;
                }

                if (!org.hide_code_on_pdf)
                {
                    if (items[i].services != null) ws.Cells[itemsLineStart + i, serviceCodeColIdx].Value = items[i].services.internalId;
                    ws.Cells[itemsLineStart + i, serviceCodeColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                    ws.Cells[itemsLineStart + i, serviceCodeColIdx].Style.VerticalAlignment = VerticalAlignmentStyle.Top;
                }

                if (!hideUnit)
                {
                    ws.Cells[itemsLineStart + i, serviceUnitColIdx].Value = items[i].unitName;
                    ws.Cells[itemsLineStart + i, serviceUnitColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                    ws.Cells[itemsLineStart + i, serviceUnitColIdx].Style.VerticalAlignment = VerticalAlignmentStyle.Top;
                }

                ws.Cells[itemsLineStart + i, serviceQtyColIdx].Value = DoubleToString(items[i].serviceQuantity, quantity);
                ws.Cells[itemsLineStart + i, serviceQtyColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                ws.Cells[itemsLineStart + i, serviceQtyColIdx].Style.VerticalAlignment = VerticalAlignmentStyle.Top;
                ws.Cells[itemsLineStart + i, serviceQtyColIdx].Style.WrapText = false;

                double price = 0;
                
                if (!String.IsNullOrWhiteSpace(job.currency))
                {
                    price = items[i].servicePriceInCurrency ?? 0;
                    if (usePriceIncludingVat)
                    {
                        price = items[i].services?.price_including_vat ?? true 
                            ? price
                            : price + price * (items[i].taxRate ?? 0) / 100;
                    }
                }
                else
                {
                    price = items[i].servicePrice ?? 0;
                    if (usePriceIncludingVat)
                    {
                        price = items[i].services?.price_including_vat ?? true 
                            ? price
                            : price + price * (items[i].taxRate ?? 0) / 100;
                    }
                }
                ws.Cells[itemsLineStart + i, servicePriceColIdx].Value = DoubleToString(price, quantity);

                ws.Cells[itemsLineStart + i, servicePriceColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                ws.Cells[itemsLineStart + i, servicePriceColIdx].Style.VerticalAlignment = VerticalAlignmentStyle.Top;


                if (!hideDiscount)
                { 
                    ws.Cells[itemsLineStart + i, discountColIdx].Value = DoubleToString(items[i].discount ?? 0 / 100, quantity);
                    ws.Cells[itemsLineStart + i, discountColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                    ws.Cells[itemsLineStart + i, discountColIdx].Style.VerticalAlignment = VerticalAlignmentStyle.Top;
                }

                if (!hideVat)
                {
                    ws.Cells[itemsLineStart + i, taxRateColIdx].Value = DoubleToStringWithSuffix(items[i].taxRate, quantity, "%");
                    ws.Cells[itemsLineStart + i, taxRateColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                    ws.Cells[itemsLineStart + i, taxRateColIdx].Style.VerticalAlignment = VerticalAlignmentStyle.Top;
                }

                var serviceAmountWithoutTaxes = price * (items[i].serviceQuantity ?? 0) * (100 - items[i].discount ?? 0) / 100;
                totalWithoutTaxes += serviceAmountWithoutTaxes;

                CellRange totalWithTaxContainer = ws.Cells.GetSubrangeAbsolute(itemsLineStart + i, totalWithTaxColIdx, itemsLineStart + i, totalWithTaxColIdx);
                if (hideVat)
                {
                    totalWithTaxContainer = ws.Cells.GetSubrangeAbsolute(itemsLineStart + i, taxRateColIdx, itemsLineStart + i, totalWithTaxColIdx);
                    totalWithTaxContainer.Merged = true;

                    totalWithTaxContainer.Value = DoubleToString(serviceAmountWithoutTaxes, quantity);
                }
                else
                {
                    double? totalPriceIncTax;
                    
                    if (!usePriceIncludingVat)
                        totalPriceIncTax = !String.IsNullOrWhiteSpace(job.currency) ? items[i].totalPriceIncTaxInCurrency : items[i].totalPrinceIncTax;
                    else
                    {
                        if (items[i].services?.price_including_vat ?? true)
                        {
                            totalPriceIncTax = !string.IsNullOrWhiteSpace(job.currency) 
                                ? items[i].totalPriceExclTaxInCurrency 
                                : items[i].totalPriceExclTax;
                        }
                        else
                        {
                            totalPriceIncTax = !string.IsNullOrWhiteSpace(job.currency) 
                                ? items[i].totalPriceIncTaxInCurrency 
                                : items[i].totalPrinceIncTax;
                        } 
                    }
                     
                    totalWithTaxContainer.Value = DoubleToString(totalPriceIncTax, quantity);
                }
                
                totalWithTaxContainer.Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                totalWithTaxContainer.Style.VerticalAlignment = VerticalAlignmentStyle.Top;

                if (items[i].taxAmount != null && items[i].totalPriceExclTax != null && items[i].taxRate != null)
                {
                    if (!vatSum.ContainsKey(items[i].taxRate.Value))
                    {
                        vatSum.Add(items[i].taxRate.Value, 0);
                        vatAmount.Add(items[i].taxRate.Value, 0);
                    }

                    if (!String.IsNullOrWhiteSpace(job.currency))
                    {
                        vatSum[items[i].taxRate.Value] += items[i].totalPriceExclTaxInCurrency.Value;
                        vatAmount[items[i].taxRate.Value] += items[i].totalPriceIncTaxInCurrency.Value - items[i].totalPriceExclTaxInCurrency.Value;
                    }
                    else
                    {
                        vatSum[items[i].taxRate.Value] += items[i].totalPriceExclTax.Value;
                        vatAmount[items[i].taxRate.Value] += items[i].taxAmount.Value;
                    }
                }

                ws.Rows[itemsLineStart + i].AutoFit();
            }

            int lineShift = 1;
            CellRange mergedCells = null;

            var discounts = new List<(float percent, string type)>
            {
                (job.discount_percent.HasValue && job.discount_percent > 0 ?  job.discount_percent.Value : 0, GetResource(((TypeOfDiscountEnum)(job.discount_type ?? 1)).GetDescription())),
                (job.discount_percent2.HasValue && job.discount_percent2 > 0 ?  job.discount_percent2.Value : 0, GetResource(((TypeOfDiscountEnum)(job.discount_type2 ?? 1)).GetDescription())),
                (job.discount_percent3.HasValue && job.discount_percent3 > 0 ?  job.discount_percent3.Value : 0, GetResource(((TypeOfDiscountEnum)(job.discount_type3 ?? 1)).GetDescription())),
                (job.discount_percent4.HasValue && job.discount_percent4 > 0 ?  job.discount_percent4.Value : 0, GetResource(((TypeOfDiscountEnum)(job.discount_type4 ?? 1)).GetDescription())),
            };

            if (discounts.Any(d => d.percent > 0))
            {
                mergedCells = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + ++lineShift, servicePriceColIdx, itemsLineEnd + lineShift, servicePriceColIdx + 1);
                mergedCells.Merged = true;
                mergedCells.Value = $"{GetResource("SubTotal")} HT";
                mergedCells.Style.Font.Weight = ExcelFont.BoldWeight;
                mergedCells.Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

                ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Value = currency;
                ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Value = FormatAmount(job.CalculateTotalExclTax(), true);
                ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

                for (var i = 0; i < discounts.Count; i++)
                {
                    if (discounts[i].percent == 0) continue;

                    mergedCells = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + ++lineShift, servicePriceColIdx, itemsLineEnd + lineShift, servicePriceColIdx + 1);
                    mergedCells.Merged = true;
                    mergedCells.Value = $"{discounts[i].type} {discounts[i].percent:0.00}%";
                    mergedCells.Style.Font.Weight = ExcelFont.BoldWeight;
                    mergedCells.Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

                    ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Value = currency;
                    ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

                    ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Value = FormatAmount(job.CalculateRebaitAmount(i + 1), true);
                    ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                }
            }

            if (!usePriceIncludingVat)
            {
                mergedCells = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + ++lineShift, servicePriceColIdx, itemsLineEnd + lineShift, servicePriceColIdx + 1);
                mergedCells.Merged = true;
                mergedCells.Value = "Total HT";
                mergedCells.Style.Font.Weight = ExcelFont.BoldWeight;
                mergedCells.Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                
                ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Value = currency;
                ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Value = FormatAmount(job.CalculateTotalExclTaxDiscounted(), true);
                ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
            }

            var vatsPerRate = job.CalculateVATPerRate(true, usePriceIncludingVat);

            if (vatsPerRate.Count > 0)
            {
                var rangeForBorder = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + lineShift + 1, servicePriceColIdx, itemsLineEnd + lineShift + vatsPerRate.Count, totalWithTaxColIdx);
                rangeForBorder.Style.Borders.SetBorders(MultipleBorders.Top | MultipleBorders.Bottom, SpreadsheetColor.FromArgb(169, 169, 169), LineStyle.Thin);
            }

            foreach (var vat in vatsPerRate)
            {
                var vatFromDisc = vat.Item2;
                var vatAmountDisc = vat.Item3;

                mergedCells = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + ++lineShift, servicePriceColIdx, itemsLineEnd + lineShift, servicePriceColIdx + 1);
                mergedCells.Merged = true;
                mergedCells.Value = vatsPerRate.Count > 1 ?
                        $"{(usePriceIncludingVat && usePriceIncludingVat ? GetResource("IncludingVat") : GetResource("VAT"))} {vat.Item1:0.00}% / {FormatAmount(vatFromDisc, true)}" :
                        $"{(usePriceIncludingVat && usePriceIncludingVat ? GetResource("IncludingVat") : GetResource("VAT"))} {vat.Item1:0.00}%";
                mergedCells.Style.Font.Weight = ExcelFont.BoldWeight;
                mergedCells.Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

                ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Value = currency;
                ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

                ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Value = FormatAmount(vatAmountDisc, true);
                ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
            }

            if (org.round_amount)
            {
                var roundAmount = Math.Round(job.CalculateTotal(true) - job.CalculateTotal(false), 2, MidpointRounding.AwayFromZero);

                if(roundAmount != 0)
                {
                    mergedCells = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + ++lineShift, servicePriceColIdx, itemsLineEnd + lineShift, servicePriceColIdx + 1);
                    mergedCells.Merged = true;
                    mergedCells.Value = GetResource("RoundedAmount");
                    mergedCells.Style.Font.Weight = ExcelFont.BoldWeight;
                    mergedCells.Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

                    ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Value = currency;
                    ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

                    ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Value = FormatAmount(roundAmount, true);
                    ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                }
            }

            mergedCells = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + ++lineShift, servicePriceColIdx, itemsLineEnd + lineShift, servicePriceColIdx + 1);
            mergedCells.Merged = true;
            mergedCells.Value = "Total TTC";
            mergedCells.Style.Font.Weight = ExcelFont.BoldWeight;
            mergedCells.Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

            ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Value = currency;
            ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

            ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Value = FormatAmount(job.CalculateTotal(org.round_amount, true, usePriceIncludingVat), true);
            ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

            double due = job.CalculateDue(org.round_amount, true, usePriceIncludingVat);
            double paid = job.CalculatePayments();

            if(paid > 0)
            {
                mergedCells = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + ++lineShift, servicePriceColIdx, itemsLineEnd + lineShift, servicePriceColIdx + 1);
                mergedCells.Merged = true;
                mergedCells.Value = GetResource("AlreadyPaid");
                mergedCells.Style.Font.Weight = ExcelFont.BoldWeight;
                mergedCells.Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

                ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Value = currency;
                ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

                ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Value = FormatAmount(paid > 0 ? Math.Round(paid, 2, MidpointRounding.AwayFromZero) : 0, true);
                ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
            }
            
            mergedCells = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + ++lineShift, servicePriceColIdx, itemsLineEnd + lineShift, servicePriceColIdx + 1);
            mergedCells.Merged = true;
            mergedCells.Value = GetResource("Due");
            mergedCells.Style.Font.Weight = ExcelFont.BoldWeight;
            mergedCells.Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

            ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Value = currency;
            ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

            ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Value = FormatAmount(Math.Round(due, 2, MidpointRounding.AwayFromZero), true);
            ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;


            var toPayBefore = GetResource("ToPayBefore") + ": ";
            string delayInvoice;

            if (!string.IsNullOrEmpty(action) && action.Equals("sendreminder", StringComparison.InvariantCulture))
            {
                delayInvoice = toPayBefore + DateTime.Now.AddDays(job.organizations.unpaid_invoice_reminder_days).ToString("dd/MM/yyyy");
            }
            else if (job.dateInvoiceDue != null)
            {
                delayInvoice = toPayBefore + job.dateInvoiceDue.Value.ToString("dd/MM/yyyy");
            }
            else
            {
                delayInvoice = toPayBefore + DateTime.Now.AddDays(job.organizations.delayInvoice??0).ToString("dd/MM/yyyy");
            }

            lineShift++;
            var mergedRange = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + lineShift, 0, itemsLineEnd + lineShift, totalWithTaxColIdx);
            mergedRange.Merged = true;
            mergedRange.Value = delayInvoice;

            string footerId = "PdfInvoiceFooter";
            string listenToYouFooter = GetResource("Email_Default_" + footerId);

            var tplMgr = new Calani.BusinessObjects.Settings.DocumentTemplateManager(_userId, Culture);
            var tpl = tplMgr.Get(footerId);
            listenToYouFooter = tpl.DefaultContent;
            if (tpl.UserTemplate != null && !tpl.UserTemplate.content.Trim().Equals("Votre texte personnalisé"))
            {
                listenToYouFooter = tpl.UserTemplate.content;
            }

            lineShift++;
            mergedRange = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + lineShift, 0, itemsLineEnd + lineShift, totalWithTaxColIdx);
            mergedRange.Merged = true;
            mergedRange.Value = listenToYouFooter;

            ws.Rows[itemsLineEnd + lineShift].AutoFit(true);

            itemsLineEnd += lineShift;
        }

        private void BindInvoiceInfo(jobs job, bool hideCode)
        {
            ws.Cells["A11"].Value = GetResource("Invoice") + " #" + job.internalId;
            if (Reminder > 0)
            {
                ws.Cells["A11"].Value += " / ";
                ws.Cells["A11"].Value += Reminder + GetResource("nth");
                ws.Cells["A11"].Value += " ";
                ws.Cells["A11"].Value += GetResource("Reminder");

                ws.Cells["A12"].Value = GetResource("Date") + " : ";
                ws.Cells["A12"].Value += DateTime.Now.ToString("dd/MM/yyyy");

                if (job.dateInvoicingOpen.HasValue)
                {
                    ws.Cells["C12"].Value = GetResource("InvoiceDate") + " : ";
                    ws.Cells["C12"].Value += job.dateInvoicingOpen.Value.ToString("dd/MM/yyyy");
                }
            }
            else if (job.dateInvoicingOpen.HasValue)
            {
                ws.Cells["A12"].Value = GetResource("Date") + " : ";

                ws.Cells["A12"].Value += job.dateInvoicingOpen.Value.ToString("dd/MM/yyyy");
            }
            else if  (job.dateCreated.HasValue)
            {
                ws.Cells["A12"].Value = GetResource("Date") + " : ";

                ws.Cells["A12"].Value += job.dateCreated.Value.ToString("dd/MM/yyyy");
            }

            if (job.clients != null && !String.IsNullOrWhiteSpace(job.clients.vatNumber))
            {
                ws.Cells["G16"].Value = GetResource("VATNumber") + ":";
                ws.Cells["H16"].Value = job.clients.vatNumber;
            }

            var custRefStr = job.GetCustomerReference(Culture);
            if (custRefStr.Length > 0)
            {
                ws.Cells["A13"].Value += custRefStr + Environment.NewLine;
            }

            var locationOfIntervention = job.GetLocationOfIntervention(Culture).Replace("\n", string.Empty);
            if (locationOfIntervention.Length > 0)
            {
                ws.Cells["A13"].Value += locationOfIntervention + Environment.NewLine;
            }

            var typeOfIntervention = job.GetTypeOfIntervention(Culture);
            if (typeOfIntervention.Length > 0)
            {
                ws.Cells["A13"].Value += typeOfIntervention + Environment.NewLine;
            }

            ws.Cells["A13"].Value += job.description;
            ws.Cells["A13"].Style.WrapText = true;

            // Make long description fit
            ws.Rows[12].AutoFit(true);

            ws.Cells["A19"].Value = "#";
            
            if (hideCode)
            {
                var mergedUnit = ws.Cells.GetSubrangeAbsolute(18, 3, 18, 4); 
                mergedUnit.Merged = true;
                mergedUnit.Value = GetResource("Unit");
                mergedUnit.Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
            }
            else
            {
                ws.Cells["D19"].Value = GetResource("Code");
                ws.Cells["E19"].Value = GetResource("Unit");
            }
            
            ws.Cells["F19"].Value = GetResource("Qty");
            ws.Cells["F19"].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
            ws.Cells["G19"].Value = job.organizations.use_price_including_vat 
                ? GetResource("Unit_Price_IncTax")
                : GetResource("Unit_Price_NoTax");
            
            ws.Cells["H19"].Value = GetResource("Discount_P");

            if (hideServiceCustomNumber)
            {
                // Firstly unmerged what is merged
                var descriptionHeaderContainer = ws.Cells.GetSubrangeAbsolute(18, 1, 18, 2);
                descriptionHeaderContainer.Value = null;
                descriptionHeaderContainer.Merged = false;

                // Merge another group of cells
                descriptionHeaderContainer = ws.Cells.GetSubrangeAbsolute(18, 0, 18, 2);
                descriptionHeaderContainer.Merged = true;
                descriptionHeaderContainer.Value = GetResource("Description");
            }
            else
            {
                ws.Cells["B19"].Value = GetResource("Description");
            }

            if (hideDiscount)
            {
                ws.Cells["H19"].Value = string.Empty;
                ws.Cells["H19"].Style.Borders[IndividualBorder.Right].LineStyle = LineStyle.None;
            }

            if (hideUnit)
            {
                ws.Cells["E19"].Value = string.Empty;
                ws.Cells["E19"].Style.Borders[IndividualBorder.Right].LineStyle = LineStyle.None;
            }

            if (hideVat)
            {
                ws.Cells["I19"].Value = string.Empty;
                ws.Cells["I19"].Style.Borders[IndividualBorder.Right].LineStyle = LineStyle.None;
            }
            else
            {
                ws.Cells["I19"].Value = GetResource("VAT");
            }

            ws.Cells["J19"].Value = GetResource("Amount");
            if (hideVat)
            {
                ws.Cells["J19"].Value = GetResource("Amount") + " HT";
            } 
            if (job.organizations.use_price_including_vat)
            {
                ws.Cells["J19"].Value = GetResource("TotalTTC");
            }

        }

        private void BindYourCustomer(clients cust, addresses custAddr, contacts custContact,
            bool showContactInfoInvoices)
        {
            if(cust != null)
                CommonToolsGenerator.BindYourCustomer(ws, cust, custAddr, custContact, showContactInfoInvoices);

        }

        private void BindAttachments(ExcelFile ef, jobs job, List<long> attIds)
        {
            List<attachments> attachments = new List<attachments>();
            if (attIds != null)
            {
                attachments = job.attachments.OrderByDescending(a => a.date)
                    .Where(a => a.type != (int)AttachmentTypeEnum.DeliveryNoteSign && attIds.Contains(a.id)).ToList();
            }
            else
            {
                attachments = job.attachments.OrderByDescending(a => a.date)
                    .Where(a => a.type != (int)AttachmentTypeEnum.DeliveryNoteSign && (a.properties & (int)AttachmentProperties.IncludeToPdf) == (int)AttachmentProperties.IncludeToPdf).ToList();
            }


            var counter = 1;
            foreach (var att in attachments)
            {
                if (att != null)
                {
                    var extJob = new jobsExtended(job);
                    var mgr = new AttachmentsManager(true);
                    mgr.OrganizationId = att.organizationId.Value;


                    var path = mgr.GetPath(att.id);

                    if (!String.IsNullOrWhiteSpace(path))

                    {
                        byte[] imageFile = System.IO.File.ReadAllBytes(path);

                        var imgr = new Generic.ImageManager();
                        imgr.MaxSide = 600;
                        imgr.Init(imageFile);

                        byte[] imgdata = imgr.Save(System.Drawing.Imaging.ImageFormat.Jpeg);

                        MemoryStream imgms = new MemoryStream(imgdata);
                        var img = System.Drawing.Image.FromStream(imgms);

                        double w = img.Width;
                        double h = img.Height;

                        if (w > 10)
                        {
                            var sheet = ef.Worksheets.Add(counter.ToString());
                            sheet.Pictures.Add(imgms, ExcelPictureFormat.Png, "A9", w, h, LengthUnit.Pixel);
                            _ms.Add(imgms);
                            counter++;
                        }
                    }



                }
            }
        }

        private bool BindYourIdentity(Model.organizations org)
        {
            bool isHorizontalLogo = false;

            int adrType = Convert.ToInt32(Contacts.AddressTypeEnum.CompanyAddress);
            Model.addresses addr = (from r in org.addresses where r.type == adrType select r).FirstOrDefault();

            if (org.logo != null)
            {
                bool imageOk = false;
                try
                {
                    MemoryStream msLogoTest = new MemoryStream(org.logo);
                    var imgLogoTest = System.Drawing.Image.FromStream(msLogoTest);
                    if (imgLogoTest.Width > 0 && imgLogoTest.Height > 0) imageOk = true;
                    msLogoTest.Dispose();
                }
                catch { }

                if (imageOk)
                {
                    MemoryStream msLogo = new MemoryStream(org.logo);
                    _ms.Add(msLogo);
                    (double newWidth, double newHeight, bool isHorizontal) = GetLogoSize(msLogo);
                    double left = isHorizontal ? 360 : 460;
                    double top = isHorizontal ? 0 : 30;
                    ws.Pictures.Add(msLogo, ExcelPictureFormat.Png, left, top, newWidth, newHeight, LengthUnit.Pixel);
                    isHorizontalLogo = isHorizontal;
                }

                ws.Cells["A1"].Value = org.name;
                if (addr != null)
                {
                    if (addr.street == null || addr.nb == null || addr.npa == null || addr.country == null || addr.city == null)
                    {
                        throw new Exception(GetResource("ERR_address_not_set"));
                    }
                    
                    ws.Cells["A2"].Value = addr.street.Trim();
                    if (!String.IsNullOrWhiteSpace(addr.nb)) ws.Cells["A2"].Value += " " + addr.nb;
                    ws.Cells["A3"].Value = addr.npa + " " + addr.city;
                    if (!String.IsNullOrEmpty(addr.country)) ws.Cells["A3"].Value += ", " + addr.country;
                }

                int counter = 4;
                if (!String.IsNullOrWhiteSpace(org.website))
                {
                    ws.Cells["A" + counter].Value = org.website;
                    counter++;
                }
                if (!String.IsNullOrWhiteSpace(org.email))
                {
                    ws.Cells["A" + counter].Value = GetResource("Email") + ": " + org.email;
                    counter++;
                }
                if (!String.IsNullOrWhiteSpace(org.phone))
                {
                    ws.Cells["A" + counter].Value = GetResource("Phone") + ": " + org.phone;
                    counter++;
                }
                if (!String.IsNullOrWhiteSpace(org.vatnumber))
                {
                    ws.Cells["A" + counter].Value = GetResource("VAT") + ": " + org.vatnumber;
                    counter++;
                }
            }

            return isHorizontalLogo;
        }

    }
}
