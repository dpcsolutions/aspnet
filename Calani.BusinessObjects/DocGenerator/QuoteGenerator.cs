﻿using Calani.BusinessObjects.Attachments;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Projects;
using GemBox.Spreadsheet;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Calani.BusinessObjects.CustomerAdmin;

namespace Calani.BusinessObjects.DocGenerator
{
    public class QuoteGenerator : PdfGenerator
    {
        public long ProjectId { get; set; }

        public static string ExcelTemplates { get; set; }

        public CultureInfo Culture { get; set; }

        public bool ExcelMode { get; set; }

        public Model.jobs CustomJob { get; set; }

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        long _userId;

        public QuoteGenerator(System.Globalization.CultureInfo culture, long userId)
        {
            _userId = userId;
            Culture = culture;
        }

        List<MemoryStream> _ms = new List<MemoryStream>();

        private string GetResource(string n)
        {
            if (SharedResource.Resource == null) return n;
            return SharedResource.Resource.GetString(n, Culture);
        }

        ExcelWorksheet ws;

        int itemsLineStart = 17;
        int itemsLineEnd = 17;


        public byte[] GenerateDocument(List<long> attIds, TypeOfDeliveryNotePdfEnum? deliveryNoteType = null)
        {
            var db = new Model.CalaniEntities();
            Model.jobs dbjob = null;
            if (CustomJob != null)
            {
                dbjob = CustomJob;
            }
            else
            {
                dbjob = (from r in db.jobs.Include("services_jobs").Include("services_jobs.services") where r.id == ProjectId select r).FirstOrDefault();
            }

            var job = new jobsExtended(dbjob, false);
            
            var orgMgr = new OrganizationManager(job.organizationId.Value);
            orgMgr.Load();
            
            hideDiscount = job.organizations.hide_0_discount_col && job.GetTotalItemsDiscount() == 0;
            hideUnit = job.organizations.hide_unit_column;
            hideVat = NeedHideVat(job);
            hideServiceCustomNumber = NeedHideServiceCustomNumber(job);

            SpreadsheetInfo.SetLicense(GemboxAPI.SpreadsheetLicense);

            ExcelFile ef = ExcelFile.Load(Path.Combine(ExcelTemplates, "Quote.xlsx"), LoadOptions.XlsxDefault);

            bool isDeliveryNote = job.subtype != null && job.subtype == "DeliveryNote";

            string title = isDeliveryNote ? GetResource("DeliveryNote") : GetResource("Quote");

            ws = ef.Worksheets.First();
            string sheetname = title + " " + (job.internalId ?? string.Empty)
                .Replace(":", "")
                .Replace("\0", "")
                .Replace("\x3", "")
                .Replace("\\", "")
                .Replace("/", "")
                .Replace("?", "")
                .Replace("*", "");
            if (sheetname.Length > 30) 
                sheetname = sheetname.Substring(0, 30);

            ws.Name = sheetname;
            ws.PrintOptions.BottomMargin = 0.5;

            if (job.organizations == null && job.organizationId > 0)
            {
                job.organizations = (from r in db.organizations where r.id == job.organizationId select r).FirstOrDefault();
            }

            var isHorizontalLogo = BindYourIdentity(job.organizations);


            var cust = (from r in db.clients where r.id == job.clientId select r).FirstOrDefault();
            var custAddr = (from r in db.addresses where r.id == job.clientAddress select r).FirstOrDefault();
            var custContact = (from r in db.contacts where r.id == job.clientContact select r).FirstOrDefault();

            bool isPricedDeliveryNote = isDeliveryNote && ((deliveryNoteType.HasValue ? deliveryNoteType.Value : (TypeOfDeliveryNotePdfEnum)job.organizations.delivery_note_pdf_type) == TypeOfDeliveryNotePdfEnum.Priced);

            BindYourCustomer(cust, custAddr, custContact, job.organizations.show_contact_info_invoices);

            BindQuoteInfo(job, isDeliveryNote && !isPricedDeliveryNote, orgMgr.HideCodeOnPdf);

            var lastLineIndex = BindItems(ef, job, job.organizations, isPricedDeliveryNote);

            if (!isDeliveryNote)
            {
                BindAcceptanceBlock(job.organizations, lastLineIndex);
            }

            BindAttachments(ef, job, attIds);

            //BindPaymentFooter(job.organizations);

            MemoryStream ms = new MemoryStream();
            if (ExcelMode)
            {
                ef.Save(ms, SaveOptions.XlsxDefault);
            }
            else
            {
                var options = SaveOptions.PdfDefault;
                options.SelectionType = SelectionType.EntireFile;

                ef.Save(ms, options);
            }
            byte[] ret = ms.ToArray();
            ms.Dispose();

            foreach (var ms2 in _ms)
            {
                ms2.Dispose();
            }


            return ret;
        }


        int footerHeight = 6;
        int pageHeight = 48;

        private void BindPaymentFooter(organizations organizations)
        {
            int actualFooterY = itemsLineEnd + 9;
            int footerY = pageHeight - footerHeight;

            int newFooterY = actualFooterY;

            var rowBlank = ws.Rows[itemsLineEnd + 8];
            while (newFooterY % footerY != 0)
            {
                ws.Rows.InsertCopy(itemsLineEnd + 8, rowBlank);
                newFooterY++;
            }

            if (newFooterY > 49)
            {
                for (int i = 0; i < 9; i++)
                {
                    ws.Rows.InsertCopy(itemsLineEnd + 8, rowBlank);
                    newFooterY++;
                }
            }

            var bank1 = (from r in organizations.bankaccount where r.priority == 1 && !String.IsNullOrWhiteSpace(r.bankname) select r).FirstOrDefault();
            if (bank1 != null)
            {
                ws.Cells[newFooterY + 1, 1].Value = bank1.bankname;
                ws.Cells[newFooterY + 2, 1].Value = bank1.accountname;
                ws.Cells[newFooterY + 3, 1].Value = bank1.iban;
                ws.Cells[newFooterY + 4, 1].Value = bank1.swift;
                ws.Cells[newFooterY + 5, 1].Value = bank1.currency;
                ws.Cells[newFooterY + 1, 1].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                ws.Cells[newFooterY + 2, 1].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                ws.Cells[newFooterY + 3, 1].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                ws.Cells[newFooterY + 4, 1].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                ws.Cells[newFooterY + 5, 1].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;

                ws.Cells[newFooterY + 1, 0].Value = GetResource("BankName") + ":";
                ws.Cells[newFooterY + 2, 0].Value = GetResource("AccountName") + ":";
                ws.Cells[newFooterY + 3, 0].Value = GetResource("Iban") + ":";
                ws.Cells[newFooterY + 4, 0].Value = GetResource("Swift") + ":";
                ws.Cells[newFooterY + 5, 0].Value = GetResource("Currency") + ":";


            }
            else
            {
                ws.Cells[newFooterY + 1, 0].Value = "";
                ws.Cells[newFooterY + 2, 0].Value = "";
                ws.Cells[newFooterY + 3, 0].Value = "";
                ws.Cells[newFooterY + 4, 0].Value = "";
                ws.Cells[newFooterY + 5, 0].Value = "";

                ws.Cells[newFooterY + 1, 1].Value = "";
                ws.Cells[newFooterY + 2, 1].Value = "";
                ws.Cells[newFooterY + 3, 1].Value = "";
                ws.Cells[newFooterY + 4, 1].Value = "";
                ws.Cells[newFooterY + 5, 1].Value = "";


            }

            var bank2 = (from r in organizations.bankaccount where r.priority == 2 && !String.IsNullOrWhiteSpace(r.bankname) select r).FirstOrDefault();
            if (bank2 != null)
            {
                ws.Cells[newFooterY + 1, 5].Value = bank2.bankname;
                ws.Cells[newFooterY + 2, 5].Value = bank2.accountname;
                ws.Cells[newFooterY + 3, 5].Value = bank2.iban;
                ws.Cells[newFooterY + 4, 5].Value = bank2.swift;
                ws.Cells[newFooterY + 5, 5].Value = bank2.currency;
                ws.Cells[newFooterY + 1, 5].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                ws.Cells[newFooterY + 2, 5].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                ws.Cells[newFooterY + 3, 5].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                ws.Cells[newFooterY + 4, 5].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                ws.Cells[newFooterY + 5, 5].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;

                ws.Cells[newFooterY + 1, 3].Value = GetResource("Bank") + ":";
                ws.Cells[newFooterY + 2, 3].Value = GetResource("BankAccountName") + ":";
                ws.Cells[newFooterY + 3, 3].Value = GetResource("Iban") + ":";
                ws.Cells[newFooterY + 4, 3].Value = GetResource("Swift") + ":";
                ws.Cells[newFooterY + 5, 3].Value = GetResource("Currency") + ":";
            }
            else
            {
                ws.Cells[newFooterY + 1, 3].Value = "";
                ws.Cells[newFooterY + 2, 3].Value = "";
                ws.Cells[newFooterY + 3, 3].Value = "";
                ws.Cells[newFooterY + 4, 3].Value = "";
                ws.Cells[newFooterY + 5, 3].Value = "";

                ws.Cells[newFooterY + 1, 5].Value = "";
                ws.Cells[newFooterY + 2, 5].Value = "";
                ws.Cells[newFooterY + 3, 5].Value = "";
                ws.Cells[newFooterY + 4, 5].Value = "";
                ws.Cells[newFooterY + 5, 5].Value = "";
            }

            if (bank1 == null && bank2 == null)
            {
                ws.Cells[newFooterY, 0].Value = "";
                ws.Cells[newFooterY, 0].Style.Borders.SetBorders(MultipleBorders.All, SpreadsheetColor.FromArgb(255, 255, 255), LineStyle.None);
            }
        }

        private int BindItems(ExcelFile ef, jobsExtended job, organizations org, bool isPricedDeliveryNote)
        {
            string currency = !String.IsNullOrWhiteSpace(org.currency) ? org.currency : "CHF";
            currency = !String.IsNullOrWhiteSpace(job.currency) ? job.currency.Substring(3) : currency;
            var usePriceIncludingVat = org.use_price_including_vat;

            bool isDeliveryNote = job.subtype != null && job.subtype == "DeliveryNote";

            var row = ws.Rows[itemsLineStart + 1];

            var items = job.services_jobs.OrderBy(s => s.position).ToList();
            for (int i = 0; i < items.Count - 3; i++)
            {
                ws.Rows.InsertCopy(itemsLineStart + 1, row);
                ws.Cells[itemsLineStart + 1, 0].Style.Borders.SetBorders(MultipleBorders.None, SpreadsheetColor.FromArgb(255, 255, 255), LineStyle.None);
            }

            
            itemsLineEnd = itemsLineStart + Math.Max(3, items.Count);

            Dictionary<double, double> vatSum = new Dictionary<double, double>();
            Dictionary<double, double> vatAmount = new Dictionary<double, double>();

            int serviceCustomNumberColIdx = 0;
            int serviceNameColIdx = 1;
            int serviceCodeColIdx = 3;
            int serviceUnitColIdx = org.hide_code_on_pdf ? 3 : 4;
            int serviceQtyColIdx = 5;
            int servicePriceColIdx = 6;
            int discountColIdx = 7;
            int taxRateColIdx = 8;
            int totalWithTaxColIdx = 9;

            for (int i = 0; i < items.Count; i++)
            {
                var quantity = items[i].serviceQuantity ?? 0;

                // Replace internalId from service name
                var serviceName = items[i].services != null ?
                    Regex.Replace(items[i].serviceName.Trim(), $"{items[i].services.internalId} -", "").Trim()
                    : items[i].serviceName;

                if (!hideServiceCustomNumber)
                {
                    ws.Cells[itemsLineStart + i, serviceCustomNumberColIdx].Value = items[i].custom_number?.Trim();
                    ws.Cells[itemsLineStart + i, serviceCustomNumberColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                    ws.Cells[itemsLineStart + i, serviceCustomNumberColIdx].Style.VerticalAlignment = VerticalAlignmentStyle.Top;
                    ws.Cells[itemsLineStart + i, serviceCustomNumberColIdx].Style.WrapText = true;

                    ws.Cells[itemsLineStart + i, serviceNameColIdx].Value = $"{serviceName}{Environment.NewLine}";
                    ws.Cells[itemsLineStart + i, serviceNameColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                    ws.Cells[itemsLineStart + i, serviceNameColIdx].Style.VerticalAlignment = VerticalAlignmentStyle.Top;
                    ws.Cells[itemsLineStart + i, serviceNameColIdx].Style.WrapText = true;
                }
                else
                {
                    // Unmerge what is merged
                    var descriptionContainer = ws.Cells.GetSubrangeAbsolute(itemsLineStart + i, serviceNameColIdx, itemsLineStart + i, serviceNameColIdx + 1);
                    descriptionContainer.Merged = false;

                    // Merge another group of cells
                    descriptionContainer = ws.Cells.GetSubrangeAbsolute(itemsLineStart + i, serviceCustomNumberColIdx, itemsLineStart + i, serviceCustomNumberColIdx + 2);
                    descriptionContainer.Merged = true;
                    descriptionContainer.Value = $"{serviceName}{Environment.NewLine}";
                }

                if (org != null && org.articles_bold_pdf)
                {
                    ws.Cells[itemsLineStart + i, serviceNameColIdx].Style.Font.Weight = ExcelFont.BoldWeight;
                }

                if (!org.hide_code_on_pdf)
                {
                    if (items[i].services != null) ws.Cells[itemsLineStart + i, serviceCodeColIdx].Value = items[i].services.internalId;
                    ws.Cells[itemsLineStart + i, serviceCodeColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                    ws.Cells[itemsLineStart + i, serviceCodeColIdx].Style.VerticalAlignment = VerticalAlignmentStyle.Top;
                }

                if (!hideUnit && (!isDeliveryNote || isPricedDeliveryNote))
                {
                    ws.Cells[itemsLineStart + i, serviceUnitColIdx].Value = items[i].unitName;
                    ws.Cells[itemsLineStart + i, serviceUnitColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
                    ws.Cells[itemsLineStart + i, serviceUnitColIdx].Style.VerticalAlignment = VerticalAlignmentStyle.Top;
                }

                ws.Cells[itemsLineStart + i, serviceQtyColIdx].Value = DoubleToString(items[i].serviceQuantity, quantity);
                ws.Cells[itemsLineStart + i, serviceQtyColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                ws.Cells[itemsLineStart + i, serviceQtyColIdx].Style.VerticalAlignment = VerticalAlignmentStyle.Top;
                ws.Cells[itemsLineStart + i, serviceQtyColIdx].Style.WrapText = false;

                if (isDeliveryNote && !isPricedDeliveryNote)
                {
                    ws.Cells[itemsLineStart + i, servicePriceColIdx].Value = "";
                    ws.Cells[itemsLineStart + i, discountColIdx].Value = "";
                    ws.Cells[itemsLineStart + i, taxRateColIdx].Value = "";
                }
                else
                {
                    double servicePrice = 0;
                
                    if (!String.IsNullOrWhiteSpace(job.currency))
                    {
                        servicePrice = items[i].servicePriceInCurrency ?? 0;
                        if (usePriceIncludingVat)
                        {
                            servicePrice = items[i].services?.price_including_vat ?? true 
                                ? servicePrice
                                : servicePrice + servicePrice * (items[i].taxRate ?? 0) / 100;
                        }
                    }
                    else
                    {
                        servicePrice = items[i].servicePrice ?? 0;
                        if (usePriceIncludingVat)
                        {
                            var vatIncluded = items[i].services?.price_including_vat ?? true;
                            servicePrice = vatIncluded
                                ? servicePrice
                                : servicePrice + servicePrice * (items[i].taxRate ?? 0) / 100;
                        }
                    }
                    
                    ws.Cells[itemsLineStart + i, servicePriceColIdx].Value = DoubleToString(servicePrice, quantity);

                    ws.Cells[itemsLineStart + i, servicePriceColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                    ws.Cells[itemsLineStart + i, servicePriceColIdx].Style.VerticalAlignment = VerticalAlignmentStyle.Top;


                    if (!hideDiscount)
                    {
                        ws.Cells[itemsLineStart + i, discountColIdx].Value = DoubleToString(items[i].discount ?? 0 / 100, quantity);
                    }
                    
                    ws.Cells[itemsLineStart + i, discountColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                    ws.Cells[itemsLineStart + i, discountColIdx].Style.VerticalAlignment = VerticalAlignmentStyle.Top;

                    if (!hideVat)
                    {
                        ws.Cells[itemsLineStart + i, taxRateColIdx].Value = DoubleToStringWithSuffix(items[i].taxRate, quantity, "%");
                        ws.Cells[itemsLineStart + i, taxRateColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                        ws.Cells[itemsLineStart + i, taxRateColIdx].Style.VerticalAlignment = VerticalAlignmentStyle.Top;
                    }

                    CellRange totalWithTaxContainer = ws.Cells.GetSubrangeAbsolute(itemsLineStart + i, totalWithTaxColIdx, itemsLineStart + i, totalWithTaxColIdx);
                    if (hideVat)
                    {
                        totalWithTaxContainer = ws.Cells.GetSubrangeAbsolute(itemsLineStart + i, taxRateColIdx, itemsLineStart + i, totalWithTaxColIdx);
                        totalWithTaxContainer.Merged = true;

                        var totalPriceExclTax = !String.IsNullOrWhiteSpace(job.currency) ? items[i].totalPriceExclTaxInCurrency : items[i].totalPriceExclTax;
                        totalWithTaxContainer.Value = DoubleToString(totalPriceExclTax, quantity);
                    }
                    else
                    {
                        double? totalPriceIncTax;
                    
                        if (!usePriceIncludingVat)
                            totalPriceIncTax = !String.IsNullOrWhiteSpace(job.currency) ? items[i].totalPriceIncTaxInCurrency : items[i].totalPrinceIncTax;
                        else
                        {
                            if (items[i].services?.price_including_vat ?? true)
                            {
                                totalPriceIncTax = !string.IsNullOrWhiteSpace(job.currency) 
                                    ? items[i].totalPriceExclTaxInCurrency 
                                    : items[i].totalPriceExclTax;
                            }
                            else
                            {
                                totalPriceIncTax = !string.IsNullOrWhiteSpace(job.currency) 
                                    ? items[i].totalPriceIncTaxInCurrency 
                                    : items[i].totalPrinceIncTax;
                            } 
                        }
                        
                        totalWithTaxContainer.Value = DoubleToString(totalPriceIncTax, quantity);
                    }

                    totalWithTaxContainer.Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                    totalWithTaxContainer.Style.VerticalAlignment = VerticalAlignmentStyle.Top;

                    if (items[i].taxAmount != null && items[i].totalPriceExclTax != null && items[i].taxRate != null)
                    {
                        if (!vatSum.ContainsKey(items[i].taxRate.Value))
                        {
                            vatSum.Add(items[i].taxRate.Value, 0);
                            vatAmount.Add(items[i].taxRate.Value, 0);
                        }

                        if (!String.IsNullOrWhiteSpace(job.currency))
                        {
                            vatSum[items[i].taxRate.Value] += items[i].totalPriceExclTaxInCurrency.Value;
                            vatAmount[items[i].taxRate.Value] += items[i].totalPriceIncTaxInCurrency.Value - items[i].totalPriceExclTaxInCurrency.Value;
                        }
                        else
                        {
                            vatSum[items[i].taxRate.Value] += items[i].totalPriceExclTax.Value;
                            vatAmount[items[i].taxRate.Value] += items[i].taxAmount.Value;
                        }
                    }
                }

                ws.Rows[itemsLineStart + i].AutoFit();
            }

            var lineShift = 0;
            CellRange mergedCells = null;

            var discounts = new List<(float percent, string type)>
            {
                (job.discount_percent.HasValue && job.discount_percent > 0 ?  job.discount_percent.Value : 0, GetResource(((TypeOfDiscountEnum)(job.discount_type ?? 1)).GetDescription())),
                (job.discount_percent2.HasValue && job.discount_percent2 > 0 ?  job.discount_percent2.Value : 0, GetResource(((TypeOfDiscountEnum)(job.discount_type2 ?? 1)).GetDescription())),
                (job.discount_percent3.HasValue && job.discount_percent3 > 0 ?  job.discount_percent3.Value : 0, GetResource(((TypeOfDiscountEnum)(job.discount_type3 ?? 1)).GetDescription())),
                (job.discount_percent4.HasValue && job.discount_percent4 > 0 ?  job.discount_percent4.Value : 0, GetResource(((TypeOfDiscountEnum)(job.discount_type4 ?? 1)).GetDescription())),
            };

            if (!isDeliveryNote || isPricedDeliveryNote)
            {
                if (discounts.Any(d => d.percent > 0))
                {
                    mergedCells = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + lineShift, servicePriceColIdx, itemsLineEnd + lineShift, servicePriceColIdx + 1);
                    mergedCells.Merged = true;
                    mergedCells.Value = $"{GetResource("SubTotal")} HT";
                    mergedCells.Style.Font.Weight = ExcelFont.BoldWeight;
                    mergedCells.Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

                    ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Value = currency;
                    ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                    ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Value = FormatAmount(job.CalculateTotalExclTax(), true);
                    ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

                    for (var i = 0; i < discounts.Count; i++)
                    {
                        if (discounts[i].percent == 0) continue;

                        mergedCells = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + ++lineShift, servicePriceColIdx, itemsLineEnd + lineShift, servicePriceColIdx + 1);
                        mergedCells.Merged = true;
                        mergedCells.Value = $"{discounts[i].type} {discounts[i].percent:0.00}%";
                        mergedCells.Style.Font.Weight = ExcelFont.BoldWeight;
                        mergedCells.Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

                        ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Value = currency;
                        ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

                        ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Value = FormatAmount(job.CalculateRebaitAmount(i + 1), true);
                        ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                    }
                }

                if (!usePriceIncludingVat)
                {
                    mergedCells = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + ++lineShift, servicePriceColIdx, itemsLineEnd + lineShift, servicePriceColIdx + 1);
                    mergedCells.Merged = true;
                    mergedCells.Value = "Total HT";
                    mergedCells.Style.Font.Weight = ExcelFont.BoldWeight;
                    mergedCells.Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                    ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Value = currency;
                    ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                    ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Value = FormatAmount(job.CalculateTotalExclTaxDiscounted(), true);
                    ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                }
                
            }

            if (isDeliveryNote && !isPricedDeliveryNote)
            {
                ws.Cells[itemsLineEnd, taxRateColIdx].Value = "";
                ws.Cells[itemsLineEnd, totalWithTaxColIdx].Value = "";
            }
            else
            {                
                var vatsPerRate =  job.CalculateVATPerRate(true, usePriceIncludingVat);

                if(vatsPerRate.Count > 0)
                {
                    var rangeForBorder = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + lineShift + 1, servicePriceColIdx, itemsLineEnd + lineShift + vatsPerRate.Count, totalWithTaxColIdx);
                    rangeForBorder.Style.Borders.SetBorders(MultipleBorders.Top | MultipleBorders.Bottom, SpreadsheetColor.FromArgb(169, 169, 169), LineStyle.Thin);
                }
                
                foreach (var vat in vatsPerRate)
                {
                    var vatFromDisc = vat.Item2;
                    var vatAmountDisc = vat.Item3;
                    mergedCells = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + ++lineShift, servicePriceColIdx, itemsLineEnd + lineShift, servicePriceColIdx + 1);
                    mergedCells.Merged = true;
                    mergedCells.Value = vatsPerRate.Count > 1 ?
                        $"{(usePriceIncludingVat && usePriceIncludingVat ? GetResource("IncludingVat") : GetResource("VAT"))} {vat.Item1:0.00}% / {FormatAmount(vatFromDisc, true)}" :
                        $"{(usePriceIncludingVat && usePriceIncludingVat ? GetResource("IncludingVat") : GetResource("VAT"))} {vat.Item1:0.00}%";
                    mergedCells.Style.Font.Weight = ExcelFont.BoldWeight;
                    mergedCells.Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                    ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Value = currency;
                    ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                    ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Value = FormatAmount(vatAmountDisc, true);
                    ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                }
            }

            if (!isDeliveryNote || isPricedDeliveryNote)
            {
                if (org.round_amount)
                {
                    var roundAmount = Math.Round(job.CalculateTotal(true) - job.CalculateTotal(false), 2, MidpointRounding.AwayFromZero);

                    if(roundAmount != 0)
                    {
                        mergedCells = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + ++lineShift, servicePriceColIdx, itemsLineEnd + lineShift, servicePriceColIdx + 1);
                        mergedCells.Merged = true;
                        mergedCells.Value = GetResource("RoundedAmount");
                        mergedCells.Style.Font.Weight = ExcelFont.BoldWeight;
                        mergedCells.Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                        ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Value = currency;
                        ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                        ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Value = FormatAmount(roundAmount, true);
                        ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                    }
                }

                mergedCells = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + ++lineShift, servicePriceColIdx, itemsLineEnd + lineShift, servicePriceColIdx + 1);
                mergedCells.Merged = true;
                mergedCells.Value = "Total TTC";
                mergedCells.Style.Font.Weight = ExcelFont.BoldWeight;
                mergedCells.Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Value = currency;
                ws.Cells[itemsLineEnd + lineShift, taxRateColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
                ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Value = FormatAmount(job.CalculateTotal(org.round_amount, true, usePriceIncludingVat), true);
                ws.Cells[itemsLineEnd + lineShift, totalWithTaxColIdx].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;
            }

            CellRange mergedRange;

            if (isDeliveryNote)
            {
                ws.Cells[itemsLineEnd + ++lineShift, serviceNameColIdx].Value = "";
            }
            else if (job.dateQuoteValidity != null)
            {
                lineShift += 3;
                mergedRange = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + lineShift, serviceCustomNumberColIdx, itemsLineEnd + lineShift, totalWithTaxColIdx);
                mergedRange.Merged = true;
                mergedRange.Value = GetResource("ValidityDate") + ": " + job.dateQuoteValidity.Value.ToString("dd/MM/yyyy");
            }

            
            string footerId =  isDeliveryNote? "PdfDeliveryNoteFooter" : "PdfQuoteFooter"; 
            string listenToYouFooter = GetResource("Email_Default_" + footerId);

            var tplMgr = new Calani.BusinessObjects.Settings.DocumentTemplateManager(_userId, Culture);
            var tpl = tplMgr.Get(footerId);
            listenToYouFooter = tpl.DefaultContent;
            if (tpl.UserTemplate != null && !tpl.UserTemplate.content.Trim().Equals("Votre texte personnalisé"))
            {
                listenToYouFooter = tpl.UserTemplate.content;
            }

            lineShift++;
            mergedRange = ws.Cells.GetSubrangeAbsolute(itemsLineEnd + lineShift, serviceCustomNumberColIdx, itemsLineEnd + lineShift, totalWithTaxColIdx);
            mergedRange.Merged = true;
            mergedRange.Value = listenToYouFooter;
            ws.Rows[itemsLineEnd + lineShift].AutoFit(true);

            if (org.hide_gesmob_label == false)
            {
                ws.Cells[itemsLineEnd + ++lineShift, serviceCustomNumberColIdx].Value = GetResource("DocGeneratedWithGesMobile");
            }
            

            var lastLine = BindStatusAndSign(job, itemsLineEnd + ++lineShift);

            return lastLine;
        }

        private void BindQuoteInfo(jobs job, bool isUnpricedDeliveryNote, bool hideCode)
        {
            bool isDeliveryNote = job.subtype != null && job.subtype == "DeliveryNote";

            if (isDeliveryNote)
            {
                var title = !string.IsNullOrWhiteSpace(job.organizations.delivery_note_pdf_title) ? job.organizations.delivery_note_pdf_title : GetResource("DeliveryNote");
                ws.Cells["A11"].Value = $"{title} # {job.internalId}";
                ws.Cells["A11"].Style.WrapText = true;
                ws.Rows[10].AutoFit(true);
            }
            else
            {
                ws.Cells["A11"].Value = GetResource("Quote") + " #" + job.internalId;
            }
                       

            ws.Cells["A12"].Value = GetResource("Date") + ":";
            if (isDeliveryNote)
            {
                if (job.dateQuoteSent != null)
                {
                    ws.Cells["C12"].Value = job.dateQuoteSent.Value.ToString("dd/MM/yyyy");
                }
                else
                {
                    ws.Cells["C12"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                }
            }
            else
            {
                if (job.lastUpdate != null)
                {
                    ws.Cells["C12"].Value = job.lastUpdate.Value.ToString("dd/MM/yyyy");
                }
                else
                {
                    ws.Cells["C12"].Value = DateTime.Now.ToString("dd/MM/yyyy");
                }
            }
            


            if(job.clients != null && !String.IsNullOrWhiteSpace(job.clients.vatNumber))
            {
                ws.Cells["G16"].Value = GetResource("VATNumber") + ":";
                ws.Cells["H16"].Value = job.clients.vatNumber;
            }

            var custRefStr = job.GetCustomerReference(Culture);
            if (custRefStr.Length > 0)
            {
                ws.Cells["A13"].Value += custRefStr + Environment.NewLine;
            }

            var locationOfIntervention = job.GetLocationOfIntervention(Culture);
            if (locationOfIntervention.Length > 0)
            {
                ws.Cells["A13"].Value += locationOfIntervention + Environment.NewLine;
            }

            var typeOfIntervention = job.GetTypeOfIntervention(Culture);
            if (typeOfIntervention.Length > 0)
            {
                ws.Cells["A13"].Value += typeOfIntervention + Environment.NewLine;
            }

            ws.Cells["A13"].Value += job.description;
            ws.Cells["A13"].Style.WrapText = true;

            // Make long description fit
            ws.Rows[12].AutoFit(true);


            ws.Cells["A17"].Value = "#";

            if (hideCode)
            {
                var mergedUnit = ws.Cells.GetSubrangeAbsolute(16, 3, 16, 4); 
                mergedUnit.Merged = true;
                mergedUnit.Value = GetResource("Unit");
                mergedUnit.Style.HorizontalAlignment = HorizontalAlignmentStyle.Left;
            }
            else
            {
                ws.Cells["D17"].Value = GetResource("Code");
                ws.Cells["E17"].Value = GetResource("Unit");
            }
            
            ws.Cells["F17"].Value = GetResource("Qty");
            ws.Cells["F17"].Style.HorizontalAlignment = HorizontalAlignmentStyle.Right;

            if (hideServiceCustomNumber)
            {
                // Firstly unmerged what is merged
                var descriptionHeaderContainer = ws.Cells.GetSubrangeAbsolute(16, 1, 16, 2);
                descriptionHeaderContainer.Value = null;
                descriptionHeaderContainer.Merged = false;

                // Merge another group of cells
                descriptionHeaderContainer = ws.Cells.GetSubrangeAbsolute(16, 0, 16, 2);
                descriptionHeaderContainer.Merged = true;
                descriptionHeaderContainer.Value = GetResource("Description");
            }
            else
            {
                ws.Cells["B17"].Value = GetResource("Description");
            }

            if (hideUnit || isUnpricedDeliveryNote)
            {
                ws.Cells["E17"].Value = string.Empty;
                ws.Cells["E17"].Style.Borders[IndividualBorder.Right].LineStyle = LineStyle.None;
            }

            if (hideDiscount || isUnpricedDeliveryNote)
            {
                ws.Cells["H17"].Value = string.Empty;
                ws.Cells["H17"].Style.Borders[IndividualBorder.Right].LineStyle = LineStyle.None;
            }
            else
            {
                ws.Cells["H17"].Value = GetResource("Discount_P");
            }

            if (isUnpricedDeliveryNote)
            {
                // Hide headers for VAT, Amount
                ws.Cells["G17"].Value = string.Empty;
                ws.Cells["I17"].Value = string.Empty;
                ws.Cells["J17"].Value = string.Empty;
                ws.Cells["G17"].Style.Borders[IndividualBorder.Right].LineStyle = LineStyle.None;
                ws.Cells["I17"].Style.Borders[IndividualBorder.Right].LineStyle = LineStyle.None;
                ws.Cells["J17"].Style.Borders[IndividualBorder.Right].LineStyle = LineStyle.None;
            }

            if (!isDeliveryNote)
            {
                ws.Cells["G17"].Value = job.organizations.use_price_including_vat 
                    ? GetResource("Unit_Price_IncTax")
                    : GetResource("Unit_Price_NoTax");
                ws.Cells["J17"].Value = GetResource("Amount");                
            }

            if (hideVat)
            {
                ws.Cells["I17"].Value = string.Empty;
                ws.Cells["I17"].Style.Borders[IndividualBorder.Right].LineStyle = LineStyle.None;
                ws.Cells["J17"].Value = GetResource("Amount") + " HT";
            } 
            else if(!isUnpricedDeliveryNote)
            {
                ws.Cells["I17"].Value = GetResource("VAT");
            } 
            
            if (job.organizations.use_price_including_vat)
            {
                ws.Cells["J17"].Value = GetResource("TotalTTC");
            }
        }


        private int BindStatusAndSign(jobs job, int lineIndex)
        {
            bool bl = job.subtype != null && job.subtype == "DeliveryNote";
            if (bl)
            {
                var att = job.attachments.OrderByDescending(a=>a.date).FirstOrDefault(a => a.type == (int)AttachmentTypeEnum.DeliveryNoteSign);

                if (att != null)
                {
                    var extJob = new jobsExtended(job);
                    if (att.date.HasValue)
                    {
                        var merged = ws.Cells.GetSubrangeAbsolute(lineIndex + 1, 3, lineIndex + 1, 6);
                        merged.Merged = true;
                        merged.Value = $"{GetResource("AcceptedOn")} : {att.date.Value.ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture)}";
                        merged.Style.WrapText = true;
                        ws.Rows[itemsLineEnd + 1].AutoFit(true);

                        var contact = job.clients.contacts.FirstOrDefault(c => c.id == job.clientContact);
                        merged = ws.Cells.GetSubrangeAbsolute(lineIndex + 2, 3, lineIndex + 2, 6);
                        merged.Merged = true;
                        merged.Value = $"{GetResource("To")} : {contact.Description}";
                        merged.Style.WrapText = true;
                        ws.Rows[itemsLineEnd + 2].AutoFit(true);

                        var status = "Draft";
                        switch (extJob.ProjectStatus)
                        {

                            case ProjectStatus.DeliveryNoteDraft:
                                status =  GetResource("Draft");                                
                                break;
                            case ProjectStatus.DeliveryNoteSent:
                                status = GetResource("Sent");
                                break;
                            case ProjectStatus.DeliveryNoteAccepted:
                                status = GetResource("Accepted");
                                break;
                            case ProjectStatus.DeliveryNoteRefused:
                                status = GetResource("Declined");
                                break;

                        }
                        merged = ws.Cells.GetSubrangeAbsolute(lineIndex + 3, 3, lineIndex + 3, 6);
                        merged.Merged = true;
                        merged.Value = $"{GetResource("Status")} : {status}";
                        merged.Style.WrapText = true;
                        ws.Rows[itemsLineEnd + 3].AutoFit(true);

                        merged = ws.Cells.GetSubrangeAbsolute(lineIndex + 4, 3, lineIndex + 4, 6);
                        merged.Merged = true;
                        merged.Value = $"{GetResource("Contact")} : {job.deliveredTo}";
                        merged.Style.WrapText = true;
                        ws.Rows[itemsLineEnd + 4].AutoFit(true);

                        merged = ws.Cells.GetSubrangeAbsolute(lineIndex + 5, 3, lineIndex + 5, 6);
                        merged.Merged = true;
                        merged.Value = $"{GetResource("Comment")} : {job.comment}";
                        merged.Style.WrapText = true;
                        ws.Rows[itemsLineEnd + 5].AutoFit(true);

                    }
                    
                    var mgr = new AttachmentsManager(true);
                    mgr.OrganizationId = att.organizationId.Value;

                    bool imageOk = false;
                    try
                    {
                        using (MemoryStream imgms = mgr.GetStream(att.id, false))
                        {
                            var imgLogoTest = System.Drawing.Image.FromStream(imgms);
                            if (imgLogoTest.Width > 0 && imgLogoTest.Height > 0)
                                imageOk = true;
                        }


                    }
                    catch (Exception ex)
                    {
                        _log.Error("BindStatusAndSign. Image error.", ex);
                    }

                    if (imageOk)
                    {
                        MemoryStream imgms = mgr.GetStream(att.id, false);
                        _ms.Add(imgms);
                        ws.Pictures.Add(imgms, ExcelPictureFormat.Jpeg, "D" + (lineIndex + 8), "G" + (lineIndex + 10)).Position.Mode = PositioningMode.Move;
                    
                    }

                    return lineIndex + 5;
                }

            }

            return lineIndex;
        }

        private void BindAttachments(ExcelFile ef, jobs job, List<long> attIds)
        {
            List<attachments> attachments = new List<attachments>();
            if (attIds != null)
            {
                if(attIds.Any())
                    attachments = job.attachments.OrderByDescending(a => a.date)
                        .Where(a => a.type != (int)AttachmentTypeEnum.DeliveryNoteSign && attIds.Contains(a.id)).ToList();
            }
            else
            {
                attachments = job.attachments.OrderByDescending(a => a.date)
                    .Where(a => a.type != (int)AttachmentTypeEnum.DeliveryNoteSign && (a.properties & (int)AttachmentProperties.IncludeToPdf) == (int)AttachmentProperties.IncludeToPdf).ToList();
            }
            

            var counter = 1;
            foreach (var att in attachments)
            {
                if (att != null)
                {
                    var extJob = new jobsExtended(job);
                    var mgr = new AttachmentsManager(true);
                    mgr.OrganizationId = att.organizationId.Value;


                    var path = mgr.GetPath(att.id);

                    if (!String.IsNullOrWhiteSpace(path))

                    {
                        byte[] imageFile = System.IO.File.ReadAllBytes(path);

                        var imgr = new Generic.ImageManager();
                        imgr.MaxSide = 600;
                        imgr.Init(imageFile);

                        byte[] imgdata = imgr.Save(System.Drawing.Imaging.ImageFormat.Jpeg);

                        MemoryStream imgms = new MemoryStream(imgdata);
                        var img = System.Drawing.Image.FromStream(imgms);

                        double w = img.Width;
                        double h = img.Height;

                        if (w > 10)
                        {
                            var sheet = ef.Worksheets.Add(counter.ToString());
                            sheet.Pictures.Add(imgms, ExcelPictureFormat.Png, "A9", w, h, LengthUnit.Pixel);
                            _ms.Add(imgms);
                            counter++;
                        }
                    }
                    

                    
                }
            }
        }

        private void BindYourCustomer(clients cust, addresses custAddr, contacts custContact, bool showContactInfoInvoices)
        {
            CommonToolsGenerator.BindYourCustomer(ws, cust, custAddr, custContact, showContactInfoInvoices);
        }

        private bool BindYourIdentity(Model.organizations org)
        {
            bool isHorizontalLogo = false;
            int adrType = Convert.ToInt32(Contacts.AddressTypeEnum.CompanyAddress);
            Model.addresses addr = (from r in org.addresses where r.type == adrType select r).FirstOrDefault();

            if (org.logo != null)
            {
                bool imageOk = false;
                try
                {
                    MemoryStream msLogoTest = new MemoryStream(org.logo);
                    var imgLogoTest = System.Drawing.Image.FromStream(msLogoTest);
                    if (imgLogoTest.Width > 0 && imgLogoTest.Height > 0) 
                        imageOk = true;
                    msLogoTest.Dispose();
                }
                catch { }

                if (imageOk)
                {
                    MemoryStream msLogo = new MemoryStream(org.logo);
                    _ms.Add(msLogo);

                    (double newWidth, double newHeight, bool isHorizontal) = GetLogoSize(msLogo);

                    double left = isHorizontal ? 420 : 460;
                    double top = isHorizontal ? 0 : 30;

                    ws.Pictures.Add(msLogo, ExcelPictureFormat.Png, left, top, newWidth, newHeight, LengthUnit.Pixel);

                    isHorizontalLogo = isHorizontal;
                }
            }

            ws.Cells["A1"].Value = org.name;
            if (addr != null)
            {
                ws.Cells["A2"].Value = addr.street;
                if (!String.IsNullOrWhiteSpace(addr.nb)) ws.Cells["A2"].Value += " " + addr.nb;
                ws.Cells["A3"].Value = addr.npa + " " + addr.city;
                if (!String.IsNullOrEmpty(addr.country)) ws.Cells["A3"].Value += ", " + addr.country;
            }

            int counter = 4;
            if (!String.IsNullOrWhiteSpace(org.website))
            {
                ws.Cells["A" + counter].Value = org.website;
                counter++;
            }

            if (!String.IsNullOrWhiteSpace(org.email))
            {
                ws.Cells["A" + counter].Value = GetResource("Email") + ": " + org.email;
                counter++;
            }

            if (!String.IsNullOrWhiteSpace(org.phone))
            {
                ws.Cells["A" + counter].Value = GetResource("Phone") + ": " + org.phone;
                counter++;
            }

            if (!String.IsNullOrWhiteSpace(org.vatnumber))
            {
                ws.Cells["A" + counter].Value = GetResource("VAT") + ": " + org.vatnumber;
                counter++;
            }

            return isHorizontalLogo;
        }

        private int BindAcceptanceBlock(Model.organizations org, int lineIndex)
        {
            if (!org.hide_quote_acceptance_pdf)
            {
                lineIndex += 2;
                var fisrtLine = lineIndex - 1;

                var padding = "   ";
                var merged = ws.Cells.GetSubrangeAbsolute(lineIndex, 0, lineIndex, 3);
                merged.Merged = true;
                merged.Value = $"{padding}{padding}{padding}{GetResource("AcceptanceOfThisOffer").ToUpper()}";
                merged.Style.Font.Weight = ExcelFont.BoldWeight;
                merged.Style.Font.Size = 13 * 20;

                lineIndex++;
                merged = ws.Cells.GetSubrangeAbsolute(lineIndex, 0, lineIndex, 3);
                merged.Merged = true;
                merged.Value = $"{padding}{padding}  ({GetResource("AcceptanceOfThisOfferNote")})";

                lineIndex += 2;
                merged = ws.Cells.GetSubrangeAbsolute(lineIndex, 0, lineIndex, 1);
                merged.Merged = true;
                merged.Value = $"{padding}{GetResource("Date")}:";
                merged.Style.Font.Italic = true;

                merged = ws.Cells.GetSubrangeAbsolute(lineIndex, 2, lineIndex, 3);
                merged.Merged = true;
                merged.Value = $"{GetResource("Mention")}:";
                merged.Style.Font.Italic = true;

                lineIndex += 2;
                merged = ws.Cells.GetSubrangeAbsolute(lineIndex, 0, lineIndex, 1);
                merged.Merged = true;
                merged.Value = $"{padding}{GetResource("Signature")}:";
                merged.Style.Font.Italic = true;

                ws.Cells[++lineIndex, 0].Value = "     ";
                ws.Cells[++lineIndex, 0].Value = string.Empty;
                ws.Cells[++lineIndex, 0].Value = string.Empty;

                var block = ws.Cells.GetSubrangeAbsolute(fisrtLine, 0, lineIndex + 2, 4);
                block.Style.FillPattern.SetSolid(SpreadsheetColor.FromArgb(235, 235, 235));
                block.Style.Borders.SetBorders(MultipleBorders.All, SpreadsheetColor.FromArgb(235, 235, 235), LineStyle.None);
            }
            
            return lineIndex;
        }
    }
}
