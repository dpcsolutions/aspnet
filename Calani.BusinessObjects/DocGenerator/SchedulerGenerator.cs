﻿using Calani.BusinessObjects.Calendars;
using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.Departments;
using Calani.BusinessObjects.DocGenerator.Resources;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.FrontEndModels.Scheduler;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.OtherResources;
using Calani.BusinessObjects.Scheduler;
using Calani.BusinessObjects.Scheduler.Models;
using GemBox.Spreadsheet;
using log4net;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Calani.BusinessObjects.DocGenerator
{
    public class SchedulerGenerator
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly long _orgId;
        private readonly long _userId;

        public System.Globalization.CultureInfo Culture { get; set; }
        public bool ExcelMode { get; set; }

        private List<MemoryStream> _ms = new List<MemoryStream>();

        public SchedulerGenerator(long orgId, long userId, System.Globalization.CultureInfo culture)
        {
            _orgId = orgId;
            _userId = userId;
            Culture = culture;
        }

        public byte[] GenerateDocument(DateTime? dtFrom, DateTime? dtTo)
        {
            byte[] doc = Encoding.UTF8.GetBytes(string.Empty);

            // Load icon images
            var icons = LoadImages();

            try
            {
                var validDateRange = ValidateDateRange(dtFrom, dtTo);
                bool isMonthlyView = validDateRange.DurationDays > 7;

                DateTime start = validDateRange.Start;
                DateTime end = validDateRange.End;

                // Load data
                var service = new ResourcesAllocationsService(_orgId, _userId);
                var _calendarsRecManager = new CalendarsRecManager(_orgId);
                var models = service.ListResourceAllocationsForScheduler(validDateRange.Start, validDateRange.End, Culture.Name);
                var calendarEvents = _calendarsRecManager.GetCalendarRecordsDto()
                    .Where(r => r.StartDate >= start && r.EndDate <= end)
                    .Select(r => new ResourceAllocationModel(new InvalidDateModel(r))).ToList();
                var employees = new EmployeesManager(_orgId).ListEmployees()
                    .Select(e => new KeyValuePair<long, string>(e.id, $"{e.firstName} {e.lastName} ({e.initials})"))
                    .OrderBy(e => e.Value).ToList();
                var otherResources = new OtherResourcesManager(_orgId).List().Select(e => new KeyValuePair<long, string>(e.id, e.name))
                    .OrderBy(e => e.Value).ToList();
                var departmentResource = new DepartmentsManager(_orgId).List().Select(d => new KeyValuePair<long, string>(d.id, d.name))
                    .OrderBy(d => d.Value).ToList();

                // Initialize Excel document
                SpreadsheetInfo.SetLicense(GemboxAPI.SpreadsheetLicense);
                var excelFile = new ExcelFile();
                int weekNumber = CalendarTools.GetNonIsoWeekOfYear(validDateRange.Start);
                var title = !isMonthlyView
                                    ? $"{GetResource("Scheduler")} - {GetResource("Week")} {weekNumber}"
                                    : $"{GetResource("Scheduler")} - {validDateRange.Start.ToString("MMMM yyyy", Culture)}";
                var worksheet = excelFile.Worksheets.Add(title);

                // Set header
                SheetHeaderFooter sheetHeadersFooters = worksheet.HeadersFooters;
                HeaderFooterPage firstHeaderFooter = sheetHeadersFooters.FirstPage;
                firstHeaderFooter.Header.LeftSection.Append(title);

                CellRange range;
                Color color = ColorTranslator.FromHtml("#000");
                int startRow = 0;
                int currRow = startRow;
                int startCol = 0;

                var datesCols = new Dictionary<long, int>();
                DateTime currDate = start;

                // Print dates and keep column indexes
                int currCol = 1;
                while (currDate <= end)
                {
                    worksheet.Cells[currRow, currCol].Value = FormatHeaderDate(currDate, Culture, isMonthlyView);
                    worksheet.Cells[currRow, currCol].Style.Font.Size = 9 * 20;
                    worksheet.Cells[currRow, currCol].Style.HorizontalAlignment = HorizontalAlignmentStyle.Center;
                    worksheet.Columns[currCol].AutoFit();
                    datesCols.Add(currDate.Ticks, currCol);
                    currDate = currDate.AddDays(1);
                    currCol++;
                }

                currRow++;

                Func<ResourceAllocationModel, int, Dictionary<int, List<int>>, int > printAllocation = (alloc, row, filledCells) =>
                {
                    var allocStart = new DateTime(alloc.StartDate.Year, alloc.StartDate.Month, alloc.StartDate.Day);
                    var allocEnd = new DateTime(alloc.EndDate.Year, alloc.EndDate.Month, alloc.EndDate.Day);

                    if (allocStart < start) allocStart = start;
                    if (allocEnd > end) allocEnd = end;

                    var isOneDayEvent = datesCols[allocStart.Date.Ticks] == datesCols[allocEnd.Date.Ticks];

                    color = ColorTranslator.FromHtml(alloc.Color);

                    CellRange allocRange = null;

                    row = GetRowIndexForAllocation(row, datesCols[allocStart.Date.Ticks], datesCols[allocEnd.Date.Ticks], filledCells);
                    MarkFilledCells(row, datesCols[allocStart.Date.Ticks], datesCols[allocEnd.Date.Ticks], filledCells);

                    if (!isOneDayEvent)
                    {
                        allocRange = worksheet.Cells.GetSubrangeAbsolute(row, datesCols[allocStart.Date.Ticks], row, datesCols[allocEnd.Date.Ticks]);
                        allocRange.Merged = true;
                        allocRange.Style.FillPattern.SetSolid(SpreadsheetColor.FromArgb(color.R, color.G, color.B));
                    }
                    else
                    {
                        worksheet.Cells[row, datesCols[allocStart.Date.Ticks]].Style.FillPattern.SetSolid(SpreadsheetColor.FromArgb(color.R, color.G, color.B));
                        worksheet.Cells[row, datesCols[allocStart.Date.Ticks]].Style.WrapText = true;
                    }

                    // Display icon instead of title for absence requests
                    if (alloc is AbsenceRequestModel)
                    {
                        var absenceModel = alloc as AbsenceRequestModel;

                        if (icons.ContainsKey(absenceModel.Icon))
                        {
                            var colIndex = isOneDayEvent ? datesCols[allocStart.Date.Ticks] : 
                                            ((datesCols[allocEnd.Date.Ticks] - datesCols[allocStart.Date.Ticks]) / 2 + datesCols[allocStart.Date.Ticks]);
                            var leftPadding = (worksheet.Columns[colIndex].GetWidth(LengthUnit.Millimeter) - 3) / 2;
                            var anchorcell = new AnchorCell(worksheet.Columns[colIndex], worksheet.Rows[row], leftPadding, 1, LengthUnit.Millimeter);
                            worksheet.Pictures.Add(icons[absenceModel.Icon].ImageStream, ExcelPictureFormat.Png, anchorcell, 3, 3, LengthUnit.Millimeter);
                        }

                        if (absenceModel.Canceled)
                        {
                            worksheet.Cells[row, datesCols[allocStart.Date.Ticks]].Style.Borders
                            .SetBorders(MultipleBorders.DiagonalDown, SpreadsheetColor.FromName(ColorName.Black), LineStyle.Thin);
                        }
                    }
                    else
                    {
                        worksheet.Cells[row, datesCols[allocStart.Date.Ticks]].Value = alloc.Title;
                    }

                    return row;
                };

                Func<List<ResourceAllocationModel>, int, string, int> printAllocations = (allocations, row, resTitle) =>
                {
                    worksheet.Cells[row, startCol].Value = resTitle;

                    if (allocations.Any())
                    {
                        var empStartRow = row;
                        var filledCells = new Dictionary<int, List<int>>();
                        allocations.ForEach(a => { row = printAllocation(a, empStartRow, filledCells); });
                        row = filledCells.Keys.Max() + 1;

                        worksheet.Cells.GetSubrangeAbsolute(empStartRow, startCol, filledCells.Keys.Max(), startCol).Merged = true;
                    }
                    else
                    {
                        row++;
                    }

                    return row;
                };

                Func<string, int, int> printResourcesHeader = (titleResource, row) =>
                {
                    worksheet.Cells[currRow, startCol].Value = GetResource(titleResource);
                    range = worksheet.Cells.GetSubrangeAbsolute(row, startCol, row, datesCols[end.Date.Ticks]);
                    range.Merged = true;
                    color = ColorTranslator.FromHtml("#C8C8C8");
                    range.Style.FillPattern.SetSolid(SpreadsheetColor.FromArgb(color.ToArgb()));
                    range.Style.Font.Weight = ExcelFont.BoldWeight;

                    return row + 1;
                };
                
                // Display Employees
                currRow = printResourcesHeader("Employees", currRow);                
                foreach (var employee in employees)
                {
                    var empAllocatons = models.Where(a => a.EmployeeId == employee.Key).ToList();
                    empAllocatons.AddRange(GetCalendarEventsForResource(employee.Key, 0, calendarEvents));
                    currRow = printAllocations(empAllocatons, currRow, employee.Value);
                }

                // Display Other Resources
                currRow = printResourcesHeader("OtherResources", currRow);
                foreach (var othRes in otherResources)
                {
                    var resAllocatons = models.Where(a => a.OtherResourceId == othRes.Key).ToList();
                    resAllocatons.AddRange(GetCalendarEventsForResource(0, othRes.Key, calendarEvents));
                    currRow = printAllocations(resAllocatons, currRow, othRes.Value);
                }

                // Display Departments
                currRow = printResourcesHeader("Departments", currRow);
                foreach (var departments in departmentResource)
                {
                    var depAllocatons = models.Where(a => a.DepartmentId == departments.Key).ToList();
                    depAllocatons.AddRange(GetCalendarEventsForResource(0, departments.Key, calendarEvents));
                    currRow = printAllocations(depAllocatons, currRow, departments.Value);
                }

                PrintAbsenceCategoriesLegend(worksheet, currRow, icons);

                // Style first column
                worksheet.Columns[startCol].Style.Font.Size = 9 * 20;
                worksheet.Columns[startCol].Style.Font.Weight = ExcelFont.BoldWeight;
                worksheet.Columns[startCol].AutoFit(1, worksheet.Rows[0], worksheet.Rows[currRow - 1]);                
                worksheet.Columns[startCol].Style.VerticalAlignment = VerticalAlignmentStyle.Center;                

                // Set borders
                color = ColorTranslator.FromHtml("#000");

                worksheet.Cells[currRow, startCol].Value = string.Empty;
                range = worksheet.Cells.GetSubrangeAbsolute(currRow, startCol, currRow, datesCols[end.Date.Ticks]);
                range.Merged = true;
                range.Style.Borders.SetBorders(MultipleBorders.All, color, LineStyle.None);

                range = worksheet.Cells.GetSubrangeAbsolute(startRow, startCol, currRow - 1, datesCols[end.Date.Ticks]);
                foreach (var cell in range)
                {
                    cell.Style.Borders.SetBorders(MultipleBorders.Bottom | MultipleBorders.Top | MultipleBorders.Left | MultipleBorders.Right,
                        SpreadsheetColor.FromArgb(color.ToArgb()), LineStyle.Thin);
                }

                // Get rid of borders for top left corner of table
                worksheet.Cells[0, 0].Style.Borders.SetBorders(MultipleBorders.Top | MultipleBorders.Left, SpreadsheetColor.FromName(ColorName.White), LineStyle.None);

                // Set page properties
                worksheet.PrintOptions.Portrait = false;
                worksheet.PrintOptions.FitToPage = true;
                worksheet.PrintOptions.PaperType = PaperType.A4;

                worksheet.PrintOptions.RightMargin = 0.3;
                worksheet.PrintOptions.LeftMargin = 0.2;
                worksheet.PrintOptions.TopMargin = 0.5;
                worksheet.PrintOptions.BottomMargin = 0.5;

                doc = SaveExcelFile(excelFile);                
            }
            catch(Exception ex)
            {
                _log.Error("SchedulerGenerator error", ex);
            }
            finally
            {
                foreach(var icon in icons)
                {
                    icon.Value.ImageStream.Dispose();
                    icon.Value.ResourceStream.Dispose();
                }
            }

            return doc;
        }

        private string FormatHeaderDate(DateTime date, CultureInfo culture, bool shortDate)
        {
            if (!shortDate)
            {
                return date.ToString("d ddd MMM yy", culture);
            }

            var dateNumber = date.ToString("dd", culture);
            var dayOfWeek = date.ToString("ddd", culture).Replace(".", "").ToUpper();

            return $"{dayOfWeek}{Environment.NewLine}{dateNumber}";
        }

        private DateRange ValidateDateRange(DateTime? dtFrom, DateTime? dtTo)
        {
            var range = new DateRange(Enums.DatesRangeEnum.CurrentMonth);

            if (dtFrom == null || dtTo == null || dtFrom > dtTo)
            {
                return range;
            }

            if ((dtTo - dtFrom).Value.TotalDays > 31)
            {
                var newEndDate = dtFrom.Value.AddDays(4);
                range = new DateRange(dtFrom.Value, newEndDate);
                return range;
            }

            range = new DateRange(dtFrom.Value, dtTo.Value);

            return range;
        }

        private byte[] SaveExcelFile(ExcelFile ef)
        {
            MemoryStream ms = new MemoryStream();

            ef.Save(ms, PdfSaveOptions.PdfDefault);
            byte[] ret = ms.ToArray();

            ms.Dispose();

            foreach (var ms2 in _ms)
            {
                ms2.Dispose();
            }

            return ret;
        }

        private string GetResource(string n)
        {
            if (SharedResource.Resource == null) return n;
            return SharedResource.Resource.GetString(n, Culture);
        }

        private Dictionary<string, IconImage> LoadImages()
        {
            var fileNames = new string[] 
            {
                "ban", "book", "car", "child", "fighter-jet", "gift",
                "heart", "hourglass-2", "plane", "square", "thermometer-three-quarters", "truck"
            };

            Assembly assembly = Assembly.GetExecutingAssembly();
            var images = new Dictionary<string, IconImage>();

            foreach (var name in fileNames)
            {
                var resourceStream = assembly.GetManifestResourceStream($"Calani.BusinessObjects.DocGenerator.Resources.{name}.png");
                var imageStream = new MemoryStream();
                resourceStream.CopyToAsync(imageStream);
                imageStream.Position = 0;

                var iconImage = new IconImage()
                {
                    Name = name,
                    ResourceStream = resourceStream,
                    ImageStream = imageStream
                };

                images.Add(name, iconImage);
            }

            return images;
        }

        private int GetRowIndexForAllocation(int startRow, int startCol, int endCol, Dictionary<int, List<int>> filledCells)
        {
            var row = startRow;
            var fits = false;
            while (!fits)
            {
                fits = !filledCells.ContainsKey(row) || Enumerable.Range(startCol, endCol).All(col => !filledCells[row].Contains(col));
                if (!fits) row++;
            }

            return row;
        }

        private void MarkFilledCells(int row, int startCol, int endCol, Dictionary<int, List<int>> filledCells)
        {
            if (!filledCells.ContainsKey(row))
            {
                filledCells.Add(row, new List<int>());
            }

            filledCells[row].AddRange(Enumerable.Range(startCol, endCol - startCol + 1));
        }

        private void PrintAbsenceCategoriesLegend(ExcelWorksheet worksheet, int row, Dictionary<string, IconImage> icons)
        {
            var categories = Enum.GetValues(typeof(AbsenceCategoryEnum)) as AbsenceCategoryEnum[];

            var currRow = row + 1;
            foreach(var category in categories)
            {
                var acell = new AnchorCell(worksheet.Columns[0], worksheet.Rows[currRow], 1, 1, LengthUnit.Millimeter);
                worksheet.Pictures.Add(icons[category.GetIcon()].ImageStream, ExcelPictureFormat.Png, acell, 3, 3, LengthUnit.Millimeter);
                worksheet.Cells[acell.Row.Index, acell.Column.Index].Value += $"      - {GetResource(category.GetDescription())}";
                Color color = ColorTranslator.FromHtml(category.GetColor());
                worksheet.Cells[acell.Row.Index, acell.Column.Index].Style.FillPattern.SetSolid(SpreadsheetColor.FromArgb(color.ToArgb()));
                currRow++;
            }
        }

        private List<ResourceAllocationModel> GetCalendarEventsForResource(long employeeId, long othResId, List<ResourceAllocationModel> calendarEvents)
        {
            foreach(var calEvent in calendarEvents)
            {
                calEvent.EmployeeId = employeeId;
                calEvent.OtherResourceId = othResId;
            }

            return calendarEvents;
        }
    }
}
