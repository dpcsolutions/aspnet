﻿using Calani.BusinessObjects.AbsenceRequests;
using Calani.BusinessObjects.AbsenceRequests.Models;
using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Projects;
using Calani.BusinessObjects.Scheduler;
using Calani.BusinessObjects.TimeManagement;
using Calani.BusinessObjects.TimeManagement.Models;
using Calani.BusinessObjects.TimeManagement.Services;
using Calani.BusinessObjects.WorkSchedulers;
using GemBox.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Calani.BusinessObjects.DocGenerator
{
    public class TimeSheetGenerator
    {
        public long TimeSheetId { get; set; }

        public static string ExcelTemplates { get; set; }

        public System.Globalization.CultureInfo Culture { get; set; }

        public bool ExcelMode { get; set; }

        long _organizationId;

        public TimeSheetGenerator(System.Globalization.CultureInfo culture, long organizationId)
        {
            _organizationId = organizationId;
            Culture = culture;
        }

        List<MemoryStream> _ms = new List<MemoryStream>();

        private string GetResource(string n)
        {
            if (SharedResource.Resource == null) return n;
            return SharedResource.Resource.GetString(n, Culture);
        }

        ExcelWorksheet ws;

        int itemsLineStart = 17;
        int itemsLineEnd = 17;


        public byte[] GenerateDocument()
        {


            Calani.BusinessObjects.TimeManagement.TimeSheetsManager mgr = new TimeManagement.TimeSheetsManager(_organizationId);
            var sheet = mgr.GetById(TimeSheetId).Query.First();
            var records = sheet.timesheetrecords.Where(t=>t.recordStatus == (int)RecordStatus.Active).ToList();

            SpreadsheetInfo.SetLicense(GemboxAPI.SpreadsheetLicense);

            ExcelFile ef = ExcelFile.Load(Path.Combine(ExcelTemplates, "TimeSheet.xlsx"), LoadOptions.XlsxDefault);


            
            ws = ef.Worksheets.First();
            ws.Name = sheet.id.ToString();

            ws.Cells["F1"].Value = GetResource("Time_Sheet");
            ws.Cells["F2"].Value = GetResource("Employee") + ":";
            ws.Cells["F3"].Value = GetResource("Week") + ":";
            ws.Cells["F4"].Value = GetResource("TotalHours") + ":";
            ws.Cells["F5"].Value = GetResource("IncBillable") + ":";
            /*ws.Cells["F5"].Value = GetResource("BillableAmount") + ":";*/
            

            ws.Cells["A13"].Value = GetResource("Date");
            ws.Cells["B13"].Value = GetResource("Start");
            ws.Cells["C13"].Value = GetResource("End");
            ws.Cells["D13"].Value = GetResource("Rate");
            ws.Cells["E13"].Value = GetResource("Duration");
            ws.Cells["F13"].Value = GetResource("Type");
            ws.Cells["G13"].Value = GetResource("Activity");
            ws.Cells["H13"].Value = GetResource("Description");
            ws.Cells["I13"].Value = GetResource("Comment");

            BindYourIdentity(sheet.organizations);
            BindHeader(sheet, records);
            BindRecords(records);
            
            



            MemoryStream ms = new MemoryStream();
            if (ExcelMode)
            {
                ef.Save(ms, SaveOptions.XlsxDefault);
            }
            else
            { 
                ef.Save(ms, SaveOptions.PdfDefault);
            }
            byte[] ret = ms.ToArray();
            ms.Dispose();

            foreach (var ms2 in _ms)
            {
                ms2.Dispose();
            }

            
            return ret;
        }

        public byte[] GenerateReport(ReportKind kind, DateTime startDate, DateTime endDate, long selectedId)
        {
            SpreadsheetInfo.SetLicense(GemboxAPI.SpreadsheetLicense);

            var workbook = new ExcelFile();
            ExcelWorksheet worksheet = workbook.Worksheets.Add("Report_"+kind);

          
            switch (kind)
            {
                case ReportKind.Employees:
                    GetEmployeesReport(worksheet, startDate, endDate, selectedId);
                    break;
                case ReportKind.Projects:
                    GetProjectsReport(worksheet, startDate, endDate, selectedId);
                    break;
                case ReportKind.Services:
                    GetServicesReport(worksheet, startDate, endDate, selectedId);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(kind), kind, null);
            }


            int columnCount = worksheet.CalculateMaxUsedColumns();
            for (int i = 0; i < columnCount; i++)
                worksheet.Columns[i].AutoFit(1.1, worksheet.Rows[0], worksheet.Rows[worksheet.Rows.Count - 1]);


            var ms = new MemoryStream();

             workbook.Save(ms, SaveOptions.XlsDefault);
             byte[] ret = ms.ToArray();

             ms.Dispose();

             foreach (var ms2 in _ms)
             {
                 ms2.Dispose();
             }
             return ret;

        }

        private void GetEmployeesReport(ExcelWorksheet worksheet, DateTime startDate, DateTime endDate, long selectedId = 0 )
        {

            var tsMgr = new TimeSheetRecordsManager(this._organizationId);
            var records = tsMgr.GetRecordsForPeriod(startDate, endDate).ToList();

            if (selectedId > 0)
            {
                records = records.Where(c => c.timesheets.contacts != null && c.timesheets.contacts.id == selectedId).ToList();
            }

            var contacts = records.Select(r => r.timesheets.contacts).OrderBy(c=>c.lastName).ThenBy(c=>c.firstName).Distinct().ToList();
            

            var categories = records
                .Where(r => r.work_schedule_profiles_rec != null)
                .Select(r => r.work_schedule_profiles_rec).Distinct().ToList();

            var title = startDate.ToString("MMMM, yyyy");
            TimeSpan span = endDate - startDate;
            if (span.TotalDays > 100)//year
            {
                title = startDate.ToString("yyyy");
            }
            else if (span.TotalDays > 31) //quarter
            {
                var q = GetQuarterByStart(startDate);
                title = startDate.ToString("yyyy")+", Q"+q;
            }

            string defaultOvertimeRateTitle = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(GetResource("OvertimeRate").ToLower());
            string activeContractRate1 = null;
            string activeContractRate2 = null;
            if (contacts.Count() == 1)
            {
                var activeContract = new EmployeesManager.EmployeesContractsManager(_organizationId).GetActiveContract(contacts.First().id);
                if (activeContract != null && activeContract.overtime_allowed)
                {
                    activeContractRate1 = activeContract.overtime_rate.ToString("0.00", CultureInfo.InvariantCulture);

                    if (activeContract.overtime_rate2_enabled)
                    {
                        activeContractRate2 = activeContract.overtime_rate2.ToString("0.00", CultureInfo.InvariantCulture);
                    }
                }
            }

            var staticColumns = 14;
            worksheet.Cells["A1"].Value = title;
            worksheet.Cells["B1"].Value = GetResource("ReqHours_Week");
            worksheet.Cells["C1"].Value = GetResource("WorkRate");
            worksheet.Cells["D1"].Value = GetResource("HoursWorked");
            worksheet.Cells["E1"].Value = GetResource("ReqHours_Period");
            worksheet.Cells["F1"].Value = GetResource("Difference");
            worksheet.Cells["G1"].Value = GetResource("VacationsRemaining");
            worksheet.Cells["H1"].Value = GetResource("TotalHoursWorked");
            worksheet.Cells["I1"].Value = GetResource("DaysOfHolidayTaken");
            worksheet.Cells["J1"].Value = !string.IsNullOrWhiteSpace(activeContractRate1) ? string.Format(GetResource("PercentHours"), activeContractRate1) : $"{defaultOvertimeRateTitle} 1";
            worksheet.Cells["K1"].Value = !string.IsNullOrWhiteSpace(activeContractRate2) ? string.Format(GetResource("PercentHours"), activeContractRate2) : $"{defaultOvertimeRateTitle} 2";
            worksheet.Cells["L1"].Value = GetResource("TotalHoursToBeCompensated");
            worksheet.Cells["M1"].Value = GetResource("TotalHoursEffectivelyCompensated");
            worksheet.Cells["N1"].Value = GetResource("TotalHoursActuallyPaid");
            
            var i = staticColumns;

            foreach (var cat in categories)
            {
                worksheet.Cells[Tools.ExcelAlphabetChars[i] + "1"].Value = cat?.name;
                i++;
            }
            int y = 2;

            var absenceReportsDict = GetAbsenceReportsForPeriod(contacts.Select(c => c.id).Distinct().ToList(), startDate, endDate);

            foreach (var contact in contacts)
            {
                var dto = contact.ToDto();
                var userRecords = records.Where(r => r.timesheets.employeeId.HasValue && r.timesheets.employeeId == contact.id).ToList();

                var timesheetsReport = GetTimesheetsReportForEmployee(contact.id, startDate, endDate) ?? new TimesheetReportModel();
                var allTimesheetsReport = GetTimesheetsReportForEmployee(contact.id, new DateTime(1990, 1, 1), DateTime.Now) ?? new TimesheetReportModel();
                
                var absenceReport = absenceReportsDict.ContainsKey(contact.id) ? absenceReportsDict[contact.id] : new AbsenceReportModel();

                var dayHours = dto.WeekWorkHours / (double)5;

                var days = Tools.GetBusinessDays(startDate, endDate);

                var periodRequiredHours = days * dayHours;

                worksheet.Cells["A" + y].Value = dto.FullName;

                worksheet.Cells["B" + y].Value = dto.WeekWorkHours;
                worksheet.Cells["B" + y].Style.NumberFormat = "0.00";


                worksheet.Cells["C" + y].Value = dto.Rate / 100;
                worksheet.Cells["C" + y].Style.NumberFormat = "0.00%";

                var hoursWorked = CalculateHoursWorked(contact, userRecords);
                worksheet.Cells["D" + y].SetValue(hoursWorked);
                worksheet.Cells["D" + y].Style.NumberFormat = "0.00";

                worksheet.Cells["E" + y].SetValue(periodRequiredHours);
                worksheet.Cells["E" + y].Style.NumberFormat = "0.00";

                worksheet.Cells["F" + y].Formula = $"={"D" + y}-{"E" + y}";
                worksheet.Cells["F" + y].Style.NumberFormat = "0.00";

                worksheet.Cells["G" + y].Value = absenceReport.VacationReport.VacationDaysRemaining;
                worksheet.Cells["G" + y].Style.NumberFormat = "0.00";

                worksheet.Cells["H" + y].Value = timesheetsReport.TotalHoursWorked;
                worksheet.Cells["H" + y].Style.NumberFormat = "0.00";

                worksheet.Cells["I" + y].Value = absenceReport.VacationReport.VacationDaysTaken;
                worksheet.Cells["I" + y].Style.NumberFormat = "0.00";

                worksheet.Cells["J" + y].Value = timesheetsReport.OvertimeRate1Hours;
                worksheet.Cells["J" + y].Style.NumberFormat = "0.00";

                worksheet.Cells["K" + y].Value = timesheetsReport.OvertimeRate2Hours;
                worksheet.Cells["K" + y].Style.NumberFormat = "0.00";

                worksheet.Cells["L" + y].Value = allTimesheetsReport.TotalHoursToBeCompensated;
                worksheet.Cells["L" + y].Style.NumberFormat = "0.00";

                worksheet.Cells["M" + y].Value = allTimesheetsReport.TotalHoursEffectivelyCompensated;
                worksheet.Cells["M" + y].Style.NumberFormat = "0.00";

                worksheet.Cells["N" + y].Value = timesheetsReport.TotalHoursActuallyPaid;
                worksheet.Cells["N" + y].Style.NumberFormat = "0.00";

                int c = staticColumns;

                foreach (var cat in categories)
                {
                    worksheet.Cells[Tools.ExcelAlphabetChars[c] + y].Value = CalculateCategoryHours(contact, userRecords, cat);
                    worksheet.Cells[Tools.ExcelAlphabetChars[c] + y].Style.NumberFormat = "0.00";
                    c++;
                }

                y++;
            }
        }

        private void GetProjectsReport(ExcelWorksheet worksheet, DateTime startDate, DateTime endDate, long selectedId = 0)
        {

            var tsMgr = new TimeSheetRecordsManager(this._organizationId);
            var yearStart = new DateTime(startDate.Year,1,1).Date;
            
            var yearRecords = tsMgr.GetRecordsForPeriod(yearStart, endDate).ToList();

            yearRecords = yearRecords.Where(r => r.timesheetrecords1 == null || !r.timesheetrecords1.Any()).ToList();

            if (selectedId > 0)
            {
                yearRecords = yearRecords.Where(c => c.jobs != null && c.jobs.id == selectedId).ToList();
            }


            var records = yearRecords.Where(r=>r.startDate >= startDate).ToList();

            var projects = records.Where(r => r.jobs != null).Select(s => s.jobs).Distinct().ToList().Select(s => new jobsExtended(s)).OrderBy(p => p.OrderTag).ToList();
            //.OrderBy(p=>p.clients?.companyName).ThenBy(p=>p.description).ThenBy(p=>p.internalId).ToList();

            var contacts = records.Select(r => r.timesheets.contacts).Distinct().ToList();



            var title = startDate.ToString("MMMM, yyyy");
            TimeSpan span = endDate - startDate;
            if (span.TotalDays > 100)//year
            {
                title = startDate.ToString("yyyy");
            }
            else if (span.TotalDays > 31) //quarter
            {
                var q = GetQuarterByStart(startDate);
                title = startDate.ToString("yyyy") + ", Q" + q;
            }

            worksheet.Cells["A1"].Value = title;

            var i = 1;


            foreach (var employee in contacts)
            {
                var employeeDto = employee.ToDto();
                worksheet.Cells[Tools.ExcelAlphabetChars[i++] + "1"].Value = employeeDto.FullName;
            }
            
            worksheet.Cells[Tools.ExcelAlphabetChars[i++] + "1"].Value = GetResource("TotalTimeUpToDate");
            worksheet.Cells[Tools.ExcelAlphabetChars[i++] + "1"].Value = GetResource("TotalCostUpToDate");
            worksheet.Cells[Tools.ExcelAlphabetChars[i++] + "1"].Value = GetResource("Difference");


            int y = 2;

            foreach (var job in projects)
            {



                List<timesheetrecords> jobHours = records.Where(r => r.jobId == job.id).ToList();

                var client = job.clients?.companyName?? GetResource("None");
                /*Project Title*/
                worksheet.Cells["A" + y].Value =  String.Format("{0} - ", client) + (String.IsNullOrWhiteSpace(job.description) ? job.internalId: job.description);
                worksheet.Cells["A" + y++].Style.Font.Weight = ExcelFont.BoldWeight;

                
               if (jobHours.Any(r => !r.work_schedule_profiles_rec_id.HasValue))
               {
                   var mgr = new WorkScheduleRecManager(_organizationId);
                   var customCat = mgr.GetScheduleRecords(null).FirstOrDefault(c=>c.recordStatus == 0 &&  c.type == (int)TimeCategoryType.Custom );

                    jobHours.ForEach(j =>
                    {
                        if (!j.work_schedule_profiles_rec_id.HasValue && customCat !=null)
                        {
                            j.work_schedule_profiles_rec_id = customCat.id;
                            j.work_schedule_profiles_rec = customCat;
                        }
                    });
               }


               var categories = jobHours.Where(r => r.work_schedule_profiles_rec != null)
                   .Select(r => r.work_schedule_profiles_rec).Distinct().ToList();

                var projRows = categories.Count * 3;

               foreach (var cat in categories)
               {
                   worksheet.Cells["A" + y++].Value = $"{cat.name}-{GetResource("Hours")}"; 
                   worksheet.Cells["A" + y++].Value = $"{cat.name}-{GetResource("Costs")}"; 
                   worksheet.Cells["A" + y++].Value = $"{cat.name}-{GetResource("Total")}";
               }
               
               worksheet.Cells["A" + y++].Value = GetResource("TotalCostEmployee");
               worksheet.Cells["A" + y++].Value = GetResource("TotalTimeEmployee");
               worksheet.Cells["A" + y++].Value = GetResource("TotalForProject");
               worksheet.Cells["A" + y++].Value = GetResource("Difference");
               
               int x = 1;
               int yy = y - (projRows+4);
               string  cellId = String.Empty;
               double totalProj = 0;
               double totalProjHours = 0;
                foreach (var empl in contacts)
               {
                   var userRecords = jobHours
                       .Where(r => r.timesheets.employeeId.HasValue && r.timesheets.employeeId == empl.id).ToList();


                   var contract = empl.contacts_contracts.FirstOrDefault(c => c.recordStatus == 0)?? new contacts_contracts();

                   var userhours = CalculateHoursOnProject(records, job.id, empl.id);
                   totalProj += (userhours * Convert.ToDouble(contract.hour_rate));

                   totalProjHours += userhours;
                    var letter = Tools.ExcelAlphabetChars[x];
                   List<string> totalEmplCells = new List<string>();
                   var yyy = yy+0;
                    
                   foreach (var cat in categories)
                   {
                       var catRecords = userRecords.Where(r => r.work_schedule_profiles_rec_id == cat.id).ToList();

                       //Cat hours on project
                       var sum = CalculateHoursOnProject(catRecords);
                       cellId = letter + (yyy++);
                       worksheet.Cells[cellId].Value = sum;

                       //Cat Cost hours on project
                       cellId = letter + (yyy++);
                       worksheet.Cells[cellId].Value = contract.hour_rate;

                       //Cat Total 
                       var totalCat = $"={letter}{(yyy - 2)}*{letter}{(yyy - 1)}";
                       cellId = letter + (yyy++);
                       worksheet.Cells[cellId].Formula = totalCat;

                       totalEmplCells.Add(cellId);
                    }

                   

                   //Total Empl Hours
                   cellId = letter + (yy + 0 + (3 * categories.Count));
                   worksheet.Cells[cellId].Value = userhours;

                   //Total Empl
                   string totalEmpl = String.Join("+", totalEmplCells);
                    cellId = letter + (yy + 1 + (3 * categories.Count));
                   worksheet.Cells[cellId].Formula = $"={totalEmpl}";

                    
                    x++;
               }




                //Total Hours
                cellId = Tools.ExcelAlphabetChars[x] + (y - 2);
                worksheet.Cells[cellId].Value = totalProjHours;

                //Total Proj
                cellId = Tools.ExcelAlphabetChars[x + 1] + (y - 2);
                worksheet.Cells[cellId].Value = totalProj;

                var totalProjId = cellId;
                cellId = Tools.ExcelAlphabetChars[x + 2] + (y - 2);
              
                //Contract Proj
                worksheet.Cells[cellId].Value = job.totalWithoutTaxes;

               var contractProjId = cellId;
                y++;

                cellId = Tools.ExcelAlphabetChars[x + 2] + (y - 2);
                //Diff
                worksheet.Cells[cellId].Formula = $"={contractProjId}-{totalProjId}" ;

                y++;
            }

        }

        private void GetServicesReport(ExcelWorksheet worksheet, DateTime startDate, DateTime endDate, long selectedId = 0)
        {
            var tsMgr = new TimeSheetRecordsManager(this._organizationId);
            var yearStart = new DateTime(startDate.Year, 1, 1).Date;

            var yearRecords = tsMgr.GetRecordsForPeriod(yearStart, endDate).ToList();

            yearRecords = yearRecords.Where(r => r.timesheetrecords1 == null || !r.timesheetrecords1.Any()).ToList();

            if (selectedId > 0)
            {
                yearRecords = yearRecords.Where(c => c.services != null && c.serviceId == selectedId).ToList();
            }


            var records = yearRecords.Where(r => r.startDate >= startDate).ToList();

            var services = records.Where(r => r.services != null).Select(s => s.services).Distinct().OrderBy(p => p.name).ToList();

            var contacts = records.Select(r => r.timesheets.contacts).Distinct().ToList();

            var title = startDate.ToString("MMMM, yyyy");
            TimeSpan span = endDate - startDate;
            if (span.TotalDays > 100)//year
            {
                title = startDate.ToString("yyyy");
            }
            else if (span.TotalDays > 31) //quarter
            {
                var q = GetQuarterByStart(startDate);
                title = startDate.ToString("yyyy") + ", Q" + q;
            }

            worksheet.Cells["A1"].Value = title;

            var i = 1;

            foreach (var employee in contacts)
            {
                var employeeDto = employee.ToDto();
                worksheet.Cells[Tools.ExcelAlphabetChars[i++] + "1"].Value = employeeDto.FullName;
            }

            worksheet.Cells[Tools.ExcelAlphabetChars[i++] + "1"].Value = GetResource("TotalTimeUpToDate");
            worksheet.Cells[Tools.ExcelAlphabetChars[i++] + "1"].Value = GetResource("TotalCostUpToDate");
            worksheet.Cells[Tools.ExcelAlphabetChars[i++] + "1"].Value = GetResource("Difference");


            int y = 2;

            foreach (var service in services)
            {
                /*Service Title*/
                worksheet.Cells["A" + y].Value = String.IsNullOrWhiteSpace(service.name) ? service.internalId : service.name;
                worksheet.Cells["A" + y++].Style.Font.Weight = ExcelFont.BoldWeight;

                List<timesheetrecords> serviceHours = records.Where(r => r.serviceId == service.id).ToList();

                if (serviceHours.Any(r => !r.work_schedule_profiles_rec_id.HasValue))
                {
                    var mgr = new WorkScheduleRecManager(_organizationId);
                    var customCat = mgr.GetScheduleRecords(null).FirstOrDefault(c => c.recordStatus == 0 && c.type == (int)TimeCategoryType.Custom);

                    serviceHours.ForEach(j =>
                    {
                        if (!j.work_schedule_profiles_rec_id.HasValue && customCat != null)
                        {
                            j.work_schedule_profiles_rec_id = customCat.id;
                            j.work_schedule_profiles_rec = customCat;
                        }
                    });
                }


                var categories = serviceHours.Where(r => r.work_schedule_profiles_rec != null)
                    .Select(r => r.work_schedule_profiles_rec).Distinct().ToList();

                var serviceRows = categories.Count * 3;

                foreach (var cat in categories)
                {
                    worksheet.Cells["A" + y++].Value = $"{cat.name}-{GetResource("Hours")}";
                    worksheet.Cells["A" + y++].Value = $"{cat.name}-{GetResource("Costs")}";
                    worksheet.Cells["A" + y++].Value = $"{cat.name}-{GetResource("Total")}";
                }

                worksheet.Cells["A" + y++].Value = GetResource("TotalCostEmployee");
                worksheet.Cells["A" + y++].Value = GetResource("TotalTimeEmployee");
                worksheet.Cells["A" + y++].Value = GetResource("TotalForProject");
                worksheet.Cells["A" + y++].Value = GetResource("Difference");

                int x = 1;
                int yy = y - (serviceRows + 4);
                string cellId = String.Empty;
                double totalService = 0;
                double totalServiceHours = 0;
                foreach (var empl in contacts)
                {
                    var userRecords = serviceHours
                        .Where(r => r.timesheets.employeeId.HasValue && r.timesheets.employeeId == empl.id).ToList();


                    var contract = empl.contacts_contracts.FirstOrDefault(c => c.recordStatus == 0) ?? new contacts_contracts();

                    var userhours = CalculateHoursOnService(records, service.id, empl.id);
                    totalService += (userhours * Convert.ToDouble(contract.hour_rate));

                    totalServiceHours += userhours;
                    var letter = Tools.ExcelAlphabetChars[x];
                    List<string> totalEmplCells = new List<string>();
                    var yyy = yy + 0;

                    foreach (var cat in categories)
                    {
                        var catRecords = userRecords.Where(r => r.work_schedule_profiles_rec_id == cat.id).ToList();

                        //Cat hours on project
                        var sum = CalculateHoursOnService(catRecords);
                        cellId = letter + (yyy++);
                        worksheet.Cells[cellId].Value = sum;

                        //Cat Cost hours on project
                        cellId = letter + (yyy++);
                        worksheet.Cells[cellId].Value = contract.hour_rate;

                        //Cat Total 
                        var totalCat = $"={letter}{(yyy - 2)}*{letter}{(yyy - 1)}";
                        cellId = letter + (yyy++);
                        worksheet.Cells[cellId].Formula = totalCat;

                        totalEmplCells.Add(cellId);
                    }



                    //Total Empl Hours
                    cellId = letter + (yy + 0 + (3 * categories.Count));
                    worksheet.Cells[cellId].Value = userhours;

                    //Total Empl
                    string totalEmpl = String.Join("+", totalEmplCells);
                    cellId = letter + (yy + 1 + (3 * categories.Count));
                    worksheet.Cells[cellId].Formula = $"={totalEmpl}";


                    x++;
                }




                //Total Hours
                cellId = Tools.ExcelAlphabetChars[x] + (y - 2);
                worksheet.Cells[cellId].Value = totalServiceHours;

                //Total Proj
                cellId = Tools.ExcelAlphabetChars[x + 1] + (y - 2);
                worksheet.Cells[cellId].Value = totalService;

                var totalProjId = cellId;
                cellId = Tools.ExcelAlphabetChars[x + 2] + (y - 2);

                //Contract Proj
                //worksheet.Cells[cellId].Value = "0"; //TODO: service.totalWithoutTaxes;

                var contractProjId = cellId;
                y++;

                cellId = Tools.ExcelAlphabetChars[x + 2] + (y - 2);
                //Diff
                worksheet.Cells[cellId].Formula = $"={contractProjId}-{totalProjId}";

                y++;
            }

        }

        private double CalculateHoursOnProject(List<timesheetrecords> rec, long jobId = 0, long emplId = 0)
        {
            if (jobId > 0)
                rec = rec.Where(r => r.jobId == jobId).ToList();

            if (emplId > 0)
                rec = rec.Where(r=> r.timesheets.employeeId.HasValue && r.timesheets.employeeId == emplId).ToList();
            

            var sum= rec.Sum(i=>(i.duration*i.rate)/100) ?? 0;

            return Math.Round(sum, 2, MidpointRounding.AwayFromZero);

        }

        private double CalculateHoursOnService(List<timesheetrecords> rec, long serviceId = 0, long emplId = 0)
        {
            if (serviceId > 0)
                rec = rec.Where(r => r.serviceId == serviceId).ToList();

            if (emplId > 0)
                rec = rec.Where(r => r.timesheets.employeeId.HasValue && r.timesheets.employeeId == emplId).ToList();


            var sum = rec.Sum(i => (i.duration * i.rate) / 100) ?? 0;

            return Math.Round(sum, 2, MidpointRounding.AwayFromZero);

        }

        private void BindHeader(timesheets sheet, List<timesheetrecords> records)
        {
            int serviceTypeFilter = Convert.ToInt32(CustomerAdmin.ServiceType.BillableServices);

            ws.Cells["G2"].Value = "";
            if(!String.IsNullOrWhiteSpace(sheet.contacts.firstName)) ws.Cells["G2"].Value += sheet.contacts.firstName + " ";
            if(!String.IsNullOrWhiteSpace(sheet.contacts.lastName)) ws.Cells["G2"].Value += sheet.contacts.lastName + " ";
            if(!String.IsNullOrWhiteSpace(sheet.contacts.initials)) ws.Cells["G2"].Value += "(" + sheet.contacts.initials + ")";

            ws.Cells["G3"].Value = sheet.year + " / " + sheet.week;
            ws.Cells["H3"].Value = Generic.CalendarTools.GetIso8601MondayOfWeek(sheet.year.Value, sheet.week.Value).ToString("dddd dd MMM yyyy", Culture);

            ws.Cells["G4"].Value = (from r in records where r.duration != null && r.rate != null
                                    select r.duration.Value * r.rate.Value / 100).Sum();
            ws.Cells["G5"].Value = (from r in records
                                    where r.duration != null && r.rate != null
                                    && r.serviceId != null && r.serviceType != null && r.serviceType == serviceTypeFilter
                                    select r.duration.Value * r.rate.Value / 100).Sum();

            if (sheet.acceptedDate != null)
            {
                ws.Cells["H6"].Value = GetResource("Validation") + ": " + sheet.acceptedDate.Value.ToString("dd/MM/yyyy");
            }

            ws.Cells["A9"].Value = GetResource("Comment") + ": ";
            if(sheet.comment != null) ws.Cells["B9"].Value = sheet.comment;
        }

        int line = 13;
        private void BindRecords(List<timesheetrecords> records)
        {
            Calani.BusinessObjects.Model.CalaniEntities db = new CalaniEntities();

            var data = records.Where(r=>r.parent_id>0 || !r.timesheetrecords1.Any()).OrderBy(r => r.startDate).ToList();

            foreach (var record in data)
            {
                var r = ws.Rows[13];
                ws.Rows.InsertCopy(14, r);
            }

            var days = data.GroupBy(i => i.startDate.GetValueOrDefault().Date);

            foreach (var day in days)
            {
                var index = 0;
                foreach (var record in day.OrderByDescending(i=>i.duration))
                {

                    if (record.startDate != null)
                        ws.Cells[line, 0].Value = record.startDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                    if (record.startDate != null && record.startDate.Value.Hour > 0)
                        ws.Cells[line, 1].Value = record.startDate.Value.ToString("HH:mm");

                    if (record.endDate != null && record.endDate.Value.Hour > 0)
                        ws.Cells[line, 2].Value = record.endDate.Value.ToString("HH:mm");

                    if (record.rate != null)
                        ws.Cells[line, 3].Value = record.rate.Value;
                    else
                        ws.Cells[line, 0].Value = 3;

                    if (record.duration != null)
                    {
                        double d = record.duration.Value;
                        d = d < 0 ? 0 : ((d * record.rate)??0)/100;

                        if (index == 0)
                        {
                            d = d - (record.lunch_duration / 60) ?? 0;
                            d = d - (record.pause_duration / 60) ?? 0;
                        }
                        
                        ws.Cells[line, 4].Value = (d < 0 ? 0: d).ToString("0.##", CultureInfo.InvariantCulture);
                    }
                    else
                        ws.Cells[line, 4].Value = 0;

                    if (record.visitId != null)
                    {
                        ws.Cells[line, 5].Value = GetResource("TimeTypeEnum_Visit");
                        ws.Cells[line, 6].Value = (from r in db.visits where r.id == record.visitId.Value select r.jobs.clients.companyName + " #" + r.jobs.id).FirstOrDefault();
                    }
                    else if (record.serviceId != null)
                    {
                        ws.Cells[line, 5].Value = GetResource("TimeTypeEnum_Internal");
                        ws.Cells[line, 6].Value = (from r in db.services where r.id == record.serviceId select r.name).FirstOrDefault();
                    }

                    if (!String.IsNullOrWhiteSpace(record.description))
                        ws.Cells[line, 7].Value = record.description;


                    if (!String.IsNullOrWhiteSpace(record.comment))
                        ws.Cells[line, 8].Value = record.comment;

                    line++;
                    index++;
                }

            }
        }

        private void BindYourIdentity(Model.organizations org)
        {
            int adrType = Convert.ToInt32(Contacts.AddressTypeEnum.CompanyAddress);
            Model.addresses addr = (from r in org.addresses where r.type == adrType select r).FirstOrDefault();

            if (org.logo != null)
            {
                MemoryStream msLogo = new MemoryStream(org.logo);
                _ms.Add(msLogo);
                ws.Pictures.Add(msLogo, ExcelPictureFormat.Png, 0, 0, 80, 80, LengthUnit.Pixel);

                ws.Cells["B1"].Value = org.name;
                if (addr != null)
                {
                    ws.Cells["B2"].Value = addr.street;
                    if (!String.IsNullOrWhiteSpace(addr.nb)) ws.Cells["B2"].Value += ", " + addr.nb;
                    ws.Cells["B3"].Value = addr.npa + " " + addr.city;
                    if (!String.IsNullOrEmpty(addr.country)) ws.Cells["B3"].Value += ", " + addr.country;
                }

                int counter = 4;
                if (!String.IsNullOrWhiteSpace(org.website))
                {
                    ws.Cells["B" + counter].Value = org.website;
                    counter++;
                }
                if (!String.IsNullOrWhiteSpace(org.email))
                {
                    ws.Cells["B" + counter].Value = GetResource("Email") + ": " + org.email;
                    counter++;
                }
                if (!String.IsNullOrWhiteSpace(org.phone))
                {
                    ws.Cells["B" + counter].Value = GetResource("Phone") + ": " + org.phone;
                    counter++;
                }
                if (!String.IsNullOrWhiteSpace(org.vatnumber))
                {
                    ws.Cells["B" + counter].Value = GetResource("VAT") + ": " + org.vatnumber;
                    counter++;
                }
                

            }

            
        }


        private double CalculateHoursWorked(contacts contact, List<timesheetrecords> userRecords)
        {
            var duration = userRecords.Sum(r => r.duration);
            var lunch = userRecords.Where(r => r.row_type == (int)TimeSheetRecordRowType.MasterRow).Sum(r => r.lunch_duration);
            var pause = userRecords.Where(r => r.row_type == (int)TimeSheetRecordRowType.MasterRow).Sum(r => r.pause_duration);

            var lunchH = lunch / 60;
            var pauseH = pause / 60;

            var timeH = duration - lunchH - pauseH;

            var ts = TimeSpan.FromHours(timeH??0);

            return Math.Round(timeH ?? 0, 2, MidpointRounding.AwayFromZero);//String.Format("{0}:{1:D2}", ts.Days * 24 + ts.Hours, ts.Minutes);
        }

        private double CalculateCategoryHours(contacts contact, List<timesheetrecords> userRecords, work_schedule_profiles_rec cat)
        {
            var catRecords = userRecords.Where(r => r.work_schedule_profiles_rec_id == cat.id).ToList();

            var duration = catRecords.Sum(r => r.duration);

            var ts = TimeSpan.FromHours(duration ?? 0);

            return Math.Round(duration??0, 2, MidpointRounding.AwayFromZero); //String.Format("{0}:{1:D2}", ts.Days * 24 + ts.Hours, ts.Minutes);
        }

        private int GetQuarterBeEnd(DateTime endDate)
        {
            int[] quarters = { 4, 4, 4, 1, 1, 1, 2, 2, 2, 3, 3, 3 };
            return quarters[endDate.Month - 1];
        }
        
        private int GetQuarterByStart(DateTime startDate)
        {
            int[] quarters = { 1, 1, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4 };
            return quarters[startDate.Month - 1];
        }

        private TimesheetReportModel GetTimesheetsReportForEmployee(long employeeId, DateTime start, DateTime end)
        {
            var employeesManager = new EmployeesManager(_organizationId);
            var contractsManager = new EmployeesManager.EmployeesContractsManager(_organizationId);
            var organizationManager = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(_organizationId);
            organizationManager.Load();
            var timesheetReportService = new TimesheetReportService(
                                                contractsManager,
                                                new TimeSheetsService(new TimeSheetsManager(_organizationId)),
                                                new TimeSheetRecordsManager(_organizationId),
                                                new EmployeesOvertimeManager(_organizationId),
                                                new AbsenceRequestsManager(_organizationId, contractsManager, employeesManager),
                                                new OrganizationManager(_organizationId),
                                                new WorkScheduleRecManager(_organizationId));

            var timesheetsReport = timesheetReportService.CalculateTimesheetReportForPeriod(_organizationId, employeeId, new DateRange(start, end));
            return timesheetsReport;
        }

        private Dictionary<long, AbsenceReportModel> GetAbsenceReportsForPeriod(List<long> employeesIds, DateTime startDate, DateTime endDate)
        {
            var organizationManager = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(_organizationId);
            long organizationId = _organizationId;
            var workScheduleRecManager = new WorkScheduleRecManager(organizationId);
            var workScheduleService = new WorkScheduleService(organizationManager, workScheduleRecManager);
            var workDay = workScheduleService.GetWorkDaySettings();
            var workingTimeCalculator = new WorkingTimeCalculator(workDay);
            var employeesManager = new EmployeesManager(_organizationId);
            var contractsManager = new EmployeesManager.EmployeesContractsManager(_organizationId);
            var absenceRequestsManager = new AbsenceRequestsManager(organizationId, contractsManager, employeesManager);
            var vacationCarriedOverDaysManager = new VacationCarriedOverDaysManager(organizationId);
            var vacationReportService = new VacationReportService(employeesManager, contractsManager, organizationManager, absenceRequestsManager,
                vacationCarriedOverDaysManager, workingTimeCalculator);
            var absenceRequestsService = new AbsenceRequestsService(absenceRequestsManager, employeesManager, contractsManager,
                vacationReportService, workingTimeCalculator);

            var reports = absenceRequestsService.GetAbsenceReports(startDate, endDate, employeesIds);

            return reports.ToDictionary(report => report.EmployeeId, report => report);
        }
    }
}
