﻿using System.IO;

namespace Calani.BusinessObjects.DocGenerator.Resources
{
    internal class IconImage
    {
        public string Name { get; set; }
        public Stream ResourceStream { get; set; }
        public MemoryStream ImageStream { get; set; }
    }
}
