﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.Admin
{
    public class PlaformCustomers
    {
        public List<PlaformCustomer> GetCustomers()
        {
            Model.CalaniEntities db = new Model.CalaniEntities();
            var orgs = (from r in db.organizations select r).ToList();
            var ret = (from r in orgs select new PlaformCustomer(r)).ToList();
            return ret;
        }


    }

    public class PlaformCustomer
    {
        public long OrgId { get; set; }
        public string Name { get; set; }
        public long OwnerId { get; set; }
        public DateTime? ActivationDateTime { get; set; }

        public string SubscriptionId { get; set; }
        public string SubscriptionStatus { get; set; }
        public int SubscriptionUsers { get; set; }

        public DateTime? SubscriptionEnd { get; set; }
        public DateTime? TrialEnd { get; set; }

        public PlaformCustomer()
        {

        }

        public PlaformCustomer(Model.organizations org)
        {
            OrgId = org.id;
            Name = org.name;
            if(org.owner != null) OwnerId = org.owner.Value;
            ActivationDateTime = org.activationDate;



            // to restore 4pat
            
            SubscriptionStatus = org.subscriptionStatus;
            if(org.userLimit != null) SubscriptionUsers = org.userLimit.Value;
            SubscriptionEnd = org.subscriptionEnd;
            TrialEnd = org.trialEnd;
            SubscriptionId = org.subscriptionId;
        }
    }
}
