﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.Admin
{
    public class TutorialModel
    {
        
        public long Id { get; set; }
        public String Name { get; set; }
        public decimal MinVersion { get; set; }
        public int DevicesDeployed { get; set; }
        
        public int ScreensCount;

        public TutorialsType Type;
        public int Viewed { get; set; }
        public List<TutorialScreenModel> Screens { get; set; }

    }

    public class TutorialScreenModel
    {
        public long Id { get; set; }
        public long TutorialId { get; set; }
        public int Position { get; set; }
        public String Title { get; set; }
        public String Explanation { get; set; }
        public String Image { get; set; }
        public String FileName { get; set; }
    }


    public static class MobileTutorialsExtensions
    {
        public static TutorialModel ToDto(this tutorials entity, long userId, bool details = false)
        {
            var model = new TutorialModel();

            if (entity != null)
            {
                model.Id = entity.id;
                model.Name = entity.name;
                model.Type = (TutorialsType)entity.type;
                model.MinVersion = entity.min_version;
                model.ScreensCount = entity.tutorials_screens.Count(s => s.recordStatus == (int) RecordStatus.Active);
                model.DevicesDeployed = entity.mobile_tutorials_deployed.Count(d => d.tutorial_id == entity.id);

                if (details)
                {
                    model.Screens = entity.tutorials_screens
                        .Where(s => s.recordStatus == (int)RecordStatus.Active)
                        .OrderBy(o=>o.position)
                        .Select(s => s.ToDto()).ToList();
                }

                model.Viewed = entity.tutorials_stat.Count(s => s.recordStatus == 0 && s.viewed_by == userId);


            }

            return model;
        }



        public static TutorialScreenModel ToDto(this tutorials_screens entity)
        {
            var model = new TutorialScreenModel();

            if (entity != null)
            {
                model.Id = entity.id;
                model.Position = entity.position;
                model.Title = entity.title;
                model.Explanation = entity.explanation;
                model.Image = entity.image;
                model.FileName = entity.filename;

                model.TutorialId = entity.tutorial_id;
            }

            return model;
        }

    }
}
