﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.Admin
{
    public class TutorialsManager : Generic.SmartCollectionManager<Model.tutorials, long>
    {

        public TutorialsManager()
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = -1;
        }

        public Model.tutorials GetMobileTutorial(long currentUser)
        {
            Model.tutorials ret = null;

            var q = base.GetBaseQuery();
            q.Query = (from r in q.Context.tutorials select r);

            ret = q.Query.FirstOrDefault();

            return ret;
        }

      
        public long SaveTutorial(long userId, long orgId, TutorialModel model)
        {
            SmartAction<tutorials> res = null;
            var entity = new Calani.BusinessObjects.Model.tutorials()
            {
                
                name = model.Name,
                type = (int)model.Type,
                min_version = model.MinVersion,
                updated_by = userId,
                recordStatus = (int)RecordStatus.Active
            };

            var tId = model.Id;
            if (model.Id < 1)
            {
                res = this.Add(entity);

                tId = res.Record.id;
            }
            else
                res = this.Update(model.Id, entity);

            

            var screenMngr = new TutorialScreensManager();
            screenMngr.SaveScreens(userId, tId, model.Screens);

            return tId;
        }

        public TutorialModel GeTutorial(long userId, long id, TutorialsType type, string version = null)
        {


            decimal dver = 0;

            if (!String.IsNullOrWhiteSpace(version))
            {
                decimal d;
                if (decimal.TryParse(version, out d))
                    dver = d;
            }

            TutorialModel model = new TutorialModel
            {
                Screens = new List<TutorialScreenModel>()
            };

            if (id > 0)
            {
                var q = GetBaseQuery().Query;
                var entity = q.FirstOrDefault(t => t.id == id && t.recordStatus == (int)RecordStatus.Active && t.type == (int)type);
                model = entity.ToDto(userId,true);
                if (dver > 0)
                {
                    
                   
                        if (dver < model.MinVersion)
                            model = null;
                        else
                        {
                            
                            UpdateCountDeployed(userId, model, version);
                        }
                    
                }
            }
            
            return model;
        }

        public bool IsNewTutorials(long userId)
        {
            return GeTutorialsList(userId, TutorialsType.Web, false, true).Any(a=>a.Viewed < 1 );
        }

        private void UpdateCountDeployed(long userId, TutorialModel model, string version)
        {
            var t = GetById(model.Id);

            var count = t.Context.mobile_tutorials_deployed.Count(d => d.tutorial_id == model.Id && d.user_id == userId);

            if (count < 1)
            {
                t.Context.mobile_tutorials_deployed.Add(new mobile_tutorials_deployed()
                    { tutorial_id = model.Id, user_id = userId, version = version });
                t.Context.SaveChanges();
            }

        }

        internal override DbSet<tutorials> GetDbSet(Model.CalaniEntities db)
        {
            return db.tutorials;
        }

        internal override SmartQuery<tutorials> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.tutorials where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(tutorials record)
        {
           // if (record != null )
           //     return record.organization_Id;
            return -1;
        }

        internal override SmartQuery<tutorials> GetBaseQuery()
        {
            var q = base.GetBaseQuery();
            // base
            q.Query = (from r in q.Context.tutorials select r);

           

            if (!IgnoreRecordStatus) 
                q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) 
                q.Query = (from r in q.Query orderby r.id  select r);


            return q;
        }

        public List<TutorialModel> GeTutorialsList(long userId, TutorialsType type, bool details = false, bool versionFilter = false)
        {
           
            List<TutorialModel> ret = null;

            var q = GetBaseQuery();

            var queue = (from r in q.Query
                where r.recordStatus == (int) RecordStatus.Active && r.type == (int) type
                select r);

         

            if (versionFilter)
            {
                var vStr  = System.Configuration.ConfigurationManager.AppSettings["Version"];
                decimal version = 0;

                if(decimal.TryParse(vStr, out version))
                {
                    queue = queue.Where(w => version>=w.min_version);

                }


            }

            var entities = queue.OrderByDescending(r => r.id).ToList();

            ret = entities.Select(e => e.ToDto(userId, details)).ToList();
            return ret;
        }

        public bool CheckVersion(Double version, string minVerStr)
        {
            Double minVersion = 0;
            if (Double.TryParse(minVerStr, out minVersion)) 
                return version>=minVersion;

            return false;
        }
        internal override void BeforeAction(SmartAction<tutorials> action)
        {
            if (action.Type == SmartActionType.Update && action.Record != null && action.Record.updated_at == null)
            {
                action.Record.updated_at = DateTime.UtcNow;
            }
        }

        public void FinishTutorial(long userId, long tid)
        {
            var db = new Model.CalaniEntities();
            var stat = new tutorials_stat();
            
            stat.recordStatus = 0;
            stat.tutorial_id = tid;
            stat.viewed_by = userId;
            stat.viewed_date = DateTime.UtcNow;
            
            
            db.tutorials_stat.Add(stat);
            db.SaveChanges();
        }

        public void FinishAllTutorials(long userId, TutorialsType type)
        {
            var list =  GeTutorialsList(userId, type, false, false).Where(a => a.Viewed < 1);

            foreach (var tuto in list)
            {
                FinishTutorial(userId, tuto.Id);
            }
        }
    }

    public class TutorialScreensManager : SmartCollectionManager<tutorials_screens, long>
    {

        public TutorialScreensManager()
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
        }

        internal override DbSet<Model.tutorials_screens> GetDbSet(Model.CalaniEntities db)
        {
            return db.tutorials_screens;
        }

        internal override SmartQuery<Model.tutorials_screens> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = from r in q.Context.tutorials_screens where r.id == id select r;
            return q;
        }


        /// <summary>
        /// GetBaseQuery
        /// </summary>
        /// <returns>query of profiles</returns>
        internal override SmartQuery<Model.tutorials_screens> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = from r in q.Context.tutorials_screens select r;
            q.Query = from r in q.Query where (r.recordStatus == null || r.recordStatus == 0) select r;

            return q;
        }


        internal override void BeforeAction(SmartAction<tutorials_screens> action)
        {
            if (action.Type == SmartActionType.Update && action.Record != null && action.Record.updated_at == null)
            {
                action.Record.updated_at = DateTime.UtcNow;
            }
            base.BeforeAction(action);
        }

        public List<TutorialScreenModel> GetScreens(long tutorialId)
        {
            var q = GetBaseQuery();
            q.Query = from r in q.Query where r.tutorial_id == tutorialId  && r.recordStatus == (int)RecordStatus.Active select r;
            
            var data = q.Query.ToList();

            return (from r in data select r.ToDto()).ToList();
        }

        public void SaveScreens(long userId, long tutorialId, List<TutorialScreenModel> modelScreens)
        {
            var q = GetBaseQuery().Query;
            var records = q.Where(s => s.tutorial_id == tutorialId).Select(r=>r.id).ToList();

            var deleteRec = records.Where(i => modelScreens.All(m => m.Id != i));

            foreach (var record in deleteRec)
            {
                this.Delete(record, DeleteMethod.DeleteRecord);
            }

            foreach (var screen in modelScreens)
            {
                var entity = new tutorials_screens()
                {
                    tutorial_id = tutorialId,
                    position = screen.Position,
                    title = screen.Title,
                    explanation = screen.Explanation,
                    image = screen.Image,
                    filename = screen.FileName,
                    updated_by = userId,
                    recordStatus = (int)RecordStatus.Active
                };

                if (screen.Id > 0)
                    this.Update(screen.Id, entity);
                else
                    this.Add(entity);

            }
        }
    }

}
