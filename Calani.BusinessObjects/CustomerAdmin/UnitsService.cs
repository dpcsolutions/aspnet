﻿using Calani.BusinessObjects.FrontEndModels;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Calani.BusinessObjects.CustomerAdmin
{
    public class UnitsService
    {
        private UnitsManager _unitsManager;

        public UnitsService(long orgId)
        {
            _unitsManager = new UnitsManager(orgId);
        }

        public List<UnitModel> GetUnits(CultureInfo culture)
        {
            var units = _unitsManager.ListUnits();

            var emptyUnit = new UnitModel()
            {
                Name = ""
            };

            var models = new List<UnitModel>() { emptyUnit };

            models.AddRange(units.Select(u => new UnitModel()
            {
                Id = u.id,
                Name = !string.IsNullOrEmpty(u.stringResourceName) ? SharedResource.Resource.GetString(u.stringResourceName, culture) : u.name,
                OrganizationId = u.organizationId
            }).OrderByDescending(u => u.OrganizationId ?? 0)
            .ToList());

            return models;
        }
    }
}
