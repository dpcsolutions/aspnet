﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.CustomerAdmin
{
    public enum ServiceType
    {
        BillableServices = 1,
        InternalJobs = 2
    }
}
