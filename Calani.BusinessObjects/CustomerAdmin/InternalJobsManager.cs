﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Imports;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.CustomerAdmin
{
    public class InternalJobsManager : Generic.SmartCollectionManager<Model.services, long>
    {
        int typeFilter = 0;

        public InternalJobsManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
            typeFilter = Convert.ToInt32(ServiceType.InternalJobs);
        }



        internal override DbSet<services> GetDbSet(Model.CalaniEntities db)
        {
            return db.services;
        }

        private SmartQuery<services> GetByInternalId(string id)
        {
            var q = GetBaseQuery();
            q.Query = (from r in q.Query where r.internalId == id select r);
            return q;
        }


        internal override SmartQuery<services> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in GetDbSet(q.Context) where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(services record)
        {
            if (record.organizationId != null) return record.organizationId.Value;
            return -1;
        }

        internal override SmartQuery<services> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from r in q.Context.services where r.type == typeFilter select r);
            if (OrganizationID > -1) q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.name select r);


            return q;
        }

        internal override void BeforeAction(SmartAction<services> action)
        {
            if(action.Record != null && action.Record.internalId != null)
            {
                if(action.Record.internalId.Length > 11)
                {
                    action.Record.internalId = action.Record.internalId.Substring(0, 11);
                }
            }
        }

        public List<ImportedDataItem> BatchImport(List<string> columns, List<Imports.Row> rows)
        {
            var ret = new List<ImportedDataItem>();

            Model.CalaniEntities db = new CalaniEntities();
            List<Model.services> adds = new List<services>();


            foreach (var row in rows)
            {
                string uniqId = ImportedDataItem.GetValue(row, "InternalID", columns);

                if (GetByInternalId(uniqId).Query.FirstOrDefault() == null)
                {

                    Model.services s = new services();
                    s.organizationId = OrganizationID;
                    s.internalId = uniqId;
                    s.name = ImportedDataItem.GetValue(row, "Name", columns);
                    double? price = ImportedDataItem.GetValueAsDouble(row, "UnitPrice", columns);
                    if (price != null) s.unitPrice = price.Value;
                    s.type = typeFilter;



                    db.services.Add(s);
                    adds.Add(s);
                }
            }

            db.SaveChanges();

            foreach (var r in adds)
            {
                ret.Add(new ImportedDataItem
                {
                    Id = r.id,
                    Name = r.name
                });
            }

            return ret;
        }
    }
}
