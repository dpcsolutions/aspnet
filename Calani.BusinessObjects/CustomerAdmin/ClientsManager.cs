﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Imports;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Projects;

namespace Calani.BusinessObjects.CustomerAdmin
{
    public class ClientsManager : Generic.SmartCollectionManager<Model.clients, long>
    {

        public ClientsManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        public long Count()
        {
            return GetBaseQuery().Query.LongCount();
        }

        public List<clients> GetLasts(int n)
        {
            var q = GetBaseQuery().Query;
            return (from r in q orderby r.id descending select r).Take(n).ToList();
        }

        internal override DbSet<clients> GetDbSet(Model.CalaniEntities db)
        {
            return db.clients;
        }

        internal override SmartQuery<clients> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.clients where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(clients record)
        {
            if (record != null && record.organizationId != null) return record.organizationId.Value;
            return -1;
        }

        internal override SmartQuery<clients> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from r in q.Context.clients select r);
            if (OrganizationID > -1) q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.companyName select r);

            

            return q;
        }

        public SmartQuery<clients> GetPublicQuery() { return GetBaseQuery(); }
        
        internal override void BeforeAction(SmartAction<clients> action)
        {
            if (action.Type == SmartActionType.Add || action.Type == SmartActionType.Update)
            {
                if (action.Record.companyName == null || action.Record.companyName.Length < 2)
                {
                    action.Abort("ERR_Client_name_2_chars");
                }

                if ((from r in GetBaseQuery().Query where r.companyName == action.Record.companyName && r.id != action.ExistingRecord.id select r).Count() > 0)
                {
                    action.Abort("ERR_Client_name_alredy_used");
                }
            }


            if(action.Type == SmartActionType.Delete)
            {
                Model.CalaniEntities db = new CalaniEntities();
                var jobs = (from r in db.jobs where r.organizationId == OrganizationID && r.clientId == action.ExistingRecord.id select r).ToList();
                foreach (var job in jobs)
                {
                    job.recordStatus = -1;
                    var visits = (from r in db.visits where r.orgId == OrganizationID && r.jobId == job.id select r).ToList();
                    foreach (var visit in visits) visit.recordStatus = -1;
                }

                var contacts = (from r in db.contacts where r.organizationId == OrganizationID && r.clientId == action.ExistingRecord.id select r).ToList();
                foreach (var contact in contacts) contact.recordStatus = -1;

                var addresses = (from r in db.addresses where r.organizationId == OrganizationID && r.clientId == action.ExistingRecord.id select r).ToList();
                foreach (var address in addresses) address.recordStatus = -1;


                db.SaveChanges();
            }

        }

        public contacts GetClientMainContact(long clientId)
        {
            Model.CalaniEntities db = new CalaniEntities();
            var f = Convert.ToInt32(Contacts.ContactTypeEnum.Contact_CustomerMain);
            return (from r in db.contacts where r.recordStatus == 0  &&  r.type == f  && r.clientId == clientId select r).FirstOrDefault();
        }

        public List<Model.clients> ListWithContactsAndAddresses()
        {
            var bq = GetBaseQuery();
            var xQ = (from r in bq.Query.Include("contacts").Include("addresses") where r.recordStatus == 0  select r);
            var dbRecords = xQ.ToList();
            return dbRecords;
        }

        public Model.clients GetWithContactsAndAddresses(long clientId)
        {
            var bq = GetBaseQuery();
            var xQ = (from r in bq.Query.Include("contacts").Include("addresses") where r.recordStatus == 0  && r.id == clientId select r);
            var dbRecords = xQ.FirstOrDefault();
            return dbRecords;
        }

        

        public List<clientsExtended> ListExtended(DateTime? statsFrom, DateTime? statsTo, long? employeeFilter = null)
        {
            Projects.ProjectsManager pmgr = new Projects.ProjectsManager(OrganizationID);
            pmgr.CorrectClosedInvoinces();

            var bq = GetBaseQuery();
            var xQ = (from r in bq.Query.Include("contacts") select r);

            var dbRecords = xQ.ToList();
            List<clientsExtended> ret =(from r in dbRecords select new clientsExtended(r)).ToList();




            // stats on quotes

            var qQuotes = (from r in bq.Context.jobs
                where  r.dateOpenJob == null
                      && r.dateInvoicing == null
                      && r.subtype == "Quote"
                select r);


            if (statsFrom != null)
                qQuotes = (from r in qQuotes where r.lastUpdate >= statsFrom select r);

            if (statsTo != null)
                qQuotes = (from r in qQuotes where r.lastUpdate <= statsTo select r);

            var quotes = (from r in qQuotes
                          group r by r.clientId into g select new { client = g.Key, quotes = g.LongCount() }).ToList();


            // -->

            /*notes*/


            var qNotes = (from r in bq.Context.jobs
                where r.organizationId == OrganizationID
                      && r.recordStatus != -1
                      && r.dateOpenJob == null
                      && r.dateInvoicing == null
                      && r.subtype == "DeliveryNote"
                          select r);

            if (statsFrom != null)
                qNotes = (from r in qNotes where r.lastUpdate >= statsFrom select r);

            if (statsTo != null)
                qNotes = (from r in qNotes where r.lastUpdate <= statsTo select r);

            var notes = (from r in qNotes
                         group r by r.clientId into g select new { client = g.Key, notes = g.LongCount() }).ToList();

            // stats on jobs
            var qJobs = (bq.Context.jobs
                .Where(r => r.dateOpenJob != null && r.dateInvoicing == null && r.organizationId == OrganizationID &&
                            r.recordStatus == 0)
                .Select(r => r));

            if (statsFrom != null) 
                qJobs = (from r in qJobs where (r.lastUpdate ?? r.dateCreated) >= statsFrom select r);

            if (statsTo != null)
                qJobs = (from r in qJobs where (r.lastUpdate?? r.dateCreated) <= statsTo select r);

            if (employeeFilter != null) 
                qJobs = (from r in qJobs where r.visits.Any(v => v.contacts.Any(c => c.id == employeeFilter.Value)) select r);

            var jobs = (from r in qJobs
                          group r by r.clientId into g
                          select new { client = g.Key, jobs = g.LongCount() }).ToList();
            // -->

            // stats on invoices
            var qInvoinces = (from r in bq.Context.jobs
                         where r.dateInvoicing != null
                         
                         && r.organizationId == OrganizationID
                         && r.recordStatus == 0
                              select r);
            if (statsFrom != null) 
                qInvoinces = (from r in qInvoinces where r.lastUpdate >= statsFrom select r);

            if (statsTo != null)
                qInvoinces = (from r in qInvoinces where r.lastUpdate <= statsTo select r);

            var invoices = (qInvoinces.GroupBy(r => r.clientId)
                .Select(g => new
                {
                    client = g.Key,
                    invoices = g.LongCount(),
                    total = g.Sum(i => i.total),
                    totalWithoutTaxes = g.Where(i=>i.dateInvoicingOpen!=null).Sum(i => i.totalWithoutTaxes),
                    invoiceDue = g.Sum(i => i.invoiceDue),
                })).ToList();
            // -->


            // compile stats in the customer object
            foreach (var c in ret)
            {
                double? dTotal = (from r in invoices where r.client == c.id select r.total).FirstOrDefault();
                double? dTotalWithoutTaxes = (from r in invoices where r.client == c.id select r.totalWithoutTaxes).FirstOrDefault();
                
                double? dDue = (from r in invoices where r.client == c.id select r.invoiceDue).FirstOrDefault();

                c.CountQuotes = (from r in quotes where r.client == c.id select r.quotes).FirstOrDefault();
                
                c.CountDeliveryNotes = (from r in notes where r.client == c.id select r.notes).FirstOrDefault();

                c.CountJobs = (from r in jobs where r.client == c.id select r.jobs).FirstOrDefault();
                c.CountInvoices = (from r in invoices where r.client == c.id select r.invoices).FirstOrDefault();
                
                if (c.CountDeliveryNotes != null && c.CountDeliveryNotes.Value == 0) 
                    c.CountDeliveryNotes = null;

                if (c.CountQuotes != null && c.CountQuotes.Value == 0)
                    c.CountQuotes = null;

                if (c.CountJobs != null && c.CountJobs.Value == 0) 
                    c.CountJobs = null;

                if (c.CountInvoices != null && c.CountInvoices.Value == 0)
                    c.CountInvoices = null;


                if (dTotal != null && dTotal.Value > 0)
                  c.SumInvoicedTotal = dTotal.Value;


                if (dTotalWithoutTaxes != null && dTotalWithoutTaxes.Value > 0)
                  c.SumInvoicedTotalWithoutTaxes = dTotalWithoutTaxes.Value;
                

                if (dDue != null && dDue.Value > 0) 
                    c.SumInvoicedDue = dDue.Value;


                c.companyName = c.companyName.Trim();
            }
            // -->

            return ret;
        }


        public List<ImportedDataItem> BatchImport(List<string> columns, List<Imports.Row> rows)
        {
            var ret = new List<ImportedDataItem>();

            Model.CalaniEntities db = new CalaniEntities();
            List<Model.clients> adds = new List<clients>();

            foreach (var row in rows)
            {
                Model.clients cust = new clients();
                cust.organizationId = OrganizationID;
                cust.companyName = ImportedDataItem.GetValue(row, "CustName", columns);

                if (ImportedDataItem.GetValue(row, "IsCompany", columns).Trim() == "1")
                {
                    cust.isCompany = true;
                }
                else
                {
                    cust.isCompany = false;
                    if(String.IsNullOrWhiteSpace(cust.companyName))
                    {
                        string lbl = "";
                        lbl += ImportedDataItem.GetValue(row, "LastName", columns);
                        lbl += " ";
                        lbl += ImportedDataItem.GetValue(row, "FirstName", columns);
                        lbl = lbl.Trim();
                        cust.companyName = lbl;
                    }
                }
                adds.Add(cust);

                Model.contacts contact = new contacts();
                contact.organizationId = OrganizationID;
                contact.type = Convert.ToInt32(Contacts.ContactTypeEnum.Contact_CustomerMain);
                contact.clients = cust;
                contact.lastName = ImportedDataItem.GetValue(row, "LastName", columns);
                contact.firstName = ImportedDataItem.GetValue(row, "FirstName", columns);
                contact.civtitle = ImportedDataItem.GetValue(row, "Title", columns);
                contact.primaryEmail = ImportedDataItem.GetValue(row, "Email", columns);
                contact.secondaryEmail = ImportedDataItem.GetValue(row, "Email2", columns);
                contact.primaryPhone = ImportedDataItem.GetValue(row, "Phone", columns);
                contact.secondaryPhone = ImportedDataItem.GetValue(row, "Phone2", columns);

         


                Model.addresses addr = new addresses();
                addr.organizationId = OrganizationID;
                addr.type = Convert.ToInt32(Contacts.AddressTypeEnum.CustomerAddress);
                addr.clients = cust;
                addr.caption = ImportedDataItem.GetValue(row, "Service", columns);
                addr.street = ImportedDataItem.GetValue(row, "Street", columns);
                addr.nb = ImportedDataItem.GetValue(row, "StreetNr", columns);
                addr.street2 = ImportedDataItem.GetValue(row, "Street2", columns);
                addr.postbox = ImportedDataItem.GetValue(row, "PostalBox", columns);
                addr.city = ImportedDataItem.GetValue(row, "City", columns);
                addr.npa = ImportedDataItem.GetValue(row, "Postalcode", columns);
                addr.state = ImportedDataItem.GetValue(row, "State", columns);


                // valeur par défaut
                if(String.IsNullOrWhiteSpace(cust.companyName) && !String.IsNullOrWhiteSpace(contact.lastName))
                {
                    cust.companyName = contact.lastName;
                    if (!String.IsNullOrWhiteSpace(contact.firstName)) cust.companyName += " " + contact.firstName.Substring(0, 1);
                }
                else if (String.IsNullOrWhiteSpace(contact.lastName) && !String.IsNullOrWhiteSpace(cust.companyName))
                {
                    contact.lastName = cust.companyName;
                }

                if (String.IsNullOrWhiteSpace(cust.companyName)) cust.companyName = "CUST" + rows.IndexOf(row) + 1;
                if(String.IsNullOrWhiteSpace(contact.lastName)) contact.lastName = "CUST" + rows.IndexOf(row) + 1;
                //if(String.IsNullOrWhiteSpace(addr.city)) addr.city = "CUST" + rows.IndexOf(row) + 1;
                // -->

                db.clients.Add(cust);
                db.contacts.Add(contact);
                db.addresses.Add(addr);

            }

            db.SaveChanges();

            foreach (var r in adds)
            {
                ret.Add(new ImportedDataItem
                {
                    Id = r.id,
                    Name = r.companyName
                });
            }

            return ret;
        }

        
    }

    
}
