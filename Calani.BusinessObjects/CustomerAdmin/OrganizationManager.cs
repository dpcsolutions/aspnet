﻿using Calani.BusinessObjects.Calendars;
using Calani.BusinessObjects.Enums;
using System;
using System.Linq;

namespace Calani.BusinessObjects.CustomerAdmin
{
    public class OrganizationManager
    {
        public long _id;

        public string CompanyName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Telecopy { get; set; }
        public long? OwnerUserId { get; set; }
        public byte[] Logo { get; set; }


        public string AdrStreet { get; set; }
        public string AdrStreetNb { get; set; }
        public string AdrStreet2 { get; set; }
        public string AdrCity { get; set; }
        public string AdrPostalBox { get; set; }
        public string AdrZipCode { get; set; }
        public string AdrState { get; set; }
        public string AdrCountry { get; set; }


        public int DelayQuote { get; set; }
        public int DelayInvoice { get; set; }
        public int UnpaidInvoiceReminderDays { get; set; }
        public DateTime FinancialYearStart { get; set; }
        public string Currency { get; set; }
        public int EmailAlertLateInvoicesDay { get; set; }


        public string VatNumber { get; set; }
        public string WebSite { get; set; }


        public string BankAccount1_Currency { get; set; }
        public string BankAccount1_BankName { get; set; }
        public string BankAccount1_AccountName { get; set; }
        public string BankAccount1_Iban { get; set; }
        public string BankAccount1_Swift { get; set; }


        public string BankAccount2_Currency { get; set; }
        public string BankAccount2_BankName { get; set; }
        public string BankAccount2_AccountName { get; set; }
        public string BankAccount2_Iban { get; set; }
        public string BankAccount2_Swift { get; set; }

        Contacts.AddressTypeEnum _addrType = Contacts.AddressTypeEnum.CompanyAddress;


        public int BvrMode { get; set; }
        public int BvrType { get; set; }
        public int BvrTopMargin { get; set; }
        public int BvrLeftMargin { get; set; }
        public string BvrSenderBank { get; set; }
        public string BvrAccount { get; set; }
        public string BvrBankLine1 { get; set; }
        public string BvrBankLine2 { get; set; }
        public string BeforeInvoice { get; set; }
        public int LunchDuration { get; set; }
        public int DailyBreakDuration { get; set; }
        public int Vacation { get; set; }
        public double AbsenceDayDurationHours { get; set; }
        public int TimesheetEmailOption { get; set; }

        int WeekWorkMinutes { get; set; }
        

        public double WeekWorkHours
        {
            set { WeekWorkMinutes = Convert.ToInt32(value * 60);}
            get
            {
                return (double)WeekWorkMinutes / 60;
            }

        }
        public OrganizationManager(long id)
        {
            _id = id;
        }
        public void Load()
        {
            Model.CalaniEntities db = new Model.CalaniEntities();
            var org = (from r in db.organizations where r.id == _id select r).FirstOrDefault();
            
            if (org == null)
                return;

            CompanyName = org.name;
            Email = org.email;
            Phone = org.phone;
            Telecopy = org.telecopy;
            Logo = org.logo;
            OwnerUserId = org.owner;
            WebSite = org.website;
            VatNumber = org.vatnumber;
            WeekWorkMinutes = org.week_work_duration;
            LunchDuration = org.lunch_duration;
            DailyBreakDuration = org.daily_break_duration;
            AbsenceDayDurationHours = (double)org.absence_day_duration_minutes / 60;
            TimesheetEmailOption = org.timesheet_email_option;
            Vacation = org.vacation;
            AssignedWorkScheduleId = org.work_schedule_profile_id;
            UerLimit = org.userLimit;
            DailyMailsLimit = org.dailyMailsLimit;
            if(org.work_schedule_profiles != null)
                AssignedWorkSchedule = org.work_schedule_profiles.ToDto();


            DelayQuote = 30;
            DelayInvoice = 30;
            FinancialYearStart = new DateTime(2000, 1, 1);
            Currency = org.currency;

            if (org.emailAlertLateInvoicesDay != null)
                EmailAlertLateInvoicesDay = org.emailAlertLateInvoicesDay.Value;
            else
                EmailAlertLateInvoicesDay = 1;
            

            if (org.delayQuote != null) 
                DelayQuote = org.delayQuote.Value;

            if (org.delayInvoice != null)
                DelayInvoice = org.delayInvoice.Value;

            UnpaidInvoiceReminderDays = org.unpaid_invoice_reminder_days;

            if (org.financialYearStart != null)
                FinancialYearStart = org.financialYearStart.Value;

            if (!String.IsNullOrWhiteSpace(org.currency)) 
                Currency = org.currency;

            BeforeInvoice = org.beforeinvoice;
            if (String.IsNullOrWhiteSpace(BeforeInvoice)) 
                BeforeInvoice = "Quote";

            TimeSheetType = org.timesheet_type;
            TimeSheetLinesPerDay = org.timesheet_lines_per_day;

            UseWorkSchedule = org.use_work_schedule;
            ShowContactInfoInInvoices = org.show_contact_info_invoices;
            PutArticlesInBoldInPdf = org.articles_bold_pdf;
            EmployeeCanViewAllOpenProjects = org.employee_view_all_open_proj;
            DisableInvoicedOnly = org.disable_invoiced_service;
            EmployeeCanCreateProjects = org.employee_create_projects;
            DisplayTotalQuantity = org.display_total_quantity;
            EmployeeCanViewAllEvents = org.employee_view_all_events;
            EmployeeCanViewAllSchedulers = org.give_all_empl_access_to_schedulers;
            HideCodeOnPdf = org.hide_code_on_pdf;
            RoundAmount = org.round_amount;

            DefaultPeriodFilter = (PeriodFilterOptionsEnum)(org.default_period_filter ?? 0);
            EnableRefNumberInheritance = org.enable_ref_num_inheritance;
            HideBillingElementsOnProject = org.hide_billing_info_project;
            HideBillingColumnOnCustomers = org.hide_billing_column_customers;
            Hide0DiscountColOnPdf = org.hide_0_discount_col;
            HideUnitColumn = org.hide_unit_column;
            HideQuoteAcceptancePdf = org.hide_quote_acceptance_pdf;
            HideVatPdf = org.hide_vat_pdf;
            DeliveryNotePdfTitle = org.delivery_note_pdf_title;
            DeliveryNotePdfType = (TypeOfDeliveryNotePdfEnum)org.delivery_note_pdf_type;
            DisplayServiceCustomNumber = org.display_service_custom_number;
            UsePriceIncludingVat = org.use_price_including_vat;

            var adrtype = Convert.ToInt32(_addrType);
            var adr = (from r in db.addresses where r.type == adrtype && r.organizationId == _id select r).FirstOrDefault();
            if(adr != null)
            {
                AdrStreet = adr.street;
                AdrStreetNb = adr.nb;
                AdrStreet2 = adr.street2;
                AdrCity= adr.city;
                AdrZipCode = adr.npa;
                AdrState = adr.state;
                AdrCountry = adr.country;
                AdrPostalBox = adr.postbox;
            }


            var bnk1 = (from r in db.bankaccount where r.organizationId == _id && r.priority == 1 select r).FirstOrDefault();
            if(bnk1 != null)
            {
                BankAccount1_AccountName = bnk1.accountname;
                BankAccount1_BankName = bnk1.bankname;
                BankAccount1_Currency = bnk1.currency;
                BankAccount1_Iban = bnk1.iban;
                BankAccount1_Swift = bnk1.swift;
            }

            var bnk2 = (from r in db.bankaccount where r.organizationId == _id && r.priority == 2 select r).FirstOrDefault();
            if (bnk2 != null)
            {
                BankAccount2_AccountName = bnk2.accountname;
                BankAccount2_BankName = bnk2.bankname;
                BankAccount2_Currency = bnk2.currency;
                BankAccount2_Iban = bnk2.iban;
                BankAccount2_Swift = bnk2.swift;
            }


            if (String.IsNullOrWhiteSpace(BankAccount1_Iban)) BankAccount1_Iban = "CH0000000000000000000";

            var bvro = (from r in db.bvr where r.organizationId == _id select r).FirstOrDefault();
            if (bvro != null)
            {

                if (bvro.mode != null)
                {
                    BvrMode = bvro.mode.Value;
                }
                else
                {
                    BvrMode = 0;
                }

                if (bvro.type != null)
                {
                    BvrType = bvro.type.Value;
                }
                else
                {
                    BvrType = 1;
                }

                
                BvrSenderBank = bvro.idsenderbank.ToString();
                

                BvrAccount=bvro.idaccount;
                if(bvro.topmargin!=null) BvrTopMargin = bvro.topmargin.Value;
                if (bvro.leftmargin != null) BvrLeftMargin = bvro.leftmargin.Value;
                BvrBankLine1=bvro.tobankline1;
                BvrBankLine2=bvro.tobankline2;
            }
            else
            {
                BvrMode = 0;
                BvrType = 1;

            }
            
        }

        public int? UerLimit { get; set; }
        public int DailyMailsLimit { get; set; }

        public bool EmployeeCanViewAllOpenProjects { get; set; }
        public bool EmployeeCanViewAllEvents { get; set; }
        public bool EmployeeCanViewAllSchedulers { get; set; }
        public bool HideCodeOnPdf { get; set; }
        public bool EmployeeCanCreateProjects { get; set; }
        public bool DisplayTotalQuantity { get; set; }
        public bool RoundAmount { get; set; }
        public bool DisableInvoicedOnly { get; set; }
        

        public ScheduleDto AssignedWorkSchedule { get; set; }

        public long? AssignedWorkScheduleId { get; set; }
        public int TimeSheetType { get; set; }
        public int TimeSheetLinesPerDay { get; set; }
        
        public bool UseWorkSchedule { get; set; }
        public bool ShowContactInfoInInvoices { get; set; }
        public bool PutArticlesInBoldInPdf { get; set; }

        public PeriodFilterOptionsEnum DefaultPeriodFilter { get; set; }
        public bool EnableRefNumberInheritance { get; set; }
        public bool HideBillingElementsOnProject { get; set; }
        public bool HideBillingColumnOnCustomers { get; set; }
        public bool Hide0DiscountColOnPdf { get; set; }
        public bool HideUnitColumn { get; set; }
        public bool HideQuoteAcceptancePdf { get; set; }
        public bool HideVatPdf { get; set; }
        public string DeliveryNotePdfTitle { get; set; }
        public TypeOfDeliveryNotePdfEnum DeliveryNotePdfType { get; set; }
        public bool DisplayServiceCustomNumber { get; set; }
        public bool UsePriceIncludingVat { get; set; }

        public Generic.SmartAction<Model.organizations> Save(bool updateAddress, bool updateBankAccounts, bool updateDelaysAndCurrency, bool updateBvr)
        {
            Generic.SmartAction<Model.organizations> ret = new Generic.SmartAction<Model.organizations>(Generic.SmartActionType.Update);
            ret.Success = true;

            Model.CalaniEntities db = new Model.CalaniEntities();
            var org = (from r in db.organizations where r.id == _id select r).First();

            if (updateAddress)
            {
                if (!String.IsNullOrWhiteSpace(CompanyName)) 
                    org.name = CompanyName;

                if (!String.IsNullOrWhiteSpace(Email))
                    org.email = Email;

                if (!String.IsNullOrWhiteSpace(Phone))
                    org.phone = Phone;

                if (!String.IsNullOrWhiteSpace(Telecopy)) 
                    org.telecopy = Telecopy;

                org.website = WebSite;

                if (!String.IsNullOrWhiteSpace(VatNumber)) 
                    org.vatnumber = VatNumber;

                if (OwnerUserId != null)
                {
                    var admin = Convert.ToInt32(Contacts.ContactTypeEnum.User_Admin);
                    var daId = (from r in db.contacts where r.id == OwnerUserId.Value && r.organizationId == _id && r.type == admin select r.id).FirstOrDefault();
                    org.owner = daId;
                }

                if (Logo != null)
                {
                    Generic.ImageManager img = new Generic.ImageManager();
                    img.TargetHeight = 400;
                    img.TargetWidth = 400;
                    img.Init(Logo);


                    org.logo = img.Save();

                    img.Dispose();
                }
                if (OwnerUserId != null) org.owner = OwnerUserId;
            }

            if (updateDelaysAndCurrency)
            {
                org.delayQuote = DelayQuote;
                org.delayInvoice = DelayInvoice;
                org.financialYearStart = FinancialYearStart;
                org.unpaid_invoice_reminder_days = UnpaidInvoiceReminderDays;
                org.emailAlertLateInvoicesDay = EmailAlertLateInvoicesDay;
                
                if (String.IsNullOrWhiteSpace(BeforeInvoice)) 
                    BeforeInvoice = "Quote";

                org.beforeinvoice = BeforeInvoice;
                
                if (Currency != null) 
                    org.currency = Currency;

                org.work_schedule_profile_id = AssignedWorkScheduleId;
               
                org.week_work_duration = WeekWorkMinutes;
                org.lunch_duration = LunchDuration;
                org.daily_break_duration = DailyBreakDuration;
                org.absence_day_duration_minutes = (int)Math.Round(AbsenceDayDurationHours * 60);
                org.timesheet_email_option = TimesheetEmailOption;

                org.vacation = Vacation;
                org.timesheet_type = TimeSheetType;
                org.use_work_schedule = UseWorkSchedule;
                org.show_contact_info_invoices = ShowContactInfoInInvoices;
                org.articles_bold_pdf = PutArticlesInBoldInPdf;
                org.employee_view_all_open_proj = EmployeeCanViewAllOpenProjects;
                org.disable_invoiced_service = DisableInvoicedOnly;
                org.employee_view_all_events = EmployeeCanViewAllEvents;
                org.give_all_empl_access_to_schedulers = EmployeeCanViewAllSchedulers;
                org.hide_code_on_pdf = HideCodeOnPdf;
                org.employee_create_projects = EmployeeCanCreateProjects;
                org.display_total_quantity = DisplayTotalQuantity;
                org.round_amount = RoundAmount;
                org.default_period_filter = (sbyte)DefaultPeriodFilter;
                org.enable_ref_num_inheritance = EnableRefNumberInheritance;
                org.hide_billing_info_project = HideBillingElementsOnProject;
                org.hide_billing_column_customers = HideBillingColumnOnCustomers;
                org.hide_0_discount_col = Hide0DiscountColOnPdf;
                org.hide_unit_column = HideUnitColumn;
                org.hide_vat_pdf = HideVatPdf;
                org.hide_quote_acceptance_pdf = HideQuoteAcceptancePdf;
                org.delivery_note_pdf_title = DeliveryNotePdfTitle;
                org.delivery_note_pdf_type = (sbyte)DeliveryNotePdfType;
                org.display_service_custom_number = DisplayServiceCustomNumber;
                org.use_price_including_vat = UsePriceIncludingVat;

                org.timesheet_lines_per_day = TimeSheetLinesPerDay;

            }

            if (updateAddress)
            {
                var adrtype = Convert.ToInt32(_addrType);
                var adr = (from r in db.addresses where r.type == adrtype && r.organizationId == _id select r).FirstOrDefault();
                if (adr == null)
                {
                    adr = new Model.addresses
                    {
                        organizationId = _id,
                        type = Convert.ToInt32(_addrType)
                    };
                    db.addresses.Add(adr);
                }

                adr.street = AdrStreet;
                adr.nb = AdrStreetNb;
                adr.street2 = AdrStreet2;
                adr.city = AdrCity;
                adr.npa = AdrZipCode;
                adr.state = AdrState;
                adr.country = AdrCountry;
                adr.postbox = AdrPostalBox;

            }

            if(updateBankAccounts)
            {
                var bnk1 = (from r in db.bankaccount where r.organizationId == _id && r.priority == 1 select r).FirstOrDefault();
                if(bnk1 == null)
                {
                    bnk1 = new Model.bankaccount
                    {
                        organizationId = _id,
                        priority = 1
                    };
                    db.bankaccount.Add(bnk1);

                    
                }

                bnk1.accountname = BankAccount1_AccountName;
                bnk1.bankname = BankAccount1_BankName;
                bnk1.currency = BankAccount1_Currency;
                bnk1.iban = BankAccount1_Iban;
                bnk1.swift = BankAccount1_Swift;


                var bnk2 = (from r in db.bankaccount where r.organizationId == _id && r.priority == 2 select r).FirstOrDefault();
                if (bnk2 == null)
                {
                    bnk2 = new Model.bankaccount
                    {
                        organizationId = _id,
                        priority = 2
                    };
                    db.bankaccount.Add(bnk2);


                }

                bnk2.accountname = BankAccount2_AccountName;
                bnk2.bankname = BankAccount2_BankName;
                bnk2.currency = BankAccount2_Currency;
                bnk2.iban = BankAccount2_Iban;
                bnk2.swift = BankAccount2_Swift;
            }


            if(updateBvr)
            {
                var bvro = (from r in db.bvr where r.organizationId == _id select r).FirstOrDefault();
                if(bvro == null)
                {
                    bvro = new Model.bvr
                    {
                        organizationId = _id
                    };
                    db.bvr.Add(bvro);
                }

                bvro.mode = BvrMode;
                bvro.type = BvrType;
                bvro.idsenderbank = BvrSenderBank;
                bvro.idaccount = BvrAccount;
                bvro.topmargin = BvrTopMargin;
                bvro.leftmargin = BvrLeftMargin;
                bvro.tobankline1 = BvrBankLine1;
                bvro.tobankline2 = BvrBankLine2;
            }

            db.SaveChanges();

            return ret;
        }
    }

}
