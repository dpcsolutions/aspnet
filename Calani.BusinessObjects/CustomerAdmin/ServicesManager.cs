﻿using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.MobileService2;
using Calani.BusinessObjects.MobileService2.Data;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Calani.BusinessObjects.CustomerAdmin
{
    public class ServicesManager : Generic.SmartCollectionManager<Model.services, long>
    {
        int typeFilter = 0;

        private UnitsManager _unitsManager;

        public ServicesManager(long organizationId, int? typeFilterParam = null, UnitsManager unitsManager = null)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
            typeFilter = typeFilterParam == null ? Convert.ToInt32(ServiceType.BillableServices) : (int)typeFilterParam;
            _unitsManager = unitsManager;
        }        

        internal override DbSet<services> GetDbSet(Model.CalaniEntities db)
        {
            return db.services;
        }

        internal override SmartQuery<services> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in GetDbSet(q.Context) where r.id == id  select r);
            return q;
        }

        private SmartQuery<services> GetByInternalId(string id)
        {
            var q = GetBaseQuery();
            q.Query = (from r in q.Query where r.internalId == id select r);
            return q;
        }

        internal override long GetRecordOrganization(services record)
        {
            if (record.organizationId != null) return record.organizationId.Value;
            return -1;
        }

        internal override SmartQuery<services> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = typeFilter != -1 ? (from r in q.Context.services where r.type == typeFilter select r) : (from r in q.Context.services select r);
         //   q.Query = (from r in q.Context.services where r.type == typeFilter select r);
            if (OrganizationID > -1) q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.name select r);


            return q;
        }

        public bool IsFree(string code)
        {
            var q = GetBaseQuery();
            return (from r in q.Query where r.internalId == code select r).Count() == 0;
        }

        public List<Model.services> List2(bool includeTaxes, bool includeUnit = true)
        {
            var bq = GetBaseQuery();

            if (includeTaxes)
            {
                bq.Query = (from r in bq.Query.Include("taxes") select r);
            }

            if (includeUnit)
            {
                bq.Query = bq.Query.Include("units");
            }

            return bq.Query.ToList();
        }

        public List<Model.taxes> ListTaxes()
        {
            var q = new SmartQuery<taxes>();

            q.Query = (from r in q.Context.taxes select r);

            if (OrganizationID > -1) q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.name select r);
            return q.Query.ToList();
        }

        internal override void BeforeAction(SmartAction<services> action)
        {
            if (action.Record != null && action.Record.internalId != null)
            {
                if (action.Record.internalId.Length > 11)
                {
                    action.Record.internalId = action.Record.internalId.Substring(0, 11);
                }
            }


            if (action.Type == SmartActionType.Add || action.Type == SmartActionType.Update)
            {
                if (action.Record.name == null || action.Record.name.Length < 2)
                {
                    action.Abort("ERR_Service_name_2_chars");
                }

                if ((from r in GetBaseQuery().Query where r.name == action.Record.name && r.id != action.ExistingRecord.id select r).Count() > 0)
                {
                    action.Abort("ERR_Service_name_alredy_used");
                }

                if (!String.IsNullOrWhiteSpace(action.Record.internalId))
                {
                    if ((from r in GetBaseQuery().Query where r.internalId == action.Record.internalId && r.id != action.ExistingRecord.id select r).Count() > 0)
                    {
                        action.Abort("ERR_Service_code_alredy_used");
                    }
                }


                if(action.Record != null) action.Record.type = typeFilter;

                /* why not
                if (action.Record.rate != action.ExistingRecord.rate)
                {
                    action.Record.lastChange = DateTime.Now;
                }
                */
            }


        }

        public List<ImportedDataItem> BatchImport(List<string> columns, List<Imports.Row> rows)
        {
            var ret = new List<ImportedDataItem>();

            Model.CalaniEntities db = new CalaniEntities();
            List<Model.services> adds = new List<services>();

            TaxesManager tmgr = new TaxesManager(OrganizationID);
            long defaultVAT = tmgr.GetDetaultTax();

            foreach (var row in rows)
            {
                string uniqId = ImportedDataItem.GetValue(row, "InternalID", columns);

                if (GetByInternalId(uniqId).Query.FirstOrDefault() == null)
                {

                    Model.services s = new services();
                    s.organizationId = OrganizationID;
                    s.internalId = uniqId;
                    s.name = ImportedDataItem.GetValue(row, "Name", columns);
                    double? vat = ImportedDataItem.GetValueAsDouble(row, "VAT", columns);
                    if (vat == null)
                    {
                        s.taxId = defaultVAT;
                    }
                    else
                    {
                        s.taxId = tmgr.FindOrCreateTaxByRate(vat);
                    }
                    double? price = ImportedDataItem.GetValueAsDouble(row, "UnitPrice", columns);
                    if (price != null) s.unitPrice = price.Value;
                    s.type = typeFilter;

                    var unitName = ImportedDataItem.GetValue(row, "Unit", columns);
                    s.unitId = !string.IsNullOrWhiteSpace(unitName) ? GetUnitId(unitName) : null;

                    db.services.Add(s);
                    adds.Add(s);
                }
            }

            db.SaveChanges();

            foreach (var r in adds)
            {
                ret.Add(new ImportedDataItem
                {
                    Id = r.id,
                    Name = r.name
                });
            }

            return ret;
        }

        public Taxes SaveTax(long userId, long organizationId, Taxes tax)
        {

            TaxesManager tmgr = new TaxesManager(organizationId);
            var mid = new MobileId(tax.id);

            var entity = new taxes();

            entity.organizationId = organizationId;
            entity.name = tax.description;
            entity.rate = tax.rate;
            entity.lastChange = DateTime.UtcNow;
            taxes rec = null;

            if (mid.DbId.HasValue) //update
            {
                entity.id = mid.DbId.Value;
               var res = tmgr.Update(entity.id, entity);
               rec = res.Record;

            }
            else
            {
                var res = tmgr.Add(entity);

                rec = res.Record;
            }

            var dto = new Taxes(rec);
            return dto;
        }

        private long? GetUnitId(string unitName)
        {
            long? id = null;

            if (!string.IsNullOrWhiteSpace(unitName))
            {
                var unitsManager = _unitsManager ?? new UnitsManager(this.OrganizationID);

                var existingUnits = unitsManager.ListUnits();
                var existingUnit = existingUnits.SingleOrDefault(u => u.name.ToLower() == unitName.ToLower());

                if (existingUnit == null)
                {
                    var result = unitsManager.Add(new units() { name = unitName });
                    existingUnit = result.Record;
                }

                id = existingUnit.id;
            }

            return id;
        }

    }
}
