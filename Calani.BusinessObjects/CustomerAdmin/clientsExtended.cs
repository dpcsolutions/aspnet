﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.CustomerAdmin
{
   public  class clientsExtended : Model.clients
    {


        public clientsExtended(Model.clients r)
        {
            Generic.Tools.CopyAllProperties(r, this);
        }

        public string ContactName
        {
            get
            {
                string ret = "";
                var f = Convert.ToInt32(Contacts.ContactTypeEnum.Contact_CustomerMain);
                var c = (from r in this.contacts where r.type == f && r.recordStatus == 0 select r).FirstOrDefault();
                if (c != null)
                {
                    ret = c.lastName + " " + c.firstName;
                    ret = ret.Trim();
                }
                if(ret == companyName)
                {
                    ret = "";
                }
                return ret;
            }
        }

        public long? CountQuotes { get; set; }
        public long? CountDeliveryNotes { get; set; }

        public long? CountJobs { get; set; }
        public long? CountInvoices { get; set; }
        public double? SumInvoicedTotal { get; set; }
        public double? SumInvoicedTotalWithoutTaxes { get; set; }
        
        public double? SumInvoicedDue { get; set; }
    
    }
}
