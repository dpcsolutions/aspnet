﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.CustomerAdmin
{
    public class ImportedDataItem
    {
        public string Name { get; set; }
        public long Id { get; set; }


        public static string GetValue(Imports.Row row, string colname, List<string> cols)
        {
            string ret = "";
            int colIndex = cols.IndexOf(colname);
            try
            {
                ret = row.GetCell(colIndex);
            }
            catch { }
            if (String.IsNullOrWhiteSpace(ret)) ret = "";
            return ret;
        }

        public static double? GetValueAsDouble(Imports.Row row, string colname, List<string> cols)
        {
            double? ret = null;
            string str = GetValue(row, colname, cols);
            if(!String.IsNullOrWhiteSpace(str))
            {
                str = str.Trim();
                str = str.Replace(",", ".");
                double d = 0;
                if(double.TryParse(str, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out d))
                {
                    ret = d;
                }

            }
            return ret;
        }
    }
}
