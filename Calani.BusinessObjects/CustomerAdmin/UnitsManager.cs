﻿using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Calani.BusinessObjects.CustomerAdmin
{
    public class UnitsManager : Generic.SmartCollectionManager<Model.units, long>
    {
        public UnitsManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<units> GetDbSet(Model.CalaniEntities db)
        {
            return db.units;
        }

        internal override SmartQuery<units> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            q.Query = (from r in q.Context.units select r);
            if (OrganizationID > -1) q.Query = (from r in q.Query where r.organizationId == null || r.organizationId == OrganizationID select r);

            return q;
        }

        public List<units> ListUnits()
        {
            return GetBaseQuery().Query.ToList();
        }

        public long AddCustomUnit(string unitName)
        {
            if (string.IsNullOrWhiteSpace(unitName))
            {
                throw new ArgumentException("Invalid unit name");
            }

            if(GetBaseQuery().Query.Any(u => u.name.ToLower() == unitName.ToLower()))
            {
                throw new ArgumentException($"Unit name {unitName} already exists");
            }

            var unit = new units()
            {
                organizationId = this.OrganizationID,
                name = unitName
            };

            var res = Add(unit);

            return res.Record.id;
        }

    }
}
