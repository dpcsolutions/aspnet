﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.MobileService2;
using Calani.BusinessObjects.MobileService2.Data;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.CustomerAdmin
{
    public class TaxesManager : Generic.SmartCollectionManager<Model.taxes, long>
    {

        public TaxesManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<taxes> GetDbSet(Model.CalaniEntities db)
        {
            return db.taxes;
        }

        internal override SmartQuery<taxes> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.taxes where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(taxes record)
        {
            if (record.organizationId != null) return record.organizationId.Value;
            return -1;
        }

        internal override SmartQuery<taxes> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from r in q.Context.taxes select r);
            if (OrganizationID > -1) q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.name select r);


            return q;
        }


        internal override void BeforeAction(SmartAction<taxes> action)
        {
            if(action.Type == SmartActionType.Add || action.Type ==  SmartActionType.Update)
            {
                if(action.Record.name == null || action.Record.name.Length < 2)
                {
                    action.Abort("ERR_Tax_name_2_chars");
                }

                
                if((from r in GetBaseQuery().Query where r.name == action.Record.name && r.id != action.ExistingRecord.id select r).Count() > 0)
                {
                    action.Abort("ERR_Tax_name_alreay_used");
                }

                if(action.Record.rate != action.ExistingRecord.rate)
                {
                    action.Record.lastChange = DateTime.Now;
                }
            }

           
        }


        public long GetDetaultTax()
        {
            var q = GetBaseQuery();
            long? def = (from r in q.Query where r.isDefault == true select r.id).FirstOrDefault();
            if (def != null && def > 0) return def.Value;

            //nothing defined is default, get first one
            def = (from r in q.Query select r.id).FirstOrDefault();
            if (def != null) return def.Value;

            // no default set, take the first one with the biggest rate
            long? any = (from r in q.Query orderby r.rate descending  select r.id).FirstOrDefault();
            if (any != null) return any.Value;


            // nothing at all
            return -1;
        }

        public string GetDetaultTaxName()
        {
            long id = GetDetaultTax();
            if(id > 0)
            {
                var tax = Get(id);
                if (tax != null && !String.IsNullOrEmpty(tax.name)) return tax.name;
            }
            return null;
        }



        public bool SetDefautTax(long id)
        {
            bool ret = false;

            var q = GetBaseQuery();
            var toUnflag = (from r in q.Query where r.isDefault == true select r).ToList();
            foreach (var item in toUnflag)
            {
                item.isDefault = null;
            }
            q.Context.SaveChanges();

            q = GetBaseQuery();
            var toFlag = (from r in q.Query where r.id == id select r).FirstOrDefault();
            if(toFlag != null)
            {
                toFlag.isDefault = true;
                ret = true;
            }
            q.Context.SaveChanges();

            return ret;
        }

        public long FindOrCreateTaxByRate(double? vat)
        {
            var q = GetBaseQuery();
            long? existing = (from r in q.Query where r.rate == vat select r.id).FirstOrDefault();
            if (existing != null) return existing.Value;

            if (vat != null)
            {
                var newTax = this.Add(new taxes
                {
                    name = "VAT " + vat.Value,
                    rate= vat.Value,
                    isDefault=false,
                    lastChange=DateTime.Now,
                    organizationId=OrganizationID
                });
                return newTax.Record.id;
            }

            return -1;
        }

        public void SaveTax(long userId, long organizationId, Taxes tax)
        {
            var mid = new MobileId(tax.id);
            
            var entity = new taxes();
            
            if (mid.DbId.HasValue)
                entity.id = mid.DbId.Value;

            entity.organizationId = organizationId;
            entity.name = tax.description;
            entity.rate = tax.rate;
            entity.lastChange = DateTime.UtcNow;

            if (entity.id>0)//update
                this.Update(entity.id, entity);

            else
                this.Add(entity);
        }
    }
}
