﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.Calendars
{
    public class CalendarDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        public DateTime? UpdatedAt { get; set; }
        public long? UpdatedBy { get; set; }
        public int? RecordStatus { get; set; }
        public long OrganizationId { get; set; }
        public string ScheduleIds { get; set; }
    }

    public class CalendarRecordDto
    {
        public long Id { get; set; }
        public long CalendarId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public String StartDateStr
        {
            get { return StartDate.HasValue ? StartDate.Value.ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture) : String.Empty; }
        }

        public String StartTimeStr
        {
            get { return StartDate.HasValue ? StartDate.Value.ToString(Tools.DefaultShortTimeFormat, CultureInfo.InvariantCulture) : String.Empty; }
        }


        public String EndDateStr 
        { 
            get{ return EndDate.HasValue ? EndDate.Value.ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture) : String.Empty; }
        }

        public String EndTimeStr
        {
            get { return EndDate.HasValue ? EndDate.Value.ToString(Tools.DefaultShortTimeFormat, CultureInfo.InvariantCulture) : String.Empty; }
        }

        public string Hours
        {
            get
            {
                var res = StartDateStr + (String.IsNullOrWhiteSpace(StartTimeStr) ? String.Empty : (" " + StartTimeStr.Trim()))
                                       + (String.IsNullOrWhiteSpace(EndDateStr) ? 
                                           String.Empty : (" - " + EndDateStr.Trim()) + (String.IsNullOrWhiteSpace(EndTimeStr) ? String.Empty : (" " + EndTimeStr.Trim())));
                return res;
            }
        }

        public string[] HoursArr
        {
            get
            {
                var sd = StartDateStr + (String.IsNullOrWhiteSpace(StartTimeStr) ? String.Empty : (" " + StartTimeStr));
                var ed = String.IsNullOrWhiteSpace(EndDateStr) ? String.Empty : (EndDateStr) + (String.IsNullOrWhiteSpace(EndTimeStr) ? String.Empty : (" " + EndTimeStr));
                return new string[]{sd,ed};
            }
        }

        public int RecordStatus { get; set; }
        public string Name { get; set; }
    }

    public static class CalendarsExtentions
    {
        public static CalendarDto ToDto(this calendars model)
        {
            if (model == null)
            {
                return null;
            }
            CalendarDto dto = new CalendarDto();

            dto.Id = model.id;
            dto.Name = model.name;
            dto.Code = model.code;
            dto.OrganizationId = model.organization_Id;
            dto.RecordStatus = model.recordStatus;
            dto.UpdatedBy = model.updated_by;
            dto.UpdatedAt = model.updated_at;
            var ids = model.work_schedule_profiles_calendars.Select(s => s.profile_id);
            if (ids != null && ids.Any())
                dto.ScheduleIds = String.Join(",", ids);

            return dto;
        }

        public static CalendarRecordDto ToDto(this calendars_rec rec)
        {
            if (rec == null)
            {
                return null;
            }
            CalendarRecordDto dto = new CalendarRecordDto();

            dto.Id = rec.id;
            dto.CalendarId = rec.calendar_id;
            dto.Name = rec.name;
            dto.StartDate = rec.start_date;
            dto.EndDate = rec.end_date;
            dto.RecordStatus = rec.recordStatus??0;

            return dto;
        }
    }
}
