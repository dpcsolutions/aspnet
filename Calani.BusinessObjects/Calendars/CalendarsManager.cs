﻿using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Resources;
using Calani.BusinessObjects.MobileService;

namespace Calani.BusinessObjects.Calendars
{
    public class CalendarsManager : Generic.SmartCollectionManager<Model.calendars, long>
    {

        public CalendarsManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<Model.calendars> GetDbSet(Model.CalaniEntities db)
        {
            return db.calendars;
        }

        internal override SmartQuery<Model.calendars> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = from r in q.Context.calendars where r.id == id select r;
            return q;
        }

        internal override long GetRecordOrganization(Model.calendars record)
        {
            return record.organization_Id;
        }

        /// <summary>
        /// GetBaseQuery
        /// </summary>
        /// <returns>query of calendars</returns>
        internal override SmartQuery<Model.calendars> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = from r in q.Context.calendars select r;
            if (OrganizationID > -1)
            {
                q.Query = from r in q.Query where r.organization_Id == OrganizationID select r;
            }

            if (!IgnoreRecordStatus)
            {
                q.Query = from r in q.Query where (r.recordStatus == null || r.recordStatus == 0) select r;
            }

            return q;
        }


        internal override void BeforeAction(SmartAction<calendars> action)
        {
            if (action.Type == SmartActionType.Update && action.Record != null && action.Record.updated_at == null)
            {
                action.Record.updated_at = DateTime.UtcNow;
            }
            base.BeforeAction(action);
        }


        public List<CalendarDto> GetCalendarsDtos()
        {
            var q = GetBaseQuery();

            var data = q.Query.ToList();

            return (from r in data select r.ToDto()).ToList();
        }

        public long DuplicateCalendar(long calendarId)
        {
            var q = GetById(calendarId);
            var c = q.Query.FirstOrDefault();
            if (c != null)
            {
                var length = (c.name + "(Duplicated)").Length;
                var name = (c.name + "(Duplicated)").Substring(0, Math.Min(length, 45));//name can't be longer than 45 symbols;
                var newCalendar = new calendars()
                {
                    name = name,
                    code = c.code,
                    organization_Id = c.organization_Id,
                };

                var res = Add(newCalendar);

                if (res.Success)
                {
                    var mngrRec = new CalendarsRecManager(c.organization_Id);
                    var recs = mngrRec.GetCalendarRecordsDto(calendarId);
                    foreach (var rec in recs)
                    {
                        var r = new calendars_rec()
                        {
                            calendar_id = res.Record.id,
                            name = rec.Name,
                            start_date = rec.StartDate,
                            end_date = rec.EndDate,
                            recordStatus = rec.RecordStatus,
                        };
                        mngrRec.Add(r);
                    }

                    return (long)res.Record.id;
                }
            }
            return (long)0;
        }
    }

    public class CalendarsRecManager : Generic.SmartCollectionManager<Model.calendars_rec, long>
    {

        public CalendarsRecManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<Model.calendars_rec> GetDbSet(Model.CalaniEntities db)
        {
            return db.calendars_rec;
        }

        internal override SmartQuery<Model.calendars_rec> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = from r in q.Context.calendars_rec where r.id == id select r;
            return q;
        }


        /// <summary>
        /// GetBaseQuery
        /// </summary>
        /// <returns>query of calendars</returns>
        internal override SmartQuery<Model.calendars_rec> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = from r in q.Context.calendars_rec select r;
            q.Query = from r in q.Query where (r.recordStatus == null || r.recordStatus == 0) select r;

            return q;
        }


        internal override void BeforeAction(SmartAction<calendars_rec> action)
        {
            if (action.Type == SmartActionType.Update && action.Record != null && action.Record.updated_at == null)
            {
                action.Record.updated_at = DateTime.UtcNow;
            }
            base.BeforeAction(action);
        }


      
        public List<CalendarRecordDto> GetCalendarRecordsDto(long id)
        {
            var q = GetBaseQuery();
            q.Query = from r in q.Query where r.calendar_id == id select r;

            var data = q.Query.ToList();

            return (from r in data select r.ToDto()).ToList();
        }

        public List<CalendarRecordDto> GetCalendarRecordsDto(DateTime? dtFrom = null, DateTime? dtTo = null)
        {
            var q = GetBaseQuery();

            q.Query.Include(rec => rec.calendars);

            q.Query = from r in q.Query where r.calendars.organization_Id == this.OrganizationID
                      && r.calendars.recordStatus != -1
                      select r;

            if (dtFrom != null)
            {
                q.Query = (from r in q.Query where r.start_date >= dtFrom || r.end_date >= dtFrom select r);
            }

            if (dtTo != null)
            {
                q.Query = (from r in q.Query where r.start_date <= dtTo || r.end_date <= dtTo select r);
            }

            var data = q.Query.ToList();

            return (from r in data select r.ToDto()).ToList();
        }
    }
}
