﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Braintree;
/// <summary>
/// Summary description for PaymentProcessor
/// </summary>
public class PaymentProcessor
{

    BraintreeGateway _bgt;
    const string SERVER_NAME = "serverName";

    public PaymentProcessor()
    {
        Braintree.Environment env = Braintree.Environment.SANDBOX;
        string envname = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorEnvironment"].ToUpper().Trim();

        if (envname == "DEVELOPMENT") env = Braintree.Environment.DEVELOPMENT;
        else if (envname == "QA") env = Braintree.Environment.QA;
        else if (envname == "PRODUCTION") env = Braintree.Environment.PRODUCTION;

        _bgt = new BraintreeGateway()
        {
            Environment = env,
            MerchantId = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorMerchantId"],
            PublicKey = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorPublicKey"],
            PrivateKey = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorPrivateKey"]
        };


    }

    private BraintreeGateway gateway()
    {
        return _bgt;
    }

    

    public void handleHook(string payload, string signature)
    {
        WebhookNotification webhookNotification = gateway().WebhookNotification.Parse(
           signature, payload
       );

        // Example values for webhook notification properties
        System.Console.WriteLine(webhookNotification.Kind); // "subscription_went_past_due"
        System.Console.WriteLine(webhookNotification.Timestamp.Value); // Sun Jan 1 00:00:00 UTC 2012

    }

    public bool isSubscriptionValid(string subscriptionId)
    {
        try
        {
            var subscription1 = gateway().Subscription.Find(subscriptionId);
            return subscription1.Status == SubscriptionStatus.ACTIVE;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public SubscriptionStatus getSubscriptionStatus(string subscriptionId)
    {
        try
        {
            var subscription1 = gateway().Subscription.Find(subscriptionId);
            return subscription1.Status;
        }
        catch (Exception)
        {
            return SubscriptionStatus.UNRECOGNIZED;
        }
    }

    public bool cancelSubscription(string subscriptionId)
    {
        try
        {
           var ret = gateway().Subscription.Cancel(subscriptionId);
            return ret.IsSuccess();
        }
        catch (Exception)
        {
            return false;
        }
    }

    public string getClientToken(string organisationId = null)
    {
        ClientTokenRequest req = null;

        if (organisationId != null)
        {
            req = new ClientTokenRequest() { CustomerId =  PaymentProcessor.getClientId(Int32.Parse(organisationId))};
        }

        try
        {
            return gateway().ClientToken.Generate(req);
        }
        catch (Exception)
        {
            return gateway().ClientToken.Generate();
        }
    }

    public string createPaymentMethod(long organisationId, string paymentMethodNonce)
    {
        Result<PaymentMethod> result2 = gateway().PaymentMethod.Create(
                  new PaymentMethodRequest
                  {
                      CustomerId = getClientId(organisationId),
                      PaymentMethodNonce = paymentMethodNonce
                  }
              );

        if (result2.IsSuccess())
        {
            return result2.Target.Token;
        }
        else
        {
            Console.WriteLine("Could not create payment method for client " + organisationId);
            return null;
        }
    }

    

    public bool updatePaymentMethod(string subscriptionId, string paymentMethodToken)
    {
        try
        {
            var subs = gateway().Subscription.Find(subscriptionId);
            var request = new SubscriptionRequest
            {
                PaymentMethodToken = paymentMethodToken
            };

            Result<Subscription> result4 = gateway().Subscription.Update(subscriptionId, request);

            if (result4.IsSuccess())
            {
                Console.WriteLine("subscription successfully updated with payment method");
                return true;
            }
            else
            {
                Console.WriteLine("subscription unsuccessfully updated with payment method: " + result4.Message);
                return false;
            }
        }
        catch (Exception e)
        {
            return false;
        }
    }
    
    public Subscription getSubscription(string id)
    {
        try
        {
            return gateway().Subscription.Find(id);
        }
        catch (Exception)
        {
            return null;
        }
    }

    public Customer getCustomer(string id)
    {
        try
        {
            return gateway().Customer.Find(id);
        }
        catch (Exception)
        {
            return null;
        }
    }
    
    public PaymentMethod getPaymentMethodForSubscription(string subscriptionId)
    {
        var subscription = getSubscription(subscriptionId);

        if (subscription != null && subscription.PaymentMethodToken != null)
        {
            return gateway().PaymentMethod.Find(subscription.PaymentMethodToken);
        }
        else
            return null;
    }

    private PaymentMethod[] getPaymentMethods(long organisationId)
    {
        var customer = getCustomer(getClientId(organisationId));

        if (customer != null && customer.PaymentMethods != null)
        {
            return customer.PaymentMethods;
        }
        else
            return null;
    }

    private PaymentMethod getDefaultPaymentMethod(long organisationId)
    {
        var customer = getCustomer(getClientId(organisationId));
        PaymentMethod ret = null;

        if (customer != null && customer.PaymentMethods != null)
        {
            foreach (var p in customer.PaymentMethods)
            {
                if (p.IsDefault != null && (bool)p.IsDefault)
                {
                    ret = p;
                    break;
                }
            }
        }

        return ret;
    }

    private Plan getPlan(string planId)
    {
        try
        {
            return (from p in gateway().Plan.All()
                    where p.Id == planId
                    select p).FirstOrDefault();

        }
        catch (Exception)
        {
            return null;
        }
    }

    private Result<Subscription> createSubscription(string planId, string paymentMethodToken, int? yearlyLicences, int? monthlyLicences, string yearlyAddOn, string monthlyAddOn)
    {
        var request = new SubscriptionRequest
        {
            //       Descriptor = descriptor,
            PaymentMethodToken = paymentMethodToken,
            PlanId = planId,
            Options = new SubscriptionOptionsRequest { ProrateCharges = true },
           
        };

   

        if (yearlyLicences != null && yearlyLicences > 0 ||
            monthlyLicences != null && monthlyLicences > 0)
            request.AddOns = new AddOnsRequest
            {
                Update = new UpdateAddOnRequest[]
                           {
                                new UpdateAddOnRequest
                                {
                                    ExistingId = (yearlyLicences != null && yearlyLicences > 0 ? yearlyAddOn : monthlyAddOn),
                                    Quantity = (yearlyLicences != null && yearlyLicences > 0 ? yearlyLicences : monthlyLicences)
                                }
                           }
            };

        return gateway().Subscription.Create(request);
    }

    private Result<Subscription> updateSubscription(string subscriptionId, string planId, string paymentMethodToken, int? yearlyLicences, int? monthlyLicences, string yearlyAddOn, string monthlyAddOn)
    {
        var request = new SubscriptionRequest
        {
            //       Descriptor = descriptor,
            PlanId = planId,
            Options = new SubscriptionOptionsRequest { ProrateCharges = true },
            AddOns = new AddOnsRequest
            {
                Update = new UpdateAddOnRequest[]
                           {
                                new UpdateAddOnRequest
                                {
                                    ExistingId = (yearlyLicences != null && yearlyLicences > 0 ? yearlyAddOn : monthlyAddOn),
                                    Quantity = (yearlyLicences != null && yearlyLicences > 0 ? yearlyLicences : monthlyLicences)
                                }
                           }
            }
        };

        if (paymentMethodToken != null)
            request.PaymentMethodToken = paymentMethodToken;

        

        return gateway().Subscription.Update(subscriptionId, request);
    }

    public string getPaymentMethodToken(string customerId, string paymentMethodNonce)
    {
        string ret = null;

        try
        {
            Result<PaymentMethod> result = gateway().PaymentMethod.Create(
                           new PaymentMethodRequest
                           {
                               CustomerId = customerId,
                               PaymentMethodNonce = paymentMethodNonce
                           }
                       );

            if (result.IsSuccess())
                ret = result.Target.Token;
        }
        catch (Exception)
        {
        }
        return ret;       
    }

    private Result<Customer> createCustomer(string firstName, string lastName, string email, string company, string phone, string customerId)
    {
        return gateway().Customer.Create(
               new CustomerRequest
               {
                   FirstName = firstName,
                   LastName = lastName,
                   Email = email,
                   Company = company,
                   Phone = phone,
                   Id = customerId
               });
    }
    

    public string updateSubscriptionForClient(long organisationId, string subscriptionId, string paymentMethodNonce, string planId, int? yearlyLicences, int? monthlyLicences, string yearlyAddOn, string monthlyAddOn)
    {
        //find subscription
        string ret = null;
        var subscription = getSubscription(subscriptionId);
        string customerId = getClientId(organisationId);

        if (subscription != null)
        {
            //is the subscription plan the same otherwise cancel the subscription
            if(subscription.PlanId == planId)
            {
                //yes, update the subscription
                var result = updateSubscription(subscriptionId, planId, getPaymentMethodToken(customerId, paymentMethodNonce), yearlyLicences, monthlyLicences, yearlyAddOn, monthlyAddOn);

                if (result.IsSuccess())
                    ret = result.Target.Id;
                else
                    throw new Exception("Could not create subscription, error:" + result.Message);

            }
            else
            {
                if(cancelSubscription(subscriptionId))
                {
                    var result = createSubscription(planId, getPaymentMethodToken(customerId, paymentMethodNonce), yearlyLicences, monthlyLicences, yearlyAddOn, monthlyAddOn);

                    if (result.IsSuccess())
                        ret = result.Target.Id;
                    else
                        throw new Exception("Could not create subscription, error:" + result.Message);

                }
            }
        }
        else
            throw new Exception("Unknown subscription");

        return ret;
    }

    static public string getClientId(long organisationId)
    {
        return System.Configuration.ConfigurationManager.AppSettings[SERVER_NAME] + "_" + organisationId.ToString();
    }

    public string createSubscriptionForClient(long organisationId, string firstName, string lastName, string email, string company, string phone, string paymentMethodNonce, string planId, int? yearlyLicences, int? monthlyLicences, string yearlyAddOn, string monthlyAddOn)
    {
        string ret = null;

        string customerId = getClientId(organisationId);

        Customer customer = getCustomer(customerId);

        if (customer == null)
        {
            var result = createCustomer(firstName, lastName, email, company, phone, customerId);

            if (result.IsSuccess())
                customer = result.Target;
            else
                throw new Exception("Could not create custommer, error:" + result.Message);
        }

        if (customer != null)
        {
            var result = createSubscription(planId, getPaymentMethodToken(customer.Id, paymentMethodNonce), yearlyLicences, monthlyLicences, yearlyAddOn, monthlyAddOn);

            if (result.IsSuccess())
                ret = result.Target.Id;
            else
                throw new Exception("Could not create subscription, error:" + result.Message);
        }

        return ret;
    }


    public List<PlanDescription> ListPlans()
    {
        // les id des plans doivent commencer par "basic..."
        // les options doivent commencer par "1licence"...

        List<PlanDescription> ret = new List<PlanDescription>();

        var plans = gateway().Plan.All();

        foreach (var plan in plans)
        {
            PlanDescription pd = new PlanDescription();
            if (true /*plan.Id.StartsWith("basic") */)
            {
                pd.Name = plan.Id;
                if (plan.Price != null) pd.BasePrice = Convert.ToDouble(plan.Price.Value);
                if (plan.BillingFrequency != null) pd.Frequency = plan.BillingFrequency.Value;

                foreach (var addon in plan.AddOns)
                {
                    if (/*addon.Id.ToLower().StartsWith("1licence") */
                        addon.Id == System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorAddinId_MonthlyExtraUser"]
                        || addon.Id == System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorAddinId_YearlyExtraUser"]
                        || addon.Id.Contains("1licence")
                        )
                    {
                        if (addon.Amount != null)
                        {
                            pd.UserLicensePrice = Convert.ToDouble(addon.Amount.Value);
                            pd.UserLicenseId = addon.Id;
                        }
                    }
                }
            }

            ret.Add(pd);
        }

        return ret;
    }

    /*
    private void processNonce(string nonce)
    {
        //git the nonce
        Console.WriteLine("got nonce:" + nonce);

        var request = new TransactionRequest
        {
            Amount = 1.00M,
            PaymentMethodNonce = nonce,
            Options = new TransactionOptionsRequest
            {
                SubmitForSettlement = true,
                StoreInVaultOnSuccess = true
            }
        };

        Result<Transaction> result = gateway.Transaction.Sale(request);

        Console.WriteLine("result:" + result.Message);

        if (result.IsSuccess())
        {
            Transaction transaction = result.Target;
            TransactionStatus status = transaction.Status;

            Console.WriteLine("Transaction: " + transaction.Status);
            if(result.Subscription != null)
            {
                Console.WriteLine("Transaction: " + result.Subscription.Id);
            }

            if (status == TransactionStatus.AUTHORIZED)
                Console.WriteLine("authorized");
        }
        else
        {

            Console.WriteLine("payment was rejected:" + result.Message);

            Transaction transaction = result.Transaction;

            if (result.Errors.DeepCount > 0)
            {
                //check for validation errors
                foreach (var e in result.Errors.All())
                {
                    Console.WriteLine("Error:" + e.Message);
                }
            } else if (transaction.Status == TransactionStatus.PROCESSOR_DECLINED)
            {
                Console.WriteLine(transaction.ProcessorResponseCode);
                // e.g. "2001"
                Console.WriteLine(transaction.ProcessorResponseText);
                // e.g. "Insufficient Funds"
            } else if (transaction.Status == TransactionStatus.SETTLEMENT_DECLINED)
            {
                Console.WriteLine(transaction.ProcessorSettlementResponseCode);
                // e.g. "4001"
                Console.WriteLine(transaction.ProcessorSettlementResponseText);
                // e.g. "Settlement Declined"
            } else if (transaction.Status == TransactionStatus.GATEWAY_REJECTED)
            {
                Console.WriteLine("Gateway rejected.");
                Console.WriteLine(transaction.GatewayRejectionReason);
                // e.g. "avs"
            }
            
            if(transaction != null && transaction.RiskData != null)
            {
                Console.WriteLine(transaction.RiskData.id);
                Console.WriteLine(transaction.RiskData.decision);
                Console.WriteLine(transaction.RiskData.deviceDataCaptured);
            }
        }
    }*/


    public class PlanDescription
    {
        public string Name { get; set; }
        public double BasePrice { get; set; }
        public double UserLicensePrice { get; set; }
        public string UserLicenseId { get; set; }
        public int Frequency { get; set; } 
    }

}