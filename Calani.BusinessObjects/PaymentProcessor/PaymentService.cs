﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;
using System.Linq;

namespace Calani.BusinessObjects.PaymentService
{
    public class paymentOptionsExtended 
    {
        public paymentOptionsExtended()
        {
        }

        public string nonce { get; set; }
        public string plan { get; set;}
        public string subscriptionId { get; set; }
        public int? yearlyLicences { get; set; }
        public int? monthlyLicences { get; set; }
    }

    public class transaction
    {
        public transaction(string creditCard, decimal amount, DateTime createdAt,
            string currency, string planId, string status)
        {
            Amount = amount;
            CreatedAt = createdAt;
            CurrencyIsoCode = currency;
            PlanId = planId;
            Status = status;
            CreditCard = creditCard;
        }


        public string CreditCard { get; set; }
        public decimal Amount { get; set; }
        public DateTime CreatedAt { get; set; }
        public string CurrencyIsoCode { get; set; }
        public string PlanId { get; set; }
        public string Status { get; set; }
    }

    public class addin
    {
        public addin()
        {

        }
        public decimal Amount { get; set; }
        public decimal Total { get; set; }
        public decimal Quantity { get; set; }
        public string Name { get; set; }
        public string CurrencyIsoCode { get; set; }
    }

    public class subscription
    {
        public subscription(string ccyIsoCode, string id, string lastFour, string cardType, bool isExpired,string planId,
            decimal price, DateTime createdAt, DateTime updatedAt, string status, int daysPastDue,
            int failureCount, List<addin> addins, List<transaction> transactions, DateTime nextBillingDate,
            decimal? nextBillingAmount, decimal? balance)
        {
            CurrencyIsoCode = ccyIsoCode;
            Id = id;
            LastFour = lastFour;
            CardType = cardType;
            IsExpired = isExpired;
            PlanId = planId;
            Price = price;
            CreatedAt = createdAt;
            UpdatedAt = updatedAt;
            Status = status;
            DaysPastDue = daysPastDue;
            FailureCount = failureCount;
            Addins = addins;
            Transactions = transactions;
            NextBilled = nextBillingDate;
            yearlyLicences = 0;
            monthlyLicences = 0;
            if(balance != null) Balance = balance.Value;
            if(nextBillingAmount != null) NextBillingAmount = nextBillingAmount.Value;
            double tmpAddinsTotal = 0;

            foreach (var a in addins)
            {
                if (a.Name.ToLower().Contains("yearly"))
                    yearlyLicences += Convert.ToInt32(a.Quantity);
                else if (a.Name.ToLower().Contains("monthly"))
                    monthlyLicences += Convert.ToInt32(a.Quantity);

                tmpAddinsTotal += (double)(a.Amount * a.Quantity);
            }

            AddinsTotal = Convert.ToDecimal( Math.Round(tmpAddinsTotal, 2, MidpointRounding.AwayFromZero));
        }

        public List<transaction> Transactions { get; set; }
        public int yearlyLicences { get; set; }
        public int monthlyLicences { get; set; }

        public List<addin> Addins { get; set; }
        public DateTime NextBilled { get; set; }
        public decimal AddinsTotal { get; set; }
        public string LastFour { get; set; }
        public string Id { get; set; }
        public string CardType { get; set; }
        public string PlanId { get; set; }
        public bool IsExpired { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public int DaysPastDue { get; set; }
        public int FailureCount { get; set; }
        public string Status { get; set; }
        public string CurrencyIsoCode { get; set; }

        public decimal Balance { get; set; }
        public decimal NextBillingAmount { get; set; }
    }

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PaymentService : IPaymentService
    {
        public string getToken()
        {
            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            PaymentProcessor pp = new PaymentProcessor();
            return pp.getClientToken(organizationId.ToString());
        }

        public bool cancelSubscription(string subsId = null)
        {
            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);


            bool ret = false;

            try
            {
                string subscriptionId = subsId; //normally from DB 
                if (String.IsNullOrWhiteSpace(subscriptionId))
                {
                    Calani.BusinessObjects.Model.CalaniEntities db = new Calani.BusinessObjects.Model.CalaniEntities();
                    var org = (from r in db.organizations where r.id == organizationId select r).FirstOrDefault();
                    subscriptionId = org.subscriptionId;
                }

                if (subscriptionId != null)
                {
                    PaymentProcessor pp = new PaymentProcessor();
                    ret = pp.cancelSubscription(subscriptionId);
                }
            }
            catch (Exception)
            {
            }


            // dissociate from account
            try
            {


                Calani.BusinessObjects.Model.CalaniEntities db = new Calani.BusinessObjects.Model.CalaniEntities();
                var org = (from r in db.organizations where r.id == organizationId select r).FirstOrDefault();
                string prevsubid = org.subscriptionId;
                org.subscriptionId = null;
                db.SaveChanges();
            }
            catch { }
            // -->

            return ret;
        }

        public string manageSubscription(paymentOptionsExtended paymentOptions)
        {
            string ret = null;

            string monthlyPlanId = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorPlanId_Monthly"];
            string yearlyPlanId = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorPlanId_Yearly"];
            string monthlyExtraUserId = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorAddinId_MonthlyExtraUser"];
            string yearlyExtraUser = System.Configuration.ConfigurationManager.AppSettings["PaymentProcessorAddinId_YearlyExtraUser"];


            try
            {
                long userId = Convert.ToInt64(HttpContext.Current.Session["userId"]);
                userId = 6;

                long organisationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
                string subscriptionId = paymentOptions.subscriptionId; //normally from DB 
                

                if(subscriptionId == null)
                {
                    //this is the first time there is no uibscription in the db for this organisatoin
                    BusinessObjects.Contacts.EmployeesManager mgr = new Contacts.EmployeesManager(organisationId);
                    Model.contacts c = mgr.Get(userId);

                    PaymentProcessor pp = new PaymentProcessor();
                    ret = pp.createSubscriptionForClient(organisationId, c.firstName, c.lastName, c.primaryEmail, c.organizations.name, c.primaryPhone, paymentOptions.nonce, paymentOptions.plan, paymentOptions.yearlyLicences, paymentOptions.monthlyLicences,
                        yearlyExtraUser, monthlyExtraUserId);
                }
                else
                {
                    PaymentProcessor pp = new PaymentProcessor();
                    ret = pp.updateSubscriptionForClient(organisationId, subscriptionId, paymentOptions.nonce, paymentOptions.plan, paymentOptions.yearlyLicences, paymentOptions.monthlyLicences,
                        yearlyExtraUser, monthlyExtraUserId);
                }
            }
            catch (Exception)
            {
            }

            return ret;
        }
    }

    [ServiceContract(SessionMode = SessionMode.Allowed)]
    public interface IPaymentService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "manageSubscription", ResponseFormat = WebMessageFormat.Json)]
        string manageSubscription(paymentOptionsExtended paymentOptions);

        [OperationContract]
        [WebGet(UriTemplate = "getToken", ResponseFormat = WebMessageFormat.Json)]
        string getToken();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "cancelSubscription", ResponseFormat = WebMessageFormat.Json)]
        bool cancelSubscription(string subsId = null);
    }
}
