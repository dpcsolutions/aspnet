﻿using System;

namespace Calani.BusinessObjects.Hangfire
{
    public static class TimeManager
    {
        public static DateTime? GetNextDate(RecurringInvoiceTime data)
        {
            var currentDate = DateTime.Now;
            switch (data.frequencyType)
            {
                case 1:
                    currentDate = GetNextSpecificDay(currentDate, data.frequencyCount, data.isLastDay ? 7 : data.specificDayCount);
                    break;

                case 2:
                    currentDate = currentDate.AddMonths(data.frequencyCount);
                    if (data.isLastDay)
                    {
                        currentDate = new DateTime(currentDate.Year, currentDate.Month,
                            DateTime.DaysInMonth(currentDate.Year, currentDate.Month));
                    }
                    else if (data.isSpecificDay)
                    {
                        int daysInMonth = DateTime.DaysInMonth(currentDate.Year, currentDate.Month);
                        currentDate = new DateTime(currentDate.Year, currentDate.Month,
                            Math.Min(data.specificDayCount, daysInMonth));
                    }

                    break;

                case 3:
                    currentDate = currentDate.AddYears(data.frequencyCount);
                    if (data.isSpecificDay || data.isLastDay)
                    {
                        if (data.specificMonthCount > 0)
                        {
                            var daysInMonth = DateTime.DaysInMonth(currentDate.Year, data.specificMonthCount);
                            var day = data.isLastDay ? daysInMonth : Math.Min(data.specificDayCount, daysInMonth);
                            currentDate = new DateTime(currentDate.Year, data.specificMonthCount, day);
                        }
                    }
                    break;
                default:
                    throw new ArgumentException("Invalid frequency type.");
            }

            return currentDate;
        }


        private static DateTime GetNextSpecificDay(DateTime startDate, int weeksInterval, int targetDayOfWeek)
        {
            var currentDayOfWeek = (int) startDate.DayOfWeek;
            currentDayOfWeek = currentDayOfWeek == 0 ? 7 : currentDayOfWeek; 
            
            var daysUntilTarget = (targetDayOfWeek - currentDayOfWeek + 7) % 7;
            var nextTargetDate = startDate.AddDays(daysUntilTarget);

            nextTargetDate = nextTargetDate <= startDate
                ? nextTargetDate.AddDays(7 * weeksInterval)
                : nextTargetDate.AddDays(7 * (weeksInterval - 1));

            return nextTargetDate;
        }
    }
}