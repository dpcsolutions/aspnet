﻿using System;
using System.Linq.Expressions;
using Hangfire;

namespace Calani.BusinessObjects.Hangfire
{
    public static class HangfireJobScheduler
    {
        /// <summary>
        /// Creates a Recurring Job in Hangfire
        /// </summary>
        /// <param name="jobId">Unique identifier of the task</param>
        /// <param name="methodCall">Method to execute</param>
        /// <param name="cronExpression">CRON expression for the interval</param>
        /// <param name="timeZone">Time zone (default is local)</param>
        public static void CreateOrUpdateRecurringJob(
            string jobId,
            Expression<Action> methodCall,
            string cronExpression,
            TimeZoneInfo timeZone = null)
        {
            var options = new RecurringJobOptions {TimeZone = timeZone ?? TimeZoneInfo.Local};
            RecurringJob.AddOrUpdate(jobId, methodCall, cronExpression, options);
        }
        
        /// <summary>
        /// Creates a one-time job in Hangfire that executes after a specified delay
        /// </summary>
        /// <param name="methodCall">Method to execute</param>
        /// <param name="delay">Delay for the job execution</param>
        public static string CreateOneTimeJob(
            Expression<Action> methodCall,
            TimeSpan delay)
        {
            return BackgroundJob.Schedule(methodCall, delay);
        }
        
        
        /// <summary>
        /// Deletes a Job in Hangfire by ID.
        /// </summary>
        /// <param name="jobId">job ID Hangfire</param>
        public static void DeleteIfExists(string jobId)
        {
            if (!string.IsNullOrEmpty(jobId))
            {
                RecurringJob.RemoveIfExists(jobId);
            }
        }
    }
}