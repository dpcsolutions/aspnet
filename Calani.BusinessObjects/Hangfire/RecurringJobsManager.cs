﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Reflection;
using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.DocGenerator;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Projects;
using Calani.BusinessObjects.Projects.Enums;
using Calani.BusinessObjects.Settings;
using Hangfire;
using log4net;

namespace Calani.BusinessObjects.Hangfire
{
    public static class RecurringJobsManager
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod()?.DeclaringType);
        
        [AutomaticRetry(Attempts = 3)]
        public static bool RecurringInfosHasChanges(recurringinfos oldObj, recurringinfos newObj)
        {
            if (oldObj.isActive != newObj.isActive) return true;
            if (oldObj.frequencyType != newObj.frequencyType) return true;
            if (oldObj.frequencyCount != newObj.frequencyCount) return true;
            if (oldObj.specificDayCount != newObj.specificDayCount) return true;
            if (oldObj.specificMonthCount != newObj.specificMonthCount) return true;
            if (oldObj.isLastDay != newObj.isLastDay) return true;
            if (oldObj.nextDueDate != newObj.nextDueDate) return true;
            if (oldObj.isUntilNotice != newObj.isUntilNotice) return true;
            if (oldObj.isSpecificMonth != newObj.isSpecificMonth) return true;
            if (oldObj.lastDueDate != newObj.lastDueDate) return true;
            if (oldObj.invoiceSendingType != newObj.invoiceSendingType) return true;
            if (oldObj.nextExecutionDate != newObj.nextExecutionDate) return true;
            return oldObj.daysInAdvance != newObj.daysInAdvance;
        }

        public static void ProceedRecurringJobs()
        {
            var db = new CalaniEntities();
            var activeRecurrings = db.recurringinfos.Where(x => x.isActive).ToList();

            if (activeRecurrings.Count == 0) return;
            var now = DateTime.Now;
            
            var recurringsToSend = activeRecurrings
                .Where(x => x.invoiceSendingType == (int)RecurringSendingType.Automatic 
                    && x.nextExecutionDate == now.Date)
                .ToList();

            var recurringsToDraft = activeRecurrings
                .Where(x => x.invoiceSendingType != (int)RecurringSendingType.Automatic
                    && now.Date == x.nextExecutionDate.AddDays(-x.daysInAdvance)) 
                .ToList();

            var prjMgr = new ProjectsManager(0);
            if (recurringsToDraft.Any()) CreateDraftsForInvoices(db, prjMgr, recurringsToDraft);
            if (recurringsToSend.Any()) CloneAndSendInvoices(db, prjMgr, recurringsToSend);
            
            db.SaveChanges();
        }

        private static void CreateDraftsForInvoices(CalaniEntities db, ProjectsManager prjMgr, List<recurringinfos> recurringsToDraft)
        {
            foreach (var info in recurringsToDraft)
            {
                var invoice = db.jobs.FirstOrDefault(x => x.id == info.invoiceId);
                if (invoice?.organizationId == null) {
                    db.recurringinfos.Remove(info);
                    Log.Warn($"Missing data on recurring invoice job: \"invoice?.organizationId\" | infoId = {info.id}, jobId = {info.invoiceId}");
                    continue;
                }

                var org = db.organizations.FirstOrDefault(x => x.id == invoice.organizationId.Value);
                if (org?.owner == null) {
                    Log.Warn($"Missing data on recurring invoice job: \"org?.owner\" | infoId = {info.id}, jobId = {info.invoiceId}");
                    continue;
                }
                
                prjMgr.OrganizationID = invoice.organizationId.Value;
                prjMgr.CloneProject(info.invoiceId, 
                    info.invoiceSendingType == (int)RecurringSendingType.DraftInvoiceWithNotificationMail ? "invoice" : "quote",
                    null, null, new CloneItems(false), 
                    info.invoiceSendingType == (int)RecurringSendingType.DraftQuoteWithNotificationMail);
                
                info.nextDueDate = DateTime.Now.AddDays(30); //??? 
                
                var nextDate = TimeManager.GetNextDate(new RecurringInvoiceTime(info));
                if (nextDate == null)
                {
                    info.nextExecutionDate = DateTime.MinValue;
                    Log.Warn($"Missing data on recurring invoice job: \"nextDate\" | infoId = {info.id}, jobId = {info.invoiceId}");
                } 
                else info.nextExecutionDate = nextDate.Value;

                info.generatedNumber += 1;
                db.recurringinfos.AddOrUpdate(info);
            }
        }
        

        private static void CloneAndSendInvoices(CalaniEntities db, ProjectsManager prjMgr, List<recurringinfos> recurringsToSend)
        {
            
            foreach (var info in recurringsToSend)
            {
                var invoice = db.jobs.FirstOrDefault(x => x.id == info.invoiceId);
                if (invoice?.organizationId == null) {
                    db.recurringinfos.Remove(info);
                    Log.Warn($"Missing data on recurring invoice job: \"invoice?.organizationId\" | infoId = {info.id}, jobId = {info.invoiceId}");
                    continue;
                }

                var org = db.organizations.FirstOrDefault(x => x.id == invoice.organizationId.Value);
                if (org?.owner == null) {
                    Log.Warn($"Missing data on recurring invoice job: \"org?.owner\" | infoId = {info.id}, jobId = {info.invoiceId}");
                    continue;
                }
                
                var owner = db.contacts.FirstOrDefault(x => x.id == org.owner.Value);
                prjMgr.OrganizationID = invoice.organizationId.Value;
                var clone = prjMgr.CloneProject(info.invoiceId, "invoice", null, 
                    null, new CloneItems(false), false, DateTime.Now);

                var culture = System.Threading.Thread.CurrentThread.CurrentUICulture;
                // var tplMgr = new DocumentTemplateManager(0, System.Threading.Thread.CurrentThread.CurrentUICulture, true);
                // tplMgr.UserId = org.owner.Value;
                //var template = tplMgr.Get("SendInvoice");
                var template = new DocumentTemplateInfo
                {
                    DefaultContent = SharedResource.Resource.GetString("Email_Default_SendInvoice", culture),
                    DefaultTitle = SharedResource.Resource.GetString("Email_Title_SendInvoice", culture),
                    Title = SharedResource.Resource.GetString("Email_Description_SendInvoice", culture),
                    VariableDescriptions = SharedResource.Resource.GetString("Email_Vars_SendInvoice", culture)
                };
                
                var vars = GetVars(org, owner, clone.Value, info.nextDueDate);
                var msg = new SendEmail
                {
                    subject = template.DefaultTitle,
                    body = template.DefaultContent?.Replace("  ", " "),
                    toContact = invoice.clientId,
                    toEmail = $"{invoice.clientContact}; {invoice.send_copy_email_to}"
                };
                
                foreach (var item in vars)
                {
                    msg.subject = msg.subject?.Replace("$" + item.name, item.value);
                    msg.body = msg.body?.Replace("$" + item.name, item.value);
                }
                EmailHelper.SendMail(msg, org.id, $"{owner.lastName} {owner.firstName}", owner.id);
                
                info.nextDueDate = DateTime.Now.AddDays(30); //??? 
                var nextDate = TimeManager.GetNextDate(new RecurringInvoiceTime(info));
                if (nextDate == null)
                {
                    info.nextExecutionDate = DateTime.MinValue;
                    Log.Warn($"Missing data on recurring invoice job: \"nextDate\" | infoId = {info.id}, jobId = {info.invoiceId}");
                } 
                else info.nextExecutionDate = nextDate.Value;
                
                info.generatedNumber += 1;
                db.recurringinfos.AddOrUpdate(info);
            }
        }
        
        private static List<JsVariableForTemplate> GetVars (organizations org, 
            contacts owner,
            string invoiceNumber,
            DateTime? invoiceDueDate)
        {
            var shortDate = invoiceDueDate == null ? "" : invoiceDueDate.Value.ToShortDateString();
            
            return new List<JsVariableForTemplate>
            {
                new JsVariableForTemplate { name = "mycompany", value = org.name },
                new JsVariableForTemplate { name = "doctype", value = SharedResource.Resource.GetString("Invoice")},
                new JsVariableForTemplate { name = "docnr", value = invoiceNumber },
                new JsVariableForTemplate { name = "expdate", value = shortDate },
                new JsVariableForTemplate { name = "contacttitle", value = owner.title },
                new JsVariableForTemplate { name = "contactfn", value = owner.firstName },
                new JsVariableForTemplate { name = "contactln", value = owner.lastName }
            };
        }
    }
}