﻿using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.Hangfire
{
    public class RecurringInvoiceTime
    {
        public RecurringInvoiceTime()
        {
            
        }

        public RecurringInvoiceTime(recurringinfos info)
        {
            frequencyType = info.frequencyType;
            frequencyCount = info.frequencyCount;
            isLastDay = info.isLastDay;
            isSpecificDay = info.isSpecificDay;
            specificDayCount = info.specificDayCount;
            specificMonthCount = info.specificMonthCount;
        }
        
        public int frequencyType { get; set; } 
        public int frequencyCount { get; set; }
        public bool isLastDay { get; set; } 
        public bool isSpecificDay { get; set; } 
        public int specificDayCount { get; set; } 
        public int specificMonthCount { get; set; }
    }
}