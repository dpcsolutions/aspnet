﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PushServer;
using log4net;
using System.Reflection;

namespace Calani.BusinessObjects.Notifications
{
    public class NotificationManager
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public void sendNotification(string msg, string title, long userId, Dictionary<string, string> data = null, string category = null)
        {
            sendNotifications(msg, title, new List<long>() { userId }, data, category);
        }

        public void sendNotifications(string msg, string title, List<long> userIds, Dictionary<string, string> data = null, string category = null)
        {
            var p = new NotificationSender();

            _log.Debug("getting tokens from database to send notifications");

            Model.CalaniEntities db = new Model.CalaniEntities();
            var usrs = (from r in db.contacts
                       join u in userIds on r.id equals u
                       where r.notificationToken != null
                       select new NotificationSender.notificationToken() { isIOS = true, token = r.notificationToken }).ToList();

            p.sendNotificationToTokens(usrs, title, msg, 1, data == null ? new Dictionary<string, string> { } : data, category);
        }

        public void storeNotification(string notificationToken, long userId)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();
            var usr = (from r in db.contacts
                       where r.id == userId
                       select r).FirstOrDefault();

            if(usr != null)
            {
                usr.notificationToken = notificationToken;
                db.SaveChanges();
            }
        }
    }
}
