﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace Calani.BusinessObjects.Generic
{
    public static class XMLSerializer
    {

        /// <summary>
        /// Serialize any serializable object to an UTF8 XML String
        /// </summary>
        /// <param name="o">any serializable object</param>
        /// <returns>UTF8 XML String</returns>
        public static string Serialize(object o)
        {
            string ret = null;
            using (MemoryStream ms = new MemoryStream())
            {
                XmlSerializer ser = new XmlSerializer(o.GetType());
                XmlTextWriter xtw = new XmlTextWriter(ms, Encoding.UTF8);
                xtw.Formatting = Formatting.Indented;
                xtw.Indentation = 4;
                ser.Serialize(xtw, o);
                byte[] data = ((MemoryStream)xtw.BaseStream).ToArray();
                ret = Encoding.UTF8.GetString(data);
                ret = ret.Trim();
                int start = ret.IndexOf("<?");
                ret = ret.Substring(start);
            }
            return ret;
        }


        public static string ChecksumMD5(object o)
        {
            string xml = "Null";
            if (o != null) xml = o.Serialize();
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(xml));
            var md5 = System.Security.Cryptography.MD5.Create();
            xml = BitConverter.ToString(md5.ComputeHash(ms)).Replace("-", "").ToLower();
            ms.Dispose();
            md5.Dispose();
            return xml;
        }


        /// <summary>
        /// Serialize any serializable object to an UTF8 XML File
        /// </summary>
        /// <param name="o">any serializable object</param>
        /// <param name="xmlfile">UTF8 XML File to write</param>
        public static void Serialize(object o, string xmlfile)
        {
            try
            {
                if (File.Exists(xmlfile)) File.Delete(xmlfile);
            }
            catch { }

            string xml = Serialize(o);
            byte[] data = Encoding.UTF8.GetBytes(xml);
            File.WriteAllBytes(xmlfile, data);
            /*using (FileStream fs = new FileStream(xmlfile, FileMode.CreateNew))
            {
                XmlSerializer ser = new XmlSerializer(o.GetType());
                // add header
                byte[] header = Encoding.UTF8.GetBytes("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n");
                fs.Write(header, 0, header.Length);
                // --
                ser.Serialize(fs, o);
                fs.Close();
                
            }*/
        }





        /// <summary>
        /// Deserialize an object from an UTF8 XML Source
        /// </summary>
        /// <typeparam name="T">Class Type</typeparam>
        /// <param name="dataorfile">xml source (string data or file path)</param>
        /// <param name="source">xml source type</param>
        /// <returns>the deserialized object</returns>
        public static T Deserialize<T>(string dataorfile, SerializationSource source)
        {
            Stream s = null;
            if (source == SerializationSource.String)
            {
                // string
                s = new MemoryStream(Encoding.UTF8.GetBytes(dataorfile));
            }
            else
            {
                // file
                s = new FileStream(dataorfile, FileMode.Open);
            }
            Exception e = null;
            XmlSerializer ser = null;
            object o = null;
            try
            {
                ser = new XmlSerializer(typeof(T));
                ser.UnknownAttribute += new XmlAttributeEventHandler(ser_UnknownAttribute);
                ser.UnknownElement += new XmlElementEventHandler(ser_UnknownElement);
                ser.UnknownNode += new XmlNodeEventHandler(ser_UnknownNode);
                o = ser.Deserialize(s);
            }
            catch (Exception ex)
            {
                e = ex;
            }
            s.Dispose(); // end stream
            if (e != null) throw e;
            return (T)o;
        }

        static void ser_UnknownNode(object sender, XmlNodeEventArgs e)
        {

        }

        static void ser_UnknownElement(object sender, XmlElementEventArgs e)
        {

        }

        static void ser_UnknownAttribute(object sender, XmlAttributeEventArgs e)
        {

        }

        /// <summary>
        /// Deserializr an object
        /// </summary>
        /// <param name="dataorfile">xml source</param>
        /// <param name="source">xml source type</param>
        /// <param name="t">type of the object to instanciate</param>
        /// <returns></returns>
        public static object DeserializeAbstact(string dataorfile, SerializationSource source, Type t)
        {
            Stream s = null;
            if (source == SerializationSource.String)
            {
                // string
                s = new MemoryStream(Encoding.UTF8.GetBytes(dataorfile));
            }
            else
            {
                // file
                s = new FileStream(dataorfile, FileMode.Open);
            }
            Exception e = null;
            XmlSerializer ser = null;
            object o = null;
            try
            {
                ser = new XmlSerializer(t);
                o = ser.Deserialize(s);
            }
            catch (Exception ex)
            {
                e = ex;
            }
            s.Dispose(); // end stream
            if (e != null) throw e;
            return o;
        }

        /// <summary>
        /// Deserialize an object from an UTF8 XML Source
        /// </summary>
        /// <typeparam name="T">Class Type</typeparam>
        /// <param name="dataorfile">xml source (string data or file path)</param>
        /// <returns>the deserialized object</returns>
        public static T Deserialize<T>(string dataorfile)
        {
            T ret = default(T);
            if (System.IO.File.Exists(dataorfile))
            {
                ret = Deserialize<T>(dataorfile, SerializationSource.File);
            }
            else
            {
                ret = Deserialize<T>(dataorfile, SerializationSource.String);
            }
            return ret;
        }



    }


    /// <summary>
    /// Serialized object source
    /// </summary>
    public enum SerializationSource
    {
        /// <summary>
        /// XML string to parse
        /// </summary>
        String,
        /// <summary>
        /// XML file to read
        /// </summary>
        File
    }

    public static partial class DIMDotNetTools
    {
        /// <summary>
        /// Serialize any serializable object to an UTF8 XML String
        /// </summary>
        /// <param name="o">any serializable object</param>
        /// <returns>UTF8 XML String</returns>
        public static string Serialize(this object o)
        {
            return XMLSerializer.Serialize(o);
        }

        public static string ChecksumMD5(this object o)
        {
            return XMLSerializer.ChecksumMD5(o);
        }


        /// <summary>
        /// Serialize any serializable object to an UTF8 XML File
        /// </summary>
        /// <param name="o">any serializable object</param>
        /// <param name="xmlfile">UTF8 XML File to write</param>
        public static void Serialize(this object o, string xmlfile)
        {
            XMLSerializer.Serialize(o, xmlfile);
        }
    }
}
