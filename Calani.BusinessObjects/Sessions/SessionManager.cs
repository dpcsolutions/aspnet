﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Web;

namespace Calani.BusinessObjects
{
    public class SessionManager
    {
        public static void InitUserSessionIfRequired(HttpContext context)
        {
            if (context.Session.Count == 0)
            {
                if (HttpContext.Current.User.Identity.Name != null)
                {
                    SessionManager.InitUserSessions(HttpContext.Current.User.Identity.Name, context.Session);
                }
            }
        }

        public static void InitUserSessions(string email, System.Web.SessionState.HttpSessionState sessionBag, bool force = false)
        {
            Calani.BusinessObjects.Membership.LoginManager mgr = new Calani.BusinessObjects.Membership.LoginManager();
            var usr = mgr.GetUser(email);

            if(usr != null) 
                mgr.LogLogin(usr.id, "Web");

            if (usr != null && usr.type != null && usr.type.Value > 90)
            {
                if (!force)
                    return; // can not resume a staff user. too dangerous.
            }

            if (usr != null)
            {
                sessionBag["UserEmail"] = usr.primaryEmail;
                sessionBag["UserId"] = usr.id.ToString();
                try
                {
                    Calani.BusinessObjects.Contacts.ContactTypeEnum type = (Calani.BusinessObjects.Contacts.ContactTypeEnum)usr.type;
                    sessionBag["UserType"] = type;
                }
                catch
                {
                    sessionBag["UserType"] = Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee;
                }

                if (usr.organizationId != null)
                    sessionBag["UserOrganizationId"] = usr.organizationId.Value.ToString();
                else
                    sessionBag["UserOrganizationId"] = "-1";

                if (usr.lang != null && sessionBag["setlang"] != null)
                {
                    sessionBag["setlang"] = usr.lang;
                }


                sessionBag["UserName"] = (usr.firstName + " " + usr.lastName).Trim();


                

            }
        }
    }
}
