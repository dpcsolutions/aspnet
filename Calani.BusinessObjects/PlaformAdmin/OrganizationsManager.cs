﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.Admin;
using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using log4net;

namespace Calani.BusinessObjects.PlaformAdmin
{
    public class OrganizationsManager : Generic.SmartCollectionManager<Model.organizations, long>
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public OrganizationsManager()
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
           
        }
        internal override DbSet<organizations> GetDbSet(Model.CalaniEntities db)
        {
            return db.organizations;
        }

        internal override SmartQuery<organizations> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.organizations where r.id == id select r);
            return q;
        }


        internal override SmartQuery<organizations> GetBaseQuery()
        {
            var q = base.GetBaseQuery();
            // base
            q.Query = (from r in q.Context.organizations select r);



            if (!IgnoreRecordStatus)
                q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus)
                q.Query = (from r in q.Query orderby r.id select r);


            return q;
        }
        public Model.organizations RegisterNewOrganization(string name)
        {
            // manque l'email du owner
            // lui envoyer un token d'activation (a mettre en db également)
            Model.CalaniEntities db = new Model.CalaniEntities();

            var r = new Model.organizations
            {
                 name = name
              
            };
            db.organizations.Add(r);
            db.SaveChanges();

           // créer les records par défault (taxes, services, etc)

            return r;

        }

        public OrganizationsList GetOrganizations(string groupBy, bool groupDesc, int itemsPerPage, bool multiSort,
            bool mustSort, int page, string sortBy, bool sortDesc, string search,
            CalendarFilterItem range)
        {

            Model.CalaniEntities db = new Model.CalaniEntities();
            var skip = page * itemsPerPage - itemsPerPage;
            var orgs = db.organizations.Where(i=>  i.creationDate.HasValue && i.creationDate>= range.start && i.creationDate<= range.end);

            if (!string.IsNullOrEmpty(sortBy))
            {
                switch (sortBy)
                {
                    case "name":
                        orgs = sortDesc ? orgs.OrderByDescending(o => o.name) : orgs.OrderBy(o => o.name);
                        break;

                    case "activationDate":
                        orgs = sortDesc ? orgs.OrderByDescending(o => o.activationDate) : orgs.OrderBy(o => o.activationDate);
                        break;

                    case "creationDate":
                        orgs = sortDesc ? orgs.OrderByDescending(o => o.creationDate) : orgs.OrderBy(o => o.creationDate);
                        break;

                    case "trialEnd":
                        orgs = sortDesc ? orgs.OrderByDescending(o => o.trialEnd) : orgs.OrderBy(o => o.trialEnd);
                        break;

                    case "subscriptionStart":
                        orgs = sortDesc ? orgs.OrderByDescending(o => o.subscriptionStart) : orgs.OrderBy(o => o.subscriptionStart);
                        break;
                    case "subscriptionEnd":
                        orgs = sortDesc ? orgs.OrderByDescending(o => o.subscriptionEnd) : orgs.OrderBy(o => o.subscriptionEnd);
                        break;
                    case "subscriptionStatus":
                        orgs = sortDesc ? orgs.OrderByDescending(o => o.subscriptionStatus) : orgs.OrderBy(o => o.subscriptionStatus);
                        break;
                        
                    default:
                        orgs = sortDesc ? orgs.OrderByDescending(o => o.id) : orgs.OrderBy(o => o.id);
                        break;
                }

                
            }
            else
                orgs = orgs.OrderBy(o => o.id);

            if (!string.IsNullOrEmpty(search))
            {
                orgs = orgs.Where(o => o.name.Contains(search) || o.id.ToString().Contains(search));
            }

            var total = orgs.Count();
            orgs = orgs.Skip(skip).Take(itemsPerPage);

            var list = orgs.ToList();
            
            var model = new OrganizationsList()
            {
                total = total,
                items = list.Select(l=>l.ToOrganizationInfo()).ToList()
            };

       
            return model;
        }


        public OrganizationInfo GetOrganization(long id)
        {

            Model.CalaniEntities db = new Model.CalaniEntities();
            var org = db.organizations.FirstOrDefault(i => i.id == id);

            OrganizationInfo model = new OrganizationInfo();
            if (org != null)
            {
                model = org.ToOrganizationInfo();
            }
            return model;

        }

        public long SaveOrganization(OrganizationInfo model)
        {
            try
            {
                IgnoreRecordStatus = true;
                SmartAction<organizations> res = null;
                organizations entity = null;
                if (model.id < 1)
                {
                    entity = new organizations()
                    {

                        name = model.name,
                        userLimit = model.userLimit,
                        dailyMailsLimit = model.dailyMailsLimit,
                        recordStatus = model.recordStatus
                    };
                }
                else
                {
                    var q = GetBaseQuery().Query;
                    entity = q.FirstOrDefault(t => t.id == model.id);
                    entity.name = model.name;
                    entity.userLimit = model.userLimit;
                    entity.dailyMailsLimit = model.dailyMailsLimit;
                    entity.recordStatus = model.recordStatus;
                    if (model.recordStatus != 0)
                        entity.subscriptionStatus = "Disabled";
                }
                    

                if (!String.IsNullOrWhiteSpace(model.trialEnd) &&
                    DateTime.TryParseExact(model.trialEnd, Tools.IsoDateFormat, null, DateTimeStyles.None, out var _te))
                    entity.trialEnd = _te;

                if (!String.IsNullOrWhiteSpace(model.subscriptionStart) &&
                    DateTime.TryParseExact(model.subscriptionStart, Tools.IsoDateFormat, null, DateTimeStyles.None, out var _ss))
                    entity.subscriptionStart = _ss;

                if (!String.IsNullOrWhiteSpace(model.subscriptionEnd) &&
                    DateTime.TryParseExact(model.subscriptionEnd, Tools.IsoDateFormat, null, DateTimeStyles.None, out var _se))
                    entity.subscriptionEnd = _se;

                entity.hide_gesmob_label = model.hideGesmobileLabel;
               
                var tId = model.id;
                if (model.id < 1)
                {
                    res = this.Add(entity);

                    tId = res.Record.id;
                }
                else
                {

                   this.Update(model.id, entity);
                }

                return tId;
            }
            catch (Exception e)
            {
                _log.Info($"SaveOrganization error. ");

                return 0;
            }
            
        }
    }
}
