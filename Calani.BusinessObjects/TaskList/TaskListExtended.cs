﻿using System.Collections.Generic;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.TaskList
{
    public class TaskListExtended : tasklists
    {
        public TaskListExtended() { }
        public TaskListExtended(tasklists t)
        {
            id = t.id;
            internalId = t.internalId;
            date = t.date;
            isCompleted = t.isCompleted;
            cancelReason = t.cancelReason;
            recordStatus = t.recordStatus;
            listStatus = t.listStatus;
            jobId = t.jobId;
            contactId = t.contactId;
            clientid = t.clientid;
            contacts = t.contacts;
            jobs = t.jobs;
            taskliststemplates = t.taskliststemplates != null ? new HashSet<taskliststemplates>(t.taskliststemplates) : new HashSet<taskliststemplates>();

            statusName = ((TaskListStatus)t.listStatus).GetName();
        }
        
        public string statusName { get; set; }
    }
    
    public enum TaskListStatus
    {
        impossible = -1,
        inProgress = 0,
        completed = 1,
        archived = 2
    }
    
    public static class TaskListStatusExtensions
    {
        public static string GetName(this TaskListStatus status)
        {
            switch (status)
            {
                case TaskListStatus.impossible:
                    return "Impossible";
                case TaskListStatus.inProgress:
                    return "InProgress";
                case TaskListStatus.completed:
                    return "Completed";
                case TaskListStatus.archived:
                    return "Archived";
                
                default: return "Unknown";
            }
        }
    }
}