﻿namespace Calani.BusinessObjects.TaskList
{
    public class TaskListAnswerModel
    {
        public long Id { get; set; }
        public string Value { get; set; }
    }
}