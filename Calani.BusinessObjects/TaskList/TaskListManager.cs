﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using log4net;

namespace Calani.BusinessObjects.TaskList 
{ 
    public class TaskListManager : SmartCollectionManager<tasklists, long> 
    { 
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType); 
 
        public TaskListManager(long organizationId) 
        { 
            KeyProperty = "id"; 
            RecordStatusProperty = "recordStatus"; 
            OrganizationID = organizationId; 
        } 
         
        internal override DbSet<tasklists> GetDbSet(CalaniEntities db) 
        { 
            return db.tasklists; 
        } 
        
        public long GetLastIndex() 
        { 
            return base.GetBaseQuery().Context.tasklists
                .Max(t => (long?)t.id) ?? 0;
        } 
        
        internal override SmartQuery<tasklists> GetById(long id) 
        { 
            var q = base.GetById(id); 
            q.Query = (from r in q.Context.tasklists  
                where r.id == id  
                select r); 
            return q; 
        } 
        
        public tasklists GetWithTemplates(long id) 
        { 
            var q = base.GetById(id); 
            return q.Context.tasklists
                .Include(r => r.taskliststemplates.Select(tlt => tlt.templates)).ToList()
                .FirstOrDefault(r => r.id == id); 
        } 
         
        public List<TaskListExtended> ListTaskLists() 
        { 
            var dbq = base.GetBaseQuery(); 
            var tasklists = (from r in dbq.Context.tasklists  
                where r.recordStatus == 0  
                orderby r.id  
                select r).ToList();
            var g = tasklists.Select(x => new TaskListExtended(x)).ToList();
            return tasklists.Select(x => new TaskListExtended(x)).ToList(); 
        } 
        
        public List<TaskListExtended> ListLightWeight() 
        { 
            var dbq = base.GetBaseQuery(); 
            var taskLists = (from r in dbq.Context.tasklists  
                where r.recordStatus == 0 orderby r.id select r).ToList(); 
            
            return taskLists.Select(x => new TaskListExtended
            {
                id = x.id,
                internalId = x.internalId,
                contacts = new contacts
                {
                    firstName = x.contacts.firstName,
                    lastName = x.contacts.lastName,
                    id = x.contacts.id
                }
            }).ToList(); 
        } 
         
        public List<TaskListExtended> ListTaskListsForProject(long projectId)
        {
            var tasklists = base.GetBaseQuery().Context.tasklists
                .Include(x => x.jobs)
                .Where(x => x.jobs.id == projectId)
                .ToList(); 
            
            return tasklists.Select(x => new TaskListExtended(x)).ToList(); 
        } 
         
        public SmartAction<tasklists> SaveOrUpdate(long id, tasklists tasklists) 
        { 
            try 
            { 
                var result = id > 0 ? Update(id, tasklists) : Add(tasklists); 
                _log.Info($"SaveOrUpdate. userId:{id}"); 
                return result; 
            } 
            catch (Exception e) 
            { 
                _log.Info("SaveOrUpdate. error.", e); 
                throw; 
            } 
        }

        public bool SaveOrUpdateTemplates(long taskListId, List<long> templateIds)
        {
            var templates = templateIds
                .Select(x => new taskliststemplates {templateId = x, taskListId = taskListId}).ToList();
            return SaveOrUpdateTemplates(taskListId, templates);
        }
        
        public bool SaveOrUpdateTemplates(long taskListId, List<taskliststemplates> templates) 
        { 
            try
            {
                var context = base.GetBaseQuery().Context;
                var oldData = context.taskliststemplates.Where(x => x.taskListId == taskListId).ToList();
                
                context.taskliststemplates.RemoveRange(oldData);
                context.taskliststemplates.AddRange(templates);

                context.SaveChanges();
                
                return true; 
            } 
            catch (Exception e) 
            { 
                _log.Info("SaveOrUpdateTemplates. error.", e); 
                throw; 
            } 
        } 
        
        public void SaveOrUpdateTasklistanswers(long taskListId, List<TaskListAnswerModel> answers)
        {
            try
            {
                var context = base.GetBaseQuery().Context;
                var oldData = context.tasklistanswers.Where(x => x.taskListId == taskListId).ToList();
                
                context.tasklistanswers.RemoveRange(oldData);
                if (answers != null && answers.Any())
                {
                    var tasklistanswers = answers
                        .Select(x => new tasklistanswers {questionId = x.Id, taskListId = taskListId, answer = x.Value})
                        .ToList();
                    context.tasklistanswers.AddRange(tasklistanswers);
                }
                context.SaveChanges();
            } 
            catch (Exception e) 
            { 
                _log.Info("SaveOrUpdateTasklistAnswers. error.", e); 
                throw; 
            } 
        }
    } 
} 