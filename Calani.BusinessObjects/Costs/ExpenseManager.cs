﻿using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Membership;
using Calani.BusinessObjects.MobileService;
using Calani.BusinessObjects.MobileService2.Data;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Notifications;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Calani.BusinessObjects.Costs
{

    public class ExpenseManager : Generic.SmartCollectionManager<expenses, long>
    {
       

        public ExpenseManager(long organizationId)
        {
           
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        

        public long UserId { get; set; }

        internal override DbSet<expenses> GetDbSet(Model.CalaniEntities db)
        {
            return db.expenses;
        }

        public List<expenses> GetProjectRecords(long jobid)
        {
            var q = GetBaseQuery();
            var q2 = (from r in q.Query.Include("timesheets")
                      where r.linkedjobid == jobid
                        
                      orderby r.date ascending
                      select r);

            List<expenses> records = q2.ToList();

            return records;
        }

        public void SavePhoto(long id, byte[] photo)
        {
            string file = System.IO.Path.Combine(Calani.BusinessObjects.DocGenerator.CostsReportGenerator.ExpensesDir, OrganizationID.ToString(), id.ToString() + ".jpg");
            System.IO.FileInfo fi = new System.IO.FileInfo(file);
            if (!fi.Directory.Exists) fi.Directory.Create();
            System.IO.File.WriteAllBytes(file, photo);
        }

        public byte[] GetPhoto(long id)
        {
            string file = System.IO.Path.Combine(Calani.BusinessObjects.DocGenerator.CostsReportGenerator.ExpensesDir, OrganizationID.ToString(), id.ToString() + ".jpg");
            System.IO.FileInfo fi = new System.IO.FileInfo(file);
            if (fi.Exists)
            {
                return System.IO.File.ReadAllBytes(file);
            }
            return null;
        }
        
        public byte[] GetPhotoPage(long id, int page)
        {
            var file = System.IO.Path.Combine(DocGenerator.CostsReportGenerator.ExpensesDir, OrganizationID.ToString(), $"{id}page{page}.jpg");
            var fi = new System.IO.FileInfo(file);
            return fi.Exists ? System.IO.File.ReadAllBytes(file) : null;
        }

        public bool HasPhoto(long id)
        {
            var singleFilePath = System.IO.Path.Combine(DocGenerator.CostsReportGenerator.ExpensesDir, OrganizationID.ToString(), id + ".jpg");
            
            return System.IO.File.Exists(singleFilePath);
        }
        
        public bool HasPhotos(long id)
        {
            var multipleFilePath = System.IO.Path.Combine(DocGenerator.CostsReportGenerator.ExpensesDir, OrganizationID.ToString(), id + "page1" + ".jpg");
            return System.IO.File.Exists(multipleFilePath);
        }

        public List<ExpenseExtended> ListExpenses2(bool? approved, long? employee, DateTime? dtFrom = null, DateTime? dtTo = null, string dateformat = null)
        {
            var q = GetBaseQuery();
            

            if(approved != null)
            {
                if (approved.Value == true)
                {
                    q.Query = (from r in q.Query where r.validationStatus == 1 select r);
                }
                else
                {
                    q.Query = (from r in q.Query where r.validationStatus == 0 select r);
                }
            }

            if(employee != null)
            {
                q.Query = (from r in q.Query where r.employeeId == employee select r);
            }

            if (dtFrom != null)
            {
                q.Query = (from r in q.Query where r.date >= dtFrom select r);
            }

            q.Query = (from r in q.Query.Include("contacts") select r);

            var data = q.Query.ToList();

            CustomerAdmin.OrganizationManager orgmgr = new CustomerAdmin.OrganizationManager(OrganizationID);
            orgmgr.Load();
            Calani.BusinessObjects.Currency.CurrenciesManager currMgr = new Calani.BusinessObjects.Currency.CurrenciesManager(OrganizationID);
            var currencies = currMgr.List();

            var expenses = (from r in data select new ExpenseExtended(r, true, true, dateformat, orgmgr.Currency, currencies)).ToList();
            return expenses;
        }

        public long CountExpenses(bool? approved)
        {
            var q = GetBaseQuery();


            if (approved != null)
            {
                if (approved.Value == true)
                {
                    q.Query = (from r in q.Query where r.approvedmount != null select r);
                }
                else
                {
                    q.Query = (from r in q.Query where r.approvedmount == null select r);
                }
            }

            return q.Query.LongCount();
        }

        internal override SmartQuery<expenses> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in GetDbSet(q.Context) where r.id == id select r);
            return q;
        }

      
        internal override SmartQuery<expenses> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from v in q.Context.expenses.Include("contacts")
                       select v);

            if (OrganizationID > -1) q.Query = (from r in q.Query where r.contacts != null && r.contacts.organizationId != null && r.contacts.organizationId == OrganizationID select r);

            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.date descending select r);


            return q;
        }
        
        private void ProceedPhotos (long id, string fileString)
        {
            if(string.IsNullOrWhiteSpace(fileString)) return;
            
            var fileList = fileString.Split(';').ToList();
            var di = new DirectoryInfo(Path.Combine(DocGenerator.CostsReportGenerator.ExpensesDir, OrganizationID.ToString()));
            if (!di.Exists) di.Create();
            
            foreach (var fileInfo in di.GetFiles($"{id}page*")) {
                fileInfo.Delete();
            }
            
            if (fileList.Count == 1) SavePhoto(id, fileList.First());
            else {
                DeletePhoto(id);
                SavePhotos(id, fileList);
            }
        }
        
        private void SavePhoto(long id, string base64)
        {
            string file = System.IO.Path.Combine(DocGenerator.CostsReportGenerator.ExpensesDir, OrganizationID.ToString(), id.ToString() + ".jpg");

            System.IO.FileInfo fi = new System.IO.FileInfo(file);
            if (!fi.Directory.Exists)
                fi.Directory.Create();

            byte[] bin = Convert.FromBase64String(base64);
            System.IO.File.WriteAllBytes(fi.FullName, bin);
        }
        
        private void SavePhotos(long id, List<string> base64List)
        {
            var i = 1;
            foreach (var base64 in base64List)
            {
                if(String.IsNullOrWhiteSpace(base64))
                    return;
                string file = System.IO.Path.Combine(DocGenerator.CostsReportGenerator.ExpensesDir, OrganizationID.ToString(), id.ToString() + "page" + i++ + ".jpg");

                System.IO.FileInfo fi = new System.IO.FileInfo(file);
                if (!fi.Directory.Exists)
                    fi.Directory.Create();

                byte[] bin = Convert.FromBase64String(base64);
                System.IO.File.WriteAllBytes(fi.FullName, bin);
            }
        }

        public ExpenseExtended SaveOrUpdate(ExpenseExtended toUpdate, long userId)
        {
            ExpenseExtended ret = null;

            using (CalaniEntities db = new CalaniEntities())
            {
                Model.expenses expense = null;
                
                var parsedDate = DateTime.UtcNow;
                    
                if(!String.IsNullOrWhiteSpace(toUpdate.lastModificationDate))
                    parsedDate    = DateTime.ParseExact(toUpdate.lastModificationDate, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);
          
                try
                {
                    var id = long.Parse(toUpdate.id);

                    expense = (from v in GetDbSet(db)
                               where v.id == id ||
                                   (v.lastModifiedById == userId &&
                                    v.lastModificationDate == parsedDate)
                               select v).FirstOrDefault();
                }
                catch (Exception)
                {
                    expense = (from v in GetDbSet(db)
                               where (v.lastModifiedById == userId &&
                                    v.lastModificationDate == parsedDate)
                               select v).FirstOrDefault();

                }
               
                if (expense == null)
                {
                    //new expense
                    var n = new expenses() {
                        amount = toUpdate.price,
                        date = (toUpdate.date != null ? DateTime.ParseExact(toUpdate.date, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture) : DateTime.UtcNow),
                        attachmentId = null,
                        description = toUpdate.description,
                        employeeId = toUpdate.employeeId,
                        lastModificationDate = (toUpdate.lastModificationDate != null ? DateTime.ParseExact(toUpdate.lastModificationDate, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture) : DateTime.UtcNow),
                        lastModifiedById = userId,
                        linkedjobid = toUpdate.linkedjobid,
                        currency = toUpdate.currency,
                        currencyRate = toUpdate.currencyRate,
                        //picture = toUpdate.photo,
                        paytypeId = toUpdate.PayType,
                        vat1 = toUpdate.VAT1,
                        vat2 = toUpdate.VAT2,
                        vat3 = toUpdate.VAT3,
                        validationStatus = (sbyte)toUpdate.status,
                    };

                    if (toUpdate.approvedAmount > 0)
                        n.approvedmount = toUpdate.approvedAmount;

                    if(toUpdate.status != (int)ValidationEnum.Pending)
                    {
                        n.validatedAt = DateTime.Now;
                        n.validatedBy = userId;
                    }

                    db.expenses.Add(n);

                    db.SaveChanges();

                    ProceedPhotos(n.id, toUpdate.photo);
                    
                    ret = new ExpenseExtended(n, false, true);
                    ret.mobileTempId = toUpdate.mobileTempId;

                    SendNotification(userId,ItemActions.Create,n);
                }
                else
                {
                    expense.description = toUpdate.description;
                    expense.amount = toUpdate.price;
                    expense.employeeId = toUpdate.employeeId;
                    expense.approvedmount = toUpdate.approvedAmount;
                    expense.lastModificationDate = DateTime.ParseExact(toUpdate.lastModificationDate, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);
                    expense.lastModifiedById = toUpdate.lastModifiedBy;
                    //expense.picture = toUpdate.photo;
                    expense.date = DateTime.Parse(toUpdate.date, CultureInfo.InvariantCulture);

                    expense.paytypeId = toUpdate.PayType;
                    expense.vat1 = toUpdate.VAT1;
                    expense.vat2 = toUpdate.VAT2;
                    expense.vat3 = toUpdate.VAT3;

                    if (toUpdate.price != expense.amount /*|| expense.picture != toUpdate.photo*/)
                    {
                        expense.approvedmount = null;
                    }

                    ProceedPhotos(expense.id, toUpdate.photo);

                    expense.validationStatus = (sbyte)toUpdate.status;

                    if (toUpdate.status != (int)ValidationEnum.Pending)
                    {
                        expense.validatedAt = DateTime.Now;
                        expense.validatedBy = userId;
                    }

                    expense.linkedjobid = toUpdate.jobid;

                    ret = toUpdate;
    
                    db.SaveChanges();

                    var action = expense.amount == expense.approvedmount ? ItemActions.Validate : ItemActions.Update;
                    SendNotification(userId, action, expense);
                }
               

                return ret;
            }
        }

        internal void DeletePhoto(long id)
        {
            string file = System.IO.Path.Combine(Calani.BusinessObjects.DocGenerator.CostsReportGenerator.ExpensesDir, OrganizationID.ToString(), id.ToString() + ".jpg");
            System.IO.FileInfo fi = new System.IO.FileInfo(file);
            try
            {
                if (fi.Exists) fi.Delete();
            }
            catch { } 
        }

        public void SetPicture(long id, string pictureToUpload)
        {
            string file = System.IO.Path.Combine(Calani.BusinessObjects.DocGenerator.CostsReportGenerator.ExpensesDir, OrganizationID.ToString(), id.ToString() + ".jpg");
            System.IO.FileInfo fi = new System.IO.FileInfo(file);
            if (!fi.Directory.Exists) fi.Directory.Create();

            byte[] bin = Convert.FromBase64String(pictureToUpload);
            System.IO.File.WriteAllBytes(fi.FullName, bin);
        }

        public List<ExpenseExtended> ListExpenses(long employeeId)
        {
            var q = GetBaseQuery();

            q.Query = (from r in q.Query where r.employeeId == employeeId select r);

            var dbrecords = (from r in q.Query select r).ToList();
            return (from r in dbrecords select new ExpenseExtended(r, true, false)).ToList();
        }

        internal override void BeforeAction(SmartAction<expenses> action)
        {
            if(action.Type == SmartActionType.Add || action.Type == SmartActionType.Update)
            {
                if(action.Record != null && action.Record.date != null)
                {
                    if(action.Record.date > DateTime.Today.AddDays(1))
                    {
                        action.Abort(SharedResource.Resource.GetString("FuturDateNowAllowed"));
                    }

                }
            }
        }

        public SmartAction<expenses> Delete(long expenseId, long userId)
        {
            var expense = GetById(expenseId).Query.FirstOrDefault();
            if(expense != null)
            {
                expense.lastModificationDate = DateTime.Now;
                expense.lastModifiedById = userId;
                Update(expenseId, expense);

                return Delete(expenseId);
            }

            return null;
        }

       public List<ExpensesReport> ReportMonths()
        {
            List<ExpensesReport> ret = new List<ExpensesReport>();

            for (int i = 0; i < 2; i++)
            {
                try
                {
                    Model.CalaniEntities db = new CalaniEntities();
                    string sql = GetReportSql();
                    System.Data.Common.DbConnection c = db.Database.Connection;
                    MySql.Data.MySqlClient.MySqlConnection connection = c as MySql.Data.MySqlClient.MySqlConnection;
                    try
                    {
                        connection.Open();
                    }
                    catch { }
                    MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(sql, connection);

                    System.Data.DataSet ds = new System.Data.DataSet();
                    MySql.Data.MySqlClient.MySqlDataAdapter da = new MySql.Data.MySqlClient.MySqlDataAdapter(cmd);
                    da.Fill(ds);

                    foreach (System.Data.DataRow r in ds.Tables[0].Rows)
                    {
                        ExpensesReport e = new ExpensesReport();
                        e.Year = Convert.ToInt32(r["Year"]);
                        e.Month = Convert.ToInt32(r["Month"]);
                        e.Sum = Convert.ToDouble(r["Sum"]);
                        e.Count = Convert.ToInt32(r["Count"]);
                        e.LastName = r["employeeLastName"].ToString();
                        e.FirstName = r["employeeFirstName"].ToString();
                        e.Employee = Convert.ToInt64(r["employeeId"].ToString());

                        ret.Add(e);
                    }
                    try
                    {
                        connection.Close();
                    }
                    catch { }

                    return ret;
                }
                catch (Exception e)
                {
                    ret = new List<ExpensesReport>();
                }
            }


            return ret;
        }

        public List<ExpensesReport> ReportMonthsDetails(int year, int month)
        {
            List<ExpensesReport> ret = new List<ExpensesReport>();

            Model.CalaniEntities db = new CalaniEntities();

            string strmonth = month.ToString();
            if (strmonth.Length < 2) 
                strmonth = "0" + strmonth;

            string sql = GetReportSql(true, year, strmonth);


            System.Data.Common.DbConnection c = db.Database.Connection;
            MySql.Data.MySqlClient.MySqlConnection connection = c as MySql.Data.MySqlClient.MySqlConnection;
            connection.Open();
            MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand(sql, connection);

            System.Data.DataSet ds = new System.Data.DataSet();
            MySql.Data.MySqlClient.MySqlDataAdapter da = new MySql.Data.MySqlClient.MySqlDataAdapter(cmd);
            da.Fill(ds);

            foreach (System.Data.DataRow r in ds.Tables[0].Rows)
            {
                ExpensesReport e = new ExpensesReport();
                e.Year = Convert.ToInt32(r["Year"]);
                e.Month = Convert.ToInt32(r["Month"]);
                e.Sum = Convert.ToDouble(r["Sum"]);
                e.Count = Convert.ToInt32(r["Count"]);
                e.LastName = r["employeeLastName"].ToString();
                e.FirstName = r["employeeFirstName"].ToString();
                e.Employee = Convert.ToInt64(r["employeeId"].ToString());
                ret.Add(e);
            }

            connection.Close();

            return ret;
        }

        private string GetReportSql(bool isDetails = false, int? year = null, string strmonth = "")
        {
            var employeesManager = new EmployeesManager(OrganizationID);
            var user = employeesManager.Get(UserId);
            var isAdmin = user.type.HasValue && (ContactTypeEnum)user.type.Value == ContactTypeEnum.User_Admin;
            var employeeFilterSql = !isAdmin ? $"AND expenses.employeeId = {UserId}" : string.Empty;

            string sql = $@" SELECT YEAR(date) as Year
                            , MONTH(date) as Month
                            , SUM(expenses.approvedmount / IFNULL(expenses.currencyRate,1)) as Sum
                            , COUNT(expenses.id) as Count
                            , expenses.employeeId as employeeId
                            , contacts.lastName as employeeLastName
                            , contacts.firstName as employeeFirstName 
                            FROM expenses 
                            LEFT JOIN contacts ON(contacts.id = expenses.employeeId) 
                            
                            WHERE 
                                  expenses.recordStatus = 0 
                                  AND expenses.approvedmount > 0 
                                  AND contacts.organizationId = {OrganizationID}
                                  {employeeFilterSql}
                             ";
            if (isDetails)
            {
                sql += @" AND expenses.date LIKE '{1}-{2}-%' AND expenses.recordStatus >= 0
                        GROUP BY expenses.employeeId, YEAR(expenses.date), MONTH(expenses.date) 
                        ORDER BY Year DESC, Month DESC, lastName ASC, firstName ASC ";
            }
            else
            {
                sql += @" GROUP BY expenses.employeeId, YEAR(expenses.date), MONTH(expenses.date) 
                         ORDER BY Year DESC, Month DESC ";
            }

            return String.Format(sql,OrganizationID, year, strmonth, UserId);
        }

        private void SendNotification(long currentUserId, ItemActions action, expenses expense)
        {
            NotificationManager nm = new NotificationManager();

            var cultureStr = "fr-FR";

            var culture = new System.Globalization.CultureInfo(cultureStr);

            if (expense.employeeId.HasValue && expense.employeeId.Value > 0)
            {
                try
                {
                    cultureStr = UserCultureManager.GetUserCulture(expense.employeeId.Value);

                    culture = new System.Globalization.CultureInfo(cultureStr?? "fr-FR");
                }
                catch (Exception e)
                {
                    _log.Error("SendNotification, getUserCulture error.",e);
                }
            }



          

            var msg = String.Empty;
            var title = String.Empty;
            var category = String.Empty; 
            var users = new List<long>();
            var db = new CalaniEntities();

            var org = (from r in db.organizations where r.id == OrganizationID select r).FirstOrDefault();

            var employeesManager = new Calani.BusinessObjects.Contacts.EmployeesManager(OrganizationID);
            
            IEnumerable<long> tmpUsers;
            switch (action)
            {
                case ItemActions.Create:

                    /*tmpUsers = org.contacts.Where(c => c.type == 1).Select(r=>r.id).ToList();
                    if(tmpUsers.Any())
                        users.AddRange(tmpUsers);
                    
                    title = $"New expense created";
                    category = "Expenses";


                    msg = $"test msg. create. id:{expense.id}";*/
                    break;

                case ItemActions.Update:
                    // tmpUsers = org.contacts.Where(c => c.organizationId == OrganizationID && c.type == 1 && c.recordStatus == 0).Select(r => r.id).ToList();
                    if (expense.employeeId.HasValue && expense.employeeId.Value != currentUserId)
                        users.Add(expense.employeeId.Value);

                    var rTitleU = GetResource("ExpenseNotificationTitleModified", culture);
                    title = String.Format(rTitleU, expense.id);

                    category = "Expenses";

                    var rMsgU = GetResource("ExpenseNotificationMsgModified", culture);

                    msg = String.Format(rMsgU,expense.id, expense.approvedmount??0);

                    break;

                case ItemActions.Validate:
                    var rTitleV = GetResource("ExpenseNotificationTitleValidated", culture);
                    title = String.Format(rTitleV, expense.id);

                    category = "Expenses";

                    var rMsgV = GetResource("ExpenseNotificationMsgValidated", culture);

                    msg = String.Format(rMsgV, expense.id);

                    if (expense.employeeId.HasValue)
                        users.Add(expense.employeeId.Value);
                    break;
               
                default:
                    throw new ArgumentOutOfRangeException(nameof(action), action, null);
            }
            
            _log.Info($"Sending notification, title:'{title}' to:{String.Join(",", users)},action :{action}, amount:{expense.amount}, approved:{expense.approvedmount}");

            if (!String.IsNullOrWhiteSpace(title))
            {
                nm.sendNotifications(msg, title, users, new Dictionary<string, string>() { { "expenseId", expense.id.ToString() } }, category);

                var mailSender = new Calani.BusinessObjects.Generic.MailSender();
                var pUrl = System.Configuration.ConfigurationManager.AppSettings["PublicUrl"];
                mailSender.Variables.Add("PublicUrl", pUrl);

                var linkTemplate = GetResource("ConsultLinkHtmlTemplate", culture);
                var link = String.Format(linkTemplate, pUrl + "Costs/Expenses/Edit.aspx?id=" + expense.id);

                msg += " " + link;
                foreach (var userId in users)
                {
                    var user = employeesManager.Get(userId);
                    mailSender.MailTo.Email = user.primaryEmail?? user.secondaryEmail;
                    mailSender.MailTo.UserId = userId;
                    mailSender.SendSmartHtmlEmail(title, msg, category);
                }
            }

        }

        private static string GetResource(string n, CultureInfo cultureInfo)
        {
            if (SharedResource.Resource == null) return n;
            return SharedResource.Resource.GetString(n, cultureInfo);
        }

        public expenses GetExpense(long id)
        {
            var q = GetBaseQuery();

            var res = q.Query.FirstOrDefault(i => i.id == id);

            return res;
        }

        public List<ExpenseExtended> GetByProjectId(long projId)
        {
            var q = GetBaseQuery();

            q.Query = (from r in q.Query where r.linkedjobid == projId select r);

            var orgCurrency = GetOrgCurrency();
            var currencies = GetCurrencies();
            var dbrecords = (from r in q.Query select r).ToList();
            return (from r in dbrecords select new ExpenseExtended(r, true, false, null, orgCurrency, currencies)).ToList();
        }


        private List<currency> GetCurrencies()
        {
            try
            {
                Calani.BusinessObjects.Currency.CurrenciesManager currMgr = new Calani.BusinessObjects.Currency.CurrenciesManager(OrganizationID);
                return currMgr.List();
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                throw;
            }
        }

        private string GetOrgCurrency()
        {
            Calani.BusinessObjects.CustomerAdmin.OrganizationManager orgmgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(OrganizationID);
            orgmgr.Load();

            return orgmgr.Currency;
        }

        public static List<PayType> GetPayTypes(CultureInfo culture)
        {
            try
            {
                using (var db = new CalaniEntities())
                    return
                        db.pay_type
                            .Where(pt => pt.recordStatus == 0)
                            .ToList()
                            .ConvertAll(pt => new PayType(pt.id, GetResource("PayType" + pt.type, culture)));
            }
            catch (Exception ex)
            {
                _log.Error(ex);

                throw;

            }

           
        }

    }
}
