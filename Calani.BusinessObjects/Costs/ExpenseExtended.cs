﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.Costs
{
  
    public class ExpenseExtended
    {
        public string id { get; set; }
        public string mobileTempId { get; set; }
        public string date { get; set; }
        public string description { get; set; }
        public double price { get; set; }
        public string photo { get; set; }
        public int status { get; set; }
        public string lastModificationDate { get; set; }
        public long lastModifiedBy { get; set; }
        public double approvedAmount { get; set; }

        public bool approved
        {
            get
            {
                return (ValidationEnum)status == ValidationEnum.Approved;
            }
        }

        public bool rejected
        {
            get
            {
                return (ValidationEnum)status == ValidationEnum.Rejected;
            }
        }

        public string currency { get; set; }

        public string employeeName { get; set; }

        public long? jobid { get; set; }
        public long? employeeId { get; set; }
        public long? linkedjobid { get; set; }

        Model.expenses _record { get; set; }
        public double currencyRate { get; set; }

        public int PayType { get; set; }
        public double VAT1 { get; set; }
        public double VAT2 { get; set; }
        public double VAT3 { get; set; }

        public Model.expenses GetRecord()
        {
            return _record;
        }

        public ExpenseExtended()
        {

        }

        public ExpenseExtended(expenses e, bool employee, bool includeImage, string dateformat = null, string orgCurrency = "", List<currency> currencies = null)
        {
            _record = e;
            if (String.IsNullOrWhiteSpace(dateformat)) 
                dateformat = "yyyy-MM-dd HH:mm";

            id = e.id.ToString();
            mobileTempId = null;
            date = (e.date != null ? ((DateTime)e.date).ToString(dateformat, CultureInfo.InvariantCulture) : e.lastModificationDate.GetValueOrDefault().ToString(dateformat, CultureInfo.InvariantCulture));
            description = (e.description != null ? e.description : "");
            price = (e.amount != null ? (double)e.amount : 0);
            jobid = e.linkedjobid;

            // deprecated ?
            if (includeImage && e.contacts != null && e.contacts.organizationId != null)
            {
                string file = System.IO.Path.Combine(Calani.BusinessObjects.DocGenerator.CostsReportGenerator.ExpensesDir, e.contacts.organizationId.Value.ToString(), e.id.ToString() + ".jpg");
                if (System.IO.File.Exists(file))
                {
                    byte[] photobin = System.IO.File.ReadAllBytes(file);
                    photo = Convert.ToBase64String(photobin);
                }
            }
            // -->

            status = e.validationStatus;

            lastModificationDate = (e.lastModificationDate != null ? ((DateTime)e.lastModificationDate).ToString(Tools.IsoLongDateFormat, CultureInfo.InvariantCulture) : DateTime.Now.ToString(Tools.IsoLongDateFormat));
            lastModifiedBy = (e.lastModifiedById != null ? (long)e.lastModifiedById : 0);
            approvedAmount = e.approvedmount != null ? (double)e.approvedmount : 0;
            
            var currStr = orgCurrency;

            if (!String.IsNullOrEmpty(e.currency))
            {
                if (currencies != null )
                {
                    var curr = currencies.FirstOrDefault(c => c.currency1 == e.currency);
                    if (curr != null)
                        currStr = curr.symbol;
                }
                else
                {
                    currStr = e.currency;
                }
            }
           

            currency = currStr;

            if (employee && e.contacts != null)
            {
                employeeName = e.contacts.firstName + " " + e.contacts.lastName;
            }

        }
    }
}
