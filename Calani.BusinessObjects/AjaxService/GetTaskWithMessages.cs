﻿using System.Collections.Generic;

namespace Calani.BusinessObjects.AjaxService
{
	public class GetTaskWithMessages: TaskUpdate
	{
		public List<TaskActivityMessage> TaskMessages { get; set; }

		public Dictionary<long,string> Tags { get; set; }

		public Dictionary<long,string> AssignUsers { get; set; }

		public string OwnerName { get; set; }
		public string PriorityName { get; set; }
		public string StatusName { get; set; }
	}
}
