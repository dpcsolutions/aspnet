﻿using Calani.BusinessObjects.Enums;
using System;
using System.Collections.Generic;

namespace Calani.BusinessObjects.AjaxService
{
    public class ProjectUpdate
    {       
        public long id { get; set; }
        public string action { get; set; }
        public string internalId { get; set; }
        public long clientId { get; set; }
        public long? assigneeId { get; set; }
        public long clientContact { get; set; }
        public long clientAddress { get; set; }
        public long linkedjobid { get; set; }
        public string validity { get; set; }
        public bool updateInvoiceDate { get; set; }

        public List<ProjectItem> items { get; set; }

        public double discount { get; set; }

        public float discountPercent { get; set; }

        public TypeOfDiscountEnum discountType { get; set; }
        public RecurringUpdate recurringUpdate { get; set; }

        public float discountPercent2 { get; set; }

        public TypeOfDiscountEnum discountType2 { get; set; }

        public float discountPercent3 { get; set; }

        public TypeOfDiscountEnum discountType3 { get; set; }

        public float discountPercent4 { get; set; }

        public TypeOfDiscountEnum discountType4 { get; set; }

        public string description { get; set; }

        public string culture { get; set; }

        public int rating { get; set; }

        public int? batch { get; set; }

        public List<Projects.paymentExtended> payments { get; set; }

        public List<long> tsImports { get; set; }
        public List<long> expImports { get; set; }
        public List<long> additionalEmployeeIds { get; set; }
        public List<long> templateListIds { get; set; }

        public string currency { get; set; }

        public string firstNote { get; set; }

        public List<long> jobImports { get; set; }

        public string sendCopyEmailTo { get; set; }
        public string comment { get; set; }

        public CloneItems cloneItems { get; set; }

        public String tmpFiles { get; set; }
        public String deliveryDate { get; set; }
        public string deliveredTo { get; set; }

        public string color { get; set; }

        public string invoicePaidDate { get; set; }

        public string customerReference { get; set; }

        public string typeIntervention { get; set; }

        public string locationIntervention { get; set; }

        public bool skipMissingInvoiceNotif { get; set; }

        public long[] linesToRemove { get; set; }
    }

    public class CloneItems
    {
        public bool attachments { get; set; }
        public bool links { get; set; }
        public bool notes { get; set; }

        public CloneItems()
        {
            attachments = true;
            links = true;
            notes = true;
        }
        
        public CloneItems(bool all)
        {
            attachments = all;
            links = all;
            notes = all;
        }
    }

    public class ProjectItem
    {
        public long? id { get; set; }
        public string customNumber { get; set; }
        public string item { get; set; }
        public string unitName { get; set; }
        public double qty { get; set; }
        public double price { get; set; }
        public string tax { get; set; }
        public double discount { get; set; }
        public double totalprice { get; set; }
        public double? rate { get; set; }
        public int position { get; set; }
        public bool? selected { get; set; }

        public bool? isChecked { get; set; }

        public long? toinvoice { get; set; } // nouveau, permet de savoir is la ligne est deja facturée et la rend read-only. ne fonctione que sur un projet (pas devis ni facture)

        public bool serviceVatIncl { get; set; }
        public bool isDelivered { get; set; }
        public long? serviceId { get; set; }
        internal string serviceName { get; set; }
        internal string serviceInternalId { get; set; }

    }
}
