﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.AjaxService
{

    public class TimeSheetUpdate
    {
        public string culture { get; set; }
        public long sheetId { get; set; }
        public string action { get; set; }
        public List<TimeUpdate> items { get; set; }

        public long employee { get; set; }
        public int year { get; set; }
        public int week { get; set; }

        public string comment { get; set; }
    }
    public class TimeSheetUpdateAction : AjaxAction
    {
        public List<TimeUpdate> items { get; set; }
        public string validationStatus { get; set; }
        public string lastUpdateStatus { get; set; }
    }
    public class TimeUpdate
    {
        public string description { get; set;}
        public double? rate { get; set; }
        public double? duration { get; set; }
        public bool warn { get; set; }
        public string date { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public long id { get; set; }
        public string comment { get; set; }
        public long? parentId { get; set; }
        public int? position { get; set; }
        public bool isParent { get; set; }
        public int? index { get; set; }
        public int? lunchDuration { get; set; }
        public int? pauseDuration { get; set; }

        public string service { get; set; }
        public long? serviceId { get; set; }
        public string job { get; set; }
        public long? jobId { get; set; }
        
        public string visit { get; set; }
        public string time { get; set; }

        public int recordType { get; set; }
        public int? rowType { get; set; }
        public int dayOfWeek { get; set; }

        public string enteredDuration { get; set; }
      
        
        public TimeUpdate()
        {

        }

        public TimeUpdate(Model.timesheetrecords r, int type = 0)
        {
            recordType = type;
            description = r.description;
            duration = r.duration;
            id = r.id;
            warn = r.is_warning;
            rate = r.rate;
            comment = r.comment;
            parentId = r.parent_id;
            isParent = r.timesheetrecords1.Any(c => c.recordStatus != -1);
            index = r.order_index.HasValue ? r.order_index.Value : 0;
            lunchDuration = r.lunch_duration;
            pauseDuration = r.pause_duration;
            rowType = r.row_type;
            
            
            if (r.startDate.HasValue)
            {
                date = r.startDate.Value.ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture);

                if (r.startDate.Value.Hour > 0 || r.startDate.Value.Minute > 0)
                    startTime = r.startDate.Value.ToString(Tools.DefaultShortTimeFormat, CultureInfo.InvariantCulture);
                else
                    startTime = "00:00";

                dayOfWeek = (int)r.startDate.Value.DayOfWeek;
            }

            if(r.endDate.HasValue)
            {
                if (r.endDate.Value.Hour > 0 || r.endDate.Value.Minute > 0)
                    endTime = r.endDate.Value.ToString(Tools.DefaultShortTimeFormat, CultureInfo.InvariantCulture);
                else
                    endTime = "00:00";
            }
         
            if (r.work_schedule_profiles_rec_id.HasValue && r.work_schedule_profiles_rec != null)
            {
                time = r.work_schedule_profiles_rec.name;
            }


            if (startTime == "00:00" && (String.IsNullOrWhiteSpace(endTime) || startTime == endTime))
            {
                if (rowType.HasValue && (TimeSheetRecordRowType)rowType == TimeSheetRecordRowType.MasterRow && duration.HasValue)
                {
                    var timeSpan = TimeSpan.FromHours(duration.Value);
                    enteredDuration = new DateTime(timeSpan.Ticks).ToString("HH:mm");
                    startTime = "";
                    endTime = "";
                    duration = 0;
                }
            }

            if (r.jobId != null && r.jobs.clients != null)
            {
                job = r.jobs.clients.companyName + " - " + r.jobs.internalId;
                jobId = r.jobId.Value;
            }

            if (r.serviceId != null && r.services != null)
            {
                service = r.serviceName;
                serviceId = r.serviceId.Value;
            }
        }
          public void Bind(timesheetrecords dbrec, System.Globalization.CultureInfo c, long orgid, Model.CalaniEntities db, long userId)
        {
            dbrec.description = description;
            
            dbrec.rate = rate;
            dbrec.is_warning = warn;
            
            dbrec.comment = comment;
            dbrec.parent_id = parentId;
            dbrec.order_index = index;
            dbrec.lunch_duration = lunchDuration.HasValue? lunchDuration : 0;
            dbrec.pause_duration = pauseDuration.HasValue? pauseDuration : 0;
            dbrec.row_type = (int?)rowType;
            
            dbrec.startDate = BuildDate(date, startTime);
            dbrec.endDate = BuildDate(date, endTime);

            if (dbrec.startDate == dbrec.endDate )
            {
                dbrec.duration = duration;
            }
            else if(dbrec.timesheetrecords1.Any())
            {
                dbrec.duration = dbrec.timesheetrecords1.Sum(d => d.duration);
            }
            else
            {
                TimeSpan t = ((DateTime)dbrec.endDate - (DateTime)dbrec.startDate);
                TimeSpan h1 = TimeSpan.Parse("1:00:00");

                dbrec.duration = (double)t.Ticks / (double)h1.Ticks;
            }

            if (!String.IsNullOrWhiteSpace(visit))
            {
                long visitId = Convert.ToInt64(visit);
                var daVisit = (from r in db.visits where r.id == visitId select r).FirstOrDefault();
                if (daVisit != null)
                {
                    dbrec.visitId = daVisit.id;
                }
            }
            
            if (!String.IsNullOrWhiteSpace(time))
            {
                long timeId = Convert.ToInt64(time);
                var dbScheduleRec = (from r in db.work_schedule_profiles_rec where r.id == timeId select r).FirstOrDefault();
                if (dbScheduleRec != null)
                {
                    dbrec.work_schedule_profiles_rec_id =timeId;
                }
            }
            else
            {
                dbrec.work_schedule_profiles_rec_id = null;
            }


            if (jobId.HasValue)
            {
                var daJob = (from r in db.jobs where r.id == jobId.Value select r).FirstOrDefault();
                if (daJob != null)
                {
                    dbrec.jobId = daJob.id;

                }
            }

            if (serviceId.HasValue)
            {
                var daService = (from r in db.services where r.id == serviceId.Value select r).FirstOrDefault();
                if (daService != null)
                {
                    dbrec.serviceId = daService.id;
                    dbrec.serviceName = daService.name;
                    dbrec.servicePrice = daService.unitPrice;
                    dbrec.serviceType = daService.type;
                }
            }

            if (db.ChangeTracker.HasChanges())
            {
                dbrec.lastModificationDate = DateTime.Now;
                dbrec.lastModifiedBy = userId;
            }
        }

     /*   public void Bind(timesheet_records_services dbrec, System.Globalization.CultureInfo c, long orgid, Model.CalaniEntities db, long userId)
        {
            dbrec.duration = duration;
            dbrec.is_warning = warn;
            dbrec.comment = comment;
            dbrec.date = BuildDate(date,String.Empty);

            dbrec.lastModificationDate = DateTime.Now;
            dbrec.lastModifiedBy = userId;


            if (!String.IsNullOrWhiteSpace(visit))
            {
                long visitId = Convert.ToInt64(visit);
                var daVisit = (from r in db.visits where r.id == visitId select r).FirstOrDefault();
                if (daVisit != null)
                {
                    dbrec.visitId = daVisit.id;
                }
            }


            if (jobId.HasValue)
            {
                var daJob = (from r in db.jobs where r.id == jobId.Value select r).FirstOrDefault();
                if (daJob != null)
                {
                    dbrec.jobId = daJob.id;

                }
            }

            if (serviceId.HasValue)
            {
                var daService = (from r in db.services where r.id == serviceId.Value select r).FirstOrDefault();
                if (daService != null)
                {
                    dbrec.serviceId = daService.id;
                    dbrec.serviceName = daService.name;
                    dbrec.servicePrice = daService.unitPrice;
                    dbrec.serviceType = daService.type;
                }
            }

        }
        */
        private DateTime? BuildDate(string date, string time)
        {
            if (time == null)
                time = "00:00:00";

            DateTime? ret = null;
            if(date != null )
            {
                date = date.Replace(".", "/");
                DateTime d1 = new DateTime(1, 1, 1);
                if (DateTime.TryParseExact(date, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture,
                     System.Globalization.DateTimeStyles.NoCurrentDateDefault, out d1))
                {

                    string str = d1.ToString("yyyy-MM-dd " + time);

                    if(DateTime.TryParse(str, out d1))
                    {
                        ret = d1;
                    }
                }

            }
            return ret;
        }

        public bool IsEmpty()
        {
            if (date == null) return true;
            return false;
        }
    }
}
