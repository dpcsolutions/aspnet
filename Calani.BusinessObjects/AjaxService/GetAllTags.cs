﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.AjaxService
{
    public class GetAllTags
    {
        public List<TagList> PrivateTags { get; set; }

        public List<TagList> SharedTags { get; set; }
    }
}
