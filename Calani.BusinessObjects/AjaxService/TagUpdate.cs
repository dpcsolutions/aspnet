﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.AjaxService
{
    public class TagUpdate
    {
        public long id { get; set; }
        public string name { get; set; }
        public Nullable<int> module { get; set; }
        public string color { get; set; }
        public Nullable<int> type { get; set; }
    }
}
