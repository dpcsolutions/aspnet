﻿namespace Calani.BusinessObjects.AjaxService
{
    public class AjaxAction
    {
        public string cloneInternalId;

        public long cloneId { get; set; }

        public bool success { get; set; }
        public string message { get; set; }

        public long id { get; set; }

        public long attachment { get; set; }

        public string data { get; set; }
    }
}
