﻿using System.Collections.Generic;

namespace Calani.BusinessObjects.AjaxService
{
    public class ExportToProjectsTask
    {
        public long source { get; set; }
        public string culture { get; set; }
        public List<ExportToProjectsTarget> to { get; set; }
        public bool includeAllAttachments { get; set; }
        public long[] attachmentsIds { get; set; }
    }

    public class ExportToProjectsTarget
    {
        public string type { get; set; }
        public long id { get; set; }
        public string referenceNumber { get; set; }
    }
}
