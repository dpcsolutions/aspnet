﻿using Calani.BusinessObjects.AbsenceRequests.Models;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Calani.BusinessObjects.AjaxService.Models
{
    [DataContract]
    public class DataTableModel
    {
        [DataMember(Name = "data")]
        public List<AbsenceReportModel> Data { get; set; }
    }
}
