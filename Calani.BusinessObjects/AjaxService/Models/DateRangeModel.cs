﻿using Calani.BusinessObjects.Generic;
using System;
using System.Globalization;
using System.Runtime.Serialization;

namespace Calani.BusinessObjects.AjaxService.Models
{
    [DataContract]
    public class DateRangeModel
    {
        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public string StartDateStr 
        { 
            get 
            { 
                return StartDate.ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture);
            }
            set
            {

            }
        }

        [DataMember]
        public DateTime EndDate { get; set; }

        [DataMember]
        public string EndDateStr
        {
            get
            {
                return EndDate.ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture);
            }
            set
            {

            }
        }
    }
}
