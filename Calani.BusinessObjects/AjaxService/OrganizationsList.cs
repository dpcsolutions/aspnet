﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.AjaxService
{
   public class OrganizationsList
    {
        public int total { get; set; }
        
        public List<OrganizationInfo> items { get; set; }
    }

   public class OrganizationInfo
   {
       public EmployeeDto admin;
       public long id { get; set; }
       public String name { get; set; }
       public String activationDate { get; set; }
       public String creationDate { get; set; }
       public String subscriptionStart { get; set; }
       public String subscriptionEnd { get; set; }

       public String subscriptionStatus { get; set; }

        
       public string email { get; set; }
       
       public String trialEnd { get; set; }
       public string phone { get; set; }

       public int? userLimit { get; set; }
       public int dailyMailsLimit { get; set; }
       public int recordStatus { get; set; }

        public bool hideGesmobileLabel { get; set; }
        
       public string adminName
       {
           get
           {
               return admin.FullName;
           }

       }

       public string adminPrimaryEmail
       {
           get
           {
               return admin?.PrimaryEmail;
           }

       }
        
       public string adminSecondaryEmail
        {
           get
           {
               return admin?.SecondaryEmail;
           }

       }

       public string adminPrimaryPhone
        {
           get
           {
               return admin?.PrimaryPhone;
           }

       }
       public string admiSecondaryPhone
        {
           get
           {
               return admin?.SecondaryPhone;
           }

       }
    }

   public static class OrganizationInfoExtentions

    {
        public static OrganizationInfo ToOrganizationInfo(this organizations org)
        {
            var m = new OrganizationInfo();

            if (org != null)
            {
                m.id = org.id;
                m.name = org.name;
                m.activationDate = org.activationDate?.ToString(Tools.DefaultDateShortTimeFormat, CultureInfo.InvariantCulture);
                m.creationDate = org.creationDate?.ToString(Tools.DefaultDateShortTimeFormat, CultureInfo.InvariantCulture);
                m.email = org.email;
                m.trialEnd = org.trialEnd?.ToString(Tools.IsoDateFormat, CultureInfo.InvariantCulture);
                m.phone = org.phone;
                m.subscriptionStatus = org.subscriptionStatus;
                m.subscriptionStart = org.subscriptionStart?.ToString(Tools.IsoDateFormat, CultureInfo.InvariantCulture);
                m.subscriptionEnd = org.subscriptionEnd?.ToString(Tools.IsoDateFormat, CultureInfo.InvariantCulture);
                m.admin = org.contacts.FirstOrDefault(c => c.type == 1)?.ToDto();
                m.userLimit = org.userLimit;
                m.dailyMailsLimit = org.dailyMailsLimit;
                m.recordStatus = org.recordStatus;
                m.hideGesmobileLabel = org.hide_gesmob_label;
            }

            return m;
        }

    }
}
