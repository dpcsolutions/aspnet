﻿using System;
using System.Reflection;
using Calani.BusinessObjects.Projects;
using log4net;

namespace Calani.BusinessObjects.AjaxService
{
    public static class EmailHelper
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod()?.DeclaringType);

        public static AjaxAction SendMail(SendEmail msg, long orgId, string fromUserName, long fromUserId)
        {
            var sender = new ProjectEmailDocumentsSender
            {
                Email = msg,
                OrganizationId = orgId,
                From = fromUserName,
                FromUserId = fromUserId
            };

            var ret = new AjaxAction();

            if (!EmailSendingValidationManager.ValidateEmailLimit(orgId))
            {
                ret.success = false;
                ret.message = "Email limit exceeded";
                return ret;
            }

            try
            {
                sender.Send();
                ret.success = true;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                ret.success = false;
                ret.message = ex.Message;
            }
            finally
            {
                EmailSendingValidationManager.CountEmailSent(orgId);
                Log.Info($"SendMail. subject:{msg.subject}, orgId:{orgId}, From:{fromUserName}, userId:{fromUserId}, result: {(ret.success ? "success" : ret.message)}");
            }

            return ret;
        }
    }
}
