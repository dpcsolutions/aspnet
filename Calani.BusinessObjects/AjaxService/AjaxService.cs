﻿using Calani.BusinessObjects.AbsenceRequests;
using Calani.BusinessObjects.AbsenceRequests.Models;
using Calani.BusinessObjects.Admin;
using Calani.BusinessObjects.AjaxService.Models;
using Calani.BusinessObjects.Attachments;
using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.PlaformAdmin;
using Calani.BusinessObjects.Projects;
using Calani.BusinessObjects.Projects.Models;
using Calani.BusinessObjects.Scheduler;
using Calani.BusinessObjects.Scheduler.Models;
using Calani.BusinessObjects.Tags;
using Calani.BusinessObjects.TaskActivity;
using Calani.BusinessObjects.Tasks;
using Calani.BusinessObjects.TimeManagement;
using Calani.BusinessObjects.TimeManagement.Models;
using Calani.BusinessObjects.TimeManagement.Services;
using Calani.BusinessObjects.WorkSchedulers;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;
using Calani.BusinessObjects.Hangfire;

namespace Calani.BusinessObjects.AjaxService
{


    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AjaxService : IAjaxService
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #region Resources Allocations

        public AjaxAction ListResourceAllocations(DateTime dtFrom, DateTime dtTo, string locale)
        {
            var ret = new AjaxAction() { success = true };
            locale = string.IsNullOrEmpty(locale) ? "fr-CH" : locale;

            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long currentUserId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
            var orgMgr = new OrganizationManager(organizationId);
            orgMgr.Load();
            var employeesManager = new EmployeesManager(organizationId);
            var currentUser = employeesManager.GetById(currentUserId).Query.SingleOrDefault();

            if (!currentUser.IsAdmin())
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                ret.success = false;
                return ret;
            }
            try
            {
                var service = new ResourcesAllocationsService(organizationId, currentUserId);                
                var models = service.ListResourceAllocationsForScheduler(dtFrom, dtTo, locale);
                ret.data = JsonConvert.SerializeObject(models);

            }
            catch (Exception ex)
            {
                ret.success = false;
                ret.message = ex.Message;
                _log.Error(ex.Message, ex);
            }

            return ret;
        }

        public AjaxAction RestoreDeletedResourceAllocation(long id)
        {
            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long currentUserId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
            var employeesManager = new EmployeesManager(organizationId);
            var currentUser = employeesManager.GetById(currentUserId).Query.SingleOrDefault();

            if (!currentUser.IsAdmin())
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return new AjaxAction() { success = false };
            }

            var service = new ResourcesAllocationsService(currentOrganizationId: organizationId, currentUserId: currentUserId);
            return service.RestoreDeletedResourceAllocation(id);
        }

        public AjaxAction AddOrUpdateResourceAllocation(ResourceAllocationModel model)
        {
            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long currentUserId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
            var employeesManager = new EmployeesManager(organizationId);
            var currentUser = employeesManager.GetById(currentUserId).Query.SingleOrDefault();

            if (!currentUser.IsAdmin())
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return new AjaxAction() { success = true };
            }

            var manager = new ResourcesAllocationsService(currentOrganizationId: organizationId, currentUserId: currentUserId);
            return manager.AddOrUpdateResourceAllocation(model);
        }

        public AjaxAction DeleteResourceAllocation(long id)
        {
            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long currentUserId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
            var employeesManager = new EmployeesManager(organizationId);
            var currentUser = employeesManager.GetById(currentUserId).Query.SingleOrDefault();

            if (!currentUser.IsAdmin())
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return new AjaxAction() { success = true };
            }

            var manager = new ResourcesAllocationsService(currentOrganizationId: organizationId, currentUserId: currentUserId);
            return manager.DeleteResourceAllocation(id);
        }

        #endregion Resources Allocations

        #region Contracts

        public ContractDto GetActiveContract(long contactId)
        {
            if (contactId < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(contactId));
            }

            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            var contractsManager = new EmployeesManager.EmployeesContractsManager(organizationId);
            var activeContract = contractsManager.GetActiveContractDto(contactId);

            return activeContract;
        }

        #endregion Contracts

        #region Absence Requests

        public DataTableModel GetAbsencesReports(long startTicks, long endTicks)
        {
            var result = new DataTableModel()
            {
                Data = new List<AbsenceReportModel>()
            };

            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long currentUserId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
            var contractsManager = new EmployeesManager.EmployeesContractsManager(organizationId);
            var employeesManager = new EmployeesManager(organizationId);
            var currentUser = employeesManager.GetById(currentUserId).Query.SingleOrDefault();

            if (!currentUser.IsAdmin())
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                return result;
            }

            var organizationsManager = new OrganizationManager(organizationId);
            var workScheduleRecManager = new WorkScheduleRecManager(organizationId);
            var workScheduleService = new WorkScheduleService(organizationsManager, workScheduleRecManager);
            var workDay = workScheduleService.GetWorkDaySettings();
            var workingTimeCalculator = new WorkingTimeCalculator(workDay);
            var absenceRequestsManager = new AbsenceRequestsManager(organizationId, contractsManager, employeesManager);
            var vacationCarriedOverDaysManager = new VacationCarriedOverDaysManager(organizationId);
            var vacationReportService = new VacationReportService(employeesManager, contractsManager, organizationsManager, absenceRequestsManager,
                vacationCarriedOverDaysManager, workingTimeCalculator);
            var absenceRequestsService = new AbsenceRequestsService(absenceRequestsManager, employeesManager, contractsManager,
                vacationReportService, workingTimeCalculator);

            var startDate = startTicks > 0 ? new DateTime(startTicks) : DateTime.Now.Date.AddYears(-5);
            var endDate = endTicks > 0 ? new DateTime(endTicks) : DateTime.Now.Date.AddDays(1);
            if(startDate < endDate)
            {
                var reports = absenceRequestsService.GetAbsenceReports(startDate, endDate);
                result.Data = reports;
            }
            
            return result;
        }

        public AjaxAction GetVacationReportForEmployee(long employeeId, long startTicks, long endTicks)
        {
            var ret = new AjaxAction() { success = true };
            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long currentUserId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
            var employeesManager = new EmployeesManager(organizationId);
            var currentUser = employeesManager.GetById(currentUserId).Query.SingleOrDefault();

            if(!currentUser.IsAdmin() && currentUserId != employeeId)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.Unauthorized;
                ret.success = false;
                return ret;
            }

            var contractsManager = new EmployeesManager.EmployeesContractsManager(organizationId);
            var organizationsManager = new OrganizationManager(organizationId);
            var workScheduleRecManager = new WorkScheduleRecManager(organizationId);
            var workScheduleService = new WorkScheduleService(organizationsManager, workScheduleRecManager);
            var workDay = workScheduleService.GetWorkDaySettings();
            var workingTimeCalculator = new WorkingTimeCalculator(workDay);
            var absenceRequestsManager = new AbsenceRequestsManager(organizationId, contractsManager, employeesManager);
            var vacationCarriedOverDaysManager = new VacationCarriedOverDaysManager(organizationId);
            var vacationReportService = new VacationReportService(employeesManager, contractsManager, organizationsManager, absenceRequestsManager,
                vacationCarriedOverDaysManager, workingTimeCalculator);

            var startDate = startTicks > 0 ? new DateTime(startTicks) : DateTime.Now.Date.AddYears(-5);
            var endDate = endTicks > 0 ? new DateTime(endTicks) : DateTime.Now.Date.AddDays(1);

            try
            {
                var model = vacationReportService.GetVacationReport(employeeId, new DateRange(startDate, endDate));
                ret.data = JsonConvert.SerializeObject(model);

            }
            catch (Exception ex)
            {
                ret.success = false;
                ret.message = ex.Message;
                _log.Error(ex.Message, ex);
            }

            return ret;
        }

        #endregion Absence Requests


        private services_jobs GetNewServiceJobs(services_jobs service, double? qty = null, long? jobId = null)
        {
            return new services_jobs
            {
                id = 0,
                serviceId = service.serviceId,
                serviceQuantity = qty ?? service.serviceQuantity,
                currency = service.currency,
                custom_number = service.custom_number,
                discount = service.discount,
                is_checked = service.is_checked,
                position = service.position,
                toinvoice = service.toinvoice,
                contactId = service.contactId,
                isDeliveried = false,
                jobId = jobId ?? service.jobId,
                type = service.type,
                serviceName = service.serviceName,
                servicePrice = service.servicePrice,
                taxName = service.taxName,
                taxAmount = service.taxAmount,
                timesheetDate = service.timesheetDate,
                unitName = service.unitName,
                recordStatus = service.recordStatus,
                totalPriceExclTax = service.totalPriceExclTax,
                servicePriceInCurrency = service.servicePriceInCurrency,
                totalPriceExclTaxInCurrency = service.totalPriceExclTaxInCurrency,
                totalPriceIncTaxInCurrency = service.totalPriceIncTaxInCurrency,
                totalPrinceIncTax = service.totalPrinceIncTax,
                taxRate = service.taxRate
            };
        }
        public AjaxAction CreateDeliveryNoteFromService(List<JobDeliveryNoteCreateModel> models)
        {
            var result = new AjaxAction {success = false};
            var db = new CalaniEntities();

            var first = models.FirstOrDefault();
            var job = db.jobs.FirstOrDefault(x => x.id == first.jobId);
            if (job == null) return result;
            
            var newNote = new jobs
            {
                id = 0,
                clientId = job.clientId,
                subtype = "DeliveryNote",
                linkedjobid = job.id,
                clientContact = job.clientContact,
                clientAddress = job.clientAddress,
                description = string.Empty,
                rating = 0, discount = 0, total = 0,
                dateCreated = DateTime.Now, type = 1,
                organizationId = job.organizationId,
                recordStatus = 0, totalInCurrency = 0, totalWithoutTaxes = 0,
                internalId = first?.reference,
                discount_percent = 0, discount_percent2 = 0, discount_percent3 = 0, discount_percent4 = 0,
                discount_type = 1, discount_type2 = 1, discount_type3 = 1, discount_type4 = 1
            };
            
            db.jobs.Add(newNote);
            db.SaveChanges();
            result.id = newNote.id;

            var dnServices = new List<services_jobs>();
            
            foreach (var model in models)
            {
                var service = db.services_jobs.FirstOrDefault(x => x.id == model.id);
                if (service == null || model.qty <= 0) continue;

                var oldQty = service.serviceQuantity;
                service.serviceQuantity = model.qty;
                service.isDeliveried = true;
                    
                if (model.remainderQty > 0)
                {
                    var newService = GetNewServiceJobs(service, model.remainderQty);
                    var coeff = model.qty / oldQty;
                    coeff = coeff ?? 1;
                    
                    service.totalPriceExclTax *= coeff;
                    service.totalPrinceIncTax *= coeff;
                    service.totalPriceExclTaxInCurrency *= coeff;
                    service.totalPriceIncTaxInCurrency *= coeff;
                    
                    newService.totalPriceExclTax -= service.totalPriceExclTax;
                    newService.totalPrinceIncTax -= service.totalPrinceIncTax;
                    newService.totalPriceExclTaxInCurrency -= service.totalPriceExclTaxInCurrency;
                    newService.totalPriceIncTaxInCurrency -= service.totalPriceIncTaxInCurrency;
                    
                    db.services_jobs.Add(newService);
                }
                db.services_jobs.AddOrUpdate(service);

                dnServices.Add(GetNewServiceJobs(service, jobId: newNote.id));
                result.success = true;
            }

            db.services_jobs.AddRange(dnServices);
            
            newNote.total = dnServices.Sum(x => x.totalPrinceIncTax);
            newNote.totalInCurrency = dnServices.Sum(x => x.totalPriceIncTaxInCurrency);
            newNote.totalWithoutTaxes = dnServices.Sum(x => x.totalPriceExclTax);
            db.jobs.AddOrUpdate(newNote);
            
            db.SaveChanges();
            return result;
        }
        
        public JobsCollectionModel<InvoiceModel> ListRecurringInvoices()
        {
            var invoices = new JobsCollectionModel<InvoiceModel>{Data = new List<InvoiceModel>()};
            try
            {
                var organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
                var projectsManager = new ProjectsManager(organizationId);
                var jobsService = new JobsService(projectsManager);

                invoices.Data = jobsService.ListRecurringInvoices();
            }
            catch(Exception ex)
            {
                _log.Error(ex.Message, ex);
            }

            return invoices;
        }
        
        public JobsCollectionModel<InvoiceModel> ListInvoices(long startTicks, long endTicks)
        {
            var invoices = new JobsCollectionModel<InvoiceModel>() 
            {
                Data = new List<InvoiceModel>()
            };
            try
            {
                DateRange dateRange = null;

                if (startTicks > 0)
                {
                    var startDate = new DateTime(startTicks);
                    var endDate = endTicks > 0 ? new DateTime(endTicks) : DateTime.Now.Date.AddDays(1);
                    dateRange = new DateRange(startDate, endDate);
                }

                long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
                var projectsManager = new ProjectsManager(organizationId);
                
                var jobsService = new JobsService(projectsManager);

                invoices.Data = jobsService.ListInvoices(dateRange);
            }
            catch(Exception ex)
            {
                _log.Error(ex.Message, ex);
            }

            return invoices;
        }

        public JobsCollectionModel<DeliveryNoteModel> ListDeliveryNotes(long startTicks, long endTicks, long assigneeId = 0, long creatorId = 0)
        {
            var deliveryNotes = new JobsCollectionModel<DeliveryNoteModel>()
            {
                Data = new List<DeliveryNoteModel>()
            };

            DateRange dateRange = null;

            if (startTicks > 0)
            {
                var startDate = new DateTime(startTicks);
                var endDate = endTicks > 0 ? new DateTime(endTicks) : DateTime.Now.Date.AddDays(1);
                dateRange = new DateRange(startDate, endDate);
            }

            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            var projectsManager = new ProjectsManager(organizationId);
            
            var jobsService = new JobsService(projectsManager);

            long? assigneeIdNullable = null;
            if (assigneeId > 0)
            {
                assigneeIdNullable = assigneeId;
            }

            long? creatorIdNullable = null;
            if (creatorId > 0)
            {
                creatorIdNullable = creatorId;
            }

            deliveryNotes.Data = jobsService.ListDeliveryNotes(dateRange, assigneeIdNullable, creatorIdNullable);

            return deliveryNotes;
        }

        public JobsCollectionModel<QuoteModel> ListQuotes(long startTicks, long endTicks, long assigneeId = 0)
        {
            var quotes = new JobsCollectionModel<QuoteModel>()
            {
                Data = new List<QuoteModel>()
            };

            DateRange dateRange = null;

            if (startTicks > 0)
            {
                var startDate = new DateTime(startTicks);
                var endDate = endTicks > 0 ? new DateTime(endTicks) : DateTime.Now.Date.AddDays(1);
                dateRange = new DateRange(startDate, endDate);
            }

            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            var projectsManager = new ProjectsManager(organizationId);
            
            var jobsService = new JobsService(projectsManager);

            long? assigneeIdNullable = null;
            if (assigneeId > 0)
            {
                assigneeIdNullable = assigneeId;
            }

            quotes.Data = jobsService.ListQuotes(dateRange, assigneeIdNullable);

            return quotes;
        }

        public AjaxAction DeleteAttachment(ProjectAttachment pa)
        {           
            AjaxAction ret = new AjaxAction();
            ret.success = false;

            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
            _log.Info($"DeleteAttachment. AttachmentId:{pa.AttachmentId}, orgId:{organizationId}, userId:{userId}");


            Calani.BusinessObjects.Attachments.AttachmentsManager mgr = new Attachments.AttachmentsManager();
            mgr.OrganizationId = organizationId;
            var att = mgr.Get(pa.AttachmentId);
            if (att != null && att.organizationId != null && att.organizationId == organizationId)
            {
                mgr.Delete(pa.AttachmentId);
                ret.success = true;
            }

            return ret;
        }

        public AjaxAction UpdateVisit(visitToUpdate v)
        {
            SessionManager.InitUserSessionIfRequired(HttpContext.Current);

            AjaxAction ret = new AjaxAction();
            ret.success = true;

            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);

            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);

            _log.Info($"Ajax.UpdateVisit. visitId:{v.id}, visitStatus:{v.status}, orgId:{organizationId}, userId:{userId}, details:(sd:{v.startDate}, ed:{v.endDate}. repeatdays:{v.repeatdays})" );

            System.Globalization.CultureInfo c = new System.Globalization.CultureInfo("en-US");
            try
            {
                c = new System.Globalization.CultureInfo(v.culture);
            }
            catch { }

            JobsManager mgr = new JobsManager(organizationId);

            try
            {
                ret.id = mgr.UpdateOrCreate(userId, v);
                ret.success = true;
            }
            catch (Exception e)
            {
                ret.message = SharedResource.Resource.GetString("updateVisitManagerError", c);
                ret.success = false;

                _log.Error($"UpdateVisit. error.", e);

                throw;
            }

            return ret;
        }

        public List<jobExtended> GetJobsForClient(long clientId)
        {
            SessionManager.InitUserSessionIfRequired(HttpContext.Current);

            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            ProjectsManager mgr = new ProjectsManager(organizationId);
            var dbRecords = mgr.ListOpens(clientId);
            return (from r in mgr.ListOpens(clientId) orderby r.id descending select new jobExtended(r)).ToList();
        }

        public AjaxAction DeleteVisit(visitToUpdate toUpdate)
        {
            SessionManager.InitUserSessionIfRequired(HttpContext.Current);

            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);

            _log.Info($"DeleteVisit. visitId:{toUpdate.id}, visitStatus:{toUpdate.status}, orgId:{organizationId}, userId:{userId}");
            JobsManager mgr = new JobsManager(organizationId);

            if (toUpdate.applyToAllRepeat && toUpdate.numrepeat > 0 && !String.IsNullOrWhiteSpace(toUpdate.repeatGroupId))
            {
                
                    try
                    {
                        var s = mgr.DeleteGroup(toUpdate.repeatGroupId);
                        return new AjaxAction()
                        {
                            id = -1,
                            success = true
                        };
                    }
                    catch (Exception e)
                    {
                        _log.Error(e.Message, e);
                        return new AjaxAction()
                            {
                                id = -1,
                                message = "Could not update db",
                                success = false
                            };
                    }
                
            }
            else
            {
                var s = mgr.Delete(long.Parse(toUpdate.id));
                return new AjaxAction()
                {
                    id = long.Parse(toUpdate.id),
                    message = s.ErrorMessage,
                    success = s.Success
                };
            }
        }

        public List<visitsExtended> GetVisits(calendarRequest c)
        {
            SessionManager.InitUserSessionIfRequired(HttpContext.Current);

            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            JobsManager mgr = new JobsManager(organizationId);
            var absenceRequestsManager = new AbsenceRequestsManager(organizationId);

            var start = DateTime.Parse(c.start);
            var end = DateTime.Parse(c.end);

            var res =  mgr.ListVisits(start, end, c.resources, c.jobId, c.visitType);

            if (!c.excludeAbsenceRequests)
            {
                var absenceRequests = absenceRequestsManager.ListAbsenceRequestsForDiary(start, end, c.resources, c.cultureCode);
                res.AddRange(absenceRequests);
            }
            
            return res;
        }

        public ClientProperties GetClientProperties(long id)
        {
            ClientProperties ret = new ClientProperties();

            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);

            Calani.BusinessObjects.Contacts.ClientAddressesManager adrmgr = new Calani.BusinessObjects.Contacts.ClientAddressesManager(organizationId, id);
            ret.Addresses = (from r in adrmgr.List() select new SimpleItem { id = r.id.ToString(), text = r.Description }).ToList();


            Calani.BusinessObjects.Contacts.ClientContactsManager ctcmgr = new Calani.BusinessObjects.Contacts.ClientContactsManager(organizationId, id);
            ret.Contacts = (from r in ctcmgr.List() select new SimpleItem { id = r.id.ToString(), text = r.Description }).ToList();

            ProjectsManager jobmgr = new ProjectsManager(organizationId);
            //ret.Jobs = (from r in jobmgr.ListOpens(id) select new SimpleItem { id = r.id.ToString(), text = r.internalId }).ToList();
            ret.Jobs = jobmgr.ListOpens(id)
                .Where(r =>(r.dateJobDone == null && r.dateJobValidity > DateTime.Now
                               || r.dateJobDone != null && r.importedIn == null && !r.skipMissingInvoiceNotif))
                .Select(r => new SimpleItem
                {
                    id = r.id.ToString(),
                    text = r.internalId
                })
                .ToList();
            return ret;

        }

        #region TimeSheets

        public List<Tuple<string, string>> GetTimeSheetComments(long timeSheetId, string comment)
        {
            var result = new List<Tuple<string, string>>();
            var organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            
            var timeSheetRecords = new TimeSheetRecordsManager(organizationId).List().ToList();
            var timeSheetComment = new TimeSheetsManager(organizationId).GetById(timeSheetId).Query.FirstOrDefault()?.comment;
            var selected = timeSheetRecords.Where(x => x.timesheetId == timeSheetId);

            foreach (var item in selected) {
                if (string.IsNullOrEmpty(item.comment) || item.startDate == null || item.endDate == null) continue;
                result.Add(new Tuple<string, string>($"{item.startDate.Value:dd/MM/yyyy HH:mm}-{item.endDate.Value:HH:mm}", item.comment));
            }
            
            if (!string.IsNullOrEmpty(timeSheetComment)) {
                result.Add(new Tuple<string, string>(comment, timeSheetComment));
            }
            
            return result;
        }
        
        public TimeSheetsCollectionModel ListTimeSheets(long startTicks, long endTicks, long employeeId = 0)
        {
            var timeSheets = new TimeSheetsCollectionModel
            {
                Data = new List<TimeSheetModel>()
            };

            DateRange dateRange = null;

            if (startTicks > 0)
            {
                var startDate = new DateTime(startTicks);
                var endDate = endTicks > 0 ? new DateTime(endTicks) : DateTime.Now.Date.AddDays(1);
                dateRange = new DateRange(startDate, endDate);
            }

            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            var timeSheetsManager = new TimeSheetsManager(organizationId);
            var timeSheetsService = new TimeSheetsService(timeSheetsManager);

            long? employeeIdNullable = null;
            if (employeeId > 0)
            {
                employeeIdNullable = employeeId;
            }

            timeSheets.Data = timeSheetsService.ListTimeSheets(organizationId, dateRange, employeeIdNullable);

            return timeSheets;
        }

        public TimeSheetUpdateAction[] ValidateMultipleTimesheets(string[] timesheetsStrIds)
        {
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);

            var empManager = new EmployeesManager(organizationId);
            var user = empManager.Get(userId);
            if((ContactTypeEnum)(user?.type ?? 0) != ContactTypeEnum.User_Admin)
            {
                throw new UnauthorizedAccessException("Only administrator can validate timesheets.");
            }

            if(timesheetsStrIds == null || timesheetsStrIds.Length == 0)
            {
                return Array.Empty<TimeSheetUpdateAction>();
            }

            var timesheetsIds = new List<long>(timesheetsStrIds.Length);
            foreach (var strId in timesheetsStrIds)
            {
                long id = 0;
                if(long.TryParse(strId, out id))
                {
                    timesheetsIds.Add(id);
                }
            }

            if (!timesheetsIds.Any())
            {
                return Array.Empty<TimeSheetUpdateAction>();
            }

            _log.Info($"ValidateMultipleTimesheets. timesheets ids:{string.Join(", ", timesheetsIds)}, " +
                $"action: validate, orgId:{organizationId}, userId:{userId}");

            var mgr = new TimeManagement.TimeSheetRecordsManager(organizationId);

            var results = timesheetsIds
                            .Select(timesheetId => mgr.ValidateTimesheet(timesheetId, new System.Globalization.CultureInfo("en-US")))
                            .ToArray();

            return results;
        }

        public TimeSheetUpdateAction UpdateTimeSheet(TimeSheetUpdate update)
        {
            var userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
            var organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);

            var empRepository = new EmployeesManager(organizationId).List();
            var currentUser = empRepository.First(x => x.id == userId);

            var dbSheet = new CalaniEntities().timesheets.FirstOrDefault(x => x.id == update.sheetId);
            if (dbSheet?.acceptedDate != null 
                && update.action == "save" 
                && currentUser.type == (int)ContactTypeEnum.User_Employee)
            {
                var ret = new TimeSheetUpdateAction
                    {success = false, message = "Can't update validated timesheet" };
                return ret;
            }
            
            _log.Info($"UpdateTimeSheet. timesheetId:{update.sheetId}, action:{update.action}, orgId:{organizationId}, userId:{userId}");
            System.Globalization.CultureInfo c = new System.Globalization.CultureInfo("en-US");
            try
            {
                c = new System.Globalization.CultureInfo(update.culture);
            }
            catch { }

            if (update.action != null && update.action == "delete")
            {
                var ret = new TimeSheetUpdateAction();
                var mgr = new TimeManagement.TimeSheetsManager(Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]));
                var d = mgr.Delete(update.sheetId);
                ret.success = d.Success;
                ret.message = d.ErrorMessage;
                return ret;
            }
            else
            {
                var mgr = new TimeManagement.TimeSheetRecordsManager(Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]));
                return mgr.UpdateSet(update, c, userId);
            }
        }

        public List<TimeManagement.SelectableTimeItem> GetTimeSheetSelectableTimeItems(timeSheetSelectableTimeQuery q)
        {

            System.Globalization.CultureInfo c = new System.Globalization.CultureInfo("en-US");
            try
            {
                c = new System.Globalization.CultureInfo(q.culture);
            }
            catch { }

            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);

            Calani.BusinessObjects.TimeManagement.TimeSheetItemsList selectableItems = new Calani.BusinessObjects.TimeManagement.TimeSheetItemsList(organizationId, q.year, q.week, q.employee);
            return selectableItems.List(c);
        }

        public TimeSheetsRecordsCollectionModel ListTimeSheetsRecordsForInvoiceTime(long startTicks, long endTicks)
        {
            var timeSheetRecords = new TimeSheetsRecordsCollectionModel()
            {
                Data = new List<TimeSheetRecordModel>()
            };

            DateRange dateRange = null;

            if (startTicks > 0)
            {
                var startDate = new DateTime(startTicks);
                var endDate = endTicks > 0 ? new DateTime(endTicks) : DateTime.Now.Date.AddDays(1);
                dateRange = new DateRange(startDate, endDate);
            }

            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            var timeSheetRecordsManager = new TimeSheetRecordsManager(organizationId);
            var contactsManager = new ClientContactsManager(organizationId, 0);
            var timeSheetRecordsService = new TimeSheetRecordsService(timeSheetRecordsManager, contactsManager);

            timeSheetRecords.Data = timeSheetRecordsService.ListTimeSheetsRecordsForInvoiceTime(dateRange);

            return timeSheetRecords;
        }

        public AjaxAction CancelTimeSheetRecordImport(string stringDelimetedIds)
        {
            var ajaxAction = new AjaxAction
            {
                success = true
            };

            long parsedId;
            var ids = !string.IsNullOrWhiteSpace(stringDelimetedIds) ?
                    stringDelimetedIds.Split(',')
                    .Select(str => long.TryParse(str, out parsedId) ? parsedId : 0)
                    .Where(id => id > 0)
                    .ToList() : null;

            if (ids == null || !ids.Any())
            {
                ajaxAction.success = false;
                ajaxAction.message = "No ids provided";
                return ajaxAction;
            }

            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);

            var timeSheetRecordsManager = new TimeSheetRecordsManager(organizationId);
            var contactsManager = new ClientContactsManager(organizationId, 0);
            var timeSheetRecordsService = new TimeSheetRecordsService(timeSheetRecordsManager, contactsManager);

            try
            {
                timeSheetRecordsService.CancelTimeSheetRecordImport(ids, userId);
            }
            catch(Exception ex)
            {
                _log.Error(ex.Message, ex);
                ajaxAction.success = false;
                ajaxAction.message = ex.Message;
            }

            return ajaxAction;
        }        

        public List<long> ImportTimeSheetRecords(TimeSheetRecordsInvoiceModel[] models)
        {
            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            var mgr = new ProjectsManager(organizationId);
            var serviceJobsIds = new List<long>();

            if (models == null || models.Length < 1) return serviceJobsIds;

            foreach(var clientGroup in models.GroupBy(m => m.ClientId))
            {
                var clientId = clientGroup.Key;

                foreach(var projectGroup in clientGroup.GroupBy(m => m.ProjectId))
                {
                    var projectId = projectGroup.Key;
                    var recordsIds = projectGroup.SelectMany(m => m.TimeSheetRecordsIds).Distinct().ToList();
                    var jobsIds = mgr.ImportTime(clientId, projectId, recordsIds);
                    serviceJobsIds.AddRange(jobsIds);
                }
            }

            return serviceJobsIds.Distinct().ToList();
        }

        #endregion TimeSheets

        internal AjaxAction UpdateProject(ProjectUpdate update, long organizationId, long userId)
        {
            _log.Info($"UpdateProject. jobId:{update.id}, action:{update.action}, orgId:{organizationId}, userId:{userId}");

            AjaxAction ret = new AjaxAction();
            var freshAttachments = new List<attachmentsExtended>();
            ret.success = true;
            ret.id = update.id;
            string attachmentsJson = null;

            var mgr = new ProjectsManager(organizationId);

            var c = new CultureInfo("en-US");
            try
            {
                c = new CultureInfo(update.culture);
            }
            catch { }

            // items:
            try
            {
                if (update.action != null && update.action == "delete")
                {
                    var x = mgr.Delete(update.id);
                    ret.success = x.Success;
                    ret.message = x.ErrorMessage;
                }
                else
                {
                    if (update.action != null && update.action.Contains("setinvoicesentdate"))
                    {
                        var dateStr = update.action.Substring(update.action.LastIndexOf(':') + 1);

                            //var date = DateTime.ParseExact(update.action, Tools.DefaultDateFormat, System.Globalization.CultureInfo.InvariantCulture);

                            update.validity = dateStr;
                    }

                    var itemsUpdater = new ProjectItemsUpdater();
                    itemsUpdater.OrganizationId = organizationId;
                    itemsUpdater.ProjectId = update.id;
                    itemsUpdater.Update = update;
                    itemsUpdater.UpdateDb(userId, update.action);
                    ret.id = itemsUpdater.ProjectId;

                    var action = new Projects.ProjectActionManager();
                    action.Culture = c;
                    action.OrganizationId = organizationId;
                    action.ProjectId = itemsUpdater.ProjectId;
                    action.Action = update.action;
                    action.UserId = userId;
                    action.Update = update;

                    if (!action.DoAction())
                    {
                        ret.success = false;
                        ret.message = action.Error;

                    }
                    else
                    {
                        ret.attachment = action.Attachment;
                        if (action.ToInvoiceId > 0) ret.cloneId = action.ToInvoiceId;
                    }

                    if (update.action.StartsWith("clone:"))
                    {
                        string targetType = update.action.Split(':').LastOrDefault().Trim().ToLower();

                        var clone = mgr.CloneProject(action.ProjectId, targetType, null, null, update.cloneItems);
                        ret.cloneId = clone.Key;
                        ret.cloneInternalId = clone.Value;

                    }
 

                    if (!String.IsNullOrWhiteSpace(update.firstNote))
                    {
                        MobileService.NotesManager nmgr = new MobileService.NotesManager(organizationId);
                        nmgr.Add(new Model.notes
                        {
                            author = userId,
                            date = DateTime.Now,
                            message = update.firstNote,
                            jobId = ret.id
                        });
                    }

                    var attachmentsMgr = new AttachmentsManager();
                    attachmentsMgr.OrganizationId = organizationId;
                    if (!String.IsNullOrWhiteSpace(update.tmpFiles))
                    {
                        var files = JsonConvert.DeserializeObject<List<String>>(update.tmpFiles);
                        foreach (var file in files) {
                            attachmentsMgr.UploadFileAttachmentToDb(file, jobId: ret.id, userId: null, ftype: null);
                        }
                    }
                    
                    var db = new CalaniEntities();
                    var projEmp = db.projectemployees;
                    var exising = projEmp.Where(x => x.projectId == update.id);
                    projEmp.RemoveRange(exising);
                    
                    if (update.additionalEmployeeIds.Any())
                    {
                        foreach (var employeeId in update.additionalEmployeeIds)
                        {
                            projEmp.Add(new projectemployees {
                                projectId = ret.id,
                                employeeId = employeeId,
                                recordStatus = 0
                            });
                        }
                    }
                    
                    var jobTemplates = db.jobs_templates;
                    var exisingJT = jobTemplates.Where(x => x.jobId == update.id);
                    jobTemplates.RemoveRange(exisingJT);
                    
                    if (update.templateListIds.Any())
                    {
                        foreach (var id in update.templateListIds)
                        {
                            jobTemplates.Add(new jobs_templates {
                                 jobId = ret.id,
                                 templateId = id
                             });
                        }
                    }
                        
                    db.SaveChanges();
                    
                    freshAttachments = attachmentsMgr.ListJobAttachments(ret.id, onlyNotGenerated: true).ToList();
                    if (freshAttachments.Any())
                    {
                        foreach(var attachment in freshAttachments) {
                            attachment.jobs = null;
                            attachment.organizations = null;
                        }
                        attachmentsJson = JsonConvert.SerializeObject(
                            new { attachments = freshAttachments.ToArray() });
                    }
                }


            }
            catch(DbEntityValidationException ex)
            {
                var validationErrors = string.Join("; ", ex.EntityValidationErrors.SelectMany(e => e.ValidationErrors).Select(e => e.ErrorMessage));
                _log.Error(validationErrors, ex);
                ret.success = false;
                ret.message = $"{ex.Message} : {validationErrors}";
                return ret;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                ret.success = false;
                ret.message = ex.Message;
                return ret;
            }
            
            var info = mgr.GetInfo(ret.id);
            var items = info.ExportItems().OrderBy(i => i.position);

            ret.data = JsonConvert.SerializeObject(new { 
                attachments = freshAttachments.Select(a => new { id = a.id, title = a.NameWithExtension }),
                items = items,
            });

            return ret;
        }

        public AjaxAction UpdateProject(ProjectUpdate update)
        {
            long orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
            return UpdateProject(update, orgId, userId);
        }

        public AjaxAction UpdateProjectColors()
        {
            var ret = new AjaxAction() { success = true };

            var orgManager = new OrganizationsManager();
            var orgIds = orgManager.List().Where(org => org.recordStatus != -1).Select(org => org.id);

            foreach(var orgId in orgIds)
            {
                var result = UpdateProjectColorsForOrg(orgId);

                if (!result.success)
                {
                    ret.success = false;
                    ret.message += (" | " + result.message);
                }
            }

            return ret;
        }

        internal AjaxAction UpdateProjectColorsForOrg(long organizationId)
        {
            var ret = new AjaxAction() { success = true };

            var mgr = new ProjectsManager(organizationId);

            var projects = mgr.List();

            var colors = RandomColorGenerator.RandomColor.GetColors(RandomColorGenerator.ColorScheme.Random, RandomColorGenerator.Luminosity.Light, projects.Count);

            Func<System.Drawing.Color, string> colorToString = c => "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");

            for (var i = 0; i < projects.Count; i++)
            {
                var color = colors[i];
                var project = projects[i];

                project.color = colorToString(color);
                var updateRes = mgr.Update(project.id, project);
                if (!updateRes.Success)
{
                    ret.success = false;
                    ret.message = updateRes.ErrorMessage;
                    return ret;
                }
}

            return ret;
        }

        public GetTaskWithMessages GetTaskDetails(long TaskId)
        {
            GetTaskWithMessages taskUpdate = new GetTaskWithMessages();
            try
            {
                var userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
                var organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);

                TasksManager mgr = new TasksManager(organizationId);
                var task = mgr.Get(TaskId);

                TaskActivityManager tam = new TaskActivityManager(organizationId);
                List<TaskActivityMessage> taskActivityMessages = tam.GetAllTaskActivityByTaskId(TaskId, userId);
                AttachmentsManager am = new AttachmentsManager();
                am.OrganizationId = organizationId;
                am.UserId = userId;
                List<TaskActivityMessage> taskAttachments = am.AttachmentMessageByTask(TaskId, userId);
                taskActivityMessages.AddRange(taskAttachments);
                TaskTagMappingManager tagsMappingManager = new TaskTagMappingManager();
                var tagIds = tagsMappingManager.GetallTagByTaskId(TaskId).Select(e => e.tagId).ToList();

                TagsManager tagsManager = new TagsManager(organizationId);
                Dictionary<long, string> Tags = new Dictionary<long, string>();
                foreach (var tIds in tagIds)
                {
                    if (tIds != null)
                    {
                        var Tag = tagsManager.Get(tIds.Value);
                        Tags.Add(Tag.id, Tag.name);
                    }
                }

                TaskContactMappingManager contactMappingManager = new TaskContactMappingManager(organizationId);
                var assignUserIds = contactMappingManager.GetAllContactMappingByTaskId(TaskId).Select(e => e.contactId).ToList();
                Dictionary<long, string> AssignUsers = new Dictionary<long, string>();
                EmployeesManager employeesManager = new EmployeesManager(organizationId);
                foreach (var assignUserId in assignUserIds)
                {
                    if (assignUserId != null)
                    {
                        var contact = employeesManager.Get(assignUserId.Value);
                        AssignUsers.Add(contact.id, $"{contact.firstName} {contact.lastName}");
                    }
                }

                string OwnerName = string.Empty;
                string StatusName = string.Empty;
                string PriorityName = string.Empty;



                if (task.ownerId != null)
                {
                    var Owner = employeesManager.Get(task.ownerId.Value);
                    OwnerName = $"{Owner.firstName} {Owner.lastName}";
                }
                if (task.status != null)
                {
                    StatusName = Enum.GetName(typeof(enumStatus), task.status);
                }
                if (task.prority != null)
                {
                    PriorityName = Enum.GetName(typeof(enumPriority), task.prority);
                }


                taskUpdate = new GetTaskWithMessages
                {
                    id = TaskId,
                    title = task.title,
                    description = task.description,
                    progress = task.progress ?? 0,
                    status = task.status,
                    priority = task.prority,
                    dueDate = task.dueDate ==null? null: task.dueDate.Value.ToString("MM/dd/yyyy"),
                    timeline = task.timeline,
                    owner = task.ownerId,
                    OwnerName = OwnerName,
                    isPinned = mgr.GetTask_Pin_Status(userId, TaskId)?.IsPinned,
                    TaskMessages = taskActivityMessages.OrderBy(e => e.CreatedAt).ToList(),
                    Tags = Tags,
                    AssignUsers = AssignUsers,
                    PriorityName = PriorityName,
                    StatusName = StatusName,
                };
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                throw ex;
            }

            return taskUpdate;
        }

        public AjaxAction SaveTaskActivity(long taskId, string Message)
        {
            AjaxAction ret = new AjaxAction();
            ret.success = true;
            var organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);

            _log.Info($"SaveTaskActivity. taskId:{taskId}, message:{Message}, orgId:{organizationId}, userId:{userId}");

            TaskActivityManager tam = new TaskActivityManager(organizationId);
            try
            {
                task_activity task_Activity = new task_activity
                {
                    CreatedAt = DateTime.Now,
                    TaskId = taskId,
                    ContactId = Convert.ToInt64(HttpContext.Current.Session["UserId"]),
                    OrganizationId = organizationId,
                    Message = Message
                };
                tam.Add(task_Activity);
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                ret.success = false;
                ret.message = ex.Message;
            }
            return ret;
        }

        internal AjaxAction UpdateTask(TaskUpdate update, long organizationId)
        {
            AjaxAction ret = new AjaxAction();
            ret.success = true;

            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);

            _log.Info($"UpdateTask. taskId:{update.id}, action:{update.action}, orgId:{organizationId}, userId:{userId}");

            try
            {


                TagsManager tagsManager = new TagsManager(organizationId);
                var tags = tagsManager.CreateTagsIfNotExist(update.tags, (int)Modules.Task, "#000000");
                if (update.action != null && update.action == "delete")
                {
                    Tasks.TasksManager mgr = new TasksManager(organizationId);
                    var x = mgr.Delete(update.id);
                    ret.success = x.Success;
                    ret.message = x.ErrorMessage;
                }

                else if (update.id > 0)
                {
                    DateTime? dueDate = null;
                    DateTime Date, Date2;
                    if (DateTime.TryParseExact(update.dueDate, "dd/MM/yyyy", null, DateTimeStyles.None, out Date))
                    {
                        dueDate = Date;
                    }
                    else if (!string.IsNullOrEmpty(update.dueDate) && DateTime.TryParse(update.dueDate, out Date2))
                    {
                        dueDate = Date2;
                    }
                    Tasks.TasksManager mgr = new Tasks.TasksManager(organizationId);
                    var result = mgr.Update(update.id, new tasks
                    {
                        organizationId = organizationId,
                        title = update.title,
                        description = update.description,
                        progress = update.progress,
                        status = update.status,
                        prority = update.priority,
                        dueDate = dueDate,
                        recordStatus = 0,
                        timeline = update.timeline,
                        ownerId = update.owner
                    });

                    var taskId = update.id;
                    Tasks.TaskTagMappingManager tagsMappingManager = new Tasks.TaskTagMappingManager();
                    tagsMappingManager.DeleteByTaskId(taskId);

                    foreach (var tag in tags)
                    {
                        tagsMappingManager.Add(new task_tag_mapping
                        {
                            taskId = taskId,
                            tagId = tag.id
                        });
                    }


                    Tasks.TaskContactMappingManager contactMappingManager = new Tasks.TaskContactMappingManager(organizationId);
                    contactMappingManager.DeleteByTaskId(taskId);
                    if (update.assignTo != null)
                    {
                        foreach (var assign in update.assignTo)
                        {
                            long lAssign;
                            if (Int64.TryParse(assign, out lAssign))
                            {
                                var cmtask = contactMappingManager.Add(new task_contact_mapping
                                {
                                    contactId = lAssign,
                                    taskId = taskId,
                                    recordStatus = 0,
                                    organizationId = organizationId
                                });
                            }
                        }
                    }
                    ret.message = "UpdatedSuccessfully";
                }
                else if (update.action != null && update.action == "save")
                {
                    Tasks.TasksManager mgr = new Tasks.TasksManager(organizationId);

                    DateTime? dueDate = null;
                    DateTime Date;
                    if (DateTime.TryParseExact(update.dueDate, "dd/MM/yyyy", null, DateTimeStyles.None, out Date))
                    {
                        dueDate = Date;
                    }
                    var result = mgr.Add(new tasks
                    {
                        organizationId = organizationId,
                        title = update.title,
                        description = update.description,
                        progress = update.progress,
                        status = update.status,
                        prority = update.priority,
                        dueDate = dueDate,
                        recordStatus = 0,
                        timeline = update.timeline,
                        ownerId = update.owner
                    });

                    var taskId = result.Record.id;
                    Tasks.TaskTagMappingManager tagsMappingManager = new Tasks.TaskTagMappingManager();


                    foreach (var tag in tags)
                    {
                        tagsMappingManager.Add(new task_tag_mapping
                        {
                            taskId = taskId,
                            tagId = tag.id
                        });
                    }


                    Tasks.TaskContactMappingManager contactMappingManager = new Tasks.TaskContactMappingManager(organizationId);
                    if (update.assignTo != null)
                    {
                        foreach (var assign in update.assignTo)
                        {
                            try
                            {
                                long lAssign;
                                if (Int64.TryParse(assign, out lAssign))
                                {
                                    var cmtask = contactMappingManager.Add(new Calani.BusinessObjects.Model.task_contact_mapping
                                    {
                                        contactId = lAssign,
                                        taskId = taskId,
                                        recordStatus = 0,
                                        organizationId = organizationId
                                    });

                                    var mId = cmtask.Record.id;
                                }
                            }
                            catch (Exception ex)
                            {
                                _log.Error(ex.Message, ex);
                                throw ex;
                            }
                        }
                    }
                    ret.message = "SavedSuccessfully";
                }

            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                ret.success = false;
                ret.message = ex.Message;
            }

            return ret;
        }

        public GetAllTags GetAllTagsForTask()
        {
            var organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            GetAllTags tagList = new GetAllTags();
            CalaniEntities db = new CalaniEntities();
            var privateTagIds = (from t in db.tags select t).Where(c => c.organizationId == organizationId && c.module == (int)Modules.Task && c.type == (int)TagTypes.Private && c.recordStatus == 0).Select(e => e.id);
            TagsManager tagsManager = new TagsManager(organizationId);

            var privateTags = new List<TagList>();
            foreach (var tIds in privateTagIds)
            {
                if (tIds != null)
                {
                    var Tag = tagsManager.Get(tIds);
                    privateTags.Add(new TagList
                    {
                        id = tIds,
                        name = Tag.name,
                        color = Tag.color,
                        type = Tag.type,
                        module = Tag.module
                    });
                }
            }
            var sharedTagIds = (from t in db.tags select t).Where(c => c.organizationId == organizationId && c.type == (int)TagTypes.Shared && c.recordStatus == 0).Select(e => e.id);
            var sharedTags = new List<TagList>();
            foreach (var tIds in sharedTagIds)
            {
                if (tIds != null)
                {
                    var Tag = tagsManager.Get(tIds);
                    sharedTags.Add(new TagList
                    {
                        id = tIds,
                        name = Tag.name,
                        color = Tag.color,
                        type = Tag.type,
                        module = Tag.module
                    });
                }
            }

            tagList = new GetAllTags
            {
                PrivateTags = privateTags,
                SharedTags = sharedTags
            };


            return tagList;
        }

        public AjaxAction UpdateTask(TaskUpdate update)
        {
            long orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
            return UpdateTask(update, orgId);
        }

        internal AjaxAction UpdateTag(TagUpdate update, long organizationId)
        {
            AjaxAction ret = new AjaxAction();


            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);

            _log.Info($"UpdateTag. tagId:{update.id}, orgId:{organizationId}, userId:{userId}");

            ret.success = true;
            try
            {
                if (update.id > 0)
                {
                    TagsManager tagsManager = new TagsManager(organizationId);
                    tagsManager.Update(update.id, new tags
                    {
                        name = update.name,
                        module = update.module,
                        organizationId = organizationId,
                        recordStatus = 0,
                        color = update.color,
                        type = update.type

                    });

                    ret.message = "UpdatedSuccessfully";
                }
                else
                {
                    TagsManager tagsManager = new TagsManager(organizationId);
                    CalaniEntities db = new CalaniEntities();
                    var TagNames = (from t in db.tags select t).Where(c => c.organizationId == organizationId && c.recordStatus == (int)RecordStatus.Active);
                    if (!TagNames.Select(c => c.name).Contains(update.name))
                    {
                        tagsManager.Add(new tags
                        {
                            name = update.name,
                            module = update.module,
                            organizationId = organizationId,
                            recordStatus = 0,
                            color = update.color,
                            type = update.type
                        });
                        ret.message = "SavedSuccessfully";

                    }
                    else
                    {
                        ret.message = "TagAlreadyExist";
                        ret.success = false;

                    }

                }

                return ret;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                throw;
            }
        }

        public AjaxAction UpdateTaskStatus(TaskStatusUpdate update)
        {
            AjaxAction ret = new AjaxAction();
            ret.success = true;
            try
            {

                long orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
                long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);

                _log.Info($"UpdateTaskStatus. taskId:{update.TaskId}, statusId:{update.TaskStatusId}, orgId:{orgId}, userId:{userId}");

                TasksManager tasksManager = new TasksManager(orgId);
                var task = tasksManager.Get(update.TaskId);

                var result = tasksManager.Update(update.TaskId, new tasks
                {
                    organizationId = orgId,
                    title = task.title,
                    description = task.description,
                    progress = update.Praogressval != null ? update.Praogressval : task.progress,
                    status = update.TaskStatusId != null ? update.TaskStatusId : task.status,
                    prority = task.prority,
                    dueDate = task.dueDate,
                    recordStatus = 0,
                    timeline = task.timeline,
                    ownerId = task.ownerId

                });

                ret.message = "UpdatedSuccessfully";

            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                throw;
            }
            return ret;

        }

        public AjaxAction UpdateTag(TagUpdate update)
        {
            long orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
            return UpdateTag(update, orgId);
        }

        public AjaxAction DeleteTag(long tagId)
        {

            long orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            TagsManager tagsManager = new TagsManager(orgId);
            try
            {
                CalaniEntities db = new CalaniEntities();
                Tasks.TaskTagMappingManager tagsMappingManager = new Tasks.TaskTagMappingManager();
                tagsMappingManager.DeleteByTagId(tagId);
                var tagName = (from t in db.tags select t).Where(c => c.organizationId == orgId && c.id == tagId).Select(c => c.name).FirstOrDefault();
                var s = tagsManager.DeleteByTagName(tagId);
                return new AjaxAction()
                {
                    message = "DeletedSuccessfully",
                    success = true
                };
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);

                return new AjaxAction()
                {
                    id = -1,
                    message = "Could not delete tag",
                    success = false
                };
            }
        }

        public long GetSessionUserId()
        {
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
            return userId;
        }

        public string GetTagByTagName(string tagName)
        {
            try
            {
                var organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
                CalaniEntities db = new CalaniEntities();
                var tag = (from t in db.tags select t).Where(c => c.organizationId == organizationId && c.name == tagName).Select(c => c.name).FirstOrDefault();
                return tag;
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                throw ex;
            }
            

        }
        public Dictionary<string,string> GetAllTagNames(int module)
        {
            var organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            TagsManager tagsManager = new TagsManager(organizationId);
            return tagsManager.GetTagsByType(module).ToDictionary(e => e.name, e => e.color);
        }

        public List<TasksExtended> GetTasksByTabs(enumTaskType TabName)
        {
            var organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
            TasksManager tman = new TasksManager(organizationId);
            return tman.ListTasks(userId: userId, type: TabName);
        }

        public bool PinUnPinTask(long TaskId)
        {
            var orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);

            _log.Info($"PinUnPinTask. taskId:{TaskId},  orgId:{orgId}, userId:{userId}");
            TasksManager tman = new TasksManager(orgId);
            return tman.PinUnPinTask(TaskId, userId);
        }

        public AjaxAction SendMail(SendEmail msg)
        {
            return EmailHelper.SendMail(msg, 
                Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]), 
                HttpContext.Current.User.Identity.Name, 
                Convert.ToInt64(HttpContext.Current.Session["UserId"])
            );
        }

        public AjaxAction SetUserVal(uservar v)
        {
            var orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            Membership.UserVariableManager mgr = new Membership.UserVariableManager(HttpContext.Current.User.Identity.Name, orgId);
            mgr.Save(v.name, v.value);

            _log.Info($"SetUserVal. name:{v.name},  value:{v.value}, orgId:{orgId}, userName:{HttpContext.Current.User.Identity.Name}");
            return new AjaxAction { success = true };
        }

        public AjaxAction SendProjectNote(MobileService.NotesExtended note)
        {
            AjaxAction ret = new AjaxAction();

            long orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);

            MobileService.NotesManager mgr = new MobileService.NotesManager(orgId);


            _log.Info($"SendProjectNote. jobId:{note.jobId}, message:{note.content}, orgId:{orgId}, userId:{userId}");

            ret.success = mgr.Add(new Model.notes
            {
                author = userId,
                date = DateTime.Now,
                message = note.content,
                jobId = note.jobId
            }).Success;

            return ret;

        }

        public string getVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version.ToString() + "-" + System.Configuration.ConfigurationManager.AppSettings["serverName"];
        }

        public AjaxAction WelcomeWizardChange(MobileService.WelcomeWizardChange update)
        {
            string testrx = SharedResource.Resource.GetString("WizardSample_LastName");

            AjaxAction ret = new AjaxAction();
            long orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);

            if (update.company != null)
            {
                CustomerAdmin.OrganizationManager mgr = new OrganizationManager(orgId);
                if (!String.IsNullOrWhiteSpace(update.company.city)) mgr.AdrCity = update.company.city;
                if (!String.IsNullOrWhiteSpace(update.company.name)) mgr.CompanyName = update.company.name;
                if (!String.IsNullOrWhiteSpace(update.company.phone)) mgr.Phone = update.company.phone;
                if (!String.IsNullOrWhiteSpace(update.company.street)) mgr.AdrStreet = update.company.street;
                if (!String.IsNullOrWhiteSpace(update.company.streetnb)) mgr.AdrStreetNb = update.company.streetnb;
                if (!String.IsNullOrWhiteSpace(update.company.zipcode)) mgr.AdrZipCode = update.company.zipcode;

                mgr.Save(true, false, false, false);

                ret.success = true;
            }

            if (update.employee != null)
            {
                ret.success = true;

                Contacts.EmployeesManager mgr = new Contacts.EmployeesManager(orgId);

                bool newemp = false;
                var firstemp = (from r in mgr.List() where r.type == 2 orderby r.id select r).FirstOrDefault();
                if (firstemp == null)
                {
                    firstemp = new Model.contacts();
                    firstemp.organizationId = orgId;
                    newemp = true;
                }

                if (!String.IsNullOrWhiteSpace(update.employee.email)) firstemp.primaryEmail = update.employee.email;
                if (!String.IsNullOrWhiteSpace(update.employee.firstname)) firstemp.firstName = update.employee.firstname;
                if (!String.IsNullOrWhiteSpace(update.employee.lastname)) firstemp.lastName = update.employee.lastname;
                if (!String.IsNullOrWhiteSpace(update.employee.phone)) firstemp.primaryPhone = update.employee.phone;


                if (newemp)
                {
                    if (!String.IsNullOrWhiteSpace(update.employee.email)
                        || !String.IsNullOrWhiteSpace(update.employee.firstname)
                        || !String.IsNullOrWhiteSpace(update.employee.lastname)
                        || !String.IsNullOrWhiteSpace(update.employee.phone))
                    {
                        if (!String.IsNullOrWhiteSpace(firstemp.primaryEmail) && !mgr.IsEmailFree(firstemp.primaryEmail)) firstemp.primaryEmail = null;
                        if (String.IsNullOrWhiteSpace(firstemp.primaryEmail)) firstemp.primaryEmail = Guid.NewGuid().ToString("n").Substring(5) + "@" + Guid.NewGuid().ToString("n").Substring(5) + ".ch";
                        if (String.IsNullOrWhiteSpace(firstemp.lastName)) firstemp.lastName = SharedResource.Resource.GetString("WizardSample_LastName");
                        if (String.IsNullOrWhiteSpace(firstemp.firstName)) firstemp.firstName = SharedResource.Resource.GetString("WizardSample_FirstName");
                    }
                    else
                    {
                        ret.success = false;
                        return ret; // cancel
                    }
                }


                // initials
                if (!String.IsNullOrWhiteSpace(firstemp.lastName)) firstemp.initials = firstemp.lastName;
                firstemp.initials = firstemp.initials.ToUpper();
                if (firstemp.initials.Length > 3) firstemp.initials = firstemp.initials.Substring(0, 3);

                if (!String.IsNullOrWhiteSpace(firstemp.firstName) && !String.IsNullOrWhiteSpace(firstemp.lastName))
                {
                    firstemp.initials = firstemp.firstName;
                    firstemp.initials = firstemp.initials.ToUpper().Trim();
                    if (firstemp.initials.Length > 1) firstemp.initials = firstemp.initials.Substring(0, 1);

                    firstemp.initials += firstemp.lastName;
                    firstemp.initials = firstemp.initials.ToUpper().Trim();
                    if (firstemp.initials.Length > 3) firstemp.initials = firstemp.initials.Substring(0, 3);

                    Random rdm = new Random();
                    while (!mgr.IsInitialsFree(firstemp.initials))
                    {
                        firstemp.initials = firstemp.initials.Substring(0, 2) + rdm.Next(0, 10);
                    }
                }

                // -->

                if (newemp) mgr.Add(firstemp);
                else mgr.Update(firstemp.id, firstemp);
            }

            if (update.service != null)
            {
                ret.success = true;

                CustomerAdmin.ServicesManager mgr = new CustomerAdmin.ServicesManager(orgId);

                bool newserv = false;
                var firstserv = (from r in mgr.List() orderby r.id select r).FirstOrDefault();
                if (firstserv == null)
                {
                    firstserv = new Model.services();
                    firstserv.organizationId = orgId;
                    newserv = true;
                }

                if (!String.IsNullOrWhiteSpace(update.service.code)) firstserv.internalId = update.service.code;
                if (!String.IsNullOrWhiteSpace(update.service.name)) firstserv.name = update.service.name;
                if (update.service.price != 0) firstserv.unitPrice = update.service.price;

                if (newserv)
                {
                    if (!String.IsNullOrWhiteSpace(update.service.code)
                        || !String.IsNullOrWhiteSpace(update.service.name))
                    {
                        if (!String.IsNullOrWhiteSpace(firstserv.name) && !mgr.IsFree(firstserv.internalId)) firstserv.internalId = null;
                        if (String.IsNullOrWhiteSpace(update.service.code)) firstserv.internalId = SharedResource.Resource.GetString("WizardSample_RefNr");
                        if (!mgr.IsFree(firstserv.internalId)) firstserv.internalId = Guid.NewGuid().ToString("n");
                        if (String.IsNullOrWhiteSpace(update.service.name)) firstserv.name = SharedResource.Resource.GetString("WizardSample_ItemName");
                        mgr.Add(firstserv);
                    }
                }
                else mgr.Update(firstserv.id, firstserv);
            }


            if (update.customer != null)
            {
                Calani.BusinessObjects.CustomerAdmin.ClientsManager custmgr = new Calani.BusinessObjects.CustomerAdmin.ClientsManager(orgId);
                var customers = custmgr.List();
                Model.clients firstcust = null;
                Model.contacts firstcustcontact = null;

                firstcust = (from r in customers orderby r.id select r).FirstOrDefault();
                bool firstcustadd = false;
                if (firstcust != null) firstcustcontact = (from r in firstcust.contacts orderby r.id select r).FirstOrDefault();
                bool firstcustcontactadd = false;

                // cust
                if (firstcust == null)
                {
                    firstcust = new Model.clients { organizationId = orgId };
                    firstcustadd = true;
                }
                if (!String.IsNullOrWhiteSpace(update.customer.company)) firstcust.companyName = update.customer.company;

                if (String.IsNullOrWhiteSpace(update.customer.company) && !String.IsNullOrWhiteSpace(update.customer.lastname))
                {
                    firstcust.companyName = update.customer.lastname;
                }
                if (String.IsNullOrWhiteSpace(update.customer.company) && !String.IsNullOrWhiteSpace(update.customer.lastname) && !String.IsNullOrWhiteSpace(update.customer.firstname))
                {
                    firstcust.companyName = update.customer.lastname + " " + update.customer.firstname;
                }


                if (firstcustadd)
                {
                    var res = custmgr.Add(firstcust);
                    firstcust = res.Record;
                }
                else
                {
                    custmgr.Update(firstcust.id, firstcust);
                }
                // -->


                // contact
                if (firstcustcontact == null)
                {
                    int type = Convert.ToInt32(Calani.BusinessObjects.Contacts.ContactTypeEnum.Contact_CustomerMain);
                    firstcustcontact = new Model.contacts { organizationId = orgId, clientId = firstcust.id, type = type };
                    firstcustcontactadd = true;
                }

                if (!String.IsNullOrWhiteSpace(update.customer.lastname)) firstcustcontact.lastName = update.customer.lastname;
                if (!String.IsNullOrWhiteSpace(update.customer.firstname)) firstcustcontact.firstName = update.customer.firstname;
                if (!String.IsNullOrWhiteSpace(update.customer.email)) firstcustcontact.primaryEmail = update.customer.email;

                if (!String.IsNullOrWhiteSpace(update.customer.lastname) && String.IsNullOrEmpty(firstcustcontact.primaryEmail))
                {
                    firstcustcontact.primaryEmail = Guid.NewGuid().ToString("n").Substring(5) + "@" + Guid.NewGuid().ToString("n").Substring(5) + ".ch";
                }

                Calani.BusinessObjects.Contacts.ClientContactsManager contactmgr = new Contacts.ClientContactsManager(orgId, firstcust.id);
                if (firstcustcontactadd)
                {
                    contactmgr.Add(firstcustcontact);
                }
                else
                {
                    contactmgr.Update(firstcustcontact.id, firstcustcontact);
                }
                // -->

                // address

                Calani.BusinessObjects.Contacts.ClientAddressesManager addrMgr = new Calani.BusinessObjects.Contacts.ClientAddressesManager(orgId, firstcust.id);
                if (addrMgr.List().Count == 0)
                {

                    var addr = addrMgr.Add(new Calani.BusinessObjects.Model.addresses
                    {
                        street = "Rue du client",
                        nb = "1",
                        npa = "00000",
                        city = "Ville",
                        country = "Suisse",

                        clientId = firstcust.id,
                        organizationId = orgId,

                        type = Convert.ToInt32(Calani.BusinessObjects.Contacts.AddressTypeEnum.CustomerAddress)
                    });
                }

                // -->
            }

            return ret;
        }

        public AjaxAction ExportToProjects(ExportToProjectsTask task)
        {
            AjaxAction ret = new AjaxAction();
            ret.success = true;


            long orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);

            var orgMgr = new OrganizationManager(orgId);
            orgMgr.Load();

            System.Globalization.CultureInfo c = new System.Globalization.CultureInfo("en-US");
            try
            {
                c = new System.Globalization.CultureInfo(task.culture);
            }
            catch { }

            try
            {
                Model.CalaniEntities db = new Model.CalaniEntities();
                Model.jobs sourceJob = (from r in db.jobs where r.id == task.source && r.organizationId == orgId select r).FirstOrDefault();

                var attachments = new List<attachmentsExtended>();
                var attachmentsMgr = new AttachmentsManager(orgId);

                var someAttachmentsSelected = task.attachmentsIds != null && task.attachmentsIds.Length > 0;
                if (task.includeAllAttachments || someAttachmentsSelected)
                {                    
                    attachments = attachmentsMgr.ListJobAttachments(sourceJob.id, onlyNotGenerated: true);

                    if (someAttachmentsSelected)
                    {
                        attachments = attachments.Where(a => task.attachmentsIds.Contains(a.id)).ToList();
                    }
                }         
                
                foreach (var target in task.to)
                {
                    _log.Info($"ExportToProjects. sourceId:{task.source}, targetId:{target.id}, type:{target.type}, orgId:{orgId}, userId:{userId}");

                    if (target.id == 0 && target.type == "none")
                    {
                        sourceJob.importedIn = target.id;
                        ret.id = sourceJob.id;
                        db.SaveChanges();

                        ret.message = "DeliveryNotes";
                        return ret;
                    }

                    Model.jobs targetJob = null;
                    if (target.id > 0)
                    {
                        targetJob = (from r in db.jobs where r.id == target.id && r.organizationId == orgId select r).FirstOrDefault();
                    }
                    else
                    {
                        // Create a new one for the same customer
                        targetJob = new Model.jobs();
                        targetJob.dateCreated = DateTime.Now;
                        targetJob.clientId = sourceJob.clientId;
                        targetJob.clientContact = sourceJob.clientContact;
                        targetJob.description = sourceJob.description;

                        targetJob.currency = sourceJob.currency;
                        
                        if (target.type == "invoice")
                        {
                            if (!orgMgr.EnableRefNumberInheritance || string.IsNullOrWhiteSpace(target.referenceNumber))
                            {
                                var info = new Calani.BusinessObjects.Projects.jobsExtended(false, false, true, false, orgId);
                                targetJob.internalId = info.internalId;
                            }
                            else
                            {
                                var inc = new Calani.BusinessObjects.Generic.IncrementalNumber(target.referenceNumber, addDash: true);

                                var projectMgr = new ProjectsManager(orgId);
                                while (projectMgr.IsFreeName(inc.ToString()) == false)
                                {
                                    inc.Increment();
                                }

                                targetJob.internalId = inc.ToString();
                            }
                            
                            targetJob.dateInvoicing = targetJob.dateCreated;
                        }
                        else if (target.type == "quote")
                        {
                            var info = new Calani.BusinessObjects.Projects.jobsExtended(true, false, false, false, orgId);
                            targetJob.internalId = info.internalId;
                            targetJob.subtype = "Quote";
                        }
                        else if (target.type == "deliverynote")
                        {
                            var info = new Calani.BusinessObjects.Projects.jobsExtended(false, false, false, true, orgId);
                            targetJob.internalId = info.internalId;
                            targetJob.subtype = "DeliveryNote";
                        }
                        else if (target.type == "open")
                        {
                            if (!orgMgr.EnableRefNumberInheritance || string.IsNullOrWhiteSpace(target.referenceNumber))
                            {
                                var info = new Calani.BusinessObjects.Projects.jobsExtended(false, true, false, false, orgId);
                                targetJob.internalId = info.internalId;
                            }
                            else
                            {
                                targetJob.internalId = target.referenceNumber;
                            }
                            
                            targetJob.dateOpenJob = targetJob.dateCreated;
                        }

                        targetJob.customer_reference = sourceJob.customer_reference;
                        targetJob.location_intervention = sourceJob.location_intervention;
                        targetJob.type_intervention = sourceJob.type_intervention;
                        targetJob.organizationId = orgId;
                        targetJob.type = Convert.ToInt32(ProjectTypeEnum.Billable);
                        targetJob.discount_percent = sourceJob.discount_percent;
                        targetJob.discount_type = sourceJob.discount_type;
                        targetJob.discount_percent2 = sourceJob.discount_percent2;
                        targetJob.discount_type2 = sourceJob.discount_type2;
                        targetJob.discount_percent3 = sourceJob.discount_percent3;
                        targetJob.discount_type3 = sourceJob.discount_type3;
                        targetJob.discount_percent4 = sourceJob.discount_percent4;
                        targetJob.discount_type4 = sourceJob.discount_type4;

                        foreach (var att in attachments)
                        {
                            var copy = attachmentsMgr.CloneAttachment(att);
                            targetJob.attachments.Add(copy);
                        }

                        db.jobs.Add(targetJob);
                        db.SaveChanges();                        
                    }

                    ProjectActionManager.AppendProjectLinesOnCurrentProject(db, targetJob, sourceJob.id, new System.Globalization.CultureInfo(task.culture));

                    db.SaveChanges();

                    var records = targetJob.services_jobs.Where(sj => sj.recordStatus != (int)RecordStatus.Deleted).ToList();

                    Model.currency currency = null; // can not manage currencies when importing in this version
                    ProjectItemsUpdater.UpdateTotal(targetJob, records, currency, orgMgr.RoundAmount);
                    db.SaveChanges();
                    ret.id = targetJob.id;
                    
                    if (target.type == "deliverynote" || target.type == "none")
                        ret.message = "DeliveryNotes";
                    
                    if (target.type == "invoice")
                        ret.message = "Invoices";
                    
                    if (target.type == "open")
                        ret.message = "Opens";
                    
                    if (target.type == "quote")
                        ret.message = "Quotes";

                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message, ex);
                ret.success = false;
                ret.message = ex.Message;
            }
            return ret;
        }

        public List<ProjectToImport> GetProjectsToExport(long customerid, bool deliverynotes, bool quotes, bool projects, bool invoices)
        {
            List<ProjectToImport> ret = new List<ProjectToImport>();

            long orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);

            Projects.ProjectsManager mgr = new ProjectsManager(orgId);

            if (deliverynotes)
            {
                var records = mgr.ListDeliveryNotes(customerid);
                records = (from r in records orderby r.dateQuoteSent descending select r).ToList();
                foreach (var r in records)
                {
                    ProjectToImport p = new ProjectToImport();
                    p.Id = r.id;
                    p.InternalId = r.internalId;
                    p.Description = r.description;
                    if (r.dateCreated != null) p.Date = r.dateCreated.Value.ToString("dd.MM.yyyy");
                    if (r.total != null) 
                        p.Amount = r.total.Value;

                    if (r.importedIn != null && r.importedIn > 0)
                        p.ImportedInId = r.importedIn.Value;

                    p.ImportedInInternalId = r.ImportedInInternalId;

                    if (r.dateQuoteSent == null) 
                        ret.Add(p);
                }
            }

            if (quotes)
            {
                var records = mgr.ListQuotes(customerid);
                records = (from r in records orderby r.dateQuoteSent descending select r).ToList();
                foreach (var r in records)
                {
                    ProjectToImport p = new ProjectToImport();
                    p.Id = r.id;
                    p.InternalId = r.internalId;
                    p.Description = r.description;
                    if (r.dateQuoteSent != null) p.Date = r.dateQuoteSent.Value.ToString("dd.MM.yyyy");
                    if (r.total != null) p.Amount = r.total.Value;
                    
                    if (r.importedIn != null && r.importedIn > 0)
                        p.ImportedInId = r.importedIn.Value;

                    p.ImportedInInternalId = r.ImportedInInternalId;

                    if (r.dateQuoteAccepted == null && r.dateQuoteRefused == null) ret.Add(p);
                }
            }

            if (projects)
            {
                var records = mgr.ListOpens(customerid);
                records = (from r in records orderby r.dateOpenJob descending select r).ToList();
                foreach (var r in records)
                {
                    ProjectToImport p = new ProjectToImport();
                    p.Id = r.id;
                    p.InternalId = r.internalId;
                    p.Description = r.description;
                    if (r.dateOpenJob != null) p.Date = r.dateOpenJob.Value.ToString("dd.MM.yyyy");
                    if (r.total != null) p.Amount = r.total.Value;
                    if (r.importedIn != null && r.importedIn > 0)
                        p.ImportedInId = r.importedIn.Value;

                    p.ImportedInInternalId = r.ImportedInInternalId;


                    if (r.dateJobDone == null) ret.Add(p);
                }
            }

            if (invoices)
            {
                var records = mgr.ListInvoices(orgId, customerid);
                records = (from r in records orderby r.dateInvoicing descending select r).ToList();
                foreach (var r in records)
                {
                    ProjectToImport p = new ProjectToImport();
                    p.Id = r.id;
                    p.InternalId = r.internalId;
                    p.Description = r.description;
                    if (r.dateInvoicing != null) p.Date = r.dateInvoicing.Value.ToString("dd.MM.yyyy");
                    if (r.total != null) p.Amount = r.total.Value;
                    if (r.importedIn != null && r.importedIn > 0) 
                        p.ImportedInId = r.importedIn.Value;
                    p.ImportedInInternalId = r.ImportedInInternalId;


                    if (r.dateInvoicingOpen == null) 
                        ret.Add(p);
                }
            }

            return ret;
        }

        public List<TutorialModel> GetWebTutorials()
        {
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);

            var mngr = new TutorialsManager();
            var list = mngr.GeTutorialsList(userId, TutorialsType.Web, true);
            
            return list;
        }


        public TutorialModel SaveTutorial(TutorialModel model)
        {
            long orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);

            
            var mngr = new TutorialsManager();
            var tid = mngr.SaveTutorial(userId,orgId, model);

            _log.Info($"SaveTutorial. tid:{tid}, type:{model.Type}, orgId:{orgId}, userId:{userId}");
            return mngr.GeTutorial(userId, tid, model.Type, model.MinVersion.ToString());
        }

        public TutorialModel GetTutorial(long id, TutorialsType type = TutorialsType.Web)
        {
            if(id < 1)
                return new TutorialModel();

            long orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);

            _log.Info($"GetTutorial. id:{id},type:{type}, orgId:{orgId}, userId:{userId}");
            var mngr = new TutorialsManager();

            var model = mngr.GeTutorial(userId, id, type);


            return model;

        }

        #region SystemAdmin

        public OrganizationsList GetOrganizationsList(string groupBy, bool groupDesc, int itemsPerPage, bool multiSort, bool mustSort, int page, string sortBy, bool sortDesc, string search, CalendarFilterItemType type = CalendarFilterItemType.CurrentYear)
        {

            var staff = HttpContext.Current.Session["staff"].ToString();

            if (staff != "staff")
                return new OrganizationsList();


            var mgr = new OrganizationsManager();

            var item = GetCalendarFilterItems().FirstOrDefault(i => i.value == (int)type);

            OrganizationsList model = mgr.GetOrganizations(groupBy, groupDesc, itemsPerPage, multiSort, mustSort, page, sortBy, sortDesc, search, item);


            return model;
        }


        public OrganizationInfo GetOrganization(long id)
        {

            var staff = HttpContext.Current.Session["staff"].ToString();

            if (staff != "staff")
                return new OrganizationInfo();


            var mgr = new OrganizationsManager();


            var model = mgr.GetOrganization(id);


            return model;
        }


        public OrganizationInfo SaveOrganization(OrganizationInfo model)
        {

            var staff = HttpContext.Current.Session["staff"].ToString();

            if (staff != "staff")
                return new OrganizationInfo();

            _log.Info($"SaveOrganization. id:{model.id}");
            var mgr = new OrganizationsManager();


            var res = mgr.SaveOrganization(model);


            return mgr.GetOrganization(res);
        }

        #endregion

        private string GetQuarterLabel(int i)
        {
            return SharedResource.Resource.GetString("QuarterFormat")?.Replace("{0}", i.ToString());
        }

        public int GetQuarter(DateTime dt)
        {
            return (dt.Month - 1) / 3 + 1;
        }

        public List<CalendarFilterItem> GetCalendarFilterItems()
        {
            var items = new List<CalendarFilterItem>();

            int fiscYearDay = 1; // TODO a lire dans les preferences de la compagnie du user
            int fiscYearMonth = 1;  // TODO a lire dans les preferences de la compagnie du user
            int historyYears = 5; // calculer le nombre d'années + 1 depuis l'inscription de la compagnie du user (ou la date du plus vieux job)

            long orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);

            if (orgId >= 0)
            {
                var db = new CalaniEntities();
                DateTime? fiscYearStart = (from r in db.organizations where r.id == orgId select r.financialYearStart).FirstOrDefault();
                if (fiscYearStart != null)
                {
                    fiscYearDay = fiscYearStart.Value.Day;
                    fiscYearMonth = fiscYearStart.Value.Month;
                }
            }

            var now = DateTime.Now;
            DateTime thisMonthStart = new DateTime(now.Year, now.Month, 1);
            DateTime prevMonthStart = thisMonthStart.AddMonths(-1);
            int quarterNumber = (now.Month - 1) / 3 + 1;
            DateTime thisQuarterStart = new DateTime(now.Year, (quarterNumber - 1) * 3 + 1, 1);

            DateTime prevQuarterStart = thisQuarterStart.AddMonths(-3);
            DateTime thisYearStart = new DateTime(now.Year, 1, 1);
            DateTime prevYearStart = new DateTime(now.Year - 1, 1, 1);

            items.Add(new CalendarFilterItem(SharedResource.Resource.GetString("Calendar"),-2));


            items.Add(new CalendarFilterItem(CalendarFilterItemType.AllPeriods,SharedResource.Resource.GetString("All_periods"),DateTime.MinValue, DateTime.MaxValue));

            items.Add(new CalendarFilterItem(CalendarFilterItemType.CurrentMonth, SharedResource.Resource.GetString("Current_month") + " (" + thisMonthStart.ToString("MMM") + ")",
                thisMonthStart, new DateTime(now.Year, now.Month, DateTime.DaysInMonth(now.Year, now.Month))));

            
            items.Add(new CalendarFilterItem(CalendarFilterItemType.LastMonth, SharedResource.Resource.GetString("Last_month") + " (" + prevMonthStart.ToString("MMM") + ")",
                prevMonthStart, thisMonthStart.AddMinutes(-1)));


            items.Add(new CalendarFilterItem(CalendarFilterItemType.CurrentQuarter, SharedResource.Resource.GetString("Current_quarter") + " (" + thisQuarterStart.Year + " " + GetQuarterLabel(GetQuarter(thisQuarterStart)) + ")",
                thisQuarterStart, thisQuarterStart.AddMonths(3)));
            
            
            items.Add(new CalendarFilterItem(CalendarFilterItemType.LastQuarter, SharedResource.Resource.GetString("Last_quarter") + " (" + prevQuarterStart.Year + " " + GetQuarterLabel(GetQuarter(prevQuarterStart)) + ")",
                prevQuarterStart, prevQuarterStart.AddMonths(3)));



            if (quarterNumber > 3)//Second Quarter
            {
                DateTime secondQuarterStart = new DateTime(now.Year, (2 - 1) * 3 + 1, 1);

                items.Add(new CalendarFilterItem(CalendarFilterItemType.SecondQuarter, SharedResource.Resource.GetString("Quarter") + " (" + secondQuarterStart.Year + " " + GetQuarterLabel(2) + ")",
                    secondQuarterStart, secondQuarterStart.AddMonths(3)));
            }

            if (quarterNumber > 2)//First Quarter
            {
                DateTime firstQuarterStart = new DateTime(now.Year, 1, 1);

                items.Add(new CalendarFilterItem(CalendarFilterItemType.FirstQuarter, SharedResource.Resource.GetString("Quarter") + " (" + firstQuarterStart.Year + " " + GetQuarterLabel(1) + ")",
                    firstQuarterStart, firstQuarterStart.AddMonths(3)));
            }
            
            items.Add(new CalendarFilterItem(CalendarFilterItemType.CurrentYear, SharedResource.Resource.GetString("Current_calendar_year") + " (" + thisYearStart.Year + ")", thisYearStart, thisYearStart.AddYears(1)));

            items.Add(new CalendarFilterItem(CalendarFilterItemType.LastYear, SharedResource.Resource.GetString("Last_calendar_year") + " (" + prevYearStart.Year + ")", prevYearStart, prevYearStart.AddYears(1)));



            items.Add(new CalendarFilterItem(SharedResource.Resource.GetString("Financial_years"),-3));

            for (int i = 0; i < historyYears; i++)
            {
                DateTime from = new DateTime(DateTime.Now.Year - i, fiscYearMonth, fiscYearDay);
                DateTime to = from.AddYears(1);
                string label = "";
                if (i == 0) 
                    label = " (" + SharedResource.Resource.GetString("current") + ")";
                items.Add(new CalendarFilterItem(CalendarFilterItemType.Years0 + i, from.Year + label, from, to));

            }


            return items;

        }

        public AjaxAction MarkTutorialsAsSeen(TutorialsType type = TutorialsType.Web)
        {
            AjaxAction ret = new AjaxAction();
            ret.success = true;

            long organizationId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);

            var mngr = new TutorialsManager();

            mngr.FinishAllTutorials(userId, type);

            return ret;
        }

        public AjaxAction FinishTutorial(long tid)
        {
            AjaxAction ret = new AjaxAction();
            ret.success = true;

            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
            long orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            var mngr = new TutorialsManager();

            _log.Info($"FinishTutorial. tid:{tid}, orgId:{orgId}, userId:{userId}");
            mngr.FinishTutorial(userId, tid);

            return ret;
        }
        
        public List<ProjectToImport> GetProjectsToImport(long customerid, bool deliverynotes, bool quotes, bool projects, bool invoices)
        {
            List<ProjectToImport> ret1 = new List<ProjectToImport>();
            List<ProjectToImport> ret2 = new List<ProjectToImport>();
            long orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);

            Projects.ProjectsManager mgr = new ProjectsManager(orgId);

            if (deliverynotes)
            {
                var records = mgr.ListDeliveryNotes(customerid, includeVisits: true).ToList();
                foreach (var r in records)
                {
                    ProjectToImport p = new ProjectToImport();
                    p.Id = r.id;
                    p.InternalId = r.internalId;
                    p.Description = r.description;
                    if (r.visits != null && r.visits.Any() && r.visits.First().dateStart.HasValue) p.Date = r.visits.First().dateStart.Value.ToString("dd.MM.yyyy", CultureInfo.InvariantCulture);
                    if (r.total != null) p.Amount = r.total.Value;
                    if (r.importedIn != null && r.importedIn > 0) 
                        p.ImportedInId = r.importedIn.Value;
                    p.ImportedInInternalId = r.ImportedInInternalId;

                    if (p.ImportedInId == 0) ret1.Add(p);
                    else ret2.Add(p);
                }

                ret1 = ret1.OrderByDescending(p => string.IsNullOrEmpty(p.Date) ? new DateTime() : DateTime.ParseExact(p.Date, "dd.MM.yyyy", CultureInfo.InvariantCulture)).ToList();
                ret2 = ret2.OrderByDescending(p => string.IsNullOrEmpty(p.Date) ? new DateTime() : DateTime.ParseExact(p.Date, "dd.MM.yyyy", CultureInfo.InvariantCulture)).ToList();
            }

            if (quotes)
            {
                var records = mgr.ListQuotes(customerid);
                records = (from r in records orderby r.dateQuoteSent descending select r).ToList();
                foreach (var r in records)
                {
                    ProjectToImport p = new ProjectToImport();
                    p.Id = r.id;
                    p.InternalId = r.internalId;
                    p.Description = r.description;

                    if (r.dateQuoteSent != null)
                        p.Date = r.dateQuoteSent.Value.ToString("dd.MM.yyyy");
                    
                    if (r.total != null)
                        p.Amount = r.total.Value;
                    
                    if (r.importedIn != null && r.importedIn > 0)
                        p.ImportedInId = r.importedIn.Value;
                    
                    p.ImportedInInternalId = r.ImportedInInternalId;

                    if (p.ImportedInId == 0)
                        ret1.Add(p);
                    else
                        ret2.Add(p);
                }
            }

            if (projects)
            {
                var records = mgr.ListOpens(customerid);
                records = (from r in records orderby r.dateOpenJob descending select r).ToList();
                foreach (var r in records)
                {
                    ProjectToImport p = new ProjectToImport();
                    p.Id = r.id;
                    p.InternalId = r.internalId;
                    p.Description = r.description;
                    if (r.dateOpenJob != null) p.Date = r.dateOpenJob.Value.ToString("dd.MM.yyyy");
                    if (r.total != null) p.Amount = r.total.Value;
                    if (r.importedIn != null && r.importedIn > 0) p.ImportedInId = r.importedIn.Value;
                    p.ImportedInInternalId = r.ImportedInInternalId;


                    if (p.ImportedInId == 0) ret1.Add(p);
                    else ret2.Add(p);
                }
            }

            if (invoices)
            {
                var records = mgr.ListInvoices(orgId, customerid);
                records = (from r in records orderby r.dateInvoicing descending select r).ToList();
                foreach (var r in records)
                {
                    ProjectToImport p = new ProjectToImport();
                    p.Id = r.id;
                    p.InternalId = r.internalId;
                    p.Description = r.description;
                    if (r.dateInvoicing != null) p.Date = r.dateInvoicing.Value.ToString("dd.MM.yyyy");
                    if (r.total != null) p.Amount = r.total.Value;
                    if (r.importedIn != null && r.importedIn > 0) p.ImportedInId = r.importedIn.Value;
                    p.ImportedInInternalId = r.ImportedInInternalId;
                   

                    if (p.ImportedInId == 0) ret1.Add(p);
                    else ret2.Add(p);
                }
            }

            ret2 = (from r in ret2 select r).Take(5).ToList(); // we only get the last 5 alredy imported items

            ret1.AddRange(ret2);

            return ret1;
        }

        public WorkReportModel GetWorkReport(long pid, long rid)
        {
            long orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);

            var orgMgr = new OrganizationManager(orgId);
            orgMgr.Load();

            _log.Info($"GetReportDetails. rid:{rid}, pid:{pid}, orgId:{orgId}, userId:{userId}");

            var mngr = new WorkReportsManager(orgId);
            var model = mngr.GetReport(rid, pid, orgMgr.CompanyName);


            return model;

        }

        public long SaveWorkReport(WorkReportModel model)
        {
            long orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);
            
            _log.Info($"SaveWorkReport. rid:{model.Id}, orgId:{orgId}, userId:{userId}, status:{model.Status}");


            var mngr = new WorkReportsManager(orgId);
            var tid = mngr.SaveReport(userId, orgId, model);


            return tid;
        }

        public AjaxAction DeleteWorkReport(long id)
        {
            AjaxAction ret = new AjaxAction();
            ret.success = true;
            long orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);

            _log.Info($"DeleteWorkReport. rid:{id}, orgId:{orgId}, userId:{userId}");

            var mngr = new WorkReportsManager(orgId);
            mngr.DeleteReport(userId, orgId, id);
            return ret;

        }
        
        public AjaxAction GetHolidayIfExists(DateTime date)
        {
            var result = new AjaxAction { success = false, data = null };
            var orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            var db = new CalaniEntities();

            var holidayList = db.calendars_rec
                .Where(x => x.name == "Férié" && x.calendars.organization_Id == orgId && x.recordStatus != -1)
                .Include(x => x.calendars)
                .ToList();
            
            var holiday = holidayList.FirstOrDefault(x =>
                x.start_date != null && x.start_date.Value.Date == date.Date);
            
            if (holiday == null) return result;
            
            result.data = JsonConvert.SerializeObject(new 
            {
                startDate = holiday.start_date,
                endDate = holiday.end_date
            });
            
            result.success = true;
            return result;
        }
        
        public AjaxAction GetHolidayDataForAbsence(DateTime startDate, DateTime endDate)
        {
            var result = new AjaxAction { success = false, data = null };
            var orgId = Convert.ToInt64(HttpContext.Current.Session["UserOrganizationId"]);
            var db = new CalaniEntities();

            var holidayList = db.calendars_rec
                .Where(x => x.name == "Férié" && x.calendars.organization_Id == orgId 
                    && x.recordStatus != -1 
                    && x.calendars.recordStatus != -1)
                .Include(x => x.calendars)
                .ToList();

            var holidays = holidayList.Where(x =>
                x.start_date != null && x.start_date.Value.Date >= startDate.Date
                && x.end_date != null && x.end_date.Value.Date <= endDate.Date).ToList();
            
            if (holidays.Any())
            {
                var days = 0;
                foreach (var hol in holidays)
                {
                    if (hol.start_date.Value.DayOfWeek == DayOfWeek.Saturday 
                        || hol.start_date.Value.DayOfWeek == DayOfWeek.Sunday) continue;
                    days++;
                }

                var dayDuration = db.organizations
                    .FirstOrDefault(x => x.id == orgId)?.absence_day_duration_minutes ?? 480;
                result.data = JsonConvert.SerializeObject(new {days = days, hours = days * dayDuration / 60});
                result.success = true;
            }
            return result;
        }

        public AjaxAction GetNextExecutionDate(RecurringInvoiceTime model)
        {
            var result = new AjaxAction { success = false, data = null };

            try
            {
                var date = TimeManager.GetNextDate(model);
                result.data = date.HasValue ? date.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : string.Empty;
            }
            catch (Exception e)
            {
                result.message = e.Message;
                result.success = false;
            }
            finally
            {
                result.success = true;
            }
            
            return result;
        }
    }
}
