﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Newtonsoft.Json;

namespace Calani.BusinessObjects.AjaxService
{
   public class CalendarFilterItem
    {
       

        public CalendarFilterItem()
        {

        }
        public CalendarFilterItem(CalendarFilterItemType type, string text, DateTime start, DateTime end)
        {
            this.text = text;
            this.value = (int)type;
            
            this.start = start;//.ToString("O");
            this.end = end; //.ToString("O");

        }

        public CalendarFilterItem(string header, int val)
        {
                this.text = header;
                this.disabled = true;
                this.value = val;
        }

        public String text { get; set; }

        [JsonIgnore] internal DateTime start { get; set; }

        [JsonIgnore] internal DateTime end { get; set; }


        public int value { get; set; }


        public  bool disabled { get; set; }
        public bool divider { get; set; }

        public String header { get; set; }

        
    }

  
   public enum CalendarFilterItemType
   {
       Years = - 1,
       AllPeriods = 0,
       CurrentMonth = 1,
       LastMonth = 2,
       CurrentQuarter = 3,
       LastQuarter = 4,
       FirstQuarter = 5,
       SecondQuarter = 6,
        CurrentYear = 7,
       LastYear = 8,
       Years0 = 9,
       Years_1 = 10,
       Years_2 = 11,
       Years_3 = 12,
       Years_4 = 13,
    }

   public static class CalendarFilterItemExtentions
    {
       

    }
}
