﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.AjaxService
{
    public class SendEmail
    {
        public string subject { get; set; }
        public long toContact { get; set; }
        public string toEmail { get; set; }
        public string body { get; set; }
        public List<long> attachments { get; set; }
        //public bool includeDocument { get; set; }

    }
}
