﻿using Calani.BusinessObjects.Attachments;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.AjaxService
{
	public class TaskActivityMessage: task_activity
	{
		public string UserFirstName { get; set; }
		public string UserLastName { get; set; }
		public string CodeName { get; set; }
		public bool IsOwned { get; set; }
		public bool IsAttachment { get; set; }
		public long? AttachmentId { get; set; }
		public string AttachmentName { get; set; }
		public int? AttachmentType { get; set; }
		public string AttachmentMimeType { get; set; }

        public string AttachmentFile { get; set; }
        public string MyProperty { get; set; }
        public string AttachmentPath { get; set; }

        public string ImageData { get; set; }
        public string Icon
        {
            get
            {
                if (AttachmentType != null)
                {
                    AttachmentTypeEnum t = (AttachmentTypeEnum)AttachmentType;
                    if (t.ToString().Contains("Pdf"))
                    {
                        return "icon-file-pdf";
                    }
                    else if (t.ToString().Contains("Xlsx"))
                    {
                        return "icon-file-excel";
                    }
                    else if (t.ToString().Contains("Docx"))
                    {
                        return "icon-file-word";
                    }
                }
                return "icon-image3";
            }
            set { this.Icon = value; }
        }
        public string NameWithExtension
        {
            get
            {
                string name = AttachmentName;
                string extension = "bin";
                if (AttachmentType != null)
                {
                    AttachmentTypeEnum t = (AttachmentTypeEnum)AttachmentType;
                    if (t.ToString().Contains("Xlsx"))
                    {
                        extension = "xlsx";
                    }
                    else if (t.ToString().Contains("doc"))
                    {
                        extension = "doc";
                    }
                    else if (t.ToString().Contains("Docx"))
                    {
                        extension = "docx";
                    }
                    else if (t.ToString().Contains("ppt"))
                    {
                        extension = "ppt";
                    }
                    else if (t.ToString().Contains("Pdf"))
                    {
                        extension = "pdf";
                    }
                    else if (t.ToString().Contains("Jpeg"))
                    {
                        extension = "jpg";
                    }
                    else if (t.ToString().Contains("Png"))
                    {
                        extension = "png";
                    }
                    else if (t.ToString().Contains("Gif"))
                    {
                        extension = "gif";
                    }
                    else if (t.ToString().Contains("mp4"))
                    {
                        extension = "mp4";
                    }
                    else if (t.ToString().Contains("avi"))
                    {
                        extension = "avi";
                    }
                    else if (t.ToString().Contains("mov"))
                    {
                        extension = "mov";
                    }
                }
                return name + "." + extension;
            }
            set { this.Icon = value; }
        }
        public string FileNameNoExtension { get; set; }
    }
}
