﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.AjaxService
{
    public class SimpleItem
    {
        public string id { get; set; }
        public string text { get; set; }
    }


    public class ClientProperties
    {
        public List<SimpleItem> Contacts { get; set; }
        public List<SimpleItem> Addresses { get; set; }
        public List<SimpleItem> Jobs { get; set; }
    }

    public class calendarRequest
    {
        public string start { get; set; }
        public string end { get; set; }
        public long? jobId { get; set;  }
        public long? visitType { get; set; }
        public List<calendarResource> resources { get; set; }
        public string cultureCode { get; set; }
        public bool excludeAbsenceRequests { get; set; }
    }

    public class calendarResource
    {
        public string id { get; set; }
        public string color { get; set; }
    }

    public class visitToUpdate
    {
        public string id { get; set; }
        public string type { get; set; }
        public string clientId { get; set; }
        public string jobId { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public string resources { get; set; }
        public string description { get; set; }
        public string culture { get; set; }
        public string status { get; set; }
        public string repeatGroupId { get; set; }
        public int numrepeat { get; set; }
        public string repeatdays { get; set; }
        public bool applyToAllRepeat { get; set;  }
        public long internalServiceId { get; set; }
        
    }

    public class timeSheetSelectableTimeQuery
    {
        public int year { get; set; }
        public int week { get; set; }
        public long employee { get; set; }
        public string culture { get; set; }
    }

    public class uservar
    {
        public string name { get; set; }
        public string value { get; set; }
    }
}
