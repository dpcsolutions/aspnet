﻿using System;
using System.Data.Entity.Migrations;
using System.Linq;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.AjaxService
{
    public static class EmailSendingValidationManager
    {
        public static bool ValidateEmailLimit(long orgId)
        {
            var orgmgr = new OrganizationManager(orgId);
            orgmgr.Load();
        
            var db = new CalaniEntities();
            var currentDay = DateTime.Now.Date;
        
            var mailQuota = db.mailsquota.FirstOrDefault(x =>
                x.organisation_id == orgId
                && x.date == currentDay
            );
        
            if (mailQuota == null) {
                return orgmgr.DailyMailsLimit >= 1;
            }

            mailQuota.mails_sent += 1;
            return orgmgr.DailyMailsLimit >= mailQuota.mails_sent;
        }

        public static void CountEmailSent(long organizationId)
        {
            var db = new CalaniEntities();
            var currentDay = DateTime.Now.Date;
            var mailsQuotaRepository = db.mailsquota;
            
            var mailQuota = mailsQuotaRepository.FirstOrDefault(x =>
                x.organisation_id == organizationId && x.date == currentDay);
            
            if (mailQuota == null)
            {
                mailsQuotaRepository.Add(new mailsquota
                {
                    organisation_id = organizationId,
                    date = DateTime.Now.Date,
                    mails_sent = 1
                });
            }
            else
            {
                mailQuota.mails_sent += 1;
                mailsQuotaRepository.AddOrUpdate(mailQuota);
            }
            db.SaveChanges();
        }
    }
}