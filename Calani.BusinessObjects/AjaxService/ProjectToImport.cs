﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.AjaxService
{
    public class ProjectToImport
    {
        public long Id { get; set; }
        public string InternalId { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public double Amount { get; set; }
        public string Type { get; set; }
        public long ImportedInId { get; set; }
        public string ImportedInInternalId { get; set; }

    }
}
