﻿using Calani.BusinessObjects.Admin;
using Calani.BusinessObjects.AjaxService.Models;
using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Projects;
using Calani.BusinessObjects.Projects.Models;
using Calani.BusinessObjects.Scheduler.Models;
using Calani.BusinessObjects.TimeManagement.Models;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using Calani.BusinessObjects.Hangfire;

namespace Calani.BusinessObjects.AjaxService
{
    [ServiceContract(SessionMode = SessionMode.Allowed)]
    public interface IAjaxService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction AddOrUpdateResourceAllocation(ResourceAllocationModel model);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction DeleteResourceAllocation(long id);
        
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction CreateDeliveryNoteFromService(List<JobDeliveryNoteCreateModel> models);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction RestoreDeletedResourceAllocation(long id);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction ListResourceAllocations(DateTime dtFrom, DateTime dtTo, string locale);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction DeleteAttachment(ProjectAttachment pa);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        DataTableModel GetAbsencesReports(long startTicks, long endTicks);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction GetVacationReportForEmployee(long employeeId, long startTicks, long endTicks);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction MarkTutorialsAsSeen(TutorialsType type);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        string getVersion();

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        ClientProperties GetClientProperties(long id);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        JobsCollectionModel<InvoiceModel> ListInvoices(long startTicks, long endTicks);
        
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        JobsCollectionModel<InvoiceModel> ListRecurringInvoices();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        JobsCollectionModel<DeliveryNoteModel> ListDeliveryNotes(long startTicks, long endTicks, long assigneeId = 0, long creatorId = 0);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        JobsCollectionModel<QuoteModel> ListQuotes(long startTicks, long endTicks, long assigneeId = 0);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction UpdateProject(ProjectUpdate update);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction UpdateProjectColors();

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction UpdateTask(TaskUpdate update);


        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction UpdateTag(TagUpdate update);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction DeleteTag(long tagId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        Dictionary<string, string> GetAllTagNames(int module);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        GetTaskWithMessages GetTaskDetails(long TaskId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        GetAllTags GetAllTagsForTask();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        bool PinUnPinTask(long TaskId);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        string GetTagByTagName(string tagName);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        long GetSessionUserId();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction SaveTaskActivity(long taskId, string Message);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        TimeSheetsCollectionModel ListTimeSheets(long startTicks, long endTicks, long employeeId = 0);
        
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        List<Tuple<string, string>> GetTimeSheetComments(long timeSheetId, string comment);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        TimeSheetUpdateAction[] ValidateMultipleTimesheets(string[] timesheetsIds);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        TimeSheetUpdateAction UpdateTimeSheet(TimeSheetUpdate update);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        List<TimeManagement.SelectableTimeItem> GetTimeSheetSelectableTimeItems(timeSheetSelectableTimeQuery q);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        TimeSheetsRecordsCollectionModel ListTimeSheetsRecordsForInvoiceTime(long startTicks, long endTicks);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction CancelTimeSheetRecordImport(string stringDelimetedIds);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        List<long> ImportTimeSheetRecords(TimeSheetRecordsInvoiceModel[] models);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction SendMail(SendEmail msg);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        List<visitsExtended> GetVisits(calendarRequest c);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        List<jobExtended> GetJobsForClient(long clientId);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction DeleteVisit(visitToUpdate toUpdate);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction UpdateVisit(visitToUpdate v);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction UpdateTaskStatus(TaskStatusUpdate update);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction SetUserVal(uservar v);


        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction SendProjectNote(MobileService.NotesExtended note);


        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction WelcomeWizardChange(MobileService.WelcomeWizardChange update);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        List<ProjectToImport> GetProjectsToImport(long customerid, bool deliverynotes, bool quotes, bool projects, bool invoices);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        List<ProjectToImport> GetProjectsToExport(long customerid, bool deliverynotes, bool quotes, bool projects, bool invoices);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction ExportToProjects(ExportToProjectsTask task);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        List<TutorialModel> GetWebTutorials();

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction FinishTutorial(long tid);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        TutorialModel SaveTutorial(TutorialModel model);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        OrganizationInfo SaveOrganization(OrganizationInfo model);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        OrganizationsList GetOrganizationsList(string groupBy, bool groupDesc, int itemsPerPage, bool multiSort, bool mustSort, int page, string sortBy, bool sortDesc, string search, CalendarFilterItemType type = CalendarFilterItemType.CurrentYear);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        OrganizationInfo GetOrganization(long id);
        
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        List<CalendarFilterItem> GetCalendarFilterItems();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        WorkReportModel GetWorkReport(long pid, long rid);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        TutorialModel GetTutorial(long id, TutorialsType type);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        long SaveWorkReport(WorkReportModel model);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction DeleteWorkReport(long id);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        ContractDto GetActiveContract(long contactId);
        
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction GetHolidayIfExists(DateTime date);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction GetHolidayDataForAbsence(DateTime startDate, DateTime endDate);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        AjaxAction GetNextExecutionDate(RecurringInvoiceTime model);
    }
}
