﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.AjaxService
{
    public class TaskStatusUpdate
    {
        public int TaskId { get; set; }

        public int? TaskStatusId { get; set; }

        public int? Praogressval { get; set; }
    }
}
