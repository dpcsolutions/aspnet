﻿using System;

namespace Calani.BusinessObjects.AjaxService
{
    public class RecurringUpdate
    {
        public long id { get; set; }
        public bool isActive { get; set; }
        
        public int frequencyCount { get; set; }
        public int specificDayCount { get; set; }
        public int frequencyType { get; set; }
        public int daysInAdvance { get; set; }
        
        public bool isLastDay { get; set; }
        public bool isSpecificDay { get; set; }
        
        public string nextDueDate { get; set; }
        public string nextExecutionDate { get; set; }
        
        public bool isUntilNotice { get; set; }
        public bool isSpecificMonth { get; set; }
        public string lastDueDate { get; set; }
        
        public int invoiceSendingType { get; set; }
    }
}