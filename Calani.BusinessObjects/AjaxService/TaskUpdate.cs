﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.AjaxService
{
    public class TaskUpdate
    {
        public long id { get; set; }
        public string action { get; set; }
        public string title { get; set; }
        public string dueDate { get; set; }
        public string description { get; set; }
        public long? owner { get; set; }
        public int? priority { get; set; }
        public int? status { get; set; }
        public int? progress { get; set; }

        public bool? isPinned { get; set; }
        public List<string> tags { get; set; }

        public List<string> assignTo  { get; set; }
        public string timeline { get; set; }
    }

}