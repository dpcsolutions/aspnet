﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.AjaxService
{
    public class ProjectAttachment
    {
        public long ProjectId { get; set; }
        public long AttachmentId { get; set; }
    }
}
