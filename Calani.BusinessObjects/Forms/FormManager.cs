﻿using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.MobileService2;
using Calani.BusinessObjects.MobileService2.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using log4net;
using System.Reflection;

namespace Calani.BusinessObjects.Forms
{
    public class FormsDataManager : Generic.SmartCollectionManager<Model.form_data, long>
    {
        long EmployeeId = -1;
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public FormsDataManager(long organisationId, long employeeId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organisationId;
            EmployeeId = employeeId;
        }


        internal override DbSet<Model.form_data> GetDbSet(Model.CalaniEntities db)
        {
            return db.form_data;
        }

        internal override SmartQuery<form_data> GetById(long id)
        {
            var q = GetBaseQuery();
            q.Query = (from r in q.Query orderby r.id select r);
            return q;
        }


        private void ExportSILFormData(DateTime dt, long employeeId, long organizationId, string exportPath)
        {

            try
            {
                if (!String.IsNullOrEmpty(exportPath))
                {
                    Membership.LoginManager mgrUsr = new Membership.LoginManager();
                    string email = mgrUsr.GetEmail(employeeId);

                    // export the timesheet
                    if (!System.IO.Directory.Exists(exportPath))
                    {
                        System.IO.Directory.CreateDirectory(exportPath);
                    }

                    var dateOfMonday = Tools.GetFirstDateOfWeek(dt.Year, Tools.GetWeekNumber(dt));
                    var fileName = dateOfMonday.ToString("yyyy_MM_dd");
                    fileName = System.IO.Path.Combine(exportPath, fileName + ".xml");
                    StructTime data = null;

                    if (System.IO.File.Exists(fileName))
                    {
                        data = XMLSerializer.Deserialize<StructTime>(fileName);
                    }
                    else
                    {
                        data = new StructTime()
                        {
                            autres = new List<double>().ToArray(),
                            datesstr = new List<DateTime>().ToArray(),
                            debours = new List<bool>().ToArray(),
                            piquets = new List<bool>().ToArray(),
                            dtFirstDayOfWeek = dateOfMonday,
                            email = email
                        };
                    }

                    var oldData = XMLSerializer.Serialize(data);

                    if (data.dtFirstDayOfWeek == dateOfMonday && data.email == email)
                    {
                        //export debours
                        var fm = new Forms.FormsDataManager(organizationId, employeeId);
                        var debours = new List<bool>();
                        var piquets = new List<bool>();
                        var autres = new List<double>();

                        for (var i = 0; i < 6; i++)
                        {
                            var d = data.dtFirstDayOfWeek.AddDays(i);
                            var formDatas = fm.ListForDate(d);
                            if (formDatas.Count > 1)
                            {
                                _log.Debug("Found more than one form data for date " + data.dtFirstDayOfWeek.AddDays(i) + " user " + employeeId + " ignoring date!");
                            }
                            else
                            {
                                if (formDatas.Count == 0)
                                {
                                    debours.Add(false);
                                    piquets.Add(false);
                                    autres.Add(0);
                                }
                                else
                                {
                                    var formData = formDatas[0];
                                    // parse data - very ugly need to rework on that
                                    var jsonElements = formData.data.Substring(1, formData.data.Length - 2).Split(',');
                                    foreach (var element in jsonElements)
                                    {
                                        var jsonElement = element.Split(':');
                                        if (jsonElement.Length != 2)
                                        {
                                            _log.Debug("Found wrong formated JSON for  " + data.dtFirstDayOfWeek.AddDays(i) + " user " + employeeId + " ignoring data!");
                                        }
                                        else
                                        {
                                            var name = jsonElement[0].Replace('"', ' ').Trim();
                                            var value = jsonElement[1].Trim();

                                            switch (name)
                                            {
                                                case "expense":
                                                    debours.Add(value == "true");
                                                    break;
                                                case "onDuty":
                                                    piquets.Add(value == "true");
                                                    break;
                                                case "other":
                                                    if (Double.TryParse(value, out double val))
                                                        autres.Add(val);
                                                    else
                                                    {
                                                        _log.Debug("Could not parse débours for  " + data.dtFirstDayOfWeek.AddDays(i) + " user " + employeeId + " autre: " + value + "ignoring data!");
                                                        autres.Add(0);
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (debours.Count > 0)
                            data.debours = debours.ToArray();

                        if (piquets.Count > 0)
                            data.piquets = piquets.ToArray();

                        if (autres.Count > 0)
                            data.autres = autres.ToArray();

                        var strData = XMLSerializer.Serialize(data);

                        if (oldData != strData)
                        {
                            System.IO.File.WriteAllText(fileName, strData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("Exception while exporting the timesheet for date: " + dt, ex);
            }
        }

        public FormData SaveForm(long userId, long organizationId, FormData form, string exportPath = null)
        {
            var mid = new MobileId(form.id);

            var dt = DateTime.ParseExact(form.forDate, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);

            var existingForm =this.ListForDate(dt);

            form_data entity;
            form_data rec;

            if (existingForm.Count > 0)
            {
                entity = existingForm[0];
                entity.data = form.stringifiedData;
                var res = this.Update(entity.id, entity);
                rec = res.Record;
            }
            else
            {
                entity = new form_data();
                entity.organizationId = organizationId;
                entity.employeeId = userId;
                entity.forDate = dt;
                entity.data = form.stringifiedData;

                if (mid.DbId.HasValue) //update
                {
                    entity.id = mid.DbId.Value;
                    var res = this.Update(entity.id, entity);
                    rec = res.Record;
                }
                else
                {
                    var res = this.Add(entity);
                    rec = res.Record;
                }
            }

            ExportSILFormData(dt, userId, organizationId, exportPath);
            FormData dto = new FormData(rec);
            return dto;
        }

        internal override long GetRecordOrganization(Model.form_data record)
        {
            return record.organizationId;
        }

        internal override SmartQuery<Model.form_data> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from r in q.Context.form_data select r);
            if (OrganizationID > -1) q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);
            if (EmployeeId > -1) q.Query = (from r in q.Query where r.employeeId == EmployeeId select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.id select r);
            return q;
        }

        public List<form_data> ListForDate(DateTime after)
        {
            List<form_data> ret = new List<form_data>();
            var q = GetBaseQuery();
            ret = q.Query.Where(r => r.forDate.Value.Year == after.Year && r.forDate.Value.Month == after.Month && r.forDate.Value.Day == after.Day).ToList();
            q.Context.Dispose();
            return ret;
        }

        public List<form_data> List(DateTime after)
        {
            List<form_data> ret = new List<form_data>();
            var q = GetBaseQuery();
            ret = q.Query.Where(r => r.forDate.Value.Year == after.Year && r.forDate.Value.Month == after.Month && r.forDate.Value.Day == after.Day).ToList();
            q.Context.Dispose();
            return ret;
        }
    }
}
