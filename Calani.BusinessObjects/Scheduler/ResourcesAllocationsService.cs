﻿using Calani.BusinessObjects.AbsenceRequests;
using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.FrontEndModels.Scheduler;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Scheduler.Models;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Calani.BusinessObjects.Scheduler
{
    public class ResourcesAllocationsService
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly long _userId;
        private readonly long _orgId;
        private readonly ResourcesAllocationsManager _manager;
        private readonly EmployeesManager _empManager;
        public ResourcesAllocationsService(long currentOrganizationId, long currentUserId)
        {
            _orgId = currentOrganizationId;
            _userId = currentUserId;
            _manager = new ResourcesAllocationsManager(_orgId);
            _empManager = new EmployeesManager(_orgId);
        }

        public AjaxAction RestoreDeletedResourceAllocation(long id)
        {
            var ret = new AjaxAction();

            _log.Info($"RestoreDeletedResourceAllocation. orgId:{_orgId}, userId:{_userId}");

            var entity = _manager.GetById(id).Query.FirstOrDefault();

            if (entity == null)
            {
                ret.success = false;
                ret.message = $"ResourceAllocation with id={id} does not exist.";
                return ret;
            }

            if (entity.organizationId != _orgId)
            {
                ret.success = false;
                ret.message = $"Cannot access ResourceAllocation from another organization.";
                return ret;
            }

            entity.recordStatus = 0;
            entity.updatedAt = DateTime.UtcNow;
            entity.updatedBy = _userId;

            var result = _manager.SaveOrUpdate(entity.id, entity);

            ret.success = result.Success;
            ret.message = result.ErrorMessage;
            ret.id = result.Record.id;

            return ret;
        }

        public AjaxAction AddOrUpdateResourceAllocation(ResourceAllocationModel model)
        {
            var ret = new AjaxAction();

            _log.Info($"AddOrUpdateResourceAllocation. ProjectId:{model.ProjectId}, EmployeeId: {model.EmployeeId}, OtherResourceId: {model.OtherResourceId} orgId:{_orgId}, userId:{_userId}");

            resource_allocations entity;
            resource_allocations entityEmp;

            long msStart = long.Parse(model.StartDateString);
            long msEnd = long.Parse(model.EndDateString);

            model.StartDate = DateTimeOffset.FromUnixTimeMilliseconds(msStart).LocalDateTime;
            model.EndDate = DateTimeOffset.FromUnixTimeMilliseconds(msEnd).LocalDateTime;

            if (model.StartDate > model.EndDate)
            {
                ret.success = false;
                ret.message = "Start date cannot be greater than end date.";
                return ret;
            }

            if (string.IsNullOrEmpty(model.Title) && (model.ProjectId == null || model.ProjectId <= 0))
            {
                ret.success = false;
                ret.message = "Either Title or Project has to be specified.";
                return ret;
            }

            var id = long.Parse(model.Id);
            if (id > 0)
            {
                entity = _manager.GetById(id).Query.FirstOrDefault();

                if (entity == null)
                {
                    ret.success = false;
                    ret.message = $"ResourceAllocation with id={model.Id} does not exist.";
                    return ret;
                }

                if (entity.organizationId != _orgId)
                {
                    ret.success = false;
                    ret.message = $"Cannot access ResourceAllocation from another organization.";
                    return ret;
                }

                entity.updatedAt = DateTime.UtcNow;
                entity.updatedBy = _userId;
            }
            else
            {
                entity = new resource_allocations()
                {
                    createdAt = DateTime.UtcNow,
                    createdBy = _userId
                };

            }

            entity.projectId = model.ProjectId;
            entity.employeeId = model.EmployeeId;
            entity.otherResourceId = model.OtherResourceId;
            entity.departmentId = model.DepartmentId;
            entity.startDate = model.StartDate;
            entity.endDate = model.EndDate;
            entity.title = model.Title;
            entity.comment = model.Comment;
            entity.organizationId = model.OrganizationId;

            var result = _manager.SaveOrUpdate(entity.id, entity);

            //department wise employee section save
            var departmentId = entity.departmentId = model.DepartmentId;
            var parentId = result.Record.id;
            var empDeptResult = _empManager.ListActiveUsers().Where(e => e.departmentId == departmentId).ToList();
            var getResourceAllocationDetailsByParentId = _manager.GetByParentId(parentId);

            // Loop through each existing resource allocation and delete it
            foreach (var existingEntity in getResourceAllocationDetailsByParentId)
            {
                var deleteResult = _manager.Delete(existingEntity.id, Generic.DeleteMethod.FlagRecordStatus);

                if (!deleteResult.Success)
                {
                    _log.Error($"Failed to delete resource allocation for Id: {existingEntity.id}. Error: {deleteResult.ErrorMessage}");
                }
            }

            // Loop through each employee ID and create a new entity
            foreach (var contact in empDeptResult)
            {
                entityEmp = new resource_allocations()
                {
                    createdAt = DateTime.UtcNow,
                    createdBy = _userId,
                    employeeId = contact.id,
                    departmentId = null,
                    parentId = parentId,
                    startDate = model.StartDate,
                    endDate = model.EndDate,
                    title = model.Title,
                    comment = model.Comment,
                    organizationId = model.OrganizationId,
                    projectId = model.ProjectId
                };
                var empSaveResult = _manager.SaveOrUpdate(0, entityEmp);

                if (!empSaveResult.Success)
                {
                    _log.Error($"Failed to save resource allocation for EmployeeId: {contact.id}. Error: {empSaveResult.ErrorMessage}");
                }
            }
            ret.success = result.Success;
            ret.message = result.ErrorMessage;
            ret.id = result.Record.id;

            return ret;
        }

        public AjaxAction DeleteResourceAllocation(long id)
        {
            var result = _manager.Delete(id, Generic.DeleteMethod.FlagRecordStatus);
            var deletedParentId = result.Record.id;
            var getResourceAllocationDetailsByParentId = _manager.GetByParentId(deletedParentId);

            foreach (var existingEntity in getResourceAllocationDetailsByParentId)
            {
                var deleteResult = _manager.Delete(existingEntity.id, Generic.DeleteMethod.FlagRecordStatus);

                if (!deleteResult.Success)
                {
                    _log.Error($"Failed to delete resource allocation for Id: {existingEntity.id}. Error: {deleteResult.ErrorMessage}");
                }
            }

            var ret = new AjaxAction()
            {
                success = result.Success,
                message = result.ErrorMessage,
                id = result.Record.id
            };

            return ret;
        }

        public List<ResourceAllocationModel> ListResourceAllocationsForScheduler(DateTime? dtFrom, DateTime? dtTo, string cultureCode)
        {
            var culture = new CultureInfo(cultureCode);

            var allocationEntities = _manager.GetResourcesAllocations(_orgId, dtFrom: dtFrom, dtTo: dtTo);

            var defaultGreyColor = "#DEDEDE";

            var allocations = allocationEntities.Select(e => new ResourceAllocationModel()
            {
                Id = e.id.ToString(),
                Color = e.jobs != null ? !string.IsNullOrEmpty(e.jobs.color) ? e.jobs.color : defaultGreyColor : defaultGreyColor,
                Comment = e.comment,
                CurrentUserId = _userId,
                EmployeeId = e.employeeId,
                EndDate = e.endDate,
                OrganizationId = e.organizationId,
                OtherResourceId = e.otherResourceId,
                DepartmentId = e.departmentId,
                ProjectId = e.projectId,
                StartDate = e.startDate,
                Title = !string.IsNullOrEmpty(e.title) ? e.title : (e.jobs != null ? e.jobs.internalId : string.Empty),
                Type = "project",
                ResourceName = e.contacts1 != null ? $"{e.contacts1.firstName} {e.contacts1.lastName}" : 
                                e.other_resources != null ? e.other_resources.name : "",
            }).ToList();

            var absenceRequestsManager = new AbsenceRequestsManager(organizationId: _orgId);
            var absences = absenceRequestsManager.ListApprovedOrCancelledRequests(dtFrom: dtFrom, dtTo: dtTo)
            .Select(ar => new AbsenceRequestModel(ar)).ToList();

            absences.ForEach(a => {
                var isCanceled = a.Status.Equals("canceled", StringComparison.InvariantCultureIgnoreCase);

                a.Title = SharedResource.Resource.GetString(a.Title, culture)
                    + (isCanceled ? $" ({SharedResource.Resource.GetString("Canceled", culture)})" : "");
                a.Color = isCanceled ? "#FF0000" : a.Color;
            });

            var models = new List<ResourceAllocationModel>(allocations);
            models.AddRange(absences);

            return models;
        }

    }
}
