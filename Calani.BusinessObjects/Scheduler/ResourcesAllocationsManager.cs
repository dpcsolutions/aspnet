﻿using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Scheduler
{
    public class ResourcesAllocationsManager : Generic.SmartCollectionManager<Model.resource_allocations, long>
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public ResourcesAllocationsManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<resource_allocations> GetDbSet(Model.CalaniEntities db)
        {
            return db.resource_allocations;
        }

        internal override SmartQuery<resource_allocations> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.resource_allocations where r.id == id select r);
            return q;
        }


        public List<resource_allocations> GetByParentId(long parentId)
        {
            var q = base.GetById(parentId);
            q.Query = from r in q.Context.resource_allocations where r.parentId == parentId && r.recordStatus==0 select r;
            return q.Query.ToList();
        }

        public List<resource_allocations> GetResourcesAllocations(long organizationId, DateTime? dtFrom = null, DateTime? dtTo = null)
        {
            var query = GetBaseQuery().Query.Include("jobs")
                .Include(a => a.contacts1)
                .Include(a => a.other_resources)
                .Where(r => r.organizationId == organizationId);

            if (dtFrom != null)
            {
                query = (from r in query where r.startDate >= dtFrom.Value || r.endDate >= dtFrom.Value select r);
            }

            if (dtTo != null)
            {
                query = (from r in query where r.startDate <= dtTo.Value || r.endDate <= dtTo.Value select r);
            }

            return query.OrderBy(r => r.startDate).ToList();
        }

        internal override SmartQuery<resource_allocations> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            q.Query = (from r in q.Context.resource_allocations select r);

            if (!IgnoreRecordStatus)
                q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            if (OrganizationID > -1)
                q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);

            return q;
        }

        public SmartAction<resource_allocations> SaveOrUpdate(long id, resource_allocations resource)
        {
            try
            {
                var result = (id > 0) ? Update(id, resource) : Add(resource);
                return result;
            }
            catch (Exception e)
            {
                _log.Info($"SaveOrUpdate. error. ", e);
                throw;
            }
        }

    }
}
