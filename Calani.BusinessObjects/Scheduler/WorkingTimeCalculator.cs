﻿using System;
using System.Collections.Generic;
using System.Linq;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.Scheduler
{
    public class WorkingTimeCalculator
    {
        public TimeSpan StartWorkingTime { get; private set; }
        public TimeSpan EndWorkingTime { get; private set; }
        public TimeSpan LunchBreakDuration { get; private set; }
        public TimeSpan WorkingDayDuration { get; private set; }
        public TimeSpan AbsenceDayDuration { get; private set; }

        public WorkingTimeCalculator(WorkDayModel workDay)
        {
            if(workDay.StartTime.TotalHours < 0 
                || workDay.EndTime.TotalHours < 0
                || workDay.StartTime > workDay.EndTime)
            {
                workDay.StartTime = new TimeSpan(9);
                workDay.EndTime = new TimeSpan(18);
            }

            StartWorkingTime = workDay.StartTime;
            EndWorkingTime = workDay.EndTime;
            LunchBreakDuration = workDay.LunchDuration;
            WorkingDayDuration = EndWorkingTime - StartWorkingTime - LunchBreakDuration;
            AbsenceDayDuration = workDay.AbsenceDayDuration;
        }

        public TimeSpan HalfDayEndTime
        {
            get
            {
                return StartWorkingTime + new TimeSpan(WorkingDayDuration.Ticks / 2);
            }
        }

        public int CalculateWorkingMinutes(DateTime start, DateTime end, List<calendars_rec> holidayList = null)
        {
            var holidays = new List<calendars_rec>();
            
            if (holidayList != null && holidayList.Any())
            {
                 holidays = holidayList.Where(x =>
                    x.start_date != null && x.start_date.Value.Date >= start.Date
                        && x.end_date != null && x.end_date.Value.Date <= end.Date).ToList();
            }

            if(start.Date == end.Date)
            {
                return holidays.Any() ? 0 : GetWorkingMinutesSameDay(start, end);
            }

            int workingMinutes = 0;
            
            var date = start.Date;
            while (date.Date <= end.Date)
            {
                var dateDayEnd = date.Date.AddHours(EndWorkingTime.TotalHours);
                var isWeekend = date.DayOfWeek == DayOfWeek.Sunday || date.DayOfWeek == DayOfWeek.Saturday;
                var isHoliday = holidays.Any(x =>
                    x.start_date != null && x.start_date.Value.Date >= date.Date
                    && x.end_date != null && x.end_date.Value.Date <= dateDayEnd.Date);

                workingMinutes += isWeekend || isHoliday ? 0 : (int) Math.Round(WorkingDayDuration.TotalMinutes);
                date = date.Date.AddDays(1);
            }
            return workingMinutes;
        }

        public double CalculateWorkingDays(DateTime start, DateTime end, List<calendars_rec> holidayList = null)
        {
            return CalculateWorkingMinutes(start, end, holidayList) / WorkingDayDuration.TotalMinutes;
        }

        public double CalculateAbsenceMinutes(DateTime start, DateTime end, List<calendars_rec> holidayList = null)
        {
            var workingMinutes = CalculateWorkingMinutes(start, end, holidayList);
            
            return workingMinutes * AbsenceDayDuration.TotalMinutes / WorkingDayDuration.TotalMinutes;
        }

        public double CalculateAbsenceDays(DateTime start, DateTime end, List<calendars_rec> holidayList = null)
        {
            return CalculateAbsenceMinutes(start, end, holidayList) / AbsenceDayDuration.TotalMinutes;
        }

        #region Private Methods

        private int GetWorkingMinutesSameDay(DateTime start, DateTime end)
        {
            if (start > end
                || start.DayOfWeek == DayOfWeek.Sunday
                || start.DayOfWeek == DayOfWeek.Saturday)
            {
                return 0;
            }

            if(start == end)
            {
                return (int)WorkingDayDuration.TotalMinutes;
            }

            var date1 = start;
            var date2 = end;

            var workingMinutes = (int)(date2 - date1).TotalMinutes;

            var halfWorkingDayMin = WorkingDayDuration.TotalMinutes / 2;
            if(workingMinutes > halfWorkingDayMin)
            {
                // Subtract lunch break
                workingMinutes -= (int)Math.Round(LunchBreakDuration.TotalMinutes);
            }

            // If more hours are selected within a day than working day duration - duration is absence day
            if (workingMinutes >= WorkingDayDuration.TotalMinutes)
            {
                return (int)Math.Round(WorkingDayDuration.TotalMinutes);
            }

            return workingMinutes;
        }

        #endregion Private Methods

    }
}
