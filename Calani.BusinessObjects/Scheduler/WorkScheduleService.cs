﻿using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.WorkSchedulers;
using System;

namespace Calani.BusinessObjects.Scheduler
{
    public class WorkScheduleService
    {
        private readonly OrganizationManager _organizationManager;

        private readonly WorkScheduleRecManager _workScheduleRecManager;

        public WorkScheduleService(OrganizationManager organizationManager, WorkScheduleRecManager workScheduleRecManager)
        {
            _organizationManager = organizationManager;
            _workScheduleRecManager = workScheduleRecManager;

            _organizationManager.Load();
        }

        public WorkDayModel GetWorkDaySettings()
        {
            return new WorkDayModel(_organizationManager.AbsenceDayDurationHours);

            //int lunchDurationMin = _organizationManager.LunchDuration;

            //var workDay = new WorkDayModel(lunchDurationMin);

            //var scheduleRecords = _workScheduleRecManager.GetScheduleRecords(_organizationManager.AssignedWorkScheduleId);
            
            //if(scheduleRecords.Count > 0)
            //{
            //    var schedule = scheduleRecords[0];
            //    if(!string.IsNullOrEmpty(schedule.start) && !string.IsNullOrEmpty(schedule.end))
            //    {
            //        workDay.StartTime = TimeSpan.Parse(schedule.start);
            //        workDay.EndTime = TimeSpan.Parse(schedule.end);
            //    }
            //}

            //return workDay;
        }
    }
}
