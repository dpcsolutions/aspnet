﻿using System;

namespace Calani.BusinessObjects.Scheduler
{
    public class WorkDayModel
    {
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public TimeSpan LunchDuration { get; set; }
        public TimeSpan AbsenceDayDuration { get; set; }

        public WorkDayModel()
        {
            StartTime = new TimeSpan(9, 0, 0);
            EndTime = new TimeSpan(18, 0, 0);
            LunchDuration = new TimeSpan(1, 0, 0);
            AbsenceDayDuration = new TimeSpan(8, 0, 0);
        }

        public WorkDayModel(double absenceDayDurationHours)
        {
            var minutes = (int)Math.Round((absenceDayDurationHours * 60) % 60);
            var hours = (int)Math.Round((absenceDayDurationHours * 60 - minutes) / 60);
            
            AbsenceDayDuration = new TimeSpan(hours, minutes, 0);
            LunchDuration = TimeSpan.FromMinutes(0);
            StartTime = new TimeSpan(9, 0, 0);
            EndTime = StartTime.Add(AbsenceDayDuration);
        }

        public WorkDayModel(TimeSpan startTime, TimeSpan endTime, TimeSpan lunchDuration)
        {
            StartTime = startTime;
            EndTime = endTime;
            LunchDuration = lunchDuration;
        }
    }
}
