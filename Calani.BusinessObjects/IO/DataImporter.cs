﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using Calani.BusinessObjects.IO;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.IO
{

    public class DataImporter
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private dataImporterBase _importerEngine;

        public DataImporter(long organisationId, dataImporterBase importEngine)
        {
            _importerEngine = importEngine;
            _importerEngine.setOrganisationId(organisationId);
        }

        public importResult<importAddress> getAddresses(int from = 0, DateTime? lastSyncDate = null)
        {
            return _importerEngine.importAddresses(from, lastSyncDate);
        }

        public importResult<importItem> getServices(int from = 0, DateTime? lastSyncDate = null)
        {
            return _importerEngine.importServices(from, lastSyncDate);
        }

        public importResult<importInvoice> registerInvoice(importInvoice detail)
        {
            return _importerEngine.registerInvoice(detail);
        }

        public retExternalRef getReference(importedObject o, CalaniEntities db = null)
        {
            return _importerEngine.getReference(o, db);
        }
        public void setReference(importedObject rf, CalaniEntities db = null)
        {
            _importerEngine.setReference(rf, db);
        }
    }
}
