﻿using Calani.BusinessObjects.Model;
using log4net;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.IO
{ 

    public class WinBizImporter : dataImporterBase
    {
        const string DOWNLOADURL = "https://api.winbizcloud.ch/Bizinfo";
        const string PUBLICPASSWORDKEY = "BgIAAACkAABSU0ExAAQAAAEAAQBZ3myd6ZQA0tUXZ3gIzu1sQ7larRfM5KFiYbkgWk+jw2VEWpxpNNfDw8M3MIIbbDeUG02y/ZW+XFqyMA/87kiGt9eqd9Q2q3rRgl3nWoVfDnRAPR4oENfdXiq5oLW3VmSKtcBl2KzBCi/J6bbaKmtoLlnvYMfDWzkE3O1mZrouzA==";
        Dictionary<string, string> _headers = new Dictionary<string, string>();

        private string _companyName = "DPC Solutions S%C3%A0rl";
        private string _userName = "wb-preyes";
        private string _password = "TremA2011";//"u6aWWiDaT70G1wO2iynwUNNFtYLoBNasdmv+thQe0pBSgMGRjYcgzjSmXMDXKij0zmgd9e7kzOtxDDSeWEjrHUkQCxbXsL8Ad30f/Z5kJhU6/qwg6Eyxc1FKewqBg1Uz+yEXUqt5gtPU3F2Y2hRVIESUcIj4YkcNlpaQHpQ7y0Y=";
        private string _companyId = "1";
        private string _key = "TjZabgSCVfSBkzotyTRR6ObQx3NxYHaTBaxiQ45F1jeavBxJ1I2K9UwtER0+08jB7vf3RBcphMurYuxElTLA1bq6RZDOJl9jC67MN1a4zAGzEAxyW6ZHt3/EnclUIm/lAOlRTyCwsqUmkxKbOuH4wGSx5o3rj08MySKE8KoS3eI=";
        private int _year = DateTime.Now.Year;
       
        private bool _initialised = false;

        public class WinBizAddress
        {
            public long Id;
            public string Code;
            public string SimpleName;
            public string Company;
            public string CivilTitle;
            public string LastName;
            public string FirstName;
            public string Street1;
            public string Street2;
            public string ZipCode;
            public string CountryCode;
            public string Town;
            public string Language;

            public Model.addresses toAddress(long clientId, long organisationId)
            {
                return new Calani.BusinessObjects.Model.addresses()
                {
                    city = this.Town,
                    country = this.CountryCode,
                    npa = this.ZipCode,
                    organizationId = organisationId,
                    street = this.Street1,
                    street2 = this.Street2,
                    clientId = clientId,
                    type = 2
                };
            }

            public importAddress toImportAddress()
            {
                return new importAddress()
                {
                    CivilTitle = this.CivilTitle,
                    Code = this.Code,
                    Company = this.Company,
                    CountryCode = this.CountryCode,
                    FirstName = this.FirstName,
                    Language = this.Language,
                    LastName = this.LastName,
                    SimpleName = this.SimpleName,
                    Street1 = this.Street1,
                    Street2 = this.Street2,
                    Town = this.Town,
                    ZipCode = this.ZipCode
                };
            }
        }

        public class WinBizItems
        {
            public long id;
            public string StockQuantity;
            public string Code;
            public string BarCode;
            public string Price;
            public string PurchasePrice;
            public string CreationDate;
            public string LastUpdateDate;
            public string Unit;
            public string Weight;
            public string Availability;
            public string AccountingMethod;
            public string ShortDescriptionFrench;
            public string ShortDescriptionGerman;
            public string ShortDescriptionItalian;
            public string ShortDescriptionEnglish;
            public string ShortDescriptionOtherLanguage;
            public string LongDescriptionFrench;
            public string LongDescriptionGerman;
            public string LongDescriptionItalian;
            public string LongDescriptionEnglish;
            public string LongDescriptionOtherLanguage;
            public string CategoryId;
            public string BrandId;
            public string MiscellaneousField1;
            public string MiscellaneousField2;
            public string MiscellaneousField3;
            public string MiscellaneousField4;
            public string MiscellaneousField5;
            public string MiscellaneousField6;
            public string MiscellaneousField7;
            public string MiscellaneousField8;
            public string MiscellaneousField9;
            public string MiscellaneousField10;
            public string MiscellaneousField11;
            public string MiscellaneousField12;
            public string[] Pictures;

            public importItem toImportItems()
            {
                return new importItem()
                {
                    Code = this.Code,
                    LongDescriptionEnglish = this.LongDescriptionEnglish,
                    LongDescriptionFrench = this.LongDescriptionFrench,
                    LongDescriptionGerman = this.LongDescriptionGerman,
                    LongDescriptionItalian = this.LongDescriptionItalian,
                    PriceInLocalCCY = Double.Parse(this.Price.Replace('.',',')),
                    ShortDescriptionEnglish = this.ShortDescriptionEnglish,
                    ShortDescriptionFrench = this.ShortDescriptionFrench,
                    ShortDescriptionGerman = this.ShortDescriptionGerman,
                    ShortDescriptionItalian = this.ShortDescriptionItalian,
                    Unit = this.Unit
                };
            }
        }

        private bool isInitialised()
        {
            return _initialised;
        }

        public static string Encrypt(string publicKey, string data)
        {
            var cspParams = new CspParameters { ProviderType = 1 };
            var rsaProvider = new RSACryptoServiceProvider(cspParams);
            rsaProvider.ImportCspBlob(Convert.FromBase64String(publicKey));
            var plainBytes = Encoding.UTF8.GetBytes(data);
            var encryptedBytes = rsaProvider.Encrypt(plainBytes, false);
            var encryptedString = Convert.ToBase64String(encryptedBytes);
            return encryptedString;
        }

        public class WinBizAPIresponder<T>
        {
            public retAPI<T> saveResponse(string response, string surl)
            {
                retAPI<T> ret = JsonConvert.DeserializeObject<retAPI<T>>(response);

                using (Model.CalaniEntities db = new Model.CalaniEntities())
                {
                    //publish the request
                    /* NEED IO TABLE
                    db.io.Add(new Model.io()
                    {
                        datatype = 0,
                        dtoperation = DateTime.Now,
                        engine = "Winbiz",
                        importedData = response,
                        organizationid = 1,
                        export = 0,
                        result = ret.ErrorsCount > 0 ? ret.ErrorLast : "",
                        url = surl
                    });
                    */
                    db.SaveChanges();
                }

                return ret;
            }
        }

        public override void init(string companyName = null, string userName = null, string password = null,
                          string companyId = null, int year = 0, string key = null)
        {
            _companyName = companyName != null ? companyName : _companyName;
            _userName = userName != null ?  userName : _userName;
            _password = password != null ? password : _password;
            _companyId = companyId != null ? companyId : _companyId;
            _year = year != 0 ? year : _year;
            _key = key != null ? key : _key;

            _password = Encrypt(PUBLICPASSWORDKEY, _password);

            _headers.Clear();

            _headers.Add("cache-control", "no-cache");
            _headers.Add("winbiz-key", _key);
            _headers.Add("winbiz-year", _year.ToString());
            _headers.Add("winbiz-companyid", _companyId);
            _headers.Add("winbiz-password", _password);
            _headers.Add("winbiz-username", _userName);
            _headers.Add("winbiz-companyname", _companyName);
        
            _initialised = true;
        }

        public override importResult<importAddress> importAddresses(int from = 0, DateTime? lastSyncDate = null)
        {
            if (!isInitialised()) throw new Exception("not initialised");

            string strMethodName = "Addresses"; 
            string strRet = upload(_headers, DOWNLOADURL, lastSyncDate != null ? createWinbizBody(strMethodName, (((DateTime)lastSyncDate).Day + "." + ((DateTime)lastSyncDate).Month + "." + ((DateTime)lastSyncDate).Year + ",1")) : createWinbizBody(strMethodName, ", 1"));
            var ret = (new WinBizAPIresponder<WinBizAddress>()).saveResponse(strRet, DOWNLOADURL);

            var r = (from it in ret.Value select it.toImportAddress()).ToArray();

            return new retAPI<importAddress>() { ErrorLast = ret.ErrorLast, ErrorsCount = ret.ErrorsCount, ErrorsMsg = ret.ErrorsMsg, Value = r };
        }

        public override importResult<importItem> importServices(int from = 0, DateTime? lastSyncDate = null)
        {
            string strMethodName = "Products";
            string strRet = upload(_headers, DOWNLOADURL, createWinbizBody(strMethodName, "0"));
            var ret = (new WinBizAPIresponder<WinBizItems>()).saveResponse(strRet, DOWNLOADURL);

            var r = (from it in ret.Value select it.toImportItems()).ToArray();

            return new retAPI<importItem>() { ErrorLast = ret.ErrorLast, ErrorsCount = ret.ErrorsCount, ErrorsMsg = ret.ErrorsMsg, Value = r };
        }

        public override retExternalRef getReference(importedObject o, CalaniEntities db = null)
        {
            return o.getReference(new retExternalRef() { externalRef1 = o.Code });
        }

        public override void setReference(importedObject o, CalaniEntities db = null)
        {
            o.setReference(new retExternalRef() { externalRef1 = o.Code }, db);
        }

        public override importResult<importInvoice> registerInvoice(importInvoice detail) {

            retExternalRef rf = this.getReference(detail);
            bool exists = rf != null && rf.externalRef1 == detail.Code;
            string header = $"<NEW>;20;{detail.creationDate.ToString("ddMMyyyy;HH:mm")};GESM-{detail.Code};";
            header += $"{detail.amountInLocalCCY.ToString("#.##").Replace(',', '.')};{detail.LocalCCYISO};{detail.exchangeRate.ToString("#.##").Replace(',', '.')};{detail.amountInForeignCCY.ToString("#.##").Replace(',', '.')};1;{detail.productAccountNum};{detail.rebateAccountNum};;;;{detail.gesMobileDocURLRef};";
            header += (!exists ? "<UPDATE>" : "") + $"GESM-{detail.gesMobileDocId};;;{detail.clientAddress.Code};;{detail.clientAddress.Company};{detail.clientAddress.LastName};{detail.clientAddress.FirstName};{detail.clientAddress.Street1};{detail.clientAddress.Street2};{detail.clientAddress.ZipCode};{detail.clientAddress.Town};{detail.clientAddress.CountryCode};;;;;;;;;;;;;;;;;;2;";

            List<string> items = new List<string>();

            for(var i=0;i<detail.items.Count; i++)
            {
                var item = detail.items[i];
                var line = $"{i+1};1;{item.Code};{item.ShortDescriptionFrench};;{item.Quantity.ToString("#.##").Replace(',', '.')};{item.PriceInLocalCCY.ToString("#.##").Replace(',', '.')};{item.Unit};{item.Remise.ToString("#.##").Replace(',', '.')};{item.TotalInLocalCCY.ToString("#.##").Replace(',', '.')};{item.PriceInForeignCCY.ToString("#.##").Replace(',', '.')};{item.TotalInForeignCCY.ToString("#.##").Replace(',', '.')};<AUTO>;{item.VAT.ToString("#.##").Replace(',', '.')};" + (item.TotalIncludesVAT ? "1" : "2") + $";;;;;;;;;;;;" + (detail.updateItems ? "1;":"0;") + $"{item.ShortDescriptionFrench};{item.LongDescriptionFrench};{item.LongDescriptionGerman};{item.LongDescriptionItalian};{item.LongDescriptionEnglish};;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;GESM-{detail.Code}";
                items.Add(header + line);
            }

            var client = new RestClient(DOWNLOADURL);
            var request = new RestRequest(Method.POST);

            foreach (var h in _headers)
                request.AddHeader(h.Key, h.Value);

            var body = "";
            foreach(var b in items)
            {
                body += b + "\r\n";
            }

            var filewdx = new Random().Next() + ".wdx";

            File.WriteAllText($"c:\\Temp\\{filewdx}", body);

            byte[] content = Encoding.UTF8.GetBytes(body);
            //content = File.ReadAllBytes("c:\\winbiz10.wdx");

            request.AddParameter("winbiz-method", "DOCUMENTIMPORT");
            request.AddFile("winbiz-file", content, filewdx, "application/octet-stream");
            request.AlwaysMultipartFormData = true;
            IRestResponse response = client.Execute(request);

            if (response.StatusCode == HttpStatusCode.OK)
                detail.setReference(new retExternalRef() { externalRef1 = detail.Code });

            return new retAPI<importInvoice>()
            {
                ErrorLast = (response.StatusCode != HttpStatusCode.OK).ToString(),
                ErrorsCount = (response.StatusCode != HttpStatusCode.OK ? 1 : 0),
                ErrorsMsg = response.Content,
                Value = null
            };
        }

        private string createWinbizBody(string method, string data)
        {
            return "{'Method':'$method$','Parameters':[$data$]}".Replace("$method$", method).Replace("$data$", data);
        }
    }
}
