﻿using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.IO
{

    public enum importTypes
    {
        addresses = 0,
        products = 1,
        invoice = 2
    }

    public class retExternalRef
    {
        public string externalRef1 = "";
        public string externalRef2 = "";
        public string externalRef3 = "";
        public string externalRef4 = "";
        public string externalRef5 = "";

        public retExternalRef() { }

        /*
        public retExternalRef(Model.externalref r)
        {
            this.externalRef1 = r.externalRef1;
            this.externalRef2 = r.externalRef2;
            this.externalRef3 = r.externalRef3;
            this.externalRef4 = r.externalRef4;
            this.externalRef5 = r.externalRef5;
        }*/
    }

    public class importedObject
    {
        public string Code = "";
        public importTypes importType = 0;
        public long id = 0;

        public importedObject(importTypes tp)
        {
            this.importType = tp;
        }

        public retExternalRef getReference(retExternalRef rf, CalaniEntities db = null)
        {
            CalaniEntities tmpDB = db;

            if (db == null)
                tmpDB = new CalaniEntities();

                var invoiceref = (from r in tmpDB.externalref
                                  where r.externalRef1 == rf.externalRef1 &&
                                   r.externalRef2 == rf.externalRef2 &&
                                   r.externalRef3 == rf.externalRef3 &&
                                   r.externalRef4 == rf.externalRef4 &&
                                   r.externalRef5 == rf.externalRef5 &&
                                   r.datatype == (int)this.importType
                                  select r).FirstOrDefault();

            if (db == null)
                tmpDB.Dispose();

            if (invoiceref != null)
                return new retExternalRef(invoiceref);

            return null;
        }

    public void setReference(retExternalRef rf, CalaniEntities db = null)
    {
        CalaniEntities tmpDB = db;

        if (db == null)
            tmpDB = new CalaniEntities();

        var invoiceref = (from r in tmpDB.externalref
                          where
                            r.externalRef1 == rf.externalRef1 &&
                            r.externalRef2 == rf.externalRef2 &&
                            r.externalRef3 == rf.externalRef3 &&
                            r.externalRef4 == rf.externalRef4 &&
                            r.externalRef5 == rf.externalRef5 &&
                            r.datatype == (int)this.importType
                          select r).FirstOrDefault();

        if (invoiceref == null)
        {
            tmpDB.externalref.Add(new externalref()
            {
                calaniRef = this.id,
                datatype = (int)importTypes.invoice,
                externalRef1 = rf.externalRef1,
                externalRef2 = rf.externalRef2,
                externalRef3 = rf.externalRef3,
                externalRef4 = rf.externalRef4,
                externalRef5 = rf.externalRef5
            });
        }
        else
        {
            invoiceref.externalRef1 = rf.externalRef1;
            invoiceref.externalRef2 = rf.externalRef2;
            invoiceref.externalRef3 = rf.externalRef3;
            invoiceref.externalRef4 = rf.externalRef4;
            invoiceref.externalRef5 = rf.externalRef5;
        }

        tmpDB.SaveChanges();


        if (db == null)
            tmpDB.Dispose();
    }
}

    public class importInvoice : importedObject
    {
        //public DateTime invoiceDate;
        public string rebateAccountNum = "";
        public double amountInLocalCCY = 0;
        public string LocalCCYISO = "";
        public double amountInForeignCCY = 0;
        public double exchangeRate = 1;
        public string productAccountNum = "";
        public string gesMobileDocURLRef = "";
        public string gesMobileDocId = "";
        public string externalSystemId = "";
        public DateTime creationDate;
        public importAddress clientAddress;
        public bool updateAddress = false;
        public bool updateItems = false;
        public List<importItem> items;

        public importInvoice() : base(importTypes.invoice)
        { }

        public importInvoice(Model.jobs invoice) : base(importTypes.invoice)
        {
            this.amountInLocalCCY = (invoice.total == null ? 0 : (double)invoice.total);
            this.amountInForeignCCY = (invoice.totalInCurrency == null ? 0 : (double)invoice.totalInCurrency);
            this.clientAddress = new importAddress()
            {
                CivilTitle = "",
                Code = (invoice.addresses != null ? invoice.addresses.id.ToString() : ""),
                Company = (invoice.organizations != null ? invoice.organizations.name : ""),
                FirstName = (invoice.contacts != null ? invoice.contacts.firstName : ""),
                LastName = (invoice.contacts != null ? invoice.contacts.lastName : ""),
                Street1 = (invoice.street != null ? invoice.street : ""),
                Street2 = (invoice.street2 != null ? invoice.street2 : ""),
                Town = (invoice.city != null ? invoice.city : ""),
                ZipCode = (invoice.npa != null ? ((int)invoice.npa).ToString() : ""),
                SimpleName = (invoice.organizations != null ? invoice.organizations.name + "-" : "") + (invoice.contacts != null ? invoice.contacts.lastName : ""),
                //CountryCode = (invoice.addresses == null ? invoice.addresses.country
            };

            this.creationDate = DateTime.Now;
            this.exchangeRate = (invoice.currencyRate != null ? ((double)invoice.currencyRate) : 1);
            this.externalSystemId = "";
            this.gesMobileDocId = invoice.id.ToString();
            this.gesMobileDocURLRef = "";
            this.Code = (!String.IsNullOrEmpty(invoice.internalId) ? (invoice.internalId + "-" + invoice.id) : invoice.id.ToString());
            this.items = new List<importItem>();
            this.LocalCCYISO = (invoice.currency != null ? invoice.currency : "");
            this.productAccountNum = "";
            this.rebateAccountNum = "";
            this.updateAddress = true;
            this.updateItems = false;

            foreach(var s in invoice.services_jobs)
            {
                this.items.Add(
                    new importItem()
                    {
                        Code = s.id.ToString(),
                        LongDescriptionFrench = s.serviceName,
                        ShortDescriptionFrench = s.serviceName,
                        PriceInLocalCCY = (s.servicePrice != null ? (Double)s.servicePrice : 0),
                        PriceInForeignCCY = (s.servicePrice != null ? (Double)s.servicePrice : 0),
                        Quantity = (s.serviceQuantity != null ? (Double)s.serviceQuantity : 0),
                        Remise = (s.discount != null ? (Double)s.discount : 0),
                        TotalInLocalCCY = (s.totalPriceExclTax != null ? (Double)s.totalPriceExclTax : 0),
                        TotalInForeignCCY = (s.totalPriceExclTax != null ? (Double)s.totalPriceExclTax : 0),
                        VAT = (s.taxRate != null ? (Double)s.taxRate : 0),
                        TotalIncludesVAT = false,
                        Unit = ""
                    });
            }
        }
    }

    public class importAddress : importedObject
    {
        public string SimpleName = "";
        public string Company = "";
        public string CivilTitle = "";
        public string LastName = "";
        public string FirstName = "";
        public string Street1 = "";
        public string Street2 = "";
        public string ZipCode = "";
        public string CountryCode = "";
        public string Town = "";
        public string Language = "F";

        public importAddress() : base(importTypes.addresses)
        {
        }

        public importAddress(Model.addresses a) : base(importTypes.addresses)
        {
            this.Town = a.city;
            this.id = a.id;
            this.ZipCode = a.npa;
            this.Street1 = a.street;
            this.Street2 = a.street2;
            this.Code = a.id.ToString();
        }
    }

    public class importItem : importedObject
    {
        public bool TotalIncludesVAT = false;
        public double Remise = 0;
        public double PriceInForeignCCY = 0;
        public double PriceInLocalCCY = 0;
        public string Unit = "";
        public double Quantity = 0;
        public double TotalInLocalCCY = 0;
        public double TotalInForeignCCY = 0;
        public double VAT = 0;
        public string ShortDescriptionFrench = "";
        public string ShortDescriptionGerman = "";
        public string ShortDescriptionItalian = "";
        public string ShortDescriptionEnglish = "";
        public string LongDescriptionFrench = "";
        public string LongDescriptionGerman = "";
        public string LongDescriptionItalian = "";
        public string LongDescriptionEnglish = "";

        public importItem() : base(importTypes.products) { }

        public importItem(Model.services s) : base(importTypes.products)
        {
            this.ShortDescriptionFrench = s.name;
            this.LongDescriptionFrench = s.description;
            this.PriceInLocalCCY = s.unitPrice;
            this.id = s.id;
            this.Code = s.id.ToString();
        }
    }

    public interface importResult<T>
    {
        long ErrorsCount { get; set; }
        string ErrorLast { get; set; }
        string ErrorsMsg { get; set; }
        T[] Value { get; set; }
    }

    public interface Importer<importAddress, importItems>
    {
        importResult<importAddress> importAddresses(int from = 0, DateTime? lastSyncDate = null);
        importResult<importItems> importServices(int from = 0, DateTime? lastSyncDate = null);

        importResult<importInvoice> registerInvoice(importInvoice detail);

        retExternalRef getReference(importedObject o, CalaniEntities db = null);

        void setOrganisationId(long organisationId);

        void setReference(importedObject rf, CalaniEntities db = null);
    }

    public abstract class dataImporterBase: Importer<importAddress, importItem>
    {
        private long _organisationId = -1;

        public dataImporterBase(bool useSSL = true)
        {
            ServicePointManager.ServerCertificateValidationCallback += ValidateRemoteCertificate;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
        }

        abstract public retExternalRef getReference(importedObject o, CalaniEntities db = null);
        abstract public void setReference(importedObject rf, CalaniEntities db = null);

        abstract public void init(string companyName = null, string userName = null, string password = null,
                          string companyId = null, int year = 0, string key = null);

        public string upload(Dictionary<string, string> headers, string url, string body)
        {
            using (WebClient client = new WebClient())
            {
                foreach (var h in headers)
                    if(h.Key != "Content-Type") client.Headers.Add(h.Key, h.Value);

                client.Headers.Add("Content-Type", "application/json");

                return client.UploadString(url, body);
            }
        }

        private static bool ValidateRemoteCertificate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors error)
        {
            // If the certificate is a valid, signed certificate, return true.
            if (error == System.Net.Security.SslPolicyErrors.None)
            {
                return true;
            }

            Console.WriteLine("X509Certificate [{0}] Policy Error: '{1}'",
                cert.Subject,
                error.ToString());

            return false;
        }

        public void setOrganisationId(long organisationId)
        {
            _organisationId = organisationId;
        }

        abstract public importResult<importAddress> importAddresses(int from = 0, DateTime? lastSyncDate = null);

        abstract public importResult<importInvoice> registerInvoice(importInvoice detail);

        abstract public importResult<importItem> importServices(int from = 0, DateTime? lastSyncDate = null);
    }

    public class retAPI<T> : importResult<T>
    {
        public long ErrorsCount { get; set; }
        public string ErrorLast { get; set; }
        public string ErrorsMsg { get; set; }
        public T[] Value { get; set; }
    }
}
