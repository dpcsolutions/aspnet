﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Contacts;

namespace Calani.BusinessObjects.Model
{


    public partial class addresses
    {
        public string Description
        {
            get
            {
                string ret = "";
                if (!String.IsNullOrWhiteSpace(city))
                    ret += city;
                if (!String.IsNullOrWhiteSpace(street))
                    ret += ". " + street;
                if (!String.IsNullOrWhiteSpace(nb)) 
                    ret += ", " + nb;

                if (!String.IsNullOrWhiteSpace(street2))
                {
                    var maxLength = street2.Length>15? 15: street2.Length;
                    ret += " (" + street2.Substring(0,maxLength) + (street2.Length>maxLength?"...":"")+")";
                }

                ret = ret.Trim();

                if (String.IsNullOrWhiteSpace(ret) && !String.IsNullOrWhiteSpace(caption))
                {
                    ret = caption;
                }

                return ret;
            }
        }
    }

    public partial class contacts
    {
        public string passwordStr { get; set; }


        public string Description
        {
            get
            {
                string ret = "";
                if (!String.IsNullOrWhiteSpace(lastName))
                    ret += lastName;

                if (!String.IsNullOrWhiteSpace(firstName)) 
                    ret += " " + firstName;

                ret = ret.Trim();
                return ret;
            }
        }

        public string CustomText { get; set; }

        public string DescriptionAndMail
        {
            get
            {
                if (CustomText != null) return CustomText;

                string ret = "";
                if (!String.IsNullOrWhiteSpace(lastName)) ret += lastName;
                if (!String.IsNullOrWhiteSpace(firstName)) ret += " " + firstName;
                if(!String.IsNullOrWhiteSpace(primaryEmail)) ret += " <"+primaryEmail+">";
                ret = ret.Trim();
                return ret;
            }
        }

        public decimal? HourRate { get; set; }
        public int? Rate { get; set; }
        public string RateStr
        {
            get
            {
                var d = (Rate??0) / ((decimal)100);
                return Math.Round(d, 2, MidpointRounding.AwayFromZero) + " %";
            }
            
        }
    }

    public class EmployeeDto
    {
        public long Id { get; set; }
        public string SecondaryEmail { get; set; }
        public int? WeekWorkDuration { private get; set; }

        public int WeekWorkHours
        {
            
            get
            {
                if (WeekWorkDuration != null)
                    return WeekWorkDuration.Value / 60;
                return 0;
            }

        }

        public string LastName { get; set; }
        public string FirstName { get; set; }

        public string FullName
        {
            get
            {
                return String.Format("{0} {1}", FirstName, LastName);
            }
        }

        public decimal Rate { get; set; }
        public decimal VacationsRemains { get; set; }
        public string PrimaryPhone { get; set; }
        public string SecondaryPhone { get; set; }
        public string PrimaryEmail { get; set; }
    }

    public static class EmployeeExtensions
    {
        public static EmployeeDto ToDto(this contacts model)
        {
            var dto = new EmployeeDto();
            var org = model.organizations;
            var contractEntity = model.contacts_contracts.FirstOrDefault(c=>c.recordStatus == 0);
            ContractDto contract = null;
            if(contractEntity!=null)
                contract = contractEntity.ToDto();

            dto.Id = model.id;
            dto.FirstName = model.firstName;
            dto.LastName = model.lastName;

            dto.WeekWorkDuration = contract != null ? contract.DurationMin : org.week_work_duration;
            dto.Rate = contract != null ? contract.Rate : 100;
            dto.VacationsRemains = contract != null ? contract.VacationsRemains : 0;

            dto.PrimaryPhone = model.primaryPhone;
            dto.SecondaryPhone = model.secondaryPhone;
            dto.PrimaryEmail = model.primaryEmail;
            dto.SecondaryEmail = model.secondaryEmail;


            return dto;
        }
    }
}