﻿using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using log4net;
using System;
using System.Data.Entity;
using System.Linq;
using System.Reflection;

namespace Calani.BusinessObjects.Contacts
{
    public partial class EmployeesOvertimeManager : SmartCollectionManager<contacts_overtime, long>
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        
        public EmployeesOvertimeManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<contacts_overtime> GetDbSet(CalaniEntities db)
        {
            return db.contacts_overtime;
        }

        internal override SmartQuery<contacts_overtime> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            q.Query = (from r in q.Context.contacts_overtime select r);

            return q;
        }

        internal override SmartQuery<contacts_overtime> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.contacts_overtime where r.id == id select r);
            return q;
        }

        public contacts_overtime GetOvertimeForEmployee(long userId)
        {
            var q = this.GetBaseQuery();
            q.Query = q.Query.Where(o => o.contact_id == userId);
            
            return q.Query.SingleOrDefault() ?? new contacts_overtime();
        }

        public SmartAction<contacts_overtime> SaveOrUpdate(long id, contacts_overtime overtime)
        {
            try
            {
                SmartAction<contacts_overtime> result;

                if (id > 0)
                {
                    var existingOvertime = GetById(id).Query.FirstOrDefault();
                    result = this.Update(id, overtime);

                }
                else
                {
                    result = this.Add(overtime);
                }

                return result;
            }
            catch (Exception e)
            {
                _log.Info($"SaveOrUpdate. error. ",e);
                throw;
            }

        }
    }
}
