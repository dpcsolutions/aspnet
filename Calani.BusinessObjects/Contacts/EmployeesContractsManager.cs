﻿using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Membership;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Notifications;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Calani.BusinessObjects.Contacts
{
    public partial class EmployeesManager
    {
        public class EmployeesContractsManager : Generic.SmartCollectionManager<Model.contacts_contracts, long>
        {
            private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
            private readonly EmployeesManager EmployeesManager;
            private readonly NotificationManager NotificationManager;
            private readonly MailSender MailSender;
            
            public EmployeesContractsManager(long organizationId)
            {
                KeyProperty = "id";
                RecordStatusProperty = "recordStatus";
                OrganizationID = organizationId;

                EmployeesManager = new EmployeesManager(organizationId);
                MailSender = new MailSender();
                NotificationManager = new NotificationManager();
            }

            internal override DbSet<contacts_contracts> GetDbSet(Model.CalaniEntities db)
            {
                return db.contacts_contracts;
            }

            internal override SmartQuery<contacts_contracts> GetById(long id)
            {
                var q = base.GetById(id);
                q.Query = (from r in q.Context.contacts_contracts where r.id == id select r);
                return q;
            }

            internal override SmartQuery<contacts_contracts> GetBaseQuery()
            {
                var q = base.GetBaseQuery();

                // base
                q.Query = (from r in q.Context.contacts_contracts select r);

                if (!IgnoreRecordStatus)
                    q.Query = (from r in q.Query where r.recordStatus != (int) RecordStatus.Deleted select r);

                return q;
            }

            internal override void BeforeAction(SmartAction<contacts_contracts> action)
            {

                if (action.Type == SmartActionType.Update && action.Record != null && action.Record.updated_at == null)
                {
                    action.Record.updated_at = DateTime.UtcNow;
                }

                base.BeforeAction(action);
            }

            public List<ContractDto> GetContractsDto(long? contactId)
            {
                var q = GetBaseQuery();

                q.Query = q.Query.Where(c => c.contact_Id == contactId).OrderBy(c => c.start_date);

                var data = q.Query.ToList();

                return (from r in data select r.ToDto()).ToList();
            }

            public List<contacts_contracts> GetActiveContractsForPeriod(DateTime dtFrom, DateTime dtTo, long? contactId = null)
            {
                var q = GetBaseQuery();

                if (contactId.HasValue)
                {
                    q.Query = q.Query.Where(c => c.contact_Id == contactId);
                }
                
                q.Query = q.Query.Where(c => c.recordStatus != (int)RecordStatus.Deleted &&
                                ((c.recordStatus == (int)RecordStatus.Active) ||
                                (c.start_date >= dtFrom && c.start_date <= dtTo) || 
                                (c.recordStatus == (int)RecordStatus.Suspended && c.updated_at >= dtFrom && c.updated_at <= dtTo)));

                var data = q.Query.OrderBy(c => c.start_date).ToList();

                return data;
            }

            public contacts_contracts GetContractStartingInPeriod(DateTime startDate, DateTime endDate, long contactId)
            {
                IgnoreRecordStatus = true;

                var q = GetBaseQuery();

                q.Query = q.Query.Where(c => c.contact_Id == contactId && (c.start_date <= startDate || c.start_date <= endDate)).OrderByDescending(c => c.start_date);

                var contract = q.Query.FirstOrDefault(x => x.recordStatus == 0) ?? q.Query.FirstOrDefault(x => x.recordStatus == 0);

                IgnoreRecordStatus = false;

                return contract;
            }

            public ContractDto GetActiveContractDto(long contactId)
            {
                return GetContractsDto(contactId).Where(c => c.RecordStatus == RecordStatus.Active).FirstOrDefault();
            }

            public contacts_contracts GetActiveContract(long contactId)
            {
                var q = GetBaseQuery();

                q.Query = q.Query.Where(c => c.contact_Id == contactId && c.recordStatus == (int)RecordStatus.Active);

                var entity = q.Query.FirstOrDefault();

                return entity;
            }

            public long CreateContract(ContractDto contract)
            {
                if(contract.Id <= 0 && contract.StartDate == null)
                {
                    throw new ArgumentNullException(nameof(contract.StartDate));
                }

                if(contract.Id <= 0 && contract.StartDate.Value.Date < DateTime.Now.Date)
                {
                    throw new ArgumentException(nameof(contract.StartDate), "startDate of a contract cannot be set in past.");
                }

                var q = GetBaseQuery();

                var contracts = (from c in q.Query where c.contact_Id == contract.UserId select c).ToList();

                var current = contracts.FirstOrDefault(c => c.recordStatus == 0);

                var vr = (current?.vacation_remains ?? 0) - (current?.vacation_year ?? 0 - contract.VacationsPerYear);

                var rec = new contacts_contracts()
                {
                    contact_Id = contract.UserId,
                    rate = (int)(contract.Rate),
                    duration = contract.DurationMin,
                    start_date = contract.StartDate ?? current.start_date,
                    hour_rate = contract.HoursRate,
                    vacation_year = contract.VacationsPerYear,
                    vacation_remains = vr > 0 ? vr : 0,
                    vacation_carried = current?.vacation_carried ?? 0,
                    overtime_allowed = contract.OvertimeEnabled,
                    overtime_rate = contract.OvertimeRate,
                    overtime_rate2_enabled = contract.Overtime2Enabled,
                    overtime_rate2 = contract.OvertimeRate2,
                    overtime_rate2_threshold_hours = contract.Overtime2ThresholdHours,
                };

                SmartAction<contacts_contracts> ret;

                ret = Add(rec);

                if (ret.Success)
                {
                    foreach (var o in contracts.Where(c => c.recordStatus == (int)RecordStatus.Active))
                    {
                        o.recordStatus = (int) RecordStatus.Suspended;
                        o.updated_at = DateTime.UtcNow;
                    }

                    q.Context.SaveChanges();

                    SendNewContractNotification(ret.Record);

                    return ret.Record.id;
                }
                else
                    return contract.Id;

            }

            public void RecalculateContractVacations(contacts_contracts contract)
            {
                try
                {
                    _log.Info(
                        $"RecalculateContractVacations. cid:{contract.id}, vacation_remains:{contract.vacation_remains}, vacation_year:{contract.vacation_year} ");
                    contract.vacation_carried = contract.vacation_remains;
                    contract.vacation_remains += contract.vacation_year;
                    contract.updated_at = DateTime.UtcNow;
                    this.Update(contract.id, contract);
                }
                catch (Exception e)
                {
                    _log.Error(
                        $"RecalculateContractVacations. Error. cid:{contract.id}, vacation_remains:{contract.vacation_remains}, vacation_year:{contract.vacation_year}. ",
                        e);
                }
            }

            private SmartAction<contacts_contracts> UpdateRemainingVacationDays(long employeeId, decimal days)
            {
                var result = new SmartAction<contacts_contracts>(SmartActionType.Update);

                var q = GetBaseQuery();

                q.Query = q.Query.Where(c => c.contact_Id == employeeId && c.recordStatus == (int)RecordStatus.Active);

                var activeContract = q.Query.ToList().FirstOrDefault();

                if (activeContract != null)
                {
                    activeContract.vacation_remains = activeContract.vacation_remains + days > 0 ?
                                                            activeContract.vacation_remains + days : 0;

                    result = Update(activeContract.id, activeContract);
                }
                else
                {
                    result.Success = false;
                    result.ErrorMessage = $"Active contract for employee {employeeId} was not found.";
                }

                return result;
            }

            public SmartAction<contacts_contracts> RegisterApprovedAbsenceRequest(long employeeId, decimal days)
            {
                var result = UpdateRemainingVacationDays(employeeId, -days);

                return result;
            }

            public SmartAction<contacts_contracts> RegisterCancelledAbsenceRequest(long employeeId, decimal days)
            {
                var result = UpdateRemainingVacationDays(employeeId, days);

                return result;
            }

            #region Private Methods

            private void SendNewContractNotification(contacts_contracts entity)
            {
                var cultureStr = "fr-FR";
                var culture = new CultureInfo(cultureStr);

                try
                {
                    cultureStr = UserCultureManager.GetUserCulture(entity.contact_Id) ?? cultureStr;
                    culture = new CultureInfo(cultureStr);
                }
                catch (Exception e)
                {
                    _log.Error("SendNotification, getUserCulture error.", e);
}

                var title = GetResource("ContractChangedMessageSubject", culture);
                var msg = GetResource("ContractChangedMessageMessage", culture);

                if (!String.IsNullOrWhiteSpace(title))
                {
                    _log.Info($"Sending notification ContactsContracts, title: '{title}' to:{entity.contact_Id}, updated by:{entity.updated_by}");

                    SendNotification(msg, title, entity.contact_Id, entity);
                }
            }

            private void SendNotification(string message, string title, long employeeId, contacts_contracts entity)
            {
                var category = "ContactsContracts";

                NotificationManager.sendNotifications(message, title, new List<long> { employeeId }, new Dictionary<string, string>() { { "contactsContractsId", entity.id.ToString() } }, category);

                if (!MailSender.Variables.ContainsKey("PublicUrl"))
                {
                    MailSender.Variables.Add("PublicUrl", System.Configuration.ConfigurationManager.AppSettings["PublicUrl"]);
                }

                var user = EmployeesManager.Get(employeeId);
                MailSender.MailTo.Email = user.primaryEmail ?? user.secondaryEmail;
                MailSender.MailTo.UserId = employeeId;
                MailSender.SendSmartHtmlEmail(title, message, category);
            }

            private static string GetResource(string n, CultureInfo cultureInfo)
            {
                if (SharedResource.Resource == null) return n;
                return SharedResource.Resource.GetString(n, cultureInfo);
            }

            #endregion Private Methods
        }
    }
}
