﻿using Calani.BusinessObjects.Calendars;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.WorkSchedulers;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;

namespace Calani.BusinessObjects.Contacts
{
    public partial class EmployeesManager : Generic.SmartCollectionManager<Model.contacts, long>
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        
        public EmployeesManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<contacts> GetDbSet(Model.CalaniEntities db)
        {
            return db.contacts;
        }

        internal override SmartQuery<contacts> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.contacts where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(contacts record)
        {
            if (record.organizationId != null) return record.organizationId.Value;
            return -1;
        }

        public int CountEmployees(bool onlyValidAccounts)
        {
            if (onlyValidAccounts)
            {
                return (from r in List() where r.type > 0 select r).Count();
            }
            else
            {
                return List().Count();
            }
        }

        internal override SmartQuery<contacts> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from r in q.Context.contacts where r.type >= 0 && r.type <= 9 select r);
            if (OrganizationID > -1)
                q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);
            if (!IgnoreRecordStatus)
                q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus)
                q.Query = (from r in q.Query orderby r.lastName, r.firstName select r);


            return q;
        }
        
        internal SmartQuery<contacts> GetLightWeightQuery()
        {
            var q = base.GetBaseQuery();

            q.Query = from r in q.Context.contacts 
                where r.type >= 0 && r.type <= 9 && r.recordStatus == 0 && r.organizationId == OrganizationID
                orderby r.lastName, r.firstName select r;
            
            return q;
        }

        public List<contacts> ListAdmins()
        {
            var admin = Convert.ToInt32(ContactTypeEnum.User_Admin);
            return (from r in GetBaseQuery().Query where r.type == admin select r).ToList();
        }

        public List<contacts> ListEmployees()
        {
            var users = (from r in GetBaseQuery().Query select r).Include("contacts_contracts").ToList();
            if (users != null)
                users.ForEach(u =>
                {
                    var contract = u.contacts_contracts.FirstOrDefault(c => c.recordStatus == 0);

                    if (contract != null)
                    {
                        u.HourRate = contract.hour_rate;
                        u.Rate = contract.rate;
                    }

                });

            return users;
        }

        public List<contacts> ListActiveUsers()
        {
            var disabled = Convert.ToInt32(ContactTypeEnum.User_Disabled);
            return (from r in GetBaseQuery().Query where r.type != disabled select r).ToList();
        }

        public List<contacts> ListEmployeesLightweight(List<long> employeesIds = null)
        {
            var query = (from r in GetBaseQuery().Query select r);

            if(employeesIds != null && employeesIds.Any())
            {
                query = query.Where(r => employeesIds.Contains(r.id));
            }

            return query.ToList();
        }
        
        public List<contacts> ListEmployeesRealLightweight(List<long> employeesIds = null)
        {
            var query = GetLightWeightQuery().Query.ToList().Select(r => new contacts
            {
                firstName = r.firstName,
                id = r.id,
                initials = r.initials,
                lastName = r.lastName,
                type = r.type,
                organizationId = r.organizationId
            });

            if(employeesIds != null && employeesIds.Any())
            {
                query = query.Where(r => employeesIds.Contains(r.id));
            }

            return query.ToList();
        }

        public bool IsEmailFree(string email)
        {
            long org = OrganizationID;
            OrganizationID = -1;
            var q = GetBaseQuery();
            OrganizationID = org;


            email = email.ToLower();
            var sameemail = (from r in q.Query where r.primaryEmail.ToLower() == email select r).Count();
            return sameemail == 0;
        }

        public bool IsInitialsFree(string ini)
        {
            var q = GetBaseQuery();
            ini = ini.Trim().ToUpper();
            var sameini = (from r in q.Query where r.initials.ToUpper() == ini select r).Count();
            return sameini == 0;
        }

        internal override void BeforeAction(SmartAction<contacts> action)
        {
            if (action.Record.type == null) action.Record.type = Convert.ToInt32(ContactTypeEnum.User_Employee);

            if (action.Type == SmartActionType.Update)
            {
                long currentId = 0;
                if (action.ExistingRecord != null) currentId = action.ExistingRecord.id;

                var dbq = GetBaseQuery();
                var org =
                    (from r in dbq.Context.contacts where r.id == currentId select r.organizations).FirstOrDefault();

                if (org.owner == currentId && action.Record.type != Convert.ToInt32(ContactTypeEnum.User_Admin))
                {
                    action.Abort("ERR_can_not_downgrade_owner");
                }

                string email = action.Record.primaryEmail.ToLower();
                var sameemail = (from r in dbq.Context.contacts
                    where r.id != currentId && r.primaryEmail.ToLower() == email 
                          && r.type != (int)ContactTypeEnum.Contact_CustomerMain && r.type != (int)ContactTypeEnum.Contact_CustomerOther
                                 select r).Count();
                if (sameemail > 0)
                {
                    action.Abort("ERR_email_already_used");
                }
            }

            if (action.Type == SmartActionType.Add)
            {
                var dbq = GetBaseQuery();

                string email = action.Record.primaryEmail.ToLower();
                var sameemail =
                    (from r in dbq.Context.contacts where r.primaryEmail.ToLower() == email
                     && r.type != (int)ContactTypeEnum.Contact_CustomerMain && r.type != (int)ContactTypeEnum.Contact_CustomerOther
                     select r).Count();
                if (sameemail > 0)
                {
                    action.Abort("ERR_email_already_used");
                }
            }

            if (action.Type == SmartActionType.Delete)
            {
                var dbq = GetBaseQuery();
                var org = (from r in dbq.Context.organizations where r.owner == action.Record.id select r)
                    .FirstOrDefault();
                if (org != null)
                {
                    action.Abort("ERR_can_not_delete_owner");
                }
            }

            // unicity of email and initials
            if (action.Type == SmartActionType.Add || action.Type == SmartActionType.Update)
            {
                long currentId = 0;
                if (action.ExistingRecord != null) currentId = action.ExistingRecord.id;

                if (String.IsNullOrWhiteSpace(action.Record.initials))
                {
                    action.Abort("ERR_initials_mandatory");
                }

                var dbq = GetBaseQuery();
                if (!String.IsNullOrWhiteSpace(action.Record.primaryEmail))
                {
                    // check in every org !
                    int sameEmailUsers = (from r in dbq.Context.contacts
                        where
                            ((r.type >= 0 && r.type <= 9) || r.type > 90)
                            && r.id != currentId && r.primaryEmail == action.Record.primaryEmail
                        select r).Count();

                    if (sameEmailUsers > 0)
                    {
                        action.Abort("ERR_Email_already_used");
                    }
                }

                if ((from r in dbq.Query where r.id != currentId && r.initials == action.Record.initials select r)
                    .Count() > 0)
                {
                    action.Abort("ERR_Initials_already_used");
                }

            }

            // -->
        }

        public List<ScheduleRecordDto> GetAssignedScheduleRecordsDto(long employeeId, out List<long> calendarsId)
        {
            var employee = GetById(employeeId).Query.FirstOrDefault();
            calendarsId = new List<long>();

            if (employee != null)
            {
                var records = new List<ScheduleRecordDto>();

                var orgId = employee.organizationId.HasValue ? employee.organizationId.Value : this.OrganizationID;

                var wsprMngr = new WorkScheduleRecManager(orgId);



                if (employee.work_schedule_profile_id.HasValue)
                {
                    records = wsprMngr.GetScheduleRecordsDto(employee.work_schedule_profile_id.Value);
                    var ids = employee.work_schedule_profiles.work_schedule_profiles_calendars
                        .Where(c => c.calendar_id.HasValue).Select(c => c.calendar_id.Value);
                    calendarsId.AddRange(ids);
                }

                else if (employee.organizations != null && employee.organizations.work_schedule_profiles != null)
                {
                    records = wsprMngr.GetScheduleRecordsDto(employee.organizations.work_schedule_profiles.id);
                    var ids = employee.organizations.work_schedule_profiles.work_schedule_profiles_calendars
                        .Where(c => c.calendar_id.HasValue).Select(c => c.calendar_id.Value);
                    calendarsId.AddRange(ids);

                }

                var recDtos = records.Where(r => r.RecordStatus == (int) RecordStatus.Active).ToList();

                return recDtos;
            }

            return new List<ScheduleRecordDto>();
        }

        public void RecalculateVacations()
        {

            var mgr = new EmployeesContractsManager(OrganizationID);

            var q = GetBaseQuery();
            var contracts = (from r in q.Query select r.contacts_contracts.FirstOrDefault(f => f.recordStatus == 0));

            _log.Info($"RecalculateVacations. orgId:{OrganizationID}, count:{contracts.Count()}");

            foreach (var contract in contracts)
            {
                if (contract != null)
                    mgr.RecalculateContractVacations(contract);


            }
        }

        public SmartAction<contacts> SaveOrUpdate(long id, contacts contacts)
        {
            try
            {

                SmartAction<contacts> result;

                var sendEmail = false;

                if (id > 0)
                {
                    var dbUser = GetById(id).Query.FirstOrDefault();
                    var prevStatus = dbUser?.status ?? 0;

                    result = this.Update(id, contacts);

                    sendEmail = prevStatus == 0 && result.Record.status > 0;
                }
                else
                {
                    sendEmail = true;
                    result = this.Add(contacts);


                }
                _log.Info($"SaveOrUpdate. userId:{id},sendEmail:{sendEmail} ");
                if (sendEmail)
                {
                    Generic.MailSender mailSender = new Generic.MailSender();

                    Model.contacts ret = null;


                    Model.CalaniEntities db = new Model.CalaniEntities();
                    var usr = result.Record;

                    if (usr != null)
                    {
                        mailSender.MailTo.Email = usr.primaryEmail;
                        mailSender.MailTo.UserId = usr.id;

                        mailSender.Variables.Add("email", usr.primaryEmail);
                        mailSender.Variables.Add("pass", usr.passwordStr);

                        mailSender.SendSmartHtmlEmail("ges_01_user_added.html", "activation");

                    }
                }
                return result;
            }
            catch (Exception e)
            {
                _log.Info($"SaveOrUpdate. error. ",e);
                throw;
            }

        }
    }
}
