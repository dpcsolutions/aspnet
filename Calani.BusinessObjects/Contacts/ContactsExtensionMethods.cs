﻿using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.Contacts
{
    public static class ContactsExtensionMethods
    {
        public static bool IsAdmin(this contacts user)
        {
            return user.type == (int)ContactTypeEnum.User_Admin
                || user.type == (int)ContactTypeEnum.PlatformAdmin_Support;
        }
        
        public static bool IsSystemAdmin(this contacts user)
        {
            return user.type == (int)ContactTypeEnum.PlatformAdmin_Support;
        }
    }
}
