﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.Contacts
{
    public class ClientAddressesManager : Generic.SmartCollectionManager<Model.addresses, long>
    {
        long _customerId;

        public ClientAddressesManager(long organizationId, long customerId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
            _customerId = customerId;
        }


        internal override DbSet<addresses> GetDbSet(Model.CalaniEntities db)
        {
            return db.addresses;
        }

        internal override SmartQuery<addresses> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.addresses where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(addresses record)
        {
            if (record != null && record.organizationId != null) return record.organizationId.Value;
            return -1;
        }

        internal override SmartQuery<addresses> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            var f = Convert.ToInt32(AddressTypeEnum.CustomerAddress);

            // base
            q.Query = (from r in q.Context.addresses where r.type == f select r);
            if (OrganizationID > -1) q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);
            if (_customerId > -1) q.Query = (from r in q.Query where r.clientId == _customerId select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.city select r);


            return q;
        }


        internal override void BeforeAction(SmartAction<addresses> action)
        {
            if (action.Type == SmartActionType.Add || action.Type == SmartActionType.Update)
            {
                if (action.Record != null)
                {
                    action.Record.type = Convert.ToInt32(AddressTypeEnum.CustomerAddress);
                }


                /*
                if (action.Record.street == null || action.Record.street.Length < 2)
                {
                    action.Abort("ERR_Address_street_2_chars");
                }

                if (action.Record.city == null || action.Record.city.Length < 2)
                {
                    action.Abort("ERR_Address_city_2_chars");
                }*/

            }
        }
        


    }
}
