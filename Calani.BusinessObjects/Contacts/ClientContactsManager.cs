﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.Contacts
{
    public class ClientContactsManager : Generic.SmartCollectionManager<Model.contacts, long>
    {
        long _customerId;

        public ClientContactsManager(long organizationId, long customerId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
            _customerId = customerId;
        }


        internal override DbSet<contacts> GetDbSet(Model.CalaniEntities db)
        {
            return db.contacts;
        }

        internal override SmartQuery<contacts> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.contacts where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(contacts record)
        {
            if (record != null && record.organizationId != null) return record.organizationId.Value;
            return -1;
        }

        internal override SmartQuery<contacts> GetBaseQuery()
        {
            var q = base.GetBaseQuery();



            // base
            q.Query = (from r in q.Context.contacts where r.type >= 10 && r.type <= 19 select r);
            if (OrganizationID > -1) q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);
            if (_customerId > -1) q.Query = (from r in q.Query where r.clientId == _customerId select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.lastName select r);


            return q;
        }


        internal override void BeforeAction(SmartAction<contacts> action)
        {



            if (action.Type == SmartActionType.Add || action.Type == SmartActionType.Update)
            {
                if (action.Record != null)
                {
                    if (action.Record.type == null)
                        action.Record.type = Convert.ToInt32(ContactTypeEnum.Contact_CustomerOther);

                    if(action.Record.type == Convert.ToInt32(ContactTypeEnum.Contact_CustomerMain))
                    {
                        // flag other contacts to other
                        var oq = GetBaseQuery();
                        var or = (from r in oq.Query where r.id != action.Record.id select r).ToList();
                        foreach (var o in or)
                            o.type = Convert.ToInt32(ContactTypeEnum.Contact_CustomerOther);
                        oq.Context.SaveChanges();
                    }
                }

                if (action.Record.lastName == null || action.Record.lastName.Length < 2)
                {
                    action.Abort("ERR_Contact_Lastname_2_chars");
                }

                

            }
        }



        }
}
