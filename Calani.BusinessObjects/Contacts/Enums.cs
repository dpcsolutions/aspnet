﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Contacts
{
    public enum ContactTypeEnum
    {
        // plage [0-9] = users
        User_Disabled = 0,
        User_Admin = 1,
        User_Employee = 2,
        // -->

        // plage [10-19] = contacts
        Contact_CustomerMain = 10,
        Contact_CustomerOther = 11,
        // -->

        Partner_Manager = 77,


        PlatformAdmin_Support = 99
    }

    public enum AddressTypeEnum
    {
        Undefined = 0,
        CompanyAddress = 1,
        CustomerAddress = 2
    }
}
