﻿using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.Contacts.Models
{
    public class AddressDto
    {
        public long Id { get; set; }
        public string Street { get; set; } = "";
        public string Street2 { get; set; } = "";
        public string Nb { get; set; } = "";
        public string Npa { get; set; } = "";
        public string City { get; set; } = "";
        public string Postbox { get; set; } = "";
        public string State { get; set; } = "";
        public string Country { get; set; } = "";
        public string Caption { get; set; } = "";

        public long? ClientId { get; set; }
        public long? OrganizationId { get; set; }
        public AddressTypeEnum type { get; set; }

        public AddressDto()
        {
        }

        public AddressDto(addresses entity)
        {
            Id = entity.id;
            Street = (entity.street ?? "").Trim();
            Street2 = (entity.street2 ?? "").Trim();
            Nb = (entity.nb ?? "").Trim();
            Npa = (entity.npa ?? "").Trim();
            City = (entity.city ?? "").Trim();
            Postbox = (entity.postbox ?? "").Trim();
            State = (entity.state ?? "").Trim();
            Country = (entity.country ?? "").Trim();
            Caption = (entity.caption ?? "").Trim();
            ClientId = entity.clientId;
            OrganizationId = entity.organizationId;
            type = entity.type.HasValue ? (AddressTypeEnum)entity.type : AddressTypeEnum.Undefined;
        }
    }
}
