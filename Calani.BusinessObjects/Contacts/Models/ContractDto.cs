﻿using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Globalization;

namespace Calani.BusinessObjects.Contacts
{
    public class ContractDto
    {
        public int? DurationMin;
        public int VacationsPerYear { get; set; }
        
        public decimal VacationsCarriedOver { get; set; }

        public decimal VacationsRemains { get; set; }
        public decimal HoursRate { get; set; }

        public long Id { get; set; }
        public long UserId { get; set; }
        public string StartStr { get; set; }
        public DateTime? StartDate { get; set; }
        public decimal Rate { get; set; }
        public double Hours { get; set; }
        public bool OvertimeEnabled { get; set; }
        public decimal OvertimeRate { get; set; }
        public decimal OvertimeRate2 { get; set; }
        public decimal Overtime2ThresholdHours { get; set; }
        public bool Overtime2Enabled { get; set; }

        public DateTime? UpdatedAt { get; set; }
        public long? UpdatedBy { get; set; }
        public RecordStatus RecordStatus { get; set; }        
    }

    public static class ContractsExtentions
    {
        public static ContractDto ToDto(this contacts_contracts model)
        {
            if (model == null)
            {
                return null;
            }
            ContractDto dto = new ContractDto();

            dto.Id = model.id;
            dto.UserId = model.contact_Id;
            if (model.start_date.HasValue)
            {
                dto.StartStr = model.start_date.Value.ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture);
                dto.StartDate = model.start_date;
            }

            if (model.rate.HasValue)
            {
                var d = model.rate.Value /(decimal) 100;
                dto.Rate = Math.Round(d, 1, MidpointRounding.AwayFromZero);
            }

            if (model.duration.HasValue)
            {
                var d = (model.duration.Value /(decimal)60);
                dto.Hours = (double)Math.Round(d, 1, MidpointRounding.AwayFromZero);

                dto.DurationMin = model.duration;
            } 

            dto.RecordStatus = (RecordStatus)model.recordStatus;
            dto.VacationsPerYear = model.vacation_year;
            dto.VacationsCarriedOver = model.vacation_carried;
            dto.VacationsRemains = model.vacation_remains;
            dto.OvertimeEnabled = model.overtime_allowed;
            dto.OvertimeRate = model.overtime_rate;
            dto.OvertimeRate2 = model.overtime_rate2;
            dto.Overtime2ThresholdHours = model.overtime_rate2_threshold_hours;
            dto.Overtime2Enabled = model.overtime_rate2_enabled;
            dto.HoursRate = model.hour_rate;
            dto.UpdatedBy = model.updated_by;
            dto.UpdatedAt = model.updated_at;

            return dto;
        }

    }
}
