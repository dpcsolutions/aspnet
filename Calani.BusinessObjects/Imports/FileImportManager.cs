﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Calani.BusinessObjects.DocGenerator;
using GemBox.Spreadsheet;


namespace Calani.BusinessObjects.Imports
{
    public class FileImportManager
    {
        public static string DIR { get; set; }

        public bool CanBeImported { get; private set; }

        public SheetFileContent Content { get; set; }

        public string FilePrefix { get; set; }

        public FileImportManager(byte[] data, string filePrefix)
        {
            FilePrefix = filePrefix;

            SpreadsheetInfo.SetLicense(GemboxAPI.SpreadsheetLicense);

            MemoryStream ms = new MemoryStream(data);
            ExcelFile ef = null;
            ExcelWorksheet exw = null;

            try
            {
                ef = ExcelFile.Load(ms, LoadOptions.XlsxDefault);
                exw = ef.Worksheets.ActiveWorksheet;
            }
            catch (Exception e)
            {
            }

            int countColumns = 0;
            while(exw.Cells[0, countColumns].Value != null && !String.IsNullOrWhiteSpace(exw.Cells[0, countColumns].Value.ToString()))
            {
                countColumns++;
            }
            


            if (exw != null && exw.Rows.Count > 1 && countColumns > 0)
            {
                Content = new SheetFileContent();

                for (int i = 0; i < exw.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        string celldata = null;
                        for (int j = 0; j < countColumns; j++)
                        {
                            if (exw.Cells[i, j] != null && exw.Cells[i, j].Value != null)
                            {
                                celldata = exw.Cells[i, j].Value.ToString();
                            }

                            if (!String.IsNullOrWhiteSpace(celldata))
                            {
                                Content.Columns.Add(celldata);

                            }
                        }
                    }
                    else
                    {

                        var r = new Row();
                        for (int j = 0; j < countColumns; j++)
                        {
                            string celldata = null;
                            if (exw.Cells[i, j] != null && exw.Cells[i, j].Value != null)
                            {
                                celldata = exw.Cells[i, j].Value.ToString();
                            }

                            if (!String.IsNullOrWhiteSpace(celldata))
                            {
                                string collname = j.ToString();
                                if (collname.Length == 1) collname = "0" + collname;
                                collname = "Cell" + collname;

                                var pi = r.GetType().GetProperty(collname);
                                if (pi != null)
                                {
                                    pi.SetValue(r, celldata);
                                }
                            }

                        }
                        Content.Rows.Add(r);
                    }
                }
            }

            try
            {
                ms.Dispose();
            }
            catch { }


           



            if (Content == null)
            {
                Generic.CSVReader cs = new Generic.CSVReader();
                cs.ColumnSeparator = ";";
                cs.LoadBin(data);
                Content = new SheetFileContent();
                if (cs.DataSet != null && cs.DataSet.Tables.Count > 0 && cs.DataSet.Tables[0].Rows.Count > 0 && cs.DataSet.Tables[0].Columns.Count > 2)
                {
                    var dt = cs.DataSet.Tables[0];
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        var r = new Row();
                        for (int j = 0; j < dt.Columns.Count; j++)
                        {
                            string celldata = null;
                            if (dt.Rows[i] != null && dt.Rows[i][j] != null)
                            {
                                celldata = dt.Rows[i][j].ToString().Trim();
                            }

                            if (!String.IsNullOrWhiteSpace(celldata))
                            {
                                string collname = j.ToString();
                                if (collname.Length == 1) collname = "0" + collname;
                                collname = "Cell" + collname;

                                var pi = r.GetType().GetProperty(collname);
                                if (pi != null)
                                {
                                    pi.SetValue(r, celldata);
                                }
                            }

                        }
                        Content.Rows.Add(r);
                    }


                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        Content.Columns.Add(dt.Columns[j].ColumnName);
                    }
                }

                // eof


            }


            if (Content != null)
            {
                // remove empty rows
                Content.Rows = (from r in Content.Rows where !r.EmptyLine select r).ToList();

                // remove doublons
                Dictionary<string, Row> uniqueRows = new Dictionary<string, Row>();
                foreach (var r in Content.Rows)
                {
                    if (!uniqueRows.ContainsKey(r.AllDataConcat)) uniqueRows.Add(r.AllDataConcat, r);
                }

                Content.Rows = uniqueRows.Values.ToList();
            }


            CanBeImported = Content != null && Content.Rows.Count > 0;

        } 

        public string Save(long orgId)
        {
            string ret = null;
            string file = FilePrefix + "_" + Guid.NewGuid().ToString("n") + ".xml";

            string path = Path.Combine(DIR, orgId.ToString(), file);
            if(Content != null && CanBeImported)
            {
                XmlSerializer xs = new XmlSerializer(Content.GetType());
                using (StreamWriter wr = new StreamWriter(path))
                {
                    xs.Serialize(wr, Content);
                    ret = file;
                }
            }

            return ret;
        }


        public static SheetFileContent Load(string file)
        {
            SheetFileContent ret = null;
            XmlSerializer xs = new XmlSerializer(typeof(SheetFileContent));
            using (StreamReader wr = new StreamReader(file))
            {
                ret = (SheetFileContent)xs.Deserialize(wr);           
            }
            return ret;
        }
    }

    public class SheetFileImport
    {
        public string Mime { get; set; }
        public string Mode { get; set; }
    }

}
