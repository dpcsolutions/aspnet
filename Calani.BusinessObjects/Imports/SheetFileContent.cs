﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Imports
{
    public class SheetFileContent
    {
        public List<Row> Rows { get; set; }

        public List<string> Columns { get; set; }

        public SheetFileContent()
        {
            Rows = new List<Row>();
            Columns = new List<string>();
        }
    }

    public class Row
    {
        public string Cell00 { get; set; }
        public string Cell01 { get; set; }
        public string Cell02 { get; set; }
        public string Cell03 { get; set; }
        public string Cell04 { get; set; }
        public string Cell05 { get; set; }
        public string Cell06 { get; set; }
        public string Cell07 { get; set; }
        public string Cell08 { get; set; }
        public string Cell09 { get; set; }
        public string Cell10 { get; set; }
        public string Cell11 { get; set; }
        public string Cell12 { get; set; }
        public string Cell13 { get; set; }
        public string Cell14 { get; set; }
        public string Cell15 { get; set; }
        public string Cell16 { get; set; }
        public string Cell17 { get; set; }
        public string Cell18 { get; set; }
        public string Cell19 { get; set; }
        public string Cell20 { get; set; }
        public string Cell21 { get; set; }
        public string Cell22 { get; set; }
        public string Cell23 { get; set; }
        public string Cell24 { get; set; }
        public string Cell25 { get; set; }
        public string Cell26 { get; set; }
        public string Cell27 { get; set; }
        public string Cell28 { get; set; }
        public string Cell29 { get; set; }
        public string Cell30 { get; set; }
        public string Cell31 { get; set; }
        public string Cell32 { get; set; }
        public string Cell33 { get; set; }
        public string Cell34 { get; set; }
        public string Cell35 { get; set; }
        public string Cell36 { get; set; }
        public string Cell37 { get; set; }
        public string Cell38 { get; set; }
        public string Cell39 { get; set; }


        public string AllDataConcat
        {
            get
            {
                return "" + Cell00
                    + ";" + Cell01
                    + ";" + Cell02
                    + ";" + Cell03
                    + ";" + Cell04
                    + ";" + Cell05
                    + ";" + Cell06
                    + ";" + Cell07
                    + ";" + Cell08
                    + ";" + Cell09
                    + ";" + Cell10
                    + ";" + Cell11
                    + ";" + Cell12
                    + ";" + Cell13
                    + ";" + Cell14
                    + ";" + Cell15
                    + ";" + Cell16
                    + ";" + Cell17
                    + ";" + Cell18
                    + ";" + Cell19
                    + ";" + Cell20
                    + ";" + Cell21
                    + ";" + Cell22
                    + ";" + Cell23
                    + ";" + Cell24
                    + ";" + Cell25
                    + ";" + Cell26
                    + ";" + Cell27
                    + ";" + Cell28
                    + ";" + Cell29
                    + ";" + Cell30
                    + ";" + Cell31
                    + ";" + Cell32
                    + ";" + Cell33
                    + ";" + Cell34
                    + ";" + Cell35
                    + ";" + Cell36
                    + ";" + Cell37
                    + ";" + Cell38
                    + ";" + Cell39;
            }
        }

        public bool EmptyLine
        {
            get
            {
                return Cell00 == null
                    && Cell01 == null
                    && Cell02 == null
                    && Cell03 == null
                    && Cell04 == null
                    && Cell05 == null
                    && Cell06 == null
                    && Cell07 == null
                    && Cell08 == null
                    && Cell09 == null
                    && Cell10 == null
                    && Cell11 == null
                    && Cell12 == null
                    && Cell13 == null
                    && Cell14 == null
                    && Cell15 == null
                    && Cell16 == null
                    && Cell17 == null
                    && Cell18 == null
                    && Cell19 == null
                    && Cell20 == null
                    && Cell21 == null
                    && Cell22 == null
                    && Cell23 == null
                    && Cell24 == null
                    && Cell25 == null
                    && Cell26 == null
                    && Cell27 == null
                    && Cell28 == null
                    && Cell29 == null
                    && Cell30 == null
                    && Cell31 == null
                    && Cell32 == null
                    && Cell33 == null
                    && Cell34 == null
                    && Cell35 == null
                    && Cell36 == null
                    && Cell37 == null
                    && Cell38 == null
                    && Cell39 == null
                    && Cell30 == null
                    && Cell31 == null
                    && Cell32 == null
                    && Cell33 == null
                    && Cell34 == null
                    && Cell35 == null
                    && Cell36 == null
                    && Cell37 == null
                    && Cell38 == null
                    && Cell39 == null;
            }
        }


        public string GetCell(int j)
        {
            string ret = "";
            string suffx = j.ToString();
            if (suffx.Length < 2) suffx = "0" + suffx;

            var prop = this.GetType().GetProperty("Cell" + suffx);
            if (prop != null)
            {
                var o = prop.GetValue(this);
                if(o!= null)
                {
                    ret = o.ToString();
                }
            }
            return ret;
        }
    }
}
