﻿namespace Calani.BusinessObjects.FrontEndModels
{
    public class UnitModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long? OrganizationId { get; set; }
        public bool Selected { get; set; }
    }
}
