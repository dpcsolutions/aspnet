﻿using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.FrontEndModels.Scheduler
{
    public class EmployeeModel : ResourceModel
    {
        public EmployeeModel()
        {
            Group = "Employees";
            Prefix = "emp_";
        }

        public string LastName { get; set; }
        
        public EmployeeModel(contacts entity, int defaultWeeklyHours) : this()
        {
            Id = entity.id.ToString();
            Name = string.Format("{0} {1}", entity.firstName, entity.lastName);
            LastName = entity.lastName;
            RemainingTimeMin = (int)(entity.weeklyHours ?? (defaultWeeklyHours * (entity.Rate ?? 10000) / 10000)) * 60;
            WorkingTimeRate = (double)(entity.Rate ?? 10000) / 10000;
        }
    }
}
