﻿using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.AbsenceRequests;
using Calani.BusinessObjects.Scheduler.Models;
using Newtonsoft.Json;

namespace Calani.BusinessObjects.FrontEndModels.Scheduler
{
    public class AbsenceRequestModel : ResourceAllocationModel
    {
        [JsonIgnore]
        public readonly string Prefix = "absreq_";

        [JsonProperty("category")]
        public string AbsenceCategory { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonIgnore]
        public bool Canceled { get; set; }

        public AbsenceRequestModel()
        {
            Color = "#fff";
            Type = "absence";
        }

        public AbsenceRequestModel(absence_requests entity)
            :this()
        {
            var absenceCategory = (AbsenceCategoryEnum)entity.absenceCategory;
            var cancellationStatus = entity.GetCancellationStatus();
            Status = cancellationStatus.GetDescription().ToLowerInvariant();

            Id = $"{Prefix}{entity.id}";
            EmployeeId = entity.employeeId;
            StartDate = entity.startDate;
            EndDate = entity.endDate;
            AbsenceCategory = absenceCategory.ToString();
            OrganizationId = entity.organizationId;
            Title = absenceCategory.GetDescription();
            Comment = entity.comment;
            Color = absenceCategory.GetColor();
            Icon = absenceCategory.GetIcon();
            ResourceName = entity.contacts4 != null ? $"{entity.contacts4.firstName} {entity.contacts4.lastName}" : string.Empty;
            Canceled = cancellationStatus == CancellationStatusEnum.Cancelled;
        }
    }
}
