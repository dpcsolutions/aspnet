﻿using Calani.BusinessObjects.Model;
using System.Collections.Generic;

namespace Calani.BusinessObjects.FrontEndModels.Scheduler
{
    public class OtherResourceModel : ResourceModel
    {
        public OtherResourceModel()
        {
            Group = "OtherResources";
            Prefix = "othres_";
        }

        public OtherResourceModel(other_resources entity) : this()
        {
            Id = entity.id.ToString();
            Name = entity.name;
            RemainingTimeMin = (int)entity.weeklyHours * 60;
            WorkingTimeRate = 1;
        }
    }
}
