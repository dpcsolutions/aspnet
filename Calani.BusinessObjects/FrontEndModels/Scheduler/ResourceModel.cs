﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Calani.BusinessObjects.FrontEndModels.Scheduler
{
    public class ResourceModel
    {
        private string _id;

        [JsonProperty("id")]
        public string Id 
        {
            get { return _id; } 
            set { _id = GetIdWithPrefix(value); }
        }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("color")]
        public string Color { get; set; }

        [JsonProperty("minutes")]
        public int RemainingTimeMin { get; set; }

        [JsonProperty("minutesTotal")]
        public int TotalTimeMin { get; set; }

        [JsonProperty("timePercent")]
        public int RemainingTimePercent { get; set; }

        [JsonProperty("rate")]
        public double WorkingTimeRate { get; set; }

        [JsonProperty("group")]
        public string Group { get; set; }

        [JsonIgnore]
        public string Prefix { get; set; }

        [JsonProperty("children")]
        public IEnumerable<ResourceModel> Children { get; set; }

        public ResourceModel()
        {
            Color = "#ffffff";
            RemainingTimeMin = 40 * 60;
            TotalTimeMin = RemainingTimeMin;
            RemainingTimePercent = 100;
            WorkingTimeRate = 1;
        }

        public string GetIdWithPrefix(string id)
        {
            return string.Format("{0}{1}", Prefix, id);
        }

        public int SumChildrenHours()
        {
            if(Children == null || !Children.Any())
            {
                return 0;
            }

            return Children.Sum(c => c.RemainingTimeMin);
        }
    }
}
