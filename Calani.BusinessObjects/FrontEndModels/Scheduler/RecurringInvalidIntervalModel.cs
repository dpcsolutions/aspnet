﻿using Newtonsoft.Json;

namespace Calani.BusinessObjects.FrontEndModels.Scheduler
{
    public class RecurringInvalidIntervalModel
    {
        [JsonProperty("allDay")]
        public bool AllDay { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("weekDays")]
        public string WeekDays { get; set; }

        [JsonProperty("repeat")]
        public string Repeat { get; set; }
    }
}
