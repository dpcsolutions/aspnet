﻿using Calani.BusinessObjects.Model;
using Newtonsoft.Json;

namespace Calani.BusinessObjects.FrontEndModels.Scheduler
{
    public class ProjectModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("text")]
        public string Name { get; set; }

        [JsonProperty("color")]
        public string Color { get; set; }

        [JsonProperty("clientId")]
        public long ClientId { get; set; }

        [JsonProperty("clientName")]
        public string ClientName { get; set; }

        public ProjectModel()
        {
        }

        public ProjectModel(jobs entity)
        {
            Id = entity.id;
            Name = entity.internalId;
            Color = entity.color;
            ClientId = entity.clientId;
            ClientName = entity.clients != null ? entity.clients.companyName : string.Empty;
        }
    }
}
