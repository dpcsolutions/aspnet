﻿using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.FrontEndModels.Scheduler
{
    public class DepartmentModel : ResourceModel
    {
        public DepartmentModel()
        {
            Group = "Department";
            Prefix = "depa_";
        }

        public DepartmentModel(departments entity) : this()
        {
            Id = entity.id.ToString();
            Name = entity.name;
            RemainingTimeMin = (int)40 * 60;
            WorkingTimeRate = 1;
        }
    }
}
