﻿using Newtonsoft.Json;
using System;

namespace Calani.BusinessObjects.FrontEndModels.Scheduler
{
    public class InvalidIntervalModel
    {
        [JsonProperty("allDay")]
        public bool AllDay { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("start")]
        public DateTime? Start { get; set; }

        [JsonProperty("end")]
        public DateTime? End { get; set; }

        [JsonProperty("recurring")]
        public RecurringInvalidIntervalModel Recurring { get; set; }

        [JsonProperty("resource")]
        public string ResourceFrontEndId { get; set; }
    }
}
