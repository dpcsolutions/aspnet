﻿using Calani.BusinessObjects.FrontEndModels.Scheduler;
using Newtonsoft.Json;
using System;

namespace Calani.BusinessObjects.Scheduler.Models
{
    public class ResourceAllocationModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonIgnore]
        public long? EmployeeId { get; set; }

        [JsonIgnore]
        public long? parentId { get; set; }

        [JsonIgnore]
        public long? OtherResourceId { get; set; }
        [JsonIgnore]
        public int? DepartmentId { get; set; }

        [JsonProperty("projectId")]
        public long? ProjectId { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("organizationId")]
        public long OrganizationId { get; set; }

        [JsonProperty("start")]
        public DateTime StartDate { get; set; }

        [JsonIgnore]
        public string StartDateString { get; set; }

        [JsonProperty("end")]
        public DateTime EndDate { get; set; }

        [JsonIgnore]
        public string EndDateString { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }

        [JsonIgnore]
        public long CurrentUserId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("color")]
        public string Color { get; set; }

        [JsonProperty("resource")]
        public string Resource
        {
            get
            {
                return EmployeeId.HasValue ? $"emp_{EmployeeId.Value}" : OtherResourceId.HasValue ? $"othres_{OtherResourceId.Value}" : DepartmentId.HasValue ? $"depa_{DepartmentId.Value}" : "";
            }
        }

        [JsonProperty("resourceName")]
        public string ResourceName { get; set; }

        public ResourceAllocationModel()
        {
        }

        public ResourceAllocationModel(InvalidDateModel model) : this()
        {
            StartDate = model.Start;
            EndDate = model.End;
            Title = model.Title;
            Color = "#EEECEC";
        }
        
    }
}
