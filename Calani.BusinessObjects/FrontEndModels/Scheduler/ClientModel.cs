﻿using Calani.BusinessObjects.Model;
using Newtonsoft.Json;

namespace Calani.BusinessObjects.FrontEndModels.Scheduler
{
    public class ClientModel
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("text")]
        public string Name { get; set; }

        public ClientModel(long id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
