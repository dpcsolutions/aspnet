﻿using Calani.BusinessObjects.Calendars;
using Newtonsoft.Json;
using System;

namespace Calani.BusinessObjects.FrontEndModels.Scheduler
{
    public class InvalidDateModel
    {
        [JsonProperty("start")]
        public DateTime Start { get; set; }

        [JsonProperty("end")]
        public DateTime End { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        public InvalidDateModel()
        {
        }

        public InvalidDateModel(CalendarRecordDto dto)
            :this()
        {
            Start = dto.StartDate ?? DateTime.Now;
            End = dto.EndDate ?? DateTime.Now;
            Title = dto.Name;
        }
    }
}  
