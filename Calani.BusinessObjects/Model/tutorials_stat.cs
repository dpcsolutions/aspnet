//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Calani.BusinessObjects.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class tutorials_stat
    {
        public long id { get; set; }
        public long tutorial_id { get; set; }
        public long viewed_by { get; set; }
        public Nullable<System.DateTime> viewed_date { get; set; }
        public int recordStatus { get; set; }
    
        public virtual contacts contacts { get; set; }
        public virtual tutorials tutorials { get; set; }
    }
}
