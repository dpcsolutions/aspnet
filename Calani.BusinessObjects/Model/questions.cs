//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Calani.BusinessObjects.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class questions
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public questions()
        {
            this.tasklistanswers = new HashSet<tasklistanswers>();
        }
    
        public long questionID { get; set; }
        public long templateId { get; set; }
        public string question { get; set; }
        public string descriptions { get; set; }
        public string responseType { get; set; }
        public Nullable<System.DateTime> lastUpdate { get; set; }
        public Nullable<long> createdBy { get; set; }
        public Nullable<long> updatedBy { get; set; }
        public Nullable<bool> isMandatory { get; set; }
        public int recordStatus { get; set; }
        public long suspendedQID { get; set; }
        public string optionForPickList { get; set; }
        public string possibleAnswer { get; set; }
    
        public virtual templates templates { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tasklistanswers> tasklistanswers { get; set; }
    }
}
