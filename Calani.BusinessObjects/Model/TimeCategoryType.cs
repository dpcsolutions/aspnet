//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Calani.BusinessObjects.Model
{
    using System;
    
    public enum TimeCategoryType : int
    {
        ScheduleRule = 0,
        ScheduleHoliday = 1,
        Custom = 2,
        Auto = 3,
        Predefined = 4,
        Vacation = 5,
        Normal = 6,
        Overtime = 7,
        Unpaid = 8,
        Ill = 9,
        Lunch = 10,
        TimeOfDay = 11,
        Accident = 12,
        Justified = 13,
        Holiday = 14
    }
}
