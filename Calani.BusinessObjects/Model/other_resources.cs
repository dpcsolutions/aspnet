//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Calani.BusinessObjects.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class other_resources
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public other_resources()
        {
            this.resource_allocations = new HashSet<resource_allocations>();
        }
    
        public long id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public double weeklyHours { get; set; }
        public long organizationId { get; set; }
        public int recordStatus { get; set; }
        public System.DateTime created_at { get; set; }
        public long created_by { get; set; }
        public Nullable<System.DateTime> updated_at { get; set; }
        public Nullable<long> updated_by { get; set; }
    
        public virtual contacts contacts { get; set; }
        public virtual contacts contacts1 { get; set; }
        public virtual organizations organizations { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<resource_allocations> resource_allocations { get; set; }
    }
}
