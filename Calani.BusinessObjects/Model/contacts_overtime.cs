//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Calani.BusinessObjects.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class contacts_overtime
    {
        public long id { get; set; }
        public decimal total_hours_effectively_compensated { get; set; }
        public decimal total_hours_actually_paid { get; set; }
        public Nullable<long> contact_id { get; set; }
    
        public virtual contacts contacts { get; set; }
    }
}
