//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Calani.BusinessObjects.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class contacts
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public contacts()
        {
            this.absence_requests = new HashSet<absence_requests>();
            this.absence_requests1 = new HashSet<absence_requests>();
            this.absence_requests2 = new HashSet<absence_requests>();
            this.absence_requests3 = new HashSet<absence_requests>();
            this.absence_requests4 = new HashSet<absence_requests>();
            this.absence_requests5 = new HashSet<absence_requests>();
            this.absence_requests6 = new HashSet<absence_requests>();
            this.attachments = new HashSet<attachments>();
            this.calendars = new HashSet<calendars>();
            this.calendars_rec = new HashSet<calendars_rec>();
            this.contacts_overtime = new HashSet<contacts_overtime>();
            this.dualauthenticationtokens = new HashSet<dualauthenticationtokens>();
            this.expenses = new HashSet<expenses>();
            this.projectemployees = new HashSet<projectemployees>();
            this.contacts_attachments = new HashSet<contacts_attachments>();
            this.contacts_attachments1 = new HashSet<contacts_attachments>();
            this.contacts_contracts = new HashSet<contacts_contracts>();
            this.contacts_contracts1 = new HashSet<contacts_contracts>();
            this.tutorials_screens = new HashSet<tutorials_screens>();
            this.documentemplates = new HashSet<documentemplates>();
            this.expenses1 = new HashSet<expenses>();
            this.form_data = new HashSet<form_data>();
            this.jobs = new HashSet<jobs>();
            this.login = new HashSet<login>();
            this.task_contact_mapping = new HashSet<task_contact_mapping>();
            this.notes = new HashSet<notes>();
            this.organizations1 = new HashSet<organizations>();
            this.services_jobs = new HashSet<services_jobs>();
            this.task_activity = new HashSet<task_activity>();
            this.task_attachments = new HashSet<task_attachments>();
            this.task_pin_status = new HashSet<task_pin_status>();
            this.timesheet_records_services = new HashSet<timesheet_records_services>();
            this.timesheetrecords = new HashSet<timesheetrecords>();
            this.timesheets = new HashSet<timesheets>();
            this.tutorials = new HashSet<tutorials>();
            this.mobile_tutorials_deployed = new HashSet<mobile_tutorials_deployed>();
            this.tutorials_stat = new HashSet<tutorials_stat>();
            this.work_reports = new HashSet<work_reports>();
            this.work_reports1 = new HashSet<work_reports>();
            this.work_reports2 = new HashSet<work_reports>();
            this.work_schedule_profiles1 = new HashSet<work_schedule_profiles>();
            this.work_schedule_profiles_rec = new HashSet<work_schedule_profiles_rec>();
            this.jobs1 = new HashSet<jobs>();
            this.jobs2 = new HashSet<jobs>();
            this.other_resources = new HashSet<other_resources>();
            this.other_resources1 = new HashSet<other_resources>();
            this.resource_allocations = new HashSet<resource_allocations>();
            this.resource_allocations1 = new HashSet<resource_allocations>();
            this.resource_allocations2 = new HashSet<resource_allocations>();
            this.services_jobs1 = new HashSet<services_jobs>();
            this.tasklists = new HashSet<tasklists>();
            this.vacation_carried_over_days = new HashSet<vacation_carried_over_days>();
            this.vacation_carried_over_days1 = new HashSet<vacation_carried_over_days>();
            this.visits = new HashSet<visits>();
        }
    
        public long id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public Nullable<System.DateTime> dateOfBirth { get; set; }
        public string primaryPhone { get; set; }
        public string secondaryPhone { get; set; }
        public string primaryEmail { get; set; }
        public string secondaryEmail { get; set; }
        public string telecopy { get; set; }
        public Nullable<long> clientId { get; set; }
        public Nullable<long> organizationId { get; set; }
        public Nullable<int> type { get; set; }
        public string password { get; set; }
        public string recoverCode { get; set; }
        public Nullable<int> status { get; set; }
        public Nullable<int> role { get; set; }
        public string title { get; set; }
        public string civtitle { get; set; }
        public string initials { get; set; }
        public string account { get; set; }
        public Nullable<double> weeklyHours { get; set; }
        public string lang { get; set; }
        public string stafftoken { get; set; }
        public int recordStatus { get; set; }
        public string notificationToken { get; set; }
        public Nullable<System.DateTime> lastlogin { get; set; }
        public string culture { get; set; }
        public bool overtime_allowed { get; set; }
        public Nullable<int> departmentId { get; set; }
        public Nullable<long> work_schedule_profile_id { get; set; }
        public double militaryDays { get; set; }
        public bool enableDualAuthentication { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<absence_requests> absence_requests { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<absence_requests> absence_requests1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<absence_requests> absence_requests2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<absence_requests> absence_requests3 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<absence_requests> absence_requests4 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<absence_requests> absence_requests5 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<absence_requests> absence_requests6 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<attachments> attachments { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<calendars> calendars { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<calendars_rec> calendars_rec { get; set; }
        public virtual clients clients { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<contacts_overtime> contacts_overtime { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<dualauthenticationtokens> dualauthenticationtokens { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<expenses> expenses { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<projectemployees> projectemployees { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<contacts_attachments> contacts_attachments { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<contacts_attachments> contacts_attachments1 { get; set; }
        public virtual organizations organizations { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<contacts_contracts> contacts_contracts { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<contacts_contracts> contacts_contracts1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tutorials_screens> tutorials_screens { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<documentemplates> documentemplates { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<expenses> expenses1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<form_data> form_data { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<jobs> jobs { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<login> login { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<task_contact_mapping> task_contact_mapping { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<notes> notes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<organizations> organizations1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<services_jobs> services_jobs { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<task_activity> task_activity { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<task_attachments> task_attachments { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<task_pin_status> task_pin_status { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<timesheet_records_services> timesheet_records_services { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<timesheetrecords> timesheetrecords { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<timesheets> timesheets { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tutorials> tutorials { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<mobile_tutorials_deployed> mobile_tutorials_deployed { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tutorials_stat> tutorials_stat { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<work_reports> work_reports { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<work_reports> work_reports1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<work_reports> work_reports2 { get; set; }
        public virtual work_schedule_profiles work_schedule_profiles { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<work_schedule_profiles> work_schedule_profiles1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<work_schedule_profiles_rec> work_schedule_profiles_rec { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<jobs> jobs1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<jobs> jobs2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<other_resources> other_resources { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<other_resources> other_resources1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<resource_allocations> resource_allocations { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<resource_allocations> resource_allocations1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<resource_allocations> resource_allocations2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<services_jobs> services_jobs1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tasklists> tasklists { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<vacation_carried_over_days> vacation_carried_over_days { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<vacation_carried_over_days> vacation_carried_over_days1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<visits> visits { get; set; }
    }
}
