//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Calani.BusinessObjects.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class bvr
    {
        public long organizationId { get; set; }
        public Nullable<int> mode { get; set; }
        public Nullable<int> type { get; set; }
        public string idsenderbank { get; set; }
        public string idaccount { get; set; }
        public Nullable<int> topmargin { get; set; }
        public Nullable<int> leftmargin { get; set; }
        public string tobankline1 { get; set; }
        public string tobankline2 { get; set; }
    
        public virtual organizations organizations { get; set; }
    }
}
