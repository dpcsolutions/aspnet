//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Calani.BusinessObjects.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class tasklists
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tasklists()
        {
            this.tasklistanswers = new HashSet<tasklistanswers>();
            this.taskliststemplates = new HashSet<taskliststemplates>();
        }
    
        public long id { get; set; }
        public string internalId { get; set; }
        public System.DateTime date { get; set; }
        public long clientid { get; set; }
        public long contactId { get; set; }
        public long jobId { get; set; }
        public bool isCompleted { get; set; }
        public bool isTemplate { get; set; }
        public string cancelReason { get; set; }
        public int recordStatus { get; set; }
        public int listStatus { get; set; }
    
        public virtual clients clients { get; set; }
        public virtual contacts contacts { get; set; }
        public virtual jobs jobs { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tasklistanswers> tasklistanswers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<taskliststemplates> taskliststemplates { get; set; }
    }
}
