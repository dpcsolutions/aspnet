﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Costs;
using Calani.BusinessObjects.Membership;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.MobileService2
{
    public partial class MobileService2 : IMobileService2
    {
        public List<Data.Expense> GetExpenses(string token)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            List<Data.Expense> ret = new List<Data.Expense>();
            ExpenseManager mgr = new ExpenseManager(t.OrganizationId);

            List<ExpenseExtended> data = null;

            if (t.UserId != -1 && (Calani.BusinessObjects.Contacts.ContactTypeEnum)Convert.ToInt32(t.Role) == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
            {
                data = mgr.ListExpenses2(null, null, null, null, DataHelper._dateFormat);
            }
            else
                data = mgr.ListExpenses2(null, t.UserId, null, null, DataHelper._dateFormat);

            foreach (var item in data)
            {
                var x = new Data.Expense(item);

                // picture Url
                MobileId mid = new MobileId(x.id);
                if (!mid.IsMobile && mid.DbId != null && mgr.HasPhoto(mid.DbId.Value))
                {
                    if (!String.IsNullOrWhiteSpace(item.photo))
                    {
                        x.pictureUrl = GetUrl("{token}/expenseImage/db:" + mid.DbId.Value);
                    }
                }
                // -->

                ret.Add(x);
            }
            return ret;
        }

        public System.IO.Stream GetExpenseImage(string token, string id)
        {
            System.IO.Stream ret = null;
            TokenManager t = new TokenManager();
            t.ParseToken(token);


            MobileId mid = new MobileId(id);
            if (!mid.IsMobile)
            {
                ExpenseManager mgr = new ExpenseManager(t.OrganizationId);
                var exp = mgr.Get(mid.DbId.Value);

                if (exp != null && mgr.HasPhoto(mid.DbId.Value))
                {
                    byte[] bin = mgr.GetPhoto(mid.DbId.Value);
                    ret = new System.IO.MemoryStream(bin);
                    SetMimeType("image/jpeg");
                    
                    return ret;
                }

            }
            return null;
        }

        public Data.Expense SaveExpense(string token, Data.Expense expense)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            _log.Info($"SaveExpense. expenseId:{expense.id}, orgId:{t.OrganizationId}, userId:{t.UserId}");

            ExpenseManager mgr = new ExpenseManager(t.OrganizationId);


            MobileId mid = new MobileId(expense.id);
            Generic.SmartAction<Model.expenses> action;
            if (!mid.IsMobile)
            {
                expenses c = mgr.Get(mid.DbId.Value);
                expense.UpdateEntity(c, t.OrganizationId, t.UserId);
                action = mgr.Update(mid.DbId.Value, c);
            }
            else
            {
                expenses c = new Model.expenses();
                expense.UpdateEntity(c, t.OrganizationId, t.UserId);
                action = mgr.Add(c);
            }



            var ret = new Data.Expense(action.Record);
            mid = new MobileId(ret.id);
            if (expense.pictureToUpload != null && action.Success)
            {
                // upload new picture
                mgr.SetPicture(mid.DbId.Value, expense.pictureToUpload);
                // -->
            }

            // refresh picture Url
            if (!mid.IsMobile && mid.DbId != null && mgr.HasPhoto(mid.DbId.Value))
            {
                ret.pictureUrl = GetUrl("{token}/expenseImage/db:" + mid.DbId.Value);
            }
            // -->


            return ret;
        }

        public bool DeleteExpense(string token, string id)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            _log.Info($"DeleteExpense. expenseId:{id}, orgId:{t.OrganizationId}, userId:{t.UserId}");

            MobileId mid = new MobileId(id);
            ExpenseManager mgr = new ExpenseManager(t.OrganizationId);




            if (!mid.IsMobile)
            {
                if (mgr.HasPhoto(mid.DbId.Value)) mgr.DeletePhoto(mid.DbId.Value);
                return mgr.Delete(mid.DbId.Value).Success;
            }


            return false;
        }

        public List<Data.Currency> GetCurrencies(string token)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);
           
            long orgId =  t.OrganizationId;

            CustomerAdmin.OrganizationManager orgmgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(orgId);
            orgmgr.Load();

            var mgr = new Currency.CurrenciesManager(orgId);
            List<Data.Currency> cur = new List<Data.Currency>();


            if (orgmgr.Currency != null)
            {
                var orgCurrency = new Data.Currency();
                orgCurrency.id = null;
                orgCurrency.symbol = orgmgr.Currency;
                orgCurrency.rate = 1;
                orgCurrency.isdefault = true;
                orgCurrency.currency = orgmgr.Currency;
                cur.Add(orgCurrency);
            }

            foreach (var item in mgr.List())
            {
                var currency = new Data.Currency();
                currency.id = MobileId.FromDb(item.id);
                currency.symbol = item.symbol;
                currency.rate = item.rate;
                currency.isdefault = false;
                currency.currency = item.currency1;


                cur.Add(currency);
            }

            return cur;
        }

        public List<Data.PayType> GetPayTypes(string token)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);
            var cultureStr = UserCultureManager.GetUserCulture(t.UserId) ?? "fr-FR";
            var culture = new CultureInfo(cultureStr);
            return ExpenseManager.GetPayTypes(culture);
        }
    }
}
