﻿using System;
using Calani.BusinessObjects.Admin;
using Calani.BusinessObjects.Calendars;
using Calani.BusinessObjects.MobileService2.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.MobileService2
{
    [ServiceContract(SessionMode = SessionMode.Allowed)]
    public interface IMobileService2
    {
        [OperationContract]
        [WebGet(UriTemplate = "{token}/absencerequests", ResponseFormat = WebMessageFormat.Json)]
        List<AbsenceRequest> GetAbsenceRequests(string token);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/absencerequests/{id}", ResponseFormat = WebMessageFormat.Json)]
        AbsenceRequest GetAbsenceRequest(string token, string id);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/holiday", ResponseFormat = WebMessageFormat.Json)]
        List<Data.CalendarRec> GetHolidayData(string token);
        
        [OperationContract]
        [WebGet(UriTemplate = "{token}/holidaydatabydate?startDate={startDate}&endDate={endDate}", ResponseFormat = WebMessageFormat.Json)]
        List<Data.CalendarRec> GetHolidayDataByDate(string token, DateTime startDate, DateTime endDate);
        
        [OperationContract]
        [WebInvoke(UriTemplate = "{token}/absencerequests", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        AbsenceRequest SaveAbsenceRequest(string token, AbsenceRequest model);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/client?light={light}", ResponseFormat = WebMessageFormat.Json)]
        List<Data.Client> GetClients(string token, bool light);


        [OperationContract]
        [WebGet(UriTemplate = "{token}/orgsetting", ResponseFormat = WebMessageFormat.Json)]
        OrgSettings GetOrgSettings(string token);
        

       [OperationContract]
        [WebGet(UriTemplate = "{token}/client/{id}", ResponseFormat = WebMessageFormat.Json)]
        Data.ClientDetails GetClient(string token, string id);

        [OperationContract]
        [WebInvoke(UriTemplate = "{token}/client", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Data.Client SaveClient(string token, Data.Client client);

        [OperationContract]
        [WebInvoke(UriTemplate = "{token}/client/{id}", Method = "DELETE", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteClient(string token, string id);


        [OperationContract]
        [WebGet(UriTemplate = "{token}/paytype", ResponseFormat = WebMessageFormat.Json)]
        List<Data.PayType> GetPayTypes(string token);


        [OperationContract]
        [WebGet(UriTemplate = "{token}/expense", ResponseFormat = WebMessageFormat.Json)]
        List<Data.Expense> GetExpenses(string token);


        [OperationContract]
        [WebGet(UriTemplate = "{token}/currencies", ResponseFormat = WebMessageFormat.Json)]
        List<Data.Currency> GetCurrencies(string token);

        [OperationContract]
        [WebInvoke(UriTemplate = "{token}/expense", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Data.Expense SaveExpense(string token, Data.Expense expense);

        [OperationContract]
        [WebInvoke(UriTemplate = "{token}/expense/{id}", Method = "DELETE", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteExpense(string token, string id);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/expenseImage/{id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        System.IO.Stream GetExpenseImage(string token, string id);






        [OperationContract]
        [WebGet(UriTemplate = "{token}/meeting?client={client}", ResponseFormat = WebMessageFormat.Json)]
        List<Data.Meeting> GetMeetings(string token, string client);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/tax", ResponseFormat = WebMessageFormat.Json)]
        List<Data.Taxes> GetTaxes(string token);

        [OperationContract]
        [WebInvoke(UriTemplate = "{token}/tax", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        Data.Taxes SaveTax(string token, Data.Taxes tax);


        [OperationContract]
        [WebInvoke(UriTemplate = "{token}/service", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        Data.Service SaveService(string token, Data.Service service);

        [OperationContract]
        [WebInvoke(UriTemplate = "{token}/meeting", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Data.Meeting SaveMeeting(string token, Data.Meeting expense);

        [OperationContract]
        [WebInvoke(UriTemplate = "{token}/meeting/{id}", Method = "DELETE", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteMeeting(string token, string id);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/project/{projectIdStr}", ResponseFormat = WebMessageFormat.Json)]
        Data.Project GetProject(string token, string projectIdStr);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/project?details={details}&client={client}", ResponseFormat = WebMessageFormat.Json)]
        List<Data.Project> GetProjects(string token, bool details, string client);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/openproject", ResponseFormat = WebMessageFormat.Json)]
        List<Data.Project> GetOpenProjects(string token);

        [OperationContract]
        [WebInvoke(UriTemplate = "{token}/project", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Data.Project SaveProject(string token, Data.Project project);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/quote/{quoteId}", ResponseFormat = WebMessageFormat.Json)]
        Data.Quote GetQuote(string token, string quoteId);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/quote?details={details}&client={client}", ResponseFormat = WebMessageFormat.Json)]
        List<Data.Quote> GetQuotes(string token, bool details, string client);

        [OperationContract]
        [WebInvoke(UriTemplate = "{token}/quote", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Data.Quote SaveQuote(string token, Data.Quote project);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/invoice/{invoiceId}", ResponseFormat = WebMessageFormat.Json)]
        Data.Invoice GetInvoice(string token, string invoiceId);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/invoice?details={details}&client={client}", ResponseFormat = WebMessageFormat.Json)]
        List<Data.Invoice> GetInvoices(string token, bool details, string client);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/quotepdf/{projectId}/{*includeattachmentsids}")]
        System.IO.Stream GenQuotePdf(string token, string projectId, string includeattachmentsids = "");

        [OperationContract]
        [WebGet(UriTemplate = "{token}/invoicepdf/{projectId}/{*includeattachmentsids}")]
        System.IO.Stream GenInvoicePdf(string token, string projectId, string includeattachmentsids = "");



        [OperationContract]
        [WebGet(UriTemplate = "{token}/deliverynotepdf/{projectId}/{*includeattachmentsids}")]
        System.IO.Stream GenDeliveryNotesPdf(string token, string projectId, string includeattachmentsids = "");
        


        [OperationContract]
        [WebInvoke(UriTemplate = "{token}/invoice", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Data.Invoice SaveInvoice(string token, Data.Invoice invoice);

        [OperationContract]
        [WebInvoke(UriTemplate = "subscribe", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Data.SubscriptionResponse Subscribe(Data.SubscriptionRequest sub);

        [OperationContract]
        [WebInvoke(UriTemplate = "login", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Data.SubscriptionResponse Login(string token);



        [OperationContract]
        [WebGet(UriTemplate = "{token}/service", ResponseFormat = WebMessageFormat.Json)]
        List<Data.Service> GetServices(string token);

       /* [OperationContract]
        [WebGet(UriTemplate = "getInternalServices/{token}", ResponseFormat = WebMessageFormat.Json)]
        List<Data.Service> GetInternalServices(string token);*/

        [OperationContract]
        [WebGet(UriTemplate = "{token}/employee", ResponseFormat = WebMessageFormat.Json)]
        List<Data.Employee> GetEmployees(string token);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/employee/{id}", ResponseFormat = WebMessageFormat.Json)]
        Data.Employee GetEmployeeDetailed(string token, string id);

        [OperationContract]
        [WebGet(UriTemplate = "ip/", ResponseFormat = WebMessageFormat.Json)]
        string ClientIp();

        /*

        [OperationContract]
        [WebInvoke(UriTemplate = "deleteProjects/{token}?id={id}", Method = "DELETE", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteProjects(string token, string id);*/



        [OperationContract]
        [WebGet(UriTemplate = "{token}/profilepict", ResponseFormat = WebMessageFormat.Json)]
        Stream GetProfilePict(string token);

        [OperationContract]
        [WebInvoke(UriTemplate = "{token}/profilepict", Method = "POST", ResponseFormat = WebMessageFormat.Json)]
        string SetProfilePict(string token);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/formdata?after={after}", ResponseFormat = WebMessageFormat.Json)]
        List<FormData> GetFormData(string token, string after);


        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{token}/formdata", ResponseFormat = WebMessageFormat.Json)]
        FormData AddOrUpdateForm(string token, FormData timesheetrecord);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/timesheet?light={light}", ResponseFormat = WebMessageFormat.Json)]
        List<TimesheetRecord> GetTimesheetRecords(string token, bool light);

        [OperationContract]
        [WebInvoke(UriTemplate = "{token}/timesheet?id={id}", Method = "DELETE", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteTimesheet(string token, string id);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{token}/timesheet", ResponseFormat = WebMessageFormat.Json)]
        TimesheetRecord AddOrUpdateTimesheet(string token, TimesheetRecord timesheetrecord);


        [OperationContract]
        [WebGet(UriTemplate = "{token}/attachment/{id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        System.IO.Stream GetAttachment(string token, string id);

        [OperationContract]
        [WebInvoke(UriTemplate = "{token}/attachment/{id}", Method = "DELETE", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        bool DeleteAttachment(string token, string id);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{token}/attachment/{projectId}/{properties}/{*description}", ResponseFormat = WebMessageFormat.Json)]
        Attachment PostAttachment(string token, string projectId, Stream data, string properties = "0", string description = "");

        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "{token}/attachment/{id}/{properties}", ResponseFormat = WebMessageFormat.Json)]
        Attachment UpdateAttachmentProperties(string token, string id, string properties = "0");

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{token}/attachment/invoice/{projectId}/{properties}", ResponseFormat = WebMessageFormat.Json)]
        Data.Invoice PostAttachmentToInvoice(string token, string projectId, Stream data, string properties = "0");

       
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{token}/attachment/quote/{projectId}/{properties}", ResponseFormat = WebMessageFormat.Json)]
        Data.Quote PostAttachmentToQuote(string token, string projectId, Stream data, string properties = "0");

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "{token}/mail/{projectId}", ResponseFormat = WebMessageFormat.Json)]
        Data.MailingContext GetMail(string token, string projectId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{token}/mail", ResponseFormat = WebMessageFormat.Json)]
        bool SendMail(string token, Data.MailingSend send);


        [OperationContract]
        [WebGet(UriTemplate = "{token}/mobiletutorials", ResponseFormat = WebMessageFormat.Json)]
        List<TutorialModel> GetTutorials(string token);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/mobiletutorial/{id}/{version}", ResponseFormat = WebMessageFormat.Json)]
        TutorialModel GetTutorialDetails(string token, string id, string version);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/deliverynote/{deliveryNoteIdStr}", ResponseFormat = WebMessageFormat.Json)]
        Data.DeliveryNote GetDeliveryNote(string token, string deliveryNoteIdStr);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/deliverynote?details={details}&client={client}", ResponseFormat = WebMessageFormat.Json)]
        List<Data.DeliveryNote> GetDeliveryNotes(string token, bool details, string client);

        [OperationContract]
        [WebInvoke(UriTemplate = "{token}/deliverynote", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Data.DeliveryNote SaveDeliveryNote(string token, Data.DeliveryNote note);
        
        [Description("Actions: markquotesent, acceptquote, rejectquote")]
        [OperationContract]
        [WebGet(UriTemplate = "{token}/deliverynoteaction/{id}/{action}", ResponseFormat = WebMessageFormat.Json)]
        bool ActionDeliveryNoteDetails(string token, string id, string action);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{token}/signdeliverynote/{id}", ResponseFormat = WebMessageFormat.Json)]
        bool SignDeliveryNote(string token, string id, Stream data);

        [OperationContract]
        [WebGet(UriTemplate = "{token}/timecategory", ResponseFormat = WebMessageFormat.Json)]
        List<ScheduleRecordDto> GetTimeCategories(string token);
        

    }
}
