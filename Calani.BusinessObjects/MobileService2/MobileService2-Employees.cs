﻿using Calani.BusinessObjects.AbsenceRequests;
using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Scheduler;
using Calani.BusinessObjects.TimeManagement;
using Calani.BusinessObjects.TimeManagement.Services;
using Calani.BusinessObjects.WorkSchedulers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Calani.BusinessObjects.MobileService2
{
    public partial class MobileService2 : IMobileService2
    {
        public List<Data.Employee> GetEmployees(string token)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            Calani.BusinessObjects.Contacts.EmployeesManager mgr = new Contacts.EmployeesManager(t.OrganizationId);
            List<Data.Employee> ret = new List<Data.Employee>();

            foreach (var item in mgr.List())
            {
                ret.Add(new Data.Employee
                {
                    id = MobileId.FromDb(item.id),
                    firstname = item.firstName,
                    lastname = item.lastName,
                    email = item.primaryEmail
                });
            }

            return ret;
        }

        public Data.Employee GetEmployeeDetailed(string token, string id)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            var mgr = new Contacts.EmployeesManager(t.OrganizationId);

            var mid = new MobileId(id);
            var item = mid.DbId.HasValue ? mgr.Get(mid.DbId.Value) : null;

            if (item == null)
            {
                return null;
            }

            var userId = mid.DbId.Value;

            var employeesManager = new EmployeesManager(t.OrganizationId);
            var contractsManager = new EmployeesManager.EmployeesContractsManager(t.OrganizationId);
            var workScheduleRecManager = new WorkScheduleRecManager(t.OrganizationId);
            var organizationManager = new CustomerAdmin.OrganizationManager(t.OrganizationId);
            var workScheduleService = new WorkScheduleService(organizationManager, workScheduleRecManager);
            var workDay = workScheduleService.GetWorkDaySettings();
            var workingTimeCalculator = new WorkingTimeCalculator(workDay);
            var absenceRequestsManager = new AbsenceRequestsManager(t.OrganizationId, contractsManager, employeesManager);
            var vacationCarriedOverDaysManager = new VacationCarriedOverDaysManager(t.OrganizationId);
            var vacationReportService = new VacationReportService(employeesManager, contractsManager, organizationManager, absenceRequestsManager,
                vacationCarriedOverDaysManager, workingTimeCalculator);
            var absenceRequestsService = new AbsenceRequestsService(absenceRequestsManager, employeesManager, contractsManager,
                vacationReportService, workingTimeCalculator);
            var timesheetReportService = new TimesheetReportService(contractsManager,
                                                                    new TimeSheetsService(new TimeSheetsManager(t.OrganizationId)),
                                                                    new TimeSheetRecordsManager(t.OrganizationId),
                                                                    new EmployeesOvertimeManager(t.OrganizationId),
                                                                    absenceRequestsManager,
                                                                    organizationManager,
                                                                    workScheduleRecManager);

            var timesheetReport = timesheetReportService.CalculateTimesheetReportForPeriod(t.OrganizationId, item.id, new DateRange(DatesRangeEnum.Since1990));
            timesheetReport.Summary = string.Empty;

            var activeContract = contractsManager.GetActiveContractDto(userId);
            if (activeContract != null)
            {
                activeContract.StartStr = activeContract.StartDate != null ? ((DateTime)activeContract.StartDate).ToString(Tools.IsoLongDateFormat, CultureInfo.InvariantCulture) : "";
                var currentYearRange = new DateRange(DatesRangeEnum.CurrentYear);
                var report = absenceRequestsService.GetAbsenceReports(currentYearRange.Start, currentYearRange.End, new List<long> { userId }).Single();
                activeContract.VacationsRemains = Math.Round((decimal)report.VacationReport.VacationDaysRemaining, 2);
            }

            var ret = new Data.Employee
            {
                id = MobileId.FromDb(item.id),
                firstname = item.firstName,
                lastname = item.lastName,
                email = item.primaryEmail,
                timesheetReport = timesheetReport,
                activeContract = activeContract,
            };

            return ret;
        }

    }
}
