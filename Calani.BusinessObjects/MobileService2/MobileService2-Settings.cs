﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Admin;
using Calani.BusinessObjects.Attachments;
using Calani.BusinessObjects.MobileService2.Data;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Projects;

namespace Calani.BusinessObjects.MobileService2
{
    public partial class MobileService2 : IMobileService2
    {
        public Data.OrgSettings GetOrgSettings(string token)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            Calani.BusinessObjects.CustomerAdmin.OrganizationManager mgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(t.OrganizationId);
            mgr.Load();
            var dto = mgr.ToDto();

            return dto;
        }

       
    }
}
