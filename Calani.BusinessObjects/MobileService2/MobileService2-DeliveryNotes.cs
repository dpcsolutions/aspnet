﻿using Calani.BusinessObjects.Attachments;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.MobileService2.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Web;

namespace Calani.BusinessObjects.MobileService2
{
    public partial class MobileService2 : IMobileService2
    {
        public Data.DeliveryNote GetDeliveryNote(string token, string deliveryNoteIdStr)
        {
            var jobExt = GetJobById(token, deliveryNoteIdStr);

            if (jobExt == null) {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                return null;
            }

            var deliveryNote = new Data.DeliveryNote(jobExt, true);

            return deliveryNote;
        }

        public List<Data.DeliveryNote> GetDeliveryNotes(string token, bool details, string client)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            Calani.BusinessObjects.Membership.LoginManager logmgr = new Calani.BusinessObjects.Membership.LoginManager();
            var user = logmgr.GetUser(t.UserId);
            
            MobileId filterClient = new MobileId(client);

            var ret = new List<Data.DeliveryNote>();
            var mgr = new Projects.ProjectsManager(t.OrganizationId);
            var attmgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
            var orgManager = new OrganizationManager(t.OrganizationId);
            orgManager.Load();

            attmgr.OrganizationId = t.OrganizationId;

            long? assigneeId = null;
            if (user.type.HasValue && (Calani.BusinessObjects.Contacts.ContactTypeEnum)user.type == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Employee)
            {
                assigneeId = user.id;
            }

            var list = mgr.ListDeliveryNotes(custid: filterClient.DbId, assigneeId: assigneeId, includeServices: true);

            foreach (var item in list)
            {
                if (String.IsNullOrWhiteSpace(item.internalId))
                    item.internalId = "Open";

                if (!String.IsNullOrWhiteSpace(item.description))
                {
                    item.description = item.description.Replace("\r", " ");
                    item.description = item.description.Replace("\n", " ");
                    item.description = item.description.Replace("  ", " ");
                }


                var proj = new Data.DeliveryNote(item, details);

                
                proj.pdfUrl = GetUrl("{token}/deliverynotepdf/db." + item.id);

                if (details)
                {
                    var attachments = attmgr.ListJobAttachments(item.id, true,false);
                    proj.SetAttachments(attachments);

                }

                ret.Add(proj);
            }
            return ret;
        }

       
        public Data.DeliveryNote SaveDeliveryNote(string token, Data.DeliveryNote note)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            _log.Info($"SaveDeliveryNote. noteId:{note.id},  orgId:{t.OrganizationId}, userId:{t.UserId}");

            CustomerAdmin.TaxesManager taxMgr = new CustomerAdmin.TaxesManager(t.OrganizationId);
            string defaultTax = taxMgr.GetDetaultTaxName();
            var userId = t.UserId;

            var mid = new MobileId(note.id);
            
           
            // build the same ajax query can the complete app
            AjaxService.ProjectUpdate q = new AjaxService.ProjectUpdate();
            q.action = "save:deliverynote";

            // first copy existing value project in it
            Projects.ProjectsManager mgr = new Projects.ProjectsManager(t.OrganizationId);


            if (String.IsNullOrWhiteSpace(note.internalId))
            {
                note.internalId = mgr.GetLastDeliveryNoteInternalId();
            }

            if (mid.DbId != null)
            {
                var dbrecord = mgr.Get(mid.DbId.Value);
                if (dbrecord != null)
                {
                    q.action = null;
                    q.batch = dbrecord.contractualUnitsCount;

                    if (dbrecord.clientAddress != null)
                        q.clientAddress = dbrecord.clientAddress.Value;

                    if (dbrecord.clientContact != null)
                        q.clientContact = dbrecord.clientContact.Value;

                    q.clientId = dbrecord.clientId;
                    q.description = dbrecord.description;

                    if (dbrecord.discount != null)
                        q.discount = dbrecord.discount.Value;

                    q.id = mid.DbId.Value;
                    q.internalId = dbrecord.internalId;
                    q.payments = (from r in dbrecord.payments.ToList() select new Projects.paymentExtended(r)).ToList();

                    if (dbrecord.rating != null)
                        q.rating = dbrecord.rating.Value;

                    //remove when MobileApp updated
                    q.assigneeId = dbrecord.assigneeId;

                    q.deliveredTo = dbrecord.deliveredTo;
                }
            }

            // it is a new project set default value
            if (q.id <= 0)
            {
                q.action = null;
                q.internalId = new Projects.jobsExtended(false, false, true, false, t.OrganizationId).internalId;
                q.id = -1;
            }

            // map new properties
            q.clientId = new MobileId(note.clientId).DbId.Value;
            CustomerAdmin.ClientsManager custMgr = new CustomerAdmin.ClientsManager(t.OrganizationId);
            var cust = custMgr.Get(q.clientId);

            if (String.IsNullOrWhiteSpace(note.clientAddressId))
            {
                if (q.clientAddress == 0) q.clientAddress = (from r in cust.addresses select r.id).FirstOrDefault();
            }
            else
            {
                var checkid = new MobileId(note.clientAddressId);
                if (checkid.DbId != null) q.clientAddress = (from r in cust.addresses where r.id == checkid.DbId.Value select r.id).FirstOrDefault();
                else q.clientAddress = (from r in cust.addresses select r.id).FirstOrDefault();
            }

            if (String.IsNullOrWhiteSpace(note.clientContactId))
            {
                if (q.clientContact == 0)
                    q.clientContact = (from r in cust.contacts select r.id).FirstOrDefault();
            }
            else
            {
                var checkid = new MobileId(note.clientContactId);
                if (checkid.DbId != null)
                    q.clientContact = (from r in cust.contacts where r.id == checkid.DbId.Value select r.id).FirstOrDefault();
                else
                    q.clientContact = (from r in cust.contacts select r.id).FirstOrDefault();
            }

            q.description = note.description;
            q.comment = note.comment;
            q.deliveryDate = note.deliveryDate;
            q.linkedjobid = (long)note.linkedjobid;

            if (!String.IsNullOrEmpty(note.clientId))
            {
                var clientMid = new MobileId(note.clientId);
                q.clientId = clientMid.DbId.Value;
            }

            if (!String.IsNullOrEmpty(note.assigneeId))
            {
                var assigneeMid = new MobileId(note.assigneeId);
                q.assigneeId = assigneeMid.DbId.Value;
            }

            if (!String.IsNullOrWhiteSpace(note.deliveredTo))
            {                
                q.deliveredTo = note.deliveredTo;
            }

            if (!String.IsNullOrWhiteSpace(note.currency))
            {
                q.currency = note.currency;
            }

            // map new items (invoice lines)
            q.items = note.lines.Select(item => ToAjaxServiceProjectItem(item)).ToList();
            note.lines = note.lines == null ? new List<ProjectItem>() : note.lines;

            // do the update
            q.action = "save:deliverynote";
            Projects.ProjectItemsUpdater itemsUpdater = new Projects.ProjectItemsUpdater();
            itemsUpdater.OrganizationId = Convert.ToInt64(t.OrganizationId);
            itemsUpdater.ProjectId = q.id;
            itemsUpdater.Update = q;
            itemsUpdater.Update.payments = null;
            itemsUpdater.UpdateDb(userId, q.action);

            if (note.action != null && note.action != "save:deliverynote")
            {
                Projects.ProjectActionManager action = new Projects.ProjectActionManager();
                action.Culture = new System.Globalization.CultureInfo("fr-FR");
                action.OrganizationId = t.OrganizationId;
                action.ProjectId = itemsUpdater.ProjectId;
                action.Action = note.action;
                action.UserId = t.UserId;
                action.DoAction();
            }
            // -->


            long projectSavedId = itemsUpdater.ProjectId;
            return GetDeliveryNote(projectSavedId, t.OrganizationId);
        }

        private Data.DeliveryNote GetDeliveryNote(long projectSavedId, long organizationId)
        {
            Projects.ProjectsManager mgr = new Projects.ProjectsManager(organizationId);
            var projectSaved = mgr.Get(projectSavedId);

            var projectSavedExt = new Projects.jobsExtended(projectSaved,false);

            var ret = new Data.DeliveryNote(projectSavedExt, true);
            Calani.BusinessObjects.Attachments.AttachmentsManager attmgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
            attmgr.OrganizationId = organizationId;
            var attachments = attmgr.ListJobAttachments(projectSavedExt.id, true);
            ret.SetAttachments(attachments);

            ret.pdfUrl = GetUrl("{token}/deliverynotepdf/" + ret.id);

            return ret;
        }

        public bool ActionDeliveryNoteDetails(string token, string id,  string action)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);
            var mid = new MobileId(id);

            var ret = false;

            _log.Info($"ActionDeliveryNoteDetails. noteId:{id}, action:{action},  orgId:{t.OrganizationId}, userId:{t.UserId}");

            if (t.UserId != -1 && mid.DbId.HasValue )
            {
                Projects.ProjectActionManager mgr = new Projects.ProjectActionManager();

                mgr.Culture = new System.Globalization.CultureInfo("fr-FR");
                mgr.OrganizationId = t.OrganizationId;
                mgr.ProjectId = mid.DbId.Value;
                mgr.Action = action;
                mgr.UserId = t.UserId;
                ret = mgr.DoAction();

            }

            return ret;
        }


        public bool SignDeliveryNote(string token, string id, System.IO.Stream data)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            var mgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
            mgr.OrganizationId = t.OrganizationId;

            _log.Info($"SignDeliveryNote. noteId:{id}, orgId:{t.OrganizationId}, userId:{t.UserId}");

            MobileId mid = new MobileId(id);

            AntsCode.Util.MultipartParser parser = new AntsCode.Util.MultipartParser(data);

            string mime = parser.ContentType;
            byte[] content = parser.FileContents;
            string name = parser.Filename;



            var res = mgr.UploadAttachment(name, mime, null, content, jobId: mid.DbId.Value, userId: null, AttachmentTypeEnum.DeliveryNoteSign);


            return res > 0;
        }
    }
}
