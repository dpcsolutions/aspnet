﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.MobileService2
{
    public class DataHelper
    {
        public const string _dateFormat = "yyyy-MM-dd HH:mm:ss";
        public static string FormatDate(DateTime? dt)
        {
            if (dt == null) return String.Empty;
            if(dt.Value.Year < 1) return String.Empty;
            return dt.Value.ToString(_dateFormat);   
        }

        internal static DateTime? ParseDate(string date)
        {
            if (!String.IsNullOrWhiteSpace(date))
            {
                DateTime dt = new DateTime(1, 1, 1);
                if(DateTime.TryParseExact(date, _dateFormat, System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeLocal, out dt))
                {
                    if (dt.Year > 1)
                    {
                        return dt;
                    }
                }
            }
            return null;
        }
    }
}
