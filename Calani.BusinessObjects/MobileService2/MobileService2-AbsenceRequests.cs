﻿using System;
using Calani.BusinessObjects.AbsenceRequests;
using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Membership;
using Calani.BusinessObjects.MobileService2.Data;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Scheduler;
using Calani.BusinessObjects.WorkSchedulers;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;

namespace Calani.BusinessObjects.MobileService2
{
    public partial class MobileService2 : IMobileService2
    {
        public List<Data.CalendarRec> GetHolidayData(string token)
        {
            var result = new List<CalendarRec>();
            var t = new TokenManager();
            t.ParseToken(token);
            
            var orgId = t.OrganizationId;
            var db = new CalaniEntities();

            var holidayList = db.calendars_rec
                .Where(x => x.name == "Férié" && x.calendars.organization_Id == orgId && x.recordStatus != -1)
                .Include(x => x.calendars)
                .ToList();
            
            if (holidayList.Any())
            {
                foreach (var hol in holidayList)
                {
                    if (hol.start_date.Value.DayOfWeek == DayOfWeek.Saturday 
                        || hol.start_date.Value.DayOfWeek == DayOfWeek.Sunday) continue;
                    result.Add(new CalendarRec
                    {
                        start_date = hol.start_date,
                        end_date = hol.end_date,
                        updated_by = hol.updated_by,
                        recordStatus = hol.recordStatus,
                        id = hol.id,
                        name = hol.name,
                        calendar_id = hol.calendar_id,
                        updated_at = hol.updated_at
                    });
                }
            }
            return result;
        }
        
        public List<Data.CalendarRec> GetHolidayDataByDate(string token, DateTime startDate, DateTime endDate)
        {
            var result = GetHolidayData(token).Where(x =>
                x.start_date != null && x.start_date.Value.Date >= startDate.Date
                    && x.end_date != null && x.end_date.Value.Date <= endDate.Date).ToList();
            
            return result;
        }
        
        public List<AbsenceRequest> GetAbsenceRequests(string token)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            var logmgr = new LoginManager();
            var user = logmgr.GetUser(t.UserId);

            long? assigneeId = null;
            if (user.type.HasValue && (ContactTypeEnum)user.type == ContactTypeEnum.User_Employee)
            {
                assigneeId = user.id;
            }

            var last2YearsRange = new DateRange(Enums.DatesRangeEnum.Last2Years);

            var manager = new AbsenceRequestsManager(t.OrganizationId);
            var orgManager = new OrganizationManager(t.OrganizationId);
            var workScheduleRecManager = new WorkScheduleRecManager(t.OrganizationId);
            var workScheduleService = new WorkScheduleService(orgManager, workScheduleRecManager);
            var workDay = workScheduleService.GetWorkDaySettings();
            var workingTimeCalculator = new WorkingTimeCalculator(workDay);

            var db = new CalaniEntities();
            var holidayList = db.calendars_rec
                .Where(x => x.name == "Férié" && x.calendars.organization_Id == t.OrganizationId && x.recordStatus != -1)
                .Include(x => x.calendars)
                .ToList();
            
            var list = manager.ListAbsenceRequests(employee: assigneeId, dtFrom: last2YearsRange.Start)
                .Select(e => ToModel(e, workingTimeCalculator, holidayList))
                .ToList();

            return list;
        }

        public AbsenceRequest GetAbsenceRequest(string token, string id)
        {
            AbsenceRequest result = null;

            TokenManager t = new TokenManager();
            t.ParseToken(token);

            var logmgr = new LoginManager();
            var user = logmgr.GetUser(t.UserId);
            var isAdmin = user.type.HasValue && (ContactTypeEnum)user.type == ContactTypeEnum.User_Admin;

            long entityId = 0;
            long.TryParse(id, out entityId);

            if (entityId > 0)
            {
                var manager = new AbsenceRequestsManager(t.OrganizationId);
                var entity = manager.Get(entityId);

                if(entity != null)
                {
                    var hasPermission = isAdmin || entity.employeeId == t.UserId;
                    if (hasPermission)
                    {
                        var workingTimeCalculator = GetWorkingTimeCalculator(t.OrganizationId);
                        result = ToModel(entity, workingTimeCalculator);
                        
                        var db = new CalaniEntities();
                        var holidayList = db.calendars_rec
                            .Where(x => x.name == "Férié" && x.calendars.organization_Id == t.OrganizationId && x.recordStatus != -1)
                            .Include(x => x.calendars)
                            .ToList();
            
                        var holidays = holidayList.Where(x =>
                            x.start_date != null && x.start_date.Value.Date >= entity.startDate.Date
                                                 && x.end_date != null && x.end_date.Value.Date <= entity.endDate.Date).ToList();
            
                        if (holidays.Any())
                        {
                            foreach (var hol in holidays)
                            {
                                if (hol.start_date.Value.DayOfWeek == DayOfWeek.Saturday 
                                    || hol.start_date.Value.DayOfWeek == DayOfWeek.Sunday) continue;
                                result.durationDays--;
                            }
                        }
            
                    }
                }
            }

            return result;
        }

        public AbsenceRequest SaveAbsenceRequest(string token, AbsenceRequest model)
        {
            AbsenceRequest result = null;

            var t = new TokenManager();
            t.ParseToken(token);

            _log.Info($"SaveAbsenceRequest. id:{model.id}, orgId:{t.OrganizationId}, userId:{t.UserId}");

            var manager = new AbsenceRequestsManager(t.OrganizationId);

            var user = new LoginManager().GetUser(t.UserId);
            var isAdmin = user.type.HasValue && (ContactTypeEnum)user.type == ContactTypeEnum.User_Admin;
            
            var mid = new MobileId(model.id);
            absence_requests existingEntity = null;
            if (mid.DbId.HasValue)
            {
                existingEntity = manager.Get(mid.DbId.Value);
            }

            absence_requests entity = ToEntity(model, t, existingEntity);

            var requests = manager.List();
            if (requests.Any(x => 
                x.id != (mid.DbId ?? 0)
                && x.startDate == entity.startDate 
                && x.endDate == entity.endDate 
                && x.employeeId == entity.employeeId
                && x.rejectedBy == null
                && x.cancellationApprovedBy == null))
            {
                throw new ArgumentException(nameof(entity.employeeId), "You can't create this request, same already exists.");
            }
            
            var hasPermission = isAdmin || entity.employeeId == t.UserId;

            if (hasPermission && entity.employeeId > 0)
            {
                var r = manager.SaveOrUpdate(entity.id, entity);

                if (r.Success)
                {
                    var workingTimeCalculator = GetWorkingTimeCalculator(t.OrganizationId);
                    result = ToModel(r.Record, workingTimeCalculator);
                }
            }

            return result;
        }

        #region Private Methods

        private WorkingTimeCalculator GetWorkingTimeCalculator(long organizationId)
        {
            var orgManager = new OrganizationManager(organizationId);
            var workScheduleRecManager = new WorkScheduleRecManager(organizationId);
            var workScheduleService = new WorkScheduleService(orgManager, workScheduleRecManager);
            var workDay = workScheduleService.GetWorkDaySettings();
            var workingTimeCalculator = new WorkingTimeCalculator(workDay);

            return workingTimeCalculator;
        }

        private AbsenceRequest ToModel(absence_requests entity, WorkingTimeCalculator workingTimeCalculator, List<calendars_rec> holidayList = null)
        {
            var model = new AbsenceRequest()
            {
                id = MobileId.FromDb(entity.id),
                employeeId = MobileId.FromDb(entity.employeeId),
                employeeName = entity.contacts4 != null ? $"{entity.contacts4.firstName} {entity.contacts4.lastName}" : "",
                absenceCategory = (AbsenceCategoryEnum)entity.absenceCategory,
                startDate = entity.startDate.ToString(Tools.IsoLongDateFormat, CultureInfo.InvariantCulture),
                endDate = entity.endDate.ToString(Tools.IsoLongDateFormat, CultureInfo.InvariantCulture),

                validationStatus = entity.approvedAt != null ? ValidationEnum.Approved :
                    entity.rejectedAt != null ? ValidationEnum.Rejected :
                    ValidationEnum.Pending,

                cancellationStatus = entity.cancellationApprovedAt != null ? CancellationStatusEnum.Cancelled :
                        entity.cancellationRejectedAt != null ? CancellationStatusEnum.CancellationRejected :
                        CancellationStatusEnum.CancellationRequested,

                comment = entity.comment,

                durationDays = (decimal)workingTimeCalculator.CalculateAbsenceDays(entity.startDate, entity.endDate, holidayList),
            };

            return model;
        }

        private absence_requests ToEntity(AbsenceRequest model, TokenManager token, absence_requests entity = null)
        {
            if (entity == null)
            {
                entity = new absence_requests()
                {
                    id = -1,
                    createdAt = System.DateTime.Now,
                    createdBy = token.UserId
                };
            }
            else
            {
                entity.updatedAt = System.DateTime.Now;
                entity.updatedBy = token.UserId;
            }

            var empMid = new MobileId(model.employeeId);

            entity.organizationId = token.OrganizationId;
            entity.startDate = System.DateTime.ParseExact(model.startDate, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);
            entity.endDate = System.DateTime.ParseExact(model.endDate, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);
            entity.employeeId = empMid.DbId.Value;
            entity.absenceCategory = (int)model.absenceCategory;
            entity.comment = model.comment;

            return entity;
        }

        #endregion Private Methods

    }
}
