﻿using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.MobileService2.Data;
using Calani.BusinessObjects.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Calani.BusinessObjects.MobileService2
{
    public class StructTime
    {
        public StructTime() { }
        public DateTime dtFirstDayOfWeek { get; set; }
        public DateTime[] datesstr { get; set; }
        public String email { get; set; }
        public bool[] debours { get; set; }
        public bool[] piquets { get; set; }
        public double[] autres { get; set; }
    }

    public class TimesheetManager : Generic.SmartCollectionManager<timesheets, long>
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public TimesheetManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<timesheets> GetDbSet(Model.CalaniEntities db)
        {
            return db.timesheets;
        }

        internal override SmartQuery<timesheets> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in GetDbSet(q.Context) where r.id == id select r);
            return q;
        }

        public bool delete(long timesheetRecordIdToBeDeleted)
        {
            var res = false;

            using (Model.CalaniEntities db = new CalaniEntities())
            {
                timesheetrecords dbTimeSheetRecord = null;
                dbTimeSheetRecord = db.timesheetrecords.FirstOrDefault(t => t.id == timesheetRecordIdToBeDeleted && t.recordStatus != -1);
                     
                if (dbTimeSheetRecord != null)
                {
                    // mark timesheet record as deleted
                    dbTimeSheetRecord.recordStatus = -1;
                    db.SaveChanges();

                    res = true;
                }
               
                
            }

            return res;
        }

        internal override SmartQuery<timesheets> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            // base
            q.Query = (from v in q.Context.timesheets
                       where v.organizationId == OrganizationID
                       select v);

            if (!IgnoreRecordStatus) q.Query = (from r in q.Query
                                                where r.recordStatus == 0 select r);

            return q;
        }

        private int GetWeekNumber(DateTime dt)
        {
            return CalendarTools.GetNonIsoWeekOfYear(dt);
            /*CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            return weekNum;*/
        }

        private visits getVisit(long visitId)
        {
            return (from f in GetBaseQuery().Context.visits
                          .Include("jobs")
                    where f.id == visitId
                    select f).FirstOrDefault();
        }

        private jobs getJobs(long jobId)
        {
            return (from f in GetBaseQuery().Context.jobs
                    where jobId == f.id
                    select f).FirstOrDefault(); ;
        }

        private services getServices(long serviceId)
        {
            return (from f in GetBaseQuery().Context.services
                    where serviceId == f.id //r.visitId != null && f.id == (long)r.visitId
                    select f).FirstOrDefault();
        }

        private TimesheetRecord FromDb(timesheetrecords r, bool isValidated)
        {
            TimesheetRecord ts = new TimesheetRecord();

            ts.isValidated = isValidated;

            var visit = r.visitId != null ? getVisit((long)r.visitId) : null;

            if (visit != null)
            {
                ts.visitDescription = (!String.IsNullOrEmpty(visit.description) ? visit.description : (visit.jobs != null ? (!String.IsNullOrEmpty(visit.jobs.description) ? visit.jobs.description : "") : ""));
                ts.clientName = visit.jobs != null ? visit.jobs.clients.companyName : "";
                ts.visitId = MobileId.FromDb(visit.id);
            }

            var job = r.jobId != null ? getJobs((long)r.jobId) : null;

            if (job != null)
            {
                ts.projectDescription = (!String.IsNullOrEmpty(job.description) ? job.description : "");
                ts.clientName = job.organizations.name;
                ts.projectId = MobileId.FromDb(job.id);
                ts.clientId = MobileId.FromDb(job.clientId);
            }
            
            var service = r.serviceId != null ? getServices((long)r.serviceId) :null;

            if (service != null)
            {
                ts.serviceDescription = (!String.IsNullOrEmpty(service.description) ? service.description : !String.IsNullOrEmpty(service.description) ? service.name : "");
                ts.serviceId = MobileId.FromDb(service.id);
                ts.serviceType = ts.clientId != null ? "1" : "2";
            }


            if (r.work_schedule_profiles_rec_id.HasValue)
            {
                ts.categoryId = MobileId.FromDb(r.work_schedule_profiles_rec_id.Value);
            }
           


            ts.timeStart = r.startDate != null ? ((DateTime)r.startDate).ToString(Tools.IsoLongDateFormat, CultureInfo.InvariantCulture) : "";
            ts.timeEnd = r.endDate != null ? ((DateTime)r.endDate).ToString(Tools.IsoLongDateFormat, CultureInfo.InvariantCulture) : "";
            ts.id = MobileId.FromDb(r.id);
            ts.rate = r.rate != null ? (double)r.rate : 100;
            ts.description = r.description;
            ts.comment = r.comment;
            ts.recordType = TimeSheetRecordType.Time;
            ts.lunchDuration = r.lunch_duration;
            ts.pauseDuration = r.pause_duration;
            ts.rowType = (TimeSheetRecordRowType) (r.row_type ?? 0);
            ts.isParent = r.timesheetrecords1.Any(c => c.recordStatus !=-1);
            ts.duration = r.duration;
            ts.date = r.startDate.GetValueOrDefault().ToString(Tools.IsoLongDateFormat, System.Globalization.CultureInfo.InvariantCulture);
            
            ts.parentId = r.parent_id;

            var timeSpan = TimeSpan.FromHours(ts.duration.Value);
            ts.enteredDuration = new DateTime(timeSpan.Ticks).ToString("HH:mm");
            
            return ts;
        }

        /* private TimesheetRecord FromDb(timesheet_records_services r, bool isValidated)
         {
             TimesheetRecord ts = new TimesheetRecord();

             ts.isValidated = isValidated;

             var visit = r.visitId != null ? getVisit((long)r.visitId) : null;

             if (visit != null)
             {
                 ts.visitDescription = (!String.IsNullOrEmpty(visit.description) ? visit.description : (visit.jobs != null ? (!String.IsNullOrEmpty(visit.jobs.description) ? visit.jobs.description : "") : ""));
                 ts.clientName = visit.jobs != null ? visit.jobs.clients.companyName : "";
                 ts.visitId = MobileId.FromDb(visit.id);
             }

             var job = r.jobId != null ? getJobs((long)r.jobId) : null;

             if (job != null)
             {
                 ts.projectDescription = (!String.IsNullOrEmpty(job.description) ? job.description : "");
                 ts.clientName = job.organizations.name;
                 ts.projectId = MobileId.FromDb(job.id);
                 ts.clientId = MobileId.FromDb(job.clientId);
             }

             var service = r.serviceId != null ? getServices((long)r.serviceId) : null;

             if (service != null)
             {
                 ts.serviceDescription = (!String.IsNullOrEmpty(service.description) ? service.description : !String.IsNullOrEmpty(service.description) ? service.name : "");
                 ts.serviceId = MobileId.FromDb(service.id);
                 ts.serviceType = ts.clientId != null ? "1" : "2";
             }

             ts.id = MobileId.FromDb(r.id);
             ts.duration = r.duration;
             var timeSpan = TimeSpan.FromHours(ts.duration.Value);
             ts.enteredDuration = new DateTime(timeSpan.Ticks).ToString("HH:mm");
             ts.recordType = TimeSheetRecordType.Service;
             ts.comment = r.comment;
             ts.date = r.date.GetValueOrDefault().ToString(Tools.IsoLongDateFormat);

             return ts;
         }
         */
        public List<TimesheetRecord> ListTimesheets(long? employeeId)
        {
            List<TimesheetRecord> ret = new List<TimesheetRecord>();

            var q = GetBaseQuery();

            DateTime startDate = DateTime.Now.AddMonths(-5);
            DateTime endDate = DateTime.Now.AddMonths(+2);

            var dbrecords = (from r in q.Query.Include("timesheetrecords")
                             where
                             r.employeeId == employeeId &&
                             r.year >= startDate.Year &&
                             r.year <= endDate.Year
                             select r);

            foreach (var timesheet in dbrecords.ToList())
            {
                foreach (var r in timesheet.timesheetrecords.Where(r => r.recordStatus == 0))
                {
                    TimesheetRecord ts = null;

                    try
                    {
                        ts = FromDb(r, timesheet.acceptedDate != null);
                    }
                    catch (Exception e)
                    {
                    }

                    if (ts != null) ret.Add(ts);
                }
            }

            /*temporary for mobile fix*/
            ret = ret.Where(d => !d.parentId.HasValue).ToList();

            return ret;
        }

        public TimesheetRecord updateOrCreate(TimesheetRecord toUpdate, long employeeId, string exportPath = null)
        {
            TimesheetRecord ret = toUpdate;

            var orgManager = new OrganizationManager(this.OrganizationID);
            orgManager.Load();

            using (CalaniEntities db = new CalaniEntities())
            {
                DateTime dt;
                //get timesheet for record
                if (!String.IsNullOrWhiteSpace(toUpdate.date))
                    dt = DateTime.ParseExact(toUpdate.date, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);
                else
                    dt = DateTime.ParseExact(toUpdate.timeStart, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);

                var newStartTime = DateTime.ParseExact(toUpdate.timeStart, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);
                var newEndTime = DateTime.ParseExact(toUpdate.timeEnd, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);

                var year = dt.Year;
                var week = GetWeekNumber(dt);
                
                MobileId id = new MobileId(toUpdate.id);
                
                var lastModificationDateFromMobile = DateTime.ParseExact(toUpdate.lastModificationDate, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);


                // is there a timesheet that covers this week ?
                var q = GetBaseQuery();

                //try to find the timesheet record
                timesheetrecords dbTimeSheetRecord = null;

                timesheets dbTimeSheet = null;
                long tid = 0;

                TimeSpan time;
                if (!String.IsNullOrWhiteSpace(toUpdate.enteredDuration))
                {
                    time = TimeSpan.Parse(toUpdate.enteredDuration);
                }
                else
                {
                    time = newEndTime - newStartTime;
                }
                
                toUpdate.duration = time.TotalHours;

                if (id.DbId.HasValue && id.DbId > 0)
                {
                    dbTimeSheetRecord = (from t in db.timesheetrecords
                        where t.id == id.DbId select t).FirstOrDefault();

                    if (dbTimeSheetRecord != null && dbTimeSheetRecord.timesheetId.HasValue && dbTimeSheetRecord.timesheetId > 0)
                    {
                        tid = dbTimeSheetRecord.timesheetId.Value;
                        dbTimeSheet = dbTimeSheetRecord.timesheets;
                    }
                   
                }
                else
                {
                    // Attempt to add a new timesheet - check if one for the same week exists
                    dbTimeSheet = FindTimesheet(employeeId, year, week);
                }
                
                if (dbTimeSheet == null) //no timesheet add a new one
                {
                    //need to create the ts record first
                    dbTimeSheet = new timesheets()
                    {
                        organizationId = OrganizationID, //change from session
                        year = year,
                        week = week,
                        employeeId = employeeId, //from session
                        sentDate = DateTime.Now,
                        timesheetrecords = new List<timesheetrecords>()
                    };

                    db.timesheets.Add(dbTimeSheet);
                    db.SaveChanges();
                }

                // var pauseExists = dbTimeSheet.timesheetrecords != null &&
                //                   dbTimeSheet.timesheetrecords.Any(r => r.recordStatus == 0 && r.pause_duration > 0 &&
                //                                                        r.startDate.HasValue && r.startDate.Value.Date == dt);
                // if (pauseExists)
                // {
                //     toUpdate.pauseDuration = 0;
                // }
               
                if (dbTimeSheetRecord == null)
                {

                    var enddate = String.IsNullOrWhiteSpace(toUpdate.timeEnd) ? (DateTime?)null :
                        DateTime.ParseExact(toUpdate.timeEnd, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);


                    var startdate = String.IsNullOrWhiteSpace(toUpdate.timeStart) ? (DateTime?)null :
                        DateTime.ParseExact(toUpdate.timeStart, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);
                    
                    //new timeshet record
                    dbTimeSheetRecord = new timesheetrecords()
                    {
                        description = toUpdate.description,
                        comment = toUpdate.comment,
                        startDate = startdate,
                        endDate = enddate,
                        lastModificationDate = DateTime.ParseExact(toUpdate.lastModificationDate, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture),
                        lastModifiedBy = employeeId, //userId from sesseion
                        rate = toUpdate.rate,
                            
                        visitId = new MobileId(toUpdate.visitId).DbId,
                        serviceId = new MobileId(toUpdate.serviceId).DbId,
                        jobId = new MobileId(toUpdate.projectId).DbId,
                        work_schedule_profiles_rec_id = new MobileId(toUpdate.categoryId).DbId,
                        timesheetId = dbTimeSheet.id,

                        lunch_duration = toUpdate.lunchDuration,
                        pause_duration = toUpdate.pauseDuration,
                        row_type = (int?)toUpdate.rowType?? (int)TimeSheetRecordRowType.DataRow,

                };

                    //dbTimeSheetRecord.startDate = dt;

                    if (toUpdate.rowType == TimeSheetRecordRowType.MasterRow)
                    {
                        dbTimeSheetRecord.startDate = dt.Date;
                        dbTimeSheetRecord.endDate = dt.Date;
                        dbTimeSheetRecord.duration = toUpdate.duration;
                        dbTimeSheetRecord.lunch_duration = toUpdate.lunchDuration;
                        dbTimeSheetRecord.pause_duration = toUpdate.pauseDuration;
                    }
                    else if (orgManager.TimeSheetType == (int)TimesheetTypeEnum.SimplifiedEntry)
                    {
                        dbTimeSheetRecord.duration = toUpdate.duration;
                        dbTimeSheetRecord.lunch_duration = toUpdate.lunchDuration;
                        dbTimeSheetRecord.pause_duration = toUpdate.pauseDuration;
                    }
                    else if (dbTimeSheetRecord.endDate != null && dbTimeSheetRecord.startDate != null)
                    {
                        TimeSpan t = ((DateTime)dbTimeSheetRecord.endDate - (DateTime)dbTimeSheetRecord.startDate);
                        TimeSpan h1 = TimeSpan.Parse("1:00:00");

                        dbTimeSheetRecord.duration = (double)t.Ticks / (double)h1.Ticks;
                    }
                    else
                    {
                        dbTimeSheetRecord.duration = 0;
                    }

                    db.timesheetrecords.Add(dbTimeSheetRecord);

                    db.SaveChanges();
                }
                else if (dbTimeSheet.acceptedDate == null)
                {
                    var enddate = String.IsNullOrWhiteSpace(toUpdate.timeEnd) ? (DateTime?)dt :
                        DateTime.ParseExact(toUpdate.timeEnd, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);

                    var startdate = String.IsNullOrWhiteSpace(toUpdate.timeStart) ? (DateTime?)dt :
                        DateTime.ParseExact(toUpdate.timeStart, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);

                    if (dbTimeSheetRecord.lastModificationDate <= DateTime.ParseExact(toUpdate.lastModificationDate, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture))
                    {
                        dbTimeSheetRecord.description = toUpdate.description;
                        dbTimeSheetRecord.endDate = enddate;
                        dbTimeSheetRecord.startDate = startdate;
                        dbTimeSheetRecord.lastModificationDate = DateTime.ParseExact(toUpdate.lastModificationDate, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture);
                        dbTimeSheetRecord.lastModifiedBy = employeeId;
                        dbTimeSheetRecord.rate = toUpdate.rate;
                        dbTimeSheetRecord.startDate = startdate;
                        dbTimeSheetRecord.visitId = new MobileId(toUpdate.visitId).DbId;
                        dbTimeSheetRecord.serviceId = new MobileId(toUpdate.serviceId).DbId;
                        dbTimeSheetRecord.jobId = new MobileId(toUpdate.projectId).DbId;
                        dbTimeSheetRecord.work_schedule_profiles_rec_id = new MobileId(toUpdate.categoryId).DbId;
                        dbTimeSheetRecord.comment = toUpdate.comment;
                        dbTimeSheetRecord.lunch_duration = toUpdate.lunchDuration;
                        dbTimeSheetRecord.pause_duration = toUpdate.pauseDuration;

                        if (toUpdate.rowType == TimeSheetRecordRowType.MasterRow)
                        {
                            dbTimeSheetRecord.startDate = dt.Date;
                            dbTimeSheetRecord.endDate = dt.Date;
                            dbTimeSheetRecord.duration = toUpdate.duration;
                            dbTimeSheetRecord.lunch_duration = toUpdate.lunchDuration;
                            dbTimeSheetRecord.pause_duration = toUpdate.pauseDuration;
                        }
                        else if (orgManager.TimeSheetType == (int)TimesheetTypeEnum.SimplifiedEntry)
                        {
                            dbTimeSheetRecord.duration = toUpdate.duration;
                            dbTimeSheetRecord.lunch_duration = toUpdate.lunchDuration;
                            dbTimeSheetRecord.pause_duration = toUpdate.pauseDuration;
                        }
                        else if (dbTimeSheetRecord.endDate != null && dbTimeSheetRecord.startDate != null)
                        {
                            TimeSpan t = ((DateTime)dbTimeSheetRecord.endDate - (DateTime)dbTimeSheetRecord.startDate);
                            TimeSpan h1 = TimeSpan.Parse("1:00:00");

                            dbTimeSheetRecord.duration = (double)t.Ticks / (double)h1.Ticks;
                        }
                        else
                            dbTimeSheetRecord.duration = 0;

                        db.SaveChanges();
                    }
                }

                ret = FromDb(dbTimeSheetRecord, dbTimeSheet.acceptedDate != null);
                
                if (toUpdate.rowType == TimeSheetRecordRowType.MasterRow)/*Delete records from details*/
                {
                   

                    var rid = dbTimeSheetRecord?.id;
                    var dbRecordsToDelete = (from t in db.timesheetrecords
                                             where t.timesheetId == dbTimeSheet.id && t.startDate.HasValue 
                                                                                   && t.startDate.Value.Year == dt.Year
                                                                                   && t.startDate.Value.Month == dt.Month
                                                                                   && t.startDate.Value.Day == dt.Day
                                                                                   &&  t.id != rid
                                             select t).ToList();

                    foreach (var item in dbRecordsToDelete)
                    {
                        db.timesheetrecords.Remove(item);
                        db.SaveChanges();
                    }

                }
                else if (toUpdate.rowType == TimeSheetRecordRowType.DataRow)/*Delete master records */
                {
                    var dbRecordsToDelete = (from t in db.timesheetrecords
                        where t.timesheetId == dbTimeSheet.id &&  t.startDate.HasValue && t.startDate.Value.Year == dt.Year  
                              && t.startDate.Value.Month == dt.Month
                              && t.startDate.Value.Day == dt.Day && t.row_type == (int)TimeSheetRecordRowType.MasterRow
                                             select t).ToList();

                    foreach (var item in dbRecordsToDelete)
                    {
                        db.timesheetrecords.Remove(item);
                        db.SaveChanges();
                    }

                }

                try
                {
                    ExportSILTimesheet(dbTimeSheet, exportPath);
                }
                catch (Exception ex)
                {
                    _log.Error("Exception while exporting the timesheet for date: " + toUpdate?.date, ex);
                }
                return ret;
            }
        }

        private void ExportSILTimesheet(timesheets dbTimeSheet, string exportPath) {

            try
            {
                if (!String.IsNullOrEmpty(exportPath) && dbTimeSheet.year.HasValue && dbTimeSheet.week.HasValue && dbTimeSheet.employeeId.HasValue)
                {
                    Membership.LoginManager mgrUsr = new Membership.LoginManager();
                    string email = mgrUsr.GetEmail(dbTimeSheet.employeeId.Value);

                    // export the timesheet
                    if (!System.IO.Directory.Exists(exportPath))
                    {
                        System.IO.Directory.CreateDirectory(exportPath);
                    }

                    var dateOfMonday = Calani.BusinessObjects.Generic.Tools.GetFirstDateOfWeek(dbTimeSheet.year.Value, dbTimeSheet.week.Value);
                    var fileName = dateOfMonday.ToString("yyyy_MM_dd");
                    fileName = System.IO.Path.Combine(exportPath, fileName + ".xml");
                    StructTime data = null;

                    if (System.IO.File.Exists(fileName))
                    {
                        data = XMLSerializer.Deserialize<StructTime>(fileName);
                    }
                    else
                    {
                        data = new StructTime()
                        {
                            autres = new List<double>().ToArray(),
                            datesstr = new List<DateTime>().ToArray(),
                            debours = new List<bool>().ToArray(),
                            piquets = new List<bool>().ToArray(),
                            dtFirstDayOfWeek = dateOfMonday,
                            email = email
                        };
                    }

                    var oldData = XMLSerializer.Serialize(data);
                    var dtList = new List<DateTime>(data.datesstr);

                    if (data.dtFirstDayOfWeek == dateOfMonday && data.email == email)
                    {
                        // export timesheets dates
                        foreach (var rec in dbTimeSheet.timesheetrecords)
                        {
                            if (rec.startDate.HasValue && !dtList.Contains(rec.startDate.Value))
                            {
                                dtList.Add(rec.startDate.Value);
                            }

                            if (rec.endDate.HasValue && !dtList.Contains(rec.endDate.Value))
                            {
                                dtList.Add(rec.endDate.Value);
                            }
                        }

                        dtList.Sort();
                        data.datesstr = dtList.ToArray();

                        //export debours
                        var fm = new Forms.FormsDataManager(dbTimeSheet.organizationId.Value, dbTimeSheet.employeeId.Value);
                        var debours = new List<bool>();
                        var piquets = new List<bool>();
                        var autres = new List<double>();

                        for (var i = 0; i < 6; i++)
                        {
                            var formDatas = fm.List(data.dtFirstDayOfWeek.AddDays(i));
                            if (formDatas.Count > 1)
                            {
                                _log.Debug("Found more than one form data for date " + data.dtFirstDayOfWeek.AddDays(i) + " user " + dbTimeSheet.employeeId.Value + " ignoring date!");
                            }
                            else
                            {
                                if (formDatas.Count == 0)
                                {
                                    debours.Add(false);
                                    piquets.Add(false);
                                    autres.Add(0);
                                }
                                else
                                {
                                    var formData = formDatas[0];
                                    // parse data - very ugly need to rework on that
                                    var jsonElements = formData.data.Substring(1, formData.data.Length - 2).Split(',');
                                    foreach (var element in jsonElements)
                                    {
                                        var jsonElement = element.Split(':');
                                        if (jsonElement.Length != 2)
                                        {
                                            _log.Debug("Found wrong formated JSON for  " + data.dtFirstDayOfWeek.AddDays(i) + " user " + dbTimeSheet.employeeId.Value + " ignoring data!");
                                        }
                                        else
                                        {
                                            var name = jsonElement[0].Replace('"', ' ').Trim();
                                            var value = jsonElement[1].Trim();

                                            switch (name)
                                            {
                                                case "expense":
                                                    debours.Add(value == "true");
                                                    break;
                                                case "onDuty":
                                                    piquets.Add(value == "true");
                                                    break;
                                                case "other":
                                                    if (Double.TryParse(value, out double val))
                                                        autres.Add(val);
                                                    else
                                                    {
                                                        _log.Debug("Could not parse débours for  " + data.dtFirstDayOfWeek.AddDays(i) + " user " + dbTimeSheet.employeeId.Value + " autre: " + value + "ignoring data!");
                                                        autres.Add(0);
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (debours.Count > 0)
                            data.debours = debours.ToArray();

                        if (piquets.Count > 0)
                            data.piquets = piquets.ToArray();

                        if (autres.Count > 0)
                            data.autres = autres.ToArray();

                        var strData = XMLSerializer.Serialize(data);

                        if(oldData != strData)
                        {
                            System.IO.File.WriteAllText(fileName, strData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("Exception while exporting the timesheet for timesheet: " + dbTimeSheet.id, ex);
            }
        }

        private timesheets FindTimesheet(long employeeId, int year, int week)
        {
            var record = GetBaseQuery().Query.FirstOrDefault(tr => tr.employeeId == employeeId && tr.year == year && tr.week == week);

            return record;
        }
    }
}
