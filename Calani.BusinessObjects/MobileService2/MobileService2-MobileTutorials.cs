﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Admin;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.MobileService2
{
    public partial class MobileService2 : IMobileService2
    {
        public List<TutorialModel> GetTutorials(string token)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            List<TutorialModel> ret = new List<TutorialModel>();

            var mgr = new TutorialsManager();

            if (t.UserId != -1 /*&& (Calani.BusinessObjects.Contacts.ContactTypeEnum)Convert.ToInt32(t.Role) == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin*/)
            {
                ret.AddRange(mgr.GeTutorialsList(t.UserId,TutorialsType.Mobile, false,false));

            }

            return ret;
        }

        public TutorialModel GetTutorialDetails(string token, string id, string version = null)
        {
            TutorialModel ret = null;
            TokenManager t = new TokenManager();
            t.ParseToken(token);
            var mid = new MobileId(id);

            var mgr = new TutorialsManager();

            if (t.UserId != -1 && mid.DbId.HasValue && !String.IsNullOrWhiteSpace(version) /*&& (Calani.BusinessObjects.Contacts.ContactTypeEnum)Convert.ToInt32(t.Role) == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin*/)
            {
                ret = mgr.GeTutorial(t.UserId, mid.DbId.Value, TutorialsType.Mobile, version);

            }

            return ret;
        }

    }
}
