﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Calani.BusinessObjects.MobileService2
{
    public partial class MobileService2 : IMobileService2
    {
        public Data.FormData AddOrUpdateForm(string token, Data.FormData form)
        {

            TokenManager t = new TokenManager();
            t.ParseToken(token);

            _log.Info($"SaveForm. FormId:{form.id}, orgId:{t.OrganizationId}, userId:{t.UserId}");


            var orgsIdToExport = System.Configuration.ConfigurationManager.AppSettings["ExportTimesheetForOrgs"];
            string exportPath = null;

            Calani.BusinessObjects.Membership.LoginManager mgrUsr = new Calani.BusinessObjects.Membership.LoginManager();
            string email = mgrUsr.GetEmail(t.UserId);

            if (!String.IsNullOrEmpty(orgsIdToExport) && !string.IsNullOrEmpty(email))
            {
                var aOrgsIdToExport = new List<string>(orgsIdToExport.Split(';'));
                if (aOrgsIdToExport.IndexOf(t.OrganizationId.ToString()) > -1)
                {
                    exportPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/TimesheetExports/" + t.OrganizationId.ToString() + "/" + email + "/");
                }
            }

            Forms.FormsDataManager mgr = new Forms.FormsDataManager(t.OrganizationId, t.UserId);
            var ret  = mgr.SaveForm(t.UserId, t.OrganizationId, form, exportPath);
            return ret;
        }

        public List<Data.FormData> GetFormData(string token, string after = null)
        {

            TokenManager t = new TokenManager();
            t.ParseToken(token);

            List<Data.FormData> ret = new List<Data.FormData>();
            Forms.FormsDataManager mgr = new Forms.FormsDataManager(t.OrganizationId, t.UserId);

            DateTime? dt = null;

            if ( after != null)
            {
                dt = DateTime.Parse(after.Replace("_", "-"));
            }

            var data = dt != null ? mgr.List(dt.Value) : mgr.List();
            foreach (var item in data)
            {
                ret.Add(new Data.FormData(item));
            }
            return ret;
        }
    }
}
