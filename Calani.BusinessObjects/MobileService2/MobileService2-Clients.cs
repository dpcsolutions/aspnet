﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Calani.BusinessObjects.MobileService2
{
    public partial class MobileService2
    {
       
        public List<Data.Client> GetClients(string token, bool light = false)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            List<Data.Client> ret = new List<Data.Client>();
            CustomerAdmin.ClientsManager mgr = new CustomerAdmin.ClientsManager(t.OrganizationId);
            var data = mgr.ListWithContactsAndAddresses();
            foreach (var item in data)
            {
                var it = new Data.Client(item);
                if (light)
                {
                    it.addresses = null;
                    it.contacts = null;
                }
                ret.Add(it);
            }
            return ret;
        }

        public Data.ClientDetails GetClient(string token, string id)
        {
            Data.ClientDetails ret = new Data.ClientDetails();

            TokenManager t = new TokenManager();
            t.ParseToken(token);

            var mid = new MobileId(id);
            if (!mid.IsMobile && mid.DbId != null)
            {
                
                CustomerAdmin.ClientsManager mgr = new CustomerAdmin.ClientsManager(t.OrganizationId);
                
                var entity = mgr.GetWithContactsAndAddresses(mid.DbId.Value);
                ret.client = new Data.Client(entity);

                ret.meetings = GetMeetings(token, id);
                ret.invoiced = GetInvoices(token, true, id);
                ret.projects = GetProjects(token, true, id);
            }
            return ret;
        }

        public Data.Client SaveClient(string token, Data.Client client)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            CustomerAdmin.ClientsManager mgr = new CustomerAdmin.ClientsManager(t.OrganizationId);

            _log.Info($"SaveClient. clientId:{client.id},  orgId:{t.OrganizationId}, userId:{t.UserId}");
            
            MobileId mid = new MobileId(client.id);
            Generic.SmartAction<Model.clients> action;
            if (!mid.IsMobile)
            {
                var c = mgr.Get(mid.DbId.Value);
                client.UpdateEntity(c, t.OrganizationId);
                action = mgr.Update(mid.DbId.Value, c);

                Calani.BusinessObjects.Contacts.ClientContactsManager mgrContacts = new Calani.BusinessObjects.Contacts.ClientContactsManager(t.OrganizationId, action.Record.id);
                foreach (var item in c.contacts)
                {
                    if (item.id > 0) mgrContacts.Update(item.id, item);
                    else mgrContacts.Add(item);
                }

                Calani.BusinessObjects.Contacts.ClientAddressesManager mgrAddress = new Calani.BusinessObjects.Contacts.ClientAddressesManager(t.OrganizationId, action.Record.id);
                foreach (var item in c.addresses)
                {
                    if (item.id > 0) mgrAddress.Update(item.id, item);
                    else mgrAddress.Add(item);
                }
            }
            else
            {
                var c = new Model.clients();
                client.UpdateEntity(c, t.OrganizationId);
                action = mgr.Add(c);
            }

            if (!action.Success) throw new Exception("Could not save client " + action.ErrorMessage);
            else
                return new Data.Client(action.Record);
        }

        public bool DeleteClient(string token, string id)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            CustomerAdmin.ClientsManager mgr = new CustomerAdmin.ClientsManager(t.OrganizationId);

            _log.Info($"DeleteClient. clientId:{id},  orgId:{t.OrganizationId}, userId:{t.UserId}");
            MobileId mid = new MobileId(id);
            if (!mid.IsMobile)
            {
                return mgr.Delete(mid.DbId.Value).Success;
            }


            return false;
        }

    }
}
