﻿using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Generic;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Web;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.MobileService2.Data;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Projects;
using log4net;

namespace Calani.BusinessObjects.MobileService2
{
    public partial class MobileService2 : IMobileService2
    {

        public Data.Project GetProject(string token, string projectIdStr)
        {
            var jobExt = GetJobById(token, projectIdStr);

            if (jobExt == null)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                return null;
            }

            var project = new Data.Project(jobExt, true);

            return project;
        }

        public List<Data.Project> GetProjects(string token, bool details, string client)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            var filterClient = new MobileId(client);

            var ret = new List<Project>();
            var mgr = new ProjectsManager(t.OrganizationId);
            var attmgr = new Attachments.AttachmentsManager();
            attmgr.OrganizationId = t.OrganizationId;

            var data = mgr.ListOpens(filterClient.DbId, includeServices: true);
            foreach (var item in data)
            {
                var proj = new Project(item, details);

                if (details)
                {
                    var attachments = attmgr.ListJobAttachments(item.id, true);
                    proj.SetAttachments(attachments);
                }
                ret.Add(proj);
            }

            return ret;
        }

        public List<Data.Project> GetOpenProjects(string token)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            

            List<Data.Project> ret = new List<Data.Project>();
            Projects.ProjectsManager mgr = new Projects.ProjectsManager(t.OrganizationId);

            var data = (from r in mgr.ListOpens() where r.ProjectStatus == Calani.BusinessObjects.Projects.ProjectStatus.JobOpened select r);
            
            foreach (var item in data)
            {
                var proj = new Data.Project(item, false);
                ret.Add(proj);
            }

            return ret;
        }
        
        public Data.Project SaveProject(string token, Data.Project project)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            _log.Info($"SaveProject. projectId:{project.id}, orgId:{t.OrganizationId}, userId:{t.UserId}");

            MobileId mid = null;

            AjaxService.ProjectUpdate proj = new AjaxService.ProjectUpdate();
            proj.action = "save:open";
            proj.id = -1;

            mid = new MobileId(project.clientId);
            try
            {
                proj.clientId = mid.DbId.Value; // is mandatory
            }
            catch (Exception e)
            {
                throw new Exception("clientId is mandatory", e);
            }

            

            var clientData = GetClient(token, project.clientId);

            // address
            if (!String.IsNullOrWhiteSpace(project.clientAddressId))
            {
                mid = new MobileId(project.clientAddressId);
                if (mid.DbId != null)
                {
                    // use the given id
                    proj.clientAddress = mid.DbId.Value;
                }
            }
            if (proj.clientAddress == 0)
            {
                try
                {
                    proj.clientAddress = new MobileId(clientData.client.addresses.First().id).DbId.Value;
                }
                catch (Exception e)
                {
                    throw new Exception("The customer has no address", e);
                }
            }
            // -->

            // contact
            if (!String.IsNullOrWhiteSpace(project.clientContactId))
            {
                mid = new MobileId(project.clientContactId);
                if (mid.DbId != null)
                {
                    // use the given id
                    proj.clientContact = mid.DbId.Value;
                }
            }
            if (proj.clientContact == 0)
            {
                try
                {
                    proj.clientContact = new MobileId(clientData.client.contactId).DbId.Value;
                }
                catch (Exception e)
                {
                    throw new Exception("The customer has no address", e);
                }
            }
            // -->

            proj.culture = "fr-FR";
            proj.description = project.description;
          

            AjaxService.AjaxService mainService = new AjaxService.AjaxService();
            var action = mainService.UpdateProject(proj, t.OrganizationId, t.UserId);

            Projects.ProjectsManager mgr = new Projects.ProjectsManager(t.OrganizationId);
            var newproject = mgr.Get(action.id);


            if (String.IsNullOrWhiteSpace(project.internalId))
            {
                project.internalId = mgr.GetLastOpenInternalId();
            }

            var orgmgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(t.OrganizationId);
            orgmgr.Load();

            var ret = new Data.Project(new Projects.jobsExtended(newproject, false, orgmgr.RoundAmount), false);
            var attmgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
            attmgr.OrganizationId = t.OrganizationId;
            var attachments = attmgr.ListJobAttachments(newproject.id, true);
            ret.SetAttachments(attachments);

            return ret;
        }

        private enum PdfType
        {
            quote, invoice, deliveryNote
        }

        private MemoryStream GenPdf(string token, string projectId, PdfType pdfType, string culture = "fr-CH", string includeattachmentsids = "")
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            MobileId mid = new MobileId(projectId);
            
            List<long> attIds = new List<long>();

            if (!String.IsNullOrWhiteSpace(includeattachmentsids))
            {
                var strAttIds = includeattachmentsids.Split(',');
                if (strAttIds.Any() && strAttIds.Length > 0)
                {
                    foreach (var a in strAttIds)
                    {
                        var m = new MobileId(a);
                        if (m.DbId.HasValue)
                            attIds.Add(m.DbId.Value);
                    }
                }
            }
            
            

            byte[] data = null;

            switch (pdfType)
            {
                case PdfType.invoice:
                    var gen = new DocGenerator.InvoiceGenerator(new System.Globalization.CultureInfo(culture), t.UserId);
                    gen.ProjectId = mid.DbId.Value;
                    gen.ExcelMode = false;
                    data = gen.GenerateDocument(attIds);
                    break;

                case PdfType.quote:
                    var gen2 = new DocGenerator.QuoteGenerator(new System.Globalization.CultureInfo(culture), t.UserId);
                    gen2.ProjectId = mid.DbId.Value;
                    gen2.ExcelMode = false;
                    data = gen2.GenerateDocument(attIds);
                    break;
                case PdfType.deliveryNote:
                    var gen3 = new DocGenerator.QuoteGenerator(new System.Globalization.CultureInfo(culture), t.UserId);
                    gen3.ProjectId = mid.DbId.Value;
                    gen3.ExcelMode = false;
                    data = gen3.GenerateDocument(attIds);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(pdfType), pdfType, null);
            }

            MemoryStream ms = new MemoryStream();
            ms.Write(data, 0, data.Length);
            ms.Position = 0;
            var filename = "GesMobile_" + mid.DbId.Value + "_" + DateTime.Now.ToString("yyyyMMddHHmmss", CultureInfo.InvariantCulture) + ".pdf";
            System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.ContentType = "application/pdf";
            System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("Content-disposition", "inline; filename=\""+ filename+"\"");
            
            return ms;
        }

        public Data.Invoice GetInvoice(string token, string invoiceIdStr)
        {
            var jobExt = GetJobById(token, invoiceIdStr);

            if (jobExt == null)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                return null;
            }

            var invoice = new Data.Invoice(jobExt, true);

            return invoice;
        }
        
        public List<Data.Invoice> GetInvoices(string token, bool details, string client)
        {
            var t = new TokenManager();
            t.ParseToken(token);

            var ret = new List<Invoice>();

            if (t.UserId != -1 && (Contacts.ContactTypeEnum)Convert.ToInt32(t.Role) == Contacts.ContactTypeEnum.User_Admin)
            {
                // we have a valid user in the token, check user role
                var filterClient = new MobileId(client);
                var mgr = new ProjectsManager(t.OrganizationId);
                var attmgr = new Attachments.AttachmentsManager();
                attmgr.OrganizationId = t.OrganizationId;
                
                var orgmgr = new OrganizationManager(t.OrganizationId);
                orgmgr.Load();
                
                var dateRange = new DateRange(DateTime.Now.AddMonths(-12), DateTime.Now.AddHours(1));
                var data = mgr.ListInvoicesDbRecords(filterClient.DbId, dateRange, includeServices: true);
                
                var changes = 0;
                foreach (var job in data)
                {
                    if (job.discount == null) job.discount = 0;
                    if (job.totalWithoutTaxes == null)
                    {
                        var activeRecords = job.services_jobs.Where(sj => sj.recordStatus != (int)RecordStatus.Deleted).ToList();
                        job.totalWithoutTaxes = (from r in activeRecords select r.totalPriceExclTax).Sum();
                        job.totalWithoutTaxes = job.totalWithoutTaxes * (100 - job.discount ?? 0) / 100;
                        if(job.totalWithoutTaxes != null) job.totalWithoutTaxes = Math.Round(job.totalWithoutTaxes.Value, 2, MidpointRounding.AwayFromZero);
                        changes++;
                    }
                
                    if(job.dateInvoicing != null && (job.invoiceDue == null || job.invoiceDue == 0))
                    {
                        var calc = new InvoiceDueCalculator(job);
                        if(calc.Recalc(orgmgr.UsePriceIncludingVat)) 
                            changes++;
                    }
                    
                    var jobExt = new jobsExtended(job, roundTotal: orgmgr.RoundAmount, isVatIncluded: orgmgr.UsePriceIncludingVat);
                    var invoice = new Data.Invoice(jobExt, details) {
                        pdfUrl = GetUrl("{token}/invoicepdf/db." + job.id)
                    };

                    if (details)
                    {
                        var attachments = attmgr.ListJobAttachments(job.id, true);
                        invoice.attachments = attachments.Select(x => x.ToDto()).ToList();
                    }

                    ret.Add(invoice);
                }
                if (changes > 0) mgr.GetBaseQuery().Context.SaveChanges();

            }

            return ret;
        }

        public Data.Quote GetQuote(string token, string quoteIdStr)
        {
            var jobExt = GetJobById(token, quoteIdStr);

            if (jobExt == null)
            {
                WebOperationContext.Current.OutgoingResponse.StatusCode = System.Net.HttpStatusCode.NotFound;
                return null;
            }

            var quote = new Data.Quote(jobExt, true);

            return quote;
        }

        public List<Data.Quote> GetQuotes(string token, bool details, string client)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            List<Data.Quote> ret = new List<Data.Quote>();

            if (t.UserId != -1 && (Calani.BusinessObjects.Contacts.ContactTypeEnum)Convert.ToInt32(t.Role) == Calani.BusinessObjects.Contacts.ContactTypeEnum.User_Admin)
            {
                // we have a valid user in the token, check user role

                var filterClient = new MobileId(client);

                var mgr = new Projects.ProjectsManager(t.OrganizationId);

                var attmgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
                attmgr.OrganizationId = t.OrganizationId;

                var data = mgr.ListQuotes(filterClient.DbId, includeServices: true);
                data = (from r in data where r.dateFullyPaid == null select r).ToList();

                foreach (var item in data)
                {
                   
                    var r = new Data.Quote(item, details);
                    r.pdfUrl = GetUrl("{token}/quotepdf/db." + item.id);
                    if (details)
                    {
                        var attachments = attmgr.ListJobAttachments(item.id, true);
                        r.SetAttachments(attachments);
                    }
                    ret.Add(r);
                }
            }

            return ret;
        }

        public Stream GenQuotePdf(string token, string projectId, string includeattachmentsids)
        {
            return GenPdf(token, projectId, PdfType.quote, "fr-CH", includeattachmentsids);
        }

        public Stream GenInvoicePdf(string token, string projectId, string includeattachmentsids)
        {
            try
            {
                return GenPdf(token, projectId, PdfType.invoice, "fr-CH", includeattachmentsids);
            }
            catch (Exception ex)
            {
                _log.Error(ex.Message);
                return new MemoryStream();
            }

        }

        public Stream GenDeliveryNotesPdf(string token, string projectId, string includeattachmentsids)
        {
            return GenPdf(token, projectId, PdfType.deliveryNote, "fr-CH", includeattachmentsids);
        }

        public Data.Quote SaveQuote(string token, Data.Quote quote)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);
            var userId = t.UserId;
            _log.Info($"SaveQuote. quoteId:{quote.id}, orgId:{t.OrganizationId}, userId:{t.UserId}");


           
            CustomerAdmin.TaxesManager taxMgr = new CustomerAdmin.TaxesManager(t.OrganizationId);
            string defaultTax = taxMgr.GetDetaultTaxName();

            var mid = new MobileId(quote.id);

            // build the same ajax query can the complete app
            AjaxService.ProjectUpdate q = new AjaxService.ProjectUpdate();

            // first copy existing value project in it
            Projects.ProjectsManager mgr = new Projects.ProjectsManager(t.OrganizationId);


            if (String.IsNullOrWhiteSpace(quote.internalId))
            {
                quote.internalId = mgr.GetLastQuoteInternalId();
            }

            if (mid.DbId != null)
            {
                var dbrecord = mgr.Get(mid.DbId.Value);

                if (dbrecord != null)
                {
                    q.action = null;
                    q.batch = dbrecord.contractualUnitsCount;

                    if (dbrecord.clientAddress != null) q.clientAddress = dbrecord.clientAddress.Value;
                    if (dbrecord.clientContact != null) q.clientContact = dbrecord.clientContact.Value;

                    q.clientId = dbrecord.clientId;
                    q.description = dbrecord.description;

                    if (dbrecord.discount != null) q.discount = dbrecord.discount.Value;

                    q.id = mid.DbId.Value;
                    q.internalId = dbrecord.internalId;

                    if (dbrecord.rating != null) q.rating = dbrecord.rating.Value;
                    q.currency = dbrecord.currency;
                }
            }

            // it is a new project set default value
            if (q.id <= 0)
            {
                q.action = null;
                q.internalId = new Projects.jobsExtended(true, false, false, false, t.OrganizationId).internalId;
                q.id = -1;
            }

            // map new properties
            q.clientId = new MobileId(quote.clientId).DbId.Value;
            q.assigneeId = !string.IsNullOrWhiteSpace(quote.assigneeId) ? new MobileId(quote.assigneeId).DbId : null;

            CustomerAdmin.ClientsManager custMgr = new CustomerAdmin.ClientsManager(t.OrganizationId);

            var cust = custMgr.Get(q.clientId);

            if (String.IsNullOrWhiteSpace(quote.clientAddressId))
            {
                if (q.clientAddress == 0) q.clientAddress = (from r in cust.addresses select r.id).FirstOrDefault();
            }
            else
            {
                var checkid = new MobileId(quote.clientAddressId);

                if (checkid.DbId != null) q.clientAddress = (from r in cust.addresses where r.id == checkid.DbId.Value select r.id).FirstOrDefault();
                else q.clientAddress = (from r in cust.addresses select r.id).FirstOrDefault();
            }

            if (String.IsNullOrWhiteSpace(quote.clientContactId))
            {
                if (q.clientContact == 0) q.clientContact = (from r in cust.contacts select r.id).FirstOrDefault();
            }
            else
            {
                var checkid = new MobileId(quote.clientContactId);

                if (checkid.DbId != null) q.clientContact = (from r in cust.contacts where r.id == checkid.DbId.Value select r.id).FirstOrDefault();
                else q.clientContact = (from r in cust.contacts select r.id).FirstOrDefault();
            }

            q.description = quote.description;
            q.currency = quote.currency;

            if(quote.dateValidity != null) 
                q.validity = DateTime.ParseExact(quote.dateValidity, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture).ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture);

            if (quote.rating != null) q.rating = (int)quote.rating;

            if (!String.IsNullOrEmpty(quote.clientId))
            {
                var clientMid = new MobileId(quote.clientId);
                q.clientId = clientMid.DbId.Value;
            }
            // -->

            // map new items (invoice lines)
            q.items = quote.lines.Select(item => ToAjaxServiceProjectItem(item)).ToList();

            // do the update             
            Projects.ProjectItemsUpdater itemsUpdater = new Projects.ProjectItemsUpdater();
            itemsUpdater.OrganizationId = Convert.ToInt64(t.OrganizationId);
            itemsUpdater.ProjectId = q.id;
            itemsUpdater.Update = q;
            itemsUpdater.Update.payments = null;
            itemsUpdater.UpdateDb(userId, "save:quote");

            if (quote.action == "markquotesent")
                q.action = "markquotesent";
            else if (quote.action == "markquoteaccepted")
                q.action = "acceptquote";
            else if (quote.action == "markquoterejected")
                q.action = "rejectquote";
            else
                q.action = "save:quote";

            if (quote.action != null && quote.action != "save:quote")
            {
                Projects.ProjectActionManager action = new Projects.ProjectActionManager();
                action.Culture = new System.Globalization.CultureInfo("fr-FR");
                action.OrganizationId = t.OrganizationId;
                action.ProjectId = itemsUpdater.ProjectId;
                action.Action = q.action;
                action.UserId = t.UserId;
                action.DoAction();
            }
            // -->

            long projectSavedId = itemsUpdater.ProjectId;
            return GetQuote(projectSavedId, t.OrganizationId);
            
        }

        private Data.Quote GetQuote(long projectSavedId, long orgId)
        {
            Projects.ProjectsManager mgr = new Projects.ProjectsManager(orgId);
            var projectSaved = mgr.Get(projectSavedId);


            var orgmgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(orgId);
            orgmgr.Load();



            var projectSavedExt = new Projects.jobsExtended(projectSaved, false);



            var ret = new Data.Quote(projectSavedExt, true);
            Calani.BusinessObjects.Attachments.AttachmentsManager attmgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
            attmgr.OrganizationId = orgId;
            var attachments = attmgr.ListJobAttachments(projectSavedExt.id, true);
            ret.SetAttachments(attachments);

            ret.pdfUrl = GetUrl("{token}/quotepdf/" + ret.id);

            return ret;
        }

        public Data.Invoice SaveInvoice(string token, Data.Invoice invoice)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);
            var userId = t.UserId;

            CustomerAdmin.TaxesManager taxMgr = new CustomerAdmin.TaxesManager(t.OrganizationId);
            string defaultTax = taxMgr.GetDetaultTaxName();

            _log.Info($"SaveInvoice. invoiceId:{invoice.id}, orgId:{t.OrganizationId}, userId:{t.UserId}");

            var mid = new MobileId(invoice.id);

            // build the same ajax query can the complete app
            AjaxService.ProjectUpdate q = new AjaxService.ProjectUpdate();
            q.action = "invoice:save";

            // first copy existing value project in it
            Projects.ProjectsManager mgr = new Projects.ProjectsManager(t.OrganizationId);
            if (mid.DbId != null)
            {
                var dbrecord = mgr.Get(mid.DbId.Value);
                if (dbrecord != null)
                {
                    q.action = null;
                    q.batch = dbrecord.contractualUnitsCount;

                    if (dbrecord.clientAddress != null) 
                        q.clientAddress = dbrecord.clientAddress.Value;

                    if (dbrecord.clientContact != null)
                        q.clientContact = dbrecord.clientContact.Value;

                    q.clientId = dbrecord.clientId;
                    q.description = dbrecord.description;

                    if (dbrecord.discount != null)
                        q.discount = dbrecord.discount.Value;

                    q.id = mid.DbId.Value;
                    q.internalId = dbrecord.internalId;
                    q.payments = (from r in dbrecord.payments.ToList() select new Projects.paymentExtended(r)).ToList();

                    if (dbrecord.rating != null) 
                        q.rating = dbrecord.rating.Value;
                }
            }

            // it is a new project set default value
            if (q.id <= 0)
            {
                q.action = null;
                q.internalId = new Projects.jobsExtended(false, false, true, false, t.OrganizationId).internalId;
                q.id = -1;
            }

            // map new properties
            q.clientId = new MobileId(invoice.clientId).DbId.Value;
            CustomerAdmin.ClientsManager custMgr = new CustomerAdmin.ClientsManager(t.OrganizationId);
            var cust = custMgr.Get(q.clientId);

            if (String.IsNullOrWhiteSpace(invoice.clientAddressId))
            {
                if (q.clientAddress == 0) q.clientAddress = (from r in cust.addresses select r.id).FirstOrDefault();
            }
            else
            {
                var checkid = new MobileId(invoice.clientAddressId);
                if (checkid.DbId != null) q.clientAddress = (from r in cust.addresses where r.id == checkid.DbId.Value select r.id).FirstOrDefault();
                else q.clientAddress = (from r in cust.addresses select r.id).FirstOrDefault();
            }

            if (String.IsNullOrWhiteSpace(invoice.clientContactId))
            {
                if (q.clientContact == 0) 
                    q.clientContact = (from r in cust.contacts select r.id).FirstOrDefault();
            }
            else
            {
                var checkid = new MobileId(invoice.clientContactId);
                if (checkid.DbId != null)
                    q.clientContact = (from r in cust.contacts where r.id == checkid.DbId.Value select r.id).FirstOrDefault();
                else
                    q.clientContact = (from r in cust.contacts select r.id).FirstOrDefault();
            }
            q.description = invoice.description;
            if (!String.IsNullOrEmpty(invoice.clientId))
            {
                var clientMid = new MobileId(invoice.clientId);
                q.clientId = clientMid.DbId.Value;
            }


            if (!String.IsNullOrWhiteSpace(invoice.currency))
                q.currency = invoice.currency;

            if (!string.IsNullOrEmpty(invoice.dueDate))
            {
                q.validity = DateTime.ParseExact(invoice.dueDate, Tools.IsoLongDateFormat, CultureInfo.InvariantCulture).ToString(Tools.DefaultDateFormat, CultureInfo.InvariantCulture);
            }
            // -->

            // map new items (invoice lines)
            q.items = invoice.lines.Select(item => ToAjaxServiceProjectItem(item)).ToList();
            
            // do the update
            q.action = "save:invoice";
            Projects.ProjectItemsUpdater itemsUpdater = new Projects.ProjectItemsUpdater();
            itemsUpdater.OrganizationId = Convert.ToInt64(t.OrganizationId);
            itemsUpdater.ProjectId = q.id;
            itemsUpdater.Update = q;
            itemsUpdater.Update.payments = null;
            itemsUpdater.UpdateDb(userId, q.action);

            if (invoice.action != null && invoice.action != "save:invoice")
            {
                Projects.ProjectActionManager action = new Projects.ProjectActionManager();
                action.Update = q;
                action.Culture = new System.Globalization.CultureInfo("fr-FR");
                action.OrganizationId = t.OrganizationId;
                action.ProjectId = itemsUpdater.ProjectId;
                action.Action = invoice.action;
                action.UserId = t.UserId;
                action.DoAction();
            }
            // -->


            long projectSavedId = itemsUpdater.ProjectId;
            return GetInvoice(projectSavedId, t.OrganizationId);
        }

        private AjaxService.ProjectItem ToAjaxServiceProjectItem(Data.ProjectItem item)
        {
            var r = new AjaxService.ProjectItem();

            var smid = new MobileId(item.id);
            if (smid.DbId != null) r.id = smid.DbId.Value;

            r.discount = item.discount;

            smid = new MobileId(item.serviceId);
            if (smid.DbId != null) r.serviceId = smid.DbId.Value;

            var db = new CalaniEntities();
            var dbService = db.services.Include(x => x.units).FirstOrDefault(x => x.id == smid.DbId.Value);
            r.unitName = dbService?.units?.name;
            
            r.serviceName = item.description;
            
            var prefix = (item.internalId ?? "") + " - ";
            r.item = ((!String.IsNullOrWhiteSpace(item.internalId) && item.description.TrimStart().IndexOf(prefix) != 0) ? prefix : "") + item.description.Trim();

            if (!String.IsNullOrWhiteSpace(item.tax)) r.tax = item.tax;
            //   else r.tax = defaultTax;

            r.qty = item.quantity;
            r.price = item.unitPrice;


            return r;
        }

        private Data.Invoice GetInvoice(long projectSavedId, long organizationId)
        {
            Projects.ProjectsManager mgr = new Projects.ProjectsManager(organizationId);
            var projectSaved = mgr.Get(projectSavedId);

            var orgmgr = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(organizationId);
            orgmgr.Load();

            var projectSavedExt = new Projects.jobsExtended(projectSaved, false);

            var ret = new Data.Invoice(projectSavedExt, true);
            Calani.BusinessObjects.Attachments.AttachmentsManager attmgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
            attmgr.OrganizationId = organizationId;
            var attachments = attmgr.ListJobAttachments(projectSavedExt.id, true);
            ret.SetAttachments(attachments);

            ret.pdfUrl = GetUrl("{token}/invoicepdf/" + ret.id);

            return ret;
        }

        public Projects.jobsExtended GetJobById(string token, string jobIdStr)
        {
            var t = new TokenManager();
            t.ParseToken(token);
            var orgMgr = new OrganizationManager(t.OrganizationId);
            orgMgr.Load();
            var mgr = new Projects.ProjectsManager(t.OrganizationId);
            var attmgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
            attmgr.OrganizationId = t.OrganizationId;

            long jobId = 0;
            if (!long.TryParse(jobIdStr, out jobId))
            {
                throw new ArgumentException(nameof(jobId), "JobId has to be numeric.");
            }

            if (jobId <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(jobId));
            }

            var entity = mgr.GetById(jobId).Query.SingleOrDefault();
            if (entity == null)
            {
                return null;
            }

            var jobExt = new Projects.jobsExtended(entity, includeReminders: true, roundTotal: orgMgr.RoundAmount);

            return jobExt;
        }

    }
}
