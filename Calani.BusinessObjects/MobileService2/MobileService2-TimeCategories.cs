﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Admin;
using Calani.BusinessObjects.Calendars;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.WorkSchedulers;

namespace Calani.BusinessObjects.MobileService2
{
    public partial class MobileService2 : IMobileService2
    {
        public List<ScheduleRecordDto> GetTimeCategories(string token)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            List<ScheduleRecordDto> ret = new List<ScheduleRecordDto>();

            var mgr = new WorkScheduleRecManager(t.OrganizationId);
            var list = mgr.GetScheduleRecordsDto(null);
            ret.AddRange(list);


            Contacts.EmployeesManager emplMgr = new Contacts.EmployeesManager(t.OrganizationId);
            List<long> calendarIds;

            var recList = emplMgr.GetAssignedScheduleRecordsDto(t.UserId, out calendarIds);
            ret.AddRange(recList);
            return ret;
        }

    }
}
