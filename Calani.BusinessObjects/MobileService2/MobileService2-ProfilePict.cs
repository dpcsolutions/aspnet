﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.MobileService2
{
    public partial class MobileService2 : IMobileService2
    {
        public Stream GetProfilePict(string token)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            Calani.BusinessObjects.Attachments.ProfilePictureManager mgr = new Attachments.ProfilePictureManager(t.OrganizationId);
            byte[] data = mgr.GetPicture(t.UserId);

            if(data != null)
            {
                MemoryStream ms = new MemoryStream();
                ms.Write(data, 0, data.Length);
                ms.Position = 0;
                System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.ContentType = "image/png";
                System.ServiceModel.Web.WebOperationContext.Current.OutgoingResponse.Headers.Add("Content-disposition", "inline; filename=ProfilePict-" + token + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png");
                return ms;
            }

            return null;
        }

        public string SetProfilePict(string token)
        {

            string b64 = Encoding.UTF8.GetString(System.ServiceModel.OperationContext.Current.RequestContext.RequestMessage.GetBody<byte[]>());
            b64 = b64.StartsWith("\"") ? b64.TrimStart('"').TrimEnd('"') : b64;

            TokenManager t = new TokenManager();
            t.ParseToken(token);

            _log.Info($"SetProfilePict. orgId:{t.OrganizationId}, userId:{t.UserId}");

            byte[] data = Convert.FromBase64String(b64);

            Calani.BusinessObjects.Attachments.ProfilePictureManager mgr = new Attachments.ProfilePictureManager(t.OrganizationId);
           
            if(mgr.SetPicture(t.UserId, data))
            {
                return GetUrl("{token}/profilepict");
            }

            return null;
        }

    }
}
