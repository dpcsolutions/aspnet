﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Calani.BusinessObjects.MobileService2
{
    public partial class MobileService2 : IMobileService2
    {
        public List<Data.TimesheetRecord> GetTimesheetRecords(string token, bool light = false)
        {

            TokenManager t = new TokenManager();
            t.ParseToken(token);

            TimesheetManager mgr = new TimesheetManager(t.OrganizationId);
            var list =  mgr.ListTimesheets(t.UserId);
            var lstReduced = new List<Data.TimesheetRecord>();

            if(light)
            {
                foreach(var l in list)
                {
                    if(DateTime.Parse(l.date) < DateTime.Now.AddMonths(-3))
                    {
                        lstReduced.Add(l);
                    }
                }
            }
            return light ? lstReduced : list;
        }

        public Data.TimesheetRecord AddOrUpdateTimesheet(string token, Data.TimesheetRecord ts)
        {
            var t = new TokenManager();

            t.ParseToken(token);

            _log.Info($"AddOrUpdateTimesheet. tsId:{ts.id}, orgId:{t.OrganizationId}, userId:{t.UserId}");

            var mgr = new TimesheetManager(t.OrganizationId);
            var orgsIdToExport = System.Configuration.ConfigurationManager.AppSettings["ExportTimesheetForOrgs"];
            string exportPath = null;

            Calani.BusinessObjects.Membership.LoginManager mgrUsr = new Calani.BusinessObjects.Membership.LoginManager();
            string email = mgrUsr.GetEmail(t.UserId);

            if (!String.IsNullOrEmpty(orgsIdToExport) && !string.IsNullOrEmpty(email))
            {
                var aOrgsIdToExport = new List<string> ( orgsIdToExport.Split(';') );
                if(aOrgsIdToExport.IndexOf(t.OrganizationId.ToString()) > -1)
                {
                    exportPath = HttpContext.Current.Server.MapPath("~/App_Data/TimesheetExports/"+t.OrganizationId.ToString()+"/" + email + "/");
                }
            }

            return mgr.updateOrCreate(ts, t.UserId, exportPath);
        }

        public bool DeleteTimesheet(string token, string id)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            _log.Info($"DeleteTimesheet. tsId:{id}, orgId:{t.OrganizationId}, userId:{t.UserId}");

            TimesheetManager mgr = new TimesheetManager(t.OrganizationId);

            var mid = new MobileId(id);

            try
            {
                if(mid.DbId.HasValue)
                return mgr.delete(mid.DbId.Value);
            }
            catch (Exception ex)
            {
                return false;
            }
            
            return false;
        }

    }
}
