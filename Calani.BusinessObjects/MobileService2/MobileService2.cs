﻿using Calani.BusinessObjects.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Web;
using log4net;
using System.Reflection;
using Calani.BusinessObjects.MobileService2.Data;

namespace Calani.BusinessObjects.MobileService2
{

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public partial class MobileService2 : IMobileService2
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public string ClientIp()
        {
            return HttpContext.Current.Request.UserHostAddress;
        }

        private string GetUrl(string relative)
        {
            string endAfter = "/json/";
            string url = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.RequestUri.OriginalString;
            int end = url.IndexOf(endAfter) + endAfter.Length;
            url = url.Substring(0, end);
            if (relative.StartsWith("/") && url.EndsWith("/")) relative = relative.Substring(1);
            url += relative;

            url = url.Replace("db:", "db.");
            return url;
        }


        private void SetMimeType(string mimetype)
        {
            WebOperationContext.Current.OutgoingResponse.ContentType = mimetype;
            
        }

        public List<string> Test(string token)
        {
            return new List<string> { "it", "is", "working" };
        }
       
    }
}
