﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.MobileService2
{
    public class MobileId
    {
        public static string FromDb(long id)
        {
            return "db:" + id;
        }
        public static string FromDb(string id)
        {
            return "db:" + id;
        }

        public long? DbId { get; set; }
        public bool IsMobile { get; set; }

        public MobileId(string k)
        {
            IsMobile = true;
            if (!String.IsNullOrWhiteSpace(k) && (k.StartsWith("db:") || k.StartsWith("db.")))
            {
                k = k.Substring(3);
                long i = -1;
                if(long.TryParse(k, out i) && i > -1)
                {
                    DbId = i;
                    IsMobile = false;
                }
            }
            
        }
    }
}
