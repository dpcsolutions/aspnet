﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Calani.BusinessObjects.MobileService2
{
    public class TokenManager
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public long OrganizationId { get; private set; }
        public long UserId { get; set; }
        public long Role { get; set; }
        public void ParseToken(string token)
        {
            OrganizationId = -1;
            UserId = -1;
            Role = 0;

            // hardcoded excpetion set in Web.Config
            string config = System.Configuration.ConfigurationManager.AppSettings["DisableMobile2Token"];
            if (!String.IsNullOrWhiteSpace(config) && config.Contains(";"))
            {
                _log.Info("Using default user: this has to be removed for PRODUCTION remove key DisableMobile2Token in webconfig");
                string[] hardcoded = config.Split(';');
                long tmp = -1;

                if (long.TryParse(hardcoded[0], out tmp)) OrganizationId = tmp;
                if (long.TryParse(hardcoded[1], out tmp)) UserId = tmp;
                if (hardcoded.Length > 2 && long.TryParse(hardcoded[2], out tmp)) Role = tmp;

                _log.Info($"Logged to: {OrganizationId} with user: {UserId} with role:  {Role}");
                return;
            }
            // -->


            string ip = HttpContext.Current.Request.UserHostAddress;
            string agent = HttpContext.Current.Request.UserAgent;

            var session = MobileService.SessionManager.IsTokenValid(token, ip, agent);

            if (session != null)
            {
                OrganizationId = session.organisationId;
                UserId = session.userId;
                Role = session.role;

                _log.Info($"Logged to: {OrganizationId} with user: {UserId} with role:  {Role}");

            }
            else
            {
                _log.Info("Got invalid session for token: " + token);
                throw new Exception("NOT_VALID_SESSION");
            }
        }
    }
}
