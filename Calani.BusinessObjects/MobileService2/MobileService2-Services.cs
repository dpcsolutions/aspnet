﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Calani.BusinessObjects.MobileService2
{
    public partial class MobileService2 : IMobileService2
    {
        public List<Data.Taxes> GetTaxes(string token)
        {
            
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            List<Data.Taxes> ret = new List<Data.Taxes>();
            CustomerAdmin.ServicesManager mgr = new CustomerAdmin.ServicesManager(t.OrganizationId, -1);
            var data = mgr.ListTaxes();
            foreach (var item in data)
            {
                ret.Add(new Data.Taxes(item));
            }
            return ret;
        }

        public Data.Taxes SaveTax(string token, Data.Taxes tax)
        {

            TokenManager t = new TokenManager();
            t.ParseToken(token);

            _log.Info($"SaveTax. taxId:{tax.id}, orgId:{t.OrganizationId}, userId:{t.UserId}");

            CustomerAdmin.ServicesManager mgr = new CustomerAdmin.ServicesManager(t.OrganizationId, -1);

            var ret  = mgr.SaveTax(t.UserId, t.OrganizationId, tax);

           
            return ret;
        }

        public List<Data.Service> GetServices(string token)
        {
            
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            List<Data.Service> ret = new List<Data.Service>();
            CustomerAdmin.ServicesManager mgr = new CustomerAdmin.ServicesManager(t.OrganizationId, -1);
            var data = mgr.List2(true);
            foreach (var item in data)
            {
                ret.Add(new Data.Service(item));
            }
            return ret;
        }


        public Data.Service SaveService(string token, Data.Service service)
        {

            TokenManager t = new TokenManager();
            t.ParseToken(token);

            var mid = new MobileId(service.id);
            var mTaxId = new MobileId(service.taxId);


            _log.Info($"SaveService. serviceId:{service.id}, taxId:{service.taxId}, orgId:{t.OrganizationId}, userId:{t.UserId}");

            var mgr = new CustomerAdmin.ServicesManager(t.OrganizationId, service.type);

            var model = new Model.services
            {
                name = service.name,
                internalId = service.internalId,
                unitPrice = service.unitPrice,
                taxId = mTaxId.DbId,
                organizationId = t.OrganizationId,
                type = service.type
            };
            if (mid.DbId.HasValue)
                model.id = mid.DbId.Value;

            var ret = mid.DbId.HasValue? mgr.Update(mid.DbId.Value, model): mgr.Add(model);
            if (ret.Success)
            {
                var entity = mgr.Get(ret.Record.id);

                return new Data.Service(entity);
            }
            
            throw new Exception(ret.ErrorMessage);
        }

        /* public List<Data.Service> GetInternalServices(string token)
         {

             TokenManager t = new TokenManager();
             t.ParseToken(token);

             List<Data.Service> ret = new List<Data.Service>();
             CustomerAdmin.ServicesManager mgr = new CustomerAdmin.ServicesManager(t.OrganizationId, 2);
             var data = mgr.List2(true); //only get internal services
             foreach (var item in data)
             {
                 ret.Add(new Data.Service(item));
             }
             return ret;
         }*/



    }
}
