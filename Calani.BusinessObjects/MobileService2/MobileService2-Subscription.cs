﻿using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Notifications;
using log4net;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Calani.BusinessObjects.MobileService2
{

    public partial class MobileService2 : IMobileService2
    {
        public Data.SubscriptionResponse Subscribe(Data.SubscriptionRequest sub)
        {
            Membership.NewAccountManager mgr = new Membership.NewAccountManager();
            mgr.CompanyName = sub.CompanyName;
            mgr.Email = sub.Email;
            mgr.Password = sub.Password1;
            mgr.FirstName = "Prénom";
            mgr.LastName = "Nom";
            mgr.AcceptTerms = true;
            var x = mgr.Create(HttpContext.Current.Server.MapPath("~/App_Data/dbschema/common/common.sql"));
            
            Data.SubscriptionResponse ret = new Data.SubscriptionResponse();
            ret.Success = x.Success;
            ret.ErrorMessage = x.ErrorMessage;
            if (ret.Success)
            {
                long orgId = x.Record.id;
                long userId = x.Record.owner.Value;

                CustomerAdmin.OrganizationManager cmgr = new CustomerAdmin.OrganizationManager(orgId);
                cmgr.Load();
                ret.CompanyName = cmgr.CompanyName;
                ret.OrgId = orgId;

                Contacts.EmployeesManager empmgr = new Contacts.EmployeesManager(orgId);
                var user = empmgr.Get(userId);
                ret.FirstName = user.firstName;
                ret.LastName = user.lastName;
                ret.Email = user.primaryEmail;
                ret.UserId = user.id;

            }
            return ret;
        }

        public string ClientAgent()
        {
            return HttpContext.Current.Request.UserAgent;
        }

        public Data.SubscriptionResponse Login(string token)
        {
            Data.SubscriptionResponse ret = new Data.SubscriptionResponse();
            string notificationToken = "";

            if(token.Contains("{"))
            {
                var items = token.Replace("{", "").Replace("}", "").Trim().Split(',');
                token = items[0].Replace("token:","").Trim();
                notificationToken = items[1].Replace("notificationToken:","").Trim();
            }

            _log.Debug("login: " + token + "," + notificationToken);

            TokenManager t = new TokenManager();
            ret.Success = true;

            t.ParseToken(token);

            MobileService.SessionManager s = new MobileService.SessionManager();
            //var usr = s.getUserFromToken(token);

            Calani.BusinessObjects.Membership.LoginManager lmgr = new Calani.BusinessObjects.Membership.LoginManager();
            var rawusr = lmgr.GetUser(t.UserId);
            _log.Debug("rawuser: " + rawusr?.lastName);

            var org = lmgr.GetOrganisation((long)rawusr.organizationId);
            _log.Debug("org: " + org?.id);

            var usr = new MobileService.ContactExtended(rawusr, org.name);

            if (!string.IsNullOrEmpty(notificationToken))
            {
                    (new NotificationManager()).storeNotification(notificationToken, usr.id);
            }

            _log.Debug($"chekck token: {token}, ip:{ClientIp()}, {ClientAgent()}");

            var sessionIsValid = MobileService.SessionManager.IsTokenValid(token, ClientIp(), ClientAgent());

            CustomerAdmin.OrganizationManager mgr = new CustomerAdmin.OrganizationManager(t.OrganizationId);
            mgr.Load();
            if (mgr.CompanyName != null)
            {
                ret.Success = true;
                ret.CompanyName = mgr.CompanyName;
                ret.OrgId = t.OrganizationId;
            }
            else
            {
                ret.Success = false;
                ret.Error = "INVALID_COMPANY";
            }

            Contacts.EmployeesManager empmgr = new Contacts.EmployeesManager(t.OrganizationId);
            var user = empmgr.Get(t.UserId);
            if (user != null)
            {
                ret.FirstName = user.firstName;
                ret.LastName = user.lastName;
                ret.Email = user.primaryEmail;
                ret.UserId = user.id;

                ret.LunchDurationMin = mgr.LunchDuration;
                ret.DailyBreakDurationMin = mgr.DailyBreakDuration;

                var contractsManager = new EmployeesManager.EmployeesContractsManager(t.OrganizationId);
                ret.ActiveContract = contractsManager.GetActiveContractDto(user.id);
                if(ret.ActiveContract != null)
                    ret.ActiveContract.StartStr = ret.ActiveContract.StartDate != null ? ((DateTime)ret.ActiveContract.StartDate).ToString(Tools.IsoLongDateFormat, CultureInfo.InvariantCulture) : "";
            }
            else
            {
                ret.Success = false;
                ret.Error = "INVALID_USER";
            }

            return ret;
        }


    }
}
