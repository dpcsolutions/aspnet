﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.MobileService2
{
    public partial class MobileService2 : IMobileService2
    {

        public List<Data.Meeting> GetMeetings(string token, string client)
        {

            TokenManager t = new TokenManager();
            t.ParseToken(token);

            MobileId filterClient = new MobileId(client);


            List<Data.Meeting> ret = new List<Data.Meeting>();
            Calani.BusinessObjects.MobileService.MobileJobManager mgr = new Calani.BusinessObjects.MobileService.MobileJobManager(t.OrganizationId);
            var data = mgr.ListVisits(t.UserId, filterClient.DbId);
            foreach (var item in data)
            {
                ret.Add(new Data.Meeting(item));
            }
            return ret;
        }

        public Data.Meeting SaveMeeting(string token, Data.Meeting meeting)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            Calani.BusinessObjects.MobileService.MobileJobManager mgr = new Calani.BusinessObjects.MobileService.MobileJobManager(t.OrganizationId);

            _log.Info($"SaveMeeting. meetingId:{meeting.id}, orgId:{t.OrganizationId}, userId:{t.UserId}");

            MobileId mid = new MobileId(meeting.id);
            Generic.SmartAction<Model.visits> action;
            if (!mid.IsMobile)
            {
                var c = mgr.Get(mid.DbId.Value);
                meeting.UpdateEntity(c, t.OrganizationId, t.UserId);
                action = mgr.Update(mid.DbId.Value, c);
            }
            else
            {
                var c = new Model.visits();
                meeting.UpdateEntity(c, t.OrganizationId, t.UserId);
                action = mgr.Add(c);
            }

            mgr.AddContact(action.Record.id, t.UserId);

            if (meeting.contacts != null)
                mgr.SyncContacts(action.Record.id, meeting.contacts);

            var dbitem = mgr.Get(action.Record.id);
            var rec = new MobileService.MobileJobExtended(dbitem, dbitem.serviceId != null ? (long)dbitem.serviceId : -1);
            return new Data.Meeting(rec);
        }

        public bool DeleteMeeting(string token, string id)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            Calani.BusinessObjects.MobileService.MobileJobManager mgr = new Calani.BusinessObjects.MobileService.MobileJobManager(t.OrganizationId);
            _log.Info($"DeleteMeeting. meetingId:{id}, orgId:{t.OrganizationId}, userId:{t.UserId}");

            MobileId mid = new MobileId(id);
            if (!mid.IsMobile)
            {
                return mgr.Delete(mid.DbId.Value).Success;
            }


            return false;
        }

    }
}
