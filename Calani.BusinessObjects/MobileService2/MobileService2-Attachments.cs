﻿using Calani.BusinessObjects.Attachments;
using Calani.BusinessObjects.Membership;
using Calani.BusinessObjects.MobileService2.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Calani.BusinessObjects.AjaxService;

namespace Calani.BusinessObjects.MobileService2
{
    public partial class MobileService2 : IMobileService2
    {
        public Data.Attachment PostAttachment(string token, string projectId, Stream data, string properties = "0", string description = "")
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            

            var mgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
            mgr.OrganizationId = t.OrganizationId;


            MobileId mid = new MobileId(projectId);

            AntsCode.Util.MultipartParser parser = new AntsCode.Util.MultipartParser(data);

            string mime = parser.ContentType;
            byte[] content = parser.FileContents;
            string name = parser.Filename;




            var attId = mgr.UploadAttachment(name, mime, null, content, jobId: mid.DbId.Value, userId: null, AttachmentTypeEnum.None, properties, description);

            _log.Info($"PostAttachment. attId:{attId}, orgId:{t.OrganizationId}, projectId:{projectId}, userId:{t.UserId}");


            var entity = mgr.Get(attId);

            var res = entity.ToDto();


            return res;
        }

        public Data.Quote PostAttachmentToQuote(string token, string projectId, Stream data, string properties = "0")
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);
            MobileId mid = new MobileId(projectId);


            var b = PostAttachment(token, projectId, data, properties);

            if (b != null)
            {
                return GetQuote(mid.DbId.Value, t.OrganizationId);
            }
            return null;
        }

        public Data.Invoice PostAttachmentToInvoice(string token, string projectId, Stream data, string properties = "0")
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);
            MobileId mid = new MobileId(projectId);

            var b = PostAttachment(token, projectId, data, properties);

            if (b!=null)
            {
                return GetInvoice(mid.DbId.Value, t.OrganizationId);
            }
            return null;
        }

        public bool DeleteAttachment(string token, string id)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            Calani.BusinessObjects.Attachments.AttachmentsManager mgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
            mgr.OrganizationId = t.OrganizationId;

            _log.Info($"DeleteAttachment. attId:{id}, orgId:{t.OrganizationId}, userId:{t.UserId}");

            MobileId mid = new MobileId(id);
            if (!mid.IsMobile)
            {
                mgr.Delete(mid.DbId.Value);
                return true;
            }


            return false;
        }

        public Attachment UpdateAttachmentProperties(string token, string id, string properties = "0")
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);
            MobileId mid = new MobileId(id);

            _log.Info($"UpdateAttachmentProperties. attId:{id}, properties:{properties}, orgId:{t.OrganizationId}, userId:{t.UserId}");


            if (!mid.DbId.HasValue)
            {
                return null;
            }

            Calani.BusinessObjects.Attachments.AttachmentsManager mgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
            mgr.OrganizationId = t.OrganizationId;
            

            var entity = mgr.UpdateAttachmentProperties(mid.DbId.Value, properties);

            var res = entity.ToDto();
            return res;
        }

        public Stream GetAttachment(string token, string id)
        {
            TokenManager t = new TokenManager();
            t.ParseToken(token);

            var mid = new MobileId(id);

            Calani.BusinessObjects.Attachments.AttachmentsManager mgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
            mgr.OrganizationId = t.OrganizationId;
            var rec = mgr.GetStream(mid.DbId.Value);
            

            return rec;
        }

        public bool SendMail(string token, Data.MailingSend send)
        {
            bool ret = false;

            TokenManager t = new TokenManager();
            t.ParseToken(token);

            _log.Info($"SendMail. projectId:{send.ProjectId},  orgId:{t.OrganizationId}, userId:{t.UserId}");

            LoginManager logmgr = new LoginManager();
            var user = logmgr.GetUser(t.UserId);

            if (!EmailSendingValidationManager.ValidateEmailLimit(t.OrganizationId))
                return false;
            
            MobileId mprojid = new MobileId(send.ProjectId);

            Projects.ProjectEmailDocumentsSender sender = new Projects.ProjectEmailDocumentsSender();
            sender.Email = new SendEmail();
            sender.Email.attachments = new List<long>();
            if (send.IncludeDocument)
            {
                AttachmentsManager attmgr = new AttachmentsManager();
                attmgr.OrganizationId = t.OrganizationId;
                var attachments = attmgr.ListJobAttachments(mprojid.DbId.Value);
                List<int> types = AttachmentsManager.GetGeneratedTypes();
                var att = (from r in attachments where r.type != null && types.Contains(r.type.Value)
                           orderby r.id descending
                           select r).FirstOrDefault();
                if(att != null) sender.Email.attachments.Add(att.id);

            }
            foreach (var strid in send.IncludeAttachmentsId)
            {
                MobileId attid = new MobileId(strid);
                sender.Email.attachments.Add(attid.DbId.Value);
            }
            sender.Email.body = send.Body;
            sender.Email.subject = send.Subject;

            MobileId midContact = new MobileId(send.ToContactId);
            if(midContact.DbId != null) sender.Email.toContact = midContact.DbId.Value;
            sender.Email.toEmail = send.ToOtherEmail;

            sender.OrganizationId =  t.OrganizationId;
            sender.From = user.primaryEmail;
            sender.FromUserId = t.UserId;

            try
            {
                ret = true;
                sender.Send();
            }
            catch (Exception e)
            {
                ret = false;
            }
            finally
            {
                EmailSendingValidationManager.CountEmailSent(t.OrganizationId);
            }

            return ret;
        }

        public Data.MailingContext GetMail(string token, string projectId)
        {
            Data.MailingContext ret = new Data.MailingContext();

            TokenManager t = new TokenManager();
            t.ParseToken(token);
           
            
            var mid = new MobileId(projectId);


            // attachments
            Calani.BusinessObjects.Attachments.AttachmentsManager mgr = new Calani.BusinessObjects.Attachments.AttachmentsManager();
            mgr.OrganizationId = t.OrganizationId;
            ret.PossibleAttachments = (from r in mgr.ListJobAttachments(mid.DbId.Value)
                                       select new Data.SelectableItem
                                       {
                                           Name = r.file,
                                           Id = MobileId.FromDb(r.id)

                                       }).ToList();

            // project info
            Calani.BusinessObjects.Projects.ProjectsManager prjectmgr = new Calani.BusinessObjects.Projects.ProjectsManager(t.OrganizationId);
            Calani.BusinessObjects.Projects.jobsExtended info = prjectmgr.GetInfo(mid.DbId.Value);

            // get variables for templates
            Calani.BusinessObjects.Membership.LoginManager logmgr = new Calani.BusinessObjects.Membership.LoginManager();
            var user = logmgr.GetUser(t.UserId);

            var cultureStr = "fr-FR";

            var culture = new System.Globalization.CultureInfo(cultureStr);
            cultureStr = UserCultureManager.GetUserCulture(user.id);

            culture = new System.Globalization.CultureInfo(cultureStr ?? "fr-FR");

            var tplMgr = new Calani.BusinessObjects.Settings.DocumentTemplateManager(user.id, culture);

            var org = new Calani.BusinessObjects.CustomerAdmin.OrganizationManager(t.OrganizationId);
            org.Load();

            List<Data.SelectableItem> vars = new List<Data.SelectableItem>();
            vars.Add(new Data.SelectableItem { Name = "myfn", Text = user.firstName });
            vars.Add(new Data.SelectableItem { Name = "myln", Text = user.lastName });
            vars.Add(new Data.SelectableItem { Name = "mytitle", Text = user.title });
            vars.Add(new Data.SelectableItem { Name = "myemail", Text = user.primaryEmail });
            vars.Add(new Data.SelectableItem { Name = "myphone", Text = user.primaryPhone });
            vars.Add(new Data.SelectableItem { Name = "mymobile", Text = user.secondaryPhone });
            vars.Add(new Data.SelectableItem { Name = "mycompany", Text = org.CompanyName });
            vars.Add(new Data.SelectableItem { Name = "mycompstreet", Text = org.AdrStreet });
            vars.Add(new Data.SelectableItem { Name = "mycompstrnr", Text = org.AdrStreetNb });
            vars.Add(new Data.SelectableItem { Name = "mycompzip", Text = org.AdrZipCode });
            vars.Add(new Data.SelectableItem { Name = "mycompcity", Text = org.AdrCity });
            vars.Add(new Data.SelectableItem { Name = "mycompcountry", Text = org.AdrCountry });
            vars.Add(new Data.SelectableItem { Name = "mycompstate", Text = org.AdrState });
            vars.Add(new Data.SelectableItem { Name = "mycompaddr2", Text = org.AdrStreet2 });
            vars.Add(new Data.SelectableItem { Name = "mycompphone", Text = org.Phone });
            vars.Add(new Data.SelectableItem { Name = "mycompfax", Text = org.Telecopy });

            


            vars.Add(new Data.SelectableItem { Name = "docnumber", Text = info.internalId });

            Calani.BusinessObjects.Settings.DocumentTemplateInfo tpl = null;


            if (info.ProjectStatus.ToString().StartsWith("Quote"))
            {



                    string date = "";
                    if (info.dateQuoteValidity != null) date = info.dateQuoteValidity.Value.ToShortDateString();
                    vars.Add(new Data.SelectableItem { Name = "doctype", Text = SharedResource.Resource.GetString("Quote", new System.Globalization.CultureInfo(cultureStr)) });
                    vars.Add(new Data.SelectableItem { Name = "docnr", Text = info.internalId });
                    vars.Add(new Data.SelectableItem { Name = "expdate", Text = date });

                    ret.Subject = SharedResource.Resource.GetString("Quote", new System.Globalization.CultureInfo(cultureStr));
                    tpl = tplMgr.Get("SendQuote");
            }
            else if (info.ProjectStatus.ToString().StartsWith("DeliveryNote"))
            {
                
                
                    vars.Add(new Data.SelectableItem { Name = "doctype", Text = SharedResource.Resource.GetString("DeliveryNote", new System.Globalization.CultureInfo(cultureStr)) });
                    vars.Add(new Data.SelectableItem { Name = "docnr", Text = info.internalId });


                    ret.Subject = SharedResource.Resource.GetString("DeliveryNote", new System.Globalization.CultureInfo(cultureStr));
                    tpl = tplMgr.Get("SendDeliveryNote");
                
            }
            else if (info.ProjectStatus.ToString().StartsWith("Job"))
            {
                ret.Subject = SharedResource.Resource.GetString("Job", new System.Globalization.CultureInfo(cultureStr));
            }
            else if (info.ProjectStatus.ToString().StartsWith("Invoice"))
            {
                string date = "";
                if (info.dateInvoiceDue != null) date = info.dateInvoiceDue.Value.ToShortDateString();

                vars.Add(new Data.SelectableItem { Name = "doctype", Text = SharedResource.Resource.GetString("Invoice", new System.Globalization.CultureInfo(cultureStr)) });
                vars.Add(new Data.SelectableItem { Name = "docnr", Text = info.internalId });
                vars.Add(new Data.SelectableItem { Name = "expdate", Text = date });


                if (false /* reminder */)
                {
                    Calani.BusinessObjects.Projects.ReminderManager rmd = new Calani.BusinessObjects.Projects.ReminderManager(t.OrganizationId);
                    string reminder = (rmd.GetReminders(info.id).Count).ToString();

                    vars.Add(new Data.SelectableItem { Name = "reminder", Text = reminder });


                    ret.Subject = SharedResource.Resource.GetString("Reminder", new System.Globalization.CultureInfo(cultureStr));
                    tpl = tplMgr.Get("SendReminder");
                }
                else
                {
                    tpl = tplMgr.Get("SendInvoice");
                }
            }

            // -->

            string templateSubject = "";
            string templateBody = "";

            if (tpl != null)
            {
                templateSubject = tpl.DefaultTitle;
                templateBody = tpl.DefaultContent;


                if (tpl.UserTemplate != null && tpl.UserTemplate.title != null) templateSubject = tpl.UserTemplate.title;
                if (tpl.UserTemplate != null && tpl.UserTemplate.content != null) templateBody = tpl.UserTemplate.content;
            }

            tpl = tplMgr.Get("Signature");
            if (tpl != null)
            {
                string signature = tpl.DefaultContent;
                if (tpl.UserTemplate != null && tpl.UserTemplate.title != null) signature = tpl.UserTemplate.content;
                templateBody += "\n\n\n";
                templateBody += signature;
            }


            foreach (var item in vars)
            {
                templateSubject = templateSubject.Replace("$" + item.Name, item.Text);
                templateBody = templateBody.Replace("$" + item.Name, item.Text);
            }

            ret.Subject = templateSubject;
            templateBody = templateBody.Replace("  ", " ");
            templateBody = templateBody.Replace("  ", " ");
            ret.Body = templateBody;

            Calani.BusinessObjects.Contacts.ClientContactsManager ctcmgr = new Calani.BusinessObjects.Contacts.ClientContactsManager(t.OrganizationId, info.clientId);
            var contacts = ctcmgr.List();
            ret.PossibleContacts = (from r in contacts
                                       select new Data.SelectableContact
                                       {
                                           Id = MobileId.FromDb(r.id),
                                           Name = r.DescriptionAndMail,
                                           Default = r.type == Convert.ToInt32(Contacts.ContactTypeEnum.Contact_CustomerMain),
                                           FirstName = info.contacts.firstName,
                                           LastName = info.contacts.lastName,
                                           Title = info.contacts.title,
                                           Email =info.contacts.primaryEmail,
                                           
                                       }).ToList();
            foreach (var item in ret.PossibleContacts)
            {
                try
                {
                    if (String.IsNullOrWhiteSpace(item.Title)) item.Title = "Mr";
                    item.Title = "Title_" + item.Title;
                    item.Title = SharedResource.Resource.GetString(item.Title, new System.Globalization.CultureInfo(cultureStr));
                }
                catch
                {
                    item.Title = SharedResource.Resource.GetString("Title_Mr", new System.Globalization.CultureInfo(cultureStr));
                }
            }

            ret.ProjectId = MobileId.FromDb(mid.DbId.Value);
            

            return ret;
        }


    }
}
