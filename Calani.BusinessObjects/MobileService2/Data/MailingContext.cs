﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class MailingContext
    {
        public string ProjectId { get; internal set; }

        public string Subject { get; set; }
        public string Body { get; set; }

        public List<SelectableItem> PossibleAttachments { get; set; }
        public List<SelectableContact> PossibleContacts { get; set; }
    }

    public class MailingSend
    {
        public string ProjectId { get; set; }

        public string Subject { get; set; }
        public string Body { get; set; }

        public string ToContactId { get; set; }
        public string ToOtherEmail { get; set; }

        public bool IncludeDocument { get; set; }
        public List<string> IncludeAttachmentsId { get; set; }
    }

    public class SelectableItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool? Default { get; set; }
        public string Text { get; set; }

    }

    public class SelectableContact : SelectableItem
    {
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}
