﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class Client
    {
        public string id { get; set; }// Utils.getNewGUID(true)
        public string fullName { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string company { get; set; }
        public bool isCompany { get; set; }
        public string primaryPhone { get; set; }
        public string secondaryPhone { get; set; }
        public string secondaryEmail { get; set; }
        public string primaryEmail { get; set; }

        public List<Address> addresses { get; set; }
        public List<Contact> contacts { get; set; }

        public string contactId { get; set; }

        public Client()
        {

        }

        public Client(clients item)
        {
            id = MobileId.FromDb(item.id);



            // find the main contact
            var list = item.contacts.ToList();
            int type = Convert.ToInt32(Contacts.ContactTypeEnum.Contact_CustomerMain);
            var mainContact = (from r in list where r.type == type select r).FirstOrDefault();
            
            if (mainContact == null) 
                (from r in list select r).FirstOrDefault();

            this.company = item.companyName;

            isCompany = item.isCompany == true;
            string fullName = "";

            if (mainContact != null)
            {
                firstName = mainContact.firstName;
                lastName = mainContact.lastName;
                primaryPhone = mainContact.primaryPhone;
                secondaryPhone = mainContact.secondaryPhone;

                primaryEmail = mainContact.primaryEmail;
                secondaryEmail = mainContact.secondaryEmail;
                contactId = MobileId.FromDb(mainContact.id);

                fullName = !String.IsNullOrEmpty(mainContact.firstName) ? mainContact.firstName : "";
                fullName = fullName + (!String.IsNullOrEmpty(mainContact.lastName) ? (!String.IsNullOrEmpty(mainContact.firstName) ? " " : "") + mainContact.lastName : "");
            }

            if (fullName.Trim() != item.companyName.Trim())
            {
                if (!String.IsNullOrEmpty(fullName))
                    this.fullName = item.companyName;
                else
                    this.fullName = fullName + " (" + item.companyName + ")";
            }
            else
                this.fullName = item.companyName;

            var dbadd = item.addresses.ToArray();
            type = Convert.ToInt32(Contacts.AddressTypeEnum.CustomerAddress);
            addresses = new List<Address>();
            var dbaddresses = (from r in dbadd where r.type == type select r).ToList();
            foreach(var a in dbaddresses)
            {
                Address i = new Address();
                i.street = a.street;
                i.num = a.nb;
                i.zip = a.npa;
                i.city = a.city;
                i.country = a.country;
                i.caption = a.caption;
                i.id = MobileId.FromDb(a.id);
                if (String.IsNullOrWhiteSpace(i.caption)) i.caption = i.city;
                addresses.Add(i);
            }
            contacts = new List<Contact>();

            foreach (var entity in list)
            {
                var i = new Contact();
                i.firstName = entity.firstName;
                i.lastName = entity.lastName;
                i.primaryPhone = entity.primaryPhone;
                i.secondaryPhone = entity.secondaryPhone;

                i.primaryEmail = entity.primaryEmail;
                i.secondaryEmail = entity.secondaryEmail;
                i.contactId = MobileId.FromDb(entity.id);

                i.fullName = !String.IsNullOrEmpty(entity.firstName) ? entity.firstName : "";
                i.fullName = fullName + (!String.IsNullOrEmpty(entity.lastName) ? (!String.IsNullOrEmpty(entity.firstName) ? " " : "") + entity.lastName : "");

                contacts.Add(i);
            }

        }

        public void UpdateEntity(clients c, long organizationId)
        {
            c.organizationId = organizationId;

            // contact
            int type = Convert.ToInt32(Contacts.ContactTypeEnum.Contact_CustomerMain);
            var mainContact = (from r in c.contacts where r.type == type select r).FirstOrDefault();
            if(mainContact == null)
            {
                mainContact = new contacts { type = type, organizationId = organizationId };
                c.contacts.Add(mainContact);
            }
            mainContact.firstName = this.firstName;
            mainContact.lastName = this.lastName;
            mainContact.primaryPhone = this.primaryPhone;
            mainContact.primaryEmail = this.primaryEmail;
            mainContact.secondaryPhone = this.secondaryPhone;
            mainContact.secondaryEmail = this.secondaryEmail;
            // -->

            // address
            type = Convert.ToInt32(Contacts.AddressTypeEnum.CustomerAddress);
            foreach (var a in c.addresses)
            {
                string adrid = MobileId.FromDb(a.id);
                var update = (from r in addresses where r.id == adrid select r).FirstOrDefault();
                if(update != null)
                {
                    a.caption = update.caption;
                    a.city = update.city;
                    a.nb = update.num;
                    a.street = update.street;
                    a.country = update.country;
                    a.npa = update.zip; // update corresponding address
                    a.organizationId = organizationId;
                }
            }
            foreach (var update in addresses)
            {
                MobileId mid = new MobileId(update.id);
                if(mid.DbId == null)
                {
                    Model.addresses a = new addresses();
                    a.clientId = c.id;
                    a.type = type;
                    a.caption = update.caption;
                    a.city = update.city;
                    a.nb = update.num;
                    a.country = update.country;
                    a.street = update.street;
                    a.npa = update.zip;
                    a.organizationId = organizationId;
                    c.addresses.Add(a); // add new address
                }
            }
            // -->

            var fullName = !String.IsNullOrEmpty(mainContact.firstName) ? mainContact.firstName : "";
            fullName = fullName + (!String.IsNullOrEmpty(mainContact.lastName) ? (!String.IsNullOrEmpty(mainContact.firstName) ? " " : "") + mainContact.lastName : "");

            c.companyName = !String.IsNullOrEmpty(this.company) ? this.company : fullName;
            c.isCompany = this.isCompany;
            
            if (this.isCompany && String.IsNullOrEmpty(this.company))
            {
                c.companyName = !String.IsNullOrEmpty(this.lastName) ? this.lastName + " " : "";
                c.companyName += !String.IsNullOrEmpty(this.firstName) ? this.firstName + " " : "";
            }
        }
    }

    public class Address
    {
        public string caption { get; set; }
        public string street { get; set; }
        public string num { get; set; }
        public string zip { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string id { get; set; }
    }

    public class Contact
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string primaryPhone { get; set; }
        public string secondaryPhone { get; set; }
        public string primaryEmail { get; set; }
        public string secondaryEmail { get; set; }
        public string contactId { get; set; }
        public string fullName { get; set; }
        
      
    }
}
