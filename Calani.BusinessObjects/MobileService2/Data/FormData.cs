﻿using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.TimeManagement.Models;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class FormData
    {
        public FormData(Model.form_data fd)
        {
            id = MobileId.FromDb(fd.id);
            employeeId = fd.employeeId;
            stringifiedData = fd.data;
            if (fd.forDate.HasValue) forDate = fd.forDate.GetValueOrDefault().ToString(Tools.IsoLongDateFormat, System.Globalization.CultureInfo.InvariantCulture);
        }

        public FormData() { }

        public long employeeId { get; set; }

        public string id { get; set; }

        public string stringifiedData { get; set; }

        public string forDate { get; set; }
    }
}
