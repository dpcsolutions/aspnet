﻿using Calani.BusinessObjects.Calendars;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.TimeManagement;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class OrgSettings
    {
        public ScheduleDto AssignedWorkSchedule { get; set; }
        public TimeSheetType TimeSheetType { get; set; }
        public int TimeSheetLinesPerDay { get; set; }
        public int LunchDuration { get; set; }
        public int DailyBreakDuration { get; set; }

        public double WeekWorkHours { get; set; }

    }

    public static class OrgSettingsExtensions
    {
        public static OrgSettings ToDto(this OrganizationManager entity)
        {
            var dto = new OrgSettings();
            dto.LunchDuration = entity.LunchDuration;
            dto.DailyBreakDuration = entity.DailyBreakDuration;
            dto.WeekWorkHours = entity.WeekWorkHours;
            dto.AssignedWorkSchedule = entity.AssignedWorkSchedule;
            dto.TimeSheetType = (TimeSheetType)(entity.TimeSheetType);
            dto.TimeSheetLinesPerDay = entity.TimeSheetLinesPerDay;
            return dto;

        }
    }
}
