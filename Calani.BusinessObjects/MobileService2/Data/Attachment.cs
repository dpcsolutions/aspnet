﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Attachments;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class Attachment
    {
        public string id { get; set; }
        public string mimeType { get; set; }
        public string name { get; set; }
        public string icon { get; set; }
        public string date { get; set; }
        public AttachmentTypeEnum? type { get; set; }
        public AttachmentProperties properties { get; set; }
        public string thumbnail { get; set; }
        public string description { get; set; }
    }

    public static class AttachmentsExtensions
    {
        public static Data.Attachment ToDto(this attachmentsExtended att)
        {
            var res = new Data.Attachment();
            res.id = MobileId.FromDb(att.id);
            res.icon = att.Icon;
            res.mimeType = att.mimeType;
            res.type = (AttachmentTypeEnum?)att.type;
            res.name = att.name;
            res.description = att.description;
            res.date = DataHelper.FormatDate(att.date);
            res.properties = (AttachmentProperties) att.properties;
            res.thumbnail = att.thumbnail;
            return res;
        }
    }
}
