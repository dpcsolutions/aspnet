﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Costs;
using Calani.BusinessObjects.MobileService;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class Expense
    {
        private expenses record;

        public string id { get; set; } // Utils.getNewGUID(true)
        public string description { get; set; }
        public double amount { get; set; }
        public double approvedAmount { get; set; }
        public string date { get; set; }
        public string statusId { get; set; } 
        public string pictureUrl { get; set; }
        public string projectId { get; set; }

        public string pictureToUpload { get; set; }

        public string currency { get; set; }
        public double? currencyRate { get; set; }

        public int PayType { get; set; }
        public double VAT1 { get; set; }
        public double VAT2 { get; set; }
        public double VAT3 { get; set; }

        public Expense()
        {

        }
        public Expense(ExpenseExtended item) : this(item.GetRecord())
        {

        }

        public Expense(expenses item)
        {
            id = MobileId.FromDb(item.id);
            date = DataHelper.FormatDate(item.date);
            description = item.description;
            if(item.amount != null) amount = item.amount.Value;
            statusId = "1"; //open
            if (item.approvedmount != null)
            {
                approvedAmount = item.approvedmount.Value;
                statusId = (item.approvedmount != 0 && item.approvedmount == item.amount) ? "2" : "3";
            }
            if (item.linkedjobid != null) 
                projectId = MobileId.FromDb(item.linkedjobid.Value);

            currency = item.currency;
            currencyRate = item.currencyRate;

            PayType = (int)item.paytypeId;
            VAT1 = item.vat1.GetValueOrDefault();
            VAT2 = item.vat2.GetValueOrDefault();
            VAT3 = item.vat3.GetValueOrDefault();
        }

        public void UpdateEntity(expenses c, long organizationId, long userId)
        {
            c.lastModificationDate = DateTime.Now;
            c.lastModifiedById = userId;
         //   c.linkedjobid = null; // le mobile ne permet pour le moment pas de lier un job
            c.timesheetId = null; // le mobile ne permet pour le moment pas de lier une timesheet
            c.description = this.description;
            c.employeeId = userId;
            c.lastModifiedById = userId;
            c.recordStatus = 0;
            c.amount = this.amount;
            c.date = DataHelper.ParseDate(this.date);
            c.linkedjobid = this.projectId != null ? (new MobileId(this.projectId)).DbId : null;
            c.approvedmount = null; // when update throught mobile, reset validation
            c.currency = currency;
            c.currencyRate = currencyRate;

            c.paytypeId = PayType;
            c.vat1 = VAT1;
            c.vat2 = VAT2;
            c.vat3 = VAT3;
            // todo : gestion de l'attachment

        }
    }
}
