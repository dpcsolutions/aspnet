﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class ClientDetails
    {

        public Client client { get; set; }
        public List<Invoice> invoiced { get; set; }
        public List<Project> projects { get; set; }
        public List<Meeting> meetings { get; set; }

    }
}
