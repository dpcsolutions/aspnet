﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class Service
    {
  

        

        public string id { get; set; }
        public string name { get; set; }
        public double taxRate { get; set; }
        public string taxId { get; set; }
        public string internalId { get; set; }

        public double unitPrice { get; set; }
        public int type { get; set; }

        public Service()
        {

        }

        public Service(services item)
        {
            this.id = MobileId.FromDb(item.id);
            this.name = item.name;
            this.internalId = item.internalId;
            this.unitPrice = item.unitPrice;
            this.type = item.type.GetValueOrDefault();

            if (item.taxId != null)
            {
                this.taxId = MobileId.FromDb(item.taxId.Value);
                this.taxRate = item.taxes.rate;
            
            }
        }
    }
}
