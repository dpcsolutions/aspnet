﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class SubscriptionRequest
    {
        public string Email { get; set; }
        public string CompanyName { get; set; }
        public string Password1 { get; set; }
        public string Password2 { get; set; }
    }
}
