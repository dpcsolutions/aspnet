﻿using Calani.BusinessObjects.Projects;
using System;
using System.Collections.Generic;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class BaseProject
    {
        private jobsExtended item;

        public BaseProject()
        {

        }

        public string id { get; set; }
        public string clientId { get; set; }

        public string clientContactId { get; set; }
        public string clientAddressId { get; set; }
        public string name { get; set; }
        public string internalId { get; set; }
        public string status { get; set; }
        public int statusId { get; set; }
        public string description { get; set; }
        public string comment { get; set; }
        public string sendCopyEmailTo { get; set; }
        public List<Data.ProjectItem> lines { get; set; }

        public string pdfUrl { get; set; }

        public string date { get; set; }

        public string action { get; set; }

        public string currency { get; set; }

        public double? currencyRate { get; set; }

        public long? linkedjobid { get; set; }
        public List<Data.Attachment> attachments { get; set; }

        public Totals totals { get; set; }

        public BaseProject(jobsExtended item, bool getItems)
        {
            id = MobileId.FromDb(item.id);
            clientId = MobileId.FromDb(item.clientId);

            if (item.clientContact != null) clientContactId = MobileId.FromDb(item.clientContact.Value);
            if (item.clientAddress != null) clientAddressId = MobileId.FromDb(item.clientAddress.Value);

            internalId = item.internalId;
            name = item.internalId + " -  " + item.description;
            status = item.ProjectStatus.ToString();
            statusId = Convert.ToInt32(item.ProjectStatus);
            description = item.description;
            comment = item.comment;
            sendCopyEmailTo = item.send_copy_email_to;
            currency = item.currency;
            currencyRate = item.currencyRate;
            linkedjobid = item.linkedjobid;

            DateTime? dt = null;

            if (item.lastUpdate != null && dt == null)
                dt = item.lastUpdate;

            if (item.lastUpdate != null && dt != null && item.lastUpdate.Value > dt.Value) 
                dt = item.lastUpdate;

            if (dt != null)
                date = DataHelper.FormatDate(dt.Value);

            if (getItems)
            {
                lines = new List<Data.ProjectItem>();
                var records = item.ExportItems();
                foreach (var record in records)
                {
                    lines.Add(new Data.ProjectItem(record));
                }
            }

            totals = new Totals(item.Totals);
        }

        public List<AjaxService.ProjectItem> GetAjaxProjectItems()
        {
            var list = new List<AjaxService.ProjectItem>();

            foreach (var line in lines)
            {
                list.Add(line.ToAjaxProjectItem());
            }

            return list;
        }


        public void SetAttachments(List<Calani.BusinessObjects.Attachments.attachmentsExtended> rec)
        {
            this.attachments = new List<Data.Attachment>();
            foreach (var a in rec)
            {
                var att = a.ToDto();
                this.attachments.Add(att);
            }
        }


    }
}
