﻿using System;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class CalendarRec
    {
        public long id { get; set; }
        public long calendar_id { get; set; }
        public string name { get; set; }
        public DateTime? start_date { get; set; }
        public DateTime? end_date { get; set; }
        public DateTime? updated_at { get; set; }
        public long? updated_by { get; set; }
        public int? recordStatus { get; set; }
    }
}