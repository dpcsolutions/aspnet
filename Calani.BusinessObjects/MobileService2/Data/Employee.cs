﻿using Calani.BusinessObjects.TimeManagement.Models;
using System;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class Employee
    {
        public string id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }

        public TimesheetReportModel timesheetReport { get; set; }
        public Calani.BusinessObjects.Contacts.ContractDto activeContract { get; set; }
    }
}
