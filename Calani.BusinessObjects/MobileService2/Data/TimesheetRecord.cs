﻿using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class TimesheetRecord
    {
        public string id { get; set; }
        public string visitId { get; set; }
        public string serviceId { get; set; }
        public string projectId { get; set; }
        public string serviceType { get; set; } // (‘1’: privé, ‘2’: client, …)
        public string clientId { get; set; }
        public string clientName { get; set; }

        public TimeSheetRecordType recordType { get; set; }
        public TimeSheetRecordRowType? rowType { get; set; }

        public double? duration { get; set; }

        public string comment { get; set; }
        public string projectDescription { get; set; }

        public string visitDescription { get; set; }

        public string serviceDescription { get; set; }

        public string date { get; set; }
        public string timeStart { get; set; }
        public string timeEnd { get; set; }
        public string description { get; set; }
        public double rate { get; set; }
        public bool isValidated { get; set; }
        public string lastModificationDate { get; set; }


        public long? parentId { get; set; }
        public int? position { get; set; }
        public bool isParent { get; set; }
        public int? index { get; set; }
        public int? lunchDuration { get; set; }
        public int? pauseDuration { get; set; }

        public string categoryId { get; set; }
        public string enteredDuration { get; set; }

        public TimesheetRecord()
        {

        }
    }
}
