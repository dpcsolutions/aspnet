﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class Currency
    {
        public string id { get; set; }
        public string symbol { get; set; }
        public double rate { get; set; }
        public bool isdefault { get; set; }
        public string currency { get; set; }
    }
}
