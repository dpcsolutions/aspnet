﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class Taxes
    {

        public string id { get; set; }
        public string description { get; set; }
        public double rate { get; set; }

        public Taxes()
        {

        }

        public Taxes(Model.taxes item)
        {
            this.id = MobileId.FromDb(item.id);
            this.description = item.name;
            this.rate = item.rate;
        }
    }
}
