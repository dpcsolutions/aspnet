﻿using System;
using System.Globalization;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Projects;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class Quote : BaseProject
    {
        public int? rating { get; set; }
        public string dateQuoteAccepted { get; set; }
        public string dateQuoteRefused { get; set; }
        public string dateValidity { get; set; }
        public string assigneeId { get; set; }

        public Quote() : base()
        {}

        public Quote(jobsExtended item, bool getItems) : base(item, getItems)
        {
            DateTime? dt = null;

            if (item.dateQuoteSent != null) dt = item.dateQuoteSent.Value;
            if (item.dateQuoteRefused != null) dt = item.dateQuoteRefused.Value;
            if (item.dateQuoteAccepted != null) dt = item.dateQuoteAccepted.Value;

            if (item.lastUpdate != null && dt == null) dt = item.lastUpdate;
            if (item.lastUpdate != null && dt != null && item.lastUpdate.Value > dt.Value) dt = item.lastUpdate;
            if (dt != null) date = DataHelper.FormatDate(dt.Value);

            rating = item.rating;
            assigneeId = item.assigneeId.HasValue ? MobileId.FromDb(item.assigneeId.Value) : null;

            dateQuoteAccepted = item.dateQuoteAccepted.HasValue ? item.dateQuoteAccepted.Value.ToString(Tools.IsoLongDateFormat, CultureInfo.InvariantCulture) : null;
            dateQuoteRefused = item.dateQuoteRefused.HasValue ? item.dateQuoteRefused.Value.ToString(Tools.IsoLongDateFormat, CultureInfo.InvariantCulture) : null;
            dateValidity = item.dateQuoteValidity.HasValue ? item.dateQuoteValidity.Value.ToString(Tools.IsoLongDateFormat, CultureInfo.InvariantCulture) : null;
        }
    }
}