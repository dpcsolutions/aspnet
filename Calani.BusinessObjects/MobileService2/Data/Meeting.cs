﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.MobileService;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class Meeting
    {
        public string id { get; set; } // Utils.getNewGUID(true)
        public string typeId { get; set; } // (‘1’: privé, ‘2’: client, …)
        public string clientId { get; set; }
        public string projectId { get; set; }
        public string serviceId { get; set; }
        public string date { get; set; }
        public string timeStart { get; set; }
        public string timeEnd { get; set; }
        public string description { get; set; }
        public string statusId { get; set; } // (‘1’: à faire, ‘2’: terminé, ‘3’: annulé)
        public string duration { get; set; }
        public List<MobileRessources> contacts {get; set;}


        public Meeting()
        {

        }

        public Meeting(MobileJobExtended item)
        {
            id = MobileId.FromDb(item.id);
            typeId = (item.internalServiceId > -1) ? typeId = "1" : typeId = "2";
            
            if (item.clientId>-1)clientId = MobileId.FromDb(item.clientId);
            if (item.jobId > -1) projectId = MobileId.FromDb(item.jobId);
            date = item.visit.startDateTime;
            timeStart = item.visit.startDateTime;
            timeEnd = item.visit.endDateTime;
            serviceId = item.internalServiceId > -1 ? MobileId.FromDb(item.internalServiceId) : null;
            description = "";
            if (!String.IsNullOrWhiteSpace(item.clientDescription)) description += item.clientDescription;
            if(!String.IsNullOrWhiteSpace(item.internalVisitDescription)) description += "\r\n" + item.internalVisitDescription;
            statusId = item.jobStatus.ToString();
            DateTime dtEnd = Convert.ToDateTime(item.visit.endDateTime);
            DateTime dtStart = Convert.ToDateTime(item.visit.startDateTime);
            TimeSpan dur = dtEnd.Subtract(dtStart);
            duration = dur.ToString("hh\\:mm");
            contacts = item.resources;

        }

        internal void UpdateEntity(visits c, long organizationId, long userId)
        {
            c.orgId = organizationId;
            c.lastModificationDate = DateTime.Now;
            c.lastModifiedById = userId;
            c.recordStatus = 0;
            c.visitStatus = Convert.ToInt32(this.statusId);

            c.dateEnd = Convert.ToDateTime(this.timeEnd);
            c.dateStart = Convert.ToDateTime(this.timeStart);
            c.description = this.description;
    
            if(this.clientId != null && this.projectId != null)
            {
                long daProjectId = new MobileId(this.projectId).DbId.Value;
                long daClientId = new MobileId(this.clientId).DbId.Value;
                
                Projects.ProjectsManager mgr = new Projects.ProjectsManager(organizationId);
                var daProject = mgr.Get(daProjectId);

                if (daProject != null && daProject.clientId == daClientId)
                {
                    c.jobId = daProjectId;
                    c.type = 2;
                }
            }
            else if(this.clientId != null && this.projectId == null)
            {
                long daClientId = new MobileId(this.clientId).DbId.Value;

                // on connait le client mais pas le projet.
                // recuprer le dernier projet en cours
                Projects.ProjectsManager mgr = new Projects.ProjectsManager(organizationId);
                var openedProjects = mgr.ListOpens(daClientId);
                var mostRecentProject = (from r in openedProjects orderby r.dateCreated descending select r).FirstOrDefault();
                if (mostRecentProject != null)
                {
                    c.jobId = mostRecentProject.id;
                    c.type = 2;
                }

                // ou ouvrir un nouveau
                else
                {
                    var newProject = new jobs();
                    var info = new Projects.jobsExtended(false, true, false, false, organizationId);
                    newProject.internalId = info.internalId;
                    newProject.clientId = daClientId;
                    newProject.dateOpenJob = DateTime.Now;
                    newProject.description = info.internalId;
                    newProject.organizationId = organizationId;
                    newProject.recordStatus = 0;
                    newProject.type = Convert.ToInt32(Projects.ProjectTypeEnum.Billable);

                    CustomerAdmin.ClientsManager cmgr = new CustomerAdmin.ClientsManager(organizationId);
                    var client = cmgr.Get(daClientId);
                    newProject.clientContact = (from r in client.contacts select r.id).FirstOrDefault();
                    newProject.clientAddress = (from r in client.addresses select r.id).FirstOrDefault();

                    var newProjectAction = mgr.Add(newProject);
                    c.jobId = newProjectAction.Record.id;
                    c.type = 2;
                }
            }
            else if(this.projectId != null)
            {
                // vérifier qu'il s'afit d'un projet interne
                long daProjectId = new MobileId(this.projectId).DbId.Value;
                CustomerAdmin.InternalJobsManager mgr = new CustomerAdmin.InternalJobsManager(organizationId);
                var internalProject = mgr.Get(daProjectId);
                if (internalProject != null && internalProject.type == Convert.ToInt32(CustomerAdmin.ServiceType.InternalJobs))
                {
                    c.jobId = null;
                    c.serviceId = internalProject.id;
                    c.type = 1;
                }
            }
            

        }
    }
}
