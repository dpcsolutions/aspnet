﻿using Calani.BusinessObjects.Projects.Models;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class Totals
    {
        public double subTotalHT { get; set; }
        public double rebaitAmount { get; set; }
        public double totalHT { get; set; }
        public double taxAmount { get; set; }
        public double totalTTC { get; set; }

        public Totals()
        {
        }

        public Totals(TotalsModel model)
        {
            subTotalHT = model.SubTotalHT;
            rebaitAmount = model.RebaitAmount;
            totalHT = model.TotalHT;
            taxAmount = model.TaxAmount;
            totalTTC = model.TotalTTC;
        }
    }
}
