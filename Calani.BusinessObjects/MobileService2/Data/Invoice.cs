﻿using Calani.BusinessObjects.Projects;
using System;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class Invoice : BaseProject
    {
        public double invoiceDue { get; set; }
        public string paidDate { get; set; }
        public string dueDate { get; set; }

        public Invoice() : base()
        {}

        public Invoice(jobsExtended item, bool getItems) : base(item, getItems)
        {
            DateTime? dt = null;

            invoiceDue = item.invoiceDue == null ? 0 : (double)item.invoiceDue;

            if (item.dateInvoicing != null) 
                dt = item.dateInvoicing.Value;

            if (item.dateInvoicingOpen != null)
                dt = item.dateInvoicingOpen.Value;

            if (item.dateInvoiceDue != null)
                dueDate = DataHelper.FormatDate(item.dateInvoiceDue);

            if (item.dateFullyPaid != null)
                paidDate = DataHelper.FormatDate(item.dateFullyPaid);

            if (item.lastUpdate != null && dt == null) 
                dt = item.lastUpdate;

            if (item.lastUpdate != null && dt != null && item.lastUpdate.Value > dt.Value) 
                dt = item.lastUpdate;

            if (dt != null) 
                date = DataHelper.FormatDate(dt.Value);
        }
    }
}
