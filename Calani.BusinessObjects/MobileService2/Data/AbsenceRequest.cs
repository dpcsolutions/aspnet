﻿using Calani.BusinessObjects.Enums;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class AbsenceRequest
    {
        public string id { get; set; }

        public string employeeId { get; set; }

        public string employeeName { get; set; }

        public AbsenceCategoryEnum absenceCategory { get; set; }

        public string startDate { get; set; }

        public string endDate { get; set; }

        public CancellationStatusEnum cancellationStatus { get; set; }

        public ValidationEnum validationStatus { get; set; }

        public string comment { get; set; }

        public decimal durationDays { get; set; }
    }
}
