﻿using Calani.BusinessObjects.Projects;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class Project : BaseProject
    {
        public Project() : base()
        {}

        public Project(jobsExtended item, bool getItems) : base(item, getItems)
        {}        
    }
}
