﻿namespace Calani.BusinessObjects.MobileService2.Data
{
    public class SubscriptionResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public string Error { get; set; }

        public string CompanyName { get; set; }
        public long OrgId { get; set; }


        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long UserId { get; set; }
        public string Email { get; set; }

        public Calani.BusinessObjects.Contacts.ContractDto ActiveContract { get; set; }

        public int LunchDurationMin { get; set; }
        public int DailyBreakDurationMin { get; set; }
    }
}
