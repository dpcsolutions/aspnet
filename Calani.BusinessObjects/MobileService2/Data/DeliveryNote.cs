﻿using Calani.BusinessObjects.Projects;
using System;
using System.Linq;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class DeliveryNote : BaseProject
    {
        public string deliveredTo { get; set; }
        public string deliveryDate { get; set; }
        public string assigneeId { get; set; }

        public DeliveryNote() : base()
        {}

        public DeliveryNote(jobsExtended item, bool getItems) : base(item, getItems)
        {
            var visit = item.visits != null ? item.visits.FirstOrDefault(v => v.recordStatus == 0) : null;
            if (visit != null)
            {
                deliveryDate = visit.dateStart.HasValue ? visit.dateStart.Value.ToString("yyyy-MM-dd HH:mm:ss") : String.Empty;
            }

            if(item.assigneeId != null)
            {
                assigneeId = MobileId.FromDb(item.assigneeId.Value);
            }
        }
    }
}
