﻿using System;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class ProjectItem
    {
        public string id { get; set; }
        public string internalId { get; set; }
        public string description { get; set; }
        public string unitName { get; set; }
        public string serviceId { get; set; }
        public double quantity { get; set; }
        public double unitPrice { get; set; }
        public double discount { get; set; }
        public double amount { get; set; }
        public double rate { get; set; }
        public string tax { get; set; }
        public bool isChecked { get; set; }
        
        public ProjectItem()
        {

        }

        public ProjectItem(AjaxService.ProjectItem record)
        {
            if(record.id != null) 
                id = MobileId.FromDb(record.id.Value);

            description = record.serviceName;

            if(record.serviceId != null) 
                serviceId = MobileId.FromDb(record.serviceId.Value);

            quantity = record.qty;
            unitName = record.unitName;
            unitPrice = record.price;
            discount = record.discount;
            amount = record.totalprice;
            tax = record.tax;
            rate = record.rate ?? rate;
            internalId = record.serviceInternalId;
            isChecked = record.isChecked ?? false;
        }

        public AjaxService.ProjectItem ToAjaxProjectItem()
        {
            var model = new AjaxService.ProjectItem();
            model.id = new MobileId(this.id).DbId;
            model.serviceInternalId = this.internalId;

            model.item = String.Format("{0} - {1}", this.internalId, this.description);

            model.serviceName = this.description;
            model.serviceId = new MobileId(this.serviceId).DbId;
            model.unitName = this.unitName;
            model.qty = this.quantity;
            model.price = this.unitPrice;
            model.discount = this.discount;
            model.totalprice = this.amount;
            model.tax = this.tax;
            model.rate = this.rate;
            model.serviceInternalId = this.internalId;
            model.isChecked = this.isChecked;

            return model;
        }

    }
}
