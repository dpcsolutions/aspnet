﻿using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.MobileService2.Data
{
    public class PayType
    {
        public long Id { get; set;  }
        public string Type { get; set; }

        public PayType()
        {
        }

        public PayType(pay_type payType)
        {
            Id = payType.id;
            Type = payType.type;
        }

        public PayType(long id, string type)
        {
            Id = id;
            Type = type;
        }
    }
}
