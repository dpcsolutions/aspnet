﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Partnership
{
   

    public class PartnershipManager
    {
        public long OrganizationId { get; private set; }

        public bool CanJoin { get; private set; }
        public bool CanLeave { get; private set; }
        public bool AlreadyJoined { get; private set; }
        public string Code { get; private set; }
        
        public PartnerInvoicingInfo PartnerInvoicing { get; set; }
        public CustomerDiscountPlanInfo CustomerDiscountPlan { get; set; }


        public PartnershipManager(long organizationId)
        {
            OrganizationId = organizationId;

            Load();
        }

        private void Load()
        {
            CanJoin = true;
            AlreadyJoined = false;
            CanLeave = false;

            Model.CalaniEntities db = new Model.CalaniEntities();
            string code = (from r in db.organizations where r.id == OrganizationId select r.partnership).FirstOrDefault();
            if (code != null)
            {
                var rec = (from r in db.partnership where r.id == code select r).FirstOrDefault();
                if (rec != null)
                {
                    CanJoin = false;
                    AlreadyJoined = true;

                    this.Code = rec.id;

                    this.PartnerInvoicing = new PartnerInvoicingInfo();
                    this.CustomerDiscountPlan = new CustomerDiscountPlanInfo();


                    if (rec.partner_invoicing != null && rec.partner_invoicing == true)
                    {
                        this.PartnerInvoicing.Enabled = true;
                        if (rec.partner_invoicing_monthy_plan_price != null) this.PartnerInvoicing.PlanPrice = rec.partner_invoicing_monthy_plan_price.Value;
                        if (rec.partner_invoicing_monthly_addinextrauser_price != null) this.PartnerInvoicing.ExtraUserPrice = rec.partner_invoicing_monthly_addinextrauser_price.Value;
                    }
                    else
                    {
                        this.PartnerInvoicing.Enabled = false;
                        this.CustomerDiscountPlan.MonthlyPlanId = rec.custom_planid_monthly;
                        this.CustomerDiscountPlan.YearlyPlanId = rec.custom_planid_yearly;
                        this.CustomerDiscountPlan.ExtraUserAddinMonthlyId = rec.custom_addinextrauser_monthly;
                        this.CustomerDiscountPlan.ExtraUserAddinYearlyId = rec.custom_addinextrauser_yearly;
                    }

                    var org = (from r in db.organizations where r.id == OrganizationId select r).FirstOrDefault();

                    if (org.subscriptionStatus == null)
                    {
                        CanLeave = true;
                    }
                    else if(org.subscriptionStatus != null && !org.subscriptionStatus.Contains("Active"))
                    {
                        CanLeave = true;
                    }
                }
            }

            
        }


        public bool Leave()
        {
            if (CanLeave)
            {
                Model.CalaniEntities db = new Model.CalaniEntities();
                var org = (from r in db.organizations where r.id == OrganizationId select r).FirstOrDefault();
                org.partnership = null;
                org.partprov_def_plan_price = null;
                org.partprov_def_user_price = null;
                db.SaveChanges();
                return true;
            }
            return false;
        }



        public bool Join(string code)
        {
            bool possible = false;

            Model.CalaniEntities db = new Model.CalaniEntities();
            var ex = (from r in db.partnership where r.id == code  && r.active == true select r).FirstOrDefault();
            if(ex != null)
            {
                int limit = 0;
                if(ex.limit != null) limit = ex.limit.Value;
                if (limit == 0)
                {
                    possible = true;
                }
                else
                {
                    int count = (from r in db.organizations where r.partnership == code select r).Count();
                    int left = limit - count;
                    if(left > 0)
                    {
                        possible = true;
                    }
                }
            }

            if (possible)
            {
                var org = (from r in db.organizations where r.id == OrganizationId select r).FirstOrDefault();

                if(ex.partner_invoicing == true)
                {
                    try
                    {
                        PaymentService.PaymentService serv = new PaymentService.PaymentService();
                        serv.cancelSubscription();
                    }
                    catch { }

                    // c'est un code d'affiliation payé
                    org.trialEnd = null;
                    org.subscriptionEnd = new DateTime(9999, 1, 1);
                    org.subscriptionStart = DateTime.Now;
                    org.subscriptionStatus = "Partnership_Active";
                }

                org.partnership = code;
                db.SaveChanges();
            }

            return possible;
        }
    }

    public class PartnerInvoicingInfo
    {
        public bool Enabled { get; set; }
        public double PlanPrice { get; set; }
        public double ExtraUserPrice { get; set; }
    }

    public class CustomerDiscountPlanInfo
    {
        public string MonthlyPlanId { get; set; }
        public string YearlyPlanId { get; set; }
        public string ExtraUserAddinMonthlyId { get; set; }
        public string ExtraUserAddinYearlyId { get; set; }
    }
}
