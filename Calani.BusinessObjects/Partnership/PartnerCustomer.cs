﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Partnership
{
    public class PartnerCustomer
    {
        public long Id { get; set; }
        public string Code { get; set; }
        public string OrganizationName { get; set; }
        public string Email { get; set; }
        public DateTime? SubscriptionStart { get; set; }

        public int Users { get; set; }

        public string Phone { get; set; }
    }
}
