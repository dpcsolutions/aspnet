﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Partnership
{
    public class PartnershipProviderManager
    {
        long _organizationid;
        public PartnershipProviderManager(long organizationid)
        {
            _organizationid = organizationid;
        }



        public List<PartnerCode> ListCodes()
        {
            Model.CalaniEntities db = new Model.CalaniEntities();
            var codes = (from r in db.partnership where r.partner_owner == _organizationid && r.partner_invoicing==true select r).ToList();
            List<PartnerCode> ret = new List<PartnerCode>();
            foreach (var c in codes)
            {
                PartnerCode pc = new PartnerCode();
                pc.Code = c.id;
                pc.Active = c.active == true;
                if (c.partner_invoicing_monthy_plan_price != null) pc.ExtraUserPrice = c.partner_invoicing_monthy_plan_price.Value;
                if(c.limit != null) pc.Limit = c.limit.Value;
                if(c.partner_invoicing_monthly_addinextrauser_price != null) pc.PlanPrice = c.partner_invoicing_monthly_addinextrauser_price.Value;
                pc.UsersCount = (from r in db.organizations where r.partnership == pc.Code select r).Count();
                ret.Add(pc);
            }
            return ret;
        }

        public List<PartnerCustomer> ListUsers()
        {
            Model.CalaniEntities db = new Model.CalaniEntities();

            List<string> codes = (from r in db.partnership where r.partner_owner == _organizationid && r.partner_invoicing == true select r.id).Distinct().ToList();

            var orgs = (from r in db.organizations where codes.Contains(r.partnership) orderby r.id select r).ToList();

            List<PartnerCustomer> ret = new List<PartnerCustomer>();
            foreach (var o in orgs)
            {
                var c = new PartnerCustomer();
                c.Id = o.id;
                c.Code = o.partnership;
                c.Email = o.email;
                c.OrganizationName = o.name;

                if(o.subscriptionStart != null) c.SubscriptionStart = o.subscriptionStart.Value;

                if (o.userLimit != null) c.Users = o.userLimit.Value;
                else c.Users = 1;

                c.Phone = o.phone;

                ret.Add(c);
            }

            return ret;
        }

        public PartnerCode Get(string id)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();
            var c = (from r in db.partnership where r.partner_owner == _organizationid && r.partner_invoicing == true && r.id == id select r).FirstOrDefault();

            if(c != null)
            {
                PartnerCode pc = new PartnerCode();
                pc.Code = c.id;
                pc.Active = c.active == true;
                if (c.partner_invoicing_monthy_plan_price != null) pc.ExtraUserPrice = c.partner_invoicing_monthy_plan_price.Value;
                if (c.limit != null) pc.Limit = c.limit.Value;
                if (c.partner_invoicing_monthly_addinextrauser_price != null) pc.PlanPrice = c.partner_invoicing_monthly_addinextrauser_price.Value;
                pc.UsersCount = (from r in db.organizations where r.partnership == pc.Code select r).Count();
                return pc;
            }
            return null;
        }

        


        public bool UpdateCode(string code, int? newLimit, bool? newActive)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();
            var c = (from r in db.partnership where r.partner_owner == _organizationid && r.id == code && r.partner_invoicing==true select r).FirstOrDefault();
            if(c != null)
            {
                if (newLimit != null) c.limit = newLimit.Value;
                if (newActive != null) c.active = newActive.Value;
                db.SaveChanges();
                NotifyCodeChange(code);
                return true;
            }
            return false;
        }

        private void NotifyCodeChange(string code)
        {
            var mailSender = new Calani.BusinessObjects.Generic.MailSender();
            if (!String.IsNullOrWhiteSpace(System.Configuration.ConfigurationManager.AppSettings["Bcc"]))
            {


                mailSender.MailTo.Email = System.Configuration.ConfigurationManager.AppSettings["Bcc"];
                mailSender.MailTo.UserId = 0;


                mailSender.Variables.Add("code", code);



                mailSender.SendSmartHtmlEmail("newpartcode.html", "newpartcode");

            }
        }


        private Model.organizations _org;

        public string PartnerName
        {
            get
            {
                string ret = "Partner";

                if (_org == null)
                {
                    Model.CalaniEntities db = new Model.CalaniEntities();
                    _org = (from r in db.organizations where r.id == _organizationid select r).FirstOrDefault();
                }
                if (_org.name != null) ret = _org.name;

                return ret;
            }
        }

        public double DefaultPlanPrice
        {
            get
            {
                double ret = 49;

                if(_org==null)
                {
                    Model.CalaniEntities db = new Model.CalaniEntities();
                    _org = (from r in db.organizations where r.id == _organizationid select r).FirstOrDefault();
                }
                if (_org.partprov_def_plan_price != null) ret = _org.partprov_def_plan_price.Value;

                return ret;
            }
        }

        public double DefaultUserPrice
        {
            get
            {
                double ret = 9;

                if (_org == null)
                {
                    Model.CalaniEntities db = new Model.CalaniEntities();
                    _org = (from r in db.organizations where r.id == _organizationid select r).FirstOrDefault();
                }
                if (_org.partprov_def_user_price != null) ret = _org.partprov_def_user_price.Value;

                return ret;
            }
        }

        public bool Delete(string id)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();
            var c = (from r in db.partnership where r.partner_owner == _organizationid && r.partner_invoicing == true && r.id == id select r).FirstOrDefault();

            

            if (c != null)
            {
                long count = (from r in db.organizations where r.partnership == c.id select r).Count();
                if (count == 0)
                {
                    db.partnership.Remove(c);
                    db.SaveChanges();
                    return true;
                }
            }

            return false;
        }

        public bool NewCode(string code, int limit, bool active)
        {


            Model.CalaniEntities db = new Model.CalaniEntities();

            double defaultPlanPrice = DefaultPlanPrice;
            double defaultUserPrice = DefaultUserPrice;

            if(code != null)
            {
                code = code.Trim();
                code = code.ToUpper();
            }

            if (code == null || code.Length < 5)
            {
                throw new Exception("Code need to be 5 caracters or more");
            }

            

            int verif = (from r in db.partnership where r.id == code select r).Count();
            if(verif > 0)
            {
                throw new Exception("Code already exists");
            }

            var part = new Model.partnership();
            part.id = code;
            part.partner_owner = _organizationid;
            part.limit = limit;
            part.active = true;
            part.partner_invoicing = active;
            part.partner_invoicing_monthly_addinextrauser_price = defaultUserPrice;
            part.partner_invoicing_monthy_plan_price = defaultPlanPrice;

            db.partnership.Add(part);
            db.SaveChanges();
            NotifyCodeChange(code);
            return true;
        }

        public PartnerInvitation Invite(string email, string code, string defaultObjectScriptPath)
        {
            // Step1 : verify email
            PartnerInvitation ret = new PartnerInvitation();
            ret.Result = PartnerInvitationStatus.OtherError;

            ret.EmailValid = Generic.EmailValidator.IsEmailValid(email);
            if (!ret.EmailValid)
            {
                ret.Result = PartnerInvitationStatus.NotValidEmail;
                return ret;
            }
            email = email.ToLower().Trim();

            // get gesmobile info about this email
            Model.CalaniEntities db = new Model.CalaniEntities();
            var existingUser = (from r in db.contacts
             where r.primaryEmail == email
                && ((r.type >= 0 && r.type <= 9) || r.type > 90 || r.type == 77)
             select r).FirstOrDefault();

            ret.EmailIsNewAccount = existingUser == null;

            if(ret.EmailIsNewAccount == false)
            {
                int t = 2;
                if(existingUser.type != null) t = existingUser.type.Value;
                Contacts.ContactTypeEnum t2 = (Contacts.ContactTypeEnum)t;

                if(t2 == Contacts.ContactTypeEnum.User_Admin)
                {
                    ret.EmailIsExistingCustAdmin = true;
                }
                else
                {
                    ret.EmailIsExistingCustAdmin = false;
                    ret.Result = PartnerInvitationStatus.ExistingAccountIncompatible;
                    return ret;
                }
            }
            // -->


            // Step 2 : verify code availability
            var xcode = Get(code);
            if(xcode == null)
            {                
                ret.Result = PartnerInvitationStatus.InvalidCode;

            }
            else if (xcode.Limit != 0)
            {
                if(xcode.UsersCount >= xcode.Limit)
                {
                    ret.Result = PartnerInvitationStatus.InvalidCode;
                }
            }
            // -->


            // case of exsiting account
            if(!ret.EmailIsNewAccount && ret.EmailIsExistingCustAdmin)
            {
                // send code manually by email
                var mailSender = new Calani.BusinessObjects.Generic.MailSender();
                mailSender.MailTo.Email = existingUser.primaryEmail;
                mailSender.MailTo.UserId = existingUser.id;
                mailSender.Variables.Add("partner", PartnerName);
                mailSender.Variables.Add("email", existingUser.primaryEmail);
                mailSender.Variables.Add("code", code);
                mailSender.Variables.Add("buttonurl", Generic.MailSender.PublicUrl + "Account/Partnership.aspx");
                mailSender.SendSmartHtmlEmail("invite_partcode.html", "invite_partcode");

                //
                ret.Result = PartnerInvitationStatus.CodeSent;
            }


            else if (ret.EmailIsNewAccount)
            {
                //create account
                Membership.NewAccountManager creation = new Membership.NewAccountManager();
                creation.Email = email;
                creation.CompanyName = "Mon entreprise";
                creation.Password = Guid.NewGuid().ToString("n") + "!" + Guid.NewGuid().ToString("n");
                creation.FirstName = "Prénom";
                creation.LastName = "Nom";
                creation.AcceptTerms = true;
                creation.DisableActivationEmail = true;
                creation.Create(defaultObjectScriptPath);

                var org = (from r in db.organizations where r.email == creation.Email select r).FirstOrDefault();
                //

                // activate account
                creation.ActivateAccount(creation.Email, org.activationCode);
                //-->

                // send custom email
                Membership.LoginManager logmgr = new Membership.LoginManager();
                logmgr.Partner = PartnerName;
                logmgr.CustomEmailPassRecover = "partner_choosepass.html";
                logmgr.RecoverPassword(creation.Email);
                //  -->
                ret.Result = PartnerInvitationStatus.AccountCreated;
            }

            return ret;

        }
    }
}
