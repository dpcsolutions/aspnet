﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Partnership
{
    public class PartnerInvitation
    {
        public bool EmailValid { get; set; }
        public bool EmailIsNewAccount { get; set; }
        public bool EmailIsExistingCustAdmin { get; set; }

        public PartnerInvitationStatus Result { get; set; }
    }

    public enum PartnerInvitationStatus
    {
        CodeSent = 001,
        AccountCreated = 002,

        InvalidCode = 990,
        NotValidEmail = 997,
        ExistingAccountIncompatible = 998,
        OtherError = 999
    }
}
