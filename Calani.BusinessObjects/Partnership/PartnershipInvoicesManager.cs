﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.Partnership
{
    public class PartnershipInvoicesManager
    {
        public long OrganizationId { get; private set; }

        public PartnershipInvoicesManager(long organizationId)
        {
            OrganizationId = organizationId;
            _cache.Clear();
            _cacheOwner.Clear();
            _cachePartership.Clear();

        }

        public InvoicedMonth ExtractMonth(int year, int month)
        {
            Model.CalaniEntities db = new Model.CalaniEntities();
            InvoicedMonth im = new InvoicedMonth();
            im.Year = year;
            im.Month = month;

            im.Items = ExtractCustomers(db, im.Year, im.Month);
            im.Customers = im.Items.Count;
            im.ExtraUserLicences = (from r in im.Items select r.ExtraUserLicences).Sum();

            return im;
        }

        public List<InvoicedMonth> ExtractData(bool details)
        {
            List<InvoicedMonth> ret = new List<InvoicedMonth>();

            Model.CalaniEntities db = new Model.CalaniEntities();
            var firstdate = (from r in db.login where r.partnerproviderid == OrganizationId orderby r.date ascending select r.date).FirstOrDefault();
            var lastdate = (from r in db.login where r.partnerproviderid == OrganizationId orderby r.date descending select r.date).FirstOrDefault();

            if(firstdate != null && lastdate != null)
            {
                DateTime dt = firstdate.Value;
                dt = new DateTime(dt.Year, dt.Month, 1);
                while (dt < lastdate.Value)
                {
                    InvoicedMonth im = new InvoicedMonth();
                    im.Year = dt.Year;
                    im.Month = dt.Month;

                    if (details)
                    {
                        im.Items = ExtractCustomers(db, im.Year, im.Month);
                        im.Customers = im.Items.Count;
                        im.ExtraUserLicences = (from r in im.Items select r.ExtraUserLicences).Sum();
                    }

                    ret.Add(im);
                    dt = dt.AddMonths(1);
                }
            }

            

            return ret;
        }

        private List<InvoicedMonthItem> ExtractCustomers(Model.CalaniEntities db, int year, int month)
        {
            List<InvoicedMonthItem> ret = new List<InvoicedMonthItem>();

            DateTime mstart = new DateTime(year, month, 1);
            DateTime mend = mstart.AddMonths(1);

            var orgs = (from r in db.login
             where r.partnerproviderid == OrganizationId
                && r.date >= mstart && r.date < mend
             select r.orgid).Distinct().ToList();
            foreach (long item in orgs)
            {
                InvoicedMonthItem ii = new InvoicedMonthItem();
                ii.CustomerId = item;
                int? imax = (from r in db.login
                             where r.partnerproviderid == OrganizationId
                                && r.date >= mstart && r.date < mend
                                && r.orgid == item
                             select r.orgactiveusers).Max();


                if (imax != null) ii.ExtraUserLicences = imax.Value;
                ii.ExtraUserLicences -= 2;
                ii.ExtraUserLicences = Math.Max(ii.ExtraUserLicences, 0);

                ii.Code = (from r in db.login
                           where r.partnerproviderid == OrganizationId
                              && r.date >= mstart && r.date < mend
                              && r.orgid == item
                           select r.partnercode).FirstOrDefault();

                AddNameAndOwner(db, ii);
                AddPrices(db, ii);

                ret.Add(ii);
            }
            return ret;
        }

        static List<Model.partnership> _cachePartership = new List<partnership>();
        private void AddPrices(CalaniEntities db, InvoicedMonthItem ii)
        {
            var e = (from r in _cachePartership where r.id == ii.Code select r).FirstOrDefault();
            if (e == null)
            {
                var p = (from r in db.partnership where r.id == ii.Code select r).First();
                if (p != null)
                {
                    _cachePartership.Add(p);
                    e = p;
                }
            }

            if (e != null)
            {
                if(e.partner_invoicing_monthy_plan_price != null) ii.BasePrice = e.partner_invoicing_monthy_plan_price.Value;
                if (e.partner_invoicing_monthly_addinextrauser_price != null) ii.ExtraUserPrice = e.partner_invoicing_monthly_addinextrauser_price.Value;

                ii.Total = ii.BasePrice;
                ii.Total += ii.ExtraUserPrice * ii.ExtraUserLicences;
            }
        }

        static List<Model.organizations> _cache = new List<Model.organizations>();
        static Dictionary<long, string> _cacheOwner = new Dictionary<long, string>();

        private void AddNameAndOwner(Model.CalaniEntities db, InvoicedMonthItem ii)
        {
            var e = (from r in _cache where r.id == ii.CustomerId select r).FirstOrDefault();
            if(e == null)
            {
                var org = (from r in db.organizations where r.id == ii.CustomerId select r).First();
                string email = (from r in db.contacts where r.id == org.owner select r.primaryEmail).FirstOrDefault();
                if (!_cacheOwner.ContainsKey(org.id)) _cacheOwner.Add(org.id, email);
                _cache.Add(org);
                e = org;
            }
            if(e != null)
            {
                ii.CustomerName = e.name;
                ii.CustomerOwnerEmail = _cacheOwner[ii.CustomerId];
            }
        }
    }

    public class InvoicedMonth
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Customers { get; set; }
        public int ExtraUserLicences { get; set; }

        public List<InvoicedMonthItem> Items { get; set; }

    }

    public class InvoicedMonthItem
    {
        public long CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerOwnerEmail { get; set; }
        public int ExtraUserLicences { get; set; }

        public string Code { get; set; }

        public double BasePrice { get; set; }
        public double ExtraUserPrice { get; set; }
        public double Total { get; set; }
    }
}
