﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Partnership
{
    public class PartnerCode
    {
        public string Code { get; set; }
        public bool Active { get; set; }
        public long Limit { get; set; }
        public long UsersCount { get; set; }
        public double PlanPrice { get; set; }
        public double ExtraUserPrice { get; set; }
    }
}
