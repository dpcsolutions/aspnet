﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace Calani.BusinessObjects.Generic
{
    public class ImageManager : IDisposable
    {
        System.IO.MemoryStream _ms;
        Bitmap _bmp;

        public int TargetWidth { get; set; }
        public int TargetHeight { get; set; }

        public int SourceWidth { get { return _bmp != null ? _bmp.Width : 0; } }
        public int SourceHeight { get { return _bmp != null ? _bmp.Height : 0; } }

        public bool Crop { get; set; }

        public int MaxSide { get; set; }

        public ImageManager()
        {

        }



        public void Init(byte[] source)
        {
            _ms = new System.IO.MemoryStream(source);
            _bmp = (Bitmap)Bitmap.FromStream(_ms);

            if (MaxSide > 0)
            {
                int maxHeight = MaxSide, maxWidth = MaxSide;

                if (maxHeight >= _bmp.Height && maxWidth >= _bmp.Width)
                    return;

                var newWidth = _bmp.Width * maxHeight / _bmp.Height;

                if (newWidth > maxWidth)
                {
                    newWidth = maxWidth;
                    maxHeight = _bmp.Height * newWidth / _bmp.Width;
                }

                var b = new Bitmap(newWidth, maxHeight);
                using (var graphic = Graphics.FromImage(b))
                {
                    graphic.Clear(Color.White);
                    graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphic.SmoothingMode = SmoothingMode.HighQuality;
                    graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    graphic.CompositingQuality = CompositingQuality.HighQuality;
                    graphic.DrawImage(_bmp, 0, 0, newWidth, maxHeight);
                    
                    _bmp.Dispose();
                    _bmp = b;
                }
            }
            else
            {
                if (!Crop)
                {
                    double ratio = 1;
                    if (_bmp.Width != TargetWidth && _bmp.Height != TargetHeight)
                    {

                        if (_bmp.Width >= _bmp.Height)
                        {
                            //landscape image or square
                            ratio = Convert.ToDouble(TargetWidth) / Convert.ToDouble(_bmp.Width);
                        }
                        else
                        {
                            // portrait image
                            ratio = Convert.ToDouble(TargetHeight) / Convert.ToDouble(_bmp.Height);
                        }

                        int nWidth = (int)(_bmp.Width * ratio);
                        int nHeight = (int)(_bmp.Height * ratio);

                        Bitmap b = new Bitmap(TargetWidth, TargetHeight);
                        Graphics g = Graphics.FromImage((Image)b);
                        g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        g.DrawImage(_bmp, (TargetWidth - nWidth) / 2, (TargetHeight - nHeight) / 2, nWidth, nHeight);
                        _bmp.Dispose();
                        _bmp = b;
                    }
                }
                else
                {
                    double ratio = 1;
                    if (_bmp.Width != TargetWidth && _bmp.Height != TargetHeight)
                    {

                        if (_bmp.Width >= _bmp.Height)
                        {
                            //landscape image or square
                            ratio = Convert.ToDouble(TargetHeight) / Convert.ToDouble(_bmp.Height);
                            
                        }
                        else
                        {
                            // portrait image
                            ratio = Convert.ToDouble(TargetWidth) / Convert.ToDouble(_bmp.Width);
                        }

                        int nWidth = (int)(_bmp.Width * ratio);
                        int nHeight = (int)(_bmp.Height * ratio);

                        Bitmap b = new Bitmap(TargetWidth, TargetHeight);
                        Graphics g = Graphics.FromImage((Image)b);
                        g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        g.DrawImage(_bmp,(TargetWidth - nWidth) / 2,(TargetHeight - nHeight) / 2,nWidth, nHeight);
                        _bmp.Dispose();
                        _bmp = b;
                    }
                }

            }
        }
        

        public byte[] Save()
        {
            return Save(ImageFormat.Png);
        }

        public byte[] Save(ImageFormat format)
        {
            MemoryStream msexp = new MemoryStream();
            if (format == ImageFormat.Jpeg)
            {
                JpegEncoder enc = new JpegEncoder(90);
                _bmp.Save(msexp, enc.Codec, enc.CodecParams);
            }
            else
            {
                _bmp.Save(msexp, format);                
            }
            byte[] ret = msexp.ToArray();
            msexp.Dispose();
            return ret;
        }

        public void Dispose()
        {
            _bmp.Dispose();
            _ms.Dispose();
        }


       
    }

    class JpegEncoder
    {

        public System.Drawing.Imaging.ImageCodecInfo Codec { get; private set; }
        /// <summary>
        /// Parameters
        /// </summary>
        public System.Drawing.Imaging.EncoderParameters CodecParams { get; private set; }


        /// <summary>
        /// Get Jpeg encoder
        /// </summary>
        /// <param name="percent">quality ratio (from 1 to 100)</param>
        public JpegEncoder(int percent)
        {

            long p = Convert.ToInt64(percent);
            System.Drawing.Imaging.ImageCodecInfo jpegEncoder = GetEncoder(System.Drawing.Imaging.ImageFormat.Jpeg);
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            System.Drawing.Imaging.EncoderParameters myEncoderParameters = new System.Drawing.Imaging.EncoderParameters(1);
            System.Drawing.Imaging.EncoderParameter myEncoderParameter = new System.Drawing.Imaging.EncoderParameter(myEncoder, p);
            myEncoderParameters.Param[0] = myEncoderParameter;

            Codec = jpegEncoder;
            CodecParams = myEncoderParameters;
        }

        private System.Drawing.Imaging.ImageCodecInfo GetEncoder(System.Drawing.Imaging.ImageFormat format)
        {
            System.Drawing.Imaging.ImageCodecInfo[] codecs = System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders();
            foreach (System.Drawing.Imaging.ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }



    }
}
