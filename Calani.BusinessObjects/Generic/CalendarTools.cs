﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Generic
{
    public class CalendarTools
    {
        public DateTime Monday { get; private set; }
        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }

        public int Year { get; private set; }
        public int Week { get; private set; }


        public CalendarTools(int year, int week)
        {
            Year = year;
            Week = week;
            Monday = GetIso8601MondayOfWeek(year, week);
            Start = Monday;
            End = Start.AddDays(7);
        }

        public CalendarTools(DateTime monday)
        {
            Monday = monday;
            Start = Monday;
            End = Start.AddDays(7);
            Year = Monday.Year;
            Week = GetNonIsoWeekOfYear(Monday);
        }
        
        public static int GetNonIsoWeekOfYear(DateTime time)
        {
            DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
            var year = time.Year;
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday) time = time.AddDays(3);
            if (year != time.Year) time = time.AddDays(-3);

            var weekNumber = CultureInfo.InvariantCulture.Calendar
                .GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            
            return weekNumber;
        }

        // public static int GetIso8601WeekOfYear(DateTime time)
        // {
        //     // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
        //     // be the same week# as whatever Thursday, Friday or Saturday are,
        //     // and we always get those right
        //     DayOfWeek day = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time);
        //     if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
        //     {
        //         time = time.AddDays(3);
        //     }
        //
        //     // Return the week of our adjusted day
        //     return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        // }

        public static DateTime GetIso8601MondayOfWeek(int year, int week)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = week;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            var result = firstThursday.AddDays(weekNum * 7);
            return result.AddDays(-3);
        }

        public static DateTime GetIso8601SundayOfWeek(int year, int week)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = week;
            if (firstWeek <= 1)
            {
                weekNum -= 1;
            }
            var result = firstThursday.AddDays(weekNum * 7);
            return result.AddDays(3);
        }

        public static int GetWeekOfMonth(DateTime dt)
        {

            int weekOfYear = GetNonIsoWeekOfYear(dt);

            if (dt.Month == 1)
            {
                //week of year == week of month in January
                //this also fixes the overflowing last December week
                return weekOfYear;
            }

            int weekOfYearAtFirst = GetNonIsoWeekOfYear(dt.AddDays(1 - dt.Day));
            return weekOfYear - weekOfYearAtFirst + 1;
        }


        public static DateTime? ParseDate(string date)
        {
            if (!String.IsNullOrWhiteSpace(date))
            {
                date = date.Replace(".", "/");
                var r = date.Split('/');
                if(r.Length == 3)
                {
                    string dd = r[0];
                    string mm = r[1];
                    string yy = r[2];
                    while (dd.Length < 2) dd = "0" + dd;
                    while (mm.Length < 2) mm = "0" + mm;
                    while (yy.Length < 4) yy = "0" + yy;

                    date = dd + "/" + mm + "/" + yy;
                }
            }
            DateTime? ret = null;
            if (date != null)
            {
                DateTime d1 = new DateTime(1, 1, 1);
                if (DateTime.TryParseExact(date, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture,
                        System.Globalization.DateTimeStyles.NoCurrentDateDefault, out d1))
                {

                    string str = d1.ToString("yyyy-MM-dd ");

                    if (DateTime.TryParse(str, out d1))
                    {
                        ret = d1;
                    }
                }

            }
            return ret;
            
        }
    }
}
