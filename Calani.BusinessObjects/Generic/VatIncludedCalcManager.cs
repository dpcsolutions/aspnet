﻿namespace Calani.BusinessObjects.Generic
{
    public static class VatIncludedCalcManager
    {
        public static double GetPriceExcludingVat(double? value, double? rate)
        {
            return value - GetVatSum(value,rate) ?? 0;
        }
        
        public static double GetVatSum(double? value, double? rate)
        {
            return value / (1  + (rate ?? 0) / 100) ?? 0;
        }
    }
}