﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Calani.BusinessObjects.Generic
{
    public class CSVReader
    {
        public System.Text.Encoding FileEncoding { get; set; }
        public string FilePath { get; set; }
        public string ColumnSeparator { get; set; }


        public System.Data.DataSet DataSet { get; private set; }
        private System.Data.DataTable _table;
        public int Errors { get; private set; }

        public CSVReader()
        {
            ColumnSeparator = ";";
        }

        public void LoadFile(string filePath)
        {
            FilePath = filePath;
            LoadFile();
        }

        public void LoadFile()
        {
            List<string> lines = File.ReadLines(FilePath, FileEncoding).ToList();
            string header = lines.FirstOrDefault();
            ParseHeader(header);
            for (int i = 1; i < lines.Count(); i++)
            {
                try
                {
                    ParseLine(lines[i]);
                }
                catch
                {
                    Errors++;
                }
            }
        }


        public void LoadBin(byte[] data)
        {
            string text = Encoding.UTF8.GetString(data);
            List<string> lines = text.Split('\n').ToList();
            string header = lines.FirstOrDefault();
            ParseHeader(header);

            for (int i = 1; i < lines.Count; i++)
            {
                try
                {
                    ParseLine(lines[i]);
                }
                catch
                {
                    Errors++;
                }
            }
        }

        public void LoadStream()
        {


        }

        private void ParseHeader(string header)
        {
            DataSet = new System.Data.DataSet();
            _table = new System.Data.DataTable("Source");
            DataSet.Tables.Add(_table);
            string[] cells = header.Split(new string[] { ColumnSeparator }, StringSplitOptions.None);
            foreach (string colName in cells)
            {
                _table.Columns.Add(colName);
            }
        }

        private void ParseLine(string line)
        {
            string[] cells = line.Split(new string[] { ColumnSeparator }, StringSplitOptions.None);
            var row = _table.NewRow();
            int i = 0;
            foreach (var cell in cells)
            {
                row[i] = cell;
                i++;
            }
            _table.Rows.Add(row);
        }

        public System.Data.DataSet Map(Dictionary<string, string> filters, Dictionary<string, string> columnsToColums)
        {
            System.Data.DataSet export = new System.Data.DataSet();
            System.Data.DataTable exportTable = new System.Data.DataTable("Export");
            export.Tables.Add(exportTable);
            foreach (string v in columnsToColums.Values)
            {
                exportTable.Columns.Add(v);
            }

            foreach (System.Data.DataRow item in _table.Rows)
            {
                bool pass = IsFiltered(item, filters);
                if (pass)
                {
                    System.Data.DataRow r = exportTable.NewRow();
                    TranslateRow(item, columnsToColums, r);
                    exportTable.Rows.Add(r);
                }
            }


            return export;
        }

        

        private bool IsFiltered(System.Data.DataRow item, Dictionary<string, string> filters)
        {
            if (filters == null) return true;
            if (filters.Count == 0) return true;

            foreach (string filterKey in filters.Keys)
            {
                string filterVal = filters[filterKey];
                string cellVal = item[filterKey].ToString();
                if (!filterVal.Equals(cellVal)) return false;
            }
            return true;
        }

        private void TranslateRow(System.Data.DataRow sourceRow, Dictionary<string, string> translation, System.Data.DataRow targetRow)
        {
            foreach (string translationFrom in translation.Keys)
            {
                string translationTo = translation[translationFrom];

                targetRow[translationTo] = sourceRow[translationFrom];
            }
        }

        public Dictionary<string, string> BuildTranslationDictionnary(string query)
        {
            Dictionary<string, string> ret = new Dictionary<string, string>();

            string[] lines = query.Split(new string[] { ";" }, StringSplitOptions.None);
            lines = (from r in lines where !String.IsNullOrWhiteSpace(r) select r).ToArray();

            foreach (string line in lines)
            {
                string[] kv = line.Split(new string[] { " as " }, StringSplitOptions.None);
                if (kv.Length == 2)
                {
                    ret.Add(kv[0].Trim(), kv[1].Trim());
                }
                else if (kv.Length == 1)
                {
                    ret.Add(kv[0].Trim(), kv[0].Trim());
                }
            }
            return ret;
        }
    }
}
