﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Calani.BusinessObjects.Generic
{
    public class Tools
    {
        public static DateTime GetFirstDateOfWeek(int year, int week)
        {
            var firstThursdayOfYear = new DateTime(year, 1, 1);
            while (firstThursdayOfYear.DayOfWeek != DayOfWeek.Thursday)
            {
                firstThursdayOfYear = firstThursdayOfYear.AddDays(1);
            }

            var startDateOfWeekOne = firstThursdayOfYear.AddDays(-(DayOfWeek.Thursday - DayOfWeek.Monday));

            return startDateOfWeekOne.AddDays(7 * (week - 1));
        }

        public static int GetWeekNumber(DateTime dt)
        {
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(dt, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
            return weekNum;
        }
        public static void CopyAllProperties(object source, object extendedTarget)
        {
            Type t = source.GetType();
            Type t2 = extendedTarget.GetType();
            foreach (var prop in t.GetProperties())
            {
                object o = prop.GetValue(source);
                var prop2 = t2.GetProperty(prop.Name);
                if (prop2 != null && prop2.CanWrite)
                {
                    prop2.SetValue(extendedTarget, o);
                }
            }
        }

        public static char[] AlphabetChars
        {
            get
            {
                return new []{'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

            }
        }
        public static string[] ExcelAlphabetChars
        {
            get
            {
                return new[]
                {
                    "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" ,
                    "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ",
                    "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ"
                };

            }
        }
        public static string GetSql(object o)
        {
            string ret = null;
            Type t = o.GetType();
            var prop = t.GetProperty("Sql");
            ret = prop.GetValue(o).ToString();
            return ret;
        }

        public static string IsoDateFormat
        {
            get { return "yyyy-MM-dd"; }
        }

        public static string IsoLongDateFormat
        {
            get { return "yyyy-MM-dd HH:mm:ss"; }
        }

        public static string DefaultDateFormat
        {
            get { return "dd/MM/yyyy"; }
        }

        public static string DefaultDateTimeFormat
        {
            get { return "dd/MM/yyyy HH:mm:ss"; }
        }

        public static string DefaultDateShortTimeFormat
        {
            get { return "dd/MM/yyyy HH:mm"; }
        }

        public static string DefaultTimeFormat
        {
            get { return " HH:mm:ss"; }
        }

        public static string DefaultShortTimeFormat
        {
            get { return " HH:mm"; }
        }

        public static string MomentDefaultDateShortTimeFormat
        {
            get { return "DD/MM/YYYY HH:mm"; }
        }

        public static string MomentDefaultDateFormat
        {
            get { return DefaultDateFormat.ToUpperInvariant(); }
        }
        
        public static string ProjectVisitDateFormat
        {
            get
            {
                return "yyyy-MM-ddTHH:mm:ss+00:00";
            }
        }

        public static string GetEmbeddedResourceText(string resourceName, Assembly resourceAssembly)
        {
            using (Stream stream = resourceAssembly.GetManifestResourceStream("Calani.BusinessObjects.Resources." + resourceName))
            {
                int streamLength = (int)stream.Length;
                byte[] data = new byte[streamLength];
                stream.Read(data, 0, streamLength);

                // lets remove the UTF8 file header if there is one:
                if ((data[0] == 0xEF) && (data[1] == 0xBB) && (data[2] == 0xBF))
                {
                    byte[] scrubbedData = new byte[data.Length - 3];
                    Array.Copy(data, 3, scrubbedData, 0, scrubbedData.Length);
                    data = scrubbedData;
                }

                return System.Text.Encoding.UTF8.GetString(data);
            }
        }


        public static double GetBusinessDays(DateTime from, DateTime to)
        {
            var dayDifference = (int)to.Subtract(from).TotalDays;
            return Enumerable
                .Range(1, dayDifference)
                .Select(x => from.AddDays(x))
                .Count(x => x.DayOfWeek != DayOfWeek.Saturday && x.DayOfWeek != DayOfWeek.Sunday);
        }
    }
    
}
