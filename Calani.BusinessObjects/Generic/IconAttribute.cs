﻿using System;

namespace Calani.BusinessObjects.Generic
{
    [AttributeUsage(AttributeTargets.Field)]
    public class IconAttribute : Attribute
    {
        public string Icon { get; private set; }

        public IconAttribute(string colorAttributeValue)
        {
            Icon = colorAttributeValue;
        }
    }
}
