﻿using System;

namespace Calani.BusinessObjects.Generic
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ColorAttribute : Attribute
    {
        public string Color { get; private set; }

        public ColorAttribute(string colorAttributeValue)
        {
            Color = colorAttributeValue;
        }
    }
}
