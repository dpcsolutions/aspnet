﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;

namespace Calani.BusinessObjects.Generic
{
    /// <summary>
    /// This abstract Manager is designed to be inherited.
    /// Please create a new class with a constructor(long organizationId).
    /// All properties and method in the "To Override" regions are designed to be implemented.
    /// </summary>
    /// <typeparam name="T">Entity class</typeparam>
    /// <typeparam name="K">Key (id) type</typeparam>
    public abstract class SmartCollectionManager<T, K> where T:class
    {

        #region [To Override] : Properties to implement in constructor(long organizationId).

        /// <summary>
        /// Current organiztion id of the caller (user). Allow manager to manage partition security.
        /// Set -1 to disable checks.
        /// </summary>
        public long OrganizationID { get; set; }

        /// <summary>
        /// Set the key column name (type long). Maybe "id" ?
        /// </summary>
        public string KeyProperty { get; set; }

        /// <summary>
        /// Set the recordStatus column name (type int). Maybe "recordStatus" ?
        /// </summary>
        public string RecordStatusProperty { get; set; }

        #endregion


        #region [Options]
        /// <summary>
        /// Allow to view deleted records
        /// </summary>
        public bool IgnoreRecordStatus { get; set; }

        #endregion


        #region [To Override] : Base Methods to implement by override synthax

        /// <summary>
        /// Implement the collection selection in the entity (return db.CollectionName;)
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        internal virtual System.Data.Entity.DbSet<T> GetDbSet(Model.CalaniEntities db)
        {
            return null;
        }

        /// <summary>
        /// Implement the query to select all records available.
        /// Don't forget to manage OrganizationID and IgnoreRecordStatus properties behaviour.
        /// </summary>
        /// <returns></returns>
        internal virtual SmartQuery<T> GetBaseQuery()
        {
            SmartQuery<T> q = new SmartQuery<T>();
            return q;
        }

        /// <summary>
        /// Implement the query to select a single record by PK (id) value.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal virtual SmartQuery<T> GetById(K id)
        {
            SmartQuery<T> q = new SmartQuery<T>();
            return q;
        }

        

        

        internal virtual long GetRecordOrganization(T record)
        {
            return -1; // no check
        }
        #endregion

        
        #region [Option] : Can be implented to add business logic, advanced check to abort any action.

        /// <summary>
        /// Method to override if you need to add business logic before Db action. You can use the Abort method to set an error message and interrupt action.
        /// </summary>
        /// <param name="action"></param>
        internal virtual void BeforeAction(SmartAction<T> action)
        {

        }
        #endregion

        //
        //
        // ============================== GENERIC METHOD FOR APP DEVELOPPERS =========================
        //
        //


        #region [SDK] : Public method to use for your queries

        /// <summary>
        /// Set a single record by its PK value (id)
        /// </summary>
        /// <param name="id">the id</param>
        /// <returns>found entity</returns>
        public T Get(K id)
        {
            T ret = null;
            var q = GetById(id);
            var record = q.Query.FirstOrDefault();
            if (CheckRecordOrganization(record))
            {
                ret = record;
            }
            else
            {
                throw new Exception("Not allowed to access this record");
            }
            return ret;
        }

        /// <summary>
        /// List all available records
        /// </summary>
        /// <returns>entities</returns>
        public List<T> List()
        {
            List<T> ret = new List<T>();
            var q = GetBaseQuery();
            ret = q.Query.ToList();
            q.Context.Dispose();
            return ret;
        }


        /// <summary>
        /// Update a entity. All non virtual properties will be changed (it means all columns of the table, without using foreign key EF helpers)
        /// </summary>
        /// <param name="id">the id</param>
        /// <param name="record">the new entity state.</param>
        /// <returns>action result (including "Record" property)</returns>
        public SmartAction<T> Update(K id, T record)
        {
            SmartAction<T> ret = new SmartAction<T>(SmartActionType.Update);
            ret.Success = true;
            ret.Record = record;
            ret.Query = new SmartQuery<T>();
            DbSet<T> tbl = GetDbSet(ret.Query.Context);

            var existingRecordQuery = GetById(id);
            ret.ExistingRecord = existingRecordQuery.Query.FirstOrDefault();

            if (ret.ExistingRecord != null)
            {
                if (CheckRecordOrganization(record))
                {
                    BeforeAction(ret);
                    if (ret.Success)
                    {
                        CopyAllProperties(record, ret.ExistingRecord);


                        try
                        {
                            existingRecordQuery.Context.SaveChanges();
                            ret.Success = true;
                        }
                        catch (DbEntityValidationException valEx)
                        {
                            // manage db validation
                            DbEntityValidationException valError = valEx.GetBaseException() as DbEntityValidationException;
                            if (valError != null)
                            {
                                ret.ErrorMessage = String.Empty;
                                foreach (var error in valError.EntityValidationErrors)
                                {
                                    foreach (var validation in error.ValidationErrors)
                                    {
                                        ret.ErrorMessage = validation.ErrorMessage;
                                        ret.ErrorMessage += "\n";
                                    }
                                }
                                ret.ErrorMessage = ret.ErrorMessage.Trim('\n').Trim();
                                ret.ErrorMessage = ret.ErrorMessage.Replace("\n", "\r\n");
                                ret.ErrorType = "EntityValidation";
                            }
                            ret.Success = false;
                            // -->
                        }
                        catch (Exception ex)
                        {
                            // manage unmanaged error
                            ret.Success = false;
                            ret.ErrorType = "Unmanaged";
                            ret.ErrorMessage = ex.Message;
                            // -->
                        }
                    }
                    else
                    {
                        ret.Success = false;
                        ret.ErrorType = "DataManager";
                    }
                }
                else
                {
                    ret.Success = false;
                    ret.ErrorType = "Security";
                    ret.ErrorMessage = "Not allowed to access this record";
                }
            }
            else
            {
                ret.Success = false;
                ret.ErrorType = "NotFound";
                ret.ErrorMessage = "Record not exists";
            }
            ret.Query.Context.Dispose();
            return ret;
        }

        /// <summary>
        /// Create a new record in the db table
        /// </summary>
        /// <param name="record">the new record (if pk is autoinc, please leave it null)</param>
        /// <returns>action result (including "Record" property)</returns>
        public SmartAction<T> Add(T record)
        {
            SmartAction<T> ret = new SmartAction<T>(SmartActionType.Add);
            ret.Success = true;
            ret.Record = record;
            ret.ExistingRecord = Activator.CreateInstance(record.GetType()) as T;
            ret.Query = new SmartQuery<T>();
            DbSet<T> tbl = GetDbSet(ret.Query.Context);
            if (CheckRecordOrganization(record))
            {
                BeforeAction(ret);
                if (ret.Success)
                {
                    tbl.Add(record);

                    try
                    {
                        ret.Query.Context.SaveChanges();
                        ret.Success = true;
                    }
                    catch (DbEntityValidationException valEx)
                    { 
                        // manage db validation
                        DbEntityValidationException valError = valEx.GetBaseException() as DbEntityValidationException;
                        if (valError != null)
                        {
                            ret.ErrorMessage = String.Empty;
                            foreach (var error in valError.EntityValidationErrors)
                            {
                                foreach (var validation in error.ValidationErrors)
                                {
                                    ret.ErrorMessage = validation.ErrorMessage;
                                    ret.ErrorMessage += "\n";
                                }
                            }
                            ret.ErrorMessage = ret.ErrorMessage.Trim('\n').Trim();
                            ret.ErrorMessage = ret.ErrorMessage.Replace("\n", "\r\n");
                            ret.ErrorType = "EntityValidation";
                        }
                        ret.Success = false;
                        // -->
                    }
                    catch (Exception ex)
                    {
                        // manage unmanaged error
                        ret.Success = false;
                        ret.ErrorType = "Unmanaged";
                        ret.ErrorMessage = ex.Message;
                        // -->
                    }
                }
                else
                {
                    ret.Success = false;
                    ret.ErrorType = "DataManager";
                }
            }
            else
            {
                ret.Success = false;
                ret.ErrorType = "Security";
                ret.ErrorMessage = "Not allowed to comit this record to another organization";
            }
            ret.Query.Context.Dispose();            
            return ret;
        }

        /// <summary>
        /// Delete an existing record in the db table.
        /// </summary>
        /// <param name="id">the id</param>
        /// <param name="method">Implementation of Delete (Flag "recordStatus" or real delete db command)</param>
        /// <returns>action result</returns>
        public SmartAction<T> Delete(K id, DeleteMethod method = DeleteMethod.FlagRecordStatus)
        {
            SmartAction<T> ret = new SmartAction<T>(SmartActionType.Delete);
            ret.Success = true;
            ret.Query = new SmartQuery<T>();
            

            var existingRecordQuery = GetById(id);
            ret.ExistingRecord = existingRecordQuery.Query.FirstOrDefault();
            ret.Record = ret.ExistingRecord;
            DbSet<T> tbl = GetDbSet(existingRecordQuery.Context);

            if (ret.ExistingRecord != null)
            {
                if (CheckRecordOrganization(ret.ExistingRecord))
                {
                    BeforeAction(ret);
                    if (ret.Success)
                    {

                        if (method == DeleteMethod.FlagRecordStatus)
                        {
                            SetRecordStatus(ret.ExistingRecord, -1);

                        }
                        else if (method == DeleteMethod.DeleteRecord)
                        {
                            tbl.Remove(ret.ExistingRecord);
                        }


                        try
                        {
                            existingRecordQuery.Context.SaveChanges();
                            ret.Success = true;
                        }
                        catch (DbEntityValidationException valEx)
                        {
                            // manage db validation
                            DbEntityValidationException valError = valEx.GetBaseException() as DbEntityValidationException;
                            if (valError != null)
                            {
                                ret.ErrorMessage = String.Empty;
                                foreach (var error in valError.EntityValidationErrors)
                                {
                                    foreach (var validation in error.ValidationErrors)
                                    {
                                        ret.ErrorMessage = validation.ErrorMessage;
                                        ret.ErrorMessage += "\n";
                                    }
                                }
                                ret.ErrorMessage = ret.ErrorMessage.Trim('\n').Trim();
                                ret.ErrorMessage = ret.ErrorMessage.Replace("\n", "\r\n");
                                ret.ErrorType = "EntityValidation";
                            }
                            ret.Success = false;
                            // -->
                        }
                        catch (Exception ex)
                        {
                            // manage unmanaged error
                            ret.Success = false;
                            ret.ErrorType = "Unmanaged";
                            ret.ErrorMessage = ex.Message;
                            // -->
                        }
                    }
                    else
                    {
                        ret.Success = false;
                        ret.ErrorType = "DataManager";
                    }
                }
                else
                {
                    ret.Success = false;
                    ret.ErrorType = "Security";
                    ret.ErrorMessage = "Not allowed to access this record";
                }
            }
            else
            {
                ret.Success = false;
                ret.ErrorType = "NotFound";
                ret.ErrorMessage = "Record not exists";
            }
            ret.Query.Context.Dispose();
            return ret;
        }

        /// <summary>
        /// Restore a deleted record from trash to active. Only works if the delete was done by "recordStatus" flagging)
        /// </summary>
        /// <param name="id">the id to restore</param>
        /// <returns>action result (including "Record" property)</returns>
        public SmartAction<T> UnDelete(K id)
        {
            SmartAction<T> ret = new SmartAction<T>(SmartActionType.Undelete);
            ret.Success = true;
            ret.Query = new SmartQuery<T>();


            var existingRecordQuery = GetById(id);
            ret.ExistingRecord = existingRecordQuery.Query.FirstOrDefault();
            ret.Record = ret.ExistingRecord;
            DbSet<T> tbl = GetDbSet(existingRecordQuery.Context);

            if (ret.ExistingRecord != null)
            {
                if (CheckRecordOrganization(ret.ExistingRecord))
                {
                    BeforeAction(ret);
                    if (ret.Success)
                    {

                        SetRecordStatus(ret.ExistingRecord, 0);


                        try
                        {
                            existingRecordQuery.Context.SaveChanges();
                            ret.Success = true;
                        }
                        catch (DbEntityValidationException valEx)
                        {
                            // manage db validation
                            DbEntityValidationException valError = valEx.GetBaseException() as DbEntityValidationException;
                            if (valError != null)
                            {
                                ret.ErrorMessage = String.Empty;
                                foreach (var error in valError.EntityValidationErrors)
                                {
                                    foreach (var validation in error.ValidationErrors)
                                    {
                                        ret.ErrorMessage = validation.ErrorMessage;
                                        ret.ErrorMessage += "\n";
                                    }
                                }
                                ret.ErrorMessage = ret.ErrorMessage.Trim('\n').Trim();
                                ret.ErrorMessage = ret.ErrorMessage.Replace("\n", "\r\n");
                                ret.ErrorType = "EntityValidation";
                            }
                            ret.Success = false;
                            // -->
                        }
                        catch (Exception ex)
                        {
                            // manage unmanaged error
                            ret.Success = false;
                            ret.ErrorType = "Unmanaged";
                            ret.ErrorMessage = ex.Message;
                            // -->
                        }
                    }
                    else
                    {
                        ret.Success = false;
                        ret.ErrorType = "DataManager";
                    }
                }
                else
                {
                    ret.Success = false;
                    ret.ErrorType = "Security";
                    ret.ErrorMessage = "Not allowed to access this record";
                }
            }
            else
            {
                ret.Success = false;
                ret.ErrorType = "NotFound";
                ret.ErrorMessage = "Record not exists";
            }
            ret.Query.Context.Dispose();
            return ret;
        }
        #endregion

        #region [Internal Security Management]

        /// <summary>
        /// Checks if a record is same organization than the OrganizationID property.
        /// </summary>
        /// <param name="record"></param>
        /// <returns></returns>
        private bool CheckRecordOrganization(T record)
        {
            long orgId = GetRecordOrganization(record);
            if (orgId > 0 && OrganizationID > 0)
            {
                if (orgId != OrganizationID) return false;
            }
            return true;
        }
        #endregion

        #region [Internal entities generic handling by reflection]

        /// <summary>
        /// Copy all (non virtual) properties value of source entity to the target entity.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="target"></param>
        public virtual void CopyAllProperties(T source, T target)
        {
            // can be overrided if needed
            Type t = source.GetType();
            foreach (var prop in t.GetProperties())
            {
                if (prop.Name != KeyProperty)
                {
                    bool isvirtual = prop.GetGetMethod().IsVirtual;
                    if (!isvirtual)
                    {
                        object o = prop.GetValue(source);
                        if (prop.SetMethod != null)
                        {
                            prop.SetValue(target, o);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Set the recordStatus value of the record. Use the RecordStatusProperty property.
        /// </summary>
        /// <param name="record"></param>
        /// <param name="value"></param>
        public virtual void SetRecordStatus(object record, int value)
        {
            Type t = record.GetType();
            foreach (var prop in t.GetProperties())
            {
                if (prop.Name.ToLower() == RecordStatusProperty.ToLower())
                {
                    prop.SetValue(record, value);
                }
            }
        }
        #endregion


    }
}
