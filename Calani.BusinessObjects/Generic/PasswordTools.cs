﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Generic
{
    public class PasswordTools
    {
        public static string Hash(string p)
        {
            string ret = null;
            string k = "$$:-8cal." + p + ".4n!//sma\\pre";

                using (SHA1Managed sha1 = new SHA1Managed())
                {
                    var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(k));
                    var sb = new StringBuilder(hash.Length * 2);

                    foreach (byte b in hash)
                    {
                        // can be "x2" if you want lowercase
                        sb.Append(b.ToString("X2"));
                    }

                    ret= sb.ToString();
                }
            return ret;   
        }

        public static string RandomCode(int length)
        {
            Random rdm = new Random();
            string pool = "abcdefghijkmnpqrtuvwxyz0123456789";
            var chars = Enumerable.Range(0, length)
                .Select(x => pool[rdm.Next(0, pool.Length)]);
            string ret = new string(chars.ToArray());
            ret = ret.ToUpper();
            return ret;
        }
    }
}
