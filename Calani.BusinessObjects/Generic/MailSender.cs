﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Calani.BusinessObjects.Generic
{
    public class MailSender
    {
        public static string PublicUrl { get; set; }
        public static string MailTemplates { get; set; }

        public MailTo MailTo { get; set; }
        public List<MailTo> MailToMany { get; set; }

        public string ReplyTo { get; set; } 
        public string FromName { get; set; }


        public List<MailAttachment> Attachments { get; set; }

        public string CustomSubject { get; set; }
        public Dictionary<string,string> Variables { get; set; }

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public MailSender()
        {
            MailTo = new MailTo();
            MailToMany = new List<MailTo>();
            Variables = new Dictionary<string, string>();
            Attachments = new List<MailAttachment>();
        }

        public void SendErrorNotification(string[] to, string error, bool warning=false)
        {
            foreach (string e in to)
            {
                var msg = new DPMessenger.NewMessage();
                DPMessenger.DpPostMan post = new DPMessenger.DpPostMan();
                
                if (warning)
                {
                    msg.Init("GesMobile", "GesMobileErrors", "0", "WarningNotification");
                    msg.SetEmail("error@gesmobile.ch", e, null, "GesMobile Server Warning", error);
                }
                else
                {
                    msg.Init("GesMobile", "GesMobileErrors", "0", "ErrorNotification");
                    msg.SetEmail("error@gesmobile.ch", e, null, "GesMobile Server Error", error);
                }
                
                
                post.SendNow(msg);
            }
        }

        public void SendSmartHtmlEmail(string file, string type)
        {
            try
            {
                if (!Variables.ContainsKey("publicurl"))
                    Variables.Add("publicurl", Generic.MailSender.PublicUrl);

                string from = "no-reply@gesmobile.ch";

                if (ReplyTo != null)
                    from = ReplyTo;

                string filepath = System.IO.Path.Combine(MailTemplates, file);

                string subject = "GesMobile.ch";
                if (!String.IsNullOrWhiteSpace(CustomSubject))
                {
                    subject = CustomSubject;
                }
                else
                {
                    string filepath_subject = filepath.Substring(0, filepath.Length - 5) + ".txt";
                    if (System.IO.File.Exists(filepath_subject))
                    {
                        subject = System.IO.File.ReadAllText(filepath_subject);

                        if (Variables != null)
                        {
                            foreach (var item in Variables.Keys)
                            {
                                subject = subject.Replace("$" + item + "$", Variables[item]);
                            }
                        }
                    }

                }

                _log.Info($"SendSmartHtmlEmail. file:{file}, type:{type}, from:{from}, subject:{subject}");
                DPMessenger.DpPostMan post = new DPMessenger.DpPostMan();

                if (MailToMany != null && MailToMany.Count > 0)
                {
                    List<string> mails = new List<string>();
                    foreach (var r in MailToMany)
                    {
                        if (!mails.Contains(r.Email))
                        {
                            mails.Add(r.Email);

                            var msg = new DPMessenger.NewMessage();

                            msg.Init("GesMobile", "GesMobileCustomers", r.UserId.ToString(), type);
                            msg.SetEmailFromHtmlFile(from, r.Email, null, subject, filepath, Variables);
                            if (Attachments != null && Attachments.Count > 0)
                            {
                                foreach (var att in Attachments)
                                {
                                    msg.AddAttachement(att.File, att.Name);
                                }
                            }
                            if (!String.IsNullOrWhiteSpace(ReplyTo))
                                msg.FromEmail = ReplyTo;
                            if (!String.IsNullOrWhiteSpace(FromName))
                                msg.FromName = FromName;

                            _log.Info($"SendSmartHtmlEmail 1. to:{r.Email}, filepath:{filepath}");

                            post.SendNow(msg);
                        }
                    }
                }
                else if (MailTo != null)
                {
                    var msg = new DPMessenger.NewMessage();
                    msg.Init("GesMobile", "GesMobileCustomers", MailTo.UserId.ToString(), type);
                    msg.SetEmailFromHtmlFile(from, MailTo.Email, null, subject, filepath, Variables);

                    if (Attachments != null && Attachments.Count > 0)
                    {
                        foreach (var att in Attachments)
                        {
                            msg.AddAttachement(att.File, att.Name);
                        }
                    }
                    if (!String.IsNullOrWhiteSpace(ReplyTo))
                        msg.FromEmail = ReplyTo;

                    if (!String.IsNullOrWhiteSpace(FromName))
                        msg.FromName = FromName;

                    _log.Info($"SendSmartHtmlEmail 2. to:{ MailTo.Email}, filepath:{filepath}");

                    post.SendNow(msg);
                }

            }
            catch (Exception e)
            {
                _log.Error($"SendSmartHtmlEmail error. ", e);
            }
            
        }

        public void SendSmartHtmlEmail(string subject, string msgText, string type)
        {
            {
                try
                {
                    if (!Variables.ContainsKey("publicurl"))
                        Variables.Add("publicurl", Generic.MailSender.PublicUrl);

                    string from = "no-reply@gesmobile.ch";

                    if (ReplyTo != null)
                        from = ReplyTo;


                    if (!String.IsNullOrWhiteSpace(CustomSubject))
                    {
                        subject = CustomSubject;
                    }

                    _log.Info($"SendSmartHtmlEmail. msg:{msgText}, type:{type}, from:{from}, subject:{subject}");
                    DPMessenger.DpPostMan post = new DPMessenger.DpPostMan();

                    if (MailToMany != null && MailToMany.Count > 0)
                    {
                        List<string> mails = new List<string>();
                        foreach (var r in MailToMany)
                        {
                            if (!mails.Contains(r.Email))
                            {
                                mails.Add(r.Email);

                                var msg = new DPMessenger.NewMessage();

                                msg.Init("GesMobile", "GesMobileCustomers", r.UserId.ToString(), type);
                                msg.SetEmailFromHtml(from,r.Email, null, subject, msgText, Variables);
                                
                                if (Attachments != null && Attachments.Count > 0)
                                {
                                    foreach (var att in Attachments)
                                    {
                                        msg.AddAttachement(att.File, att.Name);
                                    }
                                }
                                if (!String.IsNullOrWhiteSpace(ReplyTo))
                                    msg.FromEmail = ReplyTo;
                                if (!String.IsNullOrWhiteSpace(FromName))
                                    msg.FromName = FromName;

                                _log.Info($"SendSmartHtmlEmail 1. to:{r.Email}, msg:{msgText}");

                                post.SendNow(msg);
                            }
                        }
                    }
                    else if (MailTo != null)
                    {
                        var msg = new DPMessenger.NewMessage();
                        msg.Init("GesMobile", "GesMobileCustomers", MailTo.UserId.ToString(), type);
                        msg.SetEmailFromHtml(from, MailTo.Email, null, subject, msgText, Variables);

                        if (Attachments != null && Attachments.Count > 0)
                        {
                            foreach (var att in Attachments)
                            {
                                msg.AddAttachement(att.File, att.Name);
                            }
                        }
                        if (!String.IsNullOrWhiteSpace(ReplyTo))
                            msg.FromEmail = ReplyTo;

                        if (!String.IsNullOrWhiteSpace(FromName))
                            msg.FromName = FromName;

                        _log.Info($"SendSmartHtmlEmail 2. to:{ MailTo.Email},  msg:{msgText}");

                        post.SendNow(msg);
                    }

                }
                catch (Exception e)
                {
                    _log.Error($"SendSmartHtmlEmail error. ", e);
                }

            }
        }
    }

    public class MailTo
    {
        public string Email { get; set; }
        public long UserId { get; set; }
    }

    public class MailAttachment
    {
        public string File { get; set; }

        public string CustomName { get; set; }

        public string Name
        {
            get
            {
                if (!String.IsNullOrWhiteSpace(CustomName)) return CustomName;
                System.IO.FileInfo fi = new System.IO.FileInfo(File);
                return fi.Name;
            }
        }
    }
}
