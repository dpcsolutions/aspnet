﻿using System;

namespace Calani.BusinessObjects.Generic
{
    public class IncrementalNumber
    {
        public string Prefix { get; set; }

        public int Counter { get; set; }

        public IncrementalNumber(string label, bool addDash)
            :this(label)
        {
            if (addDash)
            {
                if(!string.IsNullOrEmpty(Prefix) && Prefix[Prefix.Length - 1] != '-')
                {
                    Prefix += '-';
                }
            }
        }

        public IncrementalNumber(string label)
        {
            if(String.IsNullOrWhiteSpace(label))
            {
                return;
            }
            int numberlength = 0;

            char[] caracters = label.ToCharArray();
            for (int i = caracters.Length - 1; i >= 0; i--)
            {
                int x = 0;
                if(Int32.TryParse(caracters[i].ToString(), out x))
                {
                    if (x.ToString() == caracters[i].ToString())
                    {
                        numberlength++;
                    }
                }
                else
                {
                    break;
                }
            }

            if(numberlength == 0)
            {
                Prefix = label;
                Counter = 1;
            }
            else
            {
                Prefix = label.Substring(0, label.Length - numberlength);
                string suffix = label.Substring(label.Length - numberlength);
                int c = 1;
                Int32.TryParse(suffix, out c);
                Counter = c;
            }
        }

        public override string ToString()
        {
            return Prefix + Counter;
        }

        public void Increment()
        {
            Counter++;
        }
    }
}
