﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Generic
{
    public static class StringExtentions
    {
        public static String GetSystemFriendlyStringWithEuropeanCharacters(this string phrase)
        {
            return System.Text.RegularExpressions.Regex.Replace(phrase.ToLower(), @"\W", "_"); // Remove all non-alphanumeric chars          
        }
    }
}
