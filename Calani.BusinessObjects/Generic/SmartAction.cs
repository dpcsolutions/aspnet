﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Generic
{
    /// <summary>
    /// Describe an action on database (type: add, update, delete...), record ...
    /// </summary>
    /// <typeparam name="T">Entity</typeparam>
    public class SmartAction<T> where T : class
    {
        /// <summary>
        /// Type of action (Add / Update / Delete)...
        /// </summary>
        public SmartActionType Type { get; set; }

        /// <summary>
        /// Record the action is working on (you can modify it) before context SaveChanges action.
        /// </summary>
        public T Record { get; set; }

        /// <summary>
        /// The existing record in db.
        /// </summary>
        public T ExistingRecord { get; set; }

        /// <summary>
        /// True if the action finish successfully.
        /// False if error.
        /// You can manualy set False to abort.
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Error message (for user).
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Error Type ("EntityValidation", "DataManager", "Security", "NotFound", "Unmanaged")...
        /// </summary>
        public string ErrorType { get; set; }

        /// <summary>
        /// EDF Query and Context.
        /// </summary>
        public SmartQuery<T> Query { get; set; }

        /// <summary>
        /// Define a smart action
        /// </summary>
        /// <param name="type">Type of action (Add / Update / Delete)...</param>
        public SmartAction(SmartActionType type)
        {
            Type = type;
        }

        /// <summary>
        /// Abort an action (shortcut to turn success false and set a message).
        /// </summary>
        /// <param name="message"></param>
        public void Abort(string message)
        {
            Success = false;
            ErrorMessage = message;
        }
    }

    /// <summary>
    /// Type of action (Add / Update / Delete)...
    /// </summary>
    public enum SmartActionType
    {
        Add,
        Update,
        Delete,
        Undelete
    }

    /// <summary>
    /// Implementation of Delete (Flag "recordStatus" or real delete db command)
    /// </summary>
    public enum DeleteMethod
    {
        FlagRecordStatus = 0,
        DeleteRecord = 1
    }
}
