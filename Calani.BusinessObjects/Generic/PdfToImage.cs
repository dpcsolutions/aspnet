﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Ghostscript.NET.Rasterizer;

namespace Calani.BusinessObjects.Generic
{
    public class PdfToImage
    {

        const float PREVIEW_RES = 84.6667F;
        public string[] Convert(string pdf, float factor)
        {


            var dpi = 300; //set the DPI
            var pageNumber = 1; // the pages in a PDF document

            FileInfo fi = new FileInfo(pdf);



            using (var rasterizer = new GhostscriptRasterizer()) //create an instance for GhostscriptRasterizer
            {

                rasterizer.Open(fi.FullName); //opens the PDF file for rasterizing

                //set the output image(png's) complete path
                var outputPNGPath = Path.Combine(fi.Directory.FullName, string.Format("{0}.jpg", fi.Name));

                //converts the PDF pages to png's 
                var pdf2PNG = rasterizer.GetPage(dpi,  pageNumber);

                //save the png's
                pdf2PNG.Save(outputPNGPath, System.Drawing.Imaging.ImageFormat.Jpeg);


            }

            return null;
        }
    }
}
