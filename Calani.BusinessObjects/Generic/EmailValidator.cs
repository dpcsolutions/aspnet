﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Generic
{
    public class EmailValidator
    {
        public static bool IsEmailValid(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                bool valid = addr.Address == email;


                //https://tempr.email/en/

                List<string> forbiddenServices = new List<string>();
                forbiddenServices.Add("@yopmail.com");
                forbiddenServices.Add("@20minutemail.it");
                forbiddenServices.Add("@maildrop.cc");
                forbiddenServices.Add("@dispostable.com");
                forbiddenServices.Add("@sharklasers.com");
                forbiddenServices.Add("@guerrillamail.info");
                forbiddenServices.Add("@guerrillamail.biz");
                forbiddenServices.Add("@guerrillamail.com");
                forbiddenServices.Add("@guerrillamail.de");
                forbiddenServices.Add("@guerrillamail.net");
                forbiddenServices.Add("@guerrillamail.org");
                forbiddenServices.Add("@guerrillamailblock.com");
                forbiddenServices.Add("@grr.la");
                forbiddenServices.Add("@pokemail.net");
                forbiddenServices.Add("@spam4.me");
                forbiddenServices.Add("@mailinator.com");
                forbiddenServices.Add("@tempr.email");
                forbiddenServices.Add("@discard.email");
                forbiddenServices.Add("@discardmail.com");
                forbiddenServices.Add("@discardmail.de");
                forbiddenServices.Add("@spambog.de");
                forbiddenServices.Add("@spambog.ru");
                forbiddenServices.Add("@0815.ru");
                forbiddenServices.Add("@btcmail.pw");
                forbiddenServices.Add("@knol-power.nl");
                forbiddenServices.Add("@hartbot.de");
                forbiddenServices.Add("@freundin.ru");
                forbiddenServices.Add("@smashmail.de");
                forbiddenServices.Add("@s0ny.net");
                forbiddenServices.Add("@pecinan.net");
                forbiddenServices.Add("@budaya-tionghoa.com");
                forbiddenServices.Add("@lajoska.pe.hu");
                forbiddenServices.Add("@1mail.x24hr.com");
                forbiddenServices.Add("@from.onmypc.info");
                forbiddenServices.Add("@now.mefound.com");
                forbiddenServices.Add("@mowgli.jungleheart.com");
                forbiddenServices.Add("@yourspamgoesto.space");
                forbiddenServices.Add("@pecinan.org");
                forbiddenServices.Add("@budayationghoa.com");
                forbiddenServices.Add("@cr.cloudns.asia");
                forbiddenServices.Add("@tls.cloudns.asia");
                forbiddenServices.Add("@msft.cloudns.asia");
                forbiddenServices.Add("@b.cr.cloUdnS.asia");
                forbiddenServices.Add("@ssl.tls.cloudns.asia ");
                forbiddenServices.Add("@sweetxxx.de");
                forbiddenServices.Add("@dvd.dns-cloud.net");
                forbiddenServices.Add("@dvd.dnsabr.com");
                forbiddenServices.Add("@bd.dns-cloud.net");
                forbiddenServices.Add("@yx.dns-cloud.net");
                forbiddenServices.Add("@shit.dns-cloud.net");
                forbiddenServices.Add("@shit.dnsabr.com");
                forbiddenServices.Add("@eu.dns-cloud.net");
                forbiddenServices.Add("@eu.dnsabr.com");
                forbiddenServices.Add("@asia.dnsabr.com");
                forbiddenServices.Add("@8.dnsabr.com");
                forbiddenServices.Add("@pw.8.dnsabr.com");
                forbiddenServices.Add("@mm.8.dnsabr.com");
                forbiddenServices.Add("@23.8.dnsabr.com");
                forbiddenServices.Add("@email-mail.pw");
                forbiddenServices.Add("@x.dc74.ml");
                forbiddenServices.Add("@x.dc74.cf");
                forbiddenServices.Add("@pecinan.com");
                forbiddenServices.Add("@disposable-email.ml");
                forbiddenServices.Add("@pw.epac.to");
                forbiddenServices.Add("@giallo.cf");
                forbiddenServices.Add("@giallo.ml");
                forbiddenServices.Add("@giallo.gq");
                forbiddenServices.Add("@postheo.de");
                forbiddenServices.Add("@my.viola.gq");
                forbiddenServices.Add("@spam.viola.gq");
                forbiddenServices.Add("@tempr.email.viola.gq");
                forbiddenServices.Add("@email.viola.gq ");
                forbiddenServices.Add("@sexy.camdvr.org");
                forbiddenServices.Add("@gay.theworkpc.com");
                forbiddenServices.Add("@etr610.cf");
                forbiddenServices.Add("@etr610.ga");
                forbiddenServices.Add("@purple.viola.gq ");
                forbiddenServices.Add("@disposable.ml");
                forbiddenServices.Add("@disposable-e.ml");
                forbiddenServices.Add("@laokzmaqz.tech ");
                forbiddenServices.Add("@hotmailproduct.com");
                forbiddenServices.Add("@888.dns-cloud.net");
                forbiddenServices.Add("@adult-work.info");
                forbiddenServices.Add("@maxsize.online");
                forbiddenServices.Add("@1-3-3-7.net");
                forbiddenServices.Add("@casinokun.hu");
                forbiddenServices.Add("@bangsat.in");
                forbiddenServices.Add("@happyfreshdrink.com");
                forbiddenServices.Add("@sundriesday.com");
                forbiddenServices.Add("@8estcommunity.org");
                forbiddenServices.Add("@blessingvegetarian.com");
                forbiddenServices.Add("@wallus.casinokun.hu");
                forbiddenServices.Add("@denomla.com");
                forbiddenServices.Add("@flyjet.net");
                forbiddenServices.Add("@cronostv.site");
                forbiddenServices.Add("@mouadslider.site");
                forbiddenServices.Add("@trap-mail.de");
                forbiddenServices.Add("@umailz.com");
                forbiddenServices.Add("@panchoalts.com");
                forbiddenServices.Add("@dog.ms1.email");
                forbiddenServices.Add("@katergizmo.de");
                forbiddenServices.Add("@brunhilde.ml");
                forbiddenServices.Add("@giallo.tk");
                forbiddenServices.Add("@09stees.online");
                forbiddenServices.Add("@07stees.online");
                forbiddenServices.Add("@undergmail.com");
                forbiddenServices.Add("@esamario.com");

                foreach (var f in forbiddenServices)
                {
                    if (addr.Address.ToLower().EndsWith(f))
                    {
                        return false;
                    }
                }
               

               

                return valid;
            }
            catch
            {
                return false;
            }




        }
    }
}
