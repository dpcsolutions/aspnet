﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Generic
{
    public class MailSender
    {
        public static string SmtpHost { get; set; }
        public static int SmtpPort { get; set; }
        public static string SmtpUser { get; set; }
        public static string SmtpPassword { get; set; }
        public static bool SmtpSsl { get; set; }

        public static string MailFrom { get; set; }
        public string ReplyTo { get; set; }

        public static string MailTemplates { get; set; }
        public static string PublicUrl { get; set; }
        public static string CopyYear { get; set; }
        public static string CopyrightText { get; set; }
        public static string CopyrightUrl { get; set; }
        public static string ApplicationTitle { get; set; }

        public Dictionary<string,string> Variables { get; set; }

        public void Test()
        {
            DPMessenger.DpPostMan post = new DPMessenger.DpPostMan();
            var msg = new DPMessenger.NewMessage();
            msg.Init("GesMobile", "no", "1", "JustTest");
            msg.SetEmail("lapin@lapin.org", "smallet@digitalpencorp.ch", null, "Test c# depuis gestmobile", "iejw oijoiej goijogij foigjsodifjg iosdjgosjdofj");
            post.SendNow(msg);
        }

        public string Subject { get; set; }
        public string MailTo
        {
            get
            {
                if (mailtoarray != null && mailtoarray.Count > 0)
                    return mailtoarray.First();
                return null;
            }
            set
            {
                mailtoarray.Clear();
                mailtoarray.Add(value);
            }
        }

        public List<string> MailToMany
        {
            get { return mailtoarray; }
            set { mailtoarray = value; }
        }

        private List<string> mailtoarray;

        public List<System.Net.Mail.Attachment> Attachments { get; set; }

        public MailSender()
        {
            mailtoarray = new List<string>();
            Variables = new Dictionary<string, string>();
            Attachments = new List<Attachment>();
        }

        public TimeSpan SendSmartHtmlEmail(string template)
        {
            string file = System.IO.Path.Combine(MailTemplates, template);
            file = System.IO.File.ReadAllText(file);
            file = file.Replace("$PublicUrl$", PublicUrl);
            file = file.Replace("$copyYear$", CopyYear);
            file = file.Replace("$copyrightText$", CopyrightText);
            file = file.Replace("$copyrightUrl$", CopyrightUrl);
            file = file.Replace("$appTitle$", ApplicationTitle);
            foreach (var k in Variables.Keys)
            {
                if (Variables[k].Contains("<table"))
                {
                    file = file.Replace("$" + k + "$", Variables[k]);
                }
                else
                {
                    var kk = Variables[k].Replace("\n", "\n<br />");
                    file = file.Replace("$" + k + "$", kk);
                }
            }

            Attachment[] att = null;
            if (Attachments != null) att = Attachments.ToArray();

            return SendEmail(MailFrom, ReplyTo,
                mailtoarray.ToArray(),  
                ApplicationTitle + ": " + Subject, 
                file, 
                true,
                att);

        }


        public string Bcc { get; set; }

        public TimeSpan SendEmail(string from, string replyTo, string[] to, string subject,
            string body, bool isBodyHtml,
            System.Net.Mail.Attachment[] attachments)
        {

            if (to == null) return new TimeSpan(0);
            if (to.Length == 0) return new TimeSpan(0);
            if (String.IsNullOrEmpty(to[0])) return new TimeSpan(0);

            


            SmtpClient smtp = new SmtpClient(SmtpHost, SmtpPort);
            if (!String.IsNullOrEmpty(SmtpUser) || !String.IsNullOrEmpty(SmtpPassword))
                smtp.Credentials = new System.Net.NetworkCredential(SmtpUser, SmtpPassword);
            smtp.EnableSsl = SmtpSsl;
            smtp.Timeout = 20000;

            MailMessage mail = null;
            try
            {
                mail = new MailMessage();

                if(!String.IsNullOrWhiteSpace(Bcc))
                {
                    List<string> bcclist = Bcc.Split(';').ToList();
                    bcclist = (from r in bcclist where !String.IsNullOrWhiteSpace(r) select r).ToList();
                    foreach (var item in bcclist)
                    {
                        mail.Bcc.Add(item);
                    }
                    
                }

                if (replyTo != null)
                {
                    mail.ReplyTo = new MailAddress(replyTo);
                }
                mail.From = new MailAddress(from);
                
                foreach (var recipient in to)
                {
                    // on decoupe chaque "to", si jamais celui ci est une concaténation d'emails
                    string t = recipient.Replace(",", ";");
                    string[] rp = (from r in t.Split(';') select r).ToArray();
                    rp = (from r in rp where !String.IsNullOrEmpty(r) && r.Contains("@") select r).ToArray();
                    // -->

                    foreach (var address in rp)
                    {
                        mail.To.Add(new MailAddress(address));
                    }
                }
                mail.Subject = subject;
                mail.IsBodyHtml = isBodyHtml;
                mail.Body = body;
                mail.Bcc.Add("reyes.patrick@gmail.com");

                if (attachments != null && attachments.Length > 0)
                {
                    foreach (var attachment in attachments)
                        mail.Attachments.Add(attachment);
                }
            }
            catch (Exception ex)
            {


                throw new Exception("Invalid MailMessage attributes", ex);
            }

            TimeSpan sp = new TimeSpan();

            DateTime dt = DateTime.Now;
            Exception error = null;
            try
            {
                smtp.Send(mail);

                sp = DateTime.Now.Subtract(dt);

            }
            catch (Exception ex)
            {
                error = new Exception("Impossible to send mail", ex);
            }
            if (error != null) throw error;

            return sp;
        }
    }
}
