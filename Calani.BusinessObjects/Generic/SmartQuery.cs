﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Generic
{
    /// <summary>
    /// EF Query and Context
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SmartQuery<T> where T : class
    {
        /// <summary>
        /// The data context (can be used to make other queries for the same action).
        /// </summary>
        public Model.CalaniEntities Context { get; set; }

        /// <summary>
        /// The db query. Not executed yet, so can be modified.
        /// </summary>
        public IQueryable<T> Query { get; set; }

        /// <summary>
        /// New Query and Context
        /// </summary>
        public SmartQuery()
        {
            Context = new Model.CalaniEntities();
        }
    }
}
