﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Generic
{
    public class SqlFileLauncher
    {
        const string VARIDENTIFIER = "$$";
        string queries;

        public void LoadFile(string file)
        {
            queries = System.IO.File.ReadAllText(file);
        }

        public void SetQuery(string sql)
        {
            queries = sql;
        }

        private string EnsureLatin1Text(string data)
        {
            if (String.IsNullOrWhiteSpace(data)) return "";
            return data.Trim();
            /*
            byte[] l = Encoding.Default.GetBytes(data);
            string f = Encoding.UTF8.GetString(l);
            return f;*/
            /*
            byte[] utf8Bytes = Encoding.UTF8.GetBytes(data);
            byte[] isoBytes = Encoding.Convert(Encoding.GetEncoding("ISO-8859-1"), Encoding.UTF8, utf8Bytes);
            string uf8converted = Encoding.UTF8.GetString(isoBytes);
            return uf8converted;
            */
        }

        public System.Data.DataTable LaunchReport(string sql)
        {
            string csSection = "provider connection string=";
            string csSeparator = "\"";
            string cs = System.Configuration.ConfigurationManager.ConnectionStrings["CalaniEntities"].ConnectionString;
            
            int i = cs.IndexOf(csSection);
            cs = cs.Substring(i + csSection.Length);
            i = cs.LastIndexOf(csSeparator);
            cs = cs.Substring(0, i);
            cs = cs.Replace(csSeparator, "");
            cs += ";Charset=utf8;";

            MySqlConnection con = new MySqlConnection(cs);
           
            MySqlCommand script = new MySqlCommand(sql, con);
            

            System.Data.DataSet ds = new System.Data.DataSet();
            MySqlDataAdapter da = new MySqlDataAdapter(script);
            
            da.Fill(ds);

            script.Dispose();
            con.Dispose();

            var dt = ds.Tables[0];

            foreach (System.Data.DataRow r in dt.Rows)
            {
                foreach (System.Data.DataColumn c in dt.Columns)
                {
                    if (r[c.ColumnName] != null && r[c.ColumnName].GetType() == typeof(string))
                    {
                        r[c.ColumnName] = EnsureLatin1Text(r[c.ColumnName].ToString());
                    }
                }
            }

            return dt;
        }

        public int Launch(Dictionary<string, string> args = null)
        {
            string csSection = "provider connection string=";
            string csSeparator = "\"";
            string cs = System.Configuration.ConfigurationManager.ConnectionStrings["CalaniEntities"].ConnectionString;
            int i = cs.IndexOf(csSection);
            cs = cs.Substring(i + csSection.Length);
            i = cs.LastIndexOf(csSeparator);
            cs = cs.Substring(0, i);
            cs = cs.Replace(csSeparator, "");

            if (queries.Contains("CREATE DATABASE"))
            {
                int dbs = cs.IndexOf(";database=");
                int dbe = cs.IndexOf(";", dbs + 5);
                cs = cs.Remove(dbs, dbe - dbs);
            }

            var parsedQueries = queries;

            if(args != null)
            {
                foreach(var a in args)
                {
                    parsedQueries = parsedQueries.Replace(VARIDENTIFIER + a.Key + VARIDENTIFIER, a.Value);
                }
            }

            MySqlConnection con = new MySqlConnection(cs);
            MySqlScript script = new MySqlScript(con, parsedQueries);
            //script.Delimiter = "$$";

            int ret = 0;
            try
            {
                

                ret = script.Execute();
                con.Dispose();
            }
            catch (Exception e)
            {
                con.Dispose();
                throw e;
            }

            

            return ret;
        }
    }
}
