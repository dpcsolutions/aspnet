﻿using Calani.BusinessObjects.Enums;
using System;

namespace Calani.BusinessObjects.Generic
{
    public class DateRange
    {
        public DatesRangeEnum Range { get; private set;}
        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }
        public decimal DurationDays { get { return (decimal)(End - Start).TotalSeconds / (60 * 60 * 24); } }

        public DateRange(DateTime start, DateTime end)
        {
            if(start <= end)
            {
                Start = start;
                End = end;
            }
            else
            {
                Start = end;
                End = start;
            }
        }

        public DateRange(DatesRangeEnum range)
        {
            Range = range;
            Start = GetStartDate();
            End = GetEndDate();
        }

        public DateRange(int year)
        {
            Start = new DateTime(year, 1, 1);
            End = new DateTime(year, 12, 31, 23, 59, 59);
        }

        private DateTime GetStartDate()
        {
            DateTime date;
            DateTime today = DateTime.Today;
            int quarter = 0;

            switch (Range)
            {
                case DatesRangeEnum.CurrentMonth:
                    date = new DateTime(today.Year, today.Month, 1);
                    break;
                case DatesRangeEnum.PreviousMonth:
                    date = new DateTime(today.Year, today.Month, 1).AddMonths(-1);
                    break;
                case DatesRangeEnum.CurrentQuarter:
                    quarter = GetCurrentQuarter();
                    date = GetStartDateQuarter(quarter, today.Year);
                    break;
                case DatesRangeEnum.PreviousQuarter:
                    quarter = GetCurrentQuarter();
                    date = GetStartDateQuarter(quarter, today.Year).AddMonths(-3);
                    break;
                case DatesRangeEnum.Last2Years:
                    date = new DateTime(today.Year, today.Month, today.Day).AddYears(-2);
                    break;
                case DatesRangeEnum.PreviousYear:
                    date = new DateTime(today.Year - 1, 1, 1);
                    break;
                case DatesRangeEnum.Since1990:
                    date = new DateTime(1990, 1, 1);
                    break;
                case DatesRangeEnum.CurrentYear:
                default:
                    date = new DateTime(today.Year, 1, 1);                
                    break;
            }

            return date;
        }

        private DateTime GetEndDate()
        {
            DateTime date;
            DateTime today = DateTime.Now;
            int quarter = 0;

            switch (Range)
            {
                case DatesRangeEnum.CurrentMonth:
                    date = new DateTime(today.Year, today.Month, 1, 23, 59, 59).AddMonths(1).AddDays(-1);
                    break;
                case DatesRangeEnum.PreviousMonth:
                    date = new DateTime(today.Year, today.Month, 1, 23, 59, 59).AddDays(-1);
                    break;
                case DatesRangeEnum.CurrentQuarter:
                    quarter = GetCurrentQuarter();
                    date = GetEndDateQuarter(quarter, today.Year);
                    break;
                case DatesRangeEnum.PreviousQuarter:
                    quarter = GetCurrentQuarter();
                    date = GetEndDateQuarter(quarter, today.Year).AddMonths(-3);
                    break;
                case DatesRangeEnum.Last2Years:
                    date = new DateTime(today.Year, today.Month, today.Day, 23, 59, 59).AddDays(1);
                    break;
                case DatesRangeEnum.PreviousYear:
                    date = new DateTime(today.Year - 1, 12, 31, 23, 59, 59);
                    break;
                case DatesRangeEnum.Since1990:
                    date = DateTime.Now;
                    break;
                case DatesRangeEnum.CurrentYear:
                default:
                    date = new DateTime(today.Year, 12, 31, 23, 59, 59);
                    break;
            }

            return date;
        }

        private int GetCurrentQuarter()
        {
            var month = DateTime.Today.Month;
            var quarter = (int)Math.Ceiling((decimal)month / 3);
            return quarter;
        }

        private DateTime GetStartDateQuarter(int quarter, int year)
        {
            var firstMonth = (quarter - 1) * 3 + 1;
            var date = new DateTime(year, firstMonth, 1);
            return date;
        }

        private DateTime GetEndDateQuarter(int quarter, int year)
        {
            var lastMonth = (quarter - 1) * 3 + 3;
            var date = new DateTime(year, lastMonth, DateTime.DaysInMonth(year, lastMonth), 23, 59, 59);
            return date;
        }

        public TimeSpan HowMuchTimeFromRangeBelongsToYear(int year)
        {
            if (year < Start.Year || year > End.Year) return new TimeSpan(0);
            if (year == Start.Year && year == End.Year) return End - Start;
            if (year > Start.Year && year < End.Year) return new DateTime(year, 1, 1, 0, 0, 0) - new DateTime(year + 1, 1, 1, 0, 0, 0);

            var startOfYear = Start.Year == year ? Start : new DateTime(year, 1, 1, 0, 0, 0);
            var endOfYear = End.Year == year ? End : new DateTime(year + 1, 1, 1, 0, 0, 0);

            return endOfYear - startOfYear;
        }
    }
}
