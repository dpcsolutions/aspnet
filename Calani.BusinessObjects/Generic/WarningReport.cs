﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Calani.BusinessObjects.Generic
{
    public class WarningReport
    {
        public static void Report(string message, Exception exception)
        {
            string username = "unknown";
            string userid = "-1";
            string error = "unkown";
            string path = "";

            try
            {
                var currentContext = HttpContext.Current;
                username = "";
                if (currentContext.Session["UserName"] != null)
                {
                    username = currentContext.Session["UserName"].ToString();
                }
                

                if (currentContext.Session.Count == 0)
                {
                    if (HttpContext.Current.User.Identity.Name != null)
                    {
                        userid = HttpContext.Current.User.Identity.Name;
                    }
                }
                
            }
            catch { }

            try
            {
                Exception ex = exception;

                if (ex.InnerException != null) ex = ex.InnerException;

                error = ex.Message;
                error += "\r\n";
                error += "\r\n";
                error += ex.StackTrace;
                error += "\r\n";
                error += "\r\n";
                error += path;
            }
            catch { }


            string reqParams = "Params\r\n";
            try
            {

                var currentContext = HttpContext.Current;

                foreach (string k in currentContext.Request.Params.Keys)
                {
                    string val = currentContext.Request.Params[k];
                    reqParams += k + " = " + val;
                    reqParams += "\r\n";
                }



            }
            catch { }

            string body = "User Name: " + username;
            body += "\r\n";
            body += "User Id: " + userid;
            body += "\r\n";
            body += message;
            body += "\r\n";
            body += error;
            body += "\r\n";
            body += reqParams;
            Calani.BusinessObjects.Generic.MailSender email = new Calani.BusinessObjects.Generic.MailSender();
            string to = System.Configuration.ConfigurationManager.AppSettings["Bcc"];
            //email.SendEmail("error@gesmobile.ch", "error@gesmobile.ch", to.Split(';'), "GesMobile server error", body, false, null);
            email.SendErrorNotification(to.Split(';'), body, true);
        }
    }
}
