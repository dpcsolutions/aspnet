﻿using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Departments
{
    public class DepartmentsManager : Generic.SmartCollectionManager<Model.departments, int>
    {
        public DepartmentsManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;

        }


        internal override DbSet<Model.departments> GetDbSet(Model.CalaniEntities db)
        {
            return db.departments;
        }

        internal override SmartQuery<Model.departments> GetById(int id)
        {
            var q = base.GetById(id);
             q.Query = (from r in q.Context.departments where r.id == id select r);
             return q;
        }

        internal override long GetRecordOrganization(Model.departments record)
        {
            if (record.organizationId != null) return record.organizationId.Value;
            return -1;
        }

        internal override SmartQuery<Model.departments> GetBaseQuery()
        {
            var q = base.GetBaseQuery();



            // base
            q.Query = (from r in q.Context.departments select r);
            if (OrganizationID > -1) q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.name, r.code select r);


            return q;




        }

        internal override void BeforeAction(SmartAction<departments> action)
        {
            if (action.Type == SmartActionType.Delete )
            {
                var q = base.GetBaseQuery();

                if ((from r in q.Context.contacts where r.departmentId == action.Record.id select r).Count() > 0)
                {
                    action.Abort("ERR_Department_name_alredy_used");
                }
            }
        }

        public List<ImportedDataItem> BatchImport(List<string> columns, List<Imports.Row> rows)
        {
            var ret = new List<ImportedDataItem>();

            Model.CalaniEntities db = new CalaniEntities();
            List<Model.departments> adds = new List<departments>();
            try
            {
                foreach (var row in rows)
                {
                    Model.departments s = new departments();

                    s.name = ImportedDataItem.GetValue(row, "Name", columns);
                    s.code = ImportedDataItem.GetValue(row, "Code", columns);
                    s.description = ImportedDataItem.GetValue(row, "Description", columns);
                    s.organizationId = OrganizationID;
                    s.recordStatus = 0;
                    db.departments.Add(s);
                    adds.Add(s);
                }

                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }
           

            foreach (var r in adds)
            {
                ret.Add(new ImportedDataItem
                {
                    Id = r.id,
                    Name = r.name
                });
            }

            return ret;
        }

    }
}
