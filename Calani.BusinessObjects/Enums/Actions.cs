﻿namespace Calani.BusinessObjects.Enums
{
    public enum ItemActions
    {
        Create,
        Update,
        Delete,
        Validate,
        Reject
    }
}