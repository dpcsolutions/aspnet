﻿using System.ComponentModel;

namespace Calani.BusinessObjects.Enums
{
    public enum CancellationStatusEnum
    {
        [Description("None")]
        None = 0,

        [Description("CancellationRequested")]
        CancellationRequested = 1,

        [Description("Canceled")]
        Cancelled = 2,

        [Description("CancellationRejected")]
        CancellationRejected = 3,
    }
}
