﻿using System;
using System.ComponentModel;
using Calani.BusinessObjects.Generic;

namespace Calani.BusinessObjects.Enums
{
    public static class EnumHelper
    {
        public static string GetDescription(this Enum enumVal)
        {
            var attribute = GetAttribute<DescriptionAttribute>(enumVal);
            return attribute != null ? ((DescriptionAttribute)attribute).Description : string.Empty;
        }

        public static string GetColor(this Enum enumVal)
        {
            var attribute = GetAttribute<ColorAttribute>(enumVal);
            return attribute != null ? ((ColorAttribute)attribute).Color : string.Empty;
        }

        public static string GetIcon(this Enum enumVal)
        {
            var attribute = GetAttribute<IconAttribute>(enumVal);
            return attribute != null ? ((IconAttribute)attribute).Icon : string.Empty;
        }

        private static object GetAttribute<T>(Enum enumVal) where T : Attribute
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attributes = memInfo.Length > 0 ? memInfo[0].GetCustomAttributes(typeof(T), false) : new object[0];

            object attribute = attributes.Length > 0 ? attributes[0] : null;
            return attribute;
        }
    }
}
