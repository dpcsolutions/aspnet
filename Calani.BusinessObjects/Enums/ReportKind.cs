﻿namespace Calani.BusinessObjects.Enums
{
    public enum ReportKind
    {
        Employees = 1,
        Projects = 2,
        Services = 3,
    }
}