﻿using System.ComponentModel;

namespace Calani.BusinessObjects.Enums
{
    public enum TypeOfDiscountEnum : sbyte
    {
        None = 0,

        /// <summary>
        /// Escompte
        /// </summary>
        [Description("Discount_option")]
        Discount = 1,

        /// <summary>
        /// Rabais
        /// </summary>
        [Description("Rebate_option")]
        Rebate = 2,

        /// <summary>
        /// Ristourne
        /// </summary>
        [Description("Reduction_option")]
        Reduction = 3
    }
}
