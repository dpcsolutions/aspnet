﻿namespace Calani.BusinessObjects.Enums
{
    public enum ValidationEnum
    {
        Pending = 0,
        Approved = 1,
        Rejected = 2
    }
}
