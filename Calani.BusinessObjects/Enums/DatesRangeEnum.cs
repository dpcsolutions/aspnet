﻿namespace Calani.BusinessObjects.Enums
{
    public enum DatesRangeEnum
    {
        Undefined = 0,
        CurrentYear = 1,
        CurrentMonth = 2,
        CurrentWeek = 3,
        Last2Years = 4,
        PreviousYear = 5,
        PreviousMonth = 6,
        CurrentQuarter = 7,
        PreviousQuarter = 8,
        SpecificYear = 9,
        Since1990 = 10,
    }
}
