﻿namespace Calani.BusinessObjects.Enums
{
    public enum TimesheetEmailOptionsEnum
    {
        AlwaysNotifyOnValidation = 0,
        NotifyOnValidationOnlyIfChanged = 1,
    }
}
