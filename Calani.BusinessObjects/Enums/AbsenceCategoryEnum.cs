﻿using System.ComponentModel;
using Calani.BusinessObjects.Generic;

namespace Calani.BusinessObjects.Enums
{
    public enum AbsenceCategoryEnum
    {
        [Description("AbsenceCategory_Vacation")]
        [Color("#FFF8AF")]
        [Icon("plane")]
        Vacation = 1,

        [Description("AbsenceCategory_Disease")]
        [Color("#ACA7B2")]
        [Icon("thermometer-three-quarters")]
        Disease = 2,

        [Description("AbsenceCategory_Accident")]
        [Color("#FFBEC0")]
        [Icon("car")]
        Accident = 3,

        [Description("AbsenceCategory_InternalTraining")]
        [Color("#BBE1F4")]
        [Icon("book")]
        InternalTraining = 4,

        [Description("AbsenceCategory_ExternalTraining")]
        [Color("#BBE1F4")]
        [Icon("book")]
        ExternalTraining = 5,

        [Description("AbsenceCategory_MovingHome")]
        [Color("#DBE0DE")]
        [Icon("truck")]
        MovingHome = 6,

        [Description("AbsenceCategory_RecoveryOfHours")]
        [Color("#A5F7C5")]
        [Icon("hourglass-2")]
        RecoveryOfHours = 7,

        [Description("AbsenceCategory_Birth")]
        [Color("#FFDCFE")]
        [Icon("child")]
        Birth = 8,

        [Description("AbsenceCategory_Wedding")]
        [Color("#BE256B")]
        [Icon("heart")]
        Wedding = 9,

        [Description("AbsenceCategory_Death")]
        [Color("#c1c1c1")]
        [Icon("square")]
        Death = 10,

        [Description("AbsenceCategory_PublicHoliday")]
        [Color("#FFD16E")]
        [Icon("gift")]
        PublicHoliday = 11,

        [Description("AbsenceCategory_Military")]
        [Color("#B4CDBE")]
        [Icon("fighter-jet")]
        Military = 12,

        [Description("AbsenceCategory_UnpaidLeave")]
        [Color("#5DA8DE")]
        [Icon("ban")]
        UnpaidLeave = 13,
    }
}
