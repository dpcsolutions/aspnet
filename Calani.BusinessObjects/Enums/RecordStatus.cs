﻿namespace Calani.BusinessObjects.Enums
{
    public enum RecordStatus
    {
        Active = 0,
        Deleted = -1,
        Suspended = 1

    }
}