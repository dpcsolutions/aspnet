﻿namespace Calani.BusinessObjects.Enums
{
    public enum TagTypes
    {
        Private = 1,
        Shared = 2
    }
}