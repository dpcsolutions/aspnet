﻿namespace Calani.BusinessObjects.Enums
{
    public enum TimeSheetRecordType : int
    {
        Time = 0,
        Service = 1
    }
}