﻿namespace Calani.BusinessObjects.Enums
{
    public enum ServiceUnitEnum : sbyte
    {
        Unknown = 0,
        Hour = 1
    }
}
