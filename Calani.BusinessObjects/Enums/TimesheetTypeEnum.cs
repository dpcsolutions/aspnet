﻿using System.ComponentModel;

namespace Calani.BusinessObjects.Enums
{
    public enum TimesheetTypeEnum
    {
        [Description("Simple")]
        Simple = 0,

        [Description("DetailedFullWeek")]
        DetailedFullWeek = 1,

        [Description("DetailedWorkDays")]
        DetailedWorkDays = 2,

        [Description("SimplifiedEntry")]
        SimplifiedEntry = 3,
    }
}
