﻿using System.ComponentModel;

namespace Calani.BusinessObjects.Enums
{
    public enum PeriodFilterOptionsEnum : byte
    {
        None = 0,

        [Description("All_periods")]
        AllPeriods = 1,

        [Description("Current_calendar_year")]
        CurrentCalendarYear = 2,

        [Description("Last_calendar_year")]
        PrevCalendarYear = 3,

        [Description("Current_quarter")]
        CurrentQuarter = 4,

        [Description("Last_quarter")]
        PreviousQuarter = 5,

        [Description("Current_month")]
        CurrentMonth = 6,

        [Description("Last_month")]
        PrevMonth = 7
    }
}
