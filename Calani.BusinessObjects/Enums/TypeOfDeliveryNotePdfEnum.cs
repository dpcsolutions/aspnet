﻿using System.ComponentModel;

namespace Calani.BusinessObjects.Enums
{
    public enum TypeOfDeliveryNotePdfEnum : sbyte
    {
        /// <summary>
        /// Unpriced delivery order
        /// </summary>
        [Description("UnpricedDeliveryOrder")]
        Unpriced = 0,

        /// <summary>
        /// Priced delivery order
        /// </summary>
        [Description("PricedDeliveryOrder")]
        Priced = 1,
    }
}
