﻿namespace Calani.BusinessObjects.Enums
{
    public static class ResponseType
    {
        public const string Option = "Option";
        public const string YesNo = "Yes/No";
        public const string Date = "Date";
        public const string Text = "Text";
        public const string AddPhoto = "AddPhoto";
        public const string Checkbox = "Checkbox";
    }
}