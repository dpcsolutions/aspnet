﻿using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Tasks
{
   public class TaskContactMappingManager : Generic.SmartCollectionManager<Model.task_contact_mapping, long>
    {
        public TaskContactMappingManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;

        }


        internal override DbSet<Model.task_contact_mapping> GetDbSet(Model.CalaniEntities db)
        {
            return db.task_contact_mapping;
        }

        internal override SmartQuery<Model.task_contact_mapping> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.task_contact_mapping where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(Model.task_contact_mapping record)
        {
            if (record.organizationId != null) return record.organizationId.Value;
            return -1;
        }

        internal override SmartQuery<Model.task_contact_mapping> GetBaseQuery()
        {
            var q = base.GetBaseQuery();



            // base
            q.Query = (from r in q.Context.task_contact_mapping select r);
            if (OrganizationID > -1) q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.taskId, r.contactId select r);


            return q;
        }

        public List<task_contact_mapping> GetAllContactMappingByTaskId(long TaskId)
        {
            CalaniEntities db = new CalaniEntities();

            var contact_mappings = (from c in db.task_contact_mapping select c).Where(c => c.taskId == TaskId);
            if (OrganizationID > -1) contact_mappings = (from r in contact_mappings where r.organizationId == OrganizationID select r);
            return contact_mappings.ToList();
        }

        public void DeleteByTaskId(long TaskId)
        {
            CalaniEntities db = new CalaniEntities();
            var contact_mappings = (from c in db.task_contact_mapping select c).Where(c => c.taskId == TaskId);
            if (OrganizationID > -1) contact_mappings = (from r in contact_mappings where r.organizationId == OrganizationID select r);
            db.task_contact_mapping.RemoveRange(contact_mappings);
            db.SaveChanges();
        }
    }
}
