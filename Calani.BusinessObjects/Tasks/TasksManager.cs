﻿using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Projects;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Calani.BusinessObjects.Tasks
{
    public class TasksManager : Generic.SmartCollectionManager<Model.tasks, long>
    {
        public TasksManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;

        }


        internal override DbSet<Model.tasks> GetDbSet(Model.CalaniEntities db)
        {
            return db.tasks;
        }

        internal override SmartQuery<Model.tasks> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.tasks where r.id == id select r);
            return q;
        }

        internal override long GetRecordOrganization(Model.tasks record)
        {
            if (record.organizationId != null) return record.organizationId.Value;
            return -1;
        }

        internal override SmartQuery<Model.tasks> GetBaseQuery()
        {
            var q = base.GetBaseQuery();



            // base
            q.Query = (from r in q.Context.tasks select r);
            if (OrganizationID > -1) q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            // default sorting
            if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.title, r.description select r);


            return q;
        }


        public List<TasksExtended> ListTasks(long userId, enumTaskType type = enumTaskType.Todo)
        {
            CalaniEntities db = new CalaniEntities();
            var bq = GetBaseQuery();
            var ct = (from c in db.contacts select c);
            var contact = ct.ToList();

            Dictionary<string, int> enumStatusValue = Enum.GetValues(typeof(enumStatus))
                                         .Cast<enumStatus>()
                                         .ToDictionary(k => k.ToString(), v => (int)v);
            Dictionary<string, int> enumPriorityValue = Enum.GetValues(typeof(enumPriority))
                .Cast<enumPriority>()
                .ToDictionary(k => k.ToString(), v => (int)v);
           
            var xQ = (from r in bq.Query select r);
            var dbrecords = xQ.ToList();

            var query =
            (from task in dbrecords
             join cnt in contact on task.ownerId equals cnt.id
             join st in enumStatusValue on task.status equals st.Value
             join pt in enumPriorityValue on task.prority equals pt.Value
             select new TasksExtended
             {
                 statusName = st.Key,
                 priorityName = pt.Key,
                 id = task.id,
                 title = task.title,
                 description = task.description,
                 ownerName = GetOwnerHTML((string.IsNullOrEmpty(cnt.firstName) ? "" : cnt.firstName[0].ToString()) + "" + (string.IsNullOrEmpty(cnt.lastName) ? "" : cnt.lastName[0].ToString()), task.ownerId),
                 ownerId = task.ownerId,
                 prority = task.prority,
                 status = task.status,
                 assignedTo = task.assignedTo,
                 progressPercentage = task.progress != null ? (int)task.progress * 20 : 0,
                 progress = task.progress,
                 organizationId = task.organizationId,
                 recordStatus = task.recordStatus,
                 dueDate = task.dueDate,
                 timeline = task.timeline,
                 tag = task.tag,
                 assignedToValue = GetAssignToHTML(string.Join(", ", task.task_contact_mapping.Select(e => e.contacts.firstName[0] + "" + e.contacts.lastName[0]).ToList())),
                 assignedToValueList = string.Join(",", task.task_contact_mapping.Select(e => e.contacts.id).ToList()),
                 tagValue = string.Join(",", task.task_tag_mapping.Select(e => e.tags.name).ToList()),
                 tagValueList = string.Join(",", task.task_tag_mapping.Select(e => e.tags.id).ToList()),
                 isPinned = (from tps in db.task_pin_status where tps.OrganizationId == OrganizationID && tps.TaskId == task.id && tps.ContactId == userId select tps).FirstOrDefault()?.IsPinned,
                 isMessage = (from tam in db.task_activity where tam.TaskId == task.id select tam).Count() >0 || (from ta in db.task_attachments where ta.taskId == task.id && ta.organizationId == OrganizationID select ta).Count() > 0
             });

            var s = query.ToList();
            switch (type)
            {
                case enumTaskType.Todo:
                    query = query.Where(e => (e.isPinned == null || !e.isPinned.Value) && (e.status != null && e.status.Value != (int)enumStatus.Archived));
                    break;

                case enumTaskType.Archived:
                    query = query.Where(e => e.status != null && e.status.Value == (int)enumStatus.Archived);
                    break;
                case enumTaskType.Pinned:
                    query = query.Where(e => (e.isPinned != null && e.isPinned.Value));
                    break;
                default:
                    break;
            }

            return query.ToList();
        }

        private string GetAssignToHTML(string assignTo)
        {
            string retHTML = null;
            if (assignTo != "" && assignTo.Contains(","))
            {
                var splitAssignTo = assignTo.Split(',');
                
                foreach (var item in splitAssignTo)
                {
                    retHTML = retHTML + "<label class=\"btn border-slate btn-flat btn-icon btn-rounded mr-5\">"+ item + "</label>";
                }

            }
            else if (assignTo != "")
            {
                retHTML = "<label class=\"btn border-slate btn-flat btn-icon btn-rounded mr-5\">" + assignTo + "</label>";
            }

            return retHTML;
        }

        private string GetOwnerHTML(string ownername,long? ownerid)
        {
            string retHTML = null;
            long userId = Convert.ToInt64(HttpContext.Current.Session["UserId"]);

            if (ownerid== userId)
            {
                retHTML = retHTML + "<label class=\"btn btn-flat btn-icon btn-rounded mr-5 bg-light-green\" ID=\"lblOwner\">" + ownername + "</label>";
            }
           
            else
            {
                retHTML = retHTML + "<label class=\"btn border-slate btn-flat btn-icon btn-rounded\" ID=\"lblOwner\">" + ownername + "</label>";
            }

            return retHTML;
        }

        public bool PinUnPinTask(long TaskId, long userId)
        {
            CalaniEntities db = new CalaniEntities();
            var task_pin = (from tps in db.task_pin_status select tps).Where(e => e.OrganizationId == OrganizationID && e.ContactId == userId && e.TaskId == TaskId).FirstOrDefault();
            if (task_pin != null)
            {
                task_pin.IsPinned = task_pin.IsPinned == null ? true : !task_pin.IsPinned;
            } else
            {
                task_pin = new task_pin_status
                {
                    IsPinned = true,
                    TaskId = TaskId,
                    OrganizationId = OrganizationID,
                    ContactId = userId
                };
            }
            db.task_pin_status.AddOrUpdate(task_pin);
            db.SaveChanges();
            return task_pin.IsPinned.Value;
        }

        public task_pin_status GetTask_Pin_Status(long userId, long TaskId)
        {
            CalaniEntities db = new CalaniEntities();
            return (from tps in db.task_pin_status select tps).Where(e=>e.OrganizationId == OrganizationID && e.ContactId == userId && e.TaskId == TaskId).FirstOrDefault();
        }
    }
}
