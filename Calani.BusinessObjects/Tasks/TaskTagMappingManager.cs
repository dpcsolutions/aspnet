﻿using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calani.BusinessObjects.Tasks
{
    public class TaskTagMappingManager : Generic.SmartCollectionManager<Model.task_tag_mapping, long>
    {
        public TaskTagMappingManager()
        {
            KeyProperty = "id";
           

        }

        internal override DbSet<Model.task_tag_mapping> GetDbSet(Model.CalaniEntities db)
        {
            return db.task_tag_mapping;
        }

        internal override SmartQuery<Model.task_tag_mapping> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.task_tag_mapping where r.id == id select r);
            return q;
        }

        internal override SmartQuery<Model.task_tag_mapping> GetBaseQuery()
        {
            var q = base.GetBaseQuery();



            // base
            q.Query = (from r in q.Context.task_tag_mapping select r);


            return q;




        }

        public List<task_tag_mapping> GetallTagByTaskId(long TaskId)
        {
            CalaniEntities db = new CalaniEntities();

            var Tags = (from tag in db.task_tag_mapping select tag).Where(t => t.taskId == TaskId).ToList();

            return Tags;
        }

        public void DeleteByTaskId(long TaskId)
        {
            CalaniEntities db = new CalaniEntities();
            var Tags = (from tag in db.task_tag_mapping select tag).Where(t => t.taskId == TaskId).ToList();
            db.task_tag_mapping.RemoveRange(Tags);
            db.SaveChanges();
        }

        public void DeleteByTagId(long tagId)
        {
            CalaniEntities db = new CalaniEntities();
            var Tags = (from tag in db.task_tag_mapping select tag).Where(t => t.tagId == tagId).ToList();
            db.task_tag_mapping.RemoveRange(Tags);
            db.SaveChanges();
        }
    }
}
