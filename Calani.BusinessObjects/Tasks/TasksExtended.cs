﻿using Calani.BusinessObjects.Model;
using System.Collections.Generic;

namespace Calani.BusinessObjects.Tasks
{
    public class TasksExtended: Model.tasks
    {
        public string statusName { get; set; }
        public string priorityName { get; set; }
        public string assignedToValue { get; set; }
        public string assignedToValueList { get; set; }
        public string tagValueList { get; set; }
        public string tagValue { get; set; }
        public string ownerName { get; set; }
        public int progressPercentage { get; set; }
        public List<task_contact_mapping> ListTaskContactValue { get; set; }
        public List<task_tag_mapping> ListOfTaskTagMappingValue { get; set; }
        public bool isMessage { get; set; }

        public bool? isPinned { get; set; }
    }
    public enum enumStatus
    {
        Draft=1,
        Started=2,
        Paused=3,
        Done=4,
        Cancelled=5,
        Archived=6

    }
    public enum enumPriority
    {
        Urgent=1,
        Low=2,
        Medium=3,
        High=4
    }

    public enum enumTaskType
    {
        Todo,
        Archived,
        Pinned
    }
}
