﻿using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Scheduler;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace Calani.BusinessObjects.AbsenceRequests
{
    public partial class VacationReportService
    {
        private readonly EmployeesManager _employeesManager;
        private readonly EmployeesManager.EmployeesContractsManager _contractsManager;
        private readonly OrganizationManager _organizationManager;
        private readonly AbsenceRequestsManager _absenceRequestsManager;
        private readonly VacationCarriedOverDaysManager _vacationCarriedOverDaysManager;
        private readonly WorkingTimeCalculator _workingTimeCalculator;

        public VacationReportService(
            EmployeesManager employeesManager,
            EmployeesManager.EmployeesContractsManager contractsManager,
            OrganizationManager organizationManager,
            AbsenceRequestsManager absenceRequestsManager,
            VacationCarriedOverDaysManager vacationCarriedOverDaysManager,
            WorkingTimeCalculator workingTimeCalculator)
        {
            _employeesManager = employeesManager;
            _contractsManager = contractsManager;
            _organizationManager = organizationManager;
            _absenceRequestsManager = absenceRequestsManager;
            _vacationCarriedOverDaysManager = vacationCarriedOverDaysManager;
            _workingTimeCalculator = workingTimeCalculator;

            _organizationManager.Load();
        }

        public VacationReportModel GetAnnualVacationReport(long employeeId)
        {
            var currYear = new DateRange(DatesRangeEnum.CurrentYear);
            var report = GetVacationReport(employeeId, currYear);

            return report;
        }

        public VacationReportModel GetVacationReport(long employeeId, DateRange range,
            List<contacts_contracts> contracts = null,
            List<AbsenceRequestDto> vacations = null,
            List<AbsenceRequestDto> unpaid = null)
        {
            var report = new VacationReportModel();
            List<contacts_contracts> employeeContracts;

            if (contracts == null)
            {
                employeeContracts = _contractsManager.GetActiveContractsForPeriod(range.Start, range.End, employeeId)
                    .OrderBy(c => c.start_date).ToList();
            }
            else
            {
                employeeContracts = contracts.Where(c => c.contact_Id == employeeId).OrderBy(c => c.start_date).ToList();
            }

            var contract = employeeContracts.FirstOrDefault(c => c.contact_Id == employeeId && c.recordStatus == (int)RecordStatus.Active);

            var textReport = new StringBuilder($"Vacation report for employee {employeeId} {Environment.NewLine}");
            textReport.AppendLine($"Period: {range.Start} - {range.End}");
            
            report.EmployeeId = employeeId;
            report.CarriedOverDays = GetCarriedOverDays(employeeId, range);
            report.VacationDaysTotal = GetVacationDaysTotal(range, employeeContracts, report.CarriedOverDays, textReport);
            report.VacationDaysTaken = GetVacationDaysTaken(employeeId, range, vacations);
            report.VacationDaysRemaining = report.VacationDaysTotal - report.VacationDaysTaken;
            report.UnpaidDays = GetUnpaidDaysTaken(employeeId, range, unpaid);
            
            textReport.AppendLine($"Vacation days = {report.VacationDaysTotal}");
            textReport.AppendLine($"Vacation days taken = {report.VacationDaysTaken}");
            textReport.AppendLine($"Vacation days remaining = {report.VacationDaysRemaining}");
            textReport.AppendLine($"Unpaid days = {report.UnpaidDays}");
            textReport.AppendLine($"Carried over days for years {range.Start.Year} - {range.End.Year} = {report.CarriedOverDays}");

            report.TextReport = textReport.ToString();
            report.Summary = $"{report.VacationDaysTaken:0.##}/{report.VacationDaysTotal:0.##}";

            return report;
        }

        #region Private methods

        private double GetCarriedOverDays(long employeeId, DateRange range)
        {
            double carriedOverDays = 0;

            var carriedOverDaysOverYears = _vacationCarriedOverDaysManager.GetVacationCarriedOverDays(employeeId).ToDictionary(m => m.year);
            Func<int, double> getByYear = year => carriedOverDaysOverYears.ContainsKey(year) ? (double)carriedOverDaysOverYears[year].days : 0;

            if (range.Start.Year == range.End.Year)
            {
                carriedOverDays = getByYear(range.Start.Year);
            }
            else
            {
                for(var year = range.Start.Year; year <= range.End.Year; year++)
                {
                    var timeSpan = range.HowMuchTimeFromRangeBelongsToYear(year);
                    carriedOverDays += (timeSpan.Days > 0 ? getByYear(year) : 0);
                }
            }

            return carriedOverDays;
        }

        private double GetVacationDaysTotal(DateRange range, List<contacts_contracts> contracts, double carriedOverDays, StringBuilder textReport)
        {
            if(contracts == null || !contracts.Any())
            {
                return _organizationManager.Vacation;
            }

            double vacationDaysTotal = 0;
            contacts_contracts activeContract = new contacts_contracts();

            var year = new DateRange(range.Start.Year);
            var workingDaysInYear = _workingTimeCalculator.CalculateWorkingDays(year.Start, year.End);

            textReport.AppendLine($"Working days in year = {workingDaysInYear}");

            DateTime? startOfNextContract = null;
            var orderedContracts = contracts.OrderBy(c => c.recordStatus).ThenByDescending(c => c.start_date);
            foreach (var contract in orderedContracts)
            {
                var isActive = contract.recordStatus == (int)RecordStatus.Active;

                if (isActive)
                {
                    activeContract = contract;
                }

                var startDate = contract.start_date < range.Start ? range.Start : contract.start_date.Value.Date;
                var endDate = isActive ? range.End : startOfNextContract.HasValue ? startOfNextContract.Value.Date : contract.updated_at.Value.Date;

                if (startDate <= endDate)
                {
                    var workingDays = RoundDays(_workingTimeCalculator.CalculateWorkingDays(startDate, endDate));
                    var vacationDays = RoundDays(contract.vacation_year * workingDays / workingDaysInYear);
                    vacationDaysTotal += vacationDays;

                    startOfNextContract = startDate;

                    textReport.AppendLine($"Contract #{contract.id} active:{isActive}: {startDate} - {endDate}");
                    textReport.AppendLine($"    yearly vacation days = {contract.vacation_year}");
                    textReport.AppendLine($"    actual working days = {workingDays}");
                    textReport.AppendLine($"    vacation days = {contract.vacation_year} * {workingDays} / {workingDaysInYear} = {vacationDays}");
                }
                else
                {
                    textReport.AppendLine($"Contract #{contract.id} was skipped because of conflicting dates.");
                }

            }

            return vacationDaysTotal + carriedOverDays;
        }

        private double RoundDays(double value)
        {
            var integralPart = Math.Truncate(value);
            var fractionalPart = value - integralPart;

            if (fractionalPart < 0.25) fractionalPart = 0;
            else if (fractionalPart < 0.75) fractionalPart = 0.5;
            else fractionalPart = 1;

            return integralPart + fractionalPart;
        }

        private double GetVacationDaysTaken(long employeeId, DateRange range, List<AbsenceRequestDto> vacations = null)  
        {
            if (vacations == null)
            {
                vacations = _absenceRequestsManager.ListApprovedVacations(dtFrom: range.Start, dtTo: range.End, employeeId: employeeId);
            }
            var db = new CalaniEntities();

            var holidayList = db.calendars_rec
                .Where(x => x.name == "Férié" && x.calendars.organization_Id == _organizationManager._id && x.recordStatus != -1)
                .Include(x => x.calendars)
                .ToList();
            
            var days = vacations.Where(v => v.EmployeeId == employeeId)
                                .Sum(v => _workingTimeCalculator.CalculateAbsenceDays(v.StartDate, v.EndDate, holidayList));

            return days;
        }

        private double GetUnpaidDaysTaken(long employeeId, DateRange range, List<AbsenceRequestDto> unpaid = null)
        {
            if (unpaid == null)
            {
                unpaid = _absenceRequestsManager.ListApprovedUnpaidLeaves(dtFrom: range.Start, dtTo: range.End, employeeId: employeeId);
            }

            var days = unpaid.Where(v => v.EmployeeId == employeeId)
                             .Sum(v => _workingTimeCalculator.CalculateAbsenceDays(v.StartDate, v.EndDate));

            return days;
        }

        #endregion Private methods
    }
}
