﻿using Calani.BusinessObjects.AbsenceRequests.Models;
using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Scheduler;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Calani.BusinessObjects.AbsenceRequests
{
    public class AbsenceRequestsService
    {
        private readonly AbsenceRequestsManager _absenceRequestsManager;
        private readonly EmployeesManager _employeesManager;
        private readonly EmployeesManager.EmployeesContractsManager _contractsManager;
        private readonly VacationReportService _vacationReportService;
        private readonly WorkingTimeCalculator _workingTimeCalculator;

        public AbsenceRequestsService(AbsenceRequestsManager absenceRequestsManager,
                                            EmployeesManager employeesManager,
                                            EmployeesManager.EmployeesContractsManager contractsManager,
                                            VacationReportService vacationReportService,
                                            WorkingTimeCalculator workingTimeCalculator)
        {
            _absenceRequestsManager = absenceRequestsManager;
            _employeesManager = employeesManager;
            _contractsManager = contractsManager;
            _vacationReportService = vacationReportService;
            _workingTimeCalculator = workingTimeCalculator;
        }

        public List<AbsenceReportModel> GetAbsenceReports(DateTime start, DateTime end, List<long> employeesIds = null)
        {
            var reports = new List<AbsenceReportModel>();

            var range = new DateRange(start, end);
            var employees = _employeesManager.ListEmployeesLightweight(employeesIds).OrderBy(e => $"{e.firstName} {e.lastName}").ToList();
            var contracts = _contractsManager.GetActiveContractsForPeriod(range.Start, range.End);

            Func<List<absence_requests>, AbsenceCategoryEnum, List<AbsenceRequestDto>> filterByCategory = (entities, category) =>
                                            entities.Where(e => (AbsenceCategoryEnum)e.absenceCategory == category)
                                            .Select(e => new AbsenceRequestDto(e)).ToList();
            
            var db = new CalaniEntities();
            var orgId = employees.FirstOrDefault()?.organizationId ?? 0;
            var holidayList = db.calendars_rec
                .Where(x => x.name == "Férié" && x.calendars.organization_Id == orgId && x.recordStatus != -1)
                .Include(x => x.calendars).ToList();
                
            Func<List<AbsenceRequestDto>, long, double> sumDaysForEmployee = (absences, employeeId) =>
                absences.Where(a => a.EmployeeId == employeeId)
                .Sum(a => _workingTimeCalculator.CalculateAbsenceDays(a.StartDate, a.EndDate, holidayList));

            var approvedAbsences = _absenceRequestsManager.ListAbsenceRequests(approved: true, dtFrom: start, dtTo: end);
            var vacations = filterByCategory(approvedAbsences, AbsenceCategoryEnum.Vacation);
            var unpaid = filterByCategory(approvedAbsences, AbsenceCategoryEnum.UnpaidLeave);
            var disease = filterByCategory(approvedAbsences, AbsenceCategoryEnum.Disease);
            var accident = filterByCategory(approvedAbsences, AbsenceCategoryEnum.Accident);
            var internalTraining = filterByCategory(approvedAbsences, AbsenceCategoryEnum.InternalTraining);
            var externalTraining = filterByCategory(approvedAbsences, AbsenceCategoryEnum.ExternalTraining);
            var movingHome = filterByCategory(approvedAbsences, AbsenceCategoryEnum.MovingHome);
            var birth = filterByCategory(approvedAbsences, AbsenceCategoryEnum.Birth);
            var wedding = filterByCategory(approvedAbsences, AbsenceCategoryEnum.Wedding);
            var death = filterByCategory(approvedAbsences, AbsenceCategoryEnum.Death);
            var publicHolidays = filterByCategory(approvedAbsences, AbsenceCategoryEnum.PublicHoliday);
            var military = filterByCategory(approvedAbsences, AbsenceCategoryEnum.Military);
            var recoveryOfHours = filterByCategory(approvedAbsences, AbsenceCategoryEnum.RecoveryOfHours);
            
            foreach (var employee in employees)
            {
                var report = new AbsenceReportModel()
                {
                    EmployeeId = employee.id,
                    EmployeeName = $"{employee.firstName} {employee.lastName}",
                    AccidentDays = sumDaysForEmployee(accident, employee.id),
                    DiseaseDays = sumDaysForEmployee(disease, employee.id),
                    InternalTrainingDays = sumDaysForEmployee(internalTraining, employee.id),
                    ExternalTrainingDays = sumDaysForEmployee(externalTraining, employee.id),
                    MovingHomeDays = sumDaysForEmployee(movingHome, employee.id),
                    BirthDays = sumDaysForEmployee(birth, employee.id),
                    WeddingDays = sumDaysForEmployee(wedding, employee.id),
                    DeathDays = sumDaysForEmployee(death, employee.id),
                    PublicHolidayDays = sumDaysForEmployee(publicHolidays, employee.id),
                    MilitaryDays = sumDaysForEmployee(military, employee.id),
                    RecoveryOfHoursDays = sumDaysForEmployee(recoveryOfHours, employee.id),
                };

                var militaryDaysPerYear = employee.militaryDays;
                report.MilitarySummary = $"{report.MilitaryDays}/{militaryDaysPerYear:0.##}";
                report.MilitaryRemainingDays = militaryDaysPerYear - report.MilitaryDays;

                report.VacationReport = _vacationReportService.GetVacationReport(employee.id, range, contracts, vacations, unpaid);

                reports.Add(report);
            }

            return reports;
        }
    }
}
