﻿using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;

namespace Calani.BusinessObjects.AbsenceRequests
{
    public class VacationCarriedOverDaysManager : Generic.SmartCollectionManager<Model.vacation_carried_over_days, long>
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public VacationCarriedOverDaysManager(long organizationId)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }

        internal override DbSet<vacation_carried_over_days> GetDbSet(Model.CalaniEntities db)
        {
            return db.vacation_carried_over_days;
        }

        internal override SmartQuery<vacation_carried_over_days> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.vacation_carried_over_days where r.id == id select r);
            return q;
        }

        public List<vacation_carried_over_days> GetVacationCarriedOverDays(long employeeId)
        {
            return GetBaseQuery().Query.Where(r => r.employee_id == employeeId).ToList();
        }

        public vacation_carried_over_days GetVacationCarriedOverDays(long employeeId, int year)
        {
            return GetBaseQuery().Query.Where(r => r.employee_id == employeeId && r.year == year).SingleOrDefault();
        }

        internal override SmartQuery<vacation_carried_over_days> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            q.Query = (from r in q.Context.vacation_carried_over_days select r);

            if (OrganizationID > -1)
                q.Query = (from r in q.Query where r.organization_id == OrganizationID select r);

            return q;
        }

        public SmartAction<vacation_carried_over_days> SaveOrUpdate(long employeeId, int year, decimal days)
        {
            try
            {
                if(year < 2000)
                {
                    throw new ArgumentException(nameof(year));
                }

                if (employeeId <= 0)
                {
                    throw new ArgumentException(nameof(employeeId));
                }

                var existingRecord = GetBaseQuery().Query.Where(r => r.employee_id == employeeId && r.year == year).SingleOrDefault();

                if(existingRecord != null && existingRecord.days != days)
                {
                    existingRecord.days = days;
                    return Update(existingRecord.id, existingRecord);
                }

                if(existingRecord == null)
                {
                    return Add(new vacation_carried_over_days() 
                    {
                        organization_id = this.OrganizationID,
                        employee_id = employeeId,
                        days = days,
                        year = year
                    });
                }

                return null;
            }
            catch (Exception e)
            {
                _log.Error($"SaveOrUpdate. error. ", e);
                throw;
            }
        }
    }
}
