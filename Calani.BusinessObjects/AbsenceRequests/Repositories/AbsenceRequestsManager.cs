﻿using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.Contacts;
using Calani.BusinessObjects.CustomerAdmin;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Membership;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Notifications;
using Calani.BusinessObjects.Projects;
using Calani.BusinessObjects.Scheduler;
using Calani.BusinessObjects.WorkSchedulers;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Reflection;

namespace Calani.BusinessObjects.AbsenceRequests
{
    public class AbsenceRequestsManager : Generic.SmartCollectionManager<Model.absence_requests, long>
    {
        private class Filters
        {
            public long? EmployeeId { get; set; }
            public IList<long> EmployeesIds { get; set; }
            public AbsenceCategoryEnum? AbsenceCategory { get; set; }
            public DateTime? FromDate { get; set; }
            public DateTime? ToDate { get; set; }
            public bool? Approved { get; set; }
            public bool? Rejected { get; set; }
            public bool? CancellationApproved { get; set; }
            public bool? CancellationRejected { get; set; }
        }

        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly WorkingTimeCalculator WorkingTimeCalculator;
        private readonly EmployeesManager.EmployeesContractsManager ContractsManager;
        private readonly EmployeesManager EmployeesManager;
        private readonly NotificationManager NotificationManager;
        private readonly MailSender MailSender;

        public AbsenceRequestsManager(long organizationId, 
            EmployeesManager.EmployeesContractsManager contractsManager = null,
            EmployeesManager employeesManager = null)
        {
            KeyProperty = "id";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;

            var orgManager = new OrganizationManager(organizationId);
            var workScheduleRecManager = new WorkScheduleRecManager(organizationId);
            var workScheduleService = new WorkScheduleService(orgManager, workScheduleRecManager);
            var workDay = workScheduleService.GetWorkDaySettings();
            WorkingTimeCalculator = new WorkingTimeCalculator(workDay);
            ContractsManager = contractsManager != null ? contractsManager : new EmployeesManager.EmployeesContractsManager(organizationId);
            EmployeesManager = employeesManager != null ? employeesManager : new EmployeesManager(organizationId);
            MailSender = new MailSender();
            NotificationManager = new NotificationManager();
        }

        internal override DbSet<absence_requests> GetDbSet(Model.CalaniEntities db)
        {
            return db.absence_requests;
        }

        internal override SmartQuery<absence_requests> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.absence_requests where r.id == id select r);
            return q;
        }

        public List<absence_requests> GetAbsenceRequests(long organizationId)
        {
            return GetBaseQuery().Query.Include("jobs").Where(r => r.organizationId == organizationId).ToList();
        }

        internal override SmartQuery<absence_requests> GetBaseQuery()
        {
            var q = base.GetBaseQuery();

            q.Query = (from r in q.Context.absence_requests select r);

            if (!IgnoreRecordStatus)
                q.Query = (from r in q.Query where r.recordStatus == 0 select r);

            if (OrganizationID > -1)
                q.Query = (from r in q.Query where r.organizationId == OrganizationID select r);

            return q;
        }

        public SmartAction<absence_requests> SaveOrUpdate(long id, absence_requests request)
        {
            try
            {
                var absenceDays = WorkingTimeCalculator.CalculateAbsenceDays(request.startDate, request.endDate);

                if(request.approvedAt != null 
                    && (AbsenceCategoryEnum)request.absenceCategory == AbsenceCategoryEnum.Vacation
                    && request.cancellationRequestedAt == null
                    && request.cancellationRejectedBy == null)
                {
                    var res = ContractsManager.RegisterApprovedAbsenceRequest(request.employeeId, (decimal)absenceDays);
                    if(res.Success == false)
                    {
                        _log.Error(res.ErrorMessage);
                    }
                }

                if(request.approvedAt == null 
                    && (AbsenceCategoryEnum)request.absenceCategory == AbsenceCategoryEnum.Vacation
                    && request.cancellationApprovedAt != null)
                {
                    var res = ContractsManager.RegisterCancelledAbsenceRequest(request.employeeId, (decimal)absenceDays);
                    if (res.Success == false)
                    {
                        _log.Error(res.ErrorMessage);
                    }
                }

                var result = (id > 0) ? Update(id, request) : Add(request);

                var cancellationStatus = request.GetCancellationStatus();
                if (result.Success && (request.approvedAt.HasValue || request.rejectedAt.HasValue) && cancellationStatus == CancellationStatusEnum.None)
                {
                    SendStatusChangedNotification(request);
                }
                else if (result.Success && cancellationStatus == CancellationStatusEnum.CancellationRequested)
                {
                    SendCancellationRequestedNotification(request);
                }
                else if (result.Success && id <= 0)
                {
                    SendNewAbsenceRequestNotification(result.Record);
                }

                return result;
            }
            catch (Exception e)
            {
                _log.Error($"SaveOrUpdate. error. ", e);
                throw;
            }
        }

        private IQueryable<absence_requests> QueryAbsenceRequests(Filters filters)
        {
            var q = GetBaseQuery();

            // Include employee
            q.Query.Include(e => e.contacts4);

            if (filters.Approved != null)
            {
                if (filters.Approved.Value == true)
                {
                    q.Query = (from r in q.Query where r.approvedAt != null select r);
                }
                else
                {
                    q.Query = (from r in q.Query where r.approvedAt == null select r);
                }
            }

            if (filters.Rejected != null)
            {
                if (filters.Rejected.Value == true)
                {
                    q.Query = (from r in q.Query where r.rejectedAt != null select r);
                }
                else
                {
                    q.Query = (from r in q.Query where r.rejectedAt == null select r);
                }
            }

            if (filters.CancellationApproved != null)
            {
                if (filters.CancellationApproved.Value == true)
                {
                    q.Query = (from r in q.Query where r.cancellationApprovedAt != null select r);
                }
                else
                {
                    q.Query = (from r in q.Query where r.cancellationApprovedAt == null select r);
                }
            }

            if (filters.CancellationRejected != null)
            {
                if (filters.CancellationRejected.Value == true)
                {
                    q.Query = (from r in q.Query where r.cancellationRejectedAt != null select r);
                }
                else
                {
                    q.Query = (from r in q.Query where r.cancellationRejectedAt == null select r);
                }
            }

            if (filters.EmployeeId != null)
            {
                q.Query = (from r in q.Query where r.employeeId == filters.EmployeeId select r);
            }

            if(filters.EmployeesIds != null && filters.EmployeesIds.Any())
            {
                q.Query = (from r in q.Query where filters.EmployeesIds.Contains(r.employeeId) select r);
            }

            if (filters.FromDate != null)
            {
                q.Query = (from r in q.Query where r.startDate >= filters.FromDate || r.endDate >= filters.FromDate select r);
            }

            if (filters.ToDate != null)
            {
                q.Query = (from r in q.Query where r.startDate <= filters.ToDate || r.endDate <= filters.ToDate select r);
            }

            if (filters.AbsenceCategory.HasValue)
            {
                q.Query = (from r in q.Query where r.absenceCategory == (int)filters.AbsenceCategory.Value select r);
            }

            return q.Query;
        }

        public List<absence_requests> ListAbsenceRequests(bool? approved = null, bool? rejected = null, long? employee = null, 
            DateTime? dtFrom = null, DateTime? dtTo = null)
        {
            var filters = new Filters()
            {
                Approved = approved,
                Rejected = rejected,
                EmployeeId = employee,
                FromDate = dtFrom,
                ToDate = dtTo
            };

            var data = QueryAbsenceRequests(filters).ToList();
            return data;
        }

        public List<absence_requests> ListApprovedOrCancelledRequests(
            DateTime? dtFrom = null, DateTime? dtTo = null, IList<long> employeesIds = null)
        {
            var q = GetBaseQuery();

            // Include employee
            q.Query.Include(e => e.contacts4);

            q.Query = (from r in q.Query where r.approvedAt != null || r.cancellationApprovedAt != null select r);

            if (employeesIds != null && employeesIds.Any())
            {
                q.Query = (from r in q.Query where employeesIds.Contains(r.employeeId) select r);
            }

            if (dtFrom != null)
            {
                q.Query = (from r in q.Query where r.startDate >= dtFrom || r.endDate >= dtFrom select r);
            }

            if (dtTo != null)
            {
                q.Query = (from r in q.Query where r.startDate <= dtTo || r.endDate <= dtTo select r);
            }


            return q.Query.OrderBy(r => r.startDate).ToList();
        }

        private int CountAbsenceRequests(Filters filters)
        {
            var count = QueryAbsenceRequests(filters).Count();
            return count;
        }

        public int CountAbsenceRequestsForDashboard(long employeeId, ValidationEnum status)
        {
            int result = 0;
            Filters filters;

            var user = EmployeesManager.Get(employeeId);
            var userType = (ContactTypeEnum)(user.type ?? 0);
            var isAdmin = userType == ContactTypeEnum.User_Admin;
            
            switch (status)
            {
                case ValidationEnum.Approved:
                    filters = isAdmin ? 
                        new Filters() { Approved = true } :
                        new Filters() { Approved = true, EmployeeId = employeeId };
                    result = CountAbsenceRequests(filters);
                    break;

                case ValidationEnum.Rejected:
                    filters = isAdmin ?
                        new Filters() { Rejected = true } :
                        new Filters() { Rejected = true, EmployeeId = employeeId };
                    result = CountAbsenceRequests(filters);
                    break;

                case ValidationEnum.Pending:
                default:
                    filters = isAdmin ?
                        new Filters() { Approved = false } :
                        new Filters() { Approved = false, EmployeeId = employeeId };
                    result = CountAbsenceRequests(filters);
                    break;
            }

            return result;
        }

        public List<visitsExtended> ListAbsenceRequestsForDiary(DateTime? dtFrom, DateTime? dtTo, List<calendarResource> resources = null, string cultureCode = "fr-CH")
        {
            if(resources == null || !resources.Any())
            {
                return new List<visitsExtended>();
            }

            var culture = new CultureInfo(cultureCode);

            IList<long> ids = resources.Select(r => long.Parse(r.id)).ToList();

            DateTime? startDate = dtFrom.HasValue ? dtFrom.Value.Kind != DateTimeKind.Utc ? dtFrom.Value.ToUniversalTime() : dtFrom : null;
            DateTime? endDate = dtTo.HasValue ? dtTo.Value.Kind != DateTimeKind.Utc ? dtTo.Value.ToUniversalTime() : dtTo : null;

            var entities = ListApprovedOrCancelledRequests(dtFrom: startDate, dtTo: endDate, employeesIds: ids);

            var result = entities.Select(ar => {
                var dateTimeFormat = "yyyy-MM-ddTHH:mm:ss+00:00";
                var cancellationStatus = ar.GetCancellationStatus();
                var isCanceled = cancellationStatus == CancellationStatusEnum.Cancelled;
                var bgColor = isCanceled ? "#FF0000" : ((AbsenceCategoryEnum)ar.absenceCategory).GetColor();
                var model = new visitsExtended()
                {
                    id = $"absence_{ar.id}",
                    backgroundColor = bgColor,
                    borderColor = bgColor,
                    textColor = "#000",
                    description = SharedResource.Resource.GetString(((AbsenceCategoryEnum)ar.absenceCategory).GetDescription(), culture) +
                                     (isCanceled ? $" ({SharedResource.Resource.GetString("Canceled", culture)})" : ""),
                    icon = ((AbsenceCategoryEnum)ar.absenceCategory).GetIcon(),
                    start = ar.startDate.ToString(dateTimeFormat, CultureInfo.InvariantCulture),
                    end = ar.endDate.ToString(dateTimeFormat, CultureInfo.InvariantCulture),
                    resources = new List<long> { ar.employeeId },
                    resourcesObj = new List<visitsExtended.resource>
                    {
                        new visitsExtended.resource(ar.contacts4.id, 0, $"{ar.contacts4.firstName} {ar.contacts4.lastName}", ar.contacts4.initials, -1)
                    },
                    visitType = (int)VisitTypeEnum.AbsenceRequest,
                    contactName = $"{ar.contacts4.firstName} {ar.contacts4.lastName}",
                    allDay = WorkingTimeCalculator.CalculateAbsenceDays(ar.startDate, ar.endDate) % 1 == 0,
                };

                if (model.allDay)
                {
                    // For all-day events Diary calendar treats end date as exclusive
                    model.end = ar.endDate.AddDays(1).Date.ToString(dateTimeFormat, CultureInfo.InvariantCulture);
                }

                return model;
            })
            .ToList();

            return result;
        }

        public List<AbsenceRequestDto> ListApprovedVacations(DateTime dtFrom, DateTime dtTo, long? employeeId = null)
        {
            var filters = new Filters()
            {
                Approved = true,
                CancellationApproved = false,
                EmployeeId = employeeId,
                FromDate = dtFrom,
                ToDate = dtTo,
                AbsenceCategory = AbsenceCategoryEnum.Vacation
            };

            var data = QueryAbsenceRequests(filters).ToList()
                        .Select(e => new AbsenceRequestDto(e)).ToList();

            return data;
        }

        public List<AbsenceRequestDto> ListApprovedUnpaidLeaves(DateTime dtFrom, DateTime dtTo, long? employeeId = null)
        {
            var filters = new Filters()
            {
                Approved = true,
                CancellationApproved = false,
                EmployeeId = employeeId,
                FromDate = dtFrom,
                ToDate = dtTo,
                AbsenceCategory = AbsenceCategoryEnum.UnpaidLeave
            };

            var data = QueryAbsenceRequests(filters).ToList()
                        .Select(e => new AbsenceRequestDto(e)).ToList();

            return data;
        }

        public List<AbsenceRequestDto> ListApprovedTimeRecoveryLeaves(DateTime? dtFrom, DateTime? dtTo, long? employeeId = null)
        {
            var filters = new Filters()
            {
                Approved = true,
                CancellationApproved = false,
                EmployeeId = employeeId,
                FromDate = dtFrom,
                ToDate = dtTo,
                AbsenceCategory = AbsenceCategoryEnum.RecoveryOfHours
            };

            var data = QueryAbsenceRequests(filters).ToList()
                        .Select(e => new AbsenceRequestDto(e)).ToList();

            return data;
        }

        #region Private Methods

        private void SendStatusChangedNotification(absence_requests entity)
        {
            var cultureStr = "fr-FR";
            var culture = new CultureInfo(cultureStr);
            var durationDays = WorkingTimeCalculator.CalculateAbsenceDays(entity.startDate, entity.endDate);

            if (entity.employeeId > 0)
            {
                try
                {
                    cultureStr = UserCultureManager.GetUserCulture(entity.employeeId) ?? cultureStr;
                    culture = new CultureInfo(cultureStr);
                }
                catch (Exception e)
                {
                    _log.Error("SendNotification, getUserCulture error.", e);
                }
            }

            var msg = String.Empty;
            var title = String.Empty;
            var dateFormat = "dd MMMM yyyy";

            var absenceCategory = GetResource(((AbsenceCategoryEnum)entity.absenceCategory).GetDescription(), culture);

            var model = new AbsenceRequestDto(entity);
            switch (model.ValidationStatus)
            {
                case ValidationEnum.Approved:
                    title = GetResource("AbsenceRequestApprovedEmailSubject", culture);

                    var dateApproved = entity.approvedAt.Value.ToString(dateFormat);
                    var approvedBy = EmployeesManager.Get(entity.approvedBy.Value) ?? new contacts();

                    msg = String.Format(GetResource("AbsenceRequestApprovedEmailText", culture), absenceCategory.ToLower(), dateApproved, $"{approvedBy.firstName.Trim()} {approvedBy.lastName.Trim()}");

                    break;

                case ValidationEnum.Rejected:
                    title = GetResource("AbsenceRequestRejectedEmailSubject", culture);

                    var dateRejected = entity.rejectedAt.Value.ToString(dateFormat);
                    var rejectedBy = EmployeesManager.Get(entity.rejectedBy.Value) ?? new contacts();

                    msg = String.Format(GetResource("AbsenceRequestRejectedEmailText", culture), absenceCategory.ToLower(), dateRejected, $"{rejectedBy.firstName.Trim()} {rejectedBy.lastName.Trim()}");

                    break;

                case ValidationEnum.Pending:
                default:
                    break;
            }

            if (!String.IsNullOrWhiteSpace(title))
            {
                var linkTemplate = GetResource("ConsultLinkHtmlTemplate", culture);
                var link = String.Format(linkTemplate, GetAbsenceRequestUrl(entity.id));
                msg += " " + link;

                _log.Info($"Sending notification AbsenceRequest, title:'{title}' to:{entity.employeeId}, action :{model.ValidationStatus}, duration:{durationDays:0.##}, updated by:{entity.updatedBy}, updated at: {entity.approvedAt ?? entity.rejectedAt}");

                SendNotification(msg, title, entity.employeeId, entity);
            }
        }
        
        private void SendNewAbsenceRequestNotification(absence_requests entity)
        {
            var cultureStr = "fr-FR";
            var culture = new CultureInfo(cultureStr);
            var href = GetAbsenceRequestUrl(entity.id);
            var admins = EmployeesManager.ListAdmins();
            var currentUser = EmployeesManager.Get(entity.createdBy);

            foreach (var admin in admins)
            {
                if(entity.createdBy == admin.id)
                {
                    continue;
                }

                try
                {
                    cultureStr = UserCultureManager.GetUserCulture(admin.id) ?? cultureStr;
                    culture = new CultureInfo(cultureStr);
                }
                catch (Exception e)
                {
                    _log.Error("SendNotification, getUserCulture error.", e);
                }

                var title = GetResource("NewAbsenceRequestMessageSubject", culture);
                var msg = string.Format(GetResource("NewAbsenceRequestMessageBody", culture), $"{currentUser.lastName} {currentUser.firstName}" ,href);

                if (!String.IsNullOrWhiteSpace(title))
                {
                    _log.Info($"Sending notification AbsenceRequest, title:'{title}' to:{admin.id}, created by:{entity.createdBy}");

                    SendNotification(msg, title, admin.id, entity);
                }
            }
        }

        private void SendCancellationRequestedNotification(absence_requests entity)
        {
            var cultureStr = "fr-FR";
            var culture = new CultureInfo(cultureStr);
            var href = GetAbsenceRequestUrl(entity.id);
            var admins = EmployeesManager.ListAdmins();

            foreach (var admin in admins)
            {
                try
                {
                    cultureStr = UserCultureManager.GetUserCulture(admin.id) ?? cultureStr;
                    culture = new CultureInfo(cultureStr);
                }
                catch (Exception e)
                {
                    _log.Error("SendNotification, getUserCulture error.", e);
                }

                var title = GetResource("AbsenceRequestCancellationRequestedEmailSubject", culture);
                var msg = string.Format(GetResource("AbsenceRequestCancellationRequestedEmailText", culture), href);

                if (!String.IsNullOrWhiteSpace(title))
                {
                    _log.Info($"Sending notification AbsenceRequest, title:'{title}' to:{admin.id}, created by:{entity.createdBy}");

                    SendNotification(msg, title, admin.id, entity);
                }
            }
        }

        private void SendNotification(string message, string title, long employeeId, absence_requests entity)
        {
            var category = "AbsenceRequests";

            NotificationManager.sendNotifications(message, title, new List<long> { employeeId }, new Dictionary<string, string>() { { "absenceRequestId", entity.id.ToString() } }, category);

            if (!MailSender.Variables.ContainsKey("PublicUrl")){
                MailSender.Variables.Add("PublicUrl", System.Configuration.ConfigurationManager.AppSettings["PublicUrl"]);
            }
            
            var user = EmployeesManager.Get(employeeId);
            MailSender.MailTo.Email = user.primaryEmail ?? user.secondaryEmail;
            MailSender.MailTo.UserId = employeeId;
            MailSender.SendSmartHtmlEmail(title, message, category);
        }

        private string GetAbsenceRequestUrl(long id)
        {
            var pUrl = System.Configuration.ConfigurationManager.AppSettings["PublicUrl"];
            var href = $"{pUrl}Time/AbsenceRequests/AbsenceRequest.aspx?id={id}";
            return href;
        }

        private static string GetResource(string n, CultureInfo cultureInfo)
        {
            if (SharedResource.Resource == null) return n;
            return SharedResource.Resource.GetString(n, cultureInfo);
        }

        #endregion Private Methods
    }
}
