﻿using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Model;

namespace Calani.BusinessObjects.AbsenceRequests
{
    public static class AbsenceRequestEntityExtension
    {
        public static CancellationStatusEnum GetCancellationStatus(this absence_requests entity)
        {
            CancellationStatusEnum status = entity.cancellationApprovedAt.HasValue ? CancellationStatusEnum.Cancelled
                                    : entity.cancellationRejectedAt.HasValue ? CancellationStatusEnum.CancellationRejected
                                    : entity.cancellationRequestedAt.HasValue ? CancellationStatusEnum.CancellationRequested
                                    : CancellationStatusEnum.None;

            return status;
        }
    }
}
