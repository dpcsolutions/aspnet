﻿using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Scheduler;
using System;
using System.Collections.Generic;

namespace Calani.BusinessObjects.AbsenceRequests
{
    public class AbsenceRequestGridModel
    {
        public long Id { get; set; }
        public long EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string AbsenceCategory { get; set; }
        /// <summary>
        /// Start date
        /// </summary>
        public DateTime date { get; set; }
        public DateTime EndDate { get; set; }
        public string Comment { get; set; }
        public bool Approved { get; set; }
        public bool Rejected { get; set; }
        public bool Canceled { get; set; }

        public int status 
        { 
            get
            {
                if (CancellationStatus == CancellationStatusEnum.CancellationRequested) return 0;
                if (Approved && !Canceled) return 1;
                if (Rejected) return 2;
                if (Canceled) return 3;
                return 0;
            }
        }

        public ValidationEnum ValidationStatus
        {
            get
            {
                if(Approved) return ValidationEnum.Approved;
                if (Rejected) return ValidationEnum.Rejected;
                return ValidationEnum.Pending;
            }
        }

        public CancellationStatusEnum CancellationStatus { get; set; }
        public string CancellationStatusTextLocalized { get; set; }

        public double DurationDays { get; set; }

        public AbsenceRequestGridModel() { }

        public AbsenceRequestGridModel(absence_requests entity, WorkingTimeCalculator workingTimeCalculator, List<calendars_rec> holidayList = null)
        {
            Id = entity.id;
            EmployeeId = entity.contacts4 != null ? entity.contacts4.id : 0;
            EmployeeName = entity.contacts4 != null ? $"{entity.contacts4.firstName} {entity.contacts4.lastName}" : string.Empty;
            AbsenceCategory = ((AbsenceCategoryEnum)entity.absenceCategory).GetDescription();
            date = entity.startDate;
            EndDate = entity.endDate;
            Comment = entity.comment;
            Approved = entity.approvedAt != null;
            Rejected = entity.rejectedAt != null;
            Canceled = entity.cancellationApprovedAt != null;
            CancellationStatus = entity.GetCancellationStatus();
            CancellationStatusTextLocalized = CancellationStatus.GetDescription();
            DurationDays = (double)workingTimeCalculator.CalculateAbsenceDays(entity.startDate, entity.endDate, holidayList);
        }
    }
}
