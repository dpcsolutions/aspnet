﻿namespace Calani.BusinessObjects.AbsenceRequests.Models
{
    public class AbsenceReportModel
    {
        public long EmployeeId { get; set; }

        public string EmployeeName { get; set; }

        public double DiseaseDays { get; set; }

        public double AccidentDays { get; set; }

        public double InternalTrainingDays { get; set; }

        public double ExternalTrainingDays { get; set; }

        public double MovingHomeDays { get; set; }

        public double BirthDays { get; set; }

        public double WeddingDays { get; set; }

        public double DeathDays { get; set; }

        public double RecoveryOfHoursDays { get; set; }

        public double PublicHolidayDays { get; set; }

        public double MilitaryDays { get; set; }

        public double MilitaryRemainingDays { get; set; }

        public string MilitarySummary { get; set; }

        public VacationReportModel VacationReport { get; set; }
    }
}
