﻿using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Model;
using System;

namespace Calani.BusinessObjects.AbsenceRequests
{
    public class AbsenceRequestDto
    {
        public long Id { get; set; }

        public AbsenceCategoryEnum AbsenceCategory { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public CancellationStatusEnum CancellationStatus { get; set; }

        public ValidationEnum ValidationStatus { get; set; }

        public long EmployeeId { get; set; }

        public AbsenceRequestDto()
        {
        }

        public AbsenceRequestDto(absence_requests entity)
        {
            Id = entity.id;
            AbsenceCategory = (AbsenceCategoryEnum)entity.absenceCategory;
            StartDate = entity.startDate;
            EndDate = entity.endDate;
            EmployeeId = entity.employeeId;

            ValidationStatus = entity.approvedAt != null ? ValidationEnum.Approved : 
                                entity.rejectedAt != null ? ValidationEnum.Rejected : 
                                ValidationEnum.Pending;

            CancellationStatus = entity.cancellationApprovedAt != null ? CancellationStatusEnum.Cancelled :
                                    entity.cancellationRejectedAt != null ? CancellationStatusEnum.CancellationRejected :
                                    CancellationStatusEnum.CancellationRequested;
        }
    }
}
