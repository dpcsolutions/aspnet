﻿using System;

namespace Calani.BusinessObjects.AbsenceRequests
{
    public class VacationReportModel
    {
        public long EmployeeId { get; set; }

        public string EmployeeName { get; set; }

        public double VacationDaysTotal { get; set; }

        public double VacationDaysTaken { get; set; }

        public double VacationDaysRemaining { get; set; }

        public double CarriedOverDays { get; set; }

        public double UnpaidDays { get; set; }

        public int VacationTakenPercent { 
            get 
            {
                return VacationDaysTotal != 0 ? (int)Math.Round(VacationDaysTaken * 100 / VacationDaysTotal) : 0;
            } 
        }

        public string TextReport { get; set; }

        public string Summary { get; set; }
    }
}
