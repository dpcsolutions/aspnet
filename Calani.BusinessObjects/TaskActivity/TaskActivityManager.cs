﻿using Calani.BusinessObjects.AjaxService;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Calani.BusinessObjects.TaskActivity
{
	class TaskActivityManager : SmartCollectionManager<task_activity, long>
	{
		public TaskActivityManager(long organizationId)
		{
			KeyProperty = "id";
			OrganizationID = organizationId;
		}

		internal override DbSet<task_activity> GetDbSet(CalaniEntities db)
		{
			return db.task_activity;
		}

		internal override SmartQuery<task_activity> GetById(long id)
		{
			var q = base.GetById(id);
			q.Query = (from ta in q.Context.task_activity where ta.Id == id select ta);
			return q;
		}

		internal override long GetRecordOrganization(task_activity record)
		{
			if (record != null && record.OrganizationId != null) return record.OrganizationId.Value;
			return -1;
		}

		internal override SmartQuery<task_activity> GetBaseQuery()
		{
			var q = base.GetBaseQuery();

			// base
			q.Query = (from ta in q.Context.task_activity select ta);
			if (OrganizationID > -1) q.Query = (from r in q.Query where r.OrganizationId == OrganizationID select r);

			// default sorting
			if (!IgnoreRecordStatus) q.Query = (from r in q.Query orderby r.CreatedAt select r);

			return q;
		}


		public List<TaskActivityMessage> GetAllTaskActivityByTaskId(long TaskId, long userId)
		{
			try
			{
				CalaniEntities db = new CalaniEntities();
				var taskActivities = (from ta in db.task_activity select ta)
					.Where(e => e.TaskId == TaskId && e.OrganizationId == OrganizationID)
					.Select(e => new TaskActivityMessage
					{
						Id = e.Id,
						CreatedAt = e.CreatedAt,
						TaskId = e.TaskId,
						OrganizationId = e.OrganizationId,
						Message = e.Message,
						ContactId = e.ContactId
					}).ToList();

				foreach (var taskActivity in taskActivities)
				{
					var contact = (from c in db.contacts select c).FirstOrDefault(c => c.id == taskActivity.ContactId);
					string fCode = string.IsNullOrEmpty(contact.firstName) ? string.Empty : contact.firstName.Substring(0, 1);
					string lCode = string.IsNullOrEmpty(contact.lastName) ? string.Empty : contact.lastName.Substring(0, 1);
					taskActivity.CodeName = fCode + " " + lCode;
					taskActivity.UserFirstName = contact.firstName;
					taskActivity.UserLastName = contact.lastName;
					taskActivity.IsOwned = userId == taskActivity.ContactId;
				}

				return taskActivities.ToList();
			}
			catch (System.Exception ex)
			{

				throw ex;
			}
			
		}
	}
}
