﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Resources;

namespace Calani.BusinessObjects
{
    public class SharedResource
    {
        public static ResourceManager Resource { get; set; }

        //public static string GetResource(string n, CultureInfo culture)
        //{
        //    if (Resource == null)
        //        return n;

        //    return Resource.GetString(n, culture);
        //}

        public static string ReadStringResources(CultureInfo culture)
        {
            var rs = Resource.GetResourceSet(culture, false, false);
            
            dynamic dynObject = new ExpandoObject();

            IDictionaryEnumerator dict = rs.GetEnumerator();
            while (dict.MoveNext())
            {
                AddProperty(dynObject, dict.Key as string, dict.Value);
            }

            return JsonConvert.SerializeObject(dynObject);
        }

        private static void AddProperty(ExpandoObject expando, string propertyName, object propertyValue)
        {
            var expandoDict = expando as IDictionary<string, object>;
            if (expandoDict.ContainsKey(propertyName))
            {
                expandoDict[propertyName] = propertyValue;
            }
            else
            {
                expandoDict.Add(propertyName, propertyValue);
            }
        }
    }

}
