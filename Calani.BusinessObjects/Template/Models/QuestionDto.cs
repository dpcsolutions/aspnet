﻿using System;
using Calani.BusinessObjects.Enums;

namespace Calani.BusinessObjects.Template.Models
{
    public class QuestionDto
    {
        public long questionID { get; set; }
        public long TemplateId { get; set; }
        public string Question { get; set; }
        public string Descriptions { get; set; }
        public string ResponseType { get; set; }
        public Nullable<System.DateTime> LastUpdate { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<long> UpdatedBy { get; set; }
        public Nullable<bool> IsMandatory { get; set; }
        public RecordStatus RecordStatus { get; set; }
        public string OptionForPickList { get; set; }
        public string PossibleAnswer { get; set; }
    }
}
