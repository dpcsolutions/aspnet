﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using log4net;

namespace Calani.BusinessObjects.Template
{
    public partial class TemplateManager : Generic.SmartCollectionManager<Model.templates, long>
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public TemplateManager(long organizationId)
        {
            KeyProperty = "templateId";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }
        internal override DbSet<templates> GetDbSet(Model.CalaniEntities db)
        {
            return db.templates;
        }

        internal override SmartQuery<templates> GetById(long id)
        {
            var q = base.GetById(id);
            q.Query = (from r in q.Context.templates where r.templateId == id select r);
            return q;
        }
        
        public List<templates> ListTemplates()
        {
            var dbq = base.GetBaseQuery();
            var templates = (from r in dbq.Context.templates where r.recordStatus == 0 orderby r.templateId select r).ToList();
            return templates;
        }
        
        public List<templates> ListTemplatesLightWeight()
        {
            var dbq = base.GetBaseQuery();
            var templates = (from r in dbq.Context.templates where r.recordStatus == 0 orderby r.templateId select r).ToList()
                .Select(x => new templates
                {
                    organizationId = x.organizationId,
                    recordStatus = x.recordStatus,
                    templateId = x.templateId,
                    listName = x.listName
                }).ToList();
            return templates;
        }
        
        public SmartAction<templates> SaveOrUpdate(long id, templates templates)
        {
            try
            {

                SmartAction<templates> result;
                if (id > 0)
                {
                    result = this.Update(id, templates);
                }
                else
                {
                    result = this.Add(templates);
                }
                _log.Info($"SaveOrUpdate. userId:{id}");
                return result;
            }
            catch (Exception e)
            {
                _log.Info($"SaveOrUpdate. error. ", e);
                throw;
            }

        }

    }
    
}
