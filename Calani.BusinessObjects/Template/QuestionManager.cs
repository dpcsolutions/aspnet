﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using Calani.BusinessObjects.Enums;
using Calani.BusinessObjects.Generic;
using Calani.BusinessObjects.Model;
using Calani.BusinessObjects.Template.Models;
using log4net;

namespace Calani.BusinessObjects.Template
{
    public partial class QuestionManager : Generic.SmartCollectionManager<Model.questions, long>
    {
        private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public QuestionManager(long organizationId)
        {
            KeyProperty = "questionID";
            RecordStatusProperty = "recordStatus";
            OrganizationID = organizationId;
        }
        internal override DbSet<questions> GetDbSet(Model.CalaniEntities db)
        {
            return db.questions;
        } 
        
        public List<questions> GetByTemplateId(long templateId, long taskListId)
        {
            var filteredQuestions = base.GetBaseQuery().Context.questions
                .Where(r => r.templateId == templateId && r.recordStatus == 0)
                .Include(questions => questions.tasklistanswers)
                .Include(questions => questions.templates)
                .ToList(); 
            
            var result = filteredQuestions
                .Select(r => new questions
                {
                    questionID = r.questionID, 
                    question = r.question,
                    isMandatory = r.isMandatory,
                    optionForPickList = r.optionForPickList,
                    responseType = r.responseType,
                    possibleAnswer = r.possibleAnswer,
                    templateId = r.templateId,
                    descriptions = r.descriptions,
                    recordStatus = r.recordStatus,
                    templates = r.templates,
                    tasklistanswers = r.tasklistanswers
                        .Where(a => a.taskListId == taskListId)
                        .Take(1)
                        .ToList()
                })
                .ToList();

            return result;
        }

        
        internal override SmartQuery<questions> GetBaseQuery()
        {
            var q = base.GetBaseQuery();
            q.Query = from r in q.Context.questions 
                where r.recordStatus != (int)RecordStatus.Deleted select r;
            return q;
        }
        
        public bool DeleteQuestionByQuestionId(long questionID,long userID, long templateID)
        {
            try
            {
                using (var db = new Model.CalaniEntities())
                {
                    var question = db.questions.FirstOrDefault(q => q.questionID == questionID && q.templateId == templateID);
                    if (question != null)
                    {
                        question.recordStatus = -1;
                        question.lastUpdate = DateTime.UtcNow;
                        question.updatedBy = userID;
                        db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        _log.Warn($"Question with ID {questionID} not found.");
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("Error deleting question", ex);
                throw;
            }
        }

        public List<QuestionDto> GetQuestionsByTemplateId(long? templateID)
        {
            var q = GetBaseQuery();
            q.Query = q.Query.Where(c => c.templateId == templateID && c.recordStatus!=-1).OrderBy(c => c.questionID);
            var data = q.Query.ToList();
            return data.Select(r => new QuestionDto
            {
                questionID = r.questionID,
                Question = r.question,
                Descriptions = r.descriptions,
                ResponseType = r.responseType,
                IsMandatory = r.isMandatory,
                RecordStatus =(RecordStatus)r.recordStatus,
                OptionForPickList = r.optionForPickList,
                PossibleAnswer = r.possibleAnswer,
            }).ToList();
        }

        public long CreateQuestions(QuestionDto question)
        {
            try 
            {
                var q = GetBaseQuery();
                var questions = (from c in q.Query where c.templateId == question.TemplateId && c.questionID == question.questionID select c).FirstOrDefault();
                var rec = new questions()
                {
                    templateId = question.TemplateId,
                    question = question.Question,
                    descriptions = question.Descriptions,
                    responseType = question.ResponseType,
                    isMandatory = question.IsMandatory,
                    createdBy = question.CreatedBy,
                    lastUpdate = question.LastUpdate,
                    suspendedQID = question.questionID,
                    optionForPickList = question.OptionForPickList,
                    possibleAnswer = question.PossibleAnswer,
                };

                SmartAction<questions> ret;

                ret = Add(rec);

                if (ret.Success)
                {
                    if (questions != null && questions.recordStatus == (int)RecordStatus.Active)
                    {
                        questions.recordStatus = (int)RecordStatus.Suspended;
                        questions.lastUpdate = DateTime.UtcNow;
                    }
                    q.Context.SaveChanges();
                    return ret.Record.questionID;
                }
                else
                    return question.TemplateId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            

        }
    }
}
