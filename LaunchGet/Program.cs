﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LaunchGet
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = args.FirstOrDefault();

            System.Net.WebClient wc = new System.Net.WebClient();
            Console.Write(wc.DownloadString(url));
          
        }
    }
}
