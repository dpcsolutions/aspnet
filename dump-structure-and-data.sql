/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 5.7.8-rc-log : Database - calani
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`calani` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `calani`;

/*Table structure for table `addresses` */

DROP TABLE IF EXISTS `addresses`;

CREATE TABLE `addresses` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `street` varchar(45) DEFAULT NULL,
  `street2` varchar(45) DEFAULT NULL,
  `nb` varchar(5) DEFAULT NULL,
  `npa` varchar(10) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `postbox` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `caption` varchar(45) DEFAULT NULL,
  `clientId` bigint(20) DEFAULT NULL,
  `organizationId` bigint(20) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `recordStatus` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_addresses_clients_idx` (`clientId`),
  KEY `fk_addresses_organization_idx` (`organizationId`),
  CONSTRAINT `fk_addresses_clients` FOREIGN KEY (`clientId`) REFERENCES `clients` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_addresses_organization` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2296 DEFAULT CHARSET=utf8;

/*Data for the table `addresses` */

insert  into `addresses`(`id`,`street`,`street2`,`nb`,`npa`,`city`,`postbox`,`state`,`country`,`caption`,`clientId`,`organizationId`,`type`,`recordStatus`) values 
(2294,'test street',NULL,NULL,'1110','Morges',NULL,NULL,NULL,NULL,NULL,260,1,0),
(2295,'Rue du client',NULL,'1','00000','Ville',NULL,NULL,'Suisse',NULL,2126,260,2,0);

/*Table structure for table `attachments` */

DROP TABLE IF EXISTS `attachments`;

CREATE TABLE `attachments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(45) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `mimeType` varchar(255) DEFAULT NULL,
  `organizationId` bigint(20) DEFAULT NULL,
  `clientId` bigint(20) DEFAULT NULL,
  `jobId` bigint(20) DEFAULT NULL,
  `recordStatus` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_attachments_jobs_idx` (`clientId`),
  KEY `fk_attachments_organizations_idx` (`organizationId`),
  KEY `fk_attachments_jobs_idx1` (`jobId`),
  CONSTRAINT `fk_attachments_clients` FOREIGN KEY (`clientId`) REFERENCES `clients` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_attachments_jobs` FOREIGN KEY (`jobId`) REFERENCES `jobs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_attachments_organizations` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1609 DEFAULT CHARSET=utf8;

/*Data for the table `attachments` */

/*Table structure for table `bankaccount` */

DROP TABLE IF EXISTS `bankaccount`;

CREATE TABLE `bankaccount` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `organizationId` bigint(20) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `bankname` varchar(50) DEFAULT NULL,
  `accountname` varchar(50) DEFAULT NULL,
  `iban` varchar(50) DEFAULT NULL,
  `swift` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_bankaccount_organization_idx` (`organizationId`),
  CONSTRAINT `fk_bankaccount_organization` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8;

/*Data for the table `bankaccount` */

/*Table structure for table `bvr` */

DROP TABLE IF EXISTS `bvr`;

CREATE TABLE `bvr` (
  `organizationId` bigint(20) NOT NULL,
  `mode` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `idsenderbank` varchar(45) DEFAULT NULL,
  `idaccount` varchar(45) DEFAULT NULL,
  `topmargin` int(11) DEFAULT NULL,
  `leftmargin` int(11) DEFAULT NULL,
  `tobankline1` varchar(45) DEFAULT NULL,
  `tobankline2` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`organizationId`),
  CONSTRAINT `fk_organization` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `bvr` */

/*Table structure for table `clients` */

DROP TABLE IF EXISTS `clients`;

CREATE TABLE `clients` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `companyName` varchar(45) DEFAULT NULL,
  `organizationId` bigint(20) DEFAULT NULL,
  `recordStatus` int(11) NOT NULL DEFAULT '0',
  `vatNumber` varchar(45) DEFAULT NULL,
  `isCompany` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_clients_organizations_idx` (`organizationId`),
  CONSTRAINT `fk_clients_organizations` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2127 DEFAULT CHARSET=utf8;

/*Data for the table `clients` */

insert  into `clients`(`id`,`companyName`,`organizationId`,`recordStatus`,`vatNumber`,`isCompany`) values 
(2126,'Test Customer',260,0,NULL,NULL);

/*Table structure for table `contacts` */

DROP TABLE IF EXISTS `contacts`;

CREATE TABLE `contacts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `dateOfBirth` datetime DEFAULT NULL,
  `primaryPhone` varchar(45) DEFAULT NULL,
  `secondaryPhone` varchar(45) DEFAULT NULL,
  `primaryEmail` varchar(100) DEFAULT NULL,
  `secondaryEmail` varchar(100) DEFAULT NULL,
  `telecopy` varchar(45) DEFAULT NULL,
  `clientId` bigint(20) DEFAULT NULL,
  `organizationId` bigint(20) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `recoverCode` varchar(10) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `civtitle` varchar(45) DEFAULT NULL,
  `initials` varchar(5) DEFAULT NULL,
  `account` varchar(10) DEFAULT NULL,
  `weeklyHours` double DEFAULT NULL,
  `lang` varchar(10) DEFAULT NULL,
  `stafftoken` varchar(45) DEFAULT NULL,
  `recordStatus` int(11) NOT NULL DEFAULT '0',
  `notificationToken` varchar(200) DEFAULT NULL,
  `lastlogin` datetime DEFAULT NULL,
  `culture` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_contacts_clients_idx` (`clientId`),
  KEY `fk_contacts_organizations_idx` (`organizationId`),
  CONSTRAINT `fk_contacts_clients` FOREIGN KEY (`clientId`) REFERENCES `clients` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contacts_organizations` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2509 DEFAULT CHARSET=utf8;

/*Data for the table `contacts` */

insert  into `contacts`(`id`,`firstName`,`lastName`,`dateOfBirth`,`primaryPhone`,`secondaryPhone`,`primaryEmail`,`secondaryEmail`,`telecopy`,`clientId`,`organizationId`,`type`,`password`,`recoverCode`,`status`,`role`,`title`,`civtitle`,`initials`,`account`,`weeklyHours`,`lang`,`stafftoken`,`recordStatus`,`notificationToken`,`lastlogin`,`culture`) values 
(2506,'Prénom','Nom',NULL,NULL,NULL,'test@test.com',NULL,NULL,NULL,260,1,'9A8605F7C6C0C263E60F7E9F58DA56AFC9E18929',NULL,NULL,NULL,NULL,NULL,'PNo',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL),
(2507,'John','Smith',NULL,'0780000000',NULL,'john@test.com',NULL,NULL,NULL,260,2,NULL,NULL,NULL,NULL,NULL,NULL,'JSM',NULL,NULL,NULL,NULL,0,NULL,NULL,NULL),
(2508,'Sonia','Coop',NULL,NULL,NULL,'scoop@test.com',NULL,NULL,2126,260,10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL);

/*Table structure for table `currency` */

DROP TABLE IF EXISTS `currency`;

CREATE TABLE `currency` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `organizationId` bigint(20) NOT NULL,
  `currency` varchar(6) NOT NULL,
  `symbol` varchar(3) NOT NULL,
  `rate` double NOT NULL,
  `recordStatus` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `organizationId` (`organizationId`),
  CONSTRAINT `currency_ibfk_1` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `currency` */

/*Table structure for table `documentemplates` */

DROP TABLE IF EXISTS `documentemplates`;

CREATE TABLE `documentemplates` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userId` bigint(20) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `title` longtext,
  `content` longtext,
  `recordStatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_documenttemplates_contacts_idx` (`userId`),
  CONSTRAINT `fk_documenttemplates_contacts` FOREIGN KEY (`userId`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

/*Data for the table `documentemplates` */

/*Table structure for table `expenses` */

DROP TABLE IF EXISTS `expenses`;

CREATE TABLE `expenses` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` double DEFAULT NULL,
  `attachmentId` bigint(20) DEFAULT NULL,
  `timesheetId` bigint(20) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `approvedmount` double DEFAULT NULL,
  `recordStatus` int(11) NOT NULL DEFAULT '0',
  `lastModifiedById` bigint(20) DEFAULT NULL,
  `lastModificationDate` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `employeeId` bigint(20) DEFAULT NULL,
  `linkedjobid` bigint(20) DEFAULT NULL,
  `imported` bit(1) NOT NULL,
  `currencyRate` double DEFAULT NULL,  
  `currency` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_expenses_timesheets_idx` (`timesheetId`),
  KEY `fk_expenses_contact_idx` (`employeeId`),
  KEY `fk_expenses_jobs_idx` (`linkedjobid`),
  CONSTRAINT `fk_expenses_contact` FOREIGN KEY (`employeeId`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_expenses_jobs` FOREIGN KEY (`linkedjobid`) REFERENCES `jobs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_expenses_timesheets` FOREIGN KEY (`timesheetId`) REFERENCES `timesheets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=utf8;

/*Data for the table `expenses` */

/*Table structure for table `iosettings` */

DROP TABLE IF EXISTS `iosettings`;

CREATE TABLE `iosettings` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `organizationId` bigint(20) DEFAULT NULL,
  `provider` varchar(45) DEFAULT NULL,
  `config` longtext,
  PRIMARY KEY (`id`),
  KEY `fk_iosettings_organizations_idx` (`organizationId`),
  CONSTRAINT `fk_iosettings_organizations` FOREIGN KEY (`organizationId`) REFERENCES `iosettings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `iosettings` */

/*Table structure for table `jobreminders` */

DROP TABLE IF EXISTS `jobreminders`;

CREATE TABLE `jobreminders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `jobid` bigint(20) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_jobreminders_jobs_idx` (`jobid`),
  CONSTRAINT `fk_jobreminders_jobs` FOREIGN KEY (`jobid`) REFERENCES `jobs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8;

/*Data for the table `jobreminders` */

/*Table structure for table `jobs` */

DROP TABLE IF EXISTS `jobs`;

CREATE TABLE `jobs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `internalId` varchar(45) DEFAULT NULL,
  `clientId` bigint(20) NOT NULL,
  `clientContact` bigint(20) DEFAULT NULL,
  `clientAddress` bigint(20) DEFAULT NULL,
  `description` text,
  `rating` int(11) DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `total` double DEFAULT NULL,
  `street` varchar(45) DEFAULT NULL,
  `street2` varchar(45) DEFAULT NULL,
  `nb` varchar(10) DEFAULT NULL,
  `npa` int(11) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `sentToName` varchar(45) DEFAULT NULL,
  `sentToEmail` varchar(45) DEFAULT NULL,
  `internalNote` varchar(255) DEFAULT NULL,
  `dateCreated` datetime DEFAULT NULL,
  `dateQuoteSent` datetime DEFAULT NULL,
  `dateQuoteAccepted` datetime DEFAULT NULL,
  `dateQuoteRefused` datetime DEFAULT NULL,
  `dateQuoteValidity` datetime DEFAULT NULL,
  `dateOpenJob` datetime DEFAULT NULL,
  `dateJobHeld` datetime DEFAULT NULL,
  `dateJobDone` datetime DEFAULT NULL,
  `dateJobValidity` datetime DEFAULT NULL,
  `dateInvoicing` datetime DEFAULT NULL,
  `dateInvoicingOpen` datetime DEFAULT NULL,
  `dateFullyPaid` datetime DEFAULT NULL,
  `dateInvoiceDue` datetime DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  `importedIn` bigint(20) DEFAULT NULL,
  `invoiceDue` double DEFAULT NULL,
  `contractualUnitsCount` int(11) DEFAULT NULL,
  `organizationId` bigint(20) DEFAULT NULL,
  `lastUpdate` datetime DEFAULT NULL,
  `recordStatus` int(11) NOT NULL DEFAULT '0',
  `currency` varchar(6) DEFAULT NULL,
  `currencyRate` double DEFAULT NULL,
  `totalInCurrency` double DEFAULT NULL,
  `invoiceDueInCurrency` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_jobs_clients_idx` (`clientId`),
  KEY `fk_jobs_jobs` (`parentId`),
  KEY `fk_jobs_organizations_idx` (`organizationId`),
  KEY `fk_jobs_clientcontact_idx` (`clientContact`),
  KEY `fk_jobs_clientaddress_idx` (`clientAddress`),
  CONSTRAINT `fk_jobs_clientaddress` FOREIGN KEY (`clientAddress`) REFERENCES `addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_jobs_clientcontact` FOREIGN KEY (`clientContact`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_jobs_clients` FOREIGN KEY (`clientId`) REFERENCES `clients` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_jobs_jobs` FOREIGN KEY (`parentId`) REFERENCES `jobs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_jobs_organizations` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1525 DEFAULT CHARSET=utf8;

/*Data for the table `jobs` */

/*Table structure for table `log` */

DROP TABLE IF EXISTS `log`;

CREATE TABLE `log` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Date` datetime DEFAULT NULL,
  `Thread` varchar(255) DEFAULT NULL,
  `Level` varchar(50) DEFAULT NULL,
  `Logger` varchar(255) DEFAULT NULL,
  `Message` varchar(4000) DEFAULT NULL,
  `Exception` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5158 DEFAULT CHARSET=utf8;

/*Data for the table `log` */

/*Table structure for table `login` */

DROP TABLE IF EXISTS `login`;

CREATE TABLE `login` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` bigint(20) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `app` varchar(10) DEFAULT NULL,
  `orgid` bigint(20) DEFAULT NULL,
  `orgactiveusers` int(11) DEFAULT NULL,
  `partnerproviderid` bigint(20) DEFAULT NULL,
  `partnercode` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_login_contacts` (`userid`),
  CONSTRAINT `fk_login_contacts` FOREIGN KEY (`userid`) REFERENCES `contacts` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3450 DEFAULT CHARSET=latin1;

/*Data for the table `login` */

insert  into `login`(`id`,`userid`,`date`,`app`,`orgid`,`orgactiveusers`,`partnerproviderid`,`partnercode`) values 
(3449,2506,'2020-02-27 13:25:52','Web',260,1,NULL,NULL);

/*Table structure for table `notes` */

DROP TABLE IF EXISTS `notes`;

CREATE TABLE `notes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `jobId` bigint(20) DEFAULT NULL,
  `message` text,
  `author` bigint(20) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `recordStatus` int(11) NOT NULL DEFAULT '0',
  `lastModificationDate` datetime DEFAULT NULL,
  `lastModifiedById` bigint(20) DEFAULT NULL,
  `mobileTempId` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notes_jobs_idx` (`jobId`),
  KEY `fk_notes_author_idx` (`author`),
  CONSTRAINT `fk_notes_author` FOREIGN KEY (`author`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_notes_jobs` FOREIGN KEY (`jobId`) REFERENCES `jobs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

/*Data for the table `notes` */

/*Table structure for table `organizations` */

DROP TABLE IF EXISTS `organizations`;

CREATE TABLE `organizations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `owner` bigint(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `telecopy` varchar(45) DEFAULT NULL,
  `userLimit` int(11) DEFAULT NULL,
  `dateLimit` datetime DEFAULT NULL,
  `logo` longblob,
  `creationDate` datetime DEFAULT NULL,
  `activationCode` varchar(10) DEFAULT NULL,
  `activationDate` datetime DEFAULT NULL,
  `delayQuote` int(11) DEFAULT NULL,
  `delayInvoice` int(11) DEFAULT NULL,
  `financialYearStart` datetime DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `website` varchar(80) DEFAULT NULL,
  `vatnumber` varchar(50) DEFAULT NULL,
  `trialEnd` datetime DEFAULT NULL,
  `subscriptionStatus` varchar(45) DEFAULT NULL,
  `subscriptionStart` datetime DEFAULT NULL,
  `subscriptionEnd` datetime DEFAULT NULL,
  `organizationscol` varchar(45) DEFAULT NULL,
  `subscriptionId` varchar(80) DEFAULT NULL,
  `emailAlertLateInvoicesDay` int(11) DEFAULT NULL,
  `knowus` varchar(45) DEFAULT NULL,
  `partnership` varchar(25) DEFAULT NULL,
  `partprov_def_plan_price` double DEFAULT NULL,
  `partprov_def_user_price` double DEFAULT NULL,
  `beforeinvoice` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `owner_UNIQUE` (`owner`),
  CONSTRAINT `fk_organizationa_contacts` FOREIGN KEY (`owner`) REFERENCES `contacts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=utf8;

/*Data for the table `organizations` */

insert  into `organizations`(`id`,`name`,`owner`,`email`,`phone`,`telecopy`,`userLimit`,`dateLimit`,`logo`,`creationDate`,`activationCode`,`activationDate`,`delayQuote`,`delayInvoice`,`financialYearStart`,`currency`,`website`,`vatnumber`,`trialEnd`,`subscriptionStatus`,`subscriptionStart`,`subscriptionEnd`,`organizationscol`,`subscriptionId`,`emailAlertLateInvoicesDay`,`knowus`,`partnership`,`partprov_def_plan_price`,`partprov_def_user_price`,`beforeinvoice`) values 
(260,'Test',2506,'test@test.com','021 000 00 00',NULL,5,NULL,NULL,'2020-02-27 13:25:50',NULL,'2020-02-27 13:26:00',30,30,NULL,NULL,NULL,NULL,'2021-03-13 13:25:50',NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL);

/*Table structure for table `partnership` */

DROP TABLE IF EXISTS `partnership`;

CREATE TABLE `partnership` (
  `id` varchar(25) NOT NULL,
  `partner_invoicing` tinyint(1) unsigned DEFAULT '0' COMMENT 'if 1 the final user payment by braintree is disabled and replaced by a invoice for the customer',
  `partner_invoicing_monthy_plan_price` double DEFAULT NULL COMMENT 'if partner_invoicing',
  `partner_invoicing_monthly_addinextrauser_price` double DEFAULT NULL COMMENT 'if partner_invoicing',
  `partner_owner` bigint(20) DEFAULT NULL,
  `custom_planid_monthly` varchar(45) DEFAULT NULL,
  `custom_planid_yearly` varchar(45) DEFAULT NULL,
  `custom_addinextrauser_monthly` varchar(45) DEFAULT NULL,
  `custom_addinextrauser_yearly` varchar(45) DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT '1',
  `limit` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `partnership` */

/*Table structure for table `payments` */

DROP TABLE IF EXISTS `payments`;

CREATE TABLE `payments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `datePayed` datetime DEFAULT NULL,
  `amountPayed` double DEFAULT NULL,
  `jobId` bigint(20) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `recordStatus` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_payments_jobs_idx` (`jobId`),
  CONSTRAINT `fk_payments_jobs` FOREIGN KEY (`jobId`) REFERENCES `jobs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=344 DEFAULT CHARSET=utf8;

/*Data for the table `payments` */

/*Table structure for table `services` */

DROP TABLE IF EXISTS `services`;

CREATE TABLE `services` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `internalId` varchar(45) DEFAULT NULL,
  `unitPrice` double NOT NULL DEFAULT '0',
  `taxId` bigint(20) DEFAULT NULL,
  `description` tinytext,
  `organizationId` bigint(20) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `recordStatus` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_services_taxes_idx` (`taxId`),
  KEY `fk_services_organizations_idx` (`organizationId`),
  CONSTRAINT `fk_services_organizations` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_services_taxes` FOREIGN KEY (`taxId`) REFERENCES `taxes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2524 DEFAULT CHARSET=utf8;

/*Data for the table `services` */

insert  into `services`(`id`,`name`,`internalId`,`unitPrice`,`taxId`,`description`,`organizationId`,`type`,`recordStatus`) values 
(2515,'Marketing','MKT',0,NULL,'Activités de marketing',260,2,0),
(2516,'Vente','VNT',0,NULL,'Activités de vente',260,2,0),
(2517,'Ressources Humaines','RH',0,NULL,'Ressources humaines',260,2,0),
(2518,'Logistique','LOG',0,NULL,'Activités liées à la logistique',260,2,0),
(2519,'Inventaire','INV',0,NULL,'Activités liées à l\'inventaire',260,2,0),
(2520,'Administration','ADM',0,NULL,'Activités administratives',260,2,0),
(2521,'Finance','FIN',0,NULL,'Finance, comptabilité, facturation',260,2,0),
(2522,'Autre','AUT',0,NULL,'Diverses activités non spécifiques',260,2,0),
(2523,'Test article','TST',50,NULL,NULL,260,1,0);

/*Table structure for table `services_jobs` */

DROP TABLE IF EXISTS `services_jobs`;

CREATE TABLE `services_jobs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `jobId` bigint(20) NOT NULL,
  `taxName` varchar(200) DEFAULT NULL,
  `taxRate` double DEFAULT NULL,
  `serviceQuantity` double DEFAULT NULL,
  `serviceName` varchar(200) DEFAULT NULL,
  `servicePrice` double DEFAULT NULL,
  `serviceId` bigint(20) DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `totalPriceExclTax` double DEFAULT NULL,
  `taxAmount` double DEFAULT NULL,
  `totalPrinceIncTax` double DEFAULT NULL,
  `type` int(11) DEFAULT '1',
  `toinvoice` bigint(20) DEFAULT NULL,
  `currency` varchar(6) DEFAULT NULL,
  `servicePriceInCurrency` double DEFAULT NULL,
  `totalPriceExclTaxInCurrency` double DEFAULT NULL,
  `totalPriceIncTaxInCurrency` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_services_jobs_jobs_idx` (`jobId`),
  KEY `fk_services_jobs_services_idx` (`serviceId`),
  CONSTRAINT `fk_services_jobs_jobs` FOREIGN KEY (`jobId`) REFERENCES `jobs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_services_jobs_services` FOREIGN KEY (`serviceId`) REFERENCES `services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3846 DEFAULT CHARSET=utf8;

/*Data for the table `services_jobs` */

/*Table structure for table `taxes` */

DROP TABLE IF EXISTS `taxes`;

CREATE TABLE `taxes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `rate` double NOT NULL DEFAULT '0',
  `organizationId` bigint(20) DEFAULT NULL,
  `isDefault` bit(1) DEFAULT NULL,
  `lastChange` datetime DEFAULT NULL,
  `recordStatus` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_taxes_organizations_idx` (`organizationId`),
  CONSTRAINT `fk_taxes_organizations` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=783 DEFAULT CHARSET=utf8;

/*Data for the table `taxes` */

insert  into `taxes`(`id`,`name`,`rate`,`organizationId`,`isDefault`,`lastChange`,`recordStatus`) values 
(780,'TVA 7.7',7.7,260,NULL,NULL,0),
(781,'TVA 3.7',3.7,260,NULL,NULL,0),
(782,'TVA 2.5',2.5,260,NULL,NULL,0);

/*Table structure for table `timesheetrecords` */

DROP TABLE IF EXISTS `timesheetrecords`;

CREATE TABLE `timesheetrecords` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `timesheetId` bigint(20) DEFAULT NULL,
  `visitId` bigint(20) DEFAULT NULL,
  `serviceId` bigint(20) DEFAULT NULL,
  `serviceName` varchar(45) DEFAULT NULL,
  `servicePrice` double DEFAULT NULL,
  `startDate` datetime DEFAULT NULL,
  `endDate` datetime DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `duration` double DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `recordStatus` int(11) NOT NULL DEFAULT '0',
  `lastModificationDate` datetime DEFAULT NULL,
  `lastModifiedBy` bigint(20) DEFAULT NULL,
  `jobId` bigint(20) DEFAULT NULL,
  `serviceType` int(11) DEFAULT NULL,
  `imported` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `fk_timesheetRecords_timesheets_idx` (`timesheetId`),
  KEY `fk_lastModifiedBy_idx` (`lastModifiedBy`),
  KEY `fk_jobItem_jobId_idx` (`jobId`),
  KEY `fk_timesheetRecords_service_idx` (`serviceId`),
  KEY `fk_timesheetRecords_visit_idx` (`visitId`),
  CONSTRAINT `fk_timesheetRecords_job` FOREIGN KEY (`jobId`) REFERENCES `jobs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_timesheetRecords_lastModifiedBy` FOREIGN KEY (`lastModifiedBy`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_timesheetRecords_service` FOREIGN KEY (`serviceId`) REFERENCES `services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_timesheetRecords_timesheets` FOREIGN KEY (`timesheetId`) REFERENCES `timesheets` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_timesheetRecords_visit` FOREIGN KEY (`visitId`) REFERENCES `visits` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=719 DEFAULT CHARSET=utf8;

/*Data for the table `timesheetrecords` */

/*Table structure for table `timesheets` */

DROP TABLE IF EXISTS `timesheets`;

CREATE TABLE `timesheets` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employeeId` bigint(20) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `week` int(11) DEFAULT NULL,
  `sentDate` datetime DEFAULT NULL,
  `acceptedDate` datetime DEFAULT NULL,
  `rejectDate` datetime DEFAULT NULL,
  `comment` longtext,
  `organizationId` bigint(20) DEFAULT NULL,
  `recordStatus` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_timesheets_contacts_idx` (`employeeId`),
  KEY `fk_timesheets_organizations_idx` (`organizationId`),
  CONSTRAINT `fk_timesheets_contacts` FOREIGN KEY (`employeeId`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_timesheets_organizations` FOREIGN KEY (`organizationId`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;

/*Data for the table `timesheets` */

/*Table structure for table `uservariables` */

DROP TABLE IF EXISTS `uservariables`;

CREATE TABLE `uservariables` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user` bigint(20) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `value` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=541 DEFAULT CHARSET=utf8;

/*Data for the table `uservariables` */

/*Table structure for table `visits` */

DROP TABLE IF EXISTS `visits`;

CREATE TABLE `visits` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `jobId` bigint(20) DEFAULT NULL,
  `dateStart` datetime DEFAULT NULL,
  `dateEnd` datetime DEFAULT NULL,
  `visitStatus` int(11) DEFAULT NULL,
  `recordStatus` int(11) NOT NULL DEFAULT '0',
  `type` int(11) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `orgId` bigint(20) DEFAULT NULL,
  `repeatGroupId` varchar(45) DEFAULT NULL,
  `numrepeat` int(11) DEFAULT NULL,
  `repeatdays` varchar(20) DEFAULT NULL,
  `serviceId` bigint(20) DEFAULT NULL,
  `lastModificationDate` datetime DEFAULT NULL,
  `lastModifiedById` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_visits_jobs_idx` (`jobId`),
  KEY `fk_visits_org_idx` (`orgId`),
  KEY `idx_recurring_group` (`repeatGroupId`),
  KEY `fk_visit_services_idx` (`serviceId`),
  CONSTRAINT `fk_visit_services` FOREIGN KEY (`serviceId`) REFERENCES `services` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_visits_jobs` FOREIGN KEY (`jobId`) REFERENCES `jobs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_visits_org` FOREIGN KEY (`orgId`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=560 DEFAULT CHARSET=utf8;

/*Data for the table `visits` */

/*Table structure for table `visits_contacts` */

DROP TABLE IF EXISTS `visits_contacts`;

CREATE TABLE `visits_contacts` (
  `visitId` bigint(20) NOT NULL,
  `contactId` bigint(20) NOT NULL,
  PRIMARY KEY (`visitId`,`contactId`),
  KEY `fk_visits_contacts_visits_idx` (`visitId`),
  KEY `fk_visits_contacts_contatcs_idx` (`contactId`),
  CONSTRAINT `fk_visits_contacts_contacts` FOREIGN KEY (`contactId`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_visits_contacts_visits` FOREIGN KEY (`visitId`) REFERENCES `visits` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Calendars*/
CREATE TABLE IF NOT EXISTS calendars (
  id bigint NOT NULL AUTO_INCREMENT,
  name varchar(45) DEFAULT NULL,
  code varchar(45) DEFAULT NULL,
  organization_Id BIGINT NOT NULL,
  updated_at DATETIME DEFAULT NULL,
  updated_by bigint DEFAULT NULL,
  recordStatus int default null,
  PRIMARY KEY (`id`),
  KEY `fk_calendars_contacts_idx` (`updated_by`),
  KEY `fk_calendars_organizations_idx` (`organization_Id`),
  CONSTRAINT `fk_calendars_contacts` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_calendars_organizations` FOREIGN KEY (`organization_Id`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS calendars_rec (
  id bigint NOT NULL AUTO_INCREMENT,
  calendar_id BIGINT NOT NULL,
  name varchar(45) DEFAULT NULL,
  start_date DATETIME DEFAULT NULL,
  end_date DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  updated_by BIGINT DEFAULT NULL,
  recordStatus INT DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_calendars_rec_contacts_idx` (`updated_by`),
  KEY `fk_calendars_rec_calendars_idx` (`calendar_id`),
  CONSTRAINT `fk_calendars_rec_contacts` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_calendars_rec_calendars` FOREIGN KEY (`calendar_id`) REFERENCES `calendars` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


/*profiles*/

CREATE TABLE IF NOT EXISTS profiles (
  id bigint NOT NULL AUTO_INCREMENT,
  name varchar(45) DEFAULT NULL,
  code varchar(45) DEFAULT NULL,
  calendar_Id BIGINT DEFAULT NULL,
  organization_Id BIGINT NOT NULL,
  updated_at DATETIME DEFAULT NULL,
  updated_by bigint DEFAULT NULL,
  recordStatus int default null,
  PRIMARY KEY (`id`),
  KEY `fk_profiles_contacts_idx` (`updated_by`),
  KEY `fk_profiles_organizations_idx` (`organization_Id`),
  KEY `fk_profiles_calendars_idx` (`calendar_Id`),
  CONSTRAINT `fk_profiles_contacts`			FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profiles_organizations`	FOREIGN KEY (`organization_Id`) REFERENCES `organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profiles_calendars`		FOREIGN KEY (`calendar_Id`) REFERENCES `calendars` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS profiles_rec (
  id bigint NOT NULL AUTO_INCREMENT,
  profile_id BIGINT NOT NULL,
  color varchar(8) DEFAULT NULL,
  name varchar(45) DEFAULT NULL,
  start varchar(8) DEFAULT NULL,
  end varchar(8) DEFAULT NULL,
  rate INT NOT NULL DEFAULT 100,
  mo BIT NOT NULL DEFAULT 0,
  tu BIT NOT NULL DEFAULT 0,
  we BIT NOT NULL DEFAULT 0,
  th BIT NOT NULL DEFAULT 0,
  fr BIT NOT NULL DEFAULT 0,
  sa BIT NOT NULL DEFAULT 0,
  su BIT NOT NULL DEFAULT 0,
  holidays BIT NOT NULL DEFAULT 0,
  updated_at DATETIME DEFAULT NULL,
  updated_by BIGINT DEFAULT NULL,
  recordStatus INT DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_profiles_rec_contacts_idx` (`updated_by`),
  KEY `fk_profiles_rec_profiles_idx` (`profile_id`),
  CONSTRAINT `fk_profiles_rec_contacts` FOREIGN KEY (`updated_by`) REFERENCES `contacts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_profiles_rec_profiles` FOREIGN KEY (`profile_id`) REFERENCES `profiles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


/*Data for the table `visits_contacts` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
