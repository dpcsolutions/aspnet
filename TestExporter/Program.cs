﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calani.BusinessObjects.IO;
using System.Collections.Specialized;
using System.IO;
using RestSharp;
using Calani.BusinessObjects.Model;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace TestExporter
{
    class Program
    {
        
        private static bool ValidateRemoteCertificate(object sender, X509Certificate cert, X509Chain chain, SslPolicyErrors error)
        {
            // If the certificate is a valid, signed certificate, return true.
            if (error == System.Net.Security.SslPolicyErrors.None)
            {
                return true;
            }

            Console.WriteLine("X509Certificate [{0}] Policy Error: '{1}'",
                cert.Subject,
                error.ToString());

            return false;
        }

       

        static void Main(string[] args)
        {

            /*
                  ServicePointManager.ServerCertificateValidationCallback += ValidateRemoteCertificate;
                  ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;

                  var client1 = new RestClient("https://api.winbizcloud.ch/Bizinfo");
                  var request = new RestRequest(Method.POST);
                  request.AddHeader("cache-control", "no-cache");
                  request.AddHeader("winbiz-key", "TjZabgSCVfSBkzotyTRR6ObQx3NxYHaTBaxiQ45F1jeavBxJ1I2K9UwtER0+08jB7vf3RBcphMurYuxElTLA1bq6RZDOJl9jC67MN1a4zAGzEAxyW6ZHt3/EnclUIm/lAOlRTyCwsqUmkxKbOuH4wGSx5o3rj08MySKE8KoS3eI=");
                  request.AddHeader("winbiz-year", "2019");
                  request.AddHeader("winbiz-companyid", "1");
                  request.AddHeader("winbiz-password", "u6aWWiDaT70G1wO2iynwUNNFtYLoBNasdmv+thQe0pBSgMGRjYcgzjSmXMDXKij0zmgd9e7kzOtxDDSeWEjrHUkQCxbXsL8Ad30f/Z5kJhU6/qwg6Eyxc1FKewqBg1Uz+yEXUqt5gtPU3F2Y2hRVIESUcIj4YkcNlpaQHpQ7y0Y=");
                  request.AddHeader("winbiz-username", "wb-preyes");
                  request.AddHeader("winbiz-companyname", "DPC Solutions S%C3%A0rl");
                 // request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                  //request.AddHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW");
                  // request.AddParameter("multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW", "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"winbiz-method\"\r\n\r\nDOCUMENTIMPORT\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--", ParameterType.RequestBody);
                  request.AddParameter("winbiz-method", "DOCUMENTIMPORT");
                  request.AddFile("winbiz-file", File.ReadAllBytes("c:\\winbiz10.wdx"), "winbiz10.wdx", "application/octet-stream");
                  request.AlwaysMultipartFormData = true;
                  IRestResponse response = client1.Execute(request);
                  */

            /*
            WinBizImporter engine = new WinBizImporter();

            engine.init();

            var organisationId = 1;

            DataImporter io = new DataImporter(organisationId, engine);

             var ret3 = io.registerInvoice(new importInvoice()
               {
                   productAccountNum = "1100",
                   amountInForeignCCY = 687.5,
                   amountInLocalCCY = 1072.5,//108.5,
                   rebateAccountNum = "3090",
                   clientAddress = new importAddress()
                   {
                       CivilTitle = "Monsieur",
                       Code = "LGE10001",
                       Company = "Microsoft Corp",
                       CountryCode = "USA",
                       FirstName = "William",
                       Language = "F",
                       LastName = "Gates A",
                       SimpleName = "JF",
                       Street1 = "Microsoft Way",
                       Street2 = "PO Box 1",
                       Town = "Redmond",
                       ZipCode = "11122333"
                   },
                   creationDate = DateTime.Now,
                   exchangeRate = 1.56,
                   externalSystemId = "",
                   gesMobileDocId = "",
                   gesMobileDocURLRef = "",
                   invoiceCode = "Installation reseau",
                   isNew = true,
                   LocalCCYISO = "EUR",//"CHF",
                   update = false,
                   updateAddress = true,
                   updateItems = true,
                   items = new List<importItem>()
                   {
                       new importItem {
                        Code="",
                        ShortDescriptionFrench="MS Word XP 1",
                        LongDescriptionFrench="Microsoft Word XP 2003",
                        PriceInLocalCCY = 300,
                        PriceInForeignCCY = 192.3,
                        Quantity = 1,
                        TotalInForeignCCY = 173.08,
                        TotalInLocalCCY = 270,
                        TotalIncludesVAT = false,
                        Unit="Pce",
                        VAT = 7.6,
                        Remise = 10
                       }
                   }
               });*/

            Calani.BusinessObjects.Model.jobs invoice = null;

            using (CalaniEntities db = new CalaniEntities())
            {
                invoice = (from j in db.jobs.Include("contacts")
                            .Include("addresses")
                            .Include("organizations")
                            .Include("services_jobs")
                           where j.id == 22
                           select j).FirstOrDefault();
            }

            var inv = new importInvoice(invoice);
            var organisationId = 1;


            WinBizImporter engine = new WinBizImporter();
            engine.init();
            
            DataImporter io = new DataImporter(organisationId, engine);

          /*  using (CalaniEntities db = new CalaniEntities())
            {
                var invoiceref = (from r in db.externalref
                                  where r.externalRef1 == inv.Code &&
                                    r.datatype == (int)importTypes.invoice
                                  select r).FirstOrDefault();

                if (invoiceref == null)
                    db.externalref.Add(new externalref()
                    {
                        calaniRef = invoice.id,
                        datatype = (int)importTypes.invoice,
                        externalRef1 = inv.Code
                    });
            }

            inv.update = true;*/
            var ret3 = io.registerInvoice(inv);

            if (ret3.ErrorsCount > 0 && ret3.ErrorsMsg.Contains("72"))
                //le document a déjà été comptabilisé
                return;

           /* using (CalaniEntities db = new CalaniEntities())
            {
                var invoiceref = (from r in db.externalref
                                 where r.externalRef1 == inv.Code &&
                                   r.datatype == (int)importTypes.invoice
                                 select r).FirstOrDefault();

                if (invoiceref == null)
                    db.externalref.Add(new externalref()
                    {
                        calaniRef = invoice.id,
                        datatype = (int)importTypes.invoice,
                        externalRef1 = inv.Code
                    });

                db.SaveChanges();
            }*/

            //Import adresses
            var ret = io.getAddresses();

            //Select addresses from UI then store result
            using (CalaniEntities db = new CalaniEntities())
            {

                //reading all clients from db including adresses
                foreach (var a in ret.Value)
                {
                    //search for the client otherwise create the client
                    var client = (from c in db.clients
                                  where c.companyName == a.Company &&
                                      c.organizationId == organisationId
                                  select c).FirstOrDefault();

                    if (client == null)
                    {
                        client = new Calani.BusinessObjects.Model.clients()
                        {
                            companyName = a.Company,
                            organizationId = organisationId
                        };

                        db.clients.Add(client);
                        db.SaveChanges();
                    }

                    var adressref = engine.getReference(a, db);

                    if (adressref == null)
                    {
                        Calani.BusinessObjects.Model.addresses adrs = new Calani.BusinessObjects.Model.addresses()
                        {
                            city = a.Town,
                            country = a.CountryCode,
                            npa = a.ZipCode,
                            organizationId = organisationId,
                            street = a.Street1,
                            street2 = a.Street2,
                            clientId = client.id,
                            type = 2
                        };

                        db.addresses.Add(adrs);
                        db.SaveChanges();

                        engine.setReference(new importAddress(adrs), db);
                    }

                    var contact = (from c in db.contacts
                                   where c.lastName == a.LastName &&
                                   c.firstName == a.FirstName &&
                                   c.organizationId == organisationId &&
                                   c.clientId == client.id
                                   select c).FirstOrDefault();

                    if (contact == null)
                    {
                        contact = new Calani.BusinessObjects.Model.contacts()
                        {
                            account = "",
                            firstName = a.FirstName,
                            lastName = a.LastName,
                            civtitle = a.CivilTitle,
                            clientId = client.id,
                            organizationId = organisationId
                        };

                        db.contacts.Add(contact);
                        db.SaveChanges();
                    }
                }
            }

            var ret2 = io.getServices();

            using (CalaniEntities db = new CalaniEntities())
            {
                if (ret2.ErrorsCount == 0)
                    foreach (var i in ret2.Value)
                    {
                        using (Calani.BusinessObjects.Model.CalaniEntities db1 = new Calani.BusinessObjects.Model.CalaniEntities())
                        {

                            //reading all clients from db including adresses
                            foreach (var a in ret2.Value)
                            {
                                var itemref = engine.getReference(a, db);

                                //search for the client otherwise create the client
                               /* var itemref = (from r in db1.externalref
                                               where r.externalRef1 == a.Code &&
                                               r.datatype == (int)importTypes.products
                                               select r).FirstOrDefault();*/

                                if (itemref == null)
                                {
                                    var service = new Calani.BusinessObjects.Model.services()
                                    {
                                        name = a.ShortDescriptionFrench,
                                        description = a.LongDescriptionFrench,
                                        organizationId = organisationId,
                                        unitPrice = a.PriceInLocalCCY,
                                        type = 1,
                                        internalId = (!String.IsNullOrEmpty(a.Code) ? a.Code :
                                                         (!String.IsNullOrEmpty(a.ShortDescriptionFrench) ? a.ShortDescriptionFrench :
                                                             (!String.IsNullOrEmpty(a.LongDescriptionFrench) ? a.LongDescriptionFrench :
                                                                 (new Random()).ToString())))
                                    };

                                    //add service
                                    db1.services.Add(service);

                                    db1.SaveChanges();

                                    engine.setReference(new importItem(service), db1);
                                }
                            }
                        }
                    }
                }
                /* MultiPartFormUpload multiPartFormUpload = new MultiPartFormUpload();

                 NameValueCollection headers = new NameValueCollection();

                 try
                 {
                      string _companyName = "DPC Solutions Sàrl";
                      string _userName = "wb-preyes";
                      string _password = "u6aWWiDaT70G1wO2iynwUNNFtYLoBNasdmv+thQe0pBSgMGRjYcgzjSmXMDXKij0zmgd9e7kzOtxDDSeWEjrHUkQCxbXsL8Ad30f/Z5kJhU6/qwg6Eyxc1FKewqBg1Uz+yEXUqt5gtPU3F2Y2hRVIESUcIj4YkcNlpaQHpQ7y0Y=";
                      string _companyId = "1";
                      string _key = "TjZabgSCVfSBkzotyTRR6ObQx3NxYHaTBaxiQ45F1jeavBxJ1I2K9UwtER0+08jB7vf3RBcphMurYuxElTLA1bq6RZDOJl9jC67MN1a4zAGzEAxyW6ZHt3/EnclUIm/lAOlRTyCwsqUmkxKbOuH4wGSx5o3rj08MySKE8KoS3eI=";
                      long _organisationId = 0;
                      int _year = DateTime.Now.Year;

                   //  headers.Add("Content-Type", "application/json");
                     headers.Add("winbiz-companyname", _companyName);
                     headers.Add("winbiz-username", _userName);
                     headers.Add("winbiz-password", _password);
                     headers.Add("winbiz-companyid", _companyId);
                     headers.Add("winbiz-year", _year.ToString());
                     headers.Add("winbiz-key", _key);

                     var parameters = new NameValueCollection() { };
                     parameters.Add("winbiz-method", "DOCUMENTIMPORT");

                     Dictionary<string, FileInfo> files = new Dictionary<string, FileInfo>();
                     files.Add("winbiz-file", new FileInfo("c:\\DIM\\winbiz9.wdx"));// new FileInfo(filePath) };

                     MultiPartFormUpload.UploadResponse response = multiPartFormUpload.Upload("https://api.winbizcloud.ch/Bizinfo", headers, parameters, files);
                     */
            }
        }
    }
