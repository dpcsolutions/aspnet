﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Concurrent;
using System.Threading;

namespace LogServerSocket
{
    public class TextNetworkStream
    {
        private TcpClient _client;
        private NetworkStream _stream;
        private string _socketOperation;
        private byte[] _bytesRead;

        public TextNetworkStream(TcpClient client)
        {
            _client = client;
            _stream = _client.GetStream();
        }

        private void ReadBytesFromStream(TcpClient client, NetworkStream stream)
        {
            _bytesRead = new byte[client.Available];
            stream.Read(_bytesRead, 0, _bytesRead.Length);
        }

        public bool IsHandshakeMessage()
        {
            ReadBytesFromStream(_client, _stream);

            _socketOperation = Encoding.UTF8.GetString(_bytesRead);
            return (Regex.IsMatch(_socketOperation, "^GET", RegexOptions.IgnoreCase));
        }

        private void PerformHandshakeInternal(string streamText, NetworkStream stream)
        {
            // 1. Obtain the value of the "Sec-WebSocket-Key" request header without any leading or trailing whitespace
            // 2. Concatenate it with "258EAFA5-E914-47DA-95CA-C5AB0DC85B11" (a special GUID specified by RFC 6455)
            // 3. Compute SHA-1 and Base64 hash of the new value
            // 4. Write the hash back as the value of "Sec-WebSocket-Accept" response header in an HTTP response
            string swk = Regex.Match(streamText, "Sec-WebSocket-Key: (.*)").Groups[1].Value.Trim();
            string swkAndSalt = swk + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
            byte[] swkAndSaltSha1 = System.Security.Cryptography.SHA1.Create().ComputeHash(Encoding.UTF8.GetBytes(swkAndSalt));
            string swkAndSaltSha1Base64 = Convert.ToBase64String(swkAndSaltSha1);

            // HTTP/1.1 defines the sequence CR LF as the end-of-line marker
            var str = Encoding.UTF8.GetBytes("HTTP/1.1 101 Switching Protocols\r\n" +
                "Connection: Upgrade\r\n" +
                "Upgrade: websocket\r\n" +
                "Sec-WebSocket-Accept: " + swkAndSaltSha1Base64 + "\r\n\r\n");

            stream.Write(str, 0, str.Length);
        }

        private static int GetHeader(bool finalFrame, bool contFrame)
        {
            int header = finalFrame ? 1 : 0;//fin: 0 = more frames, 1 = final frame
            header = (header << 1) + 0;//rsv1
            header = (header << 1) + 0;//rsv2
            header = (header << 1) + 0;//rsv3
            header = (header << 4) + (contFrame ? 0 : 1);//opcode : 0 = continuation frame, 1 = text
            header = (header << 1) + 0;//mask: server -> client = no mask

            return header;
        }

        private static byte[] IntToByteArray(ushort value)
        {
            var ary = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian)
            {
                Array.Reverse(ary);
            }

            return ary;
        }

        private static void SendMessageToClient(NetworkStream stream, string msg)
        {
            Queue<string> que = new Queue<string>(msg.SplitInGroups(125));
            int len = que.Count;

            while (que.Count > 0)
            {
                var header = GetHeader(
                    que.Count > 1 ? false : true,
                    que.Count == len ? false : true
                );

                byte[] list = Encoding.UTF8.GetBytes(que.Dequeue());
                header = (header << 7) + list.Length;
                if (stream.CanWrite)
                {
                    try
                    {
                        stream.Write(IntToByteArray((ushort)header), 0, 2);
                        stream.Write(list, 0, list.Length);
                    }
                    catch (Exception ex)
                    {
                        Console.Write(ex);
                    }
                    
                }
            }
        }

        public NetworkStream GetStream()
        {
            return _stream;
        }
        public bool CanWrite()
        {
            return _stream != null ? _stream.CanWrite : false;
        }

        public static void SendText(string msg, TextNetworkStream stream)
        {
            TextNetworkStream.SendMessageToClient(stream.GetStream(), msg);
        }

        public void SendText(string msg)
        {
            TextNetworkStream.SendMessageToClient(_stream, msg);
        }

        public void PerformHandshake()
        {
            PerformHandshakeInternal(_socketOperation, _stream);
        }

        public string GetMessageText()
        {
            string ret = null;

            bool fin = (_bytesRead[0] & 0b10000000) != 0;
            bool mask = (_bytesRead[1] & 0b10000000) != 0; // must be true, "All messages from the client to the server have this bit set"
            int opcode = _bytesRead[0] & 0b00001111; // expecting 1 - text message
            ulong offset = 2;
            ulong msglen = _bytesRead[1] & (ulong)0b01111111;

            if (msglen == 126)
            {
                // bytes are reversed because websocket will print them in Big-Endian, whereas
                // BitConverter will want them arranged in little-endian on windows
                msglen = BitConverter.ToUInt16(new byte[] { _bytesRead[3], _bytesRead[2] }, 0);
                offset = 4;
            }
            else if (msglen == 127)
            {
                // To test the below code, we need to manually buffer larger messages — since the NIC's autobuffering
                // may be too latency-friendly for this code to run (that is, we may have only some of the bytes in this
                // websocket frame available through client.Available).
                msglen = BitConverter.ToUInt64(new byte[] { _bytesRead[9], _bytesRead[8], _bytesRead[7],
                    _bytesRead[6], _bytesRead[5], _bytesRead[4], _bytesRead[3], _bytesRead[2] }, 0);
                offset = 10;
            }

            if (mask && msglen > 0)
            {
                byte[] decoded = new byte[msglen];
                byte[] masks = new byte[4] { _bytesRead[offset], _bytesRead[offset + 1],
                    _bytesRead[offset + 2], _bytesRead[offset + 3] };
                offset += 4;

                try
                {
                    for (ulong i = 0; i < msglen; ++i)
                        decoded[i] = (byte)(_bytesRead[offset + i] ^ masks[i % 4]);
                }
                catch (Exception ex)
                {
                    Console.Write(ex);
                }

                ret = Encoding.UTF8.GetString(decoded);
            }

            return ret;
        }

        public bool DataAvailable()
        {
            return _stream.DataAvailable && _client.Available > 3;
        }
    }

    public static class XLExtensions
    {
        public static IEnumerable<string> SplitInGroups(this string original, int size)
        {
            var p = 0;
            var l = original.Length;
            while (l - p > size)
            {
                yield return original.Substring(p, size);
                p += size;
            }
            yield return original.Substring(p);
        }
    }

    class IdentifiedStream
    {
        public IdentifiedStream(TextNetworkStream strm, string guid)
        {
            stream = strm;
            id = guid;
        }
        public TextNetworkStream stream;
        public string id;
    }

    public class CommandHandler {
        const string strHelp = "Known commands: cget <email>, cstream <email>, cregister <email>, clist, clisten <email>, help";

        static ConcurrentDictionary<string, List<string>> cd = new ConcurrentDictionary<string, List<string>>();
        static ConcurrentDictionary<string, List<IdentifiedStream>> streamListeners = new ConcurrentDictionary<string, List<IdentifiedStream>>();
        static ConcurrentDictionary<string, List<IdentifiedStream>> streamPublishers = new ConcurrentDictionary<string, List<IdentifiedStream>>();
        static ConcurrentDictionary<string, DateTime> lastClientActivities = new ConcurrentDictionary<string, DateTime>();

        TextNetworkStream _stream;

        public CommandHandler(TextNetworkStream stream)
        {
            _stream = stream;
        }

        public void ListClients()
        {
            _stream.SendText("Connected clients:");
            foreach (var sclient in cd.Keys)
            {
                DateTime dt = DateTime.Now;
                if (lastClientActivities.ContainsKey(sclient))
                {
                    while (!lastClientActivities.TryGetValue(sclient, out dt))
                    {
                        Thread.Sleep(100);
                    }
                }

                _stream.SendText(sclient + " last activity: " + dt.ToLocalTime());
            }
        }

        public void Help()
        {
            _stream.SendText(strHelp);
        }

        public static bool CheckEmptyOrNullAndReply(TextNetworkStream stream, string errorMsg, string okMessage, string param1)
        {
            if (!String.IsNullOrEmpty(param1))
            {
                TextNetworkStream.SendText(okMessage, stream);
                return true;
            }
            else
            {
                TextNetworkStream.SendText(errorMsg, stream);
                return false;
            }
        }

        public void RegisterPublisher(string email, string guid)
        {
            if (!cd.ContainsKey(email))
            {
                if (CheckEmptyOrNullAndReply(_stream, "No user email specified", "registered", email))
                {
                    cd.AddOrUpdate(email, new List<string>() { }, (key, lst) =>
                    {
                        return lst;
                    });

                    lastClientActivities.AddOrUpdate(email, DateTime.Now, (key, dt) =>
                    {
                        if (key == email)
                        {
                            return DateTime.Now;
                        }
                        else return dt;
                    });

                    streamPublishers.AddOrUpdate(email, new List<IdentifiedStream>() { new IdentifiedStream(_stream, guid)  },
                        (key, lst) =>
                        {
                            if (key == email)
                            {
                                lst.Add(new IdentifiedStream(_stream, guid));
                            }
                            return lst;
                        });
                }
            }

        }

        public int HandlerListener(string email, string param1, int index, string guid)
        {
            // registers the stream as a listener
            streamListeners.AddOrUpdate(email, new List<IdentifiedStream>() { new IdentifiedStream(_stream, guid) },
                (key, lst) => {
                    if (key == email)
                        lst.Add(new IdentifiedStream(_stream, guid));
                    return lst;
                });

            if (!cd.ContainsKey(email))
            {
                _stream.SendText(email + " not connected yet");
            }
            else if (CheckEmptyOrNullAndReply(_stream, "No user email specified", "logs > " + index, email))
            {
                List<string> lst;

                var max = 0;
                while (!cd.TryGetValue(email, out lst) || max == 10)
                {
                    max += 1;
                    Thread.Sleep(100);
                }

                if (lst != null)
                {
                    for (var i = (param1 == "" ? index : 0); i < lst.Count; i++)
                    {
                        if (i < lst.Count)
                        {
                            _stream.SendText("(" + (i + 1) + ") : " + lst[i]);
                        }
                        else
                        {
                            _stream.SendText("out of index");
                            break;
                        }
                    }

                    index = lst.Count;
                }
                else
                {
                    _stream.SendText("Failed to get the list, it is being used by another client. Please try later");
                }
            }

            return index;
        }

        public void HandleDebug(string email, string param1)
        {
            // sends to listeners

            if (streamPublishers.ContainsKey(email))
            {
                List<IdentifiedStream> lst;

                while (!streamPublishers.TryGetValue(email, out lst))
                {
                    Thread.Sleep(100);
                }

                foreach (var strm in lst)
                {
                    Console.WriteLine("sending to debug:" + strm.id);
                    TextNetworkStream.SendText("debug(" + param1 + ")", strm.stream);
                }
            }
        }

        public int HandleMessage(string email, string param1, int index, string text)
        {
            bool isResultFromDebugQuery = text.StartsWith(":res:");

            if (!isResultFromDebugQuery)
            {
                cd.AddOrUpdate(email, new List<string> { text }, (key, lstExist) =>
                {
                    if (key == email)
                    {
                        lstExist.Add(text);
                    }
                    return lstExist;
                });
                index += 1;

                lastClientActivities.AddOrUpdate(email, DateTime.Now, (key, dt) =>
                {
                    if (key == email)
                    {
                        return DateTime.Now;
                    }
                    else
                        return dt;
                });

                // sends to listeners
            }

            if (streamListeners.ContainsKey(email))
            {
                List<IdentifiedStream> lst;

                while (!streamListeners.TryGetValue(email, out lst))
                {
                    Thread.Sleep(100);
                }

                foreach (var strm in lst)
                {
                    Console.WriteLine("sending to listener:" + strm.id);
                    Thread.Sleep(100);
                    if(!isResultFromDebugQuery)
                    {
                        TextNetworkStream.SendText("stream: " + index + " " + text, strm.stream);
                    } else
                    {
                        TextNetworkStream.SendText(text, strm.stream);
                    }
                }
            }

            return index;
        }
    }

    public class Program
    {
        public static bool CheckEmptyOrNullAndReply(TextNetworkStream stream, string errorMsg, string okMessage, string param1)
        {
            if (!String.IsNullOrEmpty(param1))
            {
                TextNetworkStream.SendText(okMessage, stream);
                return true;
            }
            else
            {
                TextNetworkStream.SendText(errorMsg, stream);
                return false;
            }
        }

        private static void WriteToLog(string fileName, string text)
        {

            if (!Directory.Exists(Path.Combine(AppContext.BaseDirectory, "Logs")))
            {
                Directory.CreateDirectory(Path.Combine(AppContext.BaseDirectory, "Logs"));
            }
            
            text = DateTime.Now.ToLocalTime() + " - " + text;

            File.AppendAllText(fileName, text + "\n\r", Encoding.UTF8);
        }

        public static void Listener(TcpClient client)
        {
            TextNetworkStream stream = new TextNetworkStream(client);
            CommandHandler handler = new CommandHandler(stream);

            // enter to an infinite cycle to be able to handle every change in stream
            string fileName = null;
            string email = "";
            int index = 0;
            DateTime startDate = DateTime.Now;

            // we kill the process if it was started yesterday
            while (DateTime.Now.Day == startDate.Day)
            {
                while (!stream.DataAvailable()) {
                    Thread.Sleep(100);
                };

                if (stream.IsHandshakeMessage())
                {
                    stream.PerformHandshake();
                    handler.Help();
                }
                else
                {
                    string text = stream.GetMessageText();

                    if (!String.IsNullOrEmpty(text))                  
                    {
                        Console.WriteLine("got: " + text);
                        if (fileName == null)
                        {
                            fileName = Path.Combine(AppContext.BaseDirectory, "Logs", text + (DateTime.Now.ToString("_yyyyddMM")) + ".txt");
                        }

                        var cmds = text.Trim().Split(' ');
                        var cmd = cmds.Length > 0 ? cmds[0].Trim() : "";
                        var param1 = cmds.Length > 1 ? cmds[1].Trim() : "";
                        var param2 = cmds.Length > 2 ? cmds[2].Trim() : "";
                        var param3 = cmds.Length > 3 ? cmds[3].Trim() : "";
                        var param4 = cmds.Length > 4 ? cmds[4].Trim() : "";

                        switch (cmd)
                        {
                            case "clist":
                                handler.ListClients();
                                break;
                            case "help":
                                handler.Help();
                                break;
                            case "cregister":
                                if (String.IsNullOrEmpty(email) || param1 != "")
                                {
                                    email = param1;
                                    handler.RegisterPublisher(email, param2);
                                }
                                break;
                            case "cget":
                            case "clisten":
                                if(String.IsNullOrEmpty(email) || param1 != "")
                                {
                                    email = param1;
                                }

                                index = handler.HandlerListener(email, param1, index, param2);
                                break;

                            case "cdebug":
                                if (!String.IsNullOrEmpty(email))
                                {
                                    handler.HandleDebug(email, param1);
                                }
                                else
                                {
                                    Console.WriteLine("found empty email, not sending nor storing message");
                                }
                                break;
                            default:
                                // no specific command
                                try
                                {
                                    // writes to log file
                                    if (cmd.Contains("\u0003")) break;

                                    if(!text.StartsWith(":res:")) 
                                        WriteToLog(fileName, text);

                                    if (!String.IsNullOrEmpty(email))
                                    {
                                       index = handler.HandleMessage(email, param1, index, text);
                                    } else {
                                        Console.WriteLine("found empty email, not sending nor storing message");
                                    }
                                }
                                catch (Exception ex) {
                                    Console.WriteLine("Exception: " + ex);
                                }
                                break;
                        }
                    }
                    else
                        Console.WriteLine("text is empty");

                    Console.WriteLine();
                }
            }
        }
        public static void Main(string[] args)
        {
            string ip = args[0];
            int port = int.Parse(args[1]);
            var server = new TcpListener(IPAddress.Parse(ip), port);
            server.Start();
            Console.WriteLine("Server has started on {0}:{1}, Waiting for a connection…", ip, port);

            while (true)
            {
                try
                {
                    TcpClient client = server.AcceptTcpClient();

                    Console.WriteLine("A client connected.");
                    Task.Run(() => { Program.Listener(client); });
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
    }
}
